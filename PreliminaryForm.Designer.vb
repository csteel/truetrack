﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PreliminaryForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PreliminaryForm))
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.EnquiryMethodListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.gpbxQuestions = New System.Windows.Forms.GroupBox()
        Me.tlpResult = New System.Windows.Forms.TableLayoutPanel()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.cmbxPrelimResults = New System.Windows.Forms.ComboBox()
        Me.PrelimResultsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tlpQuestions = New System.Windows.Forms.TableLayoutPanel()
        Me.cmbxSuburb = New System.Windows.Forms.ComboBox()
        Me.SuburbListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtbxAmount = New System.Windows.Forms.TextBox()
        Me.lblPrelimContact = New System.Windows.Forms.Label()
        Me.lblTown = New System.Windows.Forms.Label()
        Me.txtbxPrelimContact = New System.Windows.Forms.TextBox()
        Me.lblPrelimArea = New System.Windows.Forms.Label()
        Me.lblSuburb = New System.Windows.Forms.Label()
        Me.lblBorrow = New System.Windows.Forms.Label()
        Me.cmbxTown = New System.Windows.Forms.ComboBox()
        Me.NZTownsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cmbxEnquiryType = New System.Windows.Forms.ComboBox()
        Me.lblPrelimSource = New System.Windows.Forms.Label()
        Me.lblReason = New System.Windows.Forms.Label()
        Me.cmbxPrelimSource = New System.Windows.Forms.ComboBox()
        Me.PrelimSourceBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblPurpose = New System.Windows.Forms.Label()
        Me.cmbxLoanPurpose = New System.Windows.Forms.ComboBox()
        Me.LoanPurposeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtContactEmail = New System.Windows.Forms.TextBox()
        Me.tlpName = New System.Windows.Forms.TableLayoutPanel()
        Me.txtbxSalutation = New System.Windows.Forms.TextBox()
        Me.pbSearchEnquires = New System.Windows.Forms.PictureBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.btnCheckEnquiries = New System.Windows.Forms.Button()
        Me.cmbxTitle = New System.Windows.Forms.ComboBox()
        Me.txtbxFirstName = New System.Windows.Forms.TextBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.btnCheckNames = New System.Windows.Forms.Button()
        Me.cmbxLastName = New System.Windows.Forms.ComboBox()
        Me.DisplayAllNamesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnCancelEnquiry = New System.Windows.Forms.Button()
        Me.lblSalutation = New System.Windows.Forms.Label()
        Me.btnAddComment = New System.Windows.Forms.Button()
        Me.txtbxPrelimComments = New System.Windows.Forms.TextBox()
        Me.lblComments = New System.Windows.Forms.Label()
        Me.PrelimReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActiveEnquiryClientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.NZTownsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.NZTownsTableAdapter()
        Me.PrelimReasonsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter()
        Me.PrelimResultsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter()
        Me.QrgListTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.QRGListTableAdapter()
        Me.SuburbListTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SuburbListTableAdapter()
        Me.btnWebApp = New System.Windows.Forms.Button()
        Me.btnWorksheetWizard = New System.Windows.Forms.Button()
        Me.ActiveDealersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActiveDealersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveDealersTableAdapter()
        Me.PrelimSourceTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimSourceTableAdapter()
        Me.PrelimSourceBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryMethodListTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryMethodListTableAdapter()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblDealerCode = New System.Windows.Forms.Label()
        Me.cmbxDealerId = New System.Windows.Forms.ComboBox()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.cmbxMethodEnquiry = New System.Windows.Forms.ComboBox()
        Me.lblMethodEnquiry = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryNumber = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ActiveEnquiryClientsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveEnquiryClientsTableAdapter()
        Me.QRGListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DisplayAllNamesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DisplayAllNamesTableAdapter()
        Me.LoanPurposeTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanPurposeTableAdapter()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryMethodListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxQuestions.SuspendLayout()
        Me.tlpResult.SuspendLayout()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpQuestions.SuspendLayout()
        CType(Me.SuburbListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NZTownsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimSourceBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoanPurposeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpName.SuspendLayout()
        CType(Me.pbSearchEnquires, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DisplayAllNamesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActiveEnquiryClientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActiveDealersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimSourceBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.QRGListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(31, 12)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 20
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EnquiryMethodListBindingSource
        '
        Me.EnquiryMethodListBindingSource.DataMember = "EnquiryMethodList"
        Me.EnquiryMethodListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'gpbxQuestions
        '
        Me.gpbxQuestions.Controls.Add(Me.tlpResult)
        Me.gpbxQuestions.Controls.Add(Me.tlpQuestions)
        Me.gpbxQuestions.Controls.Add(Me.tlpName)
        Me.gpbxQuestions.Controls.Add(Me.btnAddComment)
        Me.gpbxQuestions.Controls.Add(Me.txtbxPrelimComments)
        Me.gpbxQuestions.Controls.Add(Me.lblComments)
        Me.gpbxQuestions.Location = New System.Drawing.Point(12, 92)
        Me.gpbxQuestions.Name = "gpbxQuestions"
        Me.gpbxQuestions.Size = New System.Drawing.Size(920, 845)
        Me.gpbxQuestions.TabIndex = 2
        Me.gpbxQuestions.TabStop = False
        Me.gpbxQuestions.Text = "For us to help you can I ask you a few questions?"
        '
        'tlpResult
        '
        Me.tlpResult.ColumnCount = 2
        Me.tlpResult.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89.0!))
        Me.tlpResult.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpResult.Controls.Add(Me.lblResult, 0, 0)
        Me.tlpResult.Controls.Add(Me.cmbxPrelimResults, 1, 0)
        Me.tlpResult.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tlpResult.Location = New System.Drawing.Point(3, 815)
        Me.tlpResult.Name = "tlpResult"
        Me.tlpResult.RowCount = 1
        Me.tlpResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.tlpResult.Size = New System.Drawing.Size(914, 27)
        Me.tlpResult.TabIndex = 49
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblResult.Location = New System.Drawing.Point(3, 0)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(75, 28)
        Me.lblResult.TabIndex = 43
        Me.lblResult.Text = "Enquiry Result"
        Me.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbxPrelimResults
        '
        Me.cmbxPrelimResults.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxPrelimResults.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxPrelimResults.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "PrelimResult", True))
        Me.cmbxPrelimResults.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "PrelimResult", True))
        Me.cmbxPrelimResults.DataSource = Me.PrelimResultsBindingSource
        Me.cmbxPrelimResults.DisplayMember = "PrelimResult"
        Me.cmbxPrelimResults.FormattingEnabled = True
        Me.cmbxPrelimResults.Location = New System.Drawing.Point(92, 3)
        Me.cmbxPrelimResults.Name = "cmbxPrelimResults"
        Me.cmbxPrelimResults.Size = New System.Drawing.Size(802, 21)
        Me.cmbxPrelimResults.TabIndex = 18
        Me.cmbxPrelimResults.ValueMember = "PrelimResult"
        '
        'PrelimResultsBindingSource
        '
        Me.PrelimResultsBindingSource.DataMember = "PrelimResults"
        Me.PrelimResultsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        Me.PrelimResultsBindingSource.Filter = ""
        '
        'tlpQuestions
        '
        Me.tlpQuestions.ColumnCount = 6
        Me.tlpQuestions.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlpQuestions.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.tlpQuestions.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlpQuestions.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.tlpQuestions.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlpQuestions.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpQuestions.Controls.Add(Me.cmbxSuburb, 2, 1)
        Me.tlpQuestions.Controls.Add(Me.txtbxAmount, 2, 4)
        Me.tlpQuestions.Controls.Add(Me.lblPrelimContact, 0, 0)
        Me.tlpQuestions.Controls.Add(Me.lblTown, 3, 1)
        Me.tlpQuestions.Controls.Add(Me.txtbxPrelimContact, 2, 0)
        Me.tlpQuestions.Controls.Add(Me.lblPrelimArea, 0, 1)
        Me.tlpQuestions.Controls.Add(Me.lblSuburb, 1, 1)
        Me.tlpQuestions.Controls.Add(Me.lblBorrow, 0, 4)
        Me.tlpQuestions.Controls.Add(Me.cmbxTown, 4, 1)
        Me.tlpQuestions.Controls.Add(Me.cmbxEnquiryType, 2, 3)
        Me.tlpQuestions.Controls.Add(Me.lblPrelimSource, 0, 2)
        Me.tlpQuestions.Controls.Add(Me.lblReason, 0, 3)
        Me.tlpQuestions.Controls.Add(Me.cmbxPrelimSource, 2, 2)
        Me.tlpQuestions.Controls.Add(Me.lblPurpose, 3, 4)
        Me.tlpQuestions.Controls.Add(Me.cmbxLoanPurpose, 4, 4)
        Me.tlpQuestions.Controls.Add(Me.lblEmail, 3, 0)
        Me.tlpQuestions.Controls.Add(Me.txtContactEmail, 4, 0)
        Me.tlpQuestions.Location = New System.Drawing.Point(6, 78)
        Me.tlpQuestions.Name = "tlpQuestions"
        Me.tlpQuestions.RowCount = 5
        Me.tlpQuestions.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpQuestions.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpQuestions.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpQuestions.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpQuestions.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpQuestions.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpQuestions.Size = New System.Drawing.Size(908, 130)
        Me.tlpQuestions.TabIndex = 3
        '
        'cmbxSuburb
        '
        Me.cmbxSuburb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxSuburb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxSuburb.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "ContactSuburb", True))
        Me.cmbxSuburb.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactSuburb", True))
        Me.cmbxSuburb.DataSource = Me.SuburbListBindingSource
        Me.cmbxSuburb.DisplayMember = "Suburb"
        Me.cmbxSuburb.FormattingEnabled = True
        Me.cmbxSuburb.Location = New System.Drawing.Point(253, 28)
        Me.cmbxSuburb.Name = "cmbxSuburb"
        Me.cmbxSuburb.Size = New System.Drawing.Size(194, 21)
        Me.cmbxSuburb.TabIndex = 11
        Me.cmbxSuburb.ValueMember = "Suburb"
        '
        'SuburbListBindingSource
        '
        Me.SuburbListBindingSource.DataMember = "SuburbList"
        Me.SuburbListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'txtbxAmount
        '
        Me.txtbxAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "PrelimAmount", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtbxAmount.Location = New System.Drawing.Point(253, 103)
        Me.txtbxAmount.Name = "txtbxAmount"
        Me.txtbxAmount.Size = New System.Drawing.Size(194, 20)
        Me.txtbxAmount.TabIndex = 15
        Me.ToolTip1.SetToolTip(Me.txtbxAmount, "Only enter Numbers")
        '
        'lblPrelimContact
        '
        Me.lblPrelimContact.AutoSize = True
        Me.tlpQuestions.SetColumnSpan(Me.lblPrelimContact, 2)
        Me.lblPrelimContact.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblPrelimContact.Location = New System.Drawing.Point(3, 0)
        Me.lblPrelimContact.Name = "lblPrelimContact"
        Me.lblPrelimContact.Size = New System.Drawing.Size(121, 25)
        Me.lblPrelimContact.TabIndex = 32
        Me.lblPrelimContact.Text = "Contact phone number?"
        Me.lblPrelimContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTown
        '
        Me.lblTown.AutoSize = True
        Me.lblTown.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTown.Location = New System.Drawing.Point(481, 25)
        Me.lblTown.Name = "lblTown"
        Me.lblTown.Size = New System.Drawing.Size(56, 25)
        Me.lblTown.TabIndex = 45
        Me.lblTown.Text = "Town/City"
        Me.lblTown.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxPrelimContact
        '
        Me.txtbxPrelimContact.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.txtbxPrelimContact.Location = New System.Drawing.Point(253, 3)
        Me.txtbxPrelimContact.MaxLength = 20
        Me.txtbxPrelimContact.Name = "txtbxPrelimContact"
        Me.txtbxPrelimContact.Size = New System.Drawing.Size(194, 20)
        Me.txtbxPrelimContact.TabIndex = 9
        '
        'lblPrelimArea
        '
        Me.lblPrelimArea.AutoSize = True
        Me.lblPrelimArea.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblPrelimArea.Location = New System.Drawing.Point(3, 25)
        Me.lblPrelimArea.Name = "lblPrelimArea"
        Me.lblPrelimArea.Size = New System.Drawing.Size(128, 25)
        Me.lblPrelimArea.TabIndex = 34
        Me.lblPrelimArea.Text = "What area do you live in?"
        Me.lblPrelimArea.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSuburb
        '
        Me.lblSuburb.AutoSize = True
        Me.lblSuburb.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblSuburb.Location = New System.Drawing.Point(206, 25)
        Me.lblSuburb.Name = "lblSuburb"
        Me.lblSuburb.Size = New System.Drawing.Size(41, 25)
        Me.lblSuburb.TabIndex = 46
        Me.lblSuburb.Text = "Suburb"
        Me.lblSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBorrow
        '
        Me.lblBorrow.AutoSize = True
        Me.tlpQuestions.SetColumnSpan(Me.lblBorrow, 2)
        Me.lblBorrow.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblBorrow.Location = New System.Drawing.Point(3, 100)
        Me.lblBorrow.Name = "lblBorrow"
        Me.lblBorrow.Size = New System.Drawing.Size(186, 30)
        Me.lblBorrow.TabIndex = 38
        Me.lblBorrow.Text = "How much are you looking to borrow?"
        Me.lblBorrow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbxTown
        '
        Me.cmbxTown.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTown.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTown.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "ContactCity", True))
        Me.cmbxTown.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactCity", True))
        Me.cmbxTown.DataSource = Me.NZTownsBindingSource
        Me.cmbxTown.DisplayMember = "Towns"
        Me.cmbxTown.FormattingEnabled = True
        Me.cmbxTown.Location = New System.Drawing.Point(543, 28)
        Me.cmbxTown.Name = "cmbxTown"
        Me.cmbxTown.Size = New System.Drawing.Size(194, 21)
        Me.cmbxTown.TabIndex = 12
        Me.cmbxTown.ValueMember = "Towns"
        '
        'NZTownsBindingSource
        '
        Me.NZTownsBindingSource.DataMember = "NZTowns"
        Me.NZTownsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'cmbxEnquiryType
        '
        Me.cmbxEnquiryType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxEnquiryType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxEnquiryType.FormattingEnabled = True
        Me.cmbxEnquiryType.Location = New System.Drawing.Point(253, 78)
        Me.cmbxEnquiryType.Name = "cmbxEnquiryType"
        Me.cmbxEnquiryType.Size = New System.Drawing.Size(194, 21)
        Me.cmbxEnquiryType.TabIndex = 14
        '
        'lblPrelimSource
        '
        Me.lblPrelimSource.AutoSize = True
        Me.tlpQuestions.SetColumnSpan(Me.lblPrelimSource, 2)
        Me.lblPrelimSource.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblPrelimSource.Location = New System.Drawing.Point(3, 50)
        Me.lblPrelimSource.Name = "lblPrelimSource"
        Me.lblPrelimSource.Size = New System.Drawing.Size(234, 25)
        Me.lblPrelimSource.TabIndex = 30
        Me.lblPrelimSource.Text = "How did you come to know about Yes Finance?"
        Me.lblPrelimSource.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReason
        '
        Me.lblReason.AutoSize = True
        Me.tlpQuestions.SetColumnSpan(Me.lblReason, 2)
        Me.lblReason.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblReason.Location = New System.Drawing.Point(3, 75)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(114, 25)
        Me.lblReason.TabIndex = 36
        Me.lblReason.Text = "Type of Loan Enquiry?"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbxPrelimSource
        '
        Me.cmbxPrelimSource.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxPrelimSource.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxPrelimSource.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "PrelimSource", True))
        Me.cmbxPrelimSource.DataSource = Me.PrelimSourceBindingSource
        Me.cmbxPrelimSource.DisplayMember = "PrelimSource"
        Me.cmbxPrelimSource.FormattingEnabled = True
        Me.cmbxPrelimSource.Location = New System.Drawing.Point(253, 53)
        Me.cmbxPrelimSource.Name = "cmbxPrelimSource"
        Me.cmbxPrelimSource.Size = New System.Drawing.Size(194, 21)
        Me.cmbxPrelimSource.TabIndex = 13
        Me.cmbxPrelimSource.ValueMember = "PrelimSource"
        '
        'PrelimSourceBindingSource
        '
        Me.PrelimSourceBindingSource.DataMember = "PrelimSource"
        Me.PrelimSourceBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblPurpose
        '
        Me.lblPurpose.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblPurpose.AutoSize = True
        Me.lblPurpose.Location = New System.Drawing.Point(464, 108)
        Me.lblPurpose.Name = "lblPurpose"
        Me.lblPurpose.Size = New System.Drawing.Size(73, 13)
        Me.lblPurpose.TabIndex = 47
        Me.lblPurpose.Text = "Loan Purpose"
        Me.lblPurpose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbxLoanPurpose
        '
        Me.cmbxLoanPurpose.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxLoanPurpose.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxLoanPurpose.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanPurpose", True))
        Me.cmbxLoanPurpose.DataSource = Me.LoanPurposeBindingSource
        Me.cmbxLoanPurpose.DisplayMember = "LoanPurpose"
        Me.cmbxLoanPurpose.FormattingEnabled = True
        Me.cmbxLoanPurpose.Location = New System.Drawing.Point(543, 103)
        Me.cmbxLoanPurpose.MaxDropDownItems = 16
        Me.cmbxLoanPurpose.Name = "cmbxLoanPurpose"
        Me.cmbxLoanPurpose.Size = New System.Drawing.Size(194, 21)
        Me.cmbxLoanPurpose.TabIndex = 16
        Me.cmbxLoanPurpose.ValueMember = "LoanPurpose"
        '
        'LoanPurposeBindingSource
        '
        Me.LoanPurposeBindingSource.DataMember = "LoanPurpose"
        Me.LoanPurposeBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblEmail.Location = New System.Drawing.Point(505, 0)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(32, 25)
        Me.lblEmail.TabIndex = 0
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtContactEmail
        '
        Me.txtContactEmail.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactEmail", True))
        Me.txtContactEmail.Location = New System.Drawing.Point(543, 3)
        Me.txtContactEmail.MaxLength = 125
        Me.txtContactEmail.Name = "txtContactEmail"
        Me.txtContactEmail.Size = New System.Drawing.Size(194, 20)
        Me.txtContactEmail.TabIndex = 10
        '
        'tlpName
        '
        Me.tlpName.ColumnCount = 8
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130.0!))
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.0!))
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.0!))
        Me.tlpName.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.0!))
        Me.tlpName.Controls.Add(Me.txtbxSalutation, 7, 0)
        Me.tlpName.Controls.Add(Me.pbSearchEnquires, 5, 1)
        Me.tlpName.Controls.Add(Me.lblName, 0, 0)
        Me.tlpName.Controls.Add(Me.btnCheckEnquiries, 4, 1)
        Me.tlpName.Controls.Add(Me.cmbxTitle, 1, 0)
        Me.tlpName.Controls.Add(Me.txtbxFirstName, 2, 0)
        Me.tlpName.Controls.Add(Me.lblLastName, 3, 0)
        Me.tlpName.Controls.Add(Me.btnCheckNames, 2, 1)
        Me.tlpName.Controls.Add(Me.cmbxLastName, 4, 0)
        Me.tlpName.Controls.Add(Me.btnCancelEnquiry, 6, 1)
        Me.tlpName.Controls.Add(Me.lblSalutation, 5, 0)
        Me.tlpName.Location = New System.Drawing.Point(6, 19)
        Me.tlpName.Name = "tlpName"
        Me.tlpName.RowCount = 2
        Me.tlpName.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpName.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpName.Size = New System.Drawing.Size(908, 59)
        Me.tlpName.TabIndex = 0
        '
        'txtbxSalutation
        '
        Me.txtbxSalutation.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactDisplayName", True))
        Me.txtbxSalutation.Location = New System.Drawing.Point(707, 3)
        Me.txtbxSalutation.Name = "txtbxSalutation"
        Me.txtbxSalutation.Size = New System.Drawing.Size(194, 20)
        Me.txtbxSalutation.TabIndex = 5
        '
        'pbSearchEnquires
        '
        Me.pbSearchEnquires.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pbSearchEnquires.BackColor = System.Drawing.SystemColors.Control
        Me.pbSearchEnquires.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbSearchEnquires.Location = New System.Drawing.Point(538, 35)
        Me.pbSearchEnquires.Name = "pbSearchEnquires"
        Me.pbSearchEnquires.Size = New System.Drawing.Size(18, 18)
        Me.pbSearchEnquires.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbSearchEnquires.TabIndex = 256
        Me.pbSearchEnquires.TabStop = False
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblName.Location = New System.Drawing.Point(3, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(35, 29)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCheckEnquiries
        '
        Me.btnCheckEnquiries.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnCheckEnquiries.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnCheckEnquiries.Location = New System.Drawing.Point(382, 32)
        Me.btnCheckEnquiries.Name = "btnCheckEnquiries"
        Me.btnCheckEnquiries.Size = New System.Drawing.Size(102, 24)
        Me.btnCheckEnquiries.TabIndex = 8
        Me.btnCheckEnquiries.Text = "Search Enquiries"
        Me.btnCheckEnquiries.UseVisualStyleBackColor = False
        '
        'cmbxTitle
        '
        Me.cmbxTitle.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTitle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTitle.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactTitle", True))
        Me.cmbxTitle.FormattingEnabled = True
        Me.cmbxTitle.Items.AddRange(New Object() {"Mr", "Mrs", "Ms", "Miss", "Dr"})
        Me.cmbxTitle.Location = New System.Drawing.Point(44, 3)
        Me.cmbxTitle.Name = "cmbxTitle"
        Me.cmbxTitle.Size = New System.Drawing.Size(60, 21)
        Me.cmbxTitle.TabIndex = 1
        '
        'txtbxFirstName
        '
        Me.txtbxFirstName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactFirstName", True))
        Me.txtbxFirstName.Location = New System.Drawing.Point(113, 3)
        Me.txtbxFirstName.MaxLength = 40
        Me.txtbxFirstName.Name = "txtbxFirstName"
        Me.txtbxFirstName.Size = New System.Drawing.Size(121, 20)
        Me.txtbxFirstName.TabIndex = 2
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblLastName.Location = New System.Drawing.Point(318, 0)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 29)
        Me.lblLastName.TabIndex = 5
        Me.lblLastName.Text = "Last Name"
        Me.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCheckNames
        '
        Me.btnCheckNames.BackColor = System.Drawing.Color.LightGreen
        Me.btnCheckNames.Location = New System.Drawing.Point(113, 32)
        Me.btnCheckNames.Name = "btnCheckNames"
        Me.btnCheckNames.Size = New System.Drawing.Size(102, 23)
        Me.btnCheckNames.TabIndex = 7
        Me.btnCheckNames.Text = "Search finPower"
        Me.btnCheckNames.UseVisualStyleBackColor = False
        '
        'cmbxLastName
        '
        Me.cmbxLastName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxLastName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxLastName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactLastName", True))
        Me.cmbxLastName.DataSource = Me.DisplayAllNamesBindingSource
        Me.cmbxLastName.DisplayMember = "LastName"
        Me.cmbxLastName.FormattingEnabled = True
        Me.cmbxLastName.Location = New System.Drawing.Point(382, 3)
        Me.cmbxLastName.Name = "cmbxLastName"
        Me.cmbxLastName.Size = New System.Drawing.Size(150, 21)
        Me.cmbxLastName.TabIndex = 3
        Me.cmbxLastName.ValueMember = "LastName"
        '
        'DisplayAllNamesBindingSource
        '
        Me.DisplayAllNamesBindingSource.DataMember = "DisplayAllNames"
        Me.DisplayAllNamesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'btnCancelEnquiry
        '
        Me.btnCancelEnquiry.BackColor = System.Drawing.Color.Red
        Me.tlpName.SetColumnSpan(Me.btnCancelEnquiry, 2)
        Me.btnCancelEnquiry.ForeColor = System.Drawing.Color.White
        Me.btnCancelEnquiry.Location = New System.Drawing.Point(568, 32)
        Me.btnCancelEnquiry.Name = "btnCancelEnquiry"
        Me.btnCancelEnquiry.Size = New System.Drawing.Size(102, 23)
        Me.btnCancelEnquiry.TabIndex = 9
        Me.btnCancelEnquiry.TabStop = False
        Me.btnCancelEnquiry.Text = "Cancel Enquiry"
        Me.btnCancelEnquiry.UseVisualStyleBackColor = False
        Me.btnCancelEnquiry.Visible = False
        '
        'lblSalutation
        '
        Me.lblSalutation.AutoSize = True
        Me.tlpName.SetColumnSpan(Me.lblSalutation, 2)
        Me.lblSalutation.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblSalutation.Location = New System.Drawing.Point(629, 0)
        Me.lblSalutation.Name = "lblSalutation"
        Me.lblSalutation.Size = New System.Drawing.Size(72, 29)
        Me.lblSalutation.TabIndex = 257
        Me.lblSalutation.Text = "Display Name"
        Me.lblSalutation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAddComment
        '
        Me.btnAddComment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddComment.Location = New System.Drawing.Point(727, 227)
        Me.btnAddComment.Name = "btnAddComment"
        Me.btnAddComment.Size = New System.Drawing.Size(170, 23)
        Me.btnAddComment.TabIndex = 17
        Me.btnAddComment.Text = "Add Comment"
        Me.btnAddComment.UseVisualStyleBackColor = True
        '
        'txtbxPrelimComments
        '
        Me.txtbxPrelimComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanComments", True))
        Me.txtbxPrelimComments.Location = New System.Drawing.Point(6, 253)
        Me.txtbxPrelimComments.Multiline = True
        Me.txtbxPrelimComments.Name = "txtbxPrelimComments"
        Me.txtbxPrelimComments.ReadOnly = True
        Me.txtbxPrelimComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxPrelimComments.Size = New System.Drawing.Size(908, 554)
        Me.txtbxPrelimComments.TabIndex = 0
        Me.txtbxPrelimComments.TabStop = False
        '
        'lblComments
        '
        Me.lblComments.AutoSize = True
        Me.lblComments.Location = New System.Drawing.Point(25, 237)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(215, 13)
        Me.lblComments.TabIndex = 40
        Me.lblComments.Text = "Details and Comments on reason for Enquiry"
        '
        'PrelimReasonsBindingSource
        '
        Me.PrelimReasonsBindingSource.DataMember = "PrelimReasons"
        Me.PrelimReasonsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'ActiveEnquiryClientsBindingSource
        '
        Me.ActiveEnquiryClientsBindingSource.DataMember = "ActiveEnquiryClients"
        Me.ActiveEnquiryClientsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Me.NZTownsTableAdapter
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Me.PrelimReasonsTableAdapter
        Me.TableAdapterManager.PrelimResultsTableAdapter = Me.PrelimResultsTableAdapter
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Me.QrgListTableAdapter1
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Me.SuburbListTableAdapter
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'NZTownsTableAdapter
        '
        Me.NZTownsTableAdapter.ClearBeforeFill = True
        '
        'PrelimReasonsTableAdapter
        '
        Me.PrelimReasonsTableAdapter.ClearBeforeFill = True
        '
        'PrelimResultsTableAdapter
        '
        Me.PrelimResultsTableAdapter.ClearBeforeFill = True
        '
        'QrgListTableAdapter1
        '
        Me.QrgListTableAdapter1.ClearBeforeFill = True
        '
        'SuburbListTableAdapter
        '
        Me.SuburbListTableAdapter.ClearBeforeFill = True
        '
        'btnWebApp
        '
        Me.btnWebApp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWebApp.Location = New System.Drawing.Point(799, 12)
        Me.btnWebApp.Name = "btnWebApp"
        Me.btnWebApp.Size = New System.Drawing.Size(110, 23)
        Me.btnWebApp.TabIndex = 22
        Me.btnWebApp.Text = "Web Application"
        Me.btnWebApp.UseVisualStyleBackColor = True
        '
        'btnWorksheetWizard
        '
        Me.btnWorksheetWizard.Location = New System.Drawing.Point(374, 12)
        Me.btnWorksheetWizard.Name = "btnWorksheetWizard"
        Me.btnWorksheetWizard.Size = New System.Drawing.Size(196, 23)
        Me.btnWorksheetWizard.TabIndex = 21
        Me.btnWorksheetWizard.Text = "Due Diligence Worksheet Wizard"
        Me.btnWorksheetWizard.UseVisualStyleBackColor = True
        '
        'ActiveDealersBindingSource
        '
        Me.ActiveDealersBindingSource.DataMember = "ActiveDealers"
        Me.ActiveDealersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'ActiveDealersTableAdapter
        '
        Me.ActiveDealersTableAdapter.ClearBeforeFill = True
        '
        'PrelimSourceTableAdapter
        '
        Me.PrelimSourceTableAdapter.ClearBeforeFill = True
        '
        'PrelimSourceBindingSource1
        '
        Me.PrelimSourceBindingSource1.DataMember = "PrelimSource"
        Me.PrelimSourceBindingSource1.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryMethodListTableAdapter
        '
        Me.EnquiryMethodListTableAdapter.ClearBeforeFill = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 990)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStripMessage.Size = New System.Drawing.Size(944, 22)
        Me.StatusStripMessage.TabIndex = 26
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(42, 17)
        Me.ToolStripStatusLabel1.Text = "Ready."
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnWorksheetWizard)
        Me.Panel1.Controls.Add(Me.btnSaveAndExit)
        Me.Panel1.Controls.Add(Me.btnWebApp)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 941)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 49)
        Me.Panel1.TabIndex = 27
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblDealerCode)
        Me.Panel2.Controls.Add(Me.cmbxDealerId)
        Me.Panel2.Controls.Add(Me.lblDealer)
        Me.Panel2.Controls.Add(Me.cmbxMethodEnquiry)
        Me.Panel2.Controls.Add(Me.lblMethodEnquiry)
        Me.Panel2.Controls.Add(Me.lblDateAndTimeValue)
        Me.Panel2.Controls.Add(Me.lblDateAndTime)
        Me.Panel2.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Panel2.Controls.Add(Me.lblEnquiryNumber)
        Me.Panel2.Controls.Add(Me.lblTitle)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(944, 86)
        Me.Panel2.TabIndex = 1
        '
        'lblDealerCode
        '
        Me.lblDealerCode.AutoSize = True
        Me.lblDealerCode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ActiveDealersBindingSource, "DealerId", True))
        Me.lblDealerCode.Location = New System.Drawing.Point(636, 59)
        Me.lblDealerCode.Name = "lblDealerCode"
        Me.lblDealerCode.Size = New System.Drawing.Size(39, 13)
        Me.lblDealerCode.TabIndex = 35
        Me.lblDealerCode.Text = "Label1"
        Me.lblDealerCode.Visible = False
        '
        'cmbxDealerId
        '
        Me.cmbxDealerId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxDealerId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxDealerId.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "DealerId", True))
        Me.cmbxDealerId.DataSource = Me.ActiveDealersBindingSource
        Me.cmbxDealerId.DisplayMember = "Name"
        Me.cmbxDealerId.FormattingEnabled = True
        Me.cmbxDealerId.Location = New System.Drawing.Point(363, 56)
        Me.cmbxDealerId.Name = "cmbxDealerId"
        Me.cmbxDealerId.Size = New System.Drawing.Size(263, 21)
        Me.cmbxDealerId.TabIndex = 2
        Me.cmbxDealerId.ValueMember = "DealerId"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(319, 59)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(38, 13)
        Me.lblDealer.TabIndex = 34
        Me.lblDealer.Text = "Dealer"
        '
        'cmbxMethodEnquiry
        '
        Me.cmbxMethodEnquiry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxMethodEnquiry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxMethodEnquiry.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "PrelimMethod", True))
        Me.cmbxMethodEnquiry.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "PrelimMethod", True))
        Me.cmbxMethodEnquiry.DataSource = Me.EnquiryMethodListBindingSource
        Me.cmbxMethodEnquiry.DisplayMember = "EnquiryMethod"
        Me.cmbxMethodEnquiry.FormattingEnabled = True
        Me.cmbxMethodEnquiry.Location = New System.Drawing.Point(117, 56)
        Me.cmbxMethodEnquiry.Name = "cmbxMethodEnquiry"
        Me.cmbxMethodEnquiry.Size = New System.Drawing.Size(121, 21)
        Me.cmbxMethodEnquiry.TabIndex = 1
        Me.cmbxMethodEnquiry.ValueMember = "EnquiryMethod"
        '
        'lblMethodEnquiry
        '
        Me.lblMethodEnquiry.AutoSize = True
        Me.lblMethodEnquiry.Location = New System.Drawing.Point(18, 59)
        Me.lblMethodEnquiry.Name = "lblMethodEnquiry"
        Me.lblMethodEnquiry.Size = New System.Drawing.Size(93, 13)
        Me.lblMethodEnquiry.TabIndex = 33
        Me.lblMethodEnquiry.Text = "Method of Enquiry"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(402, 36)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 32
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(319, 36)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(80, 13)
        Me.lblDateAndTime.TabIndex = 31
        Me.lblDateAndTime.Text = "Date and Time:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(106, 36)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 30
        Me.lblEnquiryNumberValue.Text = "0000"
        '
        'lblEnquiryNumber
        '
        Me.lblEnquiryNumber.AutoSize = True
        Me.lblEnquiryNumber.Location = New System.Drawing.Point(18, 36)
        Me.lblEnquiryNumber.Name = "lblEnquiryNumber"
        Me.lblEnquiryNumber.Size = New System.Drawing.Size(85, 13)
        Me.lblEnquiryNumber.TabIndex = 29
        Me.lblEnquiryNumber.Text = "Enquiry Number:"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(376, 5)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(193, 20)
        Me.lblTitle.TabIndex = 28
        Me.lblTitle.Text = "Preliminary Information"
        '
        'ActiveEnquiryClientsTableAdapter
        '
        Me.ActiveEnquiryClientsTableAdapter.ClearBeforeFill = True
        '
        'QRGListBindingSource
        '
        Me.QRGListBindingSource.DataMember = "QRGList"
        Me.QRGListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DisplayAllNamesTableAdapter
        '
        Me.DisplayAllNamesTableAdapter.ClearBeforeFill = True
        '
        'LoanPurposeTableAdapter
        '
        Me.LoanPurposeTableAdapter.ClearBeforeFill = True
        '
        'PreliminaryForm
        '
        Me.AcceptButton = Me.btnSaveAndExit
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.PrelimFormSize
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.gpbxQuestions)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "PrelimFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "PrelimFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.PrelimFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "PreliminaryForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Preliminary Enquiry Information"
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryMethodListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxQuestions.ResumeLayout(False)
        Me.gpbxQuestions.PerformLayout()
        Me.tlpResult.ResumeLayout(False)
        Me.tlpResult.PerformLayout()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpQuestions.ResumeLayout(False)
        Me.tlpQuestions.PerformLayout()
        CType(Me.SuburbListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NZTownsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimSourceBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoanPurposeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpName.ResumeLayout(False)
        Me.tlpName.PerformLayout()
        CType(Me.pbSearchEnquires, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DisplayAllNamesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActiveEnquiryClientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActiveDealersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimSourceBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.QRGListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents gpbxQuestions As System.Windows.Forms.GroupBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cmbxPrelimResults As System.Windows.Forms.ComboBox
    Friend WithEvents txtbxPrelimComments As System.Windows.Forms.TextBox
    Friend WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents txtbxAmount As System.Windows.Forms.TextBox
    Friend WithEvents lblBorrow As System.Windows.Forms.Label
    Friend WithEvents cmbxEnquiryType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cmbxTown As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxPrelimSource As System.Windows.Forms.ComboBox
    Friend WithEvents lblPrelimSource As System.Windows.Forms.Label
    Friend WithEvents lblPrelimArea As System.Windows.Forms.Label
    Friend WithEvents txtbxPrelimContact As System.Windows.Forms.TextBox
    Friend WithEvents lblPrelimContact As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents cmbxTitle As System.Windows.Forms.ComboBox
    Friend WithEvents txtbxFirstName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents PrelimReasonsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimReasonsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter
    Friend WithEvents PrelimResultsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimResultsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter
    Friend WithEvents btnCheckNames As System.Windows.Forms.Button
    Friend WithEvents btnWebApp As System.Windows.Forms.Button
    Friend WithEvents btnWorksheetWizard As System.Windows.Forms.Button
    Friend WithEvents lblSuburb As System.Windows.Forms.Label
    Friend WithEvents lblTown As System.Windows.Forms.Label
    Friend WithEvents NZTownsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NZTownsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.NZTownsTableAdapter
    Friend WithEvents SuburbListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SuburbListTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SuburbListTableAdapter
    Friend WithEvents ActiveDealersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActiveDealersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveDealersTableAdapter
    Friend WithEvents PrelimSourceBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimSourceTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimSourceTableAdapter
    Friend WithEvents PrelimSourceBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryMethodListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryMethodListTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryMethodListTableAdapter
    Friend WithEvents btnAddComment As System.Windows.Forms.Button
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmbxDealerId As System.Windows.Forms.ComboBox
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents cmbxMethodEnquiry As System.Windows.Forms.ComboBox
    Friend WithEvents lblMethodEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumber As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents cmbxSuburb As System.Windows.Forms.ComboBox
    Friend WithEvents btnCheckEnquiries As System.Windows.Forms.Button
    Friend WithEvents tlpName As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpQuestions As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpResult As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblDealerCode As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmbxLastName As System.Windows.Forms.ComboBox
    Friend WithEvents ActiveEnquiryClientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActiveEnquiryClientsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveEnquiryClientsTableAdapter
    Friend WithEvents btnCancelEnquiry As System.Windows.Forms.Button
    Friend WithEvents QrgListTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.QRGListTableAdapter
    Friend WithEvents QRGListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DisplayAllNamesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DisplayAllNamesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DisplayAllNamesTableAdapter
    Friend WithEvents LoanPurposeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LoanPurposeTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanPurposeTableAdapter
    Friend WithEvents cmbxLoanPurpose As System.Windows.Forms.ComboBox
    Friend WithEvents lblPurpose As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtContactEmail As System.Windows.Forms.TextBox
    Friend WithEvents pbSearchEnquires As System.Windows.Forms.PictureBox
    Friend WithEvents txtbxSalutation As System.Windows.Forms.TextBox
    Friend WithEvents lblSalutation As System.Windows.Forms.Label
End Class
