﻿
Imports System.Data.SqlClient

Public Class frmLatencyPoints

    Private connectString As String
    Private connection As SqlConnection
    Dim ds As DataSet
    Dim dataAdapter1 As SqlDataAdapter
    Public EnquiryCode As String
    Public EnquiryId As Integer
    Public EnquiryType As Integer
    Public ApplicationCode As String
    Public NoofClients As Integer = 0
    Dim htClientLatencyPoints As New Hashtable
    'New global objects
    Private finPowerOpObj As finPowerOps
    Private xmlClientInfo As finPowerOps.ClientInfo

    'used in setGridProperties to set column number
    Const CONST_GRIDCOLUMN_NO_CheckBox As Integer = 0
    Const CONST_GRIDCOLUMN_NO_LatencyPointId As Integer = 1
    Const CONST_GRIDCOLUMN_NO_LatencyPointType As Integer = 2
    Const CONST_GRIDCOLUMN_NO_Subject As Integer = 3
    Const CONST_GRIDCOLUMN_NO_Notes As Integer = 4
    Const CONST_GRIDCOLUMN_NO_ActionDateDefaultSetting As Integer = 5
    Const CONST_GRIDCOLUMN_NO_ActionDate As Integer = 6

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(enquiryId As Integer, enquiryCode As String, applicationCode As String, enquiryType As Integer)
        ' This call is required by the designer.
        InitializeComponent()
        Me.EnquiryCode = enquiryCode
        Me.EnquiryId = enquiryId
        Me.EnquiryType = EnquiryType
        Me.ApplicationCode = applicationCode
        finPowerOpObj = New finPowerOps(enquiryCode, applicationCode)
    End Sub

    Public Sub SetFormCaption(ByVal ClientSalutation As String)
        Me.Text = ClientSalutation & " | Setting Latency & Retention Points"
    End Sub


    ''' <summary>
    ''' Sets the column Width and adds Action date column for dgv
    ''' </summary>
    ''' <param name="dgvLatencyPoints"></param>
    ''' <remarks></remarks>
    Private Sub setGridProperties(ByVal dgvLatencyPoints As DataGridView)

        Dim col As New CalendarColumn()
        col.Name = "InputActionDate"
        dgvLatencyPoints.Columns.Add(col)
        dgvLatencyPoints.RowHeadersWidth = 1 * ((dgvLatencyPoints.Width - 2) / 20)
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_CheckBox).Width = 1 * ((dgvLatencyPoints.Width - 2) / 20) 'Checkbox
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_LatencyPointId).Visible = False 'Latency point Id
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_LatencyPointType).ReadOnly = True 'Loan or Client(Main only) Latency point
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_LatencyPointType).Width = 2 * ((dgvLatencyPoints.Width - 2) / 20)
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_Subject).ReadOnly = True 'Log Subject
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_Subject).Width = 5 * ((dgvLatencyPoints.Width - 2) / 20)
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_Notes).Width = 8 * ((dgvLatencyPoints.Width - 2) / 20) 'Notes
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_ActionDateDefaultSetting).Visible = False
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_ActionDate).Width = 3 * ((dgvLatencyPoints.Width - 2) / 20)
        dgvLatencyPoints.Columns(CONST_GRIDCOLUMN_NO_ActionDate).HeaderText = "Action date"

    End Sub

    Private Sub frmLatencyPoints_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        setFormLayout()
    End Sub

    'Adjusts the form controls according to form size
    Private Function setFormLayout() As Boolean

        ''Maximum Size 960, 1050
        ''Minimum Size 720, 275

        'Dim formWidth As Integer = Me.Width
        'Dim formHeight As Integer = Me.Height


        ''get minimumSize
        'Dim minFormHeight As Integer = Me.MinimumSize.Height - 1
        'Dim minFormWidth As Integer = Me.MinimumSize.Width - 1

        ''If NoofClients = 1 Then
        ''    minFormWidth = 727 : minFormHeight = 323
        ''Else
        ''    minFormWidth = 727 : minFormHeight = 573
        ''End If
        ''MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)

        ''resize
        'If (formHeight > minFormHeight) Then
        '    'resize DataGridView
        '    If NoofClients = 1 Then
        '        'Me.grpClient1.Size = New Size(formWidth - 16, formHeight - 127)
        '        'Me.dgvLatencyPoints1.Size = New Size(formWidth - 25, formHeight - 143)
        '        Me.grpClient1.Size = New Size(Me.Width - 16, (Me.Height) - 90)
        '        Me.dgvLatencyPoints1.Size = New Size(Me.Width - 25, Me.Height - 93)
        '    Else
        '        Me.grpClient1.Size = New Size(formWidth - 16, (formHeight / 2) - 70)
        '        'Me.dgvLatencyPoints1.Size = New Size(formWidth - 20, (formHeight / 2) - 70)
        '        Me.dgvLatencyPoints1.Size = New Size(formWidth - 30, (formHeight / 2) - 90)
        '    End If

        '    Me.dgvLatencyPoints1.RowHeadersWidth = 1 * ((Me.dgvLatencyPoints1.Width - 2) / 20)
        '    Me.dgvLatencyPoints1.Columns(0).Width = 1 * ((Me.dgvLatencyPoints1.Width - 2) / 20)
        '    Me.dgvLatencyPoints1.Columns(CONST_GRIDCOLUMN_NO_LatencyPointType).Width = 2 * ((Me.dgvLatencyPoints1.Width - 2) / 20)
        '    Me.dgvLatencyPoints1.Columns(3).Width = 5 * ((Me.dgvLatencyPoints1.Width - 2) / 20)
        '    Me.dgvLatencyPoints1.Columns(4).Width = 8 * ((Me.dgvLatencyPoints1.Width - 2) / 20)
        '    Me.dgvLatencyPoints1.Columns(CONST_GRIDCOLUMN_NO_ActionDate).Width = 3 * ((Me.dgvLatencyPoints1.Width - 2) / 20)

        '    If NoofClients > 1 Then
        '        Me.grpClient2.Top = (formHeight / 2) - 30
        '        Me.grpClient2.Size = New Size(formWidth - 16, (formHeight / 2) - 70)
        '        'Me.dgvLatencyPoints2.Size = New Size(formWidth - 20, (formHeight / 2) - 70)
        '        Me.dgvLatencyPoints2.Size = New Size(formWidth - 30, (formHeight / 2) - 90)
        '        Me.dgvLatencyPoints2.RowHeadersWidth = 1 * ((Me.dgvLatencyPoints2.Width - 2) / 20)
        '        Me.dgvLatencyPoints2.Columns(0).Width = 1 * ((Me.dgvLatencyPoints2.Width - 2) / 20)
        '        Me.dgvLatencyPoints2.Columns(CONST_GRIDCOLUMN_NO_LatencyPointType).Width = 2 * ((Me.dgvLatencyPoints2.Width - 2) / 20)
        '        Me.dgvLatencyPoints2.Columns(3).Width = 5 * ((Me.dgvLatencyPoints2.Width - 2) / 20)
        '        Me.dgvLatencyPoints2.Columns(4).Width = 8 * ((Me.dgvLatencyPoints2.Width - 2) / 20)
        '        Me.dgvLatencyPoints2.Columns(CONST_GRIDCOLUMN_NO_ActionDate).Width = 3 * ((Me.dgvLatencyPoints2.Width - 2) / 20)
        '    End If

        'End If

    End Function


    Private Sub frmLatencyPoints_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadProcess()

    End Sub

    Public Sub LoadProcess()
        'XML: make frmLatencyPoints compatable with multiple clients
        Me.SuspendLayout()

        Dim row As DataGridViewRow
        Dim htExistingLatencyPoints As Hashtable
        'New
        Dim i As Integer
        Dim locH As Integer = 0 'keep track of current location Height
        Dim dgvH As Integer = 0
        Dim clientType As String

        NoofClients = PopulateXmlClient() 'does not include loan, just clients

        'XML: Create a groupbox for each client in xmlClientInfo
        'add customer.applicantname to groupbox 
        'create a dgv inside each groupbox
        'set datasource with GetDgvData(ClientNum) (Returns a datatable)
        'UPDATE: Peter says only do Latency Points for Loan and Main Customer.
        'Ajay wants also for co-mains, NOT required for guarantors

        For i = 0 To NoofClients
            'Filter out guarantors
            'Add test for nothing as Financial Facility client has no Type (main, comain, guarantor)
            If i > 0 Then
                If xmlClientInfo.Clients(i).Type Is Nothing Then
                    clientType = ""
                Else
                    clientType = xmlClientInfo.Clients(i).Type.ToLower
                End If
            End If

            If (i > 0 AndAlso Not clientType = MyEnums.GetDescription(DirectCast(2, MyEnums.CustomerEnquiryType)).ToLower) Or i = 0 Then
                'i = 0 refers to Loan latency Points
                '===================
                'Create groupbox
                '===================
                Dim gb As GroupBox
                If i = 0 And Not EnquiryType = MyEnums.EnquiryType.FinanceFacility Then 'For loan latency points '17/02/2017 added filter for FinanceFacility( no loan)
                    gb = New GroupBox
                    gb.Name = "gb" & i.ToString
                    gb.Text = "Loan"
                    locH += 12 'start of header
                    gb.Location = New Point(4, locH)

                ElseIf i > 0 Then 'For clients
                    gb = New GroupBox
                    gb.Name = "gb" & i.ToString
                    gb.Text = xmlClientInfo.Clients(i).ApplicantName
                    locH += 6 'header
                    gb.Location = New Point(4, locH)
                End If

                '===================
                'Create DataGridView
                '===================
                If i = 0 And EnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    'no loan
                Else
                    Dim dgvBindingSource As New BindingSource
                    dgvBindingSource.DataSource = GetDgvData(i)
                    Dim dgv As New DataGridView
                    dgv.Name = "dgv" & i.ToString
                    'Add select column
                    Dim col As New DataGridViewCheckBoxColumn
                    col.Name = "chkSelect"
                    col.HeaderText = " * "
                    dgv.Columns.Add(col)
                    dgv.AutoGenerateColumns = True
                    dgv.AllowUserToAddRows = False
                    dgv.DataSource = dgvBindingSource
                    locH += 18 '6
                    dgv.Location = New Point(15, locH)
                    dgvH = dgv.DataSource.Count * 22 + 25
                    dgv.Size = New Size(700, dgvH)
                    locH += dgvH
                    gb.Size = New Size(724, dgvH + 30)

                    locH += 6
                    dgv.AlternatingRowsDefaultCellStyle.BackColor = SystemColors.Control
                    dgv.SelectionMode = DataGridViewSelectionMode.CellSelect
                    'add to controls to form
                    Me.Controls.Add(dgv)
                    Me.Controls.Add(gb)

                    setGridProperties(dgv)
                    'Validate the data in the grid
                    For Each row In dgv.Rows
                        'Set row style
                        row.Cells(CONST_GRIDCOLUMN_NO_LatencyPointId).Style.ForeColor = Color.SlateGray
                        row.Cells(3).Style.ForeColor = Color.SlateGray
                        'Checks if row has a valid default ActionDate set, then assign this to ActionDate
                        If IsDate(row.Cells(5).Value) Then 'Columns(ConstGridcolumnNOActionDateDefaultSetting) = 5
                            row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value() = row.Cells(5).Value
                        End If
                    Next

                End If



            End If


        Next 'end of NoofClients

        Me.Size = New Size(750, locH + 18 + 64)

        'Get existing Latency points
        'contains existing Loan LatencyPoints in htClientLatencyPoints at key '0'. Key '1' and above are clients
        'contains all log notes even if they are not actually LatencyPoints
        htExistingLatencyPoints = ReadLogsFromXML()

        'check user wants to keep existing latency points
        If Not htExistingLatencyPoints Is Nothing Then
            If checkExistingLogsInClient(htExistingLatencyPoints) Then
                Dim result As System.Windows.Forms.DialogResult
                result = MessageBox.Show("Latency points exists in the data file" & vbCrLf & "Do you wish to redo?", "", MessageBoxButtons.YesNo)

                If result = System.Windows.Forms.DialogResult.No Then
                    DialogResult = System.Windows.Forms.DialogResult.OK
                Else
                    'Continue
                End If
            End If
        End If

        Me.ResumeLayout()

        ''Set focus
        'dgvLatencyPoints1.Rows(0).Cells(0).Selected = True
    End Sub



    ''fetches the data from database and binds to grids
    'Private Sub getFormData()
    '    Dim ds2 As DataSet
    '    Dim sCommand As SqlClient.SqlCommand
    '    'ActionDate
    '    'Select LatencyPointsID,LogType,Subject,Notes From LatencyPoints
    '    Try

    '        Using connection As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)

    '            dataAdapter1 = New SqlDataAdapter(New SqlCommand("Select LatencyPointsID,LogType,Subject,Notes, ActionDate From LatencyPoints Order by SortNum", connection))
    '            ds = New DataSet
    '            dataAdapter1.Fill(ds)

    '            Me.dgvLatencyPoints1.DataSource = ds.Tables(0)

    '            If NoofClients > 1 Then
    '                sCommand = New SqlCommand("Select LatencyPointsID,LogType,Subject,Notes, ActionDate From LatencyPoints where LogType=@LogType Order by SortNum")

    '                sCommand.Connection = connection
    '                sCommand.Parameters.AddWithValue("@LogType ", Constants.CONST_LATENCY_POINTS_LOG_TYPE_CLIENT)
    '                dataAdapter1 = New SqlDataAdapter(sCommand)

    '                ds2 = New DataSet
    '                dataAdapter1.Fill(ds2)
    '                Me.dgvLatencyPoints2.DataSource = ds2.Tables(0)

    '            End If

    '        End Using
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    Finally
    '    End Try

    'End Sub

    ''' <summary>
    ''' Get the LatencyPoints(empty) from database. ClientNum = '0' returns Loan Latency points. ClientNum > '0' returns just Client Latency points
    ''' </summary>
    ''' <returns>DataTable</returns>
    ''' <remarks>ClientNum = '0' returns Loan Latency points. ClientNum > '0' returns just Client Latency points</remarks>
    Private Function GetDgvData(clientNum As Integer) As DataTable
        'create a datatable and load it with Latency points columns
        Dim ds As DataSet
        Dim da As SqlDataAdapter
        Dim dt As New DataTable
        Dim connection As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)
        Dim sCommand As SqlClient.SqlCommand

        Try
            Using connection

                If clientNum = 0 Then
                    sCommand = New SqlCommand("Select LatencyPointsID,LogType,Subject,Notes, ActionDate From LatencyPoints where LogType=@LogType Order by SortNum")
                    sCommand.Connection = connection
                    sCommand.Parameters.AddWithValue("@LogType ", Constants.CONST_LATENCY_POINTS_LOG_TYPE_LOAN)
                    da = New SqlDataAdapter(sCommand)
                    ds = New DataSet
                    da.Fill(ds)
                    dt = ds.Tables(0)
                ElseIf clientNum > 0 Then
                    sCommand = New SqlCommand("Select LatencyPointsID,LogType,Subject,Notes, ActionDate From LatencyPoints where LogType=@LogType Order by SortNum")
                    sCommand.Connection = connection
                    sCommand.Parameters.AddWithValue("@LogType ", Constants.CONST_LATENCY_POINTS_LOG_TYPE_CLIENT)
                    da = New SqlDataAdapter(sCommand)
                    ds = New DataSet
                    da.Fill(ds)
                    dt = ds.Tables(0)
                End If

            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        Finally
        End Try

        Return dt

    End Function

    'Modified date gets added to XML file
    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        'XML: modify to use with multiple XMLCustomers
        'make some variables global or reuse existing globals
        Dim i As Integer
        Dim result As System.Windows.Forms.DialogResult
        Dim row As DataGridViewRow
        Dim dgvGrid As DataGridView = Nothing
        Dim alLatencyPointsDefaultList As New ArrayList
        Dim isContinueselected As Boolean = False
        Dim clientType As String
        'Validate the data in the grid
        For i = 0 To NoofClients
            'Filter out guarantors
            'Add test for nothing as Financial Facility client has no Type (main, comain, guarantor)
            If i > 0 Then
                If xmlClientInfo.Clients(i).Type Is Nothing Then
                    clientType = ""
                Else
                    clientType = xmlClientInfo.Clients(i).Type.ToLower
                End If
            End If
            If (i > 0 AndAlso Not clientType = MyEnums.GetDescription(DirectCast(2, MyEnums.CustomerEnquiryType)).ToLower) Or i = 0 Then
                dgvGrid = CType(Me.Controls("dgv" & i.ToString), DataGridView)
                If dgvGrid IsNot Nothing Then
                    For Each row In dgvGrid.Rows
                        'Checks if all rows checked are having an action date value entered
                        If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = True And Not IsDate(row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value) Then
                            MessageBox.Show("Please enter value for all latency points checked in the list")
                            Exit Sub
                        End If
                        If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = False And IsDate(row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value) Then
                            result = MessageBox.Show("Not all latency points dated in the list have been selected" & vbCrLf & "Do you wish to continue?", "", MessageBoxButtons.YesNo)

                            If result = System.Windows.Forms.DialogResult.No Then
                                Exit Sub
                            Else
                                'Continue
                                isContinueselected = True
                            End If

                        End If
                        If isContinueselected = True Then
                            Exit For
                        End If
                    Next
                End If
               
                If isContinueselected = True Then
                    Exit For
                End If

            End If

        Next
        buildDataForUpdate() 'populate the global htClientLatencyPoints(Hashtable)

        alLatencyPointsDefaultList = buildLatencyPointsDefaultList()


        If finPowerOpObj.UpdateLatencyPointsIntoXMLFile(htClientLatencyPoints, alLatencyPointsDefaultList) Then
            DialogResult = System.Windows.Forms.DialogResult.OK
        Else
            DialogResult = System.Windows.Forms.DialogResult.None
        End If

    End Sub

    ''' <summary>
    ''' Build a list of the default Latency Points (loan and client)
    ''' </summary>
    ''' <returns>List of Latency points</returns>
    ''' <remarks></remarks>
    Public Function buildLatencyPointsDefaultList() As ArrayList
        Dim LatencyPointsDTODefaultObj As finPowerOps.LatencyPoint
        'For removing any log already exists in XML 
        'Used to compare with logs in XML, if a match then can remove XML log as log is a LatencyPoint
        'This is used prior to creating new LatencyPoints as Logs in XML
        Dim alLatencyPointsDefaultList As New ArrayList 'Contains Complete List of Latency points
        'Create the list from database LatencyPoints table
        'create a datatable and load it with Latency points columns
        Dim ds As DataSet
        Dim da As SqlDataAdapter
        Dim dt As New DataTable
        Dim connection As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)
        Dim sCommand As SqlClient.SqlCommand
        Dim i As Integer = 0


        sCommand = New SqlCommand("Select LatencyPointsID,LogType,Subject,Notes, ActionDate From LatencyPoints Order by SortNum")
        sCommand.Connection = connection
        da = New SqlDataAdapter(sCommand)
        ds = New DataSet
        da.Fill(ds)
        dt = ds.Tables(0)

        For Each row As DataRow In dt.Rows
            LatencyPointsDTODefaultObj = New finPowerOps.LatencyPoint
            LatencyPointsDTODefaultObj.LatencyPointsId = If(row.Item("LatencyPointsID") Is DBNull.Value, 0, row.Item("LatencyPointsID"))
            LatencyPointsDTODefaultObj.LogType = If(row.Item("LogType") Is DBNull.Value, "", row.Item("LogType"))
            LatencyPointsDTODefaultObj.Subject = If(row.Item("Subject") Is DBNull.Value, "", row.Item("Subject"))
            LatencyPointsDTODefaultObj.Notes = If(row.Item("Notes") Is DBNull.Value, "", row.Item("Notes"))
            LatencyPointsDTODefaultObj.ActionDate = If(row.Item("ActionDate") Is DBNull.Value, "", row.Item("ActionDate"))
            alLatencyPointsDefaultList.Add(LatencyPointsDTODefaultObj)
        Next

        Return alLatencyPointsDefaultList

        

    End Function

    ''' <summary>
    ''' Builds the hashtable htClientLatencyPoints with all latency points from all the dgvs
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks>for update into database</remarks>
    Function buildDataForUpdate() As Boolean
        Dim i As Integer
        Dim alLatencyPoints As ArrayList
        Dim row As DataGridViewRow
        Dim ActionDate As DateTime
        Dim LatencyPointsDTOObj As finPowerOps.LatencyPoint
        Dim dgvLatencyPoints As DataGridView
        Dim clientType As String

        'iterate through dgvs
        For i = 0 To NoofClients
            alLatencyPoints = New ArrayList
            'Filter out guarantors
            If i > 0 Then
                If xmlClientInfo.Clients(i).Type Is Nothing Then
                    clientType = ""
                Else
                    clientType = xmlClientInfo.Clients(i).Type.ToLower
                End If
            End If
            If (i > 0 AndAlso Not clientType = MyEnums.GetDescription(DirectCast(2, MyEnums.CustomerEnquiryType)).ToLower) Or i = 0 Then
                'i = 0 refers to Loan latency Points
                dgvLatencyPoints = CType(Me.Controls("dgv" & i.ToString), DataGridView)
                If dgvLatencyPoints IsNot Nothing Then
                    For Each row In dgvLatencyPoints.Rows
                        LatencyPointsDTOObj = New finPowerOps.LatencyPoint
                        'Checks if all rows checked are having an action date value entered
                        If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = True And IsDate(row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value) Then
                            LatencyPointsDTOObj.LatencyPointsId = row.Cells(CONST_GRIDCOLUMN_NO_LatencyPointId).Value
                            LatencyPointsDTOObj.LogType = row.Cells(CONST_GRIDCOLUMN_NO_LatencyPointType).Value
                            LatencyPointsDTOObj.Subject = row.Cells(CONST_GRIDCOLUMN_NO_Subject).Value & ""
                            LatencyPointsDTOObj.Notes = row.Cells(CONST_GRIDCOLUMN_NO_Notes).Value & ""
                            ActionDate = row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value

                            LatencyPointsDTOObj.ActionDate = ActionDate.ToString("yyyy-MM-dd")

                            alLatencyPoints.Add(LatencyPointsDTOObj)

                        End If

                    Next

                    htClientLatencyPoints.Add(i, alLatencyPoints)
                End If

            End If
        Next

        Return True




        ''Dim kvp As KeyValuePair(Of String, String)
        ''kvp = CType(lstClientName.SelectedItem, KeyValuePair(Of String, String))
        'For i = 1 To 2
        '    alLatencyPoints = New ArrayList
        '    If i = 1 Then
        '        dgvLatencyPoints = dgvLatencyPoints1
        '    ElseIf i = 2 Then
        '        dgvLatencyPoints = dgvLatencyPoints2
        '    End If
        '    For Each row In dgvLatencyPoints.Rows
        '        LatencyPointsDTOObj = New LatencyPoints

        '        'Checks if all rows checked are having an action date value entered
        '        If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = True And IsDate(row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value) Then
        '            LatencyPointsDTOObj.LatencyPointsId = row.Cells(CONST_GRIDCOLUMN_NO_LatencyPointId).Value
        '            LatencyPointsDTOObj.LogType = row.Cells(CONST_GRIDCOLUMN_NO_LatencyPointType).Value
        '            LatencyPointsDTOObj.Subject = row.Cells(CONST_GRIDCOLUMN_NO_Subject).Value & ""
        '            LatencyPointsDTOObj.Notes = row.Cells(CONST_GRIDCOLUMN_NO_Notes).Value & ""
        '            ActionDate = row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value

        '            LatencyPointsDTOObj.ActionDate = ActionDate.ToString("yyyy-MM-dd")

        '            alLatencyPoints.Add(LatencyPointsDTOObj)

        '        End If

        '    Next

        '    htClientLatencyPoints.Add(i, alLatencyPoints)

        'Next

        'Return True

    End Function

    'XML: dgvLatencyPoints_CellEndEdit1
    'Private Sub dgvLatencyPoints_CellEndEdit1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    dgvLatencyPoints1.Rows(e.RowIndex).ErrorText = String.Empty
    'End Sub

    'XML: Sub dgvLatencyPoints_DataError1
    'Private Sub dgvLatencyPoints_DataError1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)
    '    Dim SortNumAsDate As DateTime


    '    Select Case e.ColumnIndex

    '        Case 5
    '            If Date.TryParse(dgvLatencyPoints1.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue, SortNumAsDate) Then
    '                ' successfully parsed as Integer
    '                Me.dgvLatencyPoints1.Rows(e.RowIndex).ErrorText = ""
    '            Else
    '                Me.dgvLatencyPoints1.Rows(e.RowIndex).ErrorText = MessageConstants.CONST_XML_LOANLOG_ACTIONDATE_OFTYPEDATE
    '                MessageBox.Show(MessageConstants.CONST_XML_LOANLOG_ACTIONDATE_OFTYPEDATE)
    '                e.Cancel = True
    '            End If
    '    End Select
    'End Sub


    ''' <summary>
    ''' Checks if all rows checked are having an action date value entered
    ''' </summary>
    ''' <param name="dgvLatencyPoints"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateGridData(ByVal dgvLatencyPoints As DataGridView) As Boolean

        Dim Result As System.Windows.Forms.DialogResult
        Dim row As DataGridViewRow

        'Validate the data in the grid
        For Each row In dgvLatencyPoints.Rows
            'Checks if all rows checked are having an action date value entered

            If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = True And Not IsDate(row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value) Then
                MessageBox.Show("Please enter value for all latency points checked in the list")
                Return False
            End If
            If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = False And IsDate(row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value) Then
                Result = MessageBox.Show("Not all latency points dated in the list have been selected" & vbCrLf & "Do you wish to continue?", "", MessageBoxButtons.YesNo)

                If Result = System.Windows.Forms.DialogResult.No Then
                    Return False
                Else
                    'Continue
                End If
            End If
        Next

        Return True

    End Function


    ''' <summary>
    ''' populates XML clients only into xmlClientInfo (key starts at 1)
    ''' </summary>
    ''' <returns>xmlClientInfo.Clients.Count</returns>
    ''' <remarks></remarks>
    Public Function PopulateXmlClient() As Integer

        xmlClientInfo = finPowerOpObj.GetXmlClientInfo()
        'Display name is already added to ApplicantName for all types of clients

        If xmlClientInfo IsNot Nothing Then
            If Not String.IsNullOrWhiteSpace(xmlClientInfo.ErrorMessage) Then
                MessageBox.Show(xmlClientInfo.ErrorMessage, "Xml parsing error", MessageBoxButtons.OK)
            End If
            If xmlClientInfo.Status = True Then

                ''htXML = clientInfo.htClient
                'For i = 1 To xmlClientInfo.Clients.Count
                '    If xmlClientInfo.Clients.ContainsKey(i) Then
                '        clientfinPow = CType(xmlClientInfo.Clients.Item(i), ClientMappingDetails)
                '        If clientfinPow IsNot Nothing Then
                '            'If clientfinPow.Comment IsNot Nothing Then
                '            '    If Not clientfinPow.Comment.ToUpper = "AKA" Then
                '            dispName = (clientfinPow.FirstName & " " & clientfinPow.LastName).Trim
                '        End If
                '        If i = 1 Then
                '            grpClient1.Text = dispName
                '        End If
                '        If i = 2 Then
                '            grpClient2.Text = dispName
                '        End If
                '    End If
                '    '    End If
                '    'End If
                'Next
            End If
        End If
        Return xmlClientInfo.Clients.Count

    End Function

    Private Sub frmLatencyPoints_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        'MsgBox("W=" & Me.Width & " H=" & Me.Height)
    End Sub

    ''' <summary>
    ''' load existing XML Loan and Client LatencyPoints (logs)
    ''' </summary>
    ''' <returns>Hashtable</returns>
    ''' <remarks>contains Loan LatencyPoints at key '0'</remarks>
    Public Function ReadLogsFromXML() As Hashtable
        Dim htExistingLatencyPoints As New Hashtable
        htExistingLatencyPoints = finPowerOpObj.ReadClientLogs

        Return htExistingLatencyPoints

    End Function


    ''' <summary>
    ''' If dgv row.Type and row.Subject matches those in Hashtable, then updates the grid row with Hashtable LatencyPoint
    ''' </summary>
    ''' <param name="htExistingXmlLogs"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>By matching we avoid loading up log notes that are not actually LatencyPoints</remarks>
    Public Function checkExistingLogsInClient(ByVal htExistingXmlLogs As Hashtable) As Boolean
        Dim row As DataGridViewRow
        Dim alClientLP As ArrayList
        Dim LatencyPointDTOObj As finPowerOps.LatencyPoint
        Dim iKey As Integer = 0
        Dim i As Integer
        Dim subject As String = String.Empty
        Dim actionDate As String = String.Empty
        Dim isExists As Boolean = False
        Dim dgvGrid As DataGridView

        'iterate through htExistingXmlLogs for iKey Client (if ikey = 0 this is loan)
        For iKey = 0 To htExistingXmlLogs.Count - 1
            'Key 0 for LogType Loan
            If htExistingXmlLogs.Contains(iKey) Then
                dgvGrid = CType(Me.Controls("dgv" & iKey.ToString), DataGridView) 'dgv0 is for loan latency points
                alClientLP = CType(htExistingXmlLogs.Item(iKey), ArrayList)

                For i = 0 To alClientLP.Count - 1

                    LatencyPointDTOObj = CType(alClientLP.Item(i), finPowerOps.LatencyPoint)

                    If Not LatencyPointDTOObj Is Nothing Then
                        For Each row In dgvGrid.Rows
                            If IsDate(LatencyPointDTOObj.ActionDate) And row.Cells(CONST_GRIDCOLUMN_NO_LatencyPointType).Value = LatencyPointDTOObj.LogType _
                                            And row.Cells(CONST_GRIDCOLUMN_NO_Subject).Value = LatencyPointDTOObj.Subject Then
                                'LatencyPointsDTOObj from htExistingXmlLogs matches this grid row (therefore is a Latency point)

                                If row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = False Then
                                    'update latency point in grid
                                    isExists = True
                                    row.Cells(CONST_GRIDCOLUMN_NO_ActionDate).Value = String.Format("{0:dd/MM/yyyy}", LatencyPointDTOObj.ActionDate)
                                    row.Cells(CONST_GRIDCOLUMN_NO_Notes).Value = LatencyPointDTOObj.Notes
                                    row.Cells(CONST_GRIDCOLUMN_NO_CheckBox).Value = True
                                    Exit For
                                End If
                            End If
                        Next

                    End If
                Next
            End If

        Next
        Return isExists

    End Function



    'Private Sub dgvLatencyPoints1_CellEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLatencyPoints1.CellEnter
    '    'Dim cell As DataGridViewCell
    '    If e.ColumnIndex = 6 Then
    '        'MessageBox.Show("Column 6")

    '    Else()
    '        'MessageBox.Show("Not Column 6")
    '    End If
    'End Sub


End Class


