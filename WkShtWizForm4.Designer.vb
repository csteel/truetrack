﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WkShtWizForm4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WkShtWizForm4))
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.gpbxDependency = New System.Windows.Forms.GroupBox()
        Me.tlpDependency = New System.Windows.Forms.TableLayoutPanel()
        Me.txtbxNumChildren = New System.Windows.Forms.TextBox()
        Me.lblDependClient = New System.Windows.Forms.Label()
        Me.lblTotalDependValue = New System.Windows.Forms.Label()
        Me.lblTotalDep = New System.Windows.Forms.Label()
        Me.lblChildren = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnDependCouple = New System.Windows.Forms.Button()
        Me.btnDependSingle = New System.Windows.Forms.Button()
        Me.gpbxExpenses = New System.Windows.Forms.GroupBox()
        Me.tlpExpenses = New System.Windows.Forms.TableLayoutPanel()
        Me.btnQuestion12Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion12No = New System.Windows.Forms.CheckBox()
        Me.btnQuestion11Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion11No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion12 = New System.Windows.Forms.Label()
        Me.btnQuestion10Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion10No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion11 = New System.Windows.Forms.Label()
        Me.btnQuestion9Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion9No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion10 = New System.Windows.Forms.Label()
        Me.btnQuestion8Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion8No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion9 = New System.Windows.Forms.Label()
        Me.btnQuestion7Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion7No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion8 = New System.Windows.Forms.Label()
        Me.btnQuestion6Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion6No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion7 = New System.Windows.Forms.Label()
        Me.btnQuestion5Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion5No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion6 = New System.Windows.Forms.Label()
        Me.btnQuestion4Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion4No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion5 = New System.Windows.Forms.Label()
        Me.btnQuestion3Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion3No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion4 = New System.Windows.Forms.Label()
        Me.lblQuestion3 = New System.Windows.Forms.Label()
        Me.lblQuestion11Note = New System.Windows.Forms.Label()
        Me.tlpExpenseStated = New System.Windows.Forms.TableLayoutPanel()
        Me.lblIfExpensesNotStated = New System.Windows.Forms.Label()
        Me.lblNettExpensesStated = New System.Windows.Forms.Label()
        Me.txtbxNetExpensesStatedValue = New System.Windows.Forms.TextBox()
        Me.btnExpensesStated = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gpbxCalculator = New System.Windows.Forms.GroupBox()
        Me.tlpCalculator = New System.Windows.Forms.TableLayoutPanel()
        Me.lblAvYTD = New System.Windows.Forms.Label()
        Me.lblAvYTDValue = New System.Windows.Forms.Label()
        Me.dtpPSDate = New System.Windows.Forms.DateTimePicker()
        Me.lblGross = New System.Windows.Forms.Label()
        Me.lblPeriodStartDate = New System.Windows.Forms.Label()
        Me.dtpPEDate = New System.Windows.Forms.DateTimePicker()
        Me.txtbxGross = New System.Windows.Forms.TextBox()
        Me.lblPEDate = New System.Windows.Forms.Label()
        Me.txtbxPAYE = New System.Windows.Forms.TextBox()
        Me.lblPAYE = New System.Windows.Forms.Label()
        Me.lblDeductions = New System.Windows.Forms.Label()
        Me.txtbxDeductions = New System.Windows.Forms.TextBox()
        Me.btnDeleteSelectedRow = New System.Windows.Forms.Button()
        Me.gpbxIncome = New System.Windows.Forms.GroupBox()
        Me.tlpOvertime = New System.Windows.Forms.TableLayoutPanel()
        Me.lblOvertime = New System.Windows.Forms.Label()
        Me.rbOvertimeNo = New System.Windows.Forms.RadioButton()
        Me.rbOvertimeYes = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tlpIncome = New System.Windows.Forms.TableLayoutPanel()
        Me.btnQuestion2Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion2No = New System.Windows.Forms.CheckBox()
        Me.btnQuestion1Add = New System.Windows.Forms.Button()
        Me.ckbxQuestion1No = New System.Windows.Forms.CheckBox()
        Me.lblQuestion2 = New System.Windows.Forms.Label()
        Me.lblQuestion1 = New System.Windows.Forms.Label()
        Me.tlpIncomeStated = New System.Windows.Forms.TableLayoutPanel()
        Me.lblNettIncomeStated = New System.Windows.Forms.Label()
        Me.lblIfIncomeNotStated = New System.Windows.Forms.Label()
        Me.txtbxNetIncomeStatedValue = New System.Windows.Forms.TextBox()
        Me.btnIncomeStatedAdd = New System.Windows.Forms.Button()
        Me.lblOr = New System.Windows.Forms.Label()
        Me.gpbxBudgetSummary = New System.Windows.Forms.GroupBox()
        Me.PanelBudgetSummary = New System.Windows.Forms.Panel()
        Me.BudgetDataGridView = New System.Windows.Forms.DataGridView()
        Me.BudgetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblOtherExplain = New System.Windows.Forms.Label()
        Me.lblMarginValue = New System.Windows.Forms.Label()
        Me.lblMargin = New System.Windows.Forms.Label()
        Me.lblTotalExpensesValue = New System.Windows.Forms.Label()
        Me.lblTotalExpenses = New System.Windows.Forms.Label()
        Me.lblTotalIncomeValue = New System.Windows.Forms.Label()
        Me.lbltotalIncome = New System.Windows.Forms.Label()
        Me.lblContactAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblContactName = New System.Windows.Forms.Label()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblContactNumber = New System.Windows.Forms.Label()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnQRGList = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.lblCurrentLoan = New System.Windows.Forms.Label()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEnquiryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeclinedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WithdrawnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DealersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.BudgetTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtbxOtherExplain = New AppWhShtB.SpellBox()
        Me.txtbxOvertime = New AppWhShtB.SpellBox()
        Me.StatusStripMessage.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxDependency.SuspendLayout()
        Me.tlpDependency.SuspendLayout()
        Me.gpbxExpenses.SuspendLayout()
        Me.tlpExpenses.SuspendLayout()
        Me.tlpExpenseStated.SuspendLayout()
        Me.gpbxCalculator.SuspendLayout()
        Me.tlpCalculator.SuspendLayout()
        Me.gpbxIncome.SuspendLayout()
        Me.tlpOvertime.SuspendLayout()
        Me.tlpIncome.SuspendLayout()
        Me.tlpIncomeStated.SuspendLayout()
        Me.gpbxBudgetSummary.SuspendLayout()
        Me.PanelBudgetSummary.SuspendLayout()
        CType(Me.BudgetDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BudgetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxStatus.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.gpbxManager.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnFinish
        '
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(617, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.TabIndex = 33
        Me.btnFinish.Text = "Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(536, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 32
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(455, 3)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 31
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(374, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 30
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 990)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(944, 22)
        Me.StatusStripMessage.TabIndex = 175
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.Controls.Add(Me.txtbxOtherExplain)
        Me.Panel1.Controls.Add(Me.gpbxDependency)
        Me.Panel1.Controls.Add(Me.gpbxExpenses)
        Me.Panel1.Controls.Add(Me.gpbxCalculator)
        Me.Panel1.Controls.Add(Me.btnDeleteSelectedRow)
        Me.Panel1.Controls.Add(Me.gpbxIncome)
        Me.Panel1.Controls.Add(Me.gpbxBudgetSummary)
        Me.Panel1.Controls.Add(Me.lblOtherExplain)
        Me.Panel1.Controls.Add(Me.lblMarginValue)
        Me.Panel1.Controls.Add(Me.lblMargin)
        Me.Panel1.Controls.Add(Me.lblTotalExpensesValue)
        Me.Panel1.Controls.Add(Me.lblTotalExpenses)
        Me.Panel1.Controls.Add(Me.lblTotalIncomeValue)
        Me.Panel1.Controls.Add(Me.lbltotalIncome)
        Me.Panel1.Location = New System.Drawing.Point(0, 102)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 853)
        Me.Panel1.TabIndex = 1
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'gpbxDependency
        '
        Me.gpbxDependency.Controls.Add(Me.tlpDependency)
        Me.gpbxDependency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxDependency.Location = New System.Drawing.Point(12, 457)
        Me.gpbxDependency.Name = "gpbxDependency"
        Me.gpbxDependency.Size = New System.Drawing.Size(912, 59)
        Me.gpbxDependency.TabIndex = 4
        Me.gpbxDependency.TabStop = False
        Me.gpbxDependency.Text = "Dependency (monthly average)"
        '
        'tlpDependency
        '
        Me.tlpDependency.ColumnCount = 8
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125.0!))
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125.0!))
        Me.tlpDependency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77.0!))
        Me.tlpDependency.Controls.Add(Me.txtbxNumChildren, 4, 0)
        Me.tlpDependency.Controls.Add(Me.lblDependClient, 2, 0)
        Me.tlpDependency.Controls.Add(Me.lblTotalDependValue, 7, 0)
        Me.tlpDependency.Controls.Add(Me.lblTotalDep, 6, 0)
        Me.tlpDependency.Controls.Add(Me.lblChildren, 3, 0)
        Me.tlpDependency.Controls.Add(Me.Label3, 5, 0)
        Me.tlpDependency.Controls.Add(Me.btnDependCouple, 1, 0)
        Me.tlpDependency.Controls.Add(Me.btnDependSingle, 0, 0)
        Me.tlpDependency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpDependency.Location = New System.Drawing.Point(6, 20)
        Me.tlpDependency.Name = "tlpDependency"
        Me.tlpDependency.RowCount = 1
        Me.tlpDependency.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpDependency.Size = New System.Drawing.Size(900, 29)
        Me.tlpDependency.TabIndex = 1
        '
        'txtbxNumChildren
        '
        Me.txtbxNumChildren.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetDependencyChildren", True))
        Me.txtbxNumChildren.Location = New System.Drawing.Point(456, 3)
        Me.txtbxNumChildren.Name = "txtbxNumChildren"
        Me.txtbxNumChildren.Size = New System.Drawing.Size(30, 20)
        Me.txtbxNumChildren.TabIndex = 3
        '
        'lblDependClient
        '
        Me.lblDependClient.AutoSize = True
        Me.lblDependClient.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetDependencyClient", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblDependClient.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblDependClient.Location = New System.Drawing.Point(122, 0)
        Me.lblDependClient.Name = "lblDependClient"
        Me.lblDependClient.Size = New System.Drawing.Size(203, 29)
        Me.lblDependClient.TabIndex = 119
        Me.lblDependClient.Text = "0.00"
        Me.lblDependClient.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTotalDependValue
        '
        Me.lblTotalDependValue.AutoSize = True
        Me.lblTotalDependValue.BackColor = System.Drawing.SystemColors.Control
        Me.lblTotalDependValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetDependencyTotal", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblTotalDependValue.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblTotalDependValue.Location = New System.Drawing.Point(826, 0)
        Me.lblTotalDependValue.Name = "lblTotalDependValue"
        Me.lblTotalDependValue.Size = New System.Drawing.Size(28, 29)
        Me.lblTotalDependValue.TabIndex = 116
        Me.lblTotalDependValue.Text = "0.00"
        Me.lblTotalDependValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalDep
        '
        Me.lblTotalDep.AutoSize = True
        Me.lblTotalDep.BackColor = System.Drawing.SystemColors.Control
        Me.lblTotalDep.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTotalDep.Location = New System.Drawing.Point(722, 0)
        Me.lblTotalDep.Name = "lblTotalDep"
        Me.lblTotalDep.Size = New System.Drawing.Size(98, 29)
        Me.lblTotalDep.TabIndex = 101
        Me.lblTotalDep.Text = "Total Dependency:"
        Me.lblTotalDep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblChildren
        '
        Me.lblChildren.AutoSize = True
        Me.lblChildren.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblChildren.Location = New System.Drawing.Point(365, 0)
        Me.lblChildren.Name = "lblChildren"
        Me.lblChildren.Size = New System.Drawing.Size(85, 29)
        Me.lblChildren.TabIndex = 98
        Me.lblChildren.Text = "No: Dependants"
        Me.lblChildren.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label3.Location = New System.Drawing.Point(492, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 29)
        Me.Label3.TabIndex = 99
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDependCouple
        '
        Me.btnDependCouple.Location = New System.Drawing.Point(59, 3)
        Me.btnDependCouple.Name = "btnDependCouple"
        Me.btnDependCouple.Size = New System.Drawing.Size(57, 23)
        Me.btnDependCouple.TabIndex = 2
        Me.btnDependCouple.Text = "Couple"
        Me.btnDependCouple.UseVisualStyleBackColor = True
        '
        'btnDependSingle
        '
        Me.btnDependSingle.Location = New System.Drawing.Point(3, 3)
        Me.btnDependSingle.Name = "btnDependSingle"
        Me.btnDependSingle.Size = New System.Drawing.Size(50, 23)
        Me.btnDependSingle.TabIndex = 1
        Me.btnDependSingle.Text = "Single"
        Me.btnDependSingle.UseVisualStyleBackColor = True
        '
        'gpbxExpenses
        '
        Me.gpbxExpenses.Controls.Add(Me.tlpExpenses)
        Me.gpbxExpenses.Controls.Add(Me.tlpExpenseStated)
        Me.gpbxExpenses.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxExpenses.Location = New System.Drawing.Point(12, 246)
        Me.gpbxExpenses.Name = "gpbxExpenses"
        Me.gpbxExpenses.Size = New System.Drawing.Size(912, 205)
        Me.gpbxExpenses.TabIndex = 3
        Me.gpbxExpenses.TabStop = False
        Me.gpbxExpenses.Text = "Expenses (monthly average)"
        '
        'tlpExpenses
        '
        Me.tlpExpenses.ColumnCount = 7
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenses.Controls.Add(Me.btnQuestion12Add, 6, 4)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion12No, 5, 4)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion11Add, 2, 4)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion11No, 1, 4)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion12, 4, 4)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion10Add, 6, 3)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion10No, 5, 3)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion11, 0, 4)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion9Add, 2, 3)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion9No, 1, 3)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion10, 4, 3)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion8Add, 6, 2)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion8No, 5, 2)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion9, 0, 3)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion7Add, 2, 2)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion7No, 1, 2)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion8, 4, 2)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion6Add, 6, 1)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion6No, 5, 1)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion7, 0, 2)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion5Add, 2, 1)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion5No, 1, 1)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion6, 4, 1)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion4Add, 6, 0)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion4No, 5, 0)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion5, 0, 1)
        Me.tlpExpenses.Controls.Add(Me.btnQuestion3Add, 2, 0)
        Me.tlpExpenses.Controls.Add(Me.ckbxQuestion3No, 1, 0)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion4, 4, 0)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion3, 0, 0)
        Me.tlpExpenses.Controls.Add(Me.lblQuestion11Note, 3, 4)
        Me.tlpExpenses.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpExpenses.Location = New System.Drawing.Point(6, 48)
        Me.tlpExpenses.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.tlpExpenses.Name = "tlpExpenses"
        Me.tlpExpenses.RowCount = 5
        Me.tlpExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.tlpExpenses.Size = New System.Drawing.Size(900, 149)
        Me.tlpExpenses.TabIndex = 2
        '
        'btnQuestion12Add
        '
        Me.btnQuestion12Add.Location = New System.Drawing.Point(855, 119)
        Me.btnQuestion12Add.Name = "btnQuestion12Add"
        Me.btnQuestion12Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion12Add.TabIndex = 20
        Me.btnQuestion12Add.Text = "Add"
        Me.btnQuestion12Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion12No
        '
        Me.ckbxQuestion12No.AutoSize = True
        Me.ckbxQuestion12No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion12No.Location = New System.Drawing.Point(811, 119)
        Me.ckbxQuestion12No.Name = "ckbxQuestion12No"
        Me.ckbxQuestion12No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion12No.Size = New System.Drawing.Size(38, 27)
        Me.ckbxQuestion12No.TabIndex = 19
        Me.ckbxQuestion12No.Text = "no"
        Me.ckbxQuestion12No.UseVisualStyleBackColor = True
        '
        'btnQuestion11Add
        '
        Me.btnQuestion11Add.Location = New System.Drawing.Point(252, 119)
        Me.btnQuestion11Add.Name = "btnQuestion11Add"
        Me.btnQuestion11Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion11Add.TabIndex = 18
        Me.btnQuestion11Add.Text = "Add"
        Me.btnQuestion11Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion11No
        '
        Me.ckbxQuestion11No.AutoSize = True
        Me.ckbxQuestion11No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion11No.Location = New System.Drawing.Point(208, 119)
        Me.ckbxQuestion11No.Name = "ckbxQuestion11No"
        Me.ckbxQuestion11No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion11No.Size = New System.Drawing.Size(38, 27)
        Me.ckbxQuestion11No.TabIndex = 17
        Me.ckbxQuestion11No.Text = "no"
        Me.ckbxQuestion11No.UseVisualStyleBackColor = True
        '
        'lblQuestion12
        '
        Me.lblQuestion12.AutoSize = True
        Me.lblQuestion12.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion12.Location = New System.Drawing.Point(606, 116)
        Me.lblQuestion12.Name = "lblQuestion12"
        Me.lblQuestion12.Size = New System.Drawing.Size(82, 33)
        Me.lblQuestion12.TabIndex = 0
        Me.lblQuestion12.Text = "This YFL Loan?"
        Me.lblQuestion12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion10Add
        '
        Me.btnQuestion10Add.Location = New System.Drawing.Point(855, 90)
        Me.btnQuestion10Add.Name = "btnQuestion10Add"
        Me.btnQuestion10Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion10Add.TabIndex = 16
        Me.btnQuestion10Add.Text = "Add"
        Me.btnQuestion10Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion10No
        '
        Me.ckbxQuestion10No.AutoSize = True
        Me.ckbxQuestion10No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion10No.Location = New System.Drawing.Point(811, 90)
        Me.ckbxQuestion10No.Name = "ckbxQuestion10No"
        Me.ckbxQuestion10No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion10No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion10No.TabIndex = 15
        Me.ckbxQuestion10No.Text = "no"
        Me.ckbxQuestion10No.UseVisualStyleBackColor = True
        '
        'lblQuestion11
        '
        Me.lblQuestion11.AutoSize = True
        Me.lblQuestion11.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion11.Location = New System.Drawing.Point(3, 116)
        Me.lblQuestion11.Name = "lblQuestion11"
        Me.lblQuestion11.Size = New System.Drawing.Size(148, 33)
        Me.lblQuestion11.TabIndex = 0
        Me.lblQuestion11.Text = "Other Board/Rent/Mortgage?"
        Me.lblQuestion11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion9Add
        '
        Me.btnQuestion9Add.Location = New System.Drawing.Point(252, 90)
        Me.btnQuestion9Add.Name = "btnQuestion9Add"
        Me.btnQuestion9Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion9Add.TabIndex = 14
        Me.btnQuestion9Add.Text = "Add"
        Me.btnQuestion9Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion9No
        '
        Me.ckbxQuestion9No.AutoSize = True
        Me.ckbxQuestion9No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion9No.Location = New System.Drawing.Point(208, 90)
        Me.ckbxQuestion9No.Name = "ckbxQuestion9No"
        Me.ckbxQuestion9No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion9No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion9No.TabIndex = 13
        Me.ckbxQuestion9No.Text = "no"
        Me.ckbxQuestion9No.UseVisualStyleBackColor = True
        '
        'lblQuestion10
        '
        Me.lblQuestion10.AutoSize = True
        Me.lblQuestion10.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion10.Location = New System.Drawing.Point(606, 87)
        Me.lblQuestion10.Name = "lblQuestion10"
        Me.lblQuestion10.Size = New System.Drawing.Size(107, 29)
        Me.lblQuestion10.TabIndex = 0
        Me.lblQuestion10.Text = "Any other Expenses?"
        Me.lblQuestion10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion8Add
        '
        Me.btnQuestion8Add.Location = New System.Drawing.Point(855, 61)
        Me.btnQuestion8Add.Name = "btnQuestion8Add"
        Me.btnQuestion8Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion8Add.TabIndex = 12
        Me.btnQuestion8Add.Text = "Add"
        Me.btnQuestion8Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion8No
        '
        Me.ckbxQuestion8No.AutoSize = True
        Me.ckbxQuestion8No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion8No.Location = New System.Drawing.Point(811, 61)
        Me.ckbxQuestion8No.Name = "ckbxQuestion8No"
        Me.ckbxQuestion8No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion8No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion8No.TabIndex = 11
        Me.ckbxQuestion8No.Text = "no"
        Me.ckbxQuestion8No.UseVisualStyleBackColor = True
        '
        'lblQuestion9
        '
        Me.lblQuestion9.AutoSize = True
        Me.lblQuestion9.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion9.Location = New System.Drawing.Point(3, 87)
        Me.lblQuestion9.Name = "lblQuestion9"
        Me.lblQuestion9.Size = New System.Drawing.Size(129, 29)
        Me.lblQuestion9.TabIndex = 0
        Me.lblQuestion9.Text = "Any Insurance payments?"
        Me.lblQuestion9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion7Add
        '
        Me.btnQuestion7Add.Location = New System.Drawing.Point(252, 61)
        Me.btnQuestion7Add.Name = "btnQuestion7Add"
        Me.btnQuestion7Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion7Add.TabIndex = 10
        Me.btnQuestion7Add.Text = "Add"
        Me.btnQuestion7Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion7No
        '
        Me.ckbxQuestion7No.AutoSize = True
        Me.ckbxQuestion7No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion7No.Location = New System.Drawing.Point(208, 61)
        Me.ckbxQuestion7No.Name = "ckbxQuestion7No"
        Me.ckbxQuestion7No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion7No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion7No.TabIndex = 9
        Me.ckbxQuestion7No.Text = "no"
        Me.ckbxQuestion7No.UseVisualStyleBackColor = True
        '
        'lblQuestion8
        '
        Me.lblQuestion8.AutoSize = True
        Me.lblQuestion8.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion8.Location = New System.Drawing.Point(606, 58)
        Me.lblQuestion8.Name = "lblQuestion8"
        Me.lblQuestion8.Size = New System.Drawing.Size(134, 29)
        Me.lblQuestion8.TabIndex = 0
        Me.lblQuestion8.Text = "Any Credit Card payments?"
        Me.lblQuestion8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion6Add
        '
        Me.btnQuestion6Add.Location = New System.Drawing.Point(855, 32)
        Me.btnQuestion6Add.Name = "btnQuestion6Add"
        Me.btnQuestion6Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion6Add.TabIndex = 8
        Me.btnQuestion6Add.Text = "Add"
        Me.btnQuestion6Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion6No
        '
        Me.ckbxQuestion6No.AutoSize = True
        Me.ckbxQuestion6No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion6No.Location = New System.Drawing.Point(811, 32)
        Me.ckbxQuestion6No.Name = "ckbxQuestion6No"
        Me.ckbxQuestion6No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion6No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion6No.TabIndex = 7
        Me.ckbxQuestion6No.Text = "no"
        Me.ckbxQuestion6No.UseVisualStyleBackColor = True
        '
        'lblQuestion7
        '
        Me.lblQuestion7.AutoSize = True
        Me.lblQuestion7.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion7.Location = New System.Drawing.Point(3, 58)
        Me.lblQuestion7.Name = "lblQuestion7"
        Me.lblQuestion7.Size = New System.Drawing.Size(148, 29)
        Me.lblQuestion7.TabIndex = 0
        Me.lblQuestion7.Text = "Any Hire purchase payments?"
        Me.lblQuestion7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion5Add
        '
        Me.btnQuestion5Add.Location = New System.Drawing.Point(252, 32)
        Me.btnQuestion5Add.Name = "btnQuestion5Add"
        Me.btnQuestion5Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion5Add.TabIndex = 6
        Me.btnQuestion5Add.Text = "Add"
        Me.btnQuestion5Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion5No
        '
        Me.ckbxQuestion5No.AutoSize = True
        Me.ckbxQuestion5No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion5No.Location = New System.Drawing.Point(208, 32)
        Me.ckbxQuestion5No.Name = "ckbxQuestion5No"
        Me.ckbxQuestion5No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion5No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion5No.TabIndex = 5
        Me.ckbxQuestion5No.Text = "no"
        Me.ckbxQuestion5No.UseVisualStyleBackColor = True
        '
        'lblQuestion6
        '
        Me.lblQuestion6.AutoSize = True
        Me.lblQuestion6.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion6.Location = New System.Drawing.Point(606, 29)
        Me.lblQuestion6.Name = "lblQuestion6"
        Me.lblQuestion6.Size = New System.Drawing.Size(166, 29)
        Me.lblQuestion6.TabIndex = 0
        Me.lblQuestion6.Text = "Is your partner paying any Loans?"
        Me.lblQuestion6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion4Add
        '
        Me.btnQuestion4Add.Location = New System.Drawing.Point(855, 3)
        Me.btnQuestion4Add.Name = "btnQuestion4Add"
        Me.btnQuestion4Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion4Add.TabIndex = 4
        Me.btnQuestion4Add.Text = "Add"
        Me.btnQuestion4Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion4No
        '
        Me.ckbxQuestion4No.AutoSize = True
        Me.ckbxQuestion4No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion4No.Location = New System.Drawing.Point(811, 3)
        Me.ckbxQuestion4No.Name = "ckbxQuestion4No"
        Me.ckbxQuestion4No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion4No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion4No.TabIndex = 3
        Me.ckbxQuestion4No.Text = "no"
        Me.ckbxQuestion4No.UseVisualStyleBackColor = True
        '
        'lblQuestion5
        '
        Me.lblQuestion5.AutoSize = True
        Me.lblQuestion5.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion5.Location = New System.Drawing.Point(3, 29)
        Me.lblQuestion5.Name = "lblQuestion5"
        Me.lblQuestion5.Size = New System.Drawing.Size(162, 29)
        Me.lblQuestion5.TabIndex = 0
        Me.lblQuestion5.Text = "Are you paying any other Loans?"
        Me.lblQuestion5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQuestion3Add
        '
        Me.btnQuestion3Add.Location = New System.Drawing.Point(252, 3)
        Me.btnQuestion3Add.Name = "btnQuestion3Add"
        Me.btnQuestion3Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion3Add.TabIndex = 2
        Me.btnQuestion3Add.Text = "Add"
        Me.btnQuestion3Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion3No
        '
        Me.ckbxQuestion3No.AutoSize = True
        Me.ckbxQuestion3No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion3No.Location = New System.Drawing.Point(208, 3)
        Me.ckbxQuestion3No.Name = "ckbxQuestion3No"
        Me.ckbxQuestion3No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion3No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion3No.TabIndex = 1
        Me.ckbxQuestion3No.Text = "no"
        Me.ckbxQuestion3No.UseVisualStyleBackColor = True
        '
        'lblQuestion4
        '
        Me.lblQuestion4.AutoSize = True
        Me.lblQuestion4.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion4.Location = New System.Drawing.Point(606, 0)
        Me.lblQuestion4.Name = "lblQuestion4"
        Me.lblQuestion4.Size = New System.Drawing.Size(128, 29)
        Me.lblQuestion4.TabIndex = 0
        Me.lblQuestion4.Text = "Are you paying any fines?"
        Me.lblQuestion4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQuestion3
        '
        Me.lblQuestion3.AutoSize = True
        Me.lblQuestion3.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion3.Location = New System.Drawing.Point(3, 0)
        Me.lblQuestion3.Name = "lblQuestion3"
        Me.lblQuestion3.Size = New System.Drawing.Size(169, 29)
        Me.lblQuestion3.TabIndex = 0
        Me.lblQuestion3.Text = "Are you paying any Child Support?"
        Me.lblQuestion3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQuestion11Note
        '
        Me.lblQuestion11Note.AutoSize = True
        Me.lblQuestion11Note.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion11Note.Location = New System.Drawing.Point(300, 116)
        Me.lblQuestion11Note.Name = "lblQuestion11Note"
        Me.lblQuestion11Note.Size = New System.Drawing.Size(124, 33)
        Me.lblQuestion11Note.TabIndex = 21
        Me.lblQuestion11Note.Text = "(Already included above)"
        Me.lblQuestion11Note.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlpExpenseStated
        '
        Me.tlpExpenseStated.ColumnCount = 5
        Me.tlpExpenseStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.tlpExpenseStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenseStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenseStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpExpenseStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpExpenseStated.Controls.Add(Me.lblIfExpensesNotStated, 4, 0)
        Me.tlpExpenseStated.Controls.Add(Me.lblNettExpensesStated, 0, 0)
        Me.tlpExpenseStated.Controls.Add(Me.txtbxNetExpensesStatedValue, 1, 0)
        Me.tlpExpenseStated.Controls.Add(Me.btnExpensesStated, 3, 0)
        Me.tlpExpenseStated.Controls.Add(Me.Label1, 2, 0)
        Me.tlpExpenseStated.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpExpenseStated.Location = New System.Drawing.Point(6, 19)
        Me.tlpExpenseStated.Name = "tlpExpenseStated"
        Me.tlpExpenseStated.RowCount = 1
        Me.tlpExpenseStated.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpExpenseStated.Size = New System.Drawing.Size(900, 29)
        Me.tlpExpenseStated.TabIndex = 1
        '
        'lblIfExpensesNotStated
        '
        Me.lblIfExpensesNotStated.AutoSize = True
        Me.lblIfExpensesNotStated.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblIfExpensesNotStated.Location = New System.Drawing.Point(361, 0)
        Me.lblIfExpensesNotStated.Name = "lblIfExpensesNotStated"
        Me.lblIfExpensesNotStated.Size = New System.Drawing.Size(62, 29)
        Me.lblIfExpensesNotStated.TabIndex = 154
        Me.lblIfExpensesNotStated.Text = "if not stated"
        Me.lblIfExpensesNotStated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNettExpensesStated
        '
        Me.lblNettExpensesStated.AutoSize = True
        Me.lblNettExpensesStated.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblNettExpensesStated.Location = New System.Drawing.Point(3, 0)
        Me.lblNettExpensesStated.Name = "lblNettExpensesStated"
        Me.lblNettExpensesStated.Size = New System.Drawing.Size(160, 29)
        Me.lblNettExpensesStated.TabIndex = 104
        Me.lblNettExpensesStated.Text = "Expenses Stated on Application:"
        Me.lblNettExpensesStated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxNetExpensesStatedValue
        '
        Me.txtbxNetExpensesStatedValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ExpensesStated", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxNetExpensesStatedValue.Location = New System.Drawing.Point(208, 3)
        Me.txtbxNetExpensesStatedValue.Name = "txtbxNetExpensesStatedValue"
        Me.txtbxNetExpensesStatedValue.Size = New System.Drawing.Size(77, 20)
        Me.txtbxNetExpensesStatedValue.TabIndex = 1
        '
        'btnExpensesStated
        '
        Me.btnExpensesStated.Location = New System.Drawing.Point(313, 3)
        Me.btnExpensesStated.Name = "btnExpensesStated"
        Me.btnExpensesStated.Size = New System.Drawing.Size(42, 23)
        Me.btnExpensesStated.TabIndex = 2
        Me.btnExpensesStated.Text = "Add"
        Me.btnExpensesStated.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(291, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 150
        Me.Label1.Text = "or"
        '
        'gpbxCalculator
        '
        Me.gpbxCalculator.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCalculator.Controls.Add(Me.tlpCalculator)
        Me.gpbxCalculator.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCalculator.Location = New System.Drawing.Point(12, 3)
        Me.gpbxCalculator.Name = "gpbxCalculator"
        Me.gpbxCalculator.Size = New System.Drawing.Size(912, 80)
        Me.gpbxCalculator.TabIndex = 193
        Me.gpbxCalculator.TabStop = False
        Me.gpbxCalculator.Text = "Calculator for checking Earnings based on Y.T.D."
        '
        'tlpCalculator
        '
        Me.tlpCalculator.ColumnCount = 8
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.736008!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.16493!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.677068!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.91679!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.06538!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.91679!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.02873!))
        Me.tlpCalculator.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.4943!))
        Me.tlpCalculator.Controls.Add(Me.lblAvYTD, 0, 1)
        Me.tlpCalculator.Controls.Add(Me.lblAvYTDValue, 4, 1)
        Me.tlpCalculator.Controls.Add(Me.dtpPSDate, 7, 0)
        Me.tlpCalculator.Controls.Add(Me.lblGross, 0, 0)
        Me.tlpCalculator.Controls.Add(Me.lblPeriodStartDate, 6, 0)
        Me.tlpCalculator.Controls.Add(Me.dtpPEDate, 7, 1)
        Me.tlpCalculator.Controls.Add(Me.txtbxGross, 1, 0)
        Me.tlpCalculator.Controls.Add(Me.lblPEDate, 6, 1)
        Me.tlpCalculator.Controls.Add(Me.txtbxPAYE, 3, 0)
        Me.tlpCalculator.Controls.Add(Me.lblPAYE, 2, 0)
        Me.tlpCalculator.Controls.Add(Me.lblDeductions, 4, 0)
        Me.tlpCalculator.Controls.Add(Me.txtbxDeductions, 5, 0)
        Me.tlpCalculator.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpCalculator.Location = New System.Drawing.Point(6, 19)
        Me.tlpCalculator.Name = "tlpCalculator"
        Me.tlpCalculator.RowCount = 2
        Me.tlpCalculator.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCalculator.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCalculator.Size = New System.Drawing.Size(900, 51)
        Me.tlpCalculator.TabIndex = 0
        '
        'lblAvYTD
        '
        Me.lblAvYTD.AutoSize = True
        Me.tlpCalculator.SetColumnSpan(Me.lblAvYTD, 4)
        Me.lblAvYTD.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblAvYTD.Location = New System.Drawing.Point(193, 25)
        Me.lblAvYTD.Name = "lblAvYTD"
        Me.lblAvYTD.Size = New System.Drawing.Size(158, 26)
        Me.lblAvYTD.TabIndex = 155
        Me.lblAvYTD.Text = "Net Earnings: mnth av. Y.T.D. ="
        Me.lblAvYTD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAvYTDValue
        '
        Me.lblAvYTDValue.AutoSize = True
        Me.lblAvYTDValue.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblAvYTDValue.Location = New System.Drawing.Point(357, 25)
        Me.lblAvYTDValue.Name = "lblAvYTDValue"
        Me.lblAvYTDValue.Size = New System.Drawing.Size(37, 26)
        Me.lblAvYTDValue.TabIndex = 150
        Me.lblAvYTDValue.Text = "$ 0.00"
        Me.lblAvYTDValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPSDate
        '
        Me.dtpPSDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPSDate.Location = New System.Drawing.Point(734, 3)
        Me.dtpPSDate.Name = "dtpPSDate"
        Me.dtpPSDate.Size = New System.Drawing.Size(145, 20)
        Me.dtpPSDate.TabIndex = 4
        '
        'lblGross
        '
        Me.lblGross.AutoSize = True
        Me.lblGross.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblGross.Location = New System.Drawing.Point(3, 0)
        Me.lblGross.Name = "lblGross"
        Me.lblGross.Size = New System.Drawing.Size(34, 25)
        Me.lblGross.TabIndex = 143
        Me.lblGross.Text = "Gross"
        Me.lblGross.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriodStartDate
        '
        Me.lblPeriodStartDate.AutoSize = True
        Me.lblPeriodStartDate.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblPeriodStartDate.Location = New System.Drawing.Point(626, 0)
        Me.lblPeriodStartDate.Name = "lblPeriodStartDate"
        Me.lblPeriodStartDate.Size = New System.Drawing.Size(102, 25)
        Me.lblPeriodStartDate.TabIndex = 154
        Me.lblPeriodStartDate.Text = "Period Starting Date"
        Me.lblPeriodStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpPEDate
        '
        Me.dtpPEDate.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.EnquiryBindingSource, "BudgetPeriodEnding", True))
        Me.dtpPEDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPEDate.Location = New System.Drawing.Point(734, 28)
        Me.dtpPEDate.Name = "dtpPEDate"
        Me.dtpPEDate.Size = New System.Drawing.Size(145, 20)
        Me.dtpPEDate.TabIndex = 5
        '
        'txtbxGross
        '
        Me.txtbxGross.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetGross", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxGross.Location = New System.Drawing.Point(63, 3)
        Me.txtbxGross.Name = "txtbxGross"
        Me.txtbxGross.Size = New System.Drawing.Size(77, 20)
        Me.txtbxGross.TabIndex = 1
        '
        'lblPEDate
        '
        Me.lblPEDate.AutoSize = True
        Me.lblPEDate.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblPEDate.Location = New System.Drawing.Point(629, 25)
        Me.lblPEDate.Name = "lblPEDate"
        Me.lblPEDate.Size = New System.Drawing.Size(99, 26)
        Me.lblPEDate.TabIndex = 153
        Me.lblPEDate.Text = "Period Ending Date"
        Me.lblPEDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxPAYE
        '
        Me.txtbxPAYE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetPAYE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxPAYE.Location = New System.Drawing.Point(250, 3)
        Me.txtbxPAYE.Name = "txtbxPAYE"
        Me.txtbxPAYE.Size = New System.Drawing.Size(68, 20)
        Me.txtbxPAYE.TabIndex = 2
        '
        'lblPAYE
        '
        Me.lblPAYE.AutoSize = True
        Me.lblPAYE.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblPAYE.Location = New System.Drawing.Point(203, 0)
        Me.lblPAYE.Name = "lblPAYE"
        Me.lblPAYE.Size = New System.Drawing.Size(41, 25)
        Me.lblPAYE.TabIndex = 145
        Me.lblPAYE.Text = "- PAYE"
        Me.lblPAYE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeductions
        '
        Me.lblDeductions.AutoSize = True
        Me.lblDeductions.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblDeductions.Location = New System.Drawing.Point(392, 0)
        Me.lblDeductions.Name = "lblDeductions"
        Me.lblDeductions.Size = New System.Drawing.Size(67, 25)
        Me.lblDeductions.TabIndex = 152
        Me.lblDeductions.Text = "- Deductions"
        Me.lblDeductions.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxDeductions
        '
        Me.txtbxDeductions.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetDeductions", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxDeductions.Location = New System.Drawing.Point(465, 3)
        Me.txtbxDeductions.Name = "txtbxDeductions"
        Me.txtbxDeductions.Size = New System.Drawing.Size(68, 20)
        Me.txtbxDeductions.TabIndex = 3
        '
        'btnDeleteSelectedRow
        '
        Me.btnDeleteSelectedRow.BackColor = System.Drawing.Color.Linen
        Me.btnDeleteSelectedRow.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.btnDeleteSelectedRow.FlatAppearance.BorderSize = 2
        Me.btnDeleteSelectedRow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnDeleteSelectedRow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.btnDeleteSelectedRow.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnDeleteSelectedRow.Location = New System.Drawing.Point(26, 733)
        Me.btnDeleteSelectedRow.Name = "btnDeleteSelectedRow"
        Me.btnDeleteSelectedRow.Size = New System.Drawing.Size(151, 23)
        Me.btnDeleteSelectedRow.TabIndex = 192
        Me.btnDeleteSelectedRow.TabStop = False
        Me.btnDeleteSelectedRow.Text = "Delete Selected Record"
        Me.btnDeleteSelectedRow.UseVisualStyleBackColor = False
        '
        'gpbxIncome
        '
        Me.gpbxIncome.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxIncome.Controls.Add(Me.tlpOvertime)
        Me.gpbxIncome.Controls.Add(Me.tlpIncome)
        Me.gpbxIncome.Controls.Add(Me.tlpIncomeStated)
        Me.gpbxIncome.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxIncome.Location = New System.Drawing.Point(12, 89)
        Me.gpbxIncome.Name = "gpbxIncome"
        Me.gpbxIncome.Size = New System.Drawing.Size(912, 150)
        Me.gpbxIncome.TabIndex = 2
        Me.gpbxIncome.TabStop = False
        Me.gpbxIncome.Text = "Income (monthly average)"
        '
        'tlpOvertime
        '
        Me.tlpOvertime.ColumnCount = 4
        Me.tlpOvertime.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpOvertime.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpOvertime.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpOvertime.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpOvertime.Controls.Add(Me.lblOvertime, 0, 0)
        Me.tlpOvertime.Controls.Add(Me.rbOvertimeNo, 1, 0)
        Me.tlpOvertime.Controls.Add(Me.rbOvertimeYes, 2, 0)
        Me.tlpOvertime.Controls.Add(Me.Label2, 0, 1)
        Me.tlpOvertime.Controls.Add(Me.txtbxOvertime, 1, 1)
        Me.tlpOvertime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpOvertime.Location = New System.Drawing.Point(6, 73)
        Me.tlpOvertime.Name = "tlpOvertime"
        Me.tlpOvertime.RowCount = 2
        Me.tlpOvertime.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpOvertime.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.tlpOvertime.Size = New System.Drawing.Size(899, 75)
        Me.tlpOvertime.TabIndex = 147
        '
        'lblOvertime
        '
        Me.lblOvertime.AutoSize = True
        Me.lblOvertime.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblOvertime.Location = New System.Drawing.Point(3, 0)
        Me.lblOvertime.Name = "lblOvertime"
        Me.lblOvertime.Size = New System.Drawing.Size(313, 32)
        Me.lblOvertime.TabIndex = 0
        Me.lblOvertime.Text = "Overtime / Commissions are included in the NET Income Stated?"
        Me.lblOvertime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbOvertimeNo
        '
        Me.rbOvertimeNo.AutoSize = True
        Me.rbOvertimeNo.Location = New System.Drawing.Point(322, 3)
        Me.rbOvertimeNo.Name = "rbOvertimeNo"
        Me.rbOvertimeNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.rbOvertimeNo.Size = New System.Drawing.Size(39, 17)
        Me.rbOvertimeNo.TabIndex = 7
        Me.rbOvertimeNo.TabStop = True
        Me.rbOvertimeNo.Text = "No"
        Me.rbOvertimeNo.UseVisualStyleBackColor = True
        '
        'rbOvertimeYes
        '
        Me.rbOvertimeYes.AutoSize = True
        Me.rbOvertimeYes.Location = New System.Drawing.Point(367, 3)
        Me.rbOvertimeYes.Name = "rbOvertimeYes"
        Me.rbOvertimeYes.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.rbOvertimeYes.Size = New System.Drawing.Size(43, 17)
        Me.rbOvertimeYes.TabIndex = 8
        Me.rbOvertimeYes.TabStop = True
        Me.rbOvertimeYes.Text = "Yes"
        Me.rbOvertimeYes.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(309, 43)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "If YES what was done to verify the continuity of these Earnings?"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlpIncome
        '
        Me.tlpIncome.ColumnCount = 7
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125.0!))
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncome.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncome.Controls.Add(Me.btnQuestion2Add, 6, 0)
        Me.tlpIncome.Controls.Add(Me.ckbxQuestion2No, 5, 0)
        Me.tlpIncome.Controls.Add(Me.btnQuestion1Add, 2, 0)
        Me.tlpIncome.Controls.Add(Me.ckbxQuestion1No, 1, 0)
        Me.tlpIncome.Controls.Add(Me.lblQuestion2, 4, 0)
        Me.tlpIncome.Controls.Add(Me.lblQuestion1, 0, 0)
        Me.tlpIncome.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpIncome.Location = New System.Drawing.Point(7, 47)
        Me.tlpIncome.Name = "tlpIncome"
        Me.tlpIncome.RowCount = 1
        Me.tlpIncome.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpIncome.Size = New System.Drawing.Size(899, 29)
        Me.tlpIncome.TabIndex = 146
        '
        'btnQuestion2Add
        '
        Me.btnQuestion2Add.Location = New System.Drawing.Point(854, 3)
        Me.btnQuestion2Add.Name = "btnQuestion2Add"
        Me.btnQuestion2Add.Size = New System.Drawing.Size(42, 23)
        Me.btnQuestion2Add.TabIndex = 6
        Me.btnQuestion2Add.Text = "Add"
        Me.btnQuestion2Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion2No
        '
        Me.ckbxQuestion2No.AutoSize = True
        Me.ckbxQuestion2No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion2No.Location = New System.Drawing.Point(810, 3)
        Me.ckbxQuestion2No.Name = "ckbxQuestion2No"
        Me.ckbxQuestion2No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion2No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion2No.TabIndex = 5
        Me.ckbxQuestion2No.Text = "no"
        Me.ckbxQuestion2No.UseVisualStyleBackColor = True
        '
        'btnQuestion1Add
        '
        Me.btnQuestion1Add.Location = New System.Drawing.Point(252, 3)
        Me.btnQuestion1Add.Name = "btnQuestion1Add"
        Me.btnQuestion1Add.Size = New System.Drawing.Size(45, 23)
        Me.btnQuestion1Add.TabIndex = 4
        Me.btnQuestion1Add.Text = "Add"
        Me.btnQuestion1Add.UseVisualStyleBackColor = True
        '
        'ckbxQuestion1No
        '
        Me.ckbxQuestion1No.AutoSize = True
        Me.ckbxQuestion1No.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxQuestion1No.Location = New System.Drawing.Point(208, 3)
        Me.ckbxQuestion1No.Name = "ckbxQuestion1No"
        Me.ckbxQuestion1No.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxQuestion1No.Size = New System.Drawing.Size(38, 23)
        Me.ckbxQuestion1No.TabIndex = 3
        Me.ckbxQuestion1No.Text = "no"
        Me.ckbxQuestion1No.UseVisualStyleBackColor = True
        '
        'lblQuestion2
        '
        Me.lblQuestion2.AutoSize = True
        Me.lblQuestion2.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion2.Location = New System.Drawing.Point(685, 0)
        Me.lblQuestion2.Name = "lblQuestion2"
        Me.lblQuestion2.Size = New System.Drawing.Size(98, 29)
        Me.lblQuestion2.TabIndex = 0
        Me.lblQuestion2.Text = "Any Other Income?"
        Me.lblQuestion2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQuestion1
        '
        Me.lblQuestion1.AutoSize = True
        Me.lblQuestion1.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblQuestion1.Location = New System.Drawing.Point(3, 0)
        Me.lblQuestion1.Name = "lblQuestion1"
        Me.lblQuestion1.Size = New System.Drawing.Size(126, 29)
        Me.lblQuestion1.TabIndex = 0
        Me.lblQuestion1.Text = "Is Partner's pay relevant?"
        Me.lblQuestion1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlpIncomeStated
        '
        Me.tlpIncomeStated.ColumnCount = 5
        Me.tlpIncomeStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.tlpIncomeStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncomeStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncomeStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpIncomeStated.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpIncomeStated.Controls.Add(Me.lblNettIncomeStated, 0, 0)
        Me.tlpIncomeStated.Controls.Add(Me.lblIfIncomeNotStated, 4, 0)
        Me.tlpIncomeStated.Controls.Add(Me.txtbxNetIncomeStatedValue, 1, 0)
        Me.tlpIncomeStated.Controls.Add(Me.btnIncomeStatedAdd, 3, 0)
        Me.tlpIncomeStated.Controls.Add(Me.lblOr, 2, 0)
        Me.tlpIncomeStated.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpIncomeStated.Location = New System.Drawing.Point(7, 13)
        Me.tlpIncomeStated.Name = "tlpIncomeStated"
        Me.tlpIncomeStated.RowCount = 1
        Me.tlpIncomeStated.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpIncomeStated.Size = New System.Drawing.Size(899, 33)
        Me.tlpIncomeStated.TabIndex = 145
        '
        'lblNettIncomeStated
        '
        Me.lblNettIncomeStated.AutoSize = True
        Me.lblNettIncomeStated.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblNettIncomeStated.Location = New System.Drawing.Point(3, 0)
        Me.lblNettIncomeStated.Name = "lblNettIncomeStated"
        Me.lblNettIncomeStated.Size = New System.Drawing.Size(169, 33)
        Me.lblNettIncomeStated.TabIndex = 102
        Me.lblNettIncomeStated.Text = "Net Income Stated on Application:"
        Me.lblNettIncomeStated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIfIncomeNotStated
        '
        Me.lblIfIncomeNotStated.AutoSize = True
        Me.lblIfIncomeNotStated.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblIfIncomeNotStated.Location = New System.Drawing.Point(361, 0)
        Me.lblIfIncomeNotStated.Name = "lblIfIncomeNotStated"
        Me.lblIfIncomeNotStated.Size = New System.Drawing.Size(65, 33)
        Me.lblIfIncomeNotStated.TabIndex = 144
        Me.lblIfIncomeNotStated.Text = "if not stated."
        Me.lblIfIncomeNotStated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxNetIncomeStatedValue
        '
        Me.txtbxNetIncomeStatedValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "NetIncomeStated", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxNetIncomeStatedValue.Location = New System.Drawing.Point(208, 3)
        Me.txtbxNetIncomeStatedValue.Name = "txtbxNetIncomeStatedValue"
        Me.txtbxNetIncomeStatedValue.Size = New System.Drawing.Size(77, 20)
        Me.txtbxNetIncomeStatedValue.TabIndex = 1
        '
        'btnIncomeStatedAdd
        '
        Me.btnIncomeStatedAdd.Location = New System.Drawing.Point(313, 3)
        Me.btnIncomeStatedAdd.Name = "btnIncomeStatedAdd"
        Me.btnIncomeStatedAdd.Size = New System.Drawing.Size(42, 23)
        Me.btnIncomeStatedAdd.TabIndex = 2
        Me.btnIncomeStatedAdd.Text = "Add"
        Me.btnIncomeStatedAdd.UseVisualStyleBackColor = True
        '
        'lblOr
        '
        Me.lblOr.AutoSize = True
        Me.lblOr.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblOr.Location = New System.Drawing.Point(291, 0)
        Me.lblOr.Name = "lblOr"
        Me.lblOr.Size = New System.Drawing.Size(16, 33)
        Me.lblOr.TabIndex = 142
        Me.lblOr.Text = "or"
        Me.lblOr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gpbxBudgetSummary
        '
        Me.gpbxBudgetSummary.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxBudgetSummary.Controls.Add(Me.PanelBudgetSummary)
        Me.gpbxBudgetSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxBudgetSummary.Location = New System.Drawing.Point(12, 522)
        Me.gpbxBudgetSummary.Name = "gpbxBudgetSummary"
        Me.gpbxBudgetSummary.Size = New System.Drawing.Size(912, 210)
        Me.gpbxBudgetSummary.TabIndex = 5
        Me.gpbxBudgetSummary.TabStop = False
        Me.gpbxBudgetSummary.Text = "Budget Items (monthly average)"
        '
        'PanelBudgetSummary
        '
        Me.PanelBudgetSummary.Controls.Add(Me.BudgetDataGridView)
        Me.PanelBudgetSummary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelBudgetSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelBudgetSummary.Location = New System.Drawing.Point(3, 16)
        Me.PanelBudgetSummary.Name = "PanelBudgetSummary"
        Me.PanelBudgetSummary.Size = New System.Drawing.Size(906, 191)
        Me.PanelBudgetSummary.TabIndex = 1
        '
        'BudgetDataGridView
        '
        Me.BudgetDataGridView.AllowUserToAddRows = False
        Me.BudgetDataGridView.AllowUserToDeleteRows = False
        Me.BudgetDataGridView.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Menu
        Me.BudgetDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.BudgetDataGridView.AutoGenerateColumns = False
        Me.BudgetDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.BudgetDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.BudgetDataGridView.DataSource = Me.BudgetBindingSource
        Me.BudgetDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BudgetDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.BudgetDataGridView.Name = "BudgetDataGridView"
        Me.BudgetDataGridView.ReadOnly = True
        Me.BudgetDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.BudgetDataGridView.Size = New System.Drawing.Size(906, 191)
        Me.BudgetDataGridView.TabIndex = 0
        '
        'BudgetBindingSource
        '
        Me.BudgetBindingSource.DataMember = "Budget"
        Me.BudgetBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblOtherExplain
        '
        Me.lblOtherExplain.AutoSize = True
        Me.lblOtherExplain.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherExplain.Location = New System.Drawing.Point(29, 764)
        Me.lblOtherExplain.Name = "lblOtherExplain"
        Me.lblOtherExplain.Size = New System.Drawing.Size(207, 13)
        Me.lblOtherExplain.TabIndex = 188
        Me.lblOtherExplain.Text = "Budget comments and explanations"
        '
        'lblMarginValue
        '
        Me.lblMarginValue.AutoSize = True
        Me.lblMarginValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarginValue.Location = New System.Drawing.Point(604, 738)
        Me.lblMarginValue.Name = "lblMarginValue"
        Me.lblMarginValue.Size = New System.Drawing.Size(32, 13)
        Me.lblMarginValue.TabIndex = 187
        Me.lblMarginValue.Text = "0.00"
        '
        'lblMargin
        '
        Me.lblMargin.AutoSize = True
        Me.lblMargin.Location = New System.Drawing.Point(556, 738)
        Me.lblMargin.Name = "lblMargin"
        Me.lblMargin.Size = New System.Drawing.Size(42, 13)
        Me.lblMargin.TabIndex = 186
        Me.lblMargin.Text = "Margin:"
        '
        'lblTotalExpensesValue
        '
        Me.lblTotalExpensesValue.AutoSize = True
        Me.lblTotalExpensesValue.Location = New System.Drawing.Point(450, 738)
        Me.lblTotalExpensesValue.Name = "lblTotalExpensesValue"
        Me.lblTotalExpensesValue.Size = New System.Drawing.Size(28, 13)
        Me.lblTotalExpensesValue.TabIndex = 185
        Me.lblTotalExpensesValue.Text = "0.00"
        '
        'lblTotalExpenses
        '
        Me.lblTotalExpenses.AutoSize = True
        Me.lblTotalExpenses.Location = New System.Drawing.Point(361, 738)
        Me.lblTotalExpenses.Name = "lblTotalExpenses"
        Me.lblTotalExpenses.Size = New System.Drawing.Size(83, 13)
        Me.lblTotalExpenses.TabIndex = 184
        Me.lblTotalExpenses.Text = "Total Expenses:"
        '
        'lblTotalIncomeValue
        '
        Me.lblTotalIncomeValue.AutoSize = True
        Me.lblTotalIncomeValue.Location = New System.Drawing.Point(298, 738)
        Me.lblTotalIncomeValue.Name = "lblTotalIncomeValue"
        Me.lblTotalIncomeValue.Size = New System.Drawing.Size(28, 13)
        Me.lblTotalIncomeValue.TabIndex = 183
        Me.lblTotalIncomeValue.Text = "0.00"
        '
        'lbltotalIncome
        '
        Me.lbltotalIncome.AutoSize = True
        Me.lbltotalIncome.Location = New System.Drawing.Point(220, 738)
        Me.lbltotalIncome.Name = "lbltotalIncome"
        Me.lbltotalIncome.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbltotalIncome.Size = New System.Drawing.Size(72, 13)
        Me.lbltotalIncome.TabIndex = 182
        Me.lbltotalIncome.Text = "Total Income:"
        '
        'lblContactAddress
        '
        Me.lblContactAddress.AutoSize = True
        Me.lblContactAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblContactAddress.Name = "lblContactAddress"
        Me.lblContactAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblContactAddress.TabIndex = 210
        Me.lblContactAddress.Text = "ClientAddress"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 209
        Me.lblAddress.Text = "Address:"
        '
        'lblContactName
        '
        Me.lblContactName.AutoSize = True
        Me.lblContactName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactName.Location = New System.Drawing.Point(15, 62)
        Me.lblContactName.Name = "lblContactName"
        Me.lblContactName.Size = New System.Drawing.Size(75, 13)
        Me.lblContactName.TabIndex = 208
        Me.lblContactName.Text = "Client Name"
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(59, 85)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(0, 13)
        Me.lblDealerName.TabIndex = 206
        '
        'lblContactNumber
        '
        Me.lblContactNumber.AutoSize = True
        Me.lblContactNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNumber.Location = New System.Drawing.Point(594, 62)
        Me.lblContactNumber.Name = "lblContactNumber"
        Me.lblContactNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblContactNumber.TabIndex = 205
        Me.lblContactNumber.Text = "Number"
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 204
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(299, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(190, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 203
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(492, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 202
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(187, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 200
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(155, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 199
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(92, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 198
        Me.lblEnquiryNumberValue.Text = "0000"
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 197
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(842, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 196
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnViewDocs)
        Me.Panel2.Controls.Add(Me.btnQRGList)
        Me.Panel2.Controls.Add(Me.btnDocs)
        Me.Panel2.Controls.Add(Me.btnSaveAndExit)
        Me.Panel2.Controls.Add(Me.btnFinish)
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Controls.Add(Me.btnBack)
        Me.Panel2.Controls.Add(Me.btnNext)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 959)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(944, 31)
        Me.Panel2.TabIndex = 177
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(34, 3)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 58
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnQRGList
        '
        Me.btnQRGList.Location = New System.Drawing.Point(106, 3)
        Me.btnQRGList.Name = "btnQRGList"
        Me.btnQRGList.Size = New System.Drawing.Size(75, 23)
        Me.btnQRGList.TabIndex = 29
        Me.btnQRGList.Text = "QRGuide"
        Me.btnQRGList.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(3, 3)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 57
        Me.btnDocs.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnDocs, "You can 'Drag and Drop' documents from Windows Explorer or your Desktop.")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(275, 3)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 34
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblCurrentLoanAmt)
        Me.Panel3.Controls.Add(Me.lblCurrentLoan)
        Me.Panel3.Controls.Add(Me.gpbxManager)
        Me.Panel3.Controls.Add(Me.gpbxStatus)
        Me.Panel3.Controls.Add(Me.MenuStrip1)
        Me.Panel3.Controls.Add(Me.lblContactAddress)
        Me.Panel3.Controls.Add(Me.lblEnquiryCode)
        Me.Panel3.Controls.Add(Me.lblAddress)
        Me.Panel3.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Panel3.Controls.Add(Me.lblContactName)
        Me.Panel3.Controls.Add(Me.lblDateAndTime)
        Me.Panel3.Controls.Add(Me.lblDateAndTimeValue)
        Me.Panel3.Controls.Add(Me.lblDealerName)
        Me.Panel3.Controls.Add(Me.lblDealer)
        Me.Panel3.Controls.Add(Me.lblContactNumber)
        Me.Panel3.Controls.Add(Me.lblLoanAmount)
        Me.Panel3.Controls.Add(Me.lblContact)
        Me.Panel3.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(944, 102)
        Me.Panel3.TabIndex = 178
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(639, 39)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmt.TabIndex = 255
        Me.lblCurrentLoanAmt.Text = "0.00"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'lblCurrentLoan
        '
        Me.lblCurrentLoan.AutoSize = True
        Me.lblCurrentLoan.Location = New System.Drawing.Point(567, 39)
        Me.lblCurrentLoan.Name = "lblCurrentLoan"
        Me.lblCurrentLoan.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrentLoan.TabIndex = 254
        Me.lblCurrentLoan.Text = "Current Loan"
        Me.lblCurrentLoan.Visible = False
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(740, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 251
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.WWWToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem, Me.MCToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 250
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEnquiryToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Enabled = False
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEnquiryToolStripMenuItem
        '
        Me.EndEnquiryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeclinedToolStripMenuItem, Me.WithdrawnToolStripMenuItem})
        Me.EndEnquiryToolStripMenuItem.Name = "EndEnquiryToolStripMenuItem"
        Me.EndEnquiryToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEnquiryToolStripMenuItem.Text = "End Enquiry"
        Me.EndEnquiryToolStripMenuItem.Visible = False
        '
        'DeclinedToolStripMenuItem
        '
        Me.DeclinedToolStripMenuItem.Name = "DeclinedToolStripMenuItem"
        Me.DeclinedToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DeclinedToolStripMenuItem.Text = "Declined"
        '
        'WithdrawnToolStripMenuItem
        '
        Me.WithdrawnToolStripMenuItem.Name = "WithdrawnToolStripMenuItem"
        Me.WithdrawnToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.WithdrawnToolStripMenuItem.Text = "Withdrawn"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem, Me.DealersToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "TrueTrack Names"
        '
        'DealersToolStripMenuItem
        '
        Me.DealersToolStripMenuItem.Name = "DealersToolStripMenuItem"
        Me.DealersToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.DealersToolStripMenuItem.Text = "Dealers"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1, Me.EmailApprovalToolStripMenuItem})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(154, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'EmailApprovalToolStripMenuItem
        '
        Me.EmailApprovalToolStripMenuItem.Name = "EmailApprovalToolStripMenuItem"
        Me.EmailApprovalToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EmailApprovalToolStripMenuItem.Text = "Email Approval"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "YFL Website"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Website - Ctrl+Alt+W"
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search TrueTrack Names - Ctrl+Alt+M"
        '
        'MCToolStripMenuItem
        '
        Me.MCToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.MCToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.OAC16
        Me.MCToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MCToolStripMenuItem.Name = "MCToolStripMenuItem"
        Me.MCToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.MCToolStripMenuItem.Text = "MembersCentre Link"
        Me.MCToolStripMenuItem.ToolTipText = "OAC Link"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 85)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 201
        Me.lblDealer.Text = "Dealer:"
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Me.BudgetTableAdapter
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'BudgetTableAdapter
        '
        Me.BudgetTableAdapter.ClearBeforeFill = True
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "BudgetId"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn1.HeaderText = "BudgetId"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.HeaderText = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 250
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "IncomeValue"
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn4.HeaderText = "IncomeValue"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "ExpenseValue"
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn5.HeaderText = "ExpenseValue"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Comments"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Comments"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'txtbxOtherExplain
        '
        Me.txtbxOtherExplain.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "BudgetExplanation", True))
        Me.txtbxOtherExplain.IsReadOnly = False
        Me.txtbxOtherExplain.Location = New System.Drawing.Point(15, 781)
        Me.txtbxOtherExplain.MaxLength = 0
        Me.txtbxOtherExplain.MultiLine = True
        Me.txtbxOtherExplain.Name = "txtbxOtherExplain"
        Me.txtbxOtherExplain.SelectedText = ""
        Me.txtbxOtherExplain.SelectionLength = 0
        Me.txtbxOtherExplain.SelectionStart = 0
        Me.txtbxOtherExplain.Size = New System.Drawing.Size(906, 90)
        Me.txtbxOtherExplain.TabIndex = 194
        Me.txtbxOtherExplain.WordWrap = True
        Me.txtbxOtherExplain.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxOvertime
        '
        Me.tlpOvertime.SetColumnSpan(Me.txtbxOvertime, 3)
        Me.txtbxOvertime.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "OvertimeText", True))
        Me.txtbxOvertime.IsReadOnly = False
        Me.txtbxOvertime.Location = New System.Drawing.Point(322, 35)
        Me.txtbxOvertime.MaxLength = 0
        Me.txtbxOvertime.MultiLine = True
        Me.txtbxOvertime.Name = "txtbxOvertime"
        Me.txtbxOvertime.SelectedText = ""
        Me.txtbxOvertime.SelectionLength = 0
        Me.txtbxOvertime.SelectionStart = 0
        Me.txtbxOvertime.Size = New System.Drawing.Size(574, 36)
        Me.txtbxOvertime.TabIndex = 9
        Me.txtbxOvertime.WordWrap = True
        Me.txtbxOvertime.Child = New System.Windows.Controls.TextBox()
        '
        'WkShtWizForm4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.PrelimFormSize
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "PrelimFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "PrelimFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.PrelimFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "WkShtWizForm4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Due Diligence Budget"
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxDependency.ResumeLayout(False)
        Me.tlpDependency.ResumeLayout(False)
        Me.tlpDependency.PerformLayout()
        Me.gpbxExpenses.ResumeLayout(False)
        Me.tlpExpenses.ResumeLayout(False)
        Me.tlpExpenses.PerformLayout()
        Me.tlpExpenseStated.ResumeLayout(False)
        Me.tlpExpenseStated.PerformLayout()
        Me.gpbxCalculator.ResumeLayout(False)
        Me.tlpCalculator.ResumeLayout(False)
        Me.tlpCalculator.PerformLayout()
        Me.gpbxIncome.ResumeLayout(False)
        Me.tlpOvertime.ResumeLayout(False)
        Me.tlpOvertime.PerformLayout()
        Me.tlpIncome.ResumeLayout(False)
        Me.tlpIncome.PerformLayout()
        Me.tlpIncomeStated.ResumeLayout(False)
        Me.tlpIncomeStated.PerformLayout()
        Me.gpbxBudgetSummary.ResumeLayout(False)
        Me.PanelBudgetSummary.ResumeLayout(False)
        CType(Me.BudgetDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BudgetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents ClientTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ClientTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BudgetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BudgetTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnDeleteSelectedRow As System.Windows.Forms.Button
    Friend WithEvents txtbxNumChildren As System.Windows.Forms.TextBox
    Friend WithEvents lblChildren As System.Windows.Forms.Label
    Friend WithEvents lblTotalDep As System.Windows.Forms.Label
    Friend WithEvents lblTotalDependValue As System.Windows.Forms.Label
    Friend WithEvents btnDependSingle As System.Windows.Forms.Button
    Friend WithEvents btnDependCouple As System.Windows.Forms.Button
    Friend WithEvents lblDependClient As System.Windows.Forms.Label
    Friend WithEvents lblIfExpensesNotStated As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion12No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion12Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion12 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion11No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion11Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion11 As System.Windows.Forms.Label
    Friend WithEvents btnExpensesStated As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtbxNetExpensesStatedValue As System.Windows.Forms.TextBox
    Friend WithEvents lblNettExpensesStated As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion10No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion10Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion10 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion3No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion3Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion3 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion9No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion9Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion9 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion4No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion4Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion4 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion8No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion8Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion8 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion5No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion5Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion5 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion7No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion7Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion7 As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion6No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion6Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion6 As System.Windows.Forms.Label
    Friend WithEvents gpbxIncome As System.Windows.Forms.GroupBox
    Friend WithEvents lblIfIncomeNotStated As System.Windows.Forms.Label
    Friend WithEvents btnIncomeStatedAdd As System.Windows.Forms.Button
    Friend WithEvents dtpPSDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPeriodStartDate As System.Windows.Forms.Label
    Friend WithEvents lblGross As System.Windows.Forms.Label
    Friend WithEvents txtbxGross As System.Windows.Forms.TextBox
    Friend WithEvents lblPAYE As System.Windows.Forms.Label
    Friend WithEvents txtbxPAYE As System.Windows.Forms.TextBox
    Friend WithEvents lblAvYTDValue As System.Windows.Forms.Label
    Friend WithEvents lblDeductions As System.Windows.Forms.Label
    Friend WithEvents txtbxDeductions As System.Windows.Forms.TextBox
    Friend WithEvents dtpPEDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPEDate As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion2No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion2Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion2 As System.Windows.Forms.Label
    Friend WithEvents lblOr As System.Windows.Forms.Label
    Friend WithEvents ckbxQuestion1No As System.Windows.Forms.CheckBox
    Friend WithEvents btnQuestion1Add As System.Windows.Forms.Button
    Friend WithEvents lblQuestion1 As System.Windows.Forms.Label
    Friend WithEvents lblNettIncomeStated As System.Windows.Forms.Label
    Friend WithEvents txtbxNetIncomeStatedValue As System.Windows.Forms.TextBox
    Friend WithEvents gpbxBudgetSummary As System.Windows.Forms.GroupBox
    Friend WithEvents BudgetDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents lblOtherExplain As System.Windows.Forms.Label
    Friend WithEvents lblMarginValue As System.Windows.Forms.Label
    Friend WithEvents lblMargin As System.Windows.Forms.Label
    Friend WithEvents lblTotalExpensesValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalExpenses As System.Windows.Forms.Label
    Friend WithEvents lblTotalIncomeValue As System.Windows.Forms.Label
    Friend WithEvents lbltotalIncome As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents btnQRGList As System.Windows.Forms.Button
    Friend WithEvents lblContactAddress As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblContactName As System.Windows.Forms.Label
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblContactNumber As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents gpbxCalculator As System.Windows.Forms.GroupBox
    Friend WithEvents tlpCalculator As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblAvYTD As System.Windows.Forms.Label
    Friend WithEvents tlpIncomeStated As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpIncome As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gpbxExpenses As System.Windows.Forms.GroupBox
    Friend WithEvents tlpExpenseStated As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpExpenses As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gpbxDependency As System.Windows.Forms.GroupBox
    Friend WithEvents tlpDependency As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tlpOvertime As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblOvertime As System.Windows.Forms.Label
    Friend WithEvents rbOvertimeNo As System.Windows.Forms.RadioButton
    Friend WithEvents rbOvertimeYes As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents lblQuestion11Note As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEnquiryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeclinedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WithdrawnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoan As System.Windows.Forms.Label
    Friend WithEvents EmailApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DealersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents txtbxOvertime As AppWhShtB.SpellBox
    Friend WithEvents txtbxOtherExplain As AppWhShtB.SpellBox
    Friend WithEvents PanelBudgetSummary As System.Windows.Forms.Panel
End Class
