﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CompanyStabilityForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CompanyStabilityForm))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeclineToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WithdrawToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.lblClientAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblClientName = New System.Windows.Forms.Label()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblContactNumber = New System.Windows.Forms.Label()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnQRGList = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.gpbxOther = New System.Windows.Forms.GroupBox()
        Me.txtbxOtherComments = New System.Windows.Forms.TextBox()
        Me.gpbxCompanyTenancy = New System.Windows.Forms.GroupBox()
        Me.tlpCompanyTenancy = New System.Windows.Forms.TableLayoutPanel()
        Me.txtbxCompanyTenancySatis = New System.Windows.Forms.TextBox()
        Me.lblTenancyComments = New System.Windows.Forms.Label()
        Me.txtbxLengthTen = New System.Windows.Forms.TextBox()
        Me.lblLengthTen = New System.Windows.Forms.Label()
        Me.cmbxTenancy = New System.Windows.Forms.ComboBox()
        Me.lblTenancy = New System.Windows.Forms.Label()
        Me.gpbxCompanyGuarantors = New System.Windows.Forms.GroupBox()
        Me.tlpCompanyGuarantors = New System.Windows.Forms.TableLayoutPanel()
        Me.lblGuarantorTenancy = New System.Windows.Forms.Label()
        Me.txtbxAssets = New System.Windows.Forms.TextBox()
        Me.lblAssests = New System.Windows.Forms.Label()
        Me.lblGuarantor = New System.Windows.Forms.Label()
        Me.ckbxAppFilledOut = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtbxGuarantor = New System.Windows.Forms.TextBox()
        Me.ckbxGuarantorID = New System.Windows.Forms.CheckBox()
        Me.lblGuarantorsTenancy = New System.Windows.Forms.Label()
        Me.cmbxCompanyGuarantorTenType = New System.Windows.Forms.ComboBox()
        Me.TypeOfTenancyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtbxGuarantorTenancy = New System.Windows.Forms.TextBox()
        Me.gpbxCompanyInfo = New System.Windows.Forms.GroupBox()
        Me.tlpCompanyInfo = New System.Windows.Forms.TableLayoutPanel()
        Me.ckbxAddresses = New System.Windows.Forms.CheckBox()
        Me.ckbxPreviousHistory = New System.Windows.Forms.CheckBox()
        Me.lblDirectorsNote = New System.Windows.Forms.Label()
        Me.ckbxDirectors = New System.Windows.Forms.CheckBox()
        Me.lblShareholdersNote = New System.Windows.Forms.Label()
        Me.ckbxShareholders = New System.Windows.Forms.CheckBox()
        Me.ckbxCapital = New System.Windows.Forms.CheckBox()
        Me.ckbxReturns = New System.Windows.Forms.CheckBox()
        Me.lblCompanyExtract = New System.Windows.Forms.Label()
        Me.ckbxRegistered = New System.Windows.Forms.CheckBox()
        Me.txtbxShareholders = New System.Windows.Forms.TextBox()
        Me.txtbxDirectors = New System.Windows.Forms.TextBox()
        Me.lblHistoryComments = New System.Windows.Forms.Label()
        Me.txtbxPreviousHistory = New System.Windows.Forms.TextBox()
        Me.lblCompanyAddresses = New System.Windows.Forms.Label()
        Me.txtbxAddresses = New System.Windows.Forms.TextBox()
        Me.gpbxReferences = New System.Windows.Forms.GroupBox()
        Me.gpbxRef2 = New System.Windows.Forms.GroupBox()
        Me.tlpRef2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblSpend2 = New System.Windows.Forms.Label()
        Me.lblCurrentAcct2 = New System.Windows.Forms.Label()
        Me.txtbxCurrentAcct2 = New System.Windows.Forms.TextBox()
        Me.lblPayTime2 = New System.Windows.Forms.Label()
        Me.lblComments2 = New System.Windows.Forms.Label()
        Me.txtbxPayTime2 = New System.Windows.Forms.TextBox()
        Me.lblRef2Name = New System.Windows.Forms.Label()
        Me.txtbxRef2Name = New System.Windows.Forms.TextBox()
        Me.txtbxSpend2 = New System.Windows.Forms.TextBox()
        Me.txtbxOther2 = New System.Windows.Forms.TextBox()
        Me.gpbxRef1 = New System.Windows.Forms.GroupBox()
        Me.tlpRef1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblSpend1 = New System.Windows.Forms.Label()
        Me.lblRef1Name = New System.Windows.Forms.Label()
        Me.txtbxRef1Name = New System.Windows.Forms.TextBox()
        Me.lblCurrentAcct1 = New System.Windows.Forms.Label()
        Me.txtbxCurrentAcct1 = New System.Windows.Forms.TextBox()
        Me.lblPayTime1 = New System.Windows.Forms.Label()
        Me.lblComments1 = New System.Windows.Forms.Label()
        Me.txtbxPayTime1 = New System.Windows.Forms.TextBox()
        Me.txtbxSpend1 = New System.Windows.Forms.TextBox()
        Me.txtbxOther1 = New System.Windows.Forms.TextBox()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.ClientTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ClientTableAdapter()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.ClientBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeOfTenancyTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfTenancyTableAdapter()
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.lblCurrentLoan = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.gpbxManager.SuspendLayout()
        Me.gpbxStatus.SuspendLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.gpbxOther.SuspendLayout()
        Me.gpbxCompanyTenancy.SuspendLayout()
        Me.tlpCompanyTenancy.SuspendLayout()
        Me.gpbxCompanyGuarantors.SuspendLayout()
        Me.tlpCompanyGuarantors.SuspendLayout()
        CType(Me.TypeOfTenancyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxCompanyInfo.SuspendLayout()
        Me.tlpCompanyInfo.SuspendLayout()
        Me.gpbxReferences.SuspendLayout()
        Me.gpbxRef2.SuspendLayout()
        Me.tlpRef2.SuspendLayout()
        Me.gpbxRef1.SuspendLayout()
        Me.tlpRef1.SuspendLayout()
        CType(Me.ClientBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.WWWToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 249
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Enabled = False
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEToolStripMenuItem
        '
        Me.EndEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeclineToolStripMenuItem, Me.WithdrawToolStripMenuItem})
        Me.EndEToolStripMenuItem.Name = "EndEToolStripMenuItem"
        Me.EndEToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEToolStripMenuItem.Text = "End Enquiry"
        Me.EndEToolStripMenuItem.Visible = False
        '
        'DeclineToolStripMenuItem
        '
        Me.DeclineToolStripMenuItem.Name = "DeclineToolStripMenuItem"
        Me.DeclineToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DeclineToolStripMenuItem.Text = "Declined"
        '
        'WithdrawToolStripMenuItem
        '
        Me.WithdrawToolStripMenuItem.Name = "WithdrawToolStripMenuItem"
        Me.WithdrawToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.WithdrawToolStripMenuItem.Text = "Withdrawn"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "Enquiries Names"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(153, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "YFL Website"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Website - Ctrl+Alt+W"
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeyDisplayString = "CTRL+ALT+M"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search TrueTrack Names - Ctrl+Alt+M"
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(740, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 252
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(842, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 251
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblClientAddress
        '
        Me.lblClientAddress.AutoSize = True
        Me.lblClientAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblClientAddress.Name = "lblClientAddress"
        Me.lblClientAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblClientAddress.TabIndex = 278
        Me.lblClientAddress.Text = "ClientAddress"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 277
        Me.lblAddress.Text = "Address:"
        '
        'lblClientName
        '
        Me.lblClientName.AutoSize = True
        Me.lblClientName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientName.Location = New System.Drawing.Point(15, 62)
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.Size = New System.Drawing.Size(75, 13)
        Me.lblClientName.TabIndex = 276
        Me.lblClientName.Text = "Client Name"
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(59, 82)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(0, 13)
        Me.lblDealerName.TabIndex = 275
        '
        'lblContactNumber
        '
        Me.lblContactNumber.AutoSize = True
        Me.lblContactNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNumber.Location = New System.Drawing.Point(594, 62)
        Me.lblContactNumber.Name = "lblContactNumber"
        Me.lblContactNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblContactNumber.TabIndex = 274
        Me.lblContactNumber.Text = "Number"
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 273
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.AutoSize = True
        Me.lblTypeLoanEnquiry.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanReason", True))
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(347, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(90, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 272
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(483, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 271
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 82)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 270
        Me.lblDealer.Text = "Dealer:"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(206, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 269
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(172, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 268
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(94, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(13, 13)
        Me.lblEnquiryNumberValue.TabIndex = 267
        Me.lblEnquiryNumberValue.Text = "0"
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 266
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 990)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(944, 22)
        Me.StatusStrip1.TabIndex = 281
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Ready"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnViewDocs)
        Me.Panel1.Controls.Add(Me.btnQRGList)
        Me.Panel1.Controls.Add(Me.btnDocs)
        Me.Panel1.Controls.Add(Me.btnSaveAndExit)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnBack)
        Me.Panel1.Controls.Add(Me.btnNext)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 961)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 29)
        Me.Panel1.TabIndex = 282
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(34, 3)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 28
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnQRGList
        '
        Me.btnQRGList.Location = New System.Drawing.Point(106, 3)
        Me.btnQRGList.Name = "btnQRGList"
        Me.btnQRGList.Size = New System.Drawing.Size(75, 23)
        Me.btnQRGList.TabIndex = 27
        Me.btnQRGList.Text = "QRGuide"
        Me.btnQRGList.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(3, 3)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 26
        Me.ToolTip1.SetToolTip(Me.btnDocs, "You can 'Drag and Drop' documents from Windows Explorer or your Desktop.")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(275, 3)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 25
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'btnFinish
        '
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(617, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.TabIndex = 17
        Me.btnFinish.Text = "Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(374, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(455, 3)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 15
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(536, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 16
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.gpbxOther)
        Me.Panel2.Controls.Add(Me.gpbxCompanyTenancy)
        Me.Panel2.Controls.Add(Me.gpbxCompanyGuarantors)
        Me.Panel2.Controls.Add(Me.gpbxCompanyInfo)
        Me.Panel2.Controls.Add(Me.gpbxReferences)
        Me.Panel2.Location = New System.Drawing.Point(0, 104)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(944, 856)
        Me.Panel2.TabIndex = 283
        '
        'gpbxOther
        '
        Me.gpbxOther.Controls.Add(Me.txtbxOtherComments)
        Me.gpbxOther.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxOther.Location = New System.Drawing.Point(12, 732)
        Me.gpbxOther.Name = "gpbxOther"
        Me.gpbxOther.Size = New System.Drawing.Size(914, 95)
        Me.gpbxOther.TabIndex = 292
        Me.gpbxOther.TabStop = False
        Me.gpbxOther.Text = "Other Comments"
        '
        'txtbxOtherComments
        '
        Me.txtbxOtherComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyOtherNotes", True))
        Me.txtbxOtherComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxOtherComments.Location = New System.Drawing.Point(6, 19)
        Me.txtbxOtherComments.MaxLength = 1022
        Me.txtbxOtherComments.Multiline = True
        Me.txtbxOtherComments.Name = "txtbxOtherComments"
        Me.txtbxOtherComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxOtherComments.Size = New System.Drawing.Size(902, 69)
        Me.txtbxOtherComments.TabIndex = 7
        '
        'gpbxCompanyTenancy
        '
        Me.gpbxCompanyTenancy.Controls.Add(Me.tlpCompanyTenancy)
        Me.gpbxCompanyTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCompanyTenancy.Location = New System.Drawing.Point(12, 609)
        Me.gpbxCompanyTenancy.Name = "gpbxCompanyTenancy"
        Me.gpbxCompanyTenancy.Size = New System.Drawing.Size(914, 117)
        Me.gpbxCompanyTenancy.TabIndex = 291
        Me.gpbxCompanyTenancy.TabStop = False
        Me.gpbxCompanyTenancy.Text = "Company Tenancy"
        '
        'tlpCompanyTenancy
        '
        Me.tlpCompanyTenancy.ColumnCount = 3
        Me.tlpCompanyTenancy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpCompanyTenancy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyTenancy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyTenancy.Controls.Add(Me.txtbxCompanyTenancySatis, 1, 2)
        Me.tlpCompanyTenancy.Controls.Add(Me.lblTenancyComments, 0, 2)
        Me.tlpCompanyTenancy.Controls.Add(Me.txtbxLengthTen, 1, 1)
        Me.tlpCompanyTenancy.Controls.Add(Me.lblLengthTen, 0, 1)
        Me.tlpCompanyTenancy.Controls.Add(Me.cmbxTenancy, 1, 0)
        Me.tlpCompanyTenancy.Controls.Add(Me.lblTenancy, 0, 0)
        Me.tlpCompanyTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpCompanyTenancy.Location = New System.Drawing.Point(6, 19)
        Me.tlpCompanyTenancy.Name = "tlpCompanyTenancy"
        Me.tlpCompanyTenancy.RowCount = 3
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpCompanyTenancy.Size = New System.Drawing.Size(902, 94)
        Me.tlpCompanyTenancy.TabIndex = 0
        '
        'txtbxCompanyTenancySatis
        '
        Me.tlpCompanyTenancy.SetColumnSpan(Me.txtbxCompanyTenancySatis, 2)
        Me.txtbxCompanyTenancySatis.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyTenancyNotes", True))
        Me.txtbxCompanyTenancySatis.Location = New System.Drawing.Point(303, 53)
        Me.txtbxCompanyTenancySatis.MaxLength = 1022
        Me.txtbxCompanyTenancySatis.Multiline = True
        Me.txtbxCompanyTenancySatis.Name = "txtbxCompanyTenancySatis"
        Me.txtbxCompanyTenancySatis.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxCompanyTenancySatis.Size = New System.Drawing.Size(596, 36)
        Me.txtbxCompanyTenancySatis.TabIndex = 5
        '
        'lblTenancyComments
        '
        Me.lblTenancyComments.AutoSize = True
        Me.lblTenancyComments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTenancyComments.Location = New System.Drawing.Point(3, 50)
        Me.lblTenancyComments.Name = "lblTenancyComments"
        Me.lblTenancyComments.Size = New System.Drawing.Size(294, 44)
        Me.lblTenancyComments.TabIndex = 5
        Me.lblTenancyComments.Text = "This is satisfactory for the term of the loan because ..."
        Me.lblTenancyComments.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxLengthTen
        '
        Me.txtbxLengthTen.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyTenancyLength", True))
        Me.txtbxLengthTen.Location = New System.Drawing.Point(303, 28)
        Me.txtbxLengthTen.MaxLength = 20
        Me.txtbxLengthTen.Name = "txtbxLengthTen"
        Me.txtbxLengthTen.Size = New System.Drawing.Size(295, 20)
        Me.txtbxLengthTen.TabIndex = 3
        '
        'lblLengthTen
        '
        Me.lblLengthTen.AutoSize = True
        Me.lblLengthTen.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblLengthTen.Location = New System.Drawing.Point(198, 25)
        Me.lblLengthTen.Name = "lblLengthTen"
        Me.lblLengthTen.Size = New System.Drawing.Size(99, 25)
        Me.lblLengthTen.TabIndex = 3
        Me.lblLengthTen.Text = "Length of tenancy?"
        Me.lblLengthTen.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbxTenancy
        '
        Me.cmbxTenancy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTenancy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTenancy.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyTenancy", True))
        Me.cmbxTenancy.FormattingEnabled = True
        Me.cmbxTenancy.Items.AddRange(New Object() {"Leased", "Sub-leased", "Rented", "Home based", "Owned"})
        Me.cmbxTenancy.Location = New System.Drawing.Point(303, 3)
        Me.cmbxTenancy.Name = "cmbxTenancy"
        Me.cmbxTenancy.Size = New System.Drawing.Size(173, 21)
        Me.cmbxTenancy.TabIndex = 2
        '
        'lblTenancy
        '
        Me.lblTenancy.AutoSize = True
        Me.lblTenancy.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTenancy.Location = New System.Drawing.Point(238, 0)
        Me.lblTenancy.Name = "lblTenancy"
        Me.lblTenancy.Size = New System.Drawing.Size(59, 25)
        Me.lblTenancy.TabIndex = 1
        Me.lblTenancy.Text = "Tenancy is"
        Me.lblTenancy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbxCompanyGuarantors
        '
        Me.gpbxCompanyGuarantors.Controls.Add(Me.tlpCompanyGuarantors)
        Me.gpbxCompanyGuarantors.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCompanyGuarantors.Location = New System.Drawing.Point(12, 379)
        Me.gpbxCompanyGuarantors.Name = "gpbxCompanyGuarantors"
        Me.gpbxCompanyGuarantors.Size = New System.Drawing.Size(914, 224)
        Me.gpbxCompanyGuarantors.TabIndex = 290
        Me.gpbxCompanyGuarantors.TabStop = False
        Me.gpbxCompanyGuarantors.Text = "Company Guarantors"
        '
        'tlpCompanyGuarantors
        '
        Me.tlpCompanyGuarantors.ColumnCount = 3
        Me.tlpCompanyGuarantors.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpCompanyGuarantors.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyGuarantors.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyGuarantors.Controls.Add(Me.lblGuarantorTenancy, 0, 5)
        Me.tlpCompanyGuarantors.Controls.Add(Me.txtbxAssets, 1, 3)
        Me.tlpCompanyGuarantors.Controls.Add(Me.lblAssests, 0, 3)
        Me.tlpCompanyGuarantors.Controls.Add(Me.lblGuarantor, 0, 2)
        Me.tlpCompanyGuarantors.Controls.Add(Me.ckbxAppFilledOut, 0, 1)
        Me.tlpCompanyGuarantors.Controls.Add(Me.Label1, 1, 0)
        Me.tlpCompanyGuarantors.Controls.Add(Me.txtbxGuarantor, 1, 2)
        Me.tlpCompanyGuarantors.Controls.Add(Me.ckbxGuarantorID, 0, 0)
        Me.tlpCompanyGuarantors.Controls.Add(Me.lblGuarantorsTenancy, 0, 4)
        Me.tlpCompanyGuarantors.Controls.Add(Me.cmbxCompanyGuarantorTenType, 1, 4)
        Me.tlpCompanyGuarantors.Controls.Add(Me.txtbxGuarantorTenancy, 1, 5)
        Me.tlpCompanyGuarantors.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpCompanyGuarantors.Location = New System.Drawing.Point(6, 19)
        Me.tlpCompanyGuarantors.Name = "tlpCompanyGuarantors"
        Me.tlpCompanyGuarantors.RowCount = 6
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyGuarantors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpCompanyGuarantors.Size = New System.Drawing.Size(902, 202)
        Me.tlpCompanyGuarantors.TabIndex = 0
        '
        'lblGuarantorTenancy
        '
        Me.lblGuarantorTenancy.AutoSize = True
        Me.lblGuarantorTenancy.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblGuarantorTenancy.Location = New System.Drawing.Point(33, 159)
        Me.lblGuarantorTenancy.Name = "lblGuarantorTenancy"
        Me.lblGuarantorTenancy.Size = New System.Drawing.Size(264, 43)
        Me.lblGuarantorTenancy.TabIndex = 15
        Me.lblGuarantorTenancy.Text = "Guarantor's tenancy is satisfactory and confirmed by ..."
        Me.lblGuarantorTenancy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxAssets
        '
        Me.tlpCompanyGuarantors.SetColumnSpan(Me.txtbxAssets, 2)
        Me.txtbxAssets.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyAssetNotes", True))
        Me.txtbxAssets.Location = New System.Drawing.Point(303, 95)
        Me.txtbxAssets.MaxLength = 1022
        Me.txtbxAssets.Multiline = True
        Me.txtbxAssets.Name = "txtbxAssets"
        Me.txtbxAssets.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxAssets.Size = New System.Drawing.Size(596, 36)
        Me.txtbxAssets.TabIndex = 11
        '
        'lblAssests
        '
        Me.lblAssests.AutoSize = True
        Me.lblAssests.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblAssests.Location = New System.Drawing.Point(54, 92)
        Me.lblAssests.Name = "lblAssests"
        Me.lblAssests.Size = New System.Drawing.Size(243, 42)
        Me.lblAssests.TabIndex = 12
        Me.lblAssests.Text = "They have sufficient assests to back up this loan. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Confirmed by ..."
        Me.lblAssests.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGuarantor
        '
        Me.lblGuarantor.AutoSize = True
        Me.lblGuarantor.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblGuarantor.Location = New System.Drawing.Point(53, 50)
        Me.lblGuarantor.Name = "lblGuarantor"
        Me.lblGuarantor.Size = New System.Drawing.Size(244, 42)
        Me.lblGuarantor.TabIndex = 11
        Me.lblGuarantor.Text = "They are satisfactory as the quarantor because ...."
        Me.lblGuarantor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ckbxAppFilledOut
        '
        Me.ckbxAppFilledOut.AutoSize = True
        Me.ckbxAppFilledOut.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyApplicationFilledOut", True))
        Me.ckbxAppFilledOut.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxAppFilledOut.Location = New System.Drawing.Point(171, 28)
        Me.ckbxAppFilledOut.Name = "ckbxAppFilledOut"
        Me.ckbxAppFilledOut.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxAppFilledOut.Size = New System.Drawing.Size(126, 19)
        Me.ckbxAppFilledOut.TabIndex = 10
        Me.ckbxAppFilledOut.Text = "?Application filled out"
        Me.ckbxAppFilledOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxAppFilledOut.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.tlpCompanyGuarantors.SetColumnSpan(Me.Label1, 2)
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label1.Location = New System.Drawing.Point(303, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "      ID to be saved under prefix ID2"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxGuarantor
        '
        Me.tlpCompanyGuarantors.SetColumnSpan(Me.txtbxGuarantor, 2)
        Me.txtbxGuarantor.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyGuarantorNotes", True))
        Me.txtbxGuarantor.Location = New System.Drawing.Point(303, 53)
        Me.txtbxGuarantor.MaxLength = 1022
        Me.txtbxGuarantor.Multiline = True
        Me.txtbxGuarantor.Name = "txtbxGuarantor"
        Me.txtbxGuarantor.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxGuarantor.Size = New System.Drawing.Size(596, 36)
        Me.txtbxGuarantor.TabIndex = 8
        '
        'ckbxGuarantorID
        '
        Me.ckbxGuarantorID.AutoSize = True
        Me.ckbxGuarantorID.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyGuarantorID", True))
        Me.ckbxGuarantorID.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxGuarantorID.Location = New System.Drawing.Point(205, 3)
        Me.ckbxGuarantorID.Name = "ckbxGuarantorID"
        Me.ckbxGuarantorID.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxGuarantorID.Size = New System.Drawing.Size(92, 19)
        Me.ckbxGuarantorID.TabIndex = 1
        Me.ckbxGuarantorID.Text = "?ID confirmed"
        Me.ckbxGuarantorID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxGuarantorID.UseVisualStyleBackColor = True
        '
        'lblGuarantorsTenancy
        '
        Me.lblGuarantorsTenancy.AutoSize = True
        Me.lblGuarantorsTenancy.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblGuarantorsTenancy.Location = New System.Drawing.Point(193, 134)
        Me.lblGuarantorsTenancy.Name = "lblGuarantorsTenancy"
        Me.lblGuarantorsTenancy.Size = New System.Drawing.Size(104, 25)
        Me.lblGuarantorsTenancy.TabIndex = 13
        Me.lblGuarantorsTenancy.Text = "Guarantors Tenancy"
        Me.lblGuarantorsTenancy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbxCompanyGuarantorTenType
        '
        Me.cmbxCompanyGuarantorTenType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "CompanyGuarantorTenancyType", True))
        Me.cmbxCompanyGuarantorTenType.DataSource = Me.TypeOfTenancyBindingSource
        Me.cmbxCompanyGuarantorTenType.DisplayMember = "TenancyType"
        Me.cmbxCompanyGuarantorTenType.FormattingEnabled = True
        Me.cmbxCompanyGuarantorTenType.Location = New System.Drawing.Point(303, 137)
        Me.cmbxCompanyGuarantorTenType.MaxLength = 30
        Me.cmbxCompanyGuarantorTenType.Name = "cmbxCompanyGuarantorTenType"
        Me.cmbxCompanyGuarantorTenType.Size = New System.Drawing.Size(173, 21)
        Me.cmbxCompanyGuarantorTenType.TabIndex = 14
        Me.cmbxCompanyGuarantorTenType.ValueMember = "TenancyType"
        '
        'TypeOfTenancyBindingSource
        '
        Me.TypeOfTenancyBindingSource.DataMember = "TypeOfTenancy"
        Me.TypeOfTenancyBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'txtbxGuarantorTenancy
        '
        Me.tlpCompanyGuarantors.SetColumnSpan(Me.txtbxGuarantorTenancy, 2)
        Me.txtbxGuarantorTenancy.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyGuarantorTenancy", True))
        Me.txtbxGuarantorTenancy.Location = New System.Drawing.Point(303, 162)
        Me.txtbxGuarantorTenancy.MaxLength = 1022
        Me.txtbxGuarantorTenancy.Multiline = True
        Me.txtbxGuarantorTenancy.Name = "txtbxGuarantorTenancy"
        Me.txtbxGuarantorTenancy.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxGuarantorTenancy.Size = New System.Drawing.Size(596, 36)
        Me.txtbxGuarantorTenancy.TabIndex = 13
        '
        'gpbxCompanyInfo
        '
        Me.gpbxCompanyInfo.Controls.Add(Me.tlpCompanyInfo)
        Me.gpbxCompanyInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCompanyInfo.Location = New System.Drawing.Point(12, 3)
        Me.gpbxCompanyInfo.Name = "gpbxCompanyInfo"
        Me.gpbxCompanyInfo.Size = New System.Drawing.Size(914, 370)
        Me.gpbxCompanyInfo.TabIndex = 289
        Me.gpbxCompanyInfo.TabStop = False
        Me.gpbxCompanyInfo.Text = "Company Information"
        '
        'tlpCompanyInfo
        '
        Me.tlpCompanyInfo.ColumnCount = 3
        Me.tlpCompanyInfo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpCompanyInfo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyInfo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxAddresses, 0, 9)
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxPreviousHistory, 0, 7)
        Me.tlpCompanyInfo.Controls.Add(Me.lblDirectorsNote, 0, 6)
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxDirectors, 0, 5)
        Me.tlpCompanyInfo.Controls.Add(Me.lblShareholdersNote, 0, 4)
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxShareholders, 0, 3)
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxCapital, 0, 2)
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxReturns, 0, 1)
        Me.tlpCompanyInfo.Controls.Add(Me.lblCompanyExtract, 1, 0)
        Me.tlpCompanyInfo.Controls.Add(Me.ckbxRegistered, 0, 0)
        Me.tlpCompanyInfo.Controls.Add(Me.txtbxShareholders, 1, 4)
        Me.tlpCompanyInfo.Controls.Add(Me.txtbxDirectors, 1, 6)
        Me.tlpCompanyInfo.Controls.Add(Me.lblHistoryComments, 0, 8)
        Me.tlpCompanyInfo.Controls.Add(Me.txtbxPreviousHistory, 1, 8)
        Me.tlpCompanyInfo.Controls.Add(Me.lblCompanyAddresses, 0, 10)
        Me.tlpCompanyInfo.Controls.Add(Me.txtbxAddresses, 1, 10)
        Me.tlpCompanyInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpCompanyInfo.Location = New System.Drawing.Point(6, 19)
        Me.tlpCompanyInfo.Name = "tlpCompanyInfo"
        Me.tlpCompanyInfo.RowCount = 11
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyInfo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyInfo.Size = New System.Drawing.Size(902, 344)
        Me.tlpCompanyInfo.TabIndex = 0
        '
        'ckbxAddresses
        '
        Me.ckbxAddresses.AutoSize = True
        Me.ckbxAddresses.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyAddressSatisfactory", True))
        Me.ckbxAddresses.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxAddresses.Location = New System.Drawing.Point(142, 279)
        Me.ckbxAddresses.Name = "ckbxAddresses"
        Me.ckbxAddresses.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxAddresses.Size = New System.Drawing.Size(155, 19)
        Me.ckbxAddresses.TabIndex = 20
        Me.ckbxAddresses.Text = "?Addresses are satisfactory"
        Me.ckbxAddresses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxAddresses.UseVisualStyleBackColor = True
        '
        'ckbxPreviousHistory
        '
        Me.ckbxPreviousHistory.AutoSize = True
        Me.ckbxPreviousHistory.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyHistorySatisfactory", True))
        Me.ckbxPreviousHistory.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxPreviousHistory.Location = New System.Drawing.Point(122, 212)
        Me.ckbxPreviousHistory.Name = "ckbxPreviousHistory"
        Me.ckbxPreviousHistory.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxPreviousHistory.Size = New System.Drawing.Size(175, 19)
        Me.ckbxPreviousHistory.TabIndex = 18
        Me.ckbxPreviousHistory.Text = "?Company history is satisfactory"
        Me.ckbxPreviousHistory.UseVisualStyleBackColor = True
        '
        'lblDirectorsNote
        '
        Me.lblDirectorsNote.AutoSize = True
        Me.lblDirectorsNote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblDirectorsNote.Location = New System.Drawing.Point(3, 167)
        Me.lblDirectorsNote.Name = "lblDirectorsNote"
        Me.lblDirectorsNote.Size = New System.Drawing.Size(294, 42)
        Me.lblDirectorsNote.TabIndex = 16
        Me.lblDirectorsNote.Text = "Comment on Directors and their related parties/interests"
        Me.lblDirectorsNote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxDirectors
        '
        Me.ckbxDirectors.AutoSize = True
        Me.ckbxDirectors.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyDirectors", True))
        Me.ckbxDirectors.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxDirectors.Location = New System.Drawing.Point(149, 145)
        Me.ckbxDirectors.Name = "ckbxDirectors"
        Me.ckbxDirectors.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxDirectors.Size = New System.Drawing.Size(148, 19)
        Me.ckbxDirectors.TabIndex = 15
        Me.ckbxDirectors.Text = "?Directors are satisfactory"
        Me.ckbxDirectors.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxDirectors.UseVisualStyleBackColor = True
        '
        'lblShareholdersNote
        '
        Me.lblShareholdersNote.AutoSize = True
        Me.lblShareholdersNote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblShareholdersNote.Location = New System.Drawing.Point(3, 100)
        Me.lblShareholdersNote.Name = "lblShareholdersNote"
        Me.lblShareholdersNote.Size = New System.Drawing.Size(294, 42)
        Me.lblShareholdersNote.TabIndex = 13
        Me.lblShareholdersNote.Text = "Comments on Shareholders and their related parties/interests"
        Me.lblShareholdersNote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxShareholders
        '
        Me.ckbxShareholders.AutoSize = True
        Me.ckbxShareholders.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyShareholders", True))
        Me.ckbxShareholders.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxShareholders.Location = New System.Drawing.Point(129, 78)
        Me.ckbxShareholders.Name = "ckbxShareholders"
        Me.ckbxShareholders.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxShareholders.Size = New System.Drawing.Size(168, 19)
        Me.ckbxShareholders.TabIndex = 5
        Me.ckbxShareholders.Text = "?Shareholders are satisfactory"
        Me.ckbxShareholders.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxShareholders.UseVisualStyleBackColor = True
        '
        'ckbxCapital
        '
        Me.ckbxCapital.AutoSize = True
        Me.ckbxCapital.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyPaidUpCapital", True))
        Me.ckbxCapital.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxCapital.Location = New System.Drawing.Point(140, 53)
        Me.ckbxCapital.Name = "ckbxCapital"
        Me.ckbxCapital.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxCapital.Size = New System.Drawing.Size(157, 19)
        Me.ckbxCapital.TabIndex = 4
        Me.ckbxCapital.Text = "?Paid-up capital is sufficient"
        Me.ckbxCapital.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxCapital.UseVisualStyleBackColor = True
        '
        'ckbxReturns
        '
        Me.ckbxReturns.AutoSize = True
        Me.ckbxReturns.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyReturnsFiled", True))
        Me.ckbxReturns.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxReturns.Location = New System.Drawing.Point(206, 28)
        Me.ckbxReturns.Name = "ckbxReturns"
        Me.ckbxReturns.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxReturns.Size = New System.Drawing.Size(91, 19)
        Me.ckbxReturns.TabIndex = 3
        Me.ckbxReturns.Text = "?Returns filed"
        Me.ckbxReturns.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxReturns.UseVisualStyleBackColor = True
        '
        'lblCompanyExtract
        '
        Me.lblCompanyExtract.AutoSize = True
        Me.tlpCompanyInfo.SetColumnSpan(Me.lblCompanyExtract, 2)
        Me.lblCompanyExtract.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblCompanyExtract.Location = New System.Drawing.Point(303, 0)
        Me.lblCompanyExtract.Name = "lblCompanyExtract"
        Me.lblCompanyExtract.Size = New System.Drawing.Size(241, 25)
        Me.lblCompanyExtract.TabIndex = 2
        Me.lblCompanyExtract.Text = "      Company extract to be saved under prefix ID1"
        Me.lblCompanyExtract.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ckbxRegistered
        '
        Me.ckbxRegistered.AutoSize = True
        Me.ckbxRegistered.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyRegistered", True))
        Me.ckbxRegistered.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxRegistered.Location = New System.Drawing.Point(172, 3)
        Me.ckbxRegistered.Name = "ckbxRegistered"
        Me.ckbxRegistered.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxRegistered.Size = New System.Drawing.Size(125, 19)
        Me.ckbxRegistered.TabIndex = 1
        Me.ckbxRegistered.Text = "?Company registered"
        Me.ckbxRegistered.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxRegistered.UseVisualStyleBackColor = True
        '
        'txtbxShareholders
        '
        Me.tlpCompanyInfo.SetColumnSpan(Me.txtbxShareholders, 2)
        Me.txtbxShareholders.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyShareholderNotes", True))
        Me.txtbxShareholders.Location = New System.Drawing.Point(303, 103)
        Me.txtbxShareholders.MaxLength = 1022
        Me.txtbxShareholders.Multiline = True
        Me.txtbxShareholders.Name = "txtbxShareholders"
        Me.txtbxShareholders.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxShareholders.Size = New System.Drawing.Size(596, 36)
        Me.txtbxShareholders.TabIndex = 14
        '
        'txtbxDirectors
        '
        Me.tlpCompanyInfo.SetColumnSpan(Me.txtbxDirectors, 2)
        Me.txtbxDirectors.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyDirectorNotes", True))
        Me.txtbxDirectors.Location = New System.Drawing.Point(303, 170)
        Me.txtbxDirectors.MaxLength = 1022
        Me.txtbxDirectors.Multiline = True
        Me.txtbxDirectors.Name = "txtbxDirectors"
        Me.txtbxDirectors.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxDirectors.Size = New System.Drawing.Size(596, 36)
        Me.txtbxDirectors.TabIndex = 17
        '
        'lblHistoryComments
        '
        Me.lblHistoryComments.AutoSize = True
        Me.lblHistoryComments.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblHistoryComments.Location = New System.Drawing.Point(119, 234)
        Me.lblHistoryComments.Name = "lblHistoryComments"
        Me.lblHistoryComments.Size = New System.Drawing.Size(178, 42)
        Me.lblHistoryComments.TabIndex = 19
        Me.lblHistoryComments.Text = "Comments on the Company's History"
        Me.lblHistoryComments.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxPreviousHistory
        '
        Me.tlpCompanyInfo.SetColumnSpan(Me.txtbxPreviousHistory, 2)
        Me.txtbxPreviousHistory.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyHistoryNotes", True))
        Me.txtbxPreviousHistory.Location = New System.Drawing.Point(303, 237)
        Me.txtbxPreviousHistory.MaxLength = 1022
        Me.txtbxPreviousHistory.Multiline = True
        Me.txtbxPreviousHistory.Name = "txtbxPreviousHistory"
        Me.txtbxPreviousHistory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxPreviousHistory.Size = New System.Drawing.Size(596, 36)
        Me.txtbxPreviousHistory.TabIndex = 9
        '
        'lblCompanyAddresses
        '
        Me.lblCompanyAddresses.AutoSize = True
        Me.lblCompanyAddresses.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCompanyAddresses.Location = New System.Drawing.Point(114, 301)
        Me.lblCompanyAddresses.Name = "lblCompanyAddresses"
        Me.lblCompanyAddresses.Size = New System.Drawing.Size(183, 43)
        Me.lblCompanyAddresses.TabIndex = 21
        Me.lblCompanyAddresses.Text = "Comment on the Company Addresses"
        Me.lblCompanyAddresses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxAddresses
        '
        Me.tlpCompanyInfo.SetColumnSpan(Me.txtbxAddresses, 2)
        Me.txtbxAddresses.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyAddressNotes", True))
        Me.txtbxAddresses.Location = New System.Drawing.Point(303, 304)
        Me.txtbxAddresses.MaxLength = 1022
        Me.txtbxAddresses.Multiline = True
        Me.txtbxAddresses.Name = "txtbxAddresses"
        Me.txtbxAddresses.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxAddresses.Size = New System.Drawing.Size(596, 36)
        Me.txtbxAddresses.TabIndex = 11
        '
        'gpbxReferences
        '
        Me.gpbxReferences.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxReferences.Controls.Add(Me.gpbxRef2)
        Me.gpbxReferences.Controls.Add(Me.gpbxRef1)
        Me.gpbxReferences.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxReferences.Location = New System.Drawing.Point(12, 834)
        Me.gpbxReferences.Name = "gpbxReferences"
        Me.gpbxReferences.Size = New System.Drawing.Size(914, 365)
        Me.gpbxReferences.TabIndex = 288
        Me.gpbxReferences.TabStop = False
        Me.gpbxReferences.Text = "Company References"
        '
        'gpbxRef2
        '
        Me.gpbxRef2.Controls.Add(Me.tlpRef2)
        Me.gpbxRef2.Location = New System.Drawing.Point(6, 190)
        Me.gpbxRef2.Name = "gpbxRef2"
        Me.gpbxRef2.Size = New System.Drawing.Size(902, 165)
        Me.gpbxRef2.TabIndex = 2
        Me.gpbxRef2.TabStop = False
        Me.gpbxRef2.Text = "Reference 2"
        '
        'tlpRef2
        '
        Me.tlpRef2.ColumnCount = 3
        Me.tlpRef2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 295.0!))
        Me.tlpRef2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpRef2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpRef2.Controls.Add(Me.lblSpend2, 0, 2)
        Me.tlpRef2.Controls.Add(Me.lblCurrentAcct2, 0, 1)
        Me.tlpRef2.Controls.Add(Me.txtbxCurrentAcct2, 1, 1)
        Me.tlpRef2.Controls.Add(Me.lblPayTime2, 0, 3)
        Me.tlpRef2.Controls.Add(Me.lblComments2, 0, 4)
        Me.tlpRef2.Controls.Add(Me.txtbxPayTime2, 1, 3)
        Me.tlpRef2.Controls.Add(Me.lblRef2Name, 0, 0)
        Me.tlpRef2.Controls.Add(Me.txtbxRef2Name, 1, 0)
        Me.tlpRef2.Controls.Add(Me.txtbxSpend2, 1, 2)
        Me.tlpRef2.Controls.Add(Me.txtbxOther2, 1, 4)
        Me.tlpRef2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpRef2.Location = New System.Drawing.Point(6, 19)
        Me.tlpRef2.Name = "tlpRef2"
        Me.tlpRef2.RowCount = 5
        Me.tlpRef2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpRef2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpRef2.Size = New System.Drawing.Size(890, 144)
        Me.tlpRef2.TabIndex = 0
        '
        'lblSpend2
        '
        Me.lblSpend2.AutoSize = True
        Me.lblSpend2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblSpend2.Location = New System.Drawing.Point(113, 50)
        Me.lblSpend2.Name = "lblSpend2"
        Me.lblSpend2.Size = New System.Drawing.Size(179, 25)
        Me.lblSpend2.TabIndex = 17
        Me.lblSpend2.Text = "How much do they spend (monthly)?"
        Me.lblSpend2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCurrentAcct2
        '
        Me.lblCurrentAcct2.AutoSize = True
        Me.lblCurrentAcct2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCurrentAcct2.Location = New System.Drawing.Point(80, 25)
        Me.lblCurrentAcct2.Name = "lblCurrentAcct2"
        Me.lblCurrentAcct2.Size = New System.Drawing.Size(212, 25)
        Me.lblCurrentAcct2.TabIndex = 9
        Me.lblCurrentAcct2.Text = "Due they have a current account with you?"
        Me.lblCurrentAcct2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxCurrentAcct2
        '
        Me.txtbxCurrentAcct2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef2Acct", True))
        Me.txtbxCurrentAcct2.Location = New System.Drawing.Point(298, 28)
        Me.txtbxCurrentAcct2.Name = "txtbxCurrentAcct2"
        Me.txtbxCurrentAcct2.Size = New System.Drawing.Size(289, 20)
        Me.txtbxCurrentAcct2.TabIndex = 12
        '
        'lblPayTime2
        '
        Me.lblPayTime2.AutoSize = True
        Me.lblPayTime2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblPayTime2.Location = New System.Drawing.Point(185, 75)
        Me.lblPayTime2.Name = "lblPayTime2"
        Me.lblPayTime2.Size = New System.Drawing.Size(107, 25)
        Me.lblPayTime2.TabIndex = 13
        Me.lblPayTime2.Text = "Do they pay on time?"
        Me.lblPayTime2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblComments2
        '
        Me.lblComments2.AutoSize = True
        Me.lblComments2.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblComments2.Location = New System.Drawing.Point(3, 100)
        Me.lblComments2.Name = "lblComments2"
        Me.lblComments2.Size = New System.Drawing.Size(189, 44)
        Me.lblComments2.TabIndex = 15
        Me.lblComments2.Text = "Other comments about this relationship"
        Me.lblComments2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxPayTime2
        '
        Me.tlpRef2.SetColumnSpan(Me.txtbxPayTime2, 2)
        Me.txtbxPayTime2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef2Pay", True))
        Me.txtbxPayTime2.Location = New System.Drawing.Point(298, 78)
        Me.txtbxPayTime2.Name = "txtbxPayTime2"
        Me.txtbxPayTime2.Size = New System.Drawing.Size(289, 20)
        Me.txtbxPayTime2.TabIndex = 14
        '
        'lblRef2Name
        '
        Me.lblRef2Name.AutoSize = True
        Me.lblRef2Name.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblRef2Name.Location = New System.Drawing.Point(257, 0)
        Me.lblRef2Name.Name = "lblRef2Name"
        Me.lblRef2Name.Size = New System.Drawing.Size(35, 25)
        Me.lblRef2Name.TabIndex = 0
        Me.lblRef2Name.Text = "Name"
        Me.lblRef2Name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxRef2Name
        '
        Me.tlpRef2.SetColumnSpan(Me.txtbxRef2Name, 2)
        Me.txtbxRef2Name.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef2Name", True))
        Me.txtbxRef2Name.Location = New System.Drawing.Point(298, 3)
        Me.txtbxRef2Name.Name = "txtbxRef2Name"
        Me.txtbxRef2Name.Size = New System.Drawing.Size(289, 20)
        Me.txtbxRef2Name.TabIndex = 1
        '
        'txtbxSpend2
        '
        Me.txtbxSpend2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef2Spend", True))
        Me.txtbxSpend2.Location = New System.Drawing.Point(298, 53)
        Me.txtbxSpend2.Name = "txtbxSpend2"
        Me.txtbxSpend2.Size = New System.Drawing.Size(289, 20)
        Me.txtbxSpend2.TabIndex = 10
        '
        'txtbxOther2
        '
        Me.tlpRef2.SetColumnSpan(Me.txtbxOther2, 2)
        Me.txtbxOther2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef2Comments", True))
        Me.txtbxOther2.Location = New System.Drawing.Point(298, 103)
        Me.txtbxOther2.Multiline = True
        Me.txtbxOther2.Name = "txtbxOther2"
        Me.txtbxOther2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxOther2.Size = New System.Drawing.Size(589, 36)
        Me.txtbxOther2.TabIndex = 16
        '
        'gpbxRef1
        '
        Me.gpbxRef1.Controls.Add(Me.tlpRef1)
        Me.gpbxRef1.Location = New System.Drawing.Point(6, 19)
        Me.gpbxRef1.Name = "gpbxRef1"
        Me.gpbxRef1.Size = New System.Drawing.Size(902, 165)
        Me.gpbxRef1.TabIndex = 1
        Me.gpbxRef1.TabStop = False
        Me.gpbxRef1.Text = "Reference 1"
        '
        'tlpRef1
        '
        Me.tlpRef1.ColumnCount = 3
        Me.tlpRef1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 295.0!))
        Me.tlpRef1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpRef1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpRef1.Controls.Add(Me.lblSpend1, 0, 2)
        Me.tlpRef1.Controls.Add(Me.lblRef1Name, 0, 0)
        Me.tlpRef1.Controls.Add(Me.txtbxRef1Name, 1, 0)
        Me.tlpRef1.Controls.Add(Me.lblCurrentAcct1, 0, 1)
        Me.tlpRef1.Controls.Add(Me.txtbxCurrentAcct1, 1, 1)
        Me.tlpRef1.Controls.Add(Me.lblPayTime1, 0, 3)
        Me.tlpRef1.Controls.Add(Me.lblComments1, 0, 4)
        Me.tlpRef1.Controls.Add(Me.txtbxPayTime1, 1, 3)
        Me.tlpRef1.Controls.Add(Me.txtbxSpend1, 1, 2)
        Me.tlpRef1.Controls.Add(Me.txtbxOther1, 1, 4)
        Me.tlpRef1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpRef1.Location = New System.Drawing.Point(6, 19)
        Me.tlpRef1.Name = "tlpRef1"
        Me.tlpRef1.RowCount = 5
        Me.tlpRef1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpRef1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpRef1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpRef1.Size = New System.Drawing.Size(890, 143)
        Me.tlpRef1.TabIndex = 0
        '
        'lblSpend1
        '
        Me.lblSpend1.AutoSize = True
        Me.lblSpend1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblSpend1.Location = New System.Drawing.Point(113, 50)
        Me.lblSpend1.Name = "lblSpend1"
        Me.lblSpend1.Size = New System.Drawing.Size(179, 25)
        Me.lblSpend1.TabIndex = 10
        Me.lblSpend1.Text = "How much do they spend (monthly)?"
        Me.lblSpend1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRef1Name
        '
        Me.lblRef1Name.AutoSize = True
        Me.lblRef1Name.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblRef1Name.Location = New System.Drawing.Point(257, 0)
        Me.lblRef1Name.Name = "lblRef1Name"
        Me.lblRef1Name.Size = New System.Drawing.Size(35, 25)
        Me.lblRef1Name.TabIndex = 0
        Me.lblRef1Name.Text = "Name"
        Me.lblRef1Name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxRef1Name
        '
        Me.tlpRef1.SetColumnSpan(Me.txtbxRef1Name, 2)
        Me.txtbxRef1Name.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef1Name", True))
        Me.txtbxRef1Name.Location = New System.Drawing.Point(298, 3)
        Me.txtbxRef1Name.Name = "txtbxRef1Name"
        Me.txtbxRef1Name.Size = New System.Drawing.Size(289, 20)
        Me.txtbxRef1Name.TabIndex = 1
        '
        'lblCurrentAcct1
        '
        Me.lblCurrentAcct1.AutoSize = True
        Me.lblCurrentAcct1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCurrentAcct1.Location = New System.Drawing.Point(80, 25)
        Me.lblCurrentAcct1.Name = "lblCurrentAcct1"
        Me.lblCurrentAcct1.Size = New System.Drawing.Size(212, 25)
        Me.lblCurrentAcct1.TabIndex = 2
        Me.lblCurrentAcct1.Text = "Due they have a current account with you?"
        Me.lblCurrentAcct1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxCurrentAcct1
        '
        Me.txtbxCurrentAcct1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef1Acct", True))
        Me.txtbxCurrentAcct1.Location = New System.Drawing.Point(298, 28)
        Me.txtbxCurrentAcct1.Name = "txtbxCurrentAcct1"
        Me.txtbxCurrentAcct1.Size = New System.Drawing.Size(291, 20)
        Me.txtbxCurrentAcct1.TabIndex = 3
        '
        'lblPayTime1
        '
        Me.lblPayTime1.AutoSize = True
        Me.lblPayTime1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblPayTime1.Location = New System.Drawing.Point(185, 75)
        Me.lblPayTime1.Name = "lblPayTime1"
        Me.lblPayTime1.Size = New System.Drawing.Size(107, 25)
        Me.lblPayTime1.TabIndex = 6
        Me.lblPayTime1.Text = "Do they pay on time?"
        Me.lblPayTime1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblComments1
        '
        Me.lblComments1.AutoSize = True
        Me.lblComments1.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblComments1.Location = New System.Drawing.Point(3, 100)
        Me.lblComments1.Name = "lblComments1"
        Me.lblComments1.Size = New System.Drawing.Size(189, 43)
        Me.lblComments1.TabIndex = 8
        Me.lblComments1.Text = "Other comments about this relationship"
        Me.lblComments1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxPayTime1
        '
        Me.tlpRef1.SetColumnSpan(Me.txtbxPayTime1, 2)
        Me.txtbxPayTime1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef1Pay", True))
        Me.txtbxPayTime1.Location = New System.Drawing.Point(298, 78)
        Me.txtbxPayTime1.Name = "txtbxPayTime1"
        Me.txtbxPayTime1.Size = New System.Drawing.Size(291, 20)
        Me.txtbxPayTime1.TabIndex = 7
        '
        'txtbxSpend1
        '
        Me.txtbxSpend1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef1Spend", True))
        Me.txtbxSpend1.Location = New System.Drawing.Point(298, 53)
        Me.txtbxSpend1.Name = "txtbxSpend1"
        Me.txtbxSpend1.Size = New System.Drawing.Size(291, 20)
        Me.txtbxSpend1.TabIndex = 5
        '
        'txtbxOther1
        '
        Me.tlpRef1.SetColumnSpan(Me.txtbxOther1, 2)
        Me.txtbxOther1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyRef1Comments", True))
        Me.txtbxOther1.Location = New System.Drawing.Point(298, 103)
        Me.txtbxOther1.Multiline = True
        Me.txtbxOther1.Name = "txtbxOther1"
        Me.txtbxOther1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtbxOther1.Size = New System.Drawing.Size(589, 36)
        Me.txtbxOther1.TabIndex = 9
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        Me.TableAdapterManager.ClientTableAdapter = Me.ClientTableAdapter
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter1
        '
        'ClientTableAdapter
        '
        Me.ClientTableAdapter.ClearBeforeFill = True
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'ClientBindingSource
        '
        Me.ClientBindingSource.DataMember = "Client"
        Me.ClientBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'TypeOfTenancyTableAdapter
        '
        Me.TypeOfTenancyTableAdapter.ClearBeforeFill = True
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(646, 39)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmt.TabIndex = 285
        Me.lblCurrentLoanAmt.Text = "0.00"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'lblCurrentLoan
        '
        Me.lblCurrentLoan.AutoSize = True
        Me.lblCurrentLoan.Location = New System.Drawing.Point(572, 39)
        Me.lblCurrentLoan.Name = "lblCurrentLoan"
        Me.lblCurrentLoan.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrentLoan.TabIndex = 284
        Me.lblCurrentLoan.Text = "Current Loan"
        Me.lblCurrentLoan.Visible = False
        '
        'CompanyStabilityForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(944, 1012)
        Me.Controls.Add(Me.lblCurrentLoanAmt)
        Me.Controls.Add(Me.lblCurrentLoan)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblClientAddress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.lblClientName)
        Me.Controls.Add(Me.lblDealerName)
        Me.Controls.Add(Me.lblContactNumber)
        Me.Controls.Add(Me.lblContact)
        Me.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.Controls.Add(Me.lblLoanAmount)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblDateAndTimeValue)
        Me.Controls.Add(Me.lblDateAndTime)
        Me.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Controls.Add(Me.lblEnquiryCode)
        Me.Controls.Add(Me.gpbxManager)
        Me.Controls.Add(Me.gpbxStatus)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "CompanyStabilityForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Due Diligence Company Stability"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.gpbxOther.ResumeLayout(False)
        Me.gpbxOther.PerformLayout()
        Me.gpbxCompanyTenancy.ResumeLayout(False)
        Me.tlpCompanyTenancy.ResumeLayout(False)
        Me.tlpCompanyTenancy.PerformLayout()
        Me.gpbxCompanyGuarantors.ResumeLayout(False)
        Me.tlpCompanyGuarantors.ResumeLayout(False)
        Me.tlpCompanyGuarantors.PerformLayout()
        CType(Me.TypeOfTenancyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxCompanyInfo.ResumeLayout(False)
        Me.tlpCompanyInfo.ResumeLayout(False)
        Me.tlpCompanyInfo.PerformLayout()
        Me.gpbxReferences.ResumeLayout(False)
        Me.gpbxRef2.ResumeLayout(False)
        Me.tlpRef2.ResumeLayout(False)
        Me.tlpRef2.PerformLayout()
        Me.gpbxRef1.ResumeLayout(False)
        Me.tlpRef1.ResumeLayout(False)
        Me.tlpRef1.PerformLayout()
        CType(Me.ClientBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeclineToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WithdrawToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblClientAddress As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblClientName As System.Windows.Forms.Label
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblContactNumber As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents btnQRGList As System.Windows.Forms.Button
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ClientTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ClientTableAdapter
    Friend WithEvents ClientBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtbxAssets As System.Windows.Forms.TextBox
    Friend WithEvents txtbxGuarantor As System.Windows.Forms.TextBox
    Friend WithEvents txtbxPreviousHistory As System.Windows.Forms.TextBox
    Friend WithEvents txtbxAddresses As System.Windows.Forms.TextBox
    Friend WithEvents txtbxGuarantorTenancy As System.Windows.Forms.TextBox
    Friend WithEvents txtbxLengthTen As System.Windows.Forms.TextBox
    Friend WithEvents txtbxCompanyTenancySatis As System.Windows.Forms.TextBox
    Friend WithEvents txtbxOtherComments As System.Windows.Forms.TextBox
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents gpbxReferences As System.Windows.Forms.GroupBox
    Friend WithEvents tlpRef1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gpbxRef2 As System.Windows.Forms.GroupBox
    Friend WithEvents tlpRef2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblRef2Name As System.Windows.Forms.Label
    Friend WithEvents txtbxRef2Name As System.Windows.Forms.TextBox
    Friend WithEvents gpbxRef1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblRef1Name As System.Windows.Forms.Label
    Friend WithEvents txtbxRef1Name As System.Windows.Forms.TextBox
    Friend WithEvents lblCurrentAcct1 As System.Windows.Forms.Label
    Friend WithEvents txtbxCurrentAcct1 As System.Windows.Forms.TextBox
    Friend WithEvents txtbxSpend1 As System.Windows.Forms.TextBox
    Friend WithEvents lblPayTime1 As System.Windows.Forms.Label
    Friend WithEvents txtbxPayTime1 As System.Windows.Forms.TextBox
    Friend WithEvents lblComments1 As System.Windows.Forms.Label
    Friend WithEvents txtbxOther1 As System.Windows.Forms.TextBox
    Friend WithEvents lblCurrentAcct2 As System.Windows.Forms.Label
    Friend WithEvents txtbxSpend2 As System.Windows.Forms.TextBox
    Friend WithEvents txtbxCurrentAcct2 As System.Windows.Forms.TextBox
    Friend WithEvents lblPayTime2 As System.Windows.Forms.Label
    Friend WithEvents lblComments2 As System.Windows.Forms.Label
    Friend WithEvents txtbxPayTime2 As System.Windows.Forms.TextBox
    Friend WithEvents txtbxOther2 As System.Windows.Forms.TextBox
    Friend WithEvents gpbxCompanyInfo As System.Windows.Forms.GroupBox
    Friend WithEvents tlpCompanyInfo As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ckbxRegistered As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxCapital As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxReturns As System.Windows.Forms.CheckBox
    Friend WithEvents lblCompanyExtract As System.Windows.Forms.Label
    Friend WithEvents lblShareholdersNote As System.Windows.Forms.Label
    Friend WithEvents ckbxShareholders As System.Windows.Forms.CheckBox
    Friend WithEvents txtbxShareholders As System.Windows.Forms.TextBox
    Friend WithEvents lblDirectorsNote As System.Windows.Forms.Label
    Friend WithEvents ckbxDirectors As System.Windows.Forms.CheckBox
    Friend WithEvents txtbxDirectors As System.Windows.Forms.TextBox
    Friend WithEvents ckbxPreviousHistory As System.Windows.Forms.CheckBox
    Friend WithEvents lblHistoryComments As System.Windows.Forms.Label
    Friend WithEvents ckbxAddresses As System.Windows.Forms.CheckBox
    Friend WithEvents lblCompanyAddresses As System.Windows.Forms.Label
    Friend WithEvents gpbxCompanyGuarantors As System.Windows.Forms.GroupBox
    Friend WithEvents tlpCompanyGuarantors As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ckbxGuarantorID As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxAppFilledOut As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblGuarantor As System.Windows.Forms.Label
    Friend WithEvents lblAssests As System.Windows.Forms.Label
    Friend WithEvents lblGuarantorsTenancy As System.Windows.Forms.Label
    Friend WithEvents cmbxCompanyGuarantorTenType As System.Windows.Forms.ComboBox
    Friend WithEvents TypeOfTenancyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TypeOfTenancyTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfTenancyTableAdapter
    Friend WithEvents lblGuarantorTenancy As System.Windows.Forms.Label
    Friend WithEvents gpbxCompanyTenancy As System.Windows.Forms.GroupBox
    Friend WithEvents tlpCompanyTenancy As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblLengthTen As System.Windows.Forms.Label
    Friend WithEvents cmbxTenancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblTenancy As System.Windows.Forms.Label
    Friend WithEvents lblTenancyComments As System.Windows.Forms.Label
    Friend WithEvents gpbxOther As System.Windows.Forms.GroupBox
    Friend WithEvents lblSpend1 As System.Windows.Forms.Label
    Friend WithEvents lblSpend2 As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoan As System.Windows.Forms.Label
End Class
