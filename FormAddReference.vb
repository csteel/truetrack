﻿Public Class FormAddReference


    Dim thisId As Integer
    Dim thisOpType As Integer
    Dim thisCustomerId As Integer

    Friend Sub PassVariable(ByVal recordId As Integer, ByVal refOpType As Integer, Optional ByVal refCustomerId As Integer = 0)
        thisId = recordId
        thisOpType = refOpType
        thisCustomerId = refCustomerId

        If refOpType = 0 Then 'Update
            btnSave.Visible = True
            btnInsert.Visible = False
            Try
                Me.TradeReferencesTableAdapter.FillById(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(recordId))
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        ElseIf refOpType = 1 Then 'Insert
            btnSave.Visible = False
            btnInsert.Visible = True
        End If

    End Sub

    Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Me.Validate()
            TradeReferencesBindingSource.EndEdit()
            TradeReferencesTableAdapter.Update(Me.EnquiryWorkSheetDataSet.TradeReferences)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub BtnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click
        Try
            Me.Validate()
            TradeReferencesBindingSource.EndEdit()
            TradeReferencesTableAdapter.Insert(txtRefName.Text, txtRefAccount.Text, txtRefSpend.Text, txtRefPay.Text, txtRefComments.Text, thisCustomerId)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub
End Class