﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DisplayDealersForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisplayDealersForm))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.DealerContactDetailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DealerContactDetailsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.DealerContactDetailsBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.DealerContactDetailsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gpbxFilters = New System.Windows.Forms.GroupBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblDealerId = New System.Windows.Forms.Label()
        Me.txtbxContactMethodFilter = New System.Windows.Forms.TextBox()
        Me.lblLegalName = New System.Windows.Forms.Label()
        Me.txtbxLegalNameFilter = New System.Windows.Forms.TextBox()
        Me.lblContactMethod = New System.Windows.Forms.Label()
        Me.txtbxNameFilter = New System.Windows.Forms.TextBox()
        Me.txtbxDealerIdFilter = New System.Windows.Forms.TextBox()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DealerContactDetailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DealerContactDetailsBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DealerContactDetailsBindingNavigator.SuspendLayout()
        CType(Me.DealerContactDetailsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxFilters.SuspendLayout()
        Me.SuspendLayout()
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DealerContactDetailsBindingSource
        '
        Me.DealerContactDetailsBindingSource.DataMember = "DealerContactDetails"
        Me.DealerContactDetailsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DealerContactDetailsTableAdapter
        '
        Me.DealerContactDetailsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'DealerContactDetailsBindingNavigator
        '
        Me.DealerContactDetailsBindingNavigator.AddNewItem = Nothing
        Me.DealerContactDetailsBindingNavigator.BindingSource = Me.DealerContactDetailsBindingSource
        Me.DealerContactDetailsBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.DealerContactDetailsBindingNavigator.DeleteItem = Nothing
        Me.DealerContactDetailsBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.DealerContactDetailsBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.DealerContactDetailsBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.DealerContactDetailsBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.DealerContactDetailsBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.DealerContactDetailsBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.DealerContactDetailsBindingNavigator.Name = "DealerContactDetailsBindingNavigator"
        Me.DealerContactDetailsBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.DealerContactDetailsBindingNavigator.Size = New System.Drawing.Size(944, 25)
        Me.DealerContactDetailsBindingNavigator.TabIndex = 0
        Me.DealerContactDetailsBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'DealerContactDetailsDataGridView
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        Me.DealerContactDetailsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DealerContactDetailsDataGridView.AutoGenerateColumns = False
        Me.DealerContactDetailsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DealerContactDetailsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewCheckBoxColumn1})
        Me.DealerContactDetailsDataGridView.DataSource = Me.DealerContactDetailsBindingSource
        Me.DealerContactDetailsDataGridView.Location = New System.Drawing.Point(0, 93)
        Me.DealerContactDetailsDataGridView.MultiSelect = False
        Me.DealerContactDetailsDataGridView.Name = "DealerContactDetailsDataGridView"
        Me.DealerContactDetailsDataGridView.RowHeadersWidth = 20
        Me.DealerContactDetailsDataGridView.RowTemplate.Height = 20
        Me.DealerContactDetailsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DealerContactDetailsDataGridView.ShowEditingIcon = False
        Me.DealerContactDetailsDataGridView.Size = New System.Drawing.Size(944, 621)
        Me.DealerContactDetailsDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "DealerId"
        Me.DataGridViewTextBoxColumn2.HeaderText = "DealerId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 50
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "CompanyNumber"
        Me.DataGridViewTextBoxColumn3.HeaderText = "CompanyNumber"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Name"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 225
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "LegalName"
        Me.DataGridViewTextBoxColumn5.HeaderText = "LegalName"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 225
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "ContactMethod"
        Me.DataGridViewTextBoxColumn6.HeaderText = "ContactMethod"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "ContactValue"
        Me.DataGridViewTextBoxColumn7.HeaderText = "ContactValue"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "Active"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Active"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'gpbxFilters
        '
        Me.gpbxFilters.Controls.Add(Me.lblName)
        Me.gpbxFilters.Controls.Add(Me.lblDealerId)
        Me.gpbxFilters.Controls.Add(Me.txtbxContactMethodFilter)
        Me.gpbxFilters.Controls.Add(Me.lblLegalName)
        Me.gpbxFilters.Controls.Add(Me.txtbxLegalNameFilter)
        Me.gpbxFilters.Controls.Add(Me.lblContactMethod)
        Me.gpbxFilters.Controls.Add(Me.txtbxNameFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxDealerIdFilter)
        Me.gpbxFilters.Location = New System.Drawing.Point(12, 28)
        Me.gpbxFilters.Name = "gpbxFilters"
        Me.gpbxFilters.Size = New System.Drawing.Size(920, 59)
        Me.gpbxFilters.TabIndex = 13
        Me.gpbxFilters.TabStop = False
        Me.gpbxFilters.Text = "Filters"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(314, 14)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(74, 13)
        Me.lblName.TabIndex = 3
        Me.lblName.Text = "Trading Name"
        '
        'lblDealerId
        '
        Me.lblDealerId.AutoSize = True
        Me.lblDealerId.Location = New System.Drawing.Point(162, 14)
        Me.lblDealerId.Name = "lblDealerId"
        Me.lblDealerId.Size = New System.Drawing.Size(50, 13)
        Me.lblDealerId.TabIndex = 2
        Me.lblDealerId.Text = "Dealer Id"
        '
        'txtbxContactMethodFilter
        '
        Me.txtbxContactMethodFilter.Location = New System.Drawing.Point(669, 32)
        Me.txtbxContactMethodFilter.Name = "txtbxContactMethodFilter"
        Me.txtbxContactMethodFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxContactMethodFilter.TabIndex = 10
        '
        'lblLegalName
        '
        Me.lblLegalName.AutoSize = True
        Me.lblLegalName.Location = New System.Drawing.Point(531, 14)
        Me.lblLegalName.Name = "lblLegalName"
        Me.lblLegalName.Size = New System.Drawing.Size(64, 13)
        Me.lblLegalName.TabIndex = 4
        Me.lblLegalName.Text = "Legal Name"
        '
        'txtbxLegalNameFilter
        '
        Me.txtbxLegalNameFilter.Location = New System.Drawing.Point(463, 32)
        Me.txtbxLegalNameFilter.Name = "txtbxLegalNameFilter"
        Me.txtbxLegalNameFilter.Size = New System.Drawing.Size(200, 20)
        Me.txtbxLegalNameFilter.TabIndex = 9
        '
        'lblContactMethod
        '
        Me.lblContactMethod.AutoSize = True
        Me.lblContactMethod.Location = New System.Drawing.Point(687, 14)
        Me.lblContactMethod.Name = "lblContactMethod"
        Me.lblContactMethod.Size = New System.Drawing.Size(83, 13)
        Me.lblContactMethod.TabIndex = 5
        Me.lblContactMethod.Text = "Contact Method"
        '
        'txtbxNameFilter
        '
        Me.txtbxNameFilter.Location = New System.Drawing.Point(257, 32)
        Me.txtbxNameFilter.Name = "txtbxNameFilter"
        Me.txtbxNameFilter.Size = New System.Drawing.Size(200, 20)
        Me.txtbxNameFilter.TabIndex = 8
        '
        'txtbxDealerIdFilter
        '
        Me.txtbxDealerIdFilter.Location = New System.Drawing.Point(126, 32)
        Me.txtbxDealerIdFilter.Name = "txtbxDealerIdFilter"
        Me.txtbxDealerIdFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxDealerIdFilter.TabIndex = 7
        '
        'DisplayDealersForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 714)
        Me.Controls.Add(Me.gpbxFilters)
        Me.Controls.Add(Me.DealerContactDetailsDataGridView)
        Me.Controls.Add(Me.DealerContactDetailsBindingNavigator)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(0, 300)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(960, 750)
        Me.Name = "DisplayDealersForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Display Dealers"
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DealerContactDetailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DealerContactDetailsBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DealerContactDetailsBindingNavigator.ResumeLayout(False)
        Me.DealerContactDetailsBindingNavigator.PerformLayout()
        CType(Me.DealerContactDetailsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxFilters.ResumeLayout(False)
        Me.gpbxFilters.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents DealerContactDetailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DealerContactDetailsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents DealerContactDetailsBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents DealerContactDetailsDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxFilters As System.Windows.Forms.GroupBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblDealerId As System.Windows.Forms.Label
    Friend WithEvents txtbxContactMethodFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblLegalName As System.Windows.Forms.Label
    Friend WithEvents txtbxLegalNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblContactMethod As System.Windows.Forms.Label
    Friend WithEvents txtbxNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents txtbxDealerIdFilter As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
