﻿Public Class frmSelectedClientDetails

    Private _clientdetails As ClientMappingDetails

    Private Sub frmSelectedClientDetails_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LoadFormData(_clientdetails)
        SettingGridColumn()
    End Sub

    Public Sub New(clientdetails As ClientMappingDetails)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _clientdetails = clientdetails

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Function LoadFormData(clientdetails As ClientMappingDetails) As Boolean
        Dim customerType As String = String.Empty
        customerType = clientdetails.CustomerType
        txtClientId.Text = clientdetails.ClientId
        Select Case customerType.ToLower
            Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL.ToLower
                txtFirstName.Text = clientdetails.FirstName
                txtLastName.Text = clientdetails.LastName
                txtDOB.Text = clientdetails.DateOfBirth
            Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY.ToLower
                lblFirstName.Text = "Company name"
                txtFirstName.Text = clientdetails.CompanyName
                lblLastName.Text = "Legal name"
                txtLastName.Text = clientdetails.LegalName
                lblDOB.Visible = False
                txtDOB.Visible = False
            Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST.ToLower
                lblFirstName.Text = "Trust name"
                txtFirstName.Text = clientdetails.CompanyName
                lblLastName.Visible = False
                txtLastName.Visible = False
                lblDOB.Visible = False
                txtDOB.Visible = False
            Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP.ToLower
                lblFirstName.Text = "Partnership"
                txtFirstName.Text = clientdetails.CompanyName
                lblLastName.Visible = False
                txtLastName.Visible = False
                lblDOB.Visible = False
                txtDOB.Visible = False
        End Select

        txtAddress.Text = clientdetails.AddressPhysical
        txtSuburb.Text = clientdetails.CityPhysical
        txtCity.Text = clientdetails.StatePhysical
        txtPostCode.Text = clientdetails.PostCodePhysical

        txtPostalAddress.Text = clientdetails.AddressPostal
        txtPostalSuburb.Text = clientdetails.CityPostal
        txtPostalCity.Text = clientdetails.StatePostal
        txtPostalPostcode.Text = clientdetails.PostCodePostal

        txtManager.Text = clientdetails.ManagerId

        'Bind ContactMethods XML
        dgvContactMethods.DataSource = BuildContactMethodsOACXmlDataTable(clientdetails)

    End Function

    'set grid columns
    Private Sub SettingGridColumn()

        If dgvContactMethods.RowCount > 0 And dgvContactMethods.ColumnCount > 1 Then
            dgvContactMethods.Columns(0).Width = (dgvContactMethods.Width / 2) - 2
            dgvContactMethods.Columns(1).Width = (dgvContactMethods.Width / 2) - 2
            dgvContactMethods.Columns(0).Name = "ContactMethodName"
            dgvContactMethods.Columns(1).Name = "ContactMethodValue"
        End If

    End Sub

    'return datatable with selected contact methods
    Private Function BuildContactMethodsOACXmlDataTable(ByVal clientfinObj As ClientMappingDetails) As DataTable
        Dim table As New DataTable
        ' Create columns in the DataTable.
        table.Columns.Add("Method", GetType(String))
        table.Columns.Add("Value", GetType(String))

        'Add Values in Table
        'Contact Methods from OAC Xml
        For Each clientContactMethod As ContactMethods In clientfinObj.ContactMethodsOACXml

            If (clientContactMethod IsNot Nothing) Then
                table.Rows.Add(clientContactMethod.MethodName, clientContactMethod.MethodValue)
            End If
        Next
        'Contact Methods from finPOWER
        For Each clientContactMethod As ContactMethods In clientfinObj.ContactMethodsFinPower

            If (clientContactMethod IsNot Nothing) Then
                table.Rows.Add(clientContactMethod.MethodName, clientContactMethod.MethodValue)
            End If
        Next
        Return table

    End Function


End Class