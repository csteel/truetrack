﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WkShtWizForm3
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim lblTradeRefName As System.Windows.Forms.Label
        Dim lblTradeRefAccount As System.Windows.Forms.Label
        Dim lblTradeRefSpend As System.Windows.Forms.Label
        Dim lblTradeRefPay As System.Windows.Forms.Label
        Dim lblTradeRefComments As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WkShtWizForm3))
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.pnlIndividual = New System.Windows.Forms.Panel()
        Me.rtxtbxStabilityOther = New AppWhShtB.SpellBox()
        Me.CustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.gpbxCredit = New System.Windows.Forms.GroupBox()
        Me.txtbxCreditPPSR = New AppWhShtB.SpellBox()
        Me.txtbxCreditDefaults = New AppWhShtB.SpellBox()
        Me.txtbxCreditHistory = New AppWhShtB.SpellBox()
        Me.gpbxCreditQuestion3 = New System.Windows.Forms.GroupBox()
        Me.rbtnNoCreditQuestion3 = New System.Windows.Forms.RadioButton()
        Me.rbtnYesCreditQuestion3 = New System.Windows.Forms.RadioButton()
        Me.gpbxCreditQuestion2 = New System.Windows.Forms.GroupBox()
        Me.rbtnNoCreditQuestion2 = New System.Windows.Forms.RadioButton()
        Me.rbtnYesCreditQuestion2 = New System.Windows.Forms.RadioButton()
        Me.gpbxCreditQuestion1 = New System.Windows.Forms.GroupBox()
        Me.rbtnNoCreditQuestion1 = New System.Windows.Forms.RadioButton()
        Me.rbtnYesCreditQuestion1 = New System.Windows.Forms.RadioButton()
        Me.lblCreditCheck = New System.Windows.Forms.Label()
        Me.lblCreditHistory = New System.Windows.Forms.Label()
        Me.lblCreditDefaults = New System.Windows.Forms.Label()
        Me.lblCreditPPSR = New System.Windows.Forms.Label()
        Me.gpbxCustomerName = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblCustomerTitle = New System.Windows.Forms.Label()
        Me.cmbxCustomerTitle = New System.Windows.Forms.ComboBox()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.txtbxFirstName = New System.Windows.Forms.TextBox()
        Me.txtMiddleNames = New System.Windows.Forms.TextBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.gpbxIdentity = New System.Windows.Forms.GroupBox()
        Me.cmbxTypeDL = New System.Windows.Forms.ComboBox()
        Me.lblTypeDL = New System.Windows.Forms.Label()
        Me.cmbxTypeOfId = New System.Windows.Forms.ComboBox()
        Me.lblTypeOfId = New System.Windows.Forms.Label()
        Me.txtAgeOfId = New AppWhShtB.SpellBox()
        Me.txtResidency = New AppWhShtB.SpellBox()
        Me.txtIdIsPerson = New AppWhShtB.SpellBox()
        Me.rbPepNo = New System.Windows.Forms.RadioButton()
        Me.rbPepYes = New System.Windows.Forms.RadioButton()
        Me.lblPep = New System.Windows.Forms.Label()
        Me.pbPEP = New System.Windows.Forms.PictureBox()
        Me.cmbxRiskAssess = New System.Windows.Forms.ComboBox()
        Me.lblRiskAssess = New System.Windows.Forms.Label()
        Me.lblAgeOfId = New System.Windows.Forms.Label()
        Me.lblResidency = New System.Windows.Forms.Label()
        Me.lblIdIsPerson = New System.Windows.Forms.Label()
        Me.ckbxIdCurrent = New System.Windows.Forms.CheckBox()
        Me.gpbxEmployment = New System.Windows.Forms.GroupBox()
        Me.txtbxQuestion8 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion7 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion6 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion4 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion3 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion9 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion2 = New AppWhShtB.SpellBox()
        Me.txtbxQuestion1 = New AppWhShtB.SpellBox()
        Me.txtbxEmployer = New System.Windows.Forms.TextBox()
        Me.lblEmployer = New System.Windows.Forms.Label()
        Me.lblSpokeTo = New System.Windows.Forms.Label()
        Me.btnRangEmpTimeStamp = New System.Windows.Forms.Button()
        Me.lblRangEmpTimeStamp = New System.Windows.Forms.Label()
        Me.txtbxSpokeTo = New System.Windows.Forms.TextBox()
        Me.lblEmploy9 = New System.Windows.Forms.Label()
        Me.lblDoes = New System.Windows.Forms.Label()
        Me.lblEmployerNotRung = New System.Windows.Forms.Label()
        Me.lblSalutation = New System.Windows.Forms.Label()
        Me.ckbxEmployerNotRung = New System.Windows.Forms.CheckBox()
        Me.lblWorkCompany = New System.Windows.Forms.Label()
        Me.lblBeneficiary = New System.Windows.Forms.Label()
        Me.lblFulltime = New System.Windows.Forms.Label()
        Me.ckbxBeneficiary = New System.Windows.Forms.CheckBox()
        Me.lblWhatDo = New System.Windows.Forms.Label()
        Me.lblHowLong = New System.Windows.Forms.Label()
        Me.lblPay = New System.Windows.Forms.Label()
        Me.txtbxQuestion5 = New System.Windows.Forms.TextBox()
        Me.lblWorker = New System.Windows.Forms.Label()
        Me.lblJobSafe = New System.Windows.Forms.Label()
        Me.gpbxTenancy = New System.Windows.Forms.GroupBox()
        Me.txtbxEstablishTenancy = New AppWhShtB.SpellBox()
        Me.txtbxTenancyEnviron = New AppWhShtB.SpellBox()
        Me.txtbxResidenceSatis = New AppWhShtB.SpellBox()
        Me.lblResidence = New System.Windows.Forms.Label()
        Me.txtbxTenancy = New System.Windows.Forms.TextBox()
        Me.lblTenancyEstablish = New System.Windows.Forms.Label()
        Me.lblResidenceSatis = New System.Windows.Forms.Label()
        Me.lblTenancyBecause = New System.Windows.Forms.Label()
        Me.lblTenancy = New System.Windows.Forms.Label()
        Me.cmbxTenancyType = New System.Windows.Forms.ComboBox()
        Me.TypeOfTenancyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblOther = New System.Windows.Forms.Label()
        Me.btnCancelCustomer = New System.Windows.Forms.Button()
        Me.btnDeleteCustomer = New System.Windows.Forms.Button()
        Me.btnSaveCustomer = New System.Windows.Forms.Button()
        Me.cmbxCustomerType = New System.Windows.Forms.ComboBox()
        Me.lblCustomerType = New System.Windows.Forms.Label()
        Me.dgvEnquiryCustomers = New System.Windows.Forms.DataGridView()
        Me.IdnumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerEnquiryTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryCustomersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnImport = New System.Windows.Forms.Button()
        Me.btnAddCustomer = New System.Windows.Forms.Button()
        Me.lblContactAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblContactName = New System.Windows.Forms.Label()
        Me.lblContactNumber = New System.Windows.Forms.Label()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.pnlFooter = New System.Windows.Forms.Panel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnQRGList = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.pbImportCustomer = New System.Windows.Forms.PictureBox()
        Me.AnimatedCirclePanel = New System.Windows.Forms.Panel()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.lblCurrentLoan = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEnquiryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeclinedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WithdrawnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsReceivedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DealersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocsReceivedImg = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnAddNewST = New System.Windows.Forms.Button()
        Me.pnlCustomerType = New System.Windows.Forms.Panel()
        Me.pbHA = New System.Windows.Forms.PictureBox()
        Me.cmbxAcceptanceMethod = New System.Windows.Forms.ComboBox()
        Me.lblAcceptanceMethod = New System.Windows.Forms.Label()
        Me.cmbxCustEnquiryType = New System.Windows.Forms.ComboBox()
        Me.lblCustEnquiryType = New System.Windows.Forms.Label()
        Me.ListAMLRiskBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.List2AMLRisk = New System.Windows.Forms.BindingSource(Me.components)
        Me.Enum_CustomerTypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.CustomerEnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CustomerEnquiryTableAdapter()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.TypeOfTenancyTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfTenancyTableAdapter()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.List_AMLRiskTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.List_AMLRiskTableAdapter()
        Me.EnquiryCustomersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter()
        Me.CustomerTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter()
        Me.CustomerEnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlCompany = New System.Windows.Forms.Panel()
        Me.gpbxTradeRef = New System.Windows.Forms.GroupBox()
        Me.btnNewRef = New System.Windows.Forms.Button()
        Me.drReferences = New Microsoft.VisualBasic.PowerPacks.DataRepeater()
        Me.txtTradeRefComments = New AppWhShtB.SpellBox()
        Me.TradeReferencesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtTradeRefPay = New System.Windows.Forms.TextBox()
        Me.txtTradeRefSpend = New System.Windows.Forms.TextBox()
        Me.txtTradeRefAccount = New System.Windows.Forms.TextBox()
        Me.btnTradeRefEdit = New System.Windows.Forms.Button()
        Me.btnTradeRefDelete = New System.Windows.Forms.Button()
        Me.txtTradeRefName = New System.Windows.Forms.TextBox()
        Me.gpbxBusOther = New System.Windows.Forms.GroupBox()
        Me.txtBusOtherComments = New AppWhShtB.SpellBox()
        Me.gpbxDtsb23 = New System.Windows.Forms.GroupBox()
        Me.PanelDtsb23 = New System.Windows.Forms.Panel()
        Me.pbPEP2 = New System.Windows.Forms.PictureBox()
        Me.tlpShareholders = New System.Windows.Forms.TableLayoutPanel()
        Me.lblShareholdersNote = New System.Windows.Forms.Label()
        Me.ckbxShareholders = New System.Windows.Forms.CheckBox()
        Me.ckbxShareIdentified = New System.Windows.Forms.CheckBox()
        Me.ckbxShareStructure = New System.Windows.Forms.CheckBox()
        Me.txtShareholdersComments = New AppWhShtB.SpellBox()
        Me.dgvDTSBType2 = New System.Windows.Forms.DataGridView()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerIdDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FirstNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LastNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateOfBirthDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdVerifiedDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.AddressVerifiedDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.AMLRiskDesc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IsaPEP = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Edit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Delete = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.DTSBBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblShareholdersNotice = New System.Windows.Forms.Label()
        Me.gpbxBusinessCredit = New System.Windows.Forms.GroupBox()
        Me.txtBusCreditPPSR = New AppWhShtB.SpellBox()
        Me.txtBusCreditDefaults = New AppWhShtB.SpellBox()
        Me.txtBusCreditHistory = New AppWhShtB.SpellBox()
        Me.gpbxBusCreditQuestion3 = New System.Windows.Forms.GroupBox()
        Me.rbtnNoBusCreditQuestion3 = New System.Windows.Forms.RadioButton()
        Me.rbtnYesBusCreditQuestion3 = New System.Windows.Forms.RadioButton()
        Me.gpbxBusCreditQuestion2 = New System.Windows.Forms.GroupBox()
        Me.rbtnNoBusCreditQuestion2 = New System.Windows.Forms.RadioButton()
        Me.rbtnYesBusCreditQuestion2 = New System.Windows.Forms.RadioButton()
        Me.gpbxBusCreditQuestion1 = New System.Windows.Forms.GroupBox()
        Me.rbtnNoBusCreditQuestion1 = New System.Windows.Forms.RadioButton()
        Me.rbtnYesBusCreditQuestion1 = New System.Windows.Forms.RadioButton()
        Me.lblBusCreditCheck = New System.Windows.Forms.Label()
        Me.lblBusCreditHistory = New System.Windows.Forms.Label()
        Me.lblBusCreditDefaults = New System.Windows.Forms.Label()
        Me.lblBusCreditPPSR = New System.Windows.Forms.Label()
        Me.gpbxCoTenancy = New System.Windows.Forms.GroupBox()
        Me.tlpCompanyTenancy = New System.Windows.Forms.TableLayoutPanel()
        Me.lblTenancyComments = New System.Windows.Forms.Label()
        Me.lblLengthTen = New System.Windows.Forms.Label()
        Me.cmbxTenancy = New System.Windows.Forms.ComboBox()
        Me.lblTenancyIs = New System.Windows.Forms.Label()
        Me.txtbxLengthTen = New AppWhShtB.SpellBox()
        Me.txtbxCompanyTenancySatis = New AppWhShtB.SpellBox()
        Me.gpbxDtsb01 = New System.Windows.Forms.GroupBox()
        Me.PanelDtsb01 = New System.Windows.Forms.Panel()
        Me.dgvDtsb01 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.AMLRisk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dtsb01Edit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Dtsb01Delete = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.DTSB01BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tlpDirectors = New System.Windows.Forms.TableLayoutPanel()
        Me.lblDirectors = New System.Windows.Forms.Label()
        Me.ckbxDirectorsTrustees = New System.Windows.Forms.CheckBox()
        Me.txtDirectorsTrustees = New AppWhShtB.SpellBox()
        Me.btnAddNewDtsb01 = New System.Windows.Forms.Button()
        Me.lblDirectorsNotice = New System.Windows.Forms.Label()
        Me.gpbxCompanyInfo = New System.Windows.Forms.GroupBox()
        Me.tlpInformation = New System.Windows.Forms.TableLayoutPanel()
        Me.lblCompanyAddresses = New System.Windows.Forms.Label()
        Me.ckbxAddresses = New System.Windows.Forms.CheckBox()
        Me.lblHistoryComments = New System.Windows.Forms.Label()
        Me.ckbxHistory = New System.Windows.Forms.CheckBox()
        Me.ckbxCapital = New System.Windows.Forms.CheckBox()
        Me.ckbxReturns = New System.Windows.Forms.CheckBox()
        Me.ckbxRegistered = New System.Windows.Forms.CheckBox()
        Me.ckbxCoExtTrustDdSaved = New System.Windows.Forms.CheckBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtCompanyName = New AppWhShtB.SpellBox()
        Me.txtbxHistoryComments = New AppWhShtB.SpellBox()
        Me.txtbxAddressesComments = New AppWhShtB.SpellBox()
        Me.EnumAMLRiskBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DTSBTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter()
        Me.DTSB01TableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter()
        Me.TradeReferencesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter()
        Me.ItemTemplate = New Microsoft.VisualBasic.PowerPacks.DataRepeaterItem()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter()
        Me.Enum_AMLRiskTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.Enum_AMLRiskTableAdapter()
        lblTradeRefName = New System.Windows.Forms.Label()
        lblTradeRefAccount = New System.Windows.Forms.Label()
        lblTradeRefSpend = New System.Windows.Forms.Label()
        lblTradeRefPay = New System.Windows.Forms.Label()
        lblTradeRefComments = New System.Windows.Forms.Label()
        Me.StatusStripMessage.SuspendLayout()
        Me.pnlIndividual.SuspendLayout()
        CType(Me.CustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxCredit.SuspendLayout()
        Me.gpbxCreditQuestion3.SuspendLayout()
        Me.gpbxCreditQuestion2.SuspendLayout()
        Me.gpbxCreditQuestion1.SuspendLayout()
        Me.gpbxCustomerName.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gpbxIdentity.SuspendLayout()
        CType(Me.pbPEP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxEmployment.SuspendLayout()
        Me.gpbxTenancy.SuspendLayout()
        CType(Me.TypeOfTenancyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEnquiryCustomers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryCustomersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxStatus.SuspendLayout()
        Me.pnlFooter.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        CType(Me.pbImportCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxManager.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlCustomerType.SuspendLayout()
        CType(Me.pbHA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ListAMLRiskBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.List2AMLRisk, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Enum_CustomerTypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerEnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCompany.SuspendLayout()
        Me.gpbxTradeRef.SuspendLayout()
        Me.drReferences.ItemTemplate.SuspendLayout()
        Me.drReferences.SuspendLayout()
        CType(Me.TradeReferencesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxBusOther.SuspendLayout()
        Me.gpbxDtsb23.SuspendLayout()
        Me.PanelDtsb23.SuspendLayout()
        CType(Me.pbPEP2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpShareholders.SuspendLayout()
        CType(Me.dgvDTSBType2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DTSBBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxBusinessCredit.SuspendLayout()
        Me.gpbxBusCreditQuestion3.SuspendLayout()
        Me.gpbxBusCreditQuestion2.SuspendLayout()
        Me.gpbxBusCreditQuestion1.SuspendLayout()
        Me.gpbxCoTenancy.SuspendLayout()
        Me.tlpCompanyTenancy.SuspendLayout()
        Me.gpbxDtsb01.SuspendLayout()
        Me.PanelDtsb01.SuspendLayout()
        CType(Me.dgvDtsb01, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DTSB01BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpDirectors.SuspendLayout()
        Me.gpbxCompanyInfo.SuspendLayout()
        Me.tlpInformation.SuspendLayout()
        CType(Me.EnumAMLRiskBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTradeRefName
        '
        lblTradeRefName.AutoSize = True
        lblTradeRefName.Location = New System.Drawing.Point(162, 26)
        lblTradeRefName.Name = "lblTradeRefName"
        lblTradeRefName.Size = New System.Drawing.Size(83, 13)
        lblTradeRefName.TabIndex = 0
        lblTradeRefName.Text = "Business Name:"
        '
        'lblTradeRefAccount
        '
        lblTradeRefAccount.AutoSize = True
        lblTradeRefAccount.Location = New System.Drawing.Point(33, 52)
        lblTradeRefAccount.Name = "lblTradeRefAccount"
        lblTradeRefAccount.Size = New System.Drawing.Size(212, 13)
        lblTradeRefAccount.TabIndex = 4
        lblTradeRefAccount.Text = "Due they have a current account with you?"
        '
        'lblTradeRefSpend
        '
        lblTradeRefSpend.AutoSize = True
        lblTradeRefSpend.Location = New System.Drawing.Point(72, 78)
        lblTradeRefSpend.Name = "lblTradeRefSpend"
        lblTradeRefSpend.Size = New System.Drawing.Size(173, 13)
        lblTradeRefSpend.TabIndex = 6
        lblTradeRefSpend.Text = "How much do they spend monthly?"
        '
        'lblTradeRefPay
        '
        lblTradeRefPay.AutoSize = True
        lblTradeRefPay.Location = New System.Drawing.Point(138, 104)
        lblTradeRefPay.Name = "lblTradeRefPay"
        lblTradeRefPay.Size = New System.Drawing.Size(107, 13)
        lblTradeRefPay.TabIndex = 8
        lblTradeRefPay.Text = "Do they pay on time?"
        '
        'lblTradeRefComments
        '
        lblTradeRefComments.AutoSize = True
        lblTradeRefComments.Location = New System.Drawing.Point(47, 135)
        lblTradeRefComments.Name = "lblTradeRefComments"
        lblTradeRefComments.Size = New System.Drawing.Size(192, 13)
        lblTradeRefComments.TabIndex = 10
        lblTradeRefComments.Text = "Other comments about this relationship:"
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(3, 987)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(938, 22)
        Me.StatusStripMessage.TabIndex = 49
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'pnlIndividual
        '
        Me.pnlIndividual.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlIndividual.AutoScroll = True
        Me.pnlIndividual.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.pnlIndividual.Controls.Add(Me.rtxtbxStabilityOther)
        Me.pnlIndividual.Controls.Add(Me.gpbxCredit)
        Me.pnlIndividual.Controls.Add(Me.gpbxCustomerName)
        Me.pnlIndividual.Controls.Add(Me.gpbxIdentity)
        Me.pnlIndividual.Controls.Add(Me.gpbxEmployment)
        Me.pnlIndividual.Controls.Add(Me.gpbxTenancy)
        Me.pnlIndividual.Controls.Add(Me.lblOther)
        Me.pnlIndividual.Location = New System.Drawing.Point(3, 234)
        Me.pnlIndividual.Name = "pnlIndividual"
        Me.pnlIndividual.Size = New System.Drawing.Size(937, 715)
        Me.pnlIndividual.TabIndex = 50
        '
        'rtxtbxStabilityOther
        '
        Me.rtxtbxStabilityOther.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityComments", True))
        Me.rtxtbxStabilityOther.IsReadOnly = False
        Me.rtxtbxStabilityOther.IsUndoEnabled = True
        Me.rtxtbxStabilityOther.Location = New System.Drawing.Point(20, 791)
        Me.rtxtbxStabilityOther.MaxLength = 0
        Me.rtxtbxStabilityOther.MultiLine = True
        Me.rtxtbxStabilityOther.Name = "rtxtbxStabilityOther"
        Me.rtxtbxStabilityOther.SelectedText = ""
        Me.rtxtbxStabilityOther.SelectionLength = 0
        Me.rtxtbxStabilityOther.SelectionStart = 0
        Me.rtxtbxStabilityOther.Size = New System.Drawing.Size(885, 55)
        Me.rtxtbxStabilityOther.TabIndex = 15
        Me.rtxtbxStabilityOther.WordWrap = True
        Me.rtxtbxStabilityOther.Child = New System.Windows.Controls.TextBox()
        '
        'CustomerBindingSource
        '
        Me.CustomerBindingSource.DataMember = "Customer"
        Me.CustomerBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'gpbxCredit
        '
        Me.gpbxCredit.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCredit.Controls.Add(Me.txtbxCreditPPSR)
        Me.gpbxCredit.Controls.Add(Me.txtbxCreditDefaults)
        Me.gpbxCredit.Controls.Add(Me.txtbxCreditHistory)
        Me.gpbxCredit.Controls.Add(Me.gpbxCreditQuestion3)
        Me.gpbxCredit.Controls.Add(Me.gpbxCreditQuestion2)
        Me.gpbxCredit.Controls.Add(Me.gpbxCreditQuestion1)
        Me.gpbxCredit.Controls.Add(Me.lblCreditCheck)
        Me.gpbxCredit.Controls.Add(Me.lblCreditHistory)
        Me.gpbxCredit.Controls.Add(Me.lblCreditDefaults)
        Me.gpbxCredit.Controls.Add(Me.lblCreditPPSR)
        Me.gpbxCredit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCredit.Location = New System.Drawing.Point(12, 862)
        Me.gpbxCredit.Name = "gpbxCredit"
        Me.gpbxCredit.Size = New System.Drawing.Size(905, 384)
        Me.gpbxCredit.TabIndex = 135
        Me.gpbxCredit.TabStop = False
        Me.gpbxCredit.Text = "Credit"
        '
        'txtbxCreditPPSR
        '
        Me.txtbxCreditPPSR.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CreditOther", True))
        Me.txtbxCreditPPSR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxCreditPPSR.IsReadOnly = False
        Me.txtbxCreditPPSR.IsUndoEnabled = True
        Me.txtbxCreditPPSR.Location = New System.Drawing.Point(13, 233)
        Me.txtbxCreditPPSR.MaxLength = 0
        Me.txtbxCreditPPSR.MultiLine = True
        Me.txtbxCreditPPSR.Name = "txtbxCreditPPSR"
        Me.txtbxCreditPPSR.SelectedText = ""
        Me.txtbxCreditPPSR.SelectionLength = 0
        Me.txtbxCreditPPSR.SelectionStart = 0
        Me.txtbxCreditPPSR.Size = New System.Drawing.Size(885, 65)
        Me.txtbxCreditPPSR.TabIndex = 24
        Me.txtbxCreditPPSR.WordWrap = True
        Me.txtbxCreditPPSR.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxCreditDefaults
        '
        Me.txtbxCreditDefaults.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CreditExplanation", True))
        Me.txtbxCreditDefaults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxCreditDefaults.IsReadOnly = False
        Me.txtbxCreditDefaults.IsUndoEnabled = True
        Me.txtbxCreditDefaults.Location = New System.Drawing.Point(13, 148)
        Me.txtbxCreditDefaults.MaxLength = 0
        Me.txtbxCreditDefaults.MultiLine = True
        Me.txtbxCreditDefaults.Name = "txtbxCreditDefaults"
        Me.txtbxCreditDefaults.SelectedText = ""
        Me.txtbxCreditDefaults.SelectionLength = 0
        Me.txtbxCreditDefaults.SelectionStart = 0
        Me.txtbxCreditDefaults.Size = New System.Drawing.Size(885, 65)
        Me.txtbxCreditDefaults.TabIndex = 22
        Me.txtbxCreditDefaults.WordWrap = True
        Me.txtbxCreditDefaults.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxCreditHistory
        '
        Me.txtbxCreditHistory.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CreditComments", True))
        Me.txtbxCreditHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxCreditHistory.IsReadOnly = False
        Me.txtbxCreditHistory.IsUndoEnabled = True
        Me.txtbxCreditHistory.Location = New System.Drawing.Point(13, 63)
        Me.txtbxCreditHistory.MaxLength = 0
        Me.txtbxCreditHistory.MultiLine = True
        Me.txtbxCreditHistory.Name = "txtbxCreditHistory"
        Me.txtbxCreditHistory.SelectedText = ""
        Me.txtbxCreditHistory.SelectionLength = 0
        Me.txtbxCreditHistory.SelectionStart = 0
        Me.txtbxCreditHistory.Size = New System.Drawing.Size(885, 65)
        Me.txtbxCreditHistory.TabIndex = 20
        Me.txtbxCreditHistory.WordWrap = True
        Me.txtbxCreditHistory.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxCreditQuestion3
        '
        Me.gpbxCreditQuestion3.Controls.Add(Me.rbtnNoCreditQuestion3)
        Me.gpbxCreditQuestion3.Controls.Add(Me.rbtnYesCreditQuestion3)
        Me.gpbxCreditQuestion3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCreditQuestion3.Location = New System.Drawing.Point(610, 301)
        Me.gpbxCreditQuestion3.Name = "gpbxCreditQuestion3"
        Me.gpbxCreditQuestion3.Size = New System.Drawing.Size(284, 66)
        Me.gpbxCreditQuestion3.TabIndex = 27
        Me.gpbxCreditQuestion3.TabStop = False
        Me.gpbxCreditQuestion3.Text = "Are there Gaps in the credit history?"
        '
        'rbtnNoCreditQuestion3
        '
        Me.rbtnNoCreditQuestion3.AutoSize = True
        Me.rbtnNoCreditQuestion3.Location = New System.Drawing.Point(9, 42)
        Me.rbtnNoCreditQuestion3.Name = "rbtnNoCreditQuestion3"
        Me.rbtnNoCreditQuestion3.Size = New System.Drawing.Size(37, 17)
        Me.rbtnNoCreditQuestion3.TabIndex = 1
        Me.rbtnNoCreditQuestion3.TabStop = True
        Me.rbtnNoCreditQuestion3.Text = "no"
        Me.rbtnNoCreditQuestion3.UseVisualStyleBackColor = True
        '
        'rbtnYesCreditQuestion3
        '
        Me.rbtnYesCreditQuestion3.AutoSize = True
        Me.rbtnYesCreditQuestion3.Location = New System.Drawing.Point(9, 19)
        Me.rbtnYesCreditQuestion3.Name = "rbtnYesCreditQuestion3"
        Me.rbtnYesCreditQuestion3.Size = New System.Drawing.Size(41, 17)
        Me.rbtnYesCreditQuestion3.TabIndex = 0
        Me.rbtnYesCreditQuestion3.TabStop = True
        Me.rbtnYesCreditQuestion3.Text = "yes"
        Me.rbtnYesCreditQuestion3.UseVisualStyleBackColor = True
        '
        'gpbxCreditQuestion2
        '
        Me.gpbxCreditQuestion2.Controls.Add(Me.rbtnNoCreditQuestion2)
        Me.gpbxCreditQuestion2.Controls.Add(Me.rbtnYesCreditQuestion2)
        Me.gpbxCreditQuestion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCreditQuestion2.Location = New System.Drawing.Point(317, 301)
        Me.gpbxCreditQuestion2.Name = "gpbxCreditQuestion2"
        Me.gpbxCreditQuestion2.Size = New System.Drawing.Size(284, 66)
        Me.gpbxCreditQuestion2.TabIndex = 26
        Me.gpbxCreditQuestion2.TabStop = False
        Me.gpbxCreditQuestion2.Text = "Are all enquiries expected and accounted for?"
        '
        'rbtnNoCreditQuestion2
        '
        Me.rbtnNoCreditQuestion2.AutoSize = True
        Me.rbtnNoCreditQuestion2.Location = New System.Drawing.Point(6, 42)
        Me.rbtnNoCreditQuestion2.Name = "rbtnNoCreditQuestion2"
        Me.rbtnNoCreditQuestion2.Size = New System.Drawing.Size(37, 17)
        Me.rbtnNoCreditQuestion2.TabIndex = 1
        Me.rbtnNoCreditQuestion2.TabStop = True
        Me.rbtnNoCreditQuestion2.Text = "no"
        Me.rbtnNoCreditQuestion2.UseVisualStyleBackColor = True
        '
        'rbtnYesCreditQuestion2
        '
        Me.rbtnYesCreditQuestion2.AutoSize = True
        Me.rbtnYesCreditQuestion2.Location = New System.Drawing.Point(6, 19)
        Me.rbtnYesCreditQuestion2.Name = "rbtnYesCreditQuestion2"
        Me.rbtnYesCreditQuestion2.Size = New System.Drawing.Size(41, 17)
        Me.rbtnYesCreditQuestion2.TabIndex = 0
        Me.rbtnYesCreditQuestion2.TabStop = True
        Me.rbtnYesCreditQuestion2.Text = "yes"
        Me.rbtnYesCreditQuestion2.UseVisualStyleBackColor = True
        '
        'gpbxCreditQuestion1
        '
        Me.gpbxCreditQuestion1.Controls.Add(Me.rbtnNoCreditQuestion1)
        Me.gpbxCreditQuestion1.Controls.Add(Me.rbtnYesCreditQuestion1)
        Me.gpbxCreditQuestion1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCreditQuestion1.Location = New System.Drawing.Point(24, 301)
        Me.gpbxCreditQuestion1.Name = "gpbxCreditQuestion1"
        Me.gpbxCreditQuestion1.Size = New System.Drawing.Size(284, 66)
        Me.gpbxCreditQuestion1.TabIndex = 25
        Me.gpbxCreditQuestion1.TabStop = False
        Me.gpbxCreditQuestion1.Text = "Is Credit History sufficent?"
        '
        'rbtnNoCreditQuestion1
        '
        Me.rbtnNoCreditQuestion1.AutoSize = True
        Me.rbtnNoCreditQuestion1.Location = New System.Drawing.Point(9, 42)
        Me.rbtnNoCreditQuestion1.Name = "rbtnNoCreditQuestion1"
        Me.rbtnNoCreditQuestion1.Size = New System.Drawing.Size(37, 17)
        Me.rbtnNoCreditQuestion1.TabIndex = 1
        Me.rbtnNoCreditQuestion1.TabStop = True
        Me.rbtnNoCreditQuestion1.Text = "no"
        Me.rbtnNoCreditQuestion1.UseVisualStyleBackColor = True
        '
        'rbtnYesCreditQuestion1
        '
        Me.rbtnYesCreditQuestion1.AutoSize = True
        Me.rbtnYesCreditQuestion1.Location = New System.Drawing.Point(9, 19)
        Me.rbtnYesCreditQuestion1.Name = "rbtnYesCreditQuestion1"
        Me.rbtnYesCreditQuestion1.Size = New System.Drawing.Size(41, 17)
        Me.rbtnYesCreditQuestion1.TabIndex = 0
        Me.rbtnYesCreditQuestion1.TabStop = True
        Me.rbtnYesCreditQuestion1.Text = "yes"
        Me.rbtnYesCreditQuestion1.UseVisualStyleBackColor = True
        '
        'lblCreditCheck
        '
        Me.lblCreditCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditCheck.Location = New System.Drawing.Point(224, 18)
        Me.lblCreditCheck.Name = "lblCreditCheck"
        Me.lblCreditCheck.Size = New System.Drawing.Size(470, 26)
        Me.lblCreditCheck.TabIndex = 18
        Me.lblCreditCheck.Text = "Veda: Check Enquiries; 1st Enquiry?   Less than 5 Enquiries per year?   Who are t" & _
    "hey to?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Finance Companies?   Consistant to what declared?"
        Me.lblCreditCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblCreditHistory
        '
        Me.lblCreditHistory.AutoSize = True
        Me.lblCreditHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditHistory.Location = New System.Drawing.Point(18, 46)
        Me.lblCreditHistory.Name = "lblCreditHistory"
        Me.lblCreditHistory.Size = New System.Drawing.Size(167, 13)
        Me.lblCreditHistory.TabIndex = 19
        Me.lblCreditHistory.Text = "Comments on History - work notes"
        '
        'lblCreditDefaults
        '
        Me.lblCreditDefaults.AutoSize = True
        Me.lblCreditDefaults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditDefaults.Location = New System.Drawing.Point(18, 131)
        Me.lblCreditDefaults.Name = "lblCreditDefaults"
        Me.lblCreditDefaults.Size = New System.Drawing.Size(166, 13)
        Me.lblCreditDefaults.TabIndex = 21
        Me.lblCreditDefaults.Text = "Defaults / Collection Explanations"
        '
        'lblCreditPPSR
        '
        Me.lblCreditPPSR.AutoSize = True
        Me.lblCreditPPSR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditPPSR.Location = New System.Drawing.Point(18, 216)
        Me.lblCreditPPSR.Name = "lblCreditPPSR"
        Me.lblCreditPPSR.Size = New System.Drawing.Size(125, 13)
        Me.lblCreditPPSR.TabIndex = 23
        Me.lblCreditPPSR.Text = "PPSR and Other Checks"
        '
        'gpbxCustomerName
        '
        Me.gpbxCustomerName.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCustomerName.Controls.Add(Me.TableLayoutPanel1)
        Me.gpbxCustomerName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCustomerName.Location = New System.Drawing.Point(12, 3)
        Me.gpbxCustomerName.Name = "gpbxCustomerName"
        Me.gpbxCustomerName.Size = New System.Drawing.Size(902, 57)
        Me.gpbxCustomerName.TabIndex = 133
        Me.gpbxCustomerName.TabStop = False
        Me.gpbxCustomerName.Text = "Customer name"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 9
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblCustomerTitle, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbxCustomerTitle, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblFirstName, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtbxFirstName, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMiddleNames, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblLastName, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLastName, 6, 0)
        Me.TableLayoutPanel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 19)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(890, 30)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lblCustomerTitle
        '
        Me.lblCustomerTitle.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblCustomerTitle.AutoSize = True
        Me.lblCustomerTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomerTitle.Location = New System.Drawing.Point(5, 8)
        Me.lblCustomerTitle.Name = "lblCustomerTitle"
        Me.lblCustomerTitle.Size = New System.Drawing.Size(27, 13)
        Me.lblCustomerTitle.TabIndex = 0
        Me.lblCustomerTitle.Text = "Title"
        Me.lblCustomerTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbxCustomerTitle
        '
        Me.cmbxCustomerTitle.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmbxCustomerTitle.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "Title", True))
        Me.cmbxCustomerTitle.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CustomerBindingSource, "Title", True))
        Me.cmbxCustomerTitle.FormattingEnabled = True
        Me.cmbxCustomerTitle.Items.AddRange(New Object() {"Mr", "Mrs", "Ms", "Miss", "Dr"})
        Me.cmbxCustomerTitle.Location = New System.Drawing.Point(38, 4)
        Me.cmbxCustomerTitle.Name = "cmbxCustomerTitle"
        Me.cmbxCustomerTitle.Size = New System.Drawing.Size(60, 21)
        Me.cmbxCustomerTitle.TabIndex = 1
        '
        'lblFirstName
        '
        Me.lblFirstName.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(112, 8)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(60, 13)
        Me.lblFirstName.TabIndex = 2
        Me.lblFirstName.Text = "First names"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxFirstName
        '
        Me.txtbxFirstName.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.txtbxFirstName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "FirstName", True))
        Me.txtbxFirstName.Location = New System.Drawing.Point(178, 5)
        Me.txtbxFirstName.Name = "txtbxFirstName"
        Me.txtbxFirstName.Size = New System.Drawing.Size(112, 20)
        Me.txtbxFirstName.TabIndex = 3
        '
        'txtMiddleNames
        '
        Me.txtMiddleNames.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.txtMiddleNames.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "MiddleNames", True))
        Me.txtMiddleNames.Location = New System.Drawing.Point(298, 5)
        Me.txtMiddleNames.Name = "txtMiddleNames"
        Me.txtMiddleNames.Size = New System.Drawing.Size(112, 20)
        Me.txtMiddleNames.TabIndex = 4
        '
        'lblLastName
        '
        Me.lblLastName.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Location = New System.Drawing.Point(426, 8)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(56, 13)
        Me.lblLastName.TabIndex = 5
        Me.lblLastName.Text = "Last name"
        '
        'txtLastName
        '
        Me.txtLastName.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.txtLastName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "LastName", True))
        Me.txtLastName.Location = New System.Drawing.Point(488, 5)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(138, 20)
        Me.txtLastName.TabIndex = 11
        '
        'gpbxIdentity
        '
        Me.gpbxIdentity.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxIdentity.Controls.Add(Me.cmbxTypeDL)
        Me.gpbxIdentity.Controls.Add(Me.lblTypeDL)
        Me.gpbxIdentity.Controls.Add(Me.cmbxTypeOfId)
        Me.gpbxIdentity.Controls.Add(Me.lblTypeOfId)
        Me.gpbxIdentity.Controls.Add(Me.txtAgeOfId)
        Me.gpbxIdentity.Controls.Add(Me.txtResidency)
        Me.gpbxIdentity.Controls.Add(Me.txtIdIsPerson)
        Me.gpbxIdentity.Controls.Add(Me.rbPepNo)
        Me.gpbxIdentity.Controls.Add(Me.rbPepYes)
        Me.gpbxIdentity.Controls.Add(Me.lblPep)
        Me.gpbxIdentity.Controls.Add(Me.pbPEP)
        Me.gpbxIdentity.Controls.Add(Me.cmbxRiskAssess)
        Me.gpbxIdentity.Controls.Add(Me.lblRiskAssess)
        Me.gpbxIdentity.Controls.Add(Me.lblAgeOfId)
        Me.gpbxIdentity.Controls.Add(Me.lblResidency)
        Me.gpbxIdentity.Controls.Add(Me.lblIdIsPerson)
        Me.gpbxIdentity.Controls.Add(Me.ckbxIdCurrent)
        Me.gpbxIdentity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxIdentity.Location = New System.Drawing.Point(12, 74)
        Me.gpbxIdentity.Name = "gpbxIdentity"
        Me.gpbxIdentity.Size = New System.Drawing.Size(905, 179)
        Me.gpbxIdentity.TabIndex = 125
        Me.gpbxIdentity.TabStop = False
        Me.gpbxIdentity.Text = "Identity Appraisal"
        '
        'cmbxTypeDL
        '
        Me.cmbxTypeDL.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxTypeDL.FormattingEnabled = True
        Me.cmbxTypeDL.Location = New System.Drawing.Point(458, 17)
        Me.cmbxTypeDL.Name = "cmbxTypeDL"
        Me.cmbxTypeDL.Size = New System.Drawing.Size(121, 21)
        Me.cmbxTypeDL.TabIndex = 2
        Me.cmbxTypeDL.Visible = False
        '
        'lblTypeDL
        '
        Me.lblTypeDL.AutoSize = True
        Me.lblTypeDL.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeDL.Location = New System.Drawing.Point(330, 20)
        Me.lblTypeDL.Name = "lblTypeDL"
        Me.lblTypeDL.Size = New System.Drawing.Size(116, 13)
        Me.lblTypeDL.TabIndex = 17
        Me.lblTypeDL.Text = "Type of driver's licence"
        Me.lblTypeDL.Visible = False
        '
        'cmbxTypeOfId
        '
        Me.cmbxTypeOfId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxTypeOfId.FormattingEnabled = True
        Me.cmbxTypeOfId.Location = New System.Drawing.Point(89, 17)
        Me.cmbxTypeOfId.Name = "cmbxTypeOfId"
        Me.cmbxTypeOfId.Size = New System.Drawing.Size(121, 21)
        Me.cmbxTypeOfId.TabIndex = 1
        '
        'lblTypeOfId
        '
        Me.lblTypeOfId.AutoSize = True
        Me.lblTypeOfId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOfId.Location = New System.Drawing.Point(13, 20)
        Me.lblTypeOfId.Name = "lblTypeOfId"
        Me.lblTypeOfId.Size = New System.Drawing.Size(57, 13)
        Me.lblTypeOfId.TabIndex = 15
        Me.lblTypeOfId.Text = "Type of ID"
        '
        'txtAgeOfId
        '
        Me.txtAgeOfId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "IDLegitimate", True))
        Me.txtAgeOfId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgeOfId.IsReadOnly = False
        Me.txtAgeOfId.IsUndoEnabled = True
        Me.txtAgeOfId.Location = New System.Drawing.Point(333, 115)
        Me.txtAgeOfId.MaxLength = 512
        Me.txtAgeOfId.MultiLine = True
        Me.txtAgeOfId.Name = "txtAgeOfId"
        Me.txtAgeOfId.SelectedText = ""
        Me.txtAgeOfId.SelectionLength = 0
        Me.txtAgeOfId.SelectionStart = 0
        Me.txtAgeOfId.Size = New System.Drawing.Size(565, 30)
        Me.txtAgeOfId.TabIndex = 8
        Me.txtAgeOfId.WordWrap = True
        Me.txtAgeOfId.Child = New System.Windows.Controls.TextBox()
        '
        'txtResidency
        '
        Me.txtResidency.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "ResStatusAccept", True))
        Me.txtResidency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResidency.IsReadOnly = False
        Me.txtResidency.IsUndoEnabled = True
        Me.txtResidency.Location = New System.Drawing.Point(333, 89)
        Me.txtResidency.MaxLength = 255
        Me.txtResidency.Name = "txtResidency"
        Me.txtResidency.SelectedText = ""
        Me.txtResidency.SelectionLength = 0
        Me.txtResidency.SelectionStart = 0
        Me.txtResidency.Size = New System.Drawing.Size(565, 20)
        Me.txtResidency.TabIndex = 7
        Me.txtResidency.Child = New System.Windows.Controls.TextBox()
        '
        'txtIdIsPerson
        '
        Me.txtIdIsPerson.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "IDPerson", True))
        Me.txtIdIsPerson.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdIsPerson.IsReadOnly = False
        Me.txtIdIsPerson.IsUndoEnabled = True
        Me.txtIdIsPerson.Location = New System.Drawing.Point(333, 63)
        Me.txtIdIsPerson.MaxLength = 255
        Me.txtIdIsPerson.Name = "txtIdIsPerson"
        Me.txtIdIsPerson.SelectedText = ""
        Me.txtIdIsPerson.SelectionLength = 0
        Me.txtIdIsPerson.SelectionStart = 0
        Me.txtIdIsPerson.Size = New System.Drawing.Size(565, 20)
        Me.txtIdIsPerson.TabIndex = 6
        Me.txtIdIsPerson.Child = New System.Windows.Controls.TextBox()
        '
        'rbPepNo
        '
        Me.rbPepNo.AutoSize = True
        Me.rbPepNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPepNo.Location = New System.Drawing.Point(558, 42)
        Me.rbPepNo.Name = "rbPepNo"
        Me.rbPepNo.Size = New System.Drawing.Size(37, 17)
        Me.rbPepNo.TabIndex = 5
        Me.rbPepNo.TabStop = True
        Me.rbPepNo.Text = "no"
        Me.rbPepNo.UseVisualStyleBackColor = True
        '
        'rbPepYes
        '
        Me.rbPepYes.AutoSize = True
        Me.rbPepYes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPepYes.Location = New System.Drawing.Point(502, 42)
        Me.rbPepYes.Name = "rbPepYes"
        Me.rbPepYes.Size = New System.Drawing.Size(41, 17)
        Me.rbPepYes.TabIndex = 4
        Me.rbPepYes.TabStop = True
        Me.rbPepYes.Text = "yes"
        Me.rbPepYes.UseVisualStyleBackColor = True
        '
        'lblPep
        '
        Me.lblPep.AutoSize = True
        Me.lblPep.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPep.Location = New System.Drawing.Point(336, 44)
        Me.lblPep.Name = "lblPep"
        Me.lblPep.Size = New System.Drawing.Size(152, 13)
        Me.lblPep.TabIndex = 12
        Me.lblPep.Text = "is a politically exposed person?"
        '
        'pbPEP
        '
        Me.pbPEP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbPEP.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbPEP.Location = New System.Drawing.Point(881, 41)
        Me.pbPEP.Name = "pbPEP"
        Me.pbPEP.Size = New System.Drawing.Size(18, 18)
        Me.pbPEP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbPEP.TabIndex = 11
        Me.pbPEP.TabStop = False
        '
        'cmbxRiskAssess
        '
        Me.cmbxRiskAssess.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxRiskAssess.FormattingEnabled = True
        Me.cmbxRiskAssess.Location = New System.Drawing.Point(333, 151)
        Me.cmbxRiskAssess.Name = "cmbxRiskAssess"
        Me.cmbxRiskAssess.Size = New System.Drawing.Size(121, 21)
        Me.cmbxRiskAssess.TabIndex = 10
        '
        'lblRiskAssess
        '
        Me.lblRiskAssess.AutoSize = True
        Me.lblRiskAssess.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRiskAssess.Location = New System.Drawing.Point(7, 154)
        Me.lblRiskAssess.Name = "lblRiskAssess"
        Me.lblRiskAssess.Size = New System.Drawing.Size(315, 13)
        Me.lblRiskAssess.TabIndex = 9
        Me.lblRiskAssess.Text = "Based on the above my AML/CFT risk assessment of this client is"
        '
        'lblAgeOfId
        '
        Me.lblAgeOfId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgeOfId.Location = New System.Drawing.Point(22, 115)
        Me.lblAgeOfId.Name = "lblAgeOfId"
        Me.lblAgeOfId.Size = New System.Drawing.Size(292, 30)
        Me.lblAgeOfId.TabIndex = 3
        Me.lblAgeOfId.Text = "Taking into account the age of the ID and Client I am satisfied it is legitimate " & _
    "because"
        Me.lblAgeOfId.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblResidency
        '
        Me.lblResidency.AutoSize = True
        Me.lblResidency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidency.Location = New System.Drawing.Point(94, 92)
        Me.lblResidency.Name = "lblResidency"
        Me.lblResidency.Size = New System.Drawing.Size(220, 13)
        Me.lblResidency.TabIndex = 2
        Me.lblResidency.Text = "Their residency status is acceptable because"
        Me.lblResidency.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblIdIsPerson
        '
        Me.lblIdIsPerson.AutoSize = True
        Me.lblIdIsPerson.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdIsPerson.Location = New System.Drawing.Point(88, 66)
        Me.lblIdIsPerson.Name = "lblIdIsPerson"
        Me.lblIdIsPerson.Size = New System.Drawing.Size(208, 13)
        Me.lblIdIsPerson.TabIndex = 1
        Me.lblIdIsPerson.Text = "I am satisfied the ID is this person because"
        Me.lblIdIsPerson.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ckbxIdCurrent
        '
        Me.ckbxIdCurrent.AutoSize = True
        Me.ckbxIdCurrent.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CustomerBindingSource, "IDCurrent", True))
        Me.ckbxIdCurrent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxIdCurrent.Location = New System.Drawing.Point(12, 43)
        Me.ckbxIdCurrent.Name = "ckbxIdCurrent"
        Me.ckbxIdCurrent.Size = New System.Drawing.Size(168, 17)
        Me.ckbxIdCurrent.TabIndex = 3
        Me.ckbxIdCurrent.Text = "The ID documents are current"
        Me.ckbxIdCurrent.UseVisualStyleBackColor = True
        '
        'gpbxEmployment
        '
        Me.gpbxEmployment.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion8)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion7)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion6)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion4)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion3)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion9)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion2)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion1)
        Me.gpbxEmployment.Controls.Add(Me.txtbxEmployer)
        Me.gpbxEmployment.Controls.Add(Me.lblEmployer)
        Me.gpbxEmployment.Controls.Add(Me.lblSpokeTo)
        Me.gpbxEmployment.Controls.Add(Me.btnRangEmpTimeStamp)
        Me.gpbxEmployment.Controls.Add(Me.lblRangEmpTimeStamp)
        Me.gpbxEmployment.Controls.Add(Me.txtbxSpokeTo)
        Me.gpbxEmployment.Controls.Add(Me.lblEmploy9)
        Me.gpbxEmployment.Controls.Add(Me.lblDoes)
        Me.gpbxEmployment.Controls.Add(Me.lblEmployerNotRung)
        Me.gpbxEmployment.Controls.Add(Me.lblSalutation)
        Me.gpbxEmployment.Controls.Add(Me.ckbxEmployerNotRung)
        Me.gpbxEmployment.Controls.Add(Me.lblWorkCompany)
        Me.gpbxEmployment.Controls.Add(Me.lblBeneficiary)
        Me.gpbxEmployment.Controls.Add(Me.lblFulltime)
        Me.gpbxEmployment.Controls.Add(Me.ckbxBeneficiary)
        Me.gpbxEmployment.Controls.Add(Me.lblWhatDo)
        Me.gpbxEmployment.Controls.Add(Me.lblHowLong)
        Me.gpbxEmployment.Controls.Add(Me.lblPay)
        Me.gpbxEmployment.Controls.Add(Me.txtbxQuestion5)
        Me.gpbxEmployment.Controls.Add(Me.lblWorker)
        Me.gpbxEmployment.Controls.Add(Me.lblJobSafe)
        Me.gpbxEmployment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxEmployment.Location = New System.Drawing.Point(12, 441)
        Me.gpbxEmployment.Name = "gpbxEmployment"
        Me.gpbxEmployment.Size = New System.Drawing.Size(905, 313)
        Me.gpbxEmployment.TabIndex = 7
        Me.gpbxEmployment.TabStop = False
        Me.gpbxEmployment.Text = "Employment"
        '
        'txtbxQuestion8
        '
        Me.txtbxQuestion8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ8", True))
        Me.txtbxQuestion8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion8.IsReadOnly = False
        Me.txtbxQuestion8.IsUndoEnabled = True
        Me.txtbxQuestion8.Location = New System.Drawing.Point(333, 226)
        Me.txtbxQuestion8.MaxLength = 50
        Me.txtbxQuestion8.Name = "txtbxQuestion8"
        Me.txtbxQuestion8.SelectedText = ""
        Me.txtbxQuestion8.SelectionLength = 0
        Me.txtbxQuestion8.SelectionStart = 0
        Me.txtbxQuestion8.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion8.TabIndex = 13
        Me.txtbxQuestion8.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion7
        '
        Me.txtbxQuestion7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ7", True))
        Me.txtbxQuestion7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion7.IsReadOnly = False
        Me.txtbxQuestion7.IsUndoEnabled = True
        Me.txtbxQuestion7.Location = New System.Drawing.Point(333, 200)
        Me.txtbxQuestion7.MaxLength = 50
        Me.txtbxQuestion7.Name = "txtbxQuestion7"
        Me.txtbxQuestion7.SelectedText = ""
        Me.txtbxQuestion7.SelectionLength = 0
        Me.txtbxQuestion7.SelectionStart = 0
        Me.txtbxQuestion7.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion7.TabIndex = 12
        Me.txtbxQuestion7.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion6
        '
        Me.txtbxQuestion6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ6", True))
        Me.txtbxQuestion6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion6.IsReadOnly = False
        Me.txtbxQuestion6.IsUndoEnabled = True
        Me.txtbxQuestion6.Location = New System.Drawing.Point(333, 174)
        Me.txtbxQuestion6.MaxLength = 50
        Me.txtbxQuestion6.Name = "txtbxQuestion6"
        Me.txtbxQuestion6.SelectedText = ""
        Me.txtbxQuestion6.SelectionLength = 0
        Me.txtbxQuestion6.SelectionStart = 0
        Me.txtbxQuestion6.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion6.TabIndex = 11
        Me.txtbxQuestion6.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion4
        '
        Me.txtbxQuestion4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ4", True))
        Me.txtbxQuestion4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion4.IsReadOnly = False
        Me.txtbxQuestion4.IsUndoEnabled = True
        Me.txtbxQuestion4.Location = New System.Drawing.Point(333, 148)
        Me.txtbxQuestion4.MaxLength = 50
        Me.txtbxQuestion4.Name = "txtbxQuestion4"
        Me.txtbxQuestion4.SelectedText = ""
        Me.txtbxQuestion4.SelectionLength = 0
        Me.txtbxQuestion4.SelectionStart = 0
        Me.txtbxQuestion4.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion4.TabIndex = 9
        Me.txtbxQuestion4.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion3
        '
        Me.txtbxQuestion3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ3", True))
        Me.txtbxQuestion3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion3.IsReadOnly = False
        Me.txtbxQuestion3.IsUndoEnabled = True
        Me.txtbxQuestion3.Location = New System.Drawing.Point(333, 122)
        Me.txtbxQuestion3.MaxLength = 50
        Me.txtbxQuestion3.Name = "txtbxQuestion3"
        Me.txtbxQuestion3.SelectedText = ""
        Me.txtbxQuestion3.SelectionLength = 0
        Me.txtbxQuestion3.SelectionStart = 0
        Me.txtbxQuestion3.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion3.TabIndex = 8
        Me.txtbxQuestion3.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion9
        '
        Me.txtbxQuestion9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ9", True))
        Me.txtbxQuestion9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion9.IsReadOnly = False
        Me.txtbxQuestion9.IsUndoEnabled = True
        Me.txtbxQuestion9.Location = New System.Drawing.Point(333, 96)
        Me.txtbxQuestion9.MaxLength = 50
        Me.txtbxQuestion9.Name = "txtbxQuestion9"
        Me.txtbxQuestion9.SelectedText = ""
        Me.txtbxQuestion9.SelectionLength = 0
        Me.txtbxQuestion9.SelectionStart = 0
        Me.txtbxQuestion9.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion9.TabIndex = 7
        Me.txtbxQuestion9.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion2
        '
        Me.txtbxQuestion2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ2", True))
        Me.txtbxQuestion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion2.IsReadOnly = False
        Me.txtbxQuestion2.IsUndoEnabled = True
        Me.txtbxQuestion2.Location = New System.Drawing.Point(333, 70)
        Me.txtbxQuestion2.MaxLength = 50
        Me.txtbxQuestion2.Name = "txtbxQuestion2"
        Me.txtbxQuestion2.SelectedText = ""
        Me.txtbxQuestion2.SelectionLength = 0
        Me.txtbxQuestion2.SelectionStart = 0
        Me.txtbxQuestion2.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion2.TabIndex = 6
        Me.txtbxQuestion2.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxQuestion1
        '
        Me.txtbxQuestion1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ1", True))
        Me.txtbxQuestion1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion1.IsReadOnly = False
        Me.txtbxQuestion1.IsUndoEnabled = True
        Me.txtbxQuestion1.Location = New System.Drawing.Point(333, 45)
        Me.txtbxQuestion1.MaxLength = 50
        Me.txtbxQuestion1.Name = "txtbxQuestion1"
        Me.txtbxQuestion1.SelectedText = ""
        Me.txtbxQuestion1.SelectionLength = 0
        Me.txtbxQuestion1.SelectionStart = 0
        Me.txtbxQuestion1.Size = New System.Drawing.Size(565, 20)
        Me.txtbxQuestion1.TabIndex = 5
        Me.txtbxQuestion1.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxEmployer
        '
        Me.txtbxEmployer.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "EmployerName", True))
        Me.txtbxEmployer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxEmployer.Location = New System.Drawing.Point(91, 19)
        Me.txtbxEmployer.MaxLength = 60
        Me.txtbxEmployer.Name = "txtbxEmployer"
        Me.txtbxEmployer.Size = New System.Drawing.Size(121, 20)
        Me.txtbxEmployer.TabIndex = 1
        '
        'lblEmployer
        '
        Me.lblEmployer.AutoSize = True
        Me.lblEmployer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployer.Location = New System.Drawing.Point(9, 22)
        Me.lblEmployer.Name = "lblEmployer"
        Me.lblEmployer.Size = New System.Drawing.Size(82, 13)
        Me.lblEmployer.TabIndex = 53
        Me.lblEmployer.Text = "Rang Employer:"
        '
        'lblSpokeTo
        '
        Me.lblSpokeTo.AutoSize = True
        Me.lblSpokeTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpokeTo.Location = New System.Drawing.Point(214, 22)
        Me.lblSpokeTo.Name = "lblSpokeTo"
        Me.lblSpokeTo.Size = New System.Drawing.Size(53, 13)
        Me.lblSpokeTo.TabIndex = 56
        Me.lblSpokeTo.Text = "Spoke to:"
        '
        'btnRangEmpTimeStamp
        '
        Me.btnRangEmpTimeStamp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRangEmpTimeStamp.Location = New System.Drawing.Point(416, 17)
        Me.btnRangEmpTimeStamp.Name = "btnRangEmpTimeStamp"
        Me.btnRangEmpTimeStamp.Size = New System.Drawing.Size(75, 23)
        Me.btnRangEmpTimeStamp.TabIndex = 3
        Me.btnRangEmpTimeStamp.Text = "Timestamp"
        Me.btnRangEmpTimeStamp.UseVisualStyleBackColor = True
        '
        'lblRangEmpTimeStamp
        '
        Me.lblRangEmpTimeStamp.AutoSize = True
        Me.lblRangEmpTimeStamp.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "EmployerRangDate", True))
        Me.lblRangEmpTimeStamp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangEmpTimeStamp.Location = New System.Drawing.Point(499, 22)
        Me.lblRangEmpTimeStamp.Name = "lblRangEmpTimeStamp"
        Me.lblRangEmpTimeStamp.Size = New System.Drawing.Size(0, 13)
        Me.lblRangEmpTimeStamp.TabIndex = 106
        '
        'txtbxSpokeTo
        '
        Me.txtbxSpokeTo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "EmployerSpoketoName", True))
        Me.txtbxSpokeTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxSpokeTo.Location = New System.Drawing.Point(270, 19)
        Me.txtbxSpokeTo.MaxLength = 60
        Me.txtbxSpokeTo.Name = "txtbxSpokeTo"
        Me.txtbxSpokeTo.Size = New System.Drawing.Size(140, 20)
        Me.txtbxSpokeTo.TabIndex = 2
        '
        'lblEmploy9
        '
        Me.lblEmploy9.AutoSize = True
        Me.lblEmploy9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmploy9.Location = New System.Drawing.Point(48, 99)
        Me.lblEmploy9.Name = "lblEmploy9"
        Me.lblEmploy9.Size = New System.Drawing.Size(268, 13)
        Me.lblEmploy9.TabIndex = 103
        Me.lblEmploy9.Text = "Are they PAYE employee, Contractor, Invoice or Other?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblDoes
        '
        Me.lblDoes.AutoSize = True
        Me.lblDoes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDoes.Location = New System.Drawing.Point(9, 48)
        Me.lblDoes.Name = "lblDoes"
        Me.lblDoes.Size = New System.Drawing.Size(35, 13)
        Me.lblDoes.TabIndex = 59
        Me.lblDoes.Text = "Does "
        '
        'lblEmployerNotRung
        '
        Me.lblEmployerNotRung.AutoSize = True
        Me.lblEmployerNotRung.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerNotRung.Location = New System.Drawing.Point(147, 290)
        Me.lblEmployerNotRung.Name = "lblEmployerNotRung"
        Me.lblEmployerNotRung.Size = New System.Drawing.Size(280, 13)
        Me.lblEmployerNotRung.TabIndex = 124
        Me.lblEmployerNotRung.Text = "(Must ADD comments below as to why Employer not rung)"
        Me.lblEmployerNotRung.Visible = False
        '
        'lblSalutation
        '
        Me.lblSalutation.AutoSize = True
        Me.lblSalutation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalutation.Location = New System.Drawing.Point(51, 48)
        Me.lblSalutation.Name = "lblSalutation"
        Me.lblSalutation.Size = New System.Drawing.Size(83, 13)
        Me.lblSalutation.TabIndex = 60
        Me.lblSalutation.Text = "Client Salutation"
        '
        'ckbxEmployerNotRung
        '
        Me.ckbxEmployerNotRung.AutoSize = True
        Me.ckbxEmployerNotRung.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "EmployerNotRung", True))
        Me.ckbxEmployerNotRung.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxEmployerNotRung.Location = New System.Drawing.Point(12, 288)
        Me.ckbxEmployerNotRung.Name = "ckbxEmployerNotRung"
        Me.ckbxEmployerNotRung.Size = New System.Drawing.Size(111, 17)
        Me.ckbxEmployerNotRung.TabIndex = 123
        Me.ckbxEmployerNotRung.Text = "Employer not rung"
        Me.ckbxEmployerNotRung.UseVisualStyleBackColor = True
        Me.ckbxEmployerNotRung.Visible = False
        '
        'lblWorkCompany
        '
        Me.lblWorkCompany.AutoSize = True
        Me.lblWorkCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkCompany.Location = New System.Drawing.Point(195, 48)
        Me.lblWorkCompany.Name = "lblWorkCompany"
        Me.lblWorkCompany.Size = New System.Drawing.Size(121, 13)
        Me.lblWorkCompany.TabIndex = 61
        Me.lblWorkCompany.Text = "work for your Company?"
        '
        'lblBeneficiary
        '
        Me.lblBeneficiary.AutoSize = True
        Me.lblBeneficiary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBeneficiary.Location = New System.Drawing.Point(147, 267)
        Me.lblBeneficiary.Name = "lblBeneficiary"
        Me.lblBeneficiary.Size = New System.Drawing.Size(367, 13)
        Me.lblBeneficiary.TabIndex = 122
        Me.lblBeneficiary.Text = "(Must ADD comments below regarding confirmation and continuity of benefit)"
        '
        'lblFulltime
        '
        Me.lblFulltime.AutoSize = True
        Me.lblFulltime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFulltime.Location = New System.Drawing.Point(137, 73)
        Me.lblFulltime.Name = "lblFulltime"
        Me.lblFulltime.Size = New System.Drawing.Size(179, 13)
        Me.lblFulltime.TabIndex = 62
        Me.lblFulltime.Text = "Are they fulltime permanent or other?"
        '
        'ckbxBeneficiary
        '
        Me.ckbxBeneficiary.AutoSize = True
        Me.ckbxBeneficiary.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "Beneficiary", True))
        Me.ckbxBeneficiary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxBeneficiary.Location = New System.Drawing.Point(12, 265)
        Me.ckbxBeneficiary.Name = "ckbxBeneficiary"
        Me.ckbxBeneficiary.Size = New System.Drawing.Size(126, 17)
        Me.ckbxBeneficiary.TabIndex = 121
        Me.ckbxBeneficiary.Text = "Client is a Beneficiary"
        Me.ckbxBeneficiary.UseVisualStyleBackColor = True
        '
        'lblWhatDo
        '
        Me.lblWhatDo.AutoSize = True
        Me.lblWhatDo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWhatDo.Location = New System.Drawing.Point(139, 125)
        Me.lblWhatDo.Name = "lblWhatDo"
        Me.lblWhatDo.Size = New System.Drawing.Size(177, 13)
        Me.lblWhatDo.TabIndex = 64
        Me.lblWhatDo.Text = " What does your employee do (job)?"
        '
        'lblHowLong
        '
        Me.lblHowLong.AutoSize = True
        Me.lblHowLong.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHowLong.Location = New System.Drawing.Point(154, 151)
        Me.lblHowLong.Name = "lblHowLong"
        Me.lblHowLong.Size = New System.Drawing.Size(162, 13)
        Me.lblHowLong.TabIndex = 66
        Me.lblHowLong.Text = "How long have they been there?"
        '
        'lblPay
        '
        Me.lblPay.AutoSize = True
        Me.lblPay.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPay.Location = New System.Drawing.Point(84, 177)
        Me.lblPay.Name = "lblPay"
        Me.lblPay.Size = New System.Drawing.Size(141, 13)
        Me.lblPay.TabIndex = 68
        Me.lblPay.Text = "I need to confirm their pay of"
        '
        'txtbxQuestion5
        '
        Me.txtbxQuestion5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "StabilityQ5", True))
        Me.txtbxQuestion5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxQuestion5.Location = New System.Drawing.Point(234, 174)
        Me.txtbxQuestion5.MaxLength = 50
        Me.txtbxQuestion5.Name = "txtbxQuestion5"
        Me.txtbxQuestion5.Size = New System.Drawing.Size(80, 20)
        Me.txtbxQuestion5.TabIndex = 10
        '
        'lblWorker
        '
        Me.lblWorker.AutoSize = True
        Me.lblWorker.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorker.Location = New System.Drawing.Point(193, 229)
        Me.lblWorker.Name = "lblWorker"
        Me.lblWorker.Size = New System.Drawing.Size(123, 13)
        Me.lblWorker.TabIndex = 73
        Me.lblWorker.Text = "Are they a good worker?"
        '
        'lblJobSafe
        '
        Me.lblJobSafe.AutoSize = True
        Me.lblJobSafe.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobSafe.Location = New System.Drawing.Point(232, 203)
        Me.lblJobSafe.Name = "lblJobSafe"
        Me.lblJobSafe.Size = New System.Drawing.Size(84, 13)
        Me.lblJobSafe.TabIndex = 71
        Me.lblJobSafe.Text = "Is their job safe?"
        '
        'gpbxTenancy
        '
        Me.gpbxTenancy.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxTenancy.Controls.Add(Me.txtbxEstablishTenancy)
        Me.gpbxTenancy.Controls.Add(Me.txtbxTenancyEnviron)
        Me.gpbxTenancy.Controls.Add(Me.txtbxResidenceSatis)
        Me.gpbxTenancy.Controls.Add(Me.lblResidence)
        Me.gpbxTenancy.Controls.Add(Me.txtbxTenancy)
        Me.gpbxTenancy.Controls.Add(Me.lblTenancyEstablish)
        Me.gpbxTenancy.Controls.Add(Me.lblResidenceSatis)
        Me.gpbxTenancy.Controls.Add(Me.lblTenancyBecause)
        Me.gpbxTenancy.Controls.Add(Me.lblTenancy)
        Me.gpbxTenancy.Controls.Add(Me.cmbxTenancyType)
        Me.gpbxTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxTenancy.Location = New System.Drawing.Point(12, 268)
        Me.gpbxTenancy.Name = "gpbxTenancy"
        Me.gpbxTenancy.Size = New System.Drawing.Size(905, 157)
        Me.gpbxTenancy.TabIndex = 0
        Me.gpbxTenancy.TabStop = False
        Me.gpbxTenancy.Text = "Tenancy"
        '
        'txtbxEstablishTenancy
        '
        Me.txtbxEstablishTenancy.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancyEstablish", True))
        Me.txtbxEstablishTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxEstablishTenancy.IsReadOnly = False
        Me.txtbxEstablishTenancy.IsUndoEnabled = True
        Me.txtbxEstablishTenancy.Location = New System.Drawing.Point(333, 116)
        Me.txtbxEstablishTenancy.MaxLength = 0
        Me.txtbxEstablishTenancy.MultiLine = True
        Me.txtbxEstablishTenancy.Name = "txtbxEstablishTenancy"
        Me.txtbxEstablishTenancy.SelectedText = ""
        Me.txtbxEstablishTenancy.SelectionLength = 0
        Me.txtbxEstablishTenancy.SelectionStart = 0
        Me.txtbxEstablishTenancy.Size = New System.Drawing.Size(565, 35)
        Me.txtbxEstablishTenancy.TabIndex = 5
        Me.txtbxEstablishTenancy.WordWrap = True
        Me.txtbxEstablishTenancy.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxTenancyEnviron
        '
        Me.txtbxTenancyEnviron.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancyEnvironment", True))
        Me.txtbxTenancyEnviron.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxTenancyEnviron.IsReadOnly = False
        Me.txtbxTenancyEnviron.IsUndoEnabled = True
        Me.txtbxTenancyEnviron.Location = New System.Drawing.Point(333, 79)
        Me.txtbxTenancyEnviron.MaxLength = 0
        Me.txtbxTenancyEnviron.MultiLine = True
        Me.txtbxTenancyEnviron.Name = "txtbxTenancyEnviron"
        Me.txtbxTenancyEnviron.SelectedText = ""
        Me.txtbxTenancyEnviron.SelectionLength = 0
        Me.txtbxTenancyEnviron.SelectionStart = 0
        Me.txtbxTenancyEnviron.Size = New System.Drawing.Size(565, 35)
        Me.txtbxTenancyEnviron.TabIndex = 4
        Me.txtbxTenancyEnviron.WordWrap = True
        Me.txtbxTenancyEnviron.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxResidenceSatis
        '
        Me.txtbxResidenceSatis.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancySatisfactory", True))
        Me.txtbxResidenceSatis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxResidenceSatis.IsReadOnly = False
        Me.txtbxResidenceSatis.IsUndoEnabled = True
        Me.txtbxResidenceSatis.Location = New System.Drawing.Point(333, 41)
        Me.txtbxResidenceSatis.MaxLength = 0
        Me.txtbxResidenceSatis.MultiLine = True
        Me.txtbxResidenceSatis.Name = "txtbxResidenceSatis"
        Me.txtbxResidenceSatis.SelectedText = ""
        Me.txtbxResidenceSatis.SelectionLength = 0
        Me.txtbxResidenceSatis.SelectionStart = 0
        Me.txtbxResidenceSatis.Size = New System.Drawing.Size(565, 35)
        Me.txtbxResidenceSatis.TabIndex = 3
        Me.txtbxResidenceSatis.WordWrap = True
        Me.txtbxResidenceSatis.Child = New System.Windows.Controls.TextBox()
        '
        'lblResidence
        '
        Me.lblResidence.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidence.Location = New System.Drawing.Point(9, 20)
        Me.lblResidence.Name = "lblResidence"
        Me.lblResidence.Size = New System.Drawing.Size(138, 17)
        Me.lblResidence.TabIndex = 107
        Me.lblResidence.Text = "Length of current tenancy"
        '
        'txtbxTenancy
        '
        Me.txtbxTenancy.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancyLength", True))
        Me.txtbxTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxTenancy.Location = New System.Drawing.Point(150, 17)
        Me.txtbxTenancy.MaxLength = 20
        Me.txtbxTenancy.Name = "txtbxTenancy"
        Me.txtbxTenancy.Size = New System.Drawing.Size(114, 20)
        Me.txtbxTenancy.TabIndex = 1
        '
        'lblTenancyEstablish
        '
        Me.lblTenancyEstablish.AutoSize = True
        Me.lblTenancyEstablish.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTenancyEstablish.Location = New System.Drawing.Point(209, 119)
        Me.lblTenancyEstablish.Name = "lblTenancyEstablish"
        Me.lblTenancyEstablish.Size = New System.Drawing.Size(116, 13)
        Me.lblTenancyEstablish.TabIndex = 115
        Me.lblTenancyEstablish.Text = "Established tenancy by"
        Me.lblTenancyEstablish.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblResidenceSatis
        '
        Me.lblResidenceSatis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidenceSatis.Location = New System.Drawing.Point(127, 47)
        Me.lblResidenceSatis.Name = "lblResidenceSatis"
        Me.lblResidenceSatis.Size = New System.Drawing.Size(198, 20)
        Me.lblResidenceSatis.TabIndex = 109
        Me.lblResidenceSatis.Text = "Is this satisfactory. If yes then why?"
        Me.lblResidenceSatis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTenancyBecause
        '
        Me.lblTenancyBecause.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTenancyBecause.Location = New System.Drawing.Point(66, 80)
        Me.lblTenancyBecause.Name = "lblTenancyBecause"
        Me.lblTenancyBecause.Size = New System.Drawing.Size(250, 30)
        Me.lblTenancyBecause.TabIndex = 113
        Me.lblTenancyBecause.Text = "Is the tenancy environment likely to change over the term of the loan? Explain."
        Me.lblTenancyBecause.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTenancy
        '
        Me.lblTenancy.AutoSize = True
        Me.lblTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTenancy.Location = New System.Drawing.Point(330, 20)
        Me.lblTenancy.Name = "lblTenancy"
        Me.lblTenancy.Size = New System.Drawing.Size(77, 13)
        Me.lblTenancy.TabIndex = 111
        Me.lblTenancy.Text = "The tenancy is"
        '
        'cmbxTenancyType
        '
        Me.cmbxTenancyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTenancyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTenancyType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CustomerBindingSource, "TenancyType", True))
        Me.cmbxTenancyType.DataSource = Me.TypeOfTenancyBindingSource
        Me.cmbxTenancyType.DisplayMember = "TenancyType"
        Me.cmbxTenancyType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxTenancyType.FormattingEnabled = True
        Me.cmbxTenancyType.Location = New System.Drawing.Point(413, 17)
        Me.cmbxTenancyType.Name = "cmbxTenancyType"
        Me.cmbxTenancyType.Size = New System.Drawing.Size(186, 21)
        Me.cmbxTenancyType.TabIndex = 2
        Me.cmbxTenancyType.ValueMember = "TenancyType"
        '
        'TypeOfTenancyBindingSource
        '
        Me.TypeOfTenancyBindingSource.DataMember = "TypeOfTenancy"
        Me.TypeOfTenancyBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblOther
        '
        Me.lblOther.AutoSize = True
        Me.lblOther.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOther.Location = New System.Drawing.Point(24, 775)
        Me.lblOther.Name = "lblOther"
        Me.lblOther.Size = New System.Drawing.Size(249, 13)
        Me.lblOther.TabIndex = 75
        Me.lblOther.Text = "Tenancy, employment, identity and other comments"
        '
        'btnCancelCustomer
        '
        Me.btnCancelCustomer.BackColor = System.Drawing.Color.Orange
        Me.btnCancelCustomer.Location = New System.Drawing.Point(755, 172)
        Me.btnCancelCustomer.Name = "btnCancelCustomer"
        Me.btnCancelCustomer.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelCustomer.TabIndex = 134
        Me.btnCancelCustomer.Text = "Cancel"
        Me.btnCancelCustomer.UseVisualStyleBackColor = False
        '
        'btnDeleteCustomer
        '
        Me.btnDeleteCustomer.BackColor = System.Drawing.Color.LightCoral
        Me.btnDeleteCustomer.Location = New System.Drawing.Point(836, 172)
        Me.btnDeleteCustomer.Name = "btnDeleteCustomer"
        Me.btnDeleteCustomer.Size = New System.Drawing.Size(75, 23)
        Me.btnDeleteCustomer.TabIndex = 132
        Me.btnDeleteCustomer.Text = "Delete"
        Me.btnDeleteCustomer.UseVisualStyleBackColor = False
        '
        'btnSaveCustomer
        '
        Me.btnSaveCustomer.BackColor = System.Drawing.Color.LightGreen
        Me.btnSaveCustomer.Location = New System.Drawing.Point(674, 172)
        Me.btnSaveCustomer.Name = "btnSaveCustomer"
        Me.btnSaveCustomer.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveCustomer.TabIndex = 131
        Me.btnSaveCustomer.Text = "Save"
        Me.btnSaveCustomer.UseVisualStyleBackColor = False
        '
        'cmbxCustomerType
        '
        Me.cmbxCustomerType.FormattingEnabled = True
        Me.cmbxCustomerType.Location = New System.Drawing.Point(54, 6)
        Me.cmbxCustomerType.Name = "cmbxCustomerType"
        Me.cmbxCustomerType.Size = New System.Drawing.Size(90, 21)
        Me.cmbxCustomerType.TabIndex = 129
        '
        'lblCustomerType
        '
        Me.lblCustomerType.AutoSize = True
        Me.lblCustomerType.Location = New System.Drawing.Point(17, 9)
        Me.lblCustomerType.Name = "lblCustomerType"
        Me.lblCustomerType.Size = New System.Drawing.Size(31, 13)
        Me.lblCustomerType.TabIndex = 127
        Me.lblCustomerType.Text = "Type"
        '
        'dgvEnquiryCustomers
        '
        Me.dgvEnquiryCustomers.AllowUserToAddRows = False
        Me.dgvEnquiryCustomers.AllowUserToDeleteRows = False
        Me.dgvEnquiryCustomers.AutoGenerateColumns = False
        Me.dgvEnquiryCustomers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEnquiryCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEnquiryCustomers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdnumberDataGridViewTextBoxColumn, Me.NameDataGridViewTextBoxColumn, Me.CustomerEnquiryTypeDataGridViewTextBoxColumn, Me.CustomerIdDataGridViewTextBoxColumn, Me.EnquiryIdDataGridViewTextBoxColumn, Me.TypeDataGridViewTextBoxColumn, Me.CustomerTypeDataGridViewTextBoxColumn})
        Me.dgvEnquiryCustomers.DataSource = Me.EnquiryCustomersBindingSource
        Me.dgvEnquiryCustomers.Location = New System.Drawing.Point(12, 106)
        Me.dgvEnquiryCustomers.MultiSelect = False
        Me.dgvEnquiryCustomers.Name = "dgvEnquiryCustomers"
        Me.dgvEnquiryCustomers.ReadOnly = True
        Me.dgvEnquiryCustomers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnquiryCustomers.Size = New System.Drawing.Size(902, 63)
        Me.dgvEnquiryCustomers.TabIndex = 128
        '
        'IdnumberDataGridViewTextBoxColumn
        '
        Me.IdnumberDataGridViewTextBoxColumn.DataPropertyName = "Id_number"
        Me.IdnumberDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdnumberDataGridViewTextBoxColumn.Name = "IdnumberDataGridViewTextBoxColumn"
        Me.IdnumberDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "Name"
        Me.NameDataGridViewTextBoxColumn.HeaderText = "Name"
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        Me.NameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustomerEnquiryTypeDataGridViewTextBoxColumn
        '
        Me.CustomerEnquiryTypeDataGridViewTextBoxColumn.DataPropertyName = "CustomerEnquiryType"
        Me.CustomerEnquiryTypeDataGridViewTextBoxColumn.HeaderText = "CustomerEnquiryType"
        Me.CustomerEnquiryTypeDataGridViewTextBoxColumn.Name = "CustomerEnquiryTypeDataGridViewTextBoxColumn"
        Me.CustomerEnquiryTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomerEnquiryTypeDataGridViewTextBoxColumn.Width = 150
        '
        'CustomerIdDataGridViewTextBoxColumn
        '
        Me.CustomerIdDataGridViewTextBoxColumn.DataPropertyName = "CustomerId"
        Me.CustomerIdDataGridViewTextBoxColumn.HeaderText = "CustomerId"
        Me.CustomerIdDataGridViewTextBoxColumn.Name = "CustomerIdDataGridViewTextBoxColumn"
        Me.CustomerIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomerIdDataGridViewTextBoxColumn.Visible = False
        '
        'EnquiryIdDataGridViewTextBoxColumn
        '
        Me.EnquiryIdDataGridViewTextBoxColumn.DataPropertyName = "EnquiryId"
        Me.EnquiryIdDataGridViewTextBoxColumn.HeaderText = "EnquiryId"
        Me.EnquiryIdDataGridViewTextBoxColumn.Name = "EnquiryIdDataGridViewTextBoxColumn"
        Me.EnquiryIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.EnquiryIdDataGridViewTextBoxColumn.Visible = False
        '
        'TypeDataGridViewTextBoxColumn
        '
        Me.TypeDataGridViewTextBoxColumn.DataPropertyName = "Type"
        Me.TypeDataGridViewTextBoxColumn.HeaderText = "Type"
        Me.TypeDataGridViewTextBoxColumn.Name = "TypeDataGridViewTextBoxColumn"
        Me.TypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.TypeDataGridViewTextBoxColumn.Visible = False
        '
        'CustomerTypeDataGridViewTextBoxColumn
        '
        Me.CustomerTypeDataGridViewTextBoxColumn.DataPropertyName = "CustomerType"
        Me.CustomerTypeDataGridViewTextBoxColumn.HeaderText = "CustomerType"
        Me.CustomerTypeDataGridViewTextBoxColumn.Name = "CustomerTypeDataGridViewTextBoxColumn"
        Me.CustomerTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomerTypeDataGridViewTextBoxColumn.Visible = False
        '
        'EnquiryCustomersBindingSource
        '
        Me.EnquiryCustomersBindingSource.DataMember = "EnquiryCustomers"
        Me.EnquiryCustomersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.Color.LightBlue
        Me.btnImport.Location = New System.Drawing.Point(45, 81)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(124, 23)
        Me.btnImport.TabIndex = 127
        Me.btnImport.Text = "Import customers"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnAddCustomer
        '
        Me.btnAddCustomer.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddCustomer.Location = New System.Drawing.Point(222, 81)
        Me.btnAddCustomer.Name = "btnAddCustomer"
        Me.btnAddCustomer.Size = New System.Drawing.Size(113, 23)
        Me.btnAddCustomer.TabIndex = 126
        Me.btnAddCustomer.Text = "Add a customer"
        Me.btnAddCustomer.UseVisualStyleBackColor = False
        '
        'lblContactAddress
        '
        Me.lblContactAddress.AutoSize = True
        Me.lblContactAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblContactAddress.Name = "lblContactAddress"
        Me.lblContactAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblContactAddress.TabIndex = 134
        Me.lblContactAddress.Text = "ClientAddress"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 133
        Me.lblAddress.Text = "Address:"
        '
        'lblContactName
        '
        Me.lblContactName.AutoSize = True
        Me.lblContactName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactName.Location = New System.Drawing.Point(15, 62)
        Me.lblContactName.Name = "lblContactName"
        Me.lblContactName.Size = New System.Drawing.Size(75, 13)
        Me.lblContactName.TabIndex = 132
        Me.lblContactName.Text = "Client Name"
        '
        'lblContactNumber
        '
        Me.lblContactNumber.AutoSize = True
        Me.lblContactNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNumber.Location = New System.Drawing.Point(594, 62)
        Me.lblContactNumber.Name = "lblContactNumber"
        Me.lblContactNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblContactNumber.TabIndex = 129
        Me.lblContactNumber.Text = "Number"
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 128
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(299, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(190, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 127
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(492, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 126
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(187, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 124
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(155, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 123
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(92, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 122
        Me.lblEnquiryNumberValue.Text = "0000"
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 121
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(836, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 83
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFinish
        '
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(617, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.TabIndex = 54
        Me.btnFinish.Text = "Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(455, 3)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 52
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'pnlFooter
        '
        Me.pnlFooter.Controls.Add(Me.btnCancel)
        Me.pnlFooter.Controls.Add(Me.btnViewDocs)
        Me.pnlFooter.Controls.Add(Me.btnQRGList)
        Me.pnlFooter.Controls.Add(Me.btnDocs)
        Me.pnlFooter.Controls.Add(Me.btnFinish)
        Me.pnlFooter.Controls.Add(Me.btnBack)
        Me.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlFooter.Location = New System.Drawing.Point(3, 957)
        Me.pnlFooter.Name = "pnlFooter"
        Me.pnlFooter.Size = New System.Drawing.Size(938, 30)
        Me.pnlFooter.TabIndex = 55
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(368, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 59
        Me.btnCancel.Text = "Exit"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(34, 3)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 58
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnQRGList
        '
        Me.btnQRGList.Location = New System.Drawing.Point(106, 3)
        Me.btnQRGList.Name = "btnQRGList"
        Me.btnQRGList.Size = New System.Drawing.Size(75, 23)
        Me.btnQRGList.TabIndex = 57
        Me.btnQRGList.Text = "QRGuide"
        Me.btnQRGList.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(3, 3)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 56
        Me.ToolTip1.SetToolTip(Me.btnDocs, "You can 'Drag and Drop' documents from Windows Explorer or your Desktop.")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'pnlHeader
        '
        Me.pnlHeader.Controls.Add(Me.btnDeleteCustomer)
        Me.pnlHeader.Controls.Add(Me.btnSaveCustomer)
        Me.pnlHeader.Controls.Add(Me.btnCancelCustomer)
        Me.pnlHeader.Controls.Add(Me.pbImportCustomer)
        Me.pnlHeader.Controls.Add(Me.AnimatedCirclePanel)
        Me.pnlHeader.Controls.Add(Me.dgvEnquiryCustomers)
        Me.pnlHeader.Controls.Add(Me.gpbxManager)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.lblCurrentLoanAmt)
        Me.pnlHeader.Controls.Add(Me.btnAddCustomer)
        Me.pnlHeader.Controls.Add(Me.lblCurrentLoan)
        Me.pnlHeader.Controls.Add(Me.gpbxStatus)
        Me.pnlHeader.Controls.Add(Me.MenuStrip1)
        Me.pnlHeader.Controls.Add(Me.lblContactAddress)
        Me.pnlHeader.Controls.Add(Me.lblEnquiryCode)
        Me.pnlHeader.Controls.Add(Me.lblAddress)
        Me.pnlHeader.Controls.Add(Me.lblEnquiryNumberValue)
        Me.pnlHeader.Controls.Add(Me.lblContactName)
        Me.pnlHeader.Controls.Add(Me.lblDateAndTime)
        Me.pnlHeader.Controls.Add(Me.lblDateAndTimeValue)
        Me.pnlHeader.Controls.Add(Me.lblContactNumber)
        Me.pnlHeader.Controls.Add(Me.lblLoanAmount)
        Me.pnlHeader.Controls.Add(Me.lblContact)
        Me.pnlHeader.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.Location = New System.Drawing.Point(3, 3)
        Me.pnlHeader.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(938, 198)
        Me.pnlHeader.TabIndex = 56
        '
        'pbImportCustomer
        '
        Me.pbImportCustomer.BackColor = System.Drawing.SystemColors.Control
        Me.pbImportCustomer.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbImportCustomer.Location = New System.Drawing.Point(178, 84)
        Me.pbImportCustomer.Name = "pbImportCustomer"
        Me.pbImportCustomer.Size = New System.Drawing.Size(18, 18)
        Me.pbImportCustomer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbImportCustomer.TabIndex = 255
        Me.pbImportCustomer.TabStop = False
        '
        'AnimatedCirclePanel
        '
        Me.AnimatedCirclePanel.Location = New System.Drawing.Point(15, 81)
        Me.AnimatedCirclePanel.Name = "AnimatedCirclePanel"
        Me.AnimatedCirclePanel.Size = New System.Drawing.Size(23, 23)
        Me.AnimatedCirclePanel.TabIndex = 254
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(734, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 248
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(639, 39)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmt.TabIndex = 253
        Me.lblCurrentLoanAmt.Text = "0.00"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'lblCurrentLoan
        '
        Me.lblCurrentLoan.AutoSize = True
        Me.lblCurrentLoan.Location = New System.Drawing.Point(567, 39)
        Me.lblCurrentLoan.Name = "lblCurrentLoan"
        Me.lblCurrentLoan.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrentLoan.TabIndex = 252
        Me.lblCurrentLoan.Text = "Current Loan"
        Me.lblCurrentLoan.Visible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.WWWToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem, Me.MCToolStripMenuItem, Me.DocsReceivedImg})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(938, 24)
        Me.MenuStrip1.TabIndex = 249
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEnquiryToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Enabled = False
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEnquiryToolStripMenuItem
        '
        Me.EndEnquiryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeclinedToolStripMenuItem, Me.WithdrawnToolStripMenuItem})
        Me.EndEnquiryToolStripMenuItem.Name = "EndEnquiryToolStripMenuItem"
        Me.EndEnquiryToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEnquiryToolStripMenuItem.Text = "End Enquiry"
        Me.EndEnquiryToolStripMenuItem.Visible = False
        '
        'DeclinedToolStripMenuItem
        '
        Me.DeclinedToolStripMenuItem.Name = "DeclinedToolStripMenuItem"
        Me.DeclinedToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DeclinedToolStripMenuItem.Text = "Declined"
        '
        'WithdrawnToolStripMenuItem
        '
        Me.WithdrawnToolStripMenuItem.Name = "WithdrawnToolStripMenuItem"
        Me.WithdrawnToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.WithdrawnToolStripMenuItem.Text = "Withdrawn"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem, Me.DocumentsReceivedToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'DocumentsReceivedToolStripMenuItem
        '
        Me.DocumentsReceivedToolStripMenuItem.Name = "DocumentsReceivedToolStripMenuItem"
        Me.DocumentsReceivedToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.DocumentsReceivedToolStripMenuItem.Text = "Documents received"
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem, Me.DealersToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "TrueTrack Names"
        '
        'DealersToolStripMenuItem
        '
        Me.DealersToolStripMenuItem.Name = "DealersToolStripMenuItem"
        Me.DealersToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.DealersToolStripMenuItem.Text = "Dealers"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1, Me.EmailApprovalToolStripMenuItem})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(154, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'EmailApprovalToolStripMenuItem
        '
        Me.EmailApprovalToolStripMenuItem.Name = "EmailApprovalToolStripMenuItem"
        Me.EmailApprovalToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EmailApprovalToolStripMenuItem.Text = "Email Approval"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "YFL Website"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Website - Ctrl+Alt+W"
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search TrueTrack Names - Ctrl+Alt+M"
        '
        'MCToolStripMenuItem
        '
        Me.MCToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.MCToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.OAC16
        Me.MCToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MCToolStripMenuItem.Name = "MCToolStripMenuItem"
        Me.MCToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.MCToolStripMenuItem.Text = "MembersCentre Link"
        Me.MCToolStripMenuItem.ToolTipText = "OAC Link"
        '
        'DocsReceivedImg
        '
        Me.DocsReceivedImg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.DocsReceivedImg.Image = Global.AppWhShtB.My.Resources.Resources.Docs24
        Me.DocsReceivedImg.Name = "DocsReceivedImg"
        Me.DocsReceivedImg.Size = New System.Drawing.Size(28, 20)
        Me.DocsReceivedImg.Text = "Docs Received"
        Me.DocsReceivedImg.ToolTipText = "Documents received"
        '
        'btnAddNewST
        '
        Me.btnAddNewST.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddNewST.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddNewST.Location = New System.Drawing.Point(811, 1)
        Me.btnAddNewST.Name = "btnAddNewST"
        Me.btnAddNewST.Size = New System.Drawing.Size(75, 23)
        Me.btnAddNewST.TabIndex = 8
        Me.btnAddNewST.Text = "Add new"
        Me.ToolTip1.SetToolTip(Me.btnAddNewST, "Major shareholders, typically 25% and above.")
        Me.btnAddNewST.UseVisualStyleBackColor = False
        '
        'pnlCustomerType
        '
        Me.pnlCustomerType.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.pnlCustomerType.Controls.Add(Me.pbHA)
        Me.pnlCustomerType.Controls.Add(Me.cmbxAcceptanceMethod)
        Me.pnlCustomerType.Controls.Add(Me.lblAcceptanceMethod)
        Me.pnlCustomerType.Controls.Add(Me.cmbxCustEnquiryType)
        Me.pnlCustomerType.Controls.Add(Me.lblCustEnquiryType)
        Me.pnlCustomerType.Controls.Add(Me.cmbxCustomerType)
        Me.pnlCustomerType.Controls.Add(Me.lblCustomerType)
        Me.pnlCustomerType.Location = New System.Drawing.Point(3, 201)
        Me.pnlCustomerType.Name = "pnlCustomerType"
        Me.pnlCustomerType.Size = New System.Drawing.Size(938, 34)
        Me.pnlCustomerType.TabIndex = 57
        '
        'pbHA
        '
        Me.pbHA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbHA.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbHA.Location = New System.Drawing.Point(892, 9)
        Me.pbHA.Name = "pbHA"
        Me.pbHA.Size = New System.Drawing.Size(18, 18)
        Me.pbHA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbHA.TabIndex = 134
        Me.pbHA.TabStop = False
        '
        'cmbxAcceptanceMethod
        '
        Me.cmbxAcceptanceMethod.FormattingEnabled = True
        Me.cmbxAcceptanceMethod.Location = New System.Drawing.Point(498, 6)
        Me.cmbxAcceptanceMethod.Name = "cmbxAcceptanceMethod"
        Me.cmbxAcceptanceMethod.Size = New System.Drawing.Size(140, 21)
        Me.cmbxAcceptanceMethod.TabIndex = 133
        '
        'lblAcceptanceMethod
        '
        Me.lblAcceptanceMethod.AutoSize = True
        Me.lblAcceptanceMethod.Location = New System.Drawing.Point(415, 9)
        Me.lblAcceptanceMethod.Name = "lblAcceptanceMethod"
        Me.lblAcceptanceMethod.Size = New System.Drawing.Size(77, 13)
        Me.lblAcceptanceMethod.TabIndex = 132
        Me.lblAcceptanceMethod.Text = "How accepted"
        '
        'cmbxCustEnquiryType
        '
        Me.cmbxCustEnquiryType.FormattingEnabled = True
        Me.cmbxCustEnquiryType.Location = New System.Drawing.Point(268, 6)
        Me.cmbxCustEnquiryType.Name = "cmbxCustEnquiryType"
        Me.cmbxCustEnquiryType.Size = New System.Drawing.Size(90, 21)
        Me.cmbxCustEnquiryType.TabIndex = 131
        '
        'lblCustEnquiryType
        '
        Me.lblCustEnquiryType.AutoSize = True
        Me.lblCustEnquiryType.Location = New System.Drawing.Point(193, 9)
        Me.lblCustEnquiryType.Name = "lblCustEnquiryType"
        Me.lblCustEnquiryType.Size = New System.Drawing.Size(69, 13)
        Me.lblCustEnquiryType.TabIndex = 130
        Me.lblCustEnquiryType.Text = "Enquiry Type"
        '
        'ListAMLRiskBS
        '
        Me.ListAMLRiskBS.DataMember = "List_AMLRisk"
        Me.ListAMLRiskBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'List2AMLRisk
        '
        Me.List2AMLRisk.DataMember = "List_AMLRisk"
        Me.List2AMLRisk.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'Enum_CustomerTypeBindingSource
        '
        Me.Enum_CustomerTypeBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        Me.Enum_CustomerTypeBindingSource.Position = 0
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Me.CustomerEnquiryTableAdapter
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'CustomerEnquiryTableAdapter
        '
        Me.CustomerEnquiryTableAdapter.ClearBeforeFill = True
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TypeOfTenancyTableAdapter
        '
        Me.TypeOfTenancyTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'List_AMLRiskTableAdapter
        '
        Me.List_AMLRiskTableAdapter.ClearBeforeFill = True
        '
        'EnquiryCustomersTableAdapter
        '
        Me.EnquiryCustomersTableAdapter.ClearBeforeFill = True
        '
        'CustomerTableAdapter
        '
        Me.CustomerTableAdapter.ClearBeforeFill = True
        '
        'CustomerEnquiryBindingSource
        '
        Me.CustomerEnquiryBindingSource.DataMember = "CustomerEnquiry"
        Me.CustomerEnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'pnlCompany
        '
        Me.pnlCompany.AutoScroll = True
        Me.pnlCompany.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.pnlCompany.Controls.Add(Me.gpbxTradeRef)
        Me.pnlCompany.Controls.Add(Me.gpbxBusOther)
        Me.pnlCompany.Controls.Add(Me.gpbxDtsb23)
        Me.pnlCompany.Controls.Add(Me.gpbxBusinessCredit)
        Me.pnlCompany.Controls.Add(Me.gpbxCoTenancy)
        Me.pnlCompany.Controls.Add(Me.gpbxDtsb01)
        Me.pnlCompany.Controls.Add(Me.gpbxCompanyInfo)
        Me.pnlCompany.Location = New System.Drawing.Point(3, 234)
        Me.pnlCompany.Name = "pnlCompany"
        Me.pnlCompany.Size = New System.Drawing.Size(938, 715)
        Me.pnlCompany.TabIndex = 136
        '
        'gpbxTradeRef
        '
        Me.gpbxTradeRef.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxTradeRef.Controls.Add(Me.btnNewRef)
        Me.gpbxTradeRef.Controls.Add(Me.drReferences)
        Me.gpbxTradeRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxTradeRef.Location = New System.Drawing.Point(12, 1187)
        Me.gpbxTradeRef.Name = "gpbxTradeRef"
        Me.gpbxTradeRef.Size = New System.Drawing.Size(900, 228)
        Me.gpbxTradeRef.TabIndex = 294
        Me.gpbxTradeRef.TabStop = False
        Me.gpbxTradeRef.Text = "Trade References"
        '
        'btnNewRef
        '
        Me.btnNewRef.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewRef.BackColor = System.Drawing.Color.LightGreen
        Me.btnNewRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNewRef.Location = New System.Drawing.Point(754, 15)
        Me.btnNewRef.Name = "btnNewRef"
        Me.btnNewRef.Size = New System.Drawing.Size(133, 23)
        Me.btnNewRef.TabIndex = 1
        Me.btnNewRef.Text = "add New Reference"
        Me.btnNewRef.UseVisualStyleBackColor = False
        '
        'drReferences
        '
        Me.drReferences.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.drReferences.ItemHeaderVisible = False
        '
        'drReferences.ItemTemplate
        '
        Me.drReferences.ItemTemplate.Controls.Add(Me.txtTradeRefComments)
        Me.drReferences.ItemTemplate.Controls.Add(lblTradeRefComments)
        Me.drReferences.ItemTemplate.Controls.Add(lblTradeRefPay)
        Me.drReferences.ItemTemplate.Controls.Add(Me.txtTradeRefPay)
        Me.drReferences.ItemTemplate.Controls.Add(lblTradeRefSpend)
        Me.drReferences.ItemTemplate.Controls.Add(Me.txtTradeRefSpend)
        Me.drReferences.ItemTemplate.Controls.Add(lblTradeRefAccount)
        Me.drReferences.ItemTemplate.Controls.Add(Me.txtTradeRefAccount)
        Me.drReferences.ItemTemplate.Controls.Add(Me.btnTradeRefEdit)
        Me.drReferences.ItemTemplate.Controls.Add(Me.btnTradeRefDelete)
        Me.drReferences.ItemTemplate.Controls.Add(lblTradeRefName)
        Me.drReferences.ItemTemplate.Controls.Add(Me.txtTradeRefName)
        Me.drReferences.ItemTemplate.Size = New System.Drawing.Size(859, 173)
        Me.drReferences.Location = New System.Drawing.Point(10, 44)
        Me.drReferences.Name = "drReferences"
        Me.drReferences.Size = New System.Drawing.Size(884, 177)
        Me.drReferences.TabIndex = 0
        '
        'txtTradeRefComments
        '
        Me.txtTradeRefComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Comments", True))
        Me.txtTradeRefComments.IsReadOnly = False
        Me.txtTradeRefComments.IsUndoEnabled = True
        Me.txtTradeRefComments.Location = New System.Drawing.Point(251, 127)
        Me.txtTradeRefComments.MaxLength = 1024
        Me.txtTradeRefComments.MultiLine = True
        Me.txtTradeRefComments.Name = "txtTradeRefComments"
        Me.txtTradeRefComments.SelectedText = ""
        Me.txtTradeRefComments.SelectionLength = 0
        Me.txtTradeRefComments.SelectionStart = 0
        Me.txtTradeRefComments.Size = New System.Drawing.Size(400, 36)
        Me.txtTradeRefComments.TabIndex = 11
        Me.txtTradeRefComments.WordWrap = True
        Me.txtTradeRefComments.Child = New System.Windows.Controls.TextBox()
        '
        'TradeReferencesBindingSource
        '
        Me.TradeReferencesBindingSource.DataMember = "TradeReferences"
        Me.TradeReferencesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'txtTradeRefPay
        '
        Me.txtTradeRefPay.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Pay", True))
        Me.txtTradeRefPay.Location = New System.Drawing.Point(251, 101)
        Me.txtTradeRefPay.MaxLength = 50
        Me.txtTradeRefPay.Name = "txtTradeRefPay"
        Me.txtTradeRefPay.Size = New System.Drawing.Size(200, 20)
        Me.txtTradeRefPay.TabIndex = 9
        '
        'txtTradeRefSpend
        '
        Me.txtTradeRefSpend.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Spend", True))
        Me.txtTradeRefSpend.Location = New System.Drawing.Point(251, 75)
        Me.txtTradeRefSpend.MaxLength = 20
        Me.txtTradeRefSpend.Name = "txtTradeRefSpend"
        Me.txtTradeRefSpend.Size = New System.Drawing.Size(200, 20)
        Me.txtTradeRefSpend.TabIndex = 7
        '
        'txtTradeRefAccount
        '
        Me.txtTradeRefAccount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Account", True))
        Me.txtTradeRefAccount.Location = New System.Drawing.Point(251, 49)
        Me.txtTradeRefAccount.MaxLength = 50
        Me.txtTradeRefAccount.Name = "txtTradeRefAccount"
        Me.txtTradeRefAccount.Size = New System.Drawing.Size(200, 20)
        Me.txtTradeRefAccount.TabIndex = 5
        '
        'btnTradeRefEdit
        '
        Me.btnTradeRefEdit.BackColor = System.Drawing.Color.LightBlue
        Me.btnTradeRefEdit.Location = New System.Drawing.Point(574, 3)
        Me.btnTradeRefEdit.Name = "btnTradeRefEdit"
        Me.btnTradeRefEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnTradeRefEdit.TabIndex = 3
        Me.btnTradeRefEdit.Text = "Edit"
        Me.btnTradeRefEdit.UseVisualStyleBackColor = False
        '
        'btnTradeRefDelete
        '
        Me.btnTradeRefDelete.BackColor = System.Drawing.Color.LightCoral
        Me.btnTradeRefDelete.Location = New System.Drawing.Point(740, 3)
        Me.btnTradeRefDelete.Name = "btnTradeRefDelete"
        Me.btnTradeRefDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnTradeRefDelete.TabIndex = 2
        Me.btnTradeRefDelete.Text = "Delete"
        Me.btnTradeRefDelete.UseVisualStyleBackColor = False
        '
        'txtTradeRefName
        '
        Me.txtTradeRefName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "BusinessName", True))
        Me.txtTradeRefName.Location = New System.Drawing.Point(251, 23)
        Me.txtTradeRefName.MaxLength = 50
        Me.txtTradeRefName.Name = "txtTradeRefName"
        Me.txtTradeRefName.Size = New System.Drawing.Size(200, 20)
        Me.txtTradeRefName.TabIndex = 1
        '
        'gpbxBusOther
        '
        Me.gpbxBusOther.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxBusOther.Controls.Add(Me.txtBusOtherComments)
        Me.gpbxBusOther.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxBusOther.Location = New System.Drawing.Point(12, 1088)
        Me.gpbxBusOther.Name = "gpbxBusOther"
        Me.gpbxBusOther.Size = New System.Drawing.Size(900, 89)
        Me.gpbxBusOther.TabIndex = 293
        Me.gpbxBusOther.TabStop = False
        Me.gpbxBusOther.Text = "Other Comments"
        '
        'txtBusOtherComments
        '
        Me.txtBusOtherComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "BusinessOtherNotes", True))
        Me.txtBusOtherComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusOtherComments.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtBusOtherComments.IsReadOnly = False
        Me.txtBusOtherComments.IsUndoEnabled = True
        Me.txtBusOtherComments.Location = New System.Drawing.Point(10, 19)
        Me.txtBusOtherComments.MaxLength = 1024
        Me.txtBusOtherComments.MultiLine = True
        Me.txtBusOtherComments.Name = "txtBusOtherComments"
        Me.txtBusOtherComments.SelectedText = ""
        Me.txtBusOtherComments.SelectionLength = 0
        Me.txtBusOtherComments.SelectionStart = 0
        Me.txtBusOtherComments.Size = New System.Drawing.Size(877, 65)
        Me.txtBusOtherComments.TabIndex = 7
        Me.txtBusOtherComments.WordWrap = True
        Me.txtBusOtherComments.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxDtsb23
        '
        Me.gpbxDtsb23.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxDtsb23.Controls.Add(Me.PanelDtsb23)
        Me.gpbxDtsb23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxDtsb23.Location = New System.Drawing.Point(12, 421)
        Me.gpbxDtsb23.Name = "gpbxDtsb23"
        Me.gpbxDtsb23.Size = New System.Drawing.Size(901, 163)
        Me.gpbxDtsb23.TabIndex = 133
        Me.gpbxDtsb23.TabStop = False
        Me.gpbxDtsb23.Text = "Shareholders"
        '
        'PanelDtsb23
        '
        Me.PanelDtsb23.Controls.Add(Me.pbPEP2)
        Me.PanelDtsb23.Controls.Add(Me.tlpShareholders)
        Me.PanelDtsb23.Controls.Add(Me.dgvDTSBType2)
        Me.PanelDtsb23.Controls.Add(Me.btnAddNewST)
        Me.PanelDtsb23.Controls.Add(Me.lblShareholdersNotice)
        Me.PanelDtsb23.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDtsb23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelDtsb23.Location = New System.Drawing.Point(3, 16)
        Me.PanelDtsb23.Name = "PanelDtsb23"
        Me.PanelDtsb23.Size = New System.Drawing.Size(895, 144)
        Me.PanelDtsb23.TabIndex = 10
        '
        'pbPEP2
        '
        Me.pbPEP2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbPEP2.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbPEP2.Location = New System.Drawing.Point(787, 4)
        Me.pbPEP2.Name = "pbPEP2"
        Me.pbPEP2.Size = New System.Drawing.Size(18, 18)
        Me.pbPEP2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbPEP2.TabIndex = 12
        Me.pbPEP2.TabStop = False
        '
        'tlpShareholders
        '
        Me.tlpShareholders.ColumnCount = 3
        Me.tlpShareholders.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpShareholders.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpShareholders.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpShareholders.Controls.Add(Me.lblShareholdersNote, 0, 1)
        Me.tlpShareholders.Controls.Add(Me.ckbxShareholders, 2, 0)
        Me.tlpShareholders.Controls.Add(Me.ckbxShareIdentified, 0, 0)
        Me.tlpShareholders.Controls.Add(Me.ckbxShareStructure, 1, 0)
        Me.tlpShareholders.Controls.Add(Me.txtShareholdersComments, 1, 1)
        Me.tlpShareholders.Location = New System.Drawing.Point(4, 76)
        Me.tlpShareholders.Name = "tlpShareholders"
        Me.tlpShareholders.RowCount = 2
        Me.tlpShareholders.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpShareholders.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpShareholders.Size = New System.Drawing.Size(886, 67)
        Me.tlpShareholders.TabIndex = 7
        '
        'lblShareholdersNote
        '
        Me.lblShareholdersNote.AutoSize = True
        Me.lblShareholdersNote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblShareholdersNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShareholdersNote.Location = New System.Drawing.Point(3, 25)
        Me.lblShareholdersNote.Name = "lblShareholdersNote"
        Me.lblShareholdersNote.Size = New System.Drawing.Size(294, 42)
        Me.lblShareholdersNote.TabIndex = 14
        Me.lblShareholdersNote.Text = "Comments on Shareholders and their related parties/interests"
        Me.lblShareholdersNote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxShareholders
        '
        Me.ckbxShareholders.AutoSize = True
        Me.ckbxShareholders.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessShareholders", True))
        Me.ckbxShareholders.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxShareholders.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxShareholders.Location = New System.Drawing.Point(715, 3)
        Me.ckbxShareholders.Name = "ckbxShareholders"
        Me.ckbxShareholders.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxShareholders.Size = New System.Drawing.Size(168, 19)
        Me.ckbxShareholders.TabIndex = 6
        Me.ckbxShareholders.Text = "?Shareholders are satisfactory"
        Me.ckbxShareholders.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxShareholders.UseVisualStyleBackColor = True
        '
        'ckbxShareIdentified
        '
        Me.ckbxShareIdentified.AutoSize = True
        Me.ckbxShareIdentified.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessShareIdentified", True))
        Me.ckbxShareIdentified.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxShareIdentified.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxShareIdentified.Location = New System.Drawing.Point(146, 3)
        Me.ckbxShareIdentified.Name = "ckbxShareIdentified"
        Me.ckbxShareIdentified.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxShareIdentified.Size = New System.Drawing.Size(151, 19)
        Me.ckbxShareIdentified.TabIndex = 16
        Me.ckbxShareIdentified.Text = "?All shareholders identified"
        Me.ckbxShareIdentified.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxShareIdentified.UseVisualStyleBackColor = True
        '
        'ckbxShareStructure
        '
        Me.ckbxShareStructure.AutoSize = True
        Me.ckbxShareStructure.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessShareStructure", True))
        Me.ckbxShareStructure.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxShareStructure.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxShareStructure.Location = New System.Drawing.Point(407, 3)
        Me.ckbxShareStructure.Name = "ckbxShareStructure"
        Me.ckbxShareStructure.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxShareStructure.Size = New System.Drawing.Size(183, 19)
        Me.ckbxShareStructure.TabIndex = 17
        Me.ckbxShareStructure.Text = "?Shareholding structure identified"
        Me.ckbxShareStructure.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxShareStructure.UseVisualStyleBackColor = True
        '
        'txtShareholdersComments
        '
        Me.tlpShareholders.SetColumnSpan(Me.txtShareholdersComments, 2)
        Me.txtShareholdersComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "BusinessShareholderNotes", True))
        Me.txtShareholdersComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShareholdersComments.IsReadOnly = False
        Me.txtShareholdersComments.IsUndoEnabled = True
        Me.txtShareholdersComments.Location = New System.Drawing.Point(303, 28)
        Me.txtShareholdersComments.MaxLength = 1024
        Me.txtShareholdersComments.MultiLine = True
        Me.txtShareholdersComments.Name = "txtShareholdersComments"
        Me.txtShareholdersComments.SelectedText = ""
        Me.txtShareholdersComments.SelectionLength = 0
        Me.txtShareholdersComments.SelectionStart = 0
        Me.txtShareholdersComments.Size = New System.Drawing.Size(580, 36)
        Me.txtShareholdersComments.TabIndex = 15
        Me.txtShareholdersComments.WordWrap = True
        Me.txtShareholdersComments.Child = New System.Windows.Controls.TextBox()
        '
        'dgvDTSBType2
        '
        Me.dgvDTSBType2.AllowUserToAddRows = False
        Me.dgvDTSBType2.AllowUserToDeleteRows = False
        Me.dgvDTSBType2.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvDTSBType2.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDTSBType2.AutoGenerateColumns = False
        Me.dgvDTSBType2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvDTSBType2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvDTSBType2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDTSBType2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.CustomerIdDataGridViewTextBoxColumn1, Me.TypeDataGridViewTextBoxColumn1, Me.DescriptionDataGridViewTextBoxColumn, Me.FirstNameDataGridViewTextBoxColumn1, Me.LastNameDataGridViewTextBoxColumn1, Me.DateOfBirthDataGridViewTextBoxColumn, Me.IdVerifiedDataGridViewCheckBoxColumn, Me.AddressVerifiedDataGridViewCheckBoxColumn, Me.AMLRiskDesc, Me.IsaPEP, Me.Edit, Me.Delete})
        Me.dgvDTSBType2.DataSource = Me.DTSBBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDTSBType2.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDTSBType2.Location = New System.Drawing.Point(4, 26)
        Me.dgvDTSBType2.MultiSelect = False
        Me.dgvDTSBType2.Name = "dgvDTSBType2"
        Me.dgvDTSBType2.RowHeadersWidth = 10
        Me.dgvDTSBType2.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvDTSBType2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDTSBType2.Size = New System.Drawing.Size(887, 45)
        Me.dgvDTSBType2.TabIndex = 6
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'CustomerIdDataGridViewTextBoxColumn1
        '
        Me.CustomerIdDataGridViewTextBoxColumn1.DataPropertyName = "CustomerId"
        Me.CustomerIdDataGridViewTextBoxColumn1.HeaderText = "CustomerId"
        Me.CustomerIdDataGridViewTextBoxColumn1.Name = "CustomerIdDataGridViewTextBoxColumn1"
        Me.CustomerIdDataGridViewTextBoxColumn1.Visible = False
        '
        'TypeDataGridViewTextBoxColumn1
        '
        Me.TypeDataGridViewTextBoxColumn1.DataPropertyName = "Type"
        Me.TypeDataGridViewTextBoxColumn1.HeaderText = "Type"
        Me.TypeDataGridViewTextBoxColumn1.Name = "TypeDataGridViewTextBoxColumn1"
        Me.TypeDataGridViewTextBoxColumn1.Visible = False
        '
        'DescriptionDataGridViewTextBoxColumn
        '
        Me.DescriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.HeaderText = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.Name = "DescriptionDataGridViewTextBoxColumn"
        Me.DescriptionDataGridViewTextBoxColumn.Visible = False
        Me.DescriptionDataGridViewTextBoxColumn.Width = 70
        '
        'FirstNameDataGridViewTextBoxColumn1
        '
        Me.FirstNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FirstNameDataGridViewTextBoxColumn1.DataPropertyName = "FirstName"
        Me.FirstNameDataGridViewTextBoxColumn1.HeaderText = "FirstName"
        Me.FirstNameDataGridViewTextBoxColumn1.Name = "FirstNameDataGridViewTextBoxColumn1"
        '
        'LastNameDataGridViewTextBoxColumn1
        '
        Me.LastNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.LastNameDataGridViewTextBoxColumn1.DataPropertyName = "LastName"
        Me.LastNameDataGridViewTextBoxColumn1.HeaderText = "LastName"
        Me.LastNameDataGridViewTextBoxColumn1.Name = "LastNameDataGridViewTextBoxColumn1"
        '
        'DateOfBirthDataGridViewTextBoxColumn
        '
        Me.DateOfBirthDataGridViewTextBoxColumn.DataPropertyName = "DateOfBirth"
        Me.DateOfBirthDataGridViewTextBoxColumn.HeaderText = "DateOfBirth"
        Me.DateOfBirthDataGridViewTextBoxColumn.Name = "DateOfBirthDataGridViewTextBoxColumn"
        Me.DateOfBirthDataGridViewTextBoxColumn.Width = 90
        '
        'IdVerifiedDataGridViewCheckBoxColumn
        '
        Me.IdVerifiedDataGridViewCheckBoxColumn.DataPropertyName = "IdVerified"
        Me.IdVerifiedDataGridViewCheckBoxColumn.HeaderText = "IdVerified"
        Me.IdVerifiedDataGridViewCheckBoxColumn.Name = "IdVerifiedDataGridViewCheckBoxColumn"
        Me.IdVerifiedDataGridViewCheckBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.IdVerifiedDataGridViewCheckBoxColumn.Width = 70
        '
        'AddressVerifiedDataGridViewCheckBoxColumn
        '
        Me.AddressVerifiedDataGridViewCheckBoxColumn.DataPropertyName = "AddressVerified"
        Me.AddressVerifiedDataGridViewCheckBoxColumn.HeaderText = "AddressVerified"
        Me.AddressVerifiedDataGridViewCheckBoxColumn.Name = "AddressVerifiedDataGridViewCheckBoxColumn"
        Me.AddressVerifiedDataGridViewCheckBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'AMLRiskDesc
        '
        Me.AMLRiskDesc.DataPropertyName = "AMLRiskDesc"
        Me.AMLRiskDesc.HeaderText = "AMLRisk"
        Me.AMLRiskDesc.Name = "AMLRiskDesc"
        '
        'IsaPEP
        '
        Me.IsaPEP.DataPropertyName = "IsaPEP"
        Me.IsaPEP.HeaderText = "IsaPEP"
        Me.IsaPEP.Name = "IsaPEP"
        Me.IsaPEP.ReadOnly = True
        '
        'Edit
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window
        Me.Edit.DefaultCellStyle = DataGridViewCellStyle2
        Me.Edit.HeaderText = ""
        Me.Edit.Name = "Edit"
        Me.Edit.Text = "Edit"
        Me.Edit.UseColumnTextForButtonValue = True
        Me.Edit.Width = 70
        '
        'Delete
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.LightCoral
        Me.Delete.DefaultCellStyle = DataGridViewCellStyle3
        Me.Delete.HeaderText = ""
        Me.Delete.Name = "Delete"
        Me.Delete.Text = "Delete"
        Me.Delete.UseColumnTextForButtonValue = True
        Me.Delete.Width = 70
        '
        'DTSBBindingSource
        '
        Me.DTSBBindingSource.DataMember = "DTSB"
        Me.DTSBBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblShareholdersNotice
        '
        Me.lblShareholdersNotice.AutoSize = True
        Me.lblShareholdersNotice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShareholdersNotice.ForeColor = System.Drawing.Color.Blue
        Me.lblShareholdersNotice.Location = New System.Drawing.Point(47, 6)
        Me.lblShareholdersNotice.Name = "lblShareholdersNotice"
        Me.lblShareholdersNotice.Size = New System.Drawing.Size(294, 13)
        Me.lblShareholdersNotice.TabIndex = 9
        Me.lblShareholdersNotice.Text = "A customer must be saved before you can add a shareholder"
        Me.lblShareholdersNotice.Visible = False
        '
        'gpbxBusinessCredit
        '
        Me.gpbxBusinessCredit.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxBusinessCredit.Controls.Add(Me.txtBusCreditPPSR)
        Me.gpbxBusinessCredit.Controls.Add(Me.txtBusCreditDefaults)
        Me.gpbxBusinessCredit.Controls.Add(Me.txtBusCreditHistory)
        Me.gpbxBusinessCredit.Controls.Add(Me.gpbxBusCreditQuestion3)
        Me.gpbxBusinessCredit.Controls.Add(Me.gpbxBusCreditQuestion2)
        Me.gpbxBusinessCredit.Controls.Add(Me.gpbxBusCreditQuestion1)
        Me.gpbxBusinessCredit.Controls.Add(Me.lblBusCreditCheck)
        Me.gpbxBusinessCredit.Controls.Add(Me.lblBusCreditHistory)
        Me.gpbxBusinessCredit.Controls.Add(Me.lblBusCreditDefaults)
        Me.gpbxBusinessCredit.Controls.Add(Me.lblBusCreditPPSR)
        Me.gpbxBusinessCredit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxBusinessCredit.Location = New System.Drawing.Point(12, 717)
        Me.gpbxBusinessCredit.Name = "gpbxBusinessCredit"
        Me.gpbxBusinessCredit.Size = New System.Drawing.Size(901, 370)
        Me.gpbxBusinessCredit.TabIndex = 138
        Me.gpbxBusinessCredit.TabStop = False
        Me.gpbxBusinessCredit.Text = "Company Credit"
        '
        'txtBusCreditPPSR
        '
        Me.txtBusCreditPPSR.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CreditOther", True))
        Me.txtBusCreditPPSR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusCreditPPSR.IsReadOnly = False
        Me.txtBusCreditPPSR.IsUndoEnabled = True
        Me.txtBusCreditPPSR.Location = New System.Drawing.Point(10, 224)
        Me.txtBusCreditPPSR.MaxLength = 0
        Me.txtBusCreditPPSR.MultiLine = True
        Me.txtBusCreditPPSR.Name = "txtBusCreditPPSR"
        Me.txtBusCreditPPSR.SelectedText = ""
        Me.txtBusCreditPPSR.SelectionLength = 0
        Me.txtBusCreditPPSR.SelectionStart = 0
        Me.txtBusCreditPPSR.Size = New System.Drawing.Size(877, 65)
        Me.txtBusCreditPPSR.TabIndex = 24
        Me.txtBusCreditPPSR.WordWrap = True
        Me.txtBusCreditPPSR.Child = New System.Windows.Controls.TextBox()
        '
        'txtBusCreditDefaults
        '
        Me.txtBusCreditDefaults.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CreditExplanation", True))
        Me.txtBusCreditDefaults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusCreditDefaults.IsReadOnly = False
        Me.txtBusCreditDefaults.IsUndoEnabled = True
        Me.txtBusCreditDefaults.Location = New System.Drawing.Point(10, 139)
        Me.txtBusCreditDefaults.MaxLength = 0
        Me.txtBusCreditDefaults.MultiLine = True
        Me.txtBusCreditDefaults.Name = "txtBusCreditDefaults"
        Me.txtBusCreditDefaults.SelectedText = ""
        Me.txtBusCreditDefaults.SelectionLength = 0
        Me.txtBusCreditDefaults.SelectionStart = 0
        Me.txtBusCreditDefaults.Size = New System.Drawing.Size(877, 65)
        Me.txtBusCreditDefaults.TabIndex = 22
        Me.txtBusCreditDefaults.WordWrap = True
        Me.txtBusCreditDefaults.Child = New System.Windows.Controls.TextBox()
        '
        'txtBusCreditHistory
        '
        Me.txtBusCreditHistory.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CreditComments", True))
        Me.txtBusCreditHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusCreditHistory.IsReadOnly = False
        Me.txtBusCreditHistory.IsUndoEnabled = True
        Me.txtBusCreditHistory.Location = New System.Drawing.Point(10, 54)
        Me.txtBusCreditHistory.MaxLength = 0
        Me.txtBusCreditHistory.MultiLine = True
        Me.txtBusCreditHistory.Name = "txtBusCreditHistory"
        Me.txtBusCreditHistory.SelectedText = ""
        Me.txtBusCreditHistory.SelectionLength = 0
        Me.txtBusCreditHistory.SelectionStart = 0
        Me.txtBusCreditHistory.Size = New System.Drawing.Size(877, 65)
        Me.txtBusCreditHistory.TabIndex = 28
        Me.txtBusCreditHistory.WordWrap = True
        Me.txtBusCreditHistory.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxBusCreditQuestion3
        '
        Me.gpbxBusCreditQuestion3.Controls.Add(Me.rbtnNoBusCreditQuestion3)
        Me.gpbxBusCreditQuestion3.Controls.Add(Me.rbtnYesBusCreditQuestion3)
        Me.gpbxBusCreditQuestion3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxBusCreditQuestion3.Location = New System.Drawing.Point(599, 292)
        Me.gpbxBusCreditQuestion3.Name = "gpbxBusCreditQuestion3"
        Me.gpbxBusCreditQuestion3.Size = New System.Drawing.Size(284, 66)
        Me.gpbxBusCreditQuestion3.TabIndex = 27
        Me.gpbxBusCreditQuestion3.TabStop = False
        Me.gpbxBusCreditQuestion3.Text = "Are there Gaps in the credit history?"
        '
        'rbtnNoBusCreditQuestion3
        '
        Me.rbtnNoBusCreditQuestion3.AutoSize = True
        Me.rbtnNoBusCreditQuestion3.Location = New System.Drawing.Point(9, 42)
        Me.rbtnNoBusCreditQuestion3.Name = "rbtnNoBusCreditQuestion3"
        Me.rbtnNoBusCreditQuestion3.Size = New System.Drawing.Size(37, 17)
        Me.rbtnNoBusCreditQuestion3.TabIndex = 1
        Me.rbtnNoBusCreditQuestion3.TabStop = True
        Me.rbtnNoBusCreditQuestion3.Text = "no"
        Me.rbtnNoBusCreditQuestion3.UseVisualStyleBackColor = True
        '
        'rbtnYesBusCreditQuestion3
        '
        Me.rbtnYesBusCreditQuestion3.AutoSize = True
        Me.rbtnYesBusCreditQuestion3.Location = New System.Drawing.Point(9, 19)
        Me.rbtnYesBusCreditQuestion3.Name = "rbtnYesBusCreditQuestion3"
        Me.rbtnYesBusCreditQuestion3.Size = New System.Drawing.Size(41, 17)
        Me.rbtnYesBusCreditQuestion3.TabIndex = 0
        Me.rbtnYesBusCreditQuestion3.TabStop = True
        Me.rbtnYesBusCreditQuestion3.Text = "yes"
        Me.rbtnYesBusCreditQuestion3.UseVisualStyleBackColor = True
        '
        'gpbxBusCreditQuestion2
        '
        Me.gpbxBusCreditQuestion2.Controls.Add(Me.rbtnNoBusCreditQuestion2)
        Me.gpbxBusCreditQuestion2.Controls.Add(Me.rbtnYesBusCreditQuestion2)
        Me.gpbxBusCreditQuestion2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxBusCreditQuestion2.Location = New System.Drawing.Point(274, 292)
        Me.gpbxBusCreditQuestion2.Name = "gpbxBusCreditQuestion2"
        Me.gpbxBusCreditQuestion2.Size = New System.Drawing.Size(319, 66)
        Me.gpbxBusCreditQuestion2.TabIndex = 26
        Me.gpbxBusCreditQuestion2.TabStop = False
        Me.gpbxBusCreditQuestion2.Text = "Are all enquiries expected and accounted for?"
        '
        'rbtnNoBusCreditQuestion2
        '
        Me.rbtnNoBusCreditQuestion2.AutoSize = True
        Me.rbtnNoBusCreditQuestion2.Location = New System.Drawing.Point(6, 42)
        Me.rbtnNoBusCreditQuestion2.Name = "rbtnNoBusCreditQuestion2"
        Me.rbtnNoBusCreditQuestion2.Size = New System.Drawing.Size(37, 17)
        Me.rbtnNoBusCreditQuestion2.TabIndex = 1
        Me.rbtnNoBusCreditQuestion2.TabStop = True
        Me.rbtnNoBusCreditQuestion2.Text = "no"
        Me.rbtnNoBusCreditQuestion2.UseVisualStyleBackColor = True
        '
        'rbtnYesBusCreditQuestion2
        '
        Me.rbtnYesBusCreditQuestion2.AutoSize = True
        Me.rbtnYesBusCreditQuestion2.Location = New System.Drawing.Point(6, 19)
        Me.rbtnYesBusCreditQuestion2.Name = "rbtnYesBusCreditQuestion2"
        Me.rbtnYesBusCreditQuestion2.Size = New System.Drawing.Size(41, 17)
        Me.rbtnYesBusCreditQuestion2.TabIndex = 0
        Me.rbtnYesBusCreditQuestion2.TabStop = True
        Me.rbtnYesBusCreditQuestion2.Text = "yes"
        Me.rbtnYesBusCreditQuestion2.UseVisualStyleBackColor = True
        '
        'gpbxBusCreditQuestion1
        '
        Me.gpbxBusCreditQuestion1.Controls.Add(Me.rbtnNoBusCreditQuestion1)
        Me.gpbxBusCreditQuestion1.Controls.Add(Me.rbtnYesBusCreditQuestion1)
        Me.gpbxBusCreditQuestion1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxBusCreditQuestion1.Location = New System.Drawing.Point(14, 292)
        Me.gpbxBusCreditQuestion1.Name = "gpbxBusCreditQuestion1"
        Me.gpbxBusCreditQuestion1.Size = New System.Drawing.Size(254, 66)
        Me.gpbxBusCreditQuestion1.TabIndex = 25
        Me.gpbxBusCreditQuestion1.TabStop = False
        Me.gpbxBusCreditQuestion1.Text = "Is Credit History sufficent?"
        '
        'rbtnNoBusCreditQuestion1
        '
        Me.rbtnNoBusCreditQuestion1.AutoSize = True
        Me.rbtnNoBusCreditQuestion1.Location = New System.Drawing.Point(9, 42)
        Me.rbtnNoBusCreditQuestion1.Name = "rbtnNoBusCreditQuestion1"
        Me.rbtnNoBusCreditQuestion1.Size = New System.Drawing.Size(37, 17)
        Me.rbtnNoBusCreditQuestion1.TabIndex = 1
        Me.rbtnNoBusCreditQuestion1.TabStop = True
        Me.rbtnNoBusCreditQuestion1.Text = "no"
        Me.rbtnNoBusCreditQuestion1.UseVisualStyleBackColor = True
        '
        'rbtnYesBusCreditQuestion1
        '
        Me.rbtnYesBusCreditQuestion1.AutoSize = True
        Me.rbtnYesBusCreditQuestion1.Location = New System.Drawing.Point(9, 19)
        Me.rbtnYesBusCreditQuestion1.Name = "rbtnYesBusCreditQuestion1"
        Me.rbtnYesBusCreditQuestion1.Size = New System.Drawing.Size(41, 17)
        Me.rbtnYesBusCreditQuestion1.TabIndex = 0
        Me.rbtnYesBusCreditQuestion1.TabStop = True
        Me.rbtnYesBusCreditQuestion1.Text = "yes"
        Me.rbtnYesBusCreditQuestion1.UseVisualStyleBackColor = True
        '
        'lblBusCreditCheck
        '
        Me.lblBusCreditCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBusCreditCheck.Location = New System.Drawing.Point(222, 11)
        Me.lblBusCreditCheck.Name = "lblBusCreditCheck"
        Me.lblBusCreditCheck.Size = New System.Drawing.Size(470, 26)
        Me.lblBusCreditCheck.TabIndex = 18
        Me.lblBusCreditCheck.Text = "Veda: Check Enquiries; 1st Enquiry?   Less than 5 Enquiries per year?   Who are t" & _
    "hey to?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Finance Companies?   Consistant to what declared?"
        Me.lblBusCreditCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBusCreditHistory
        '
        Me.lblBusCreditHistory.AutoSize = True
        Me.lblBusCreditHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBusCreditHistory.Location = New System.Drawing.Point(16, 37)
        Me.lblBusCreditHistory.Name = "lblBusCreditHistory"
        Me.lblBusCreditHistory.Size = New System.Drawing.Size(167, 13)
        Me.lblBusCreditHistory.TabIndex = 19
        Me.lblBusCreditHistory.Text = "Comments on History - work notes"
        '
        'lblBusCreditDefaults
        '
        Me.lblBusCreditDefaults.AutoSize = True
        Me.lblBusCreditDefaults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBusCreditDefaults.Location = New System.Drawing.Point(16, 122)
        Me.lblBusCreditDefaults.Name = "lblBusCreditDefaults"
        Me.lblBusCreditDefaults.Size = New System.Drawing.Size(166, 13)
        Me.lblBusCreditDefaults.TabIndex = 21
        Me.lblBusCreditDefaults.Text = "Defaults / Collection Explanations"
        '
        'lblBusCreditPPSR
        '
        Me.lblBusCreditPPSR.AutoSize = True
        Me.lblBusCreditPPSR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBusCreditPPSR.Location = New System.Drawing.Point(16, 207)
        Me.lblBusCreditPPSR.Name = "lblBusCreditPPSR"
        Me.lblBusCreditPPSR.Size = New System.Drawing.Size(125, 13)
        Me.lblBusCreditPPSR.TabIndex = 23
        Me.lblBusCreditPPSR.Text = "PPSR and Other Checks"
        '
        'gpbxCoTenancy
        '
        Me.gpbxCoTenancy.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCoTenancy.Controls.Add(Me.tlpCompanyTenancy)
        Me.gpbxCoTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCoTenancy.Location = New System.Drawing.Point(12, 594)
        Me.gpbxCoTenancy.Name = "gpbxCoTenancy"
        Me.gpbxCoTenancy.Size = New System.Drawing.Size(901, 115)
        Me.gpbxCoTenancy.TabIndex = 136
        Me.gpbxCoTenancy.TabStop = False
        Me.gpbxCoTenancy.Text = "Company Tenancy"
        '
        'tlpCompanyTenancy
        '
        Me.tlpCompanyTenancy.ColumnCount = 3
        Me.tlpCompanyTenancy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpCompanyTenancy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyTenancy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpCompanyTenancy.Controls.Add(Me.lblTenancyComments, 0, 2)
        Me.tlpCompanyTenancy.Controls.Add(Me.lblLengthTen, 0, 1)
        Me.tlpCompanyTenancy.Controls.Add(Me.cmbxTenancy, 1, 0)
        Me.tlpCompanyTenancy.Controls.Add(Me.lblTenancyIs, 0, 0)
        Me.tlpCompanyTenancy.Controls.Add(Me.txtbxLengthTen, 1, 1)
        Me.tlpCompanyTenancy.Controls.Add(Me.txtbxCompanyTenancySatis, 1, 2)
        Me.tlpCompanyTenancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpCompanyTenancy.Location = New System.Drawing.Point(4, 16)
        Me.tlpCompanyTenancy.Name = "tlpCompanyTenancy"
        Me.tlpCompanyTenancy.RowCount = 3
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpCompanyTenancy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpCompanyTenancy.Size = New System.Drawing.Size(886, 91)
        Me.tlpCompanyTenancy.TabIndex = 1
        '
        'lblTenancyComments
        '
        Me.lblTenancyComments.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblTenancyComments.AutoSize = True
        Me.lblTenancyComments.Location = New System.Drawing.Point(39, 64)
        Me.lblTenancyComments.Name = "lblTenancyComments"
        Me.lblTenancyComments.Size = New System.Drawing.Size(258, 13)
        Me.lblTenancyComments.TabIndex = 5
        Me.lblTenancyComments.Text = "This is satisfactory for the term of the loan because ..."
        Me.lblTenancyComments.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLengthTen
        '
        Me.lblLengthTen.AutoSize = True
        Me.lblLengthTen.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblLengthTen.Location = New System.Drawing.Point(198, 25)
        Me.lblLengthTen.Name = "lblLengthTen"
        Me.lblLengthTen.Size = New System.Drawing.Size(99, 25)
        Me.lblLengthTen.TabIndex = 3
        Me.lblLengthTen.Text = "Length of tenancy?"
        Me.lblLengthTen.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbxTenancy
        '
        Me.cmbxTenancy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTenancy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTenancy.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancyType", True))
        Me.cmbxTenancy.FormattingEnabled = True
        Me.cmbxTenancy.Items.AddRange(New Object() {"Leased", "Sub-leased", "Rented", "Home based", "Owned"})
        Me.cmbxTenancy.Location = New System.Drawing.Point(303, 3)
        Me.cmbxTenancy.Name = "cmbxTenancy"
        Me.cmbxTenancy.Size = New System.Drawing.Size(173, 21)
        Me.cmbxTenancy.TabIndex = 2
        '
        'lblTenancyIs
        '
        Me.lblTenancyIs.AutoSize = True
        Me.lblTenancyIs.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTenancyIs.Location = New System.Drawing.Point(238, 0)
        Me.lblTenancyIs.Name = "lblTenancyIs"
        Me.lblTenancyIs.Size = New System.Drawing.Size(59, 25)
        Me.lblTenancyIs.TabIndex = 1
        Me.lblTenancyIs.Text = "Tenancy is"
        Me.lblTenancyIs.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxLengthTen
        '
        Me.txtbxLengthTen.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancyLength", True))
        Me.txtbxLengthTen.IsReadOnly = False
        Me.txtbxLengthTen.IsUndoEnabled = True
        Me.txtbxLengthTen.Location = New System.Drawing.Point(303, 28)
        Me.txtbxLengthTen.MaxLength = 20
        Me.txtbxLengthTen.Name = "txtbxLengthTen"
        Me.txtbxLengthTen.SelectedText = ""
        Me.txtbxLengthTen.SelectionLength = 0
        Me.txtbxLengthTen.SelectionStart = 0
        Me.txtbxLengthTen.Size = New System.Drawing.Size(287, 19)
        Me.txtbxLengthTen.TabIndex = 3
        Me.txtbxLengthTen.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxCompanyTenancySatis
        '
        Me.tlpCompanyTenancy.SetColumnSpan(Me.txtbxCompanyTenancySatis, 2)
        Me.txtbxCompanyTenancySatis.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TenancySatisfactory", True))
        Me.txtbxCompanyTenancySatis.IsReadOnly = False
        Me.txtbxCompanyTenancySatis.IsUndoEnabled = True
        Me.txtbxCompanyTenancySatis.Location = New System.Drawing.Point(303, 53)
        Me.txtbxCompanyTenancySatis.MaxLength = 0
        Me.txtbxCompanyTenancySatis.MultiLine = True
        Me.txtbxCompanyTenancySatis.Name = "txtbxCompanyTenancySatis"
        Me.txtbxCompanyTenancySatis.SelectedText = ""
        Me.txtbxCompanyTenancySatis.SelectionLength = 0
        Me.txtbxCompanyTenancySatis.SelectionStart = 0
        Me.txtbxCompanyTenancySatis.Size = New System.Drawing.Size(580, 36)
        Me.txtbxCompanyTenancySatis.TabIndex = 5
        Me.txtbxCompanyTenancySatis.WordWrap = True
        Me.txtbxCompanyTenancySatis.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxDtsb01
        '
        Me.gpbxDtsb01.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxDtsb01.Controls.Add(Me.PanelDtsb01)
        Me.gpbxDtsb01.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxDtsb01.Location = New System.Drawing.Point(12, 247)
        Me.gpbxDtsb01.Name = "gpbxDtsb01"
        Me.gpbxDtsb01.Size = New System.Drawing.Size(901, 163)
        Me.gpbxDtsb01.TabIndex = 134
        Me.gpbxDtsb01.TabStop = False
        Me.gpbxDtsb01.Text = "Directors"
        '
        'PanelDtsb01
        '
        Me.PanelDtsb01.Controls.Add(Me.dgvDtsb01)
        Me.PanelDtsb01.Controls.Add(Me.tlpDirectors)
        Me.PanelDtsb01.Controls.Add(Me.btnAddNewDtsb01)
        Me.PanelDtsb01.Controls.Add(Me.lblDirectorsNotice)
        Me.PanelDtsb01.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDtsb01.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelDtsb01.Location = New System.Drawing.Point(3, 16)
        Me.PanelDtsb01.Name = "PanelDtsb01"
        Me.PanelDtsb01.Size = New System.Drawing.Size(895, 144)
        Me.PanelDtsb01.TabIndex = 11
        '
        'dgvDtsb01
        '
        Me.dgvDtsb01.AllowUserToAddRows = False
        Me.dgvDtsb01.AllowUserToDeleteRows = False
        Me.dgvDtsb01.AllowUserToResizeRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgvDtsb01.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvDtsb01.AutoGenerateColumns = False
        Me.dgvDtsb01.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvDtsb01.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvDtsb01.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDtsb01.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewCheckBoxColumn2, Me.AMLRisk, Me.Dtsb01Edit, Me.Dtsb01Delete})
        Me.dgvDtsb01.DataSource = Me.DTSB01BindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDtsb01.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvDtsb01.Location = New System.Drawing.Point(4, 26)
        Me.dgvDtsb01.MultiSelect = False
        Me.dgvDtsb01.Name = "dgvDtsb01"
        Me.dgvDtsb01.RowHeadersWidth = 10
        Me.dgvDtsb01.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvDtsb01.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDtsb01.Size = New System.Drawing.Size(887, 45)
        Me.dgvDtsb01.TabIndex = 6
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Id"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Id"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "CustomerId"
        Me.DataGridViewTextBoxColumn2.HeaderText = "CustomerId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "FirstName"
        Me.DataGridViewTextBoxColumn5.HeaderText = "FirstName"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "LastName"
        Me.DataGridViewTextBoxColumn6.HeaderText = "LastName"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "DateOfBirth"
        Me.DataGridViewTextBoxColumn7.HeaderText = "DateOfBirth"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 90
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "IdVerified"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "IdVerified"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        Me.DataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn1.Width = 70
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "AddressVerified"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "AddressVerified"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        Me.DataGridViewCheckBoxColumn2.ReadOnly = True
        Me.DataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'AMLRisk
        '
        Me.AMLRisk.DataPropertyName = "AMLRiskDesc"
        Me.AMLRisk.HeaderText = "AMLRisk"
        Me.AMLRisk.Name = "AMLRisk"
        Me.AMLRisk.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AMLRisk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Dtsb01Edit
        '
        Me.Dtsb01Edit.HeaderText = ""
        Me.Dtsb01Edit.Name = "Dtsb01Edit"
        Me.Dtsb01Edit.Text = "Edit"
        Me.Dtsb01Edit.UseColumnTextForButtonValue = True
        Me.Dtsb01Edit.Width = 70
        '
        'Dtsb01Delete
        '
        Me.Dtsb01Delete.HeaderText = ""
        Me.Dtsb01Delete.Name = "Dtsb01Delete"
        Me.Dtsb01Delete.Text = "Delete"
        Me.Dtsb01Delete.UseColumnTextForButtonValue = True
        Me.Dtsb01Delete.Width = 70
        '
        'DTSB01BindingSource
        '
        Me.DTSB01BindingSource.DataMember = "DTSB01"
        Me.DTSB01BindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'tlpDirectors
        '
        Me.tlpDirectors.ColumnCount = 3
        Me.tlpDirectors.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpDirectors.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpDirectors.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpDirectors.Controls.Add(Me.lblDirectors, 0, 1)
        Me.tlpDirectors.Controls.Add(Me.ckbxDirectorsTrustees, 0, 0)
        Me.tlpDirectors.Controls.Add(Me.txtDirectorsTrustees, 1, 1)
        Me.tlpDirectors.Location = New System.Drawing.Point(4, 76)
        Me.tlpDirectors.Name = "tlpDirectors"
        Me.tlpDirectors.RowCount = 2
        Me.tlpDirectors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpDirectors.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpDirectors.Size = New System.Drawing.Size(886, 67)
        Me.tlpDirectors.TabIndex = 7
        '
        'lblDirectors
        '
        Me.lblDirectors.AutoSize = True
        Me.lblDirectors.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblDirectors.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirectors.Location = New System.Drawing.Point(3, 25)
        Me.lblDirectors.Name = "lblDirectors"
        Me.lblDirectors.Size = New System.Drawing.Size(294, 42)
        Me.lblDirectors.TabIndex = 14
        Me.lblDirectors.Text = "Comments on Directors and their related parties/interests"
        Me.lblDirectors.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxDirectorsTrustees
        '
        Me.ckbxDirectorsTrustees.AutoSize = True
        Me.ckbxDirectorsTrustees.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessDirectors", True))
        Me.ckbxDirectorsTrustees.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxDirectorsTrustees.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxDirectorsTrustees.Location = New System.Drawing.Point(149, 3)
        Me.ckbxDirectorsTrustees.Name = "ckbxDirectorsTrustees"
        Me.ckbxDirectorsTrustees.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxDirectorsTrustees.Size = New System.Drawing.Size(148, 19)
        Me.ckbxDirectorsTrustees.TabIndex = 6
        Me.ckbxDirectorsTrustees.Text = "?Directors are satisfactory"
        Me.ckbxDirectorsTrustees.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxDirectorsTrustees.UseVisualStyleBackColor = True
        '
        'txtDirectorsTrustees
        '
        Me.tlpDirectors.SetColumnSpan(Me.txtDirectorsTrustees, 2)
        Me.txtDirectorsTrustees.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "BusinessDirectorNotes", True))
        Me.txtDirectorsTrustees.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDirectorsTrustees.IsReadOnly = False
        Me.txtDirectorsTrustees.IsUndoEnabled = True
        Me.txtDirectorsTrustees.Location = New System.Drawing.Point(303, 28)
        Me.txtDirectorsTrustees.MaxLength = 1024
        Me.txtDirectorsTrustees.MultiLine = True
        Me.txtDirectorsTrustees.Name = "txtDirectorsTrustees"
        Me.txtDirectorsTrustees.SelectedText = ""
        Me.txtDirectorsTrustees.SelectionLength = 0
        Me.txtDirectorsTrustees.SelectionStart = 0
        Me.txtDirectorsTrustees.Size = New System.Drawing.Size(580, 36)
        Me.txtDirectorsTrustees.TabIndex = 15
        Me.txtDirectorsTrustees.WordWrap = True
        Me.txtDirectorsTrustees.Child = New System.Windows.Controls.TextBox()
        '
        'btnAddNewDtsb01
        '
        Me.btnAddNewDtsb01.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddNewDtsb01.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddNewDtsb01.Location = New System.Drawing.Point(812, 1)
        Me.btnAddNewDtsb01.Name = "btnAddNewDtsb01"
        Me.btnAddNewDtsb01.Size = New System.Drawing.Size(75, 23)
        Me.btnAddNewDtsb01.TabIndex = 8
        Me.btnAddNewDtsb01.Text = "Add new"
        Me.btnAddNewDtsb01.UseVisualStyleBackColor = False
        '
        'lblDirectorsNotice
        '
        Me.lblDirectorsNotice.AutoSize = True
        Me.lblDirectorsNotice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirectorsNotice.ForeColor = System.Drawing.Color.Blue
        Me.lblDirectorsNotice.Location = New System.Drawing.Point(47, 6)
        Me.lblDirectorsNotice.Name = "lblDirectorsNotice"
        Me.lblDirectorsNotice.Size = New System.Drawing.Size(274, 13)
        Me.lblDirectorsNotice.TabIndex = 10
        Me.lblDirectorsNotice.Text = "A customer must be saved before you can add a director"
        Me.lblDirectorsNotice.Visible = False
        '
        'gpbxCompanyInfo
        '
        Me.gpbxCompanyInfo.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCompanyInfo.Controls.Add(Me.tlpInformation)
        Me.gpbxCompanyInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxCompanyInfo.Location = New System.Drawing.Point(12, 4)
        Me.gpbxCompanyInfo.Name = "gpbxCompanyInfo"
        Me.gpbxCompanyInfo.Size = New System.Drawing.Size(901, 232)
        Me.gpbxCompanyInfo.TabIndex = 0
        Me.gpbxCompanyInfo.TabStop = False
        Me.gpbxCompanyInfo.Text = "Company Information"
        '
        'tlpInformation
        '
        Me.tlpInformation.ColumnCount = 3
        Me.tlpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpInformation.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpInformation.Controls.Add(Me.lblCompanyAddresses, 0, 6)
        Me.tlpInformation.Controls.Add(Me.ckbxAddresses, 0, 5)
        Me.tlpInformation.Controls.Add(Me.lblHistoryComments, 0, 4)
        Me.tlpInformation.Controls.Add(Me.ckbxHistory, 0, 3)
        Me.tlpInformation.Controls.Add(Me.ckbxCapital, 1, 2)
        Me.tlpInformation.Controls.Add(Me.ckbxReturns, 0, 2)
        Me.tlpInformation.Controls.Add(Me.ckbxRegistered, 0, 1)
        Me.tlpInformation.Controls.Add(Me.ckbxCoExtTrustDdSaved, 1, 1)
        Me.tlpInformation.Controls.Add(Me.lblName, 0, 0)
        Me.tlpInformation.Controls.Add(Me.txtCompanyName, 1, 0)
        Me.tlpInformation.Controls.Add(Me.txtbxHistoryComments, 1, 4)
        Me.tlpInformation.Controls.Add(Me.txtbxAddressesComments, 1, 6)
        Me.tlpInformation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpInformation.Location = New System.Drawing.Point(6, 15)
        Me.tlpInformation.Name = "tlpInformation"
        Me.tlpInformation.RowCount = 7
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpInformation.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpInformation.Size = New System.Drawing.Size(887, 212)
        Me.tlpInformation.TabIndex = 0
        '
        'lblCompanyAddresses
        '
        Me.lblCompanyAddresses.AutoSize = True
        Me.lblCompanyAddresses.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCompanyAddresses.Location = New System.Drawing.Point(114, 167)
        Me.lblCompanyAddresses.Name = "lblCompanyAddresses"
        Me.lblCompanyAddresses.Size = New System.Drawing.Size(183, 45)
        Me.lblCompanyAddresses.TabIndex = 23
        Me.lblCompanyAddresses.Text = "Comment on the Company Addresses"
        Me.lblCompanyAddresses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ckbxAddresses
        '
        Me.ckbxAddresses.AutoSize = True
        Me.ckbxAddresses.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessAddressSatisfactory", True))
        Me.ckbxAddresses.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxAddresses.Location = New System.Drawing.Point(142, 145)
        Me.ckbxAddresses.Name = "ckbxAddresses"
        Me.ckbxAddresses.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxAddresses.Size = New System.Drawing.Size(155, 19)
        Me.ckbxAddresses.TabIndex = 22
        Me.ckbxAddresses.Text = "?Addresses are satisfactory"
        Me.ckbxAddresses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxAddresses.UseVisualStyleBackColor = True
        '
        'lblHistoryComments
        '
        Me.lblHistoryComments.AutoSize = True
        Me.lblHistoryComments.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblHistoryComments.Location = New System.Drawing.Point(119, 100)
        Me.lblHistoryComments.Name = "lblHistoryComments"
        Me.lblHistoryComments.Size = New System.Drawing.Size(178, 42)
        Me.lblHistoryComments.TabIndex = 20
        Me.lblHistoryComments.Text = "Comments on the Company's History"
        Me.lblHistoryComments.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ckbxHistory
        '
        Me.ckbxHistory.AutoSize = True
        Me.ckbxHistory.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessHistorySatisfactory", True))
        Me.ckbxHistory.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxHistory.Location = New System.Drawing.Point(122, 78)
        Me.ckbxHistory.Name = "ckbxHistory"
        Me.ckbxHistory.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxHistory.Size = New System.Drawing.Size(175, 19)
        Me.ckbxHistory.TabIndex = 19
        Me.ckbxHistory.Text = "?Company history is satisfactory"
        Me.ckbxHistory.UseVisualStyleBackColor = True
        '
        'ckbxCapital
        '
        Me.ckbxCapital.AutoSize = True
        Me.ckbxCapital.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessPaidUpCapital", True))
        Me.ckbxCapital.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxCapital.Location = New System.Drawing.Point(433, 53)
        Me.ckbxCapital.Name = "ckbxCapital"
        Me.ckbxCapital.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxCapital.Size = New System.Drawing.Size(157, 19)
        Me.ckbxCapital.TabIndex = 5
        Me.ckbxCapital.Text = "?Paid-up capital is sufficient"
        Me.ckbxCapital.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxCapital.UseVisualStyleBackColor = True
        '
        'ckbxReturns
        '
        Me.ckbxReturns.AutoSize = True
        Me.ckbxReturns.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessReturnsFiled", True))
        Me.ckbxReturns.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxReturns.Location = New System.Drawing.Point(206, 53)
        Me.ckbxReturns.Name = "ckbxReturns"
        Me.ckbxReturns.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxReturns.Size = New System.Drawing.Size(91, 19)
        Me.ckbxReturns.TabIndex = 4
        Me.ckbxReturns.Text = "?Returns filed"
        Me.ckbxReturns.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxReturns.UseVisualStyleBackColor = True
        '
        'ckbxRegistered
        '
        Me.ckbxRegistered.AutoSize = True
        Me.ckbxRegistered.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "BusinessRegistered", True))
        Me.ckbxRegistered.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxRegistered.Location = New System.Drawing.Point(172, 28)
        Me.ckbxRegistered.Name = "ckbxRegistered"
        Me.ckbxRegistered.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxRegistered.Size = New System.Drawing.Size(125, 19)
        Me.ckbxRegistered.TabIndex = 2
        Me.ckbxRegistered.Text = "?Company registered"
        Me.ckbxRegistered.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxRegistered.UseVisualStyleBackColor = True
        '
        'ckbxCoExtTrustDdSaved
        '
        Me.ckbxCoExtTrustDdSaved.AutoSize = True
        Me.ckbxCoExtTrustDdSaved.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CustomerBindingSource, "CoExtractTrustDeed", True))
        Me.ckbxCoExtTrustDdSaved.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxCoExtTrustDdSaved.Location = New System.Drawing.Point(452, 28)
        Me.ckbxCoExtTrustDdSaved.Name = "ckbxCoExtTrustDdSaved"
        Me.ckbxCoExtTrustDdSaved.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxCoExtTrustDdSaved.Size = New System.Drawing.Size(138, 19)
        Me.ckbxCoExtTrustDdSaved.TabIndex = 3
        Me.ckbxCoExtTrustDdSaved.Text = "Company Extract saved"
        Me.ckbxCoExtTrustDdSaved.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxCoExtTrustDdSaved.UseVisualStyleBackColor = True
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblName.Location = New System.Drawing.Point(262, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(35, 25)
        Me.lblName.TabIndex = 25
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCompanyName
        '
        Me.tlpInformation.SetColumnSpan(Me.txtCompanyName, 2)
        Me.txtCompanyName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "LegalName", True))
        Me.txtCompanyName.IsReadOnly = False
        Me.txtCompanyName.IsUndoEnabled = True
        Me.txtCompanyName.Location = New System.Drawing.Point(303, 3)
        Me.txtCompanyName.MaxLength = 50
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.SelectedText = ""
        Me.txtCompanyName.SelectionLength = 0
        Me.txtCompanyName.SelectionStart = 0
        Me.txtCompanyName.Size = New System.Drawing.Size(581, 19)
        Me.txtCompanyName.TabIndex = 1
        Me.txtCompanyName.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxHistoryComments
        '
        Me.tlpInformation.SetColumnSpan(Me.txtbxHistoryComments, 2)
        Me.txtbxHistoryComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "BusinessHistoryNotes", True))
        Me.txtbxHistoryComments.IsReadOnly = False
        Me.txtbxHistoryComments.IsUndoEnabled = True
        Me.txtbxHistoryComments.Location = New System.Drawing.Point(303, 103)
        Me.txtbxHistoryComments.MaxLength = 1024
        Me.txtbxHistoryComments.MultiLine = True
        Me.txtbxHistoryComments.Name = "txtbxHistoryComments"
        Me.txtbxHistoryComments.SelectedText = ""
        Me.txtbxHistoryComments.SelectionLength = 0
        Me.txtbxHistoryComments.SelectionStart = 0
        Me.txtbxHistoryComments.Size = New System.Drawing.Size(580, 36)
        Me.txtbxHistoryComments.TabIndex = 21
        Me.txtbxHistoryComments.WordWrap = True
        Me.txtbxHistoryComments.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxAddressesComments
        '
        Me.tlpInformation.SetColumnSpan(Me.txtbxAddressesComments, 2)
        Me.txtbxAddressesComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "BusinessAddressNotes", True))
        Me.txtbxAddressesComments.IsReadOnly = False
        Me.txtbxAddressesComments.IsUndoEnabled = True
        Me.txtbxAddressesComments.Location = New System.Drawing.Point(303, 170)
        Me.txtbxAddressesComments.MaxLength = 1024
        Me.txtbxAddressesComments.MultiLine = True
        Me.txtbxAddressesComments.Name = "txtbxAddressesComments"
        Me.txtbxAddressesComments.SelectedText = ""
        Me.txtbxAddressesComments.SelectionLength = 0
        Me.txtbxAddressesComments.SelectionStart = 0
        Me.txtbxAddressesComments.Size = New System.Drawing.Size(580, 36)
        Me.txtbxAddressesComments.TabIndex = 24
        Me.txtbxAddressesComments.WordWrap = True
        Me.txtbxAddressesComments.Child = New System.Windows.Controls.TextBox()
        '
        'EnumAMLRiskBindingSource
        '
        Me.EnumAMLRiskBindingSource.DataMember = "Enum_AMLRisk"
        Me.EnumAMLRiskBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DTSBTableAdapter
        '
        Me.DTSBTableAdapter.ClearBeforeFill = True
        '
        'DTSB01TableAdapter
        '
        Me.DTSB01TableAdapter.ClearBeforeFill = True
        '
        'TradeReferencesTableAdapter
        '
        Me.TradeReferencesTableAdapter.ClearBeforeFill = True
        '
        'ItemTemplate
        '
        Me.ItemTemplate.Size = New System.Drawing.Size(876, 174)
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "Security"
        Me.SecurityBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'Enum_AMLRiskTableAdapter
        '
        Me.Enum_AMLRiskTableAdapter.ClearBeforeFill = True
        '
        'WkShtWizForm3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.PrelimFormSize
        Me.Controls.Add(Me.pnlHeader)
        Me.Controls.Add(Me.pnlCustomerType)
        Me.Controls.Add(Me.pnlIndividual)
        Me.Controls.Add(Me.pnlCompany)
        Me.Controls.Add(Me.pnlFooter)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "PrelimFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "PrelimFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.PrelimFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "WkShtWizForm3"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Due Diligence Stability"
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.pnlIndividual.ResumeLayout(False)
        Me.pnlIndividual.PerformLayout()
        CType(Me.CustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxCredit.ResumeLayout(False)
        Me.gpbxCredit.PerformLayout()
        Me.gpbxCreditQuestion3.ResumeLayout(False)
        Me.gpbxCreditQuestion3.PerformLayout()
        Me.gpbxCreditQuestion2.ResumeLayout(False)
        Me.gpbxCreditQuestion2.PerformLayout()
        Me.gpbxCreditQuestion1.ResumeLayout(False)
        Me.gpbxCreditQuestion1.PerformLayout()
        Me.gpbxCustomerName.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.gpbxIdentity.ResumeLayout(False)
        Me.gpbxIdentity.PerformLayout()
        CType(Me.pbPEP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxEmployment.ResumeLayout(False)
        Me.gpbxEmployment.PerformLayout()
        Me.gpbxTenancy.ResumeLayout(False)
        Me.gpbxTenancy.PerformLayout()
        CType(Me.TypeOfTenancyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEnquiryCustomers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryCustomersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        Me.pnlFooter.ResumeLayout(False)
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlHeader.PerformLayout()
        CType(Me.pbImportCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlCustomerType.ResumeLayout(False)
        Me.pnlCustomerType.PerformLayout()
        CType(Me.pbHA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ListAMLRiskBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.List2AMLRisk, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Enum_CustomerTypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerEnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCompany.ResumeLayout(False)
        Me.gpbxTradeRef.ResumeLayout(False)
        Me.drReferences.ItemTemplate.ResumeLayout(False)
        Me.drReferences.ItemTemplate.PerformLayout()
        Me.drReferences.ResumeLayout(False)
        CType(Me.TradeReferencesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxBusOther.ResumeLayout(False)
        Me.gpbxDtsb23.ResumeLayout(False)
        Me.PanelDtsb23.ResumeLayout(False)
        Me.PanelDtsb23.PerformLayout()
        CType(Me.pbPEP2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpShareholders.ResumeLayout(False)
        Me.tlpShareholders.PerformLayout()
        CType(Me.dgvDTSBType2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DTSBBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxBusinessCredit.ResumeLayout(False)
        Me.gpbxBusinessCredit.PerformLayout()
        Me.gpbxBusCreditQuestion3.ResumeLayout(False)
        Me.gpbxBusCreditQuestion3.PerformLayout()
        Me.gpbxBusCreditQuestion2.ResumeLayout(False)
        Me.gpbxBusCreditQuestion2.PerformLayout()
        Me.gpbxBusCreditQuestion1.ResumeLayout(False)
        Me.gpbxBusCreditQuestion1.PerformLayout()
        Me.gpbxCoTenancy.ResumeLayout(False)
        Me.tlpCompanyTenancy.ResumeLayout(False)
        Me.tlpCompanyTenancy.PerformLayout()
        Me.gpbxDtsb01.ResumeLayout(False)
        Me.PanelDtsb01.ResumeLayout(False)
        Me.PanelDtsb01.PerformLayout()
        CType(Me.dgvDtsb01, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DTSB01BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpDirectors.ResumeLayout(False)
        Me.tlpDirectors.PerformLayout()
        Me.gpbxCompanyInfo.ResumeLayout(False)
        Me.tlpInformation.ResumeLayout(False)
        Me.tlpInformation.PerformLayout()
        CType(Me.EnumAMLRiskBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents pnlIndividual As System.Windows.Forms.Panel
    Friend WithEvents lblOther As System.Windows.Forms.Label
    Friend WithEvents lblWorker As System.Windows.Forms.Label
    Friend WithEvents lblJobSafe As System.Windows.Forms.Label
    Friend WithEvents txtbxQuestion5 As System.Windows.Forms.TextBox
    Friend WithEvents lblPay As System.Windows.Forms.Label
    Friend WithEvents lblHowLong As System.Windows.Forms.Label
    Friend WithEvents lblWhatDo As System.Windows.Forms.Label
    Friend WithEvents lblFulltime As System.Windows.Forms.Label
    Friend WithEvents lblWorkCompany As System.Windows.Forms.Label
    Friend WithEvents lblSalutation As System.Windows.Forms.Label
    Friend WithEvents lblDoes As System.Windows.Forms.Label
    Friend WithEvents txtbxSpokeTo As System.Windows.Forms.TextBox
    Friend WithEvents lblSpokeTo As System.Windows.Forms.Label
    Friend WithEvents txtbxEmployer As System.Windows.Forms.TextBox
    Friend WithEvents lblEmployer As System.Windows.Forms.Label
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents pnlFooter As System.Windows.Forms.Panel
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblEmploy9 As System.Windows.Forms.Label
    Friend WithEvents lblRangEmpTimeStamp As System.Windows.Forms.Label
    Friend WithEvents btnRangEmpTimeStamp As System.Windows.Forms.Button
    Friend WithEvents lblResidenceSatis As System.Windows.Forms.Label
    Friend WithEvents txtbxTenancy As System.Windows.Forms.TextBox
    Friend WithEvents lblResidence As System.Windows.Forms.Label
    Friend WithEvents cmbxTenancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTenancy As System.Windows.Forms.Label
    Friend WithEvents lblTenancyBecause As System.Windows.Forms.Label
    Friend WithEvents lblTenancyEstablish As System.Windows.Forms.Label
    Friend WithEvents gpbxTenancy As System.Windows.Forms.GroupBox
    Friend WithEvents gpbxEmployment As System.Windows.Forms.GroupBox
    Friend WithEvents TypeOfTenancyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TypeOfTenancyTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfTenancyTableAdapter
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents btnQRGList As System.Windows.Forms.Button
    Friend WithEvents lblContactAddress As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblContactName As System.Windows.Forms.Label
    Friend WithEvents lblContactNumber As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblBeneficiary As System.Windows.Forms.Label
    Friend WithEvents ckbxBeneficiary As System.Windows.Forms.CheckBox
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEnquiryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeclinedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WithdrawnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblEmployerNotRung As System.Windows.Forms.Label
    Friend WithEvents ckbxEmployerNotRung As System.Windows.Forms.CheckBox
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoan As System.Windows.Forms.Label
    Friend WithEvents EmailApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DealersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gpbxIdentity As System.Windows.Forms.GroupBox
    Friend WithEvents lblAgeOfId As System.Windows.Forms.Label
    Friend WithEvents lblResidency As System.Windows.Forms.Label
    Friend WithEvents lblIdIsPerson As System.Windows.Forms.Label
    Friend WithEvents ckbxIdCurrent As System.Windows.Forms.CheckBox
    Friend WithEvents cmbxRiskAssess As System.Windows.Forms.ComboBox
    Friend WithEvents lblRiskAssess As System.Windows.Forms.Label
    Friend WithEvents ListAMLRiskBS As System.Windows.Forms.BindingSource
    Friend WithEvents List_AMLRiskTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.List_AMLRiskTableAdapter
    Friend WithEvents List2AMLRisk As System.Windows.Forms.BindingSource
    Friend WithEvents MCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnAddCustomer As System.Windows.Forms.Button
    Friend WithEvents dgvEnquiryCustomers As System.Windows.Forms.DataGridView
    Friend WithEvents lblCustomerType As System.Windows.Forms.Label
    Friend WithEvents Enum_CustomerTypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnDeleteCustomer As System.Windows.Forms.Button
    Friend WithEvents btnSaveCustomer As System.Windows.Forms.Button
    Friend WithEvents cmbxCustomerType As System.Windows.Forms.ComboBox
    Friend WithEvents gpbxCustomerName As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnCancelCustomer As System.Windows.Forms.Button
    Friend WithEvents lblCustomerTitle As System.Windows.Forms.Label
    Friend WithEvents cmbxCustomerTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents txtbxFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtMiddleNames As System.Windows.Forms.TextBox
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents gpbxCredit As System.Windows.Forms.GroupBox
    Friend WithEvents gpbxCreditQuestion3 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNoCreditQuestion3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnYesCreditQuestion3 As System.Windows.Forms.RadioButton
    Friend WithEvents gpbxCreditQuestion2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNoCreditQuestion2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnYesCreditQuestion2 As System.Windows.Forms.RadioButton
    Friend WithEvents gpbxCreditQuestion1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNoCreditQuestion1 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnYesCreditQuestion1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblCreditCheck As System.Windows.Forms.Label
    Friend WithEvents lblCreditHistory As System.Windows.Forms.Label
    Friend WithEvents lblCreditDefaults As System.Windows.Forms.Label
    Friend WithEvents lblCreditPPSR As System.Windows.Forms.Label
    Friend WithEvents pnlCustomerType As System.Windows.Forms.Panel
    Friend WithEvents EnquiryCustomersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryCustomersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
    Friend WithEvents CustomerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CustomerTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents CustomerEnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CustomerEnquiryTableAdapter
    Friend WithEvents CustomerEnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents pnlCompany As System.Windows.Forms.Panel
    Friend WithEvents gpbxCompanyInfo As System.Windows.Forms.GroupBox
    Friend WithEvents tlpInformation As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ckbxRegistered As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxCoExtTrustDdSaved As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxReturns As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxCapital As System.Windows.Forms.CheckBox
    Friend WithEvents dgvDTSBType2 As System.Windows.Forms.DataGridView
    Friend WithEvents DTSBBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DTSBTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
    Friend WithEvents ckbxHistory As System.Windows.Forms.CheckBox
    Friend WithEvents lblHistoryComments As System.Windows.Forms.Label
    Friend WithEvents ckbxAddresses As System.Windows.Forms.CheckBox
    Friend WithEvents lblCompanyAddresses As System.Windows.Forms.Label
    Friend WithEvents gpbxDtsb23 As System.Windows.Forms.GroupBox
    Friend WithEvents tlpShareholders As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ckbxShareholders As System.Windows.Forms.CheckBox
    Friend WithEvents lblShareholdersNote As System.Windows.Forms.Label
    Friend WithEvents btnAddNewST As System.Windows.Forms.Button
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents IdnumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerEnquiryTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnquiryIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gpbxDtsb01 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAddNewDtsb01 As System.Windows.Forms.Button
    Friend WithEvents tlpDirectors As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblDirectors As System.Windows.Forms.Label
    Friend WithEvents ckbxDirectorsTrustees As System.Windows.Forms.CheckBox
    Friend WithEvents dgvDtsb01 As System.Windows.Forms.DataGridView
    Friend WithEvents DTSB01BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DTSB01TableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
    Friend WithEvents gpbxCoTenancy As System.Windows.Forms.GroupBox
    Friend WithEvents tlpCompanyTenancy As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblTenancyComments As System.Windows.Forms.Label
    Friend WithEvents lblLengthTen As System.Windows.Forms.Label
    Friend WithEvents cmbxTenancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblTenancyIs As System.Windows.Forms.Label
    Friend WithEvents ckbxShareIdentified As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxShareStructure As System.Windows.Forms.CheckBox
    Friend WithEvents gpbxBusinessCredit As System.Windows.Forms.GroupBox
    Friend WithEvents gpbxBusCreditQuestion3 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNoBusCreditQuestion3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnYesBusCreditQuestion3 As System.Windows.Forms.RadioButton
    Friend WithEvents gpbxBusCreditQuestion2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNoBusCreditQuestion2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnYesBusCreditQuestion2 As System.Windows.Forms.RadioButton
    Friend WithEvents gpbxBusCreditQuestion1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNoBusCreditQuestion1 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnYesBusCreditQuestion1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblBusCreditCheck As System.Windows.Forms.Label
    Friend WithEvents lblBusCreditHistory As System.Windows.Forms.Label
    Friend WithEvents lblBusCreditDefaults As System.Windows.Forms.Label
    Friend WithEvents lblBusCreditPPSR As System.Windows.Forms.Label
    Friend WithEvents gpbxBusOther As System.Windows.Forms.GroupBox
    Friend WithEvents gpbxTradeRef As System.Windows.Forms.GroupBox
    Friend WithEvents TradeReferencesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TradeReferencesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
    Friend WithEvents drReferences As Microsoft.VisualBasic.PowerPacks.DataRepeater
    Friend WithEvents txtTradeRefPay As System.Windows.Forms.TextBox
    Friend WithEvents txtTradeRefSpend As System.Windows.Forms.TextBox
    Friend WithEvents txtTradeRefAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnTradeRefEdit As System.Windows.Forms.Button
    Friend WithEvents btnTradeRefDelete As System.Windows.Forms.Button
    Friend WithEvents txtTradeRefName As System.Windows.Forms.TextBox
    Friend WithEvents ItemTemplate As Microsoft.VisualBasic.PowerPacks.DataRepeaterItem
    Friend WithEvents btnNewRef As System.Windows.Forms.Button
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
    Friend WithEvents lblShareholdersNotice As System.Windows.Forms.Label
    Friend WithEvents lblDirectorsNotice As System.Windows.Forms.Label
    Friend WithEvents pbPEP As System.Windows.Forms.PictureBox
    Friend WithEvents rbPepNo As System.Windows.Forms.RadioButton
    Friend WithEvents rbPepYes As System.Windows.Forms.RadioButton
    Friend WithEvents lblPep As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents AnimatedCirclePanel As System.Windows.Forms.Panel
    Friend WithEvents pbImportCustomer As System.Windows.Forms.PictureBox
    Friend WithEvents cmbxAcceptanceMethod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAcceptanceMethod As System.Windows.Forms.Label
    Friend WithEvents cmbxCustEnquiryType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCustEnquiryType As System.Windows.Forms.Label
    Friend WithEvents txtCompanyName As AppWhShtB.SpellBox
    Friend WithEvents txtbxHistoryComments As AppWhShtB.SpellBox
    Friend WithEvents txtbxAddressesComments As AppWhShtB.SpellBox
    Friend WithEvents txtDirectorsTrustees As AppWhShtB.SpellBox
    Friend WithEvents txtShareholdersComments As AppWhShtB.SpellBox
    Friend WithEvents txtbxLengthTen As AppWhShtB.SpellBox
    Friend WithEvents txtbxCompanyTenancySatis As AppWhShtB.SpellBox
    Friend WithEvents txtBusCreditHistory As AppWhShtB.SpellBox
    Friend WithEvents txtBusCreditDefaults As AppWhShtB.SpellBox
    Friend WithEvents txtBusCreditPPSR As AppWhShtB.SpellBox
    Friend WithEvents txtBusOtherComments As AppWhShtB.SpellBox
    Friend WithEvents txtTradeRefComments As AppWhShtB.SpellBox
    Friend WithEvents txtIdIsPerson As AppWhShtB.SpellBox
    Friend WithEvents txtResidency As AppWhShtB.SpellBox
    Friend WithEvents txtAgeOfId As AppWhShtB.SpellBox
    Friend WithEvents txtbxResidenceSatis As AppWhShtB.SpellBox
    Friend WithEvents txtbxTenancyEnviron As AppWhShtB.SpellBox
    Friend WithEvents txtbxEstablishTenancy As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion1 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion2 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion9 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion3 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion4 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion6 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion7 As AppWhShtB.SpellBox
    Friend WithEvents txtbxQuestion8 As AppWhShtB.SpellBox
    Friend WithEvents rtxtbxStabilityOther As AppWhShtB.SpellBox
    Friend WithEvents txtbxCreditHistory As AppWhShtB.SpellBox
    Friend WithEvents txtbxCreditDefaults As AppWhShtB.SpellBox
    Friend WithEvents txtbxCreditPPSR As AppWhShtB.SpellBox
    Friend WithEvents PanelDtsb01 As System.Windows.Forms.Panel
    Friend WithEvents PanelDtsb23 As System.Windows.Forms.Panel
    Friend WithEvents lblTypeOfId As System.Windows.Forms.Label
    Friend WithEvents cmbxTypeDL As System.Windows.Forms.ComboBox
    Friend WithEvents lblTypeDL As System.Windows.Forms.Label
    Friend WithEvents cmbxTypeOfId As System.Windows.Forms.ComboBox
    Friend WithEvents pbHA As System.Windows.Forms.PictureBox
    Friend WithEvents DocsReceivedImg As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsReceivedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pbPEP2 As System.Windows.Forms.PictureBox
    Friend WithEvents EnumAMLRiskBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Enum_AMLRiskTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.Enum_AMLRiskTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents AMLRisk As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dtsb01Edit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Dtsb01Delete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents IdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerIdDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FirstNameDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LastNameDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateOfBirthDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdVerifiedDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents AddressVerifiedDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents AMLRiskDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsaPEP As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Edit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Delete As System.Windows.Forms.DataGridViewButtonColumn
End Class
