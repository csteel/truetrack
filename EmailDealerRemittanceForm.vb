﻿Imports System.Net.Mail
Imports System.IO


Public Class EmailDealerRemittanceForm
    Dim thisEnquiryId As Integer
    'Dim signature As String
    Dim thisApplicationCode As String
    Dim thisContactDisplayName As String
    Dim thisDealerName As String
    Dim fileAttch As New ArrayList 'List of attachments
    Dim attachListString As String = "" 'String listing attachment file names
    Dim attachmentError As Boolean = False
    Dim thisDealerId As String
    Dim dealerEmail As String
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)


    'Loads form with parameters from Calling Form
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer, ByVal loadDealerIdVar As String)
        thisEnquiryId = loadEnquiryVar
        thisDealerId = loadDealerIdVar
        'set email from address
        txtbxEmailFrom.Text = LoggedinEmailAddress
        'get Dealer email address
        Me.DealerContactDetailsTableAdapter.FillByDealerEmail(EnquiryWorkSheetDataSet.DealerContactDetails, thisDealerId, "Email")
        For Each dr As DataRow In EnquiryWorkSheetDataSet.Tables("DealerContactDetails").Rows
            If dr.Item("ContactMethod") = Constants.CONST_CM_EMAIL Then
                cmbxEmailTo.Items.Add(dr.Item("ContactValue"))
            End If
        Next
        'get Dealer name
        Me.DealerContactDetailsTableAdapter.FillByDistinctDealerId(EnquiryWorkSheetDataSet.DealerContactDetails, thisDealerId)
        'get Dealer Name(Trading Name)
        thisDealerName = EnquiryWorkSheetDataSet.DealerContactDetails.Rows(DealerContactDetailsBS.Position()).Item("Name")
        'set default Dealer Name
        txtbxDealer.Text = thisDealerName
        'load data into the 'EnquiryWorkSheetDataSet' tables for this Enquiry Id
        Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
        Me.DueDiligenceTA.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
        'Me.PayoutTA.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Payout, thisEnquiryId)
        'get Application code
        thisApplicationCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBS.Position()).Item("ApplicationCode")
        thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBS.Position()).Item("ContactDisplayName")
        'set default Client name
        txtbxClient.Text = thisContactDisplayName
        'set default Email Subject
        txtbxEmailSubject.Text = "Yes Finance Dealer Remittance advice for " & thisApplicationCode & "; " & thisContactDisplayName
        'get html for sending email  and display in browser control (not editable)    
        WebBrowser1.DocumentText = GetMailBody(False, True)

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAttachments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttachments.Click
        'Add an attachment
        Dim thisFileName As String = ""
        ' Displays an OpenFileDialog so the user can select a file.
        Dim openFileDialog1 As New OpenFileDialog()
        openFileDialog1.Filter = "Text|*.txt|All|*.*"
        openFileDialog1.Title = "Select a Document File"
        'Show the Dialog.
        'If the user clicked OK in the dialog and a document file was selected, get the file path
        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            thisFileName = openFileDialog1.FileName
            'MsgBox(ThisFileName)
            fileAttch.Add(thisFileName)
            attachListString = ""
            For Each listFileName As String In fileAttch
                Dim finfo As New FileInfo(listFileName)
                If finfo.Exists Then
                    attachListString = attachListString & finfo.Name & "; "
                Else
                    MessageBox.Show("Can not find file " & finfo.Name, "Missing file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Next
        End If
        txtbxAttachmentsList.Text = attachListString
    End Sub


    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Cursor.Current = Cursors.WaitCursor
        Dim checkFieldsMessage As String = ""
        'check Dealer
        If txtbxDealer.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a Dealer!" + vbCrLf
        End If
        'check Client
        If txtbxClient.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a Client!" + vbCrLf
        End If
        'check email to field
        If Not cmbxEmailTo.Text.Length > 0 Then
            checkFieldsMessage = checkFieldsMessage + "Please enter address for 'email to'!" + vbCrLf
        ElseIf Not ValidateEmail(cmbxEmailTo.Text) Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a valid 'to' email address!" + vbCrLf
        End If
        'check email subject
        If txtbxEmailSubject.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a subject!" + vbCrLf
        End If
        'check email message
        If WebBrowser1.DocumentText = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please check remittance text" + vbCrLf
        End If
        'check for error messages
        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            Me.DialogResult = System.Windows.Forms.DialogResult.None
        Else
            'OK to go
            Dim mailBodyText As String = GetMailBody(True, True)
            If Not String.IsNullOrEmpty(mailBodyText) Then
                Try
                    'Send mail message
                    Dim thisMail As New MailMessage
                    thisMail.From = New MailAddress(txtbxEmailFrom.Text)
                    thisMail.To.Add(cmbxEmailTo.Text)
                    'set content
                    thisMail.Subject = txtbxEmailSubject.Text
                    thisMail.Body = mailBodyText
                    thisMail.IsBodyHtml = True
                    'add attachments
                    Dim attachment As System.Net.Mail.Attachment
                    For Each listFileName As String In fileAttch
                        Dim finfo As New FileInfo(listFileName)
                        If finfo.Exists Then
                            attachment = New Net.Mail.Attachment(listFileName)
                            thisMail.Attachments.Add(attachment)
                        Else
                            MessageBox.Show("Can not find file " & finfo.Name, "Missing file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            attachmentError = True
                        End If
                    Next

                    'send mail
                    Dim client As New SmtpClient(My.Settings.SmtpClient)
                    client.Port = My.Settings.SmtpClientPort

                    '*********************** adjust credentials
                    client.UseDefaultCredentials = True
                    'client.Credentials = New Net.NetworkCredential("csteel", "Art1s@n1")
                    '*********************** end of adjust credentials
                    If attachmentError = True Then
                        MessageBox.Show("Attachment error detected, sending email aborted", "Attachment Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Else
                        client.Send(thisMail)
                        Cursor.Current = Cursors.Default

                        '  Optional user reassurance:
                        MessageBox.Show(String.Format("Message Subject ' {0} ' successfully sent" & vbCrLf & "from {1}" & vbCrLf & "to {2}", thisMail.Subject, thisMail.From, thisMail.To.ToString), "EMail", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                        Cursor.Current = Cursors.WaitCursor
                        'add comments to LoanComments data
                        Dim commentStr As String
                        Dim theseAttachments As String
                        'Create Attachments string
                        If txtbxAttachmentsList.Text.Length > 0 Then
                            theseAttachments = "Attachments sent: " & txtbxAttachmentsList.Text
                        Else
                            theseAttachments = ""
                        End If
                        'Comments
                        commentStr = "Email sent to " & thisMail.To.ToString & vbCrLf & "Subject: " & _
                        thisMail.Subject & ": " & vbCrLf & "Dealer: " & txtbxDealer.Text & " - Client: " & txtbxClient.Text & vbCrLf & GetMailBody(False, False) & vbCrLf & vbCrLf & theseAttachments
                        'debug
                        'MsgBox(commentStr)
                        Dim commentId As Integer
                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

                        'Housekeeping
                        thisMail.Dispose()
                        Cursor.Current = Cursors.Default
                        'Close Form
                        Me.DialogResult = System.Windows.Forms.DialogResult.OK
                    End If



                Catch ex As FormatException
                    Dim inner As Exception = ex.InnerException
                    If Not (inner Is Nothing) Then
                        log.Error("Format Exception: " & inner.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Format Exception: " + vbCrLf + inner.Message)
                        inner = inner.InnerException
                    Else
                        log.Error("Format Exception: " & ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Format Exception: " + vbCrLf + ex.Message)
                    End If
                Catch ex As SmtpException
                    Dim inner As Exception = ex.InnerException
                    If Not (inner Is Nothing) Then
                        log.Error("SMTP Exception: " & inner.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("SMTP Exception: " + vbCrLf + inner.Message)
                        inner = inner.InnerException
                    Else
                        log.Error("SMTP Exception: " & ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("SMTP Exception: " + vbCrLf + ex.Message)
                    End If
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MessageBox.Show(ex.Message)
                End Try

            Else
                MessageBox.Show("No email body text to send", "Error generating Body Text", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End If

    End Sub

    Function GetMailBody(ByVal setHeader As Boolean, ByVal returnHtml As Boolean) As String
        Dim returnString As String
        Dim mailTemplate As String

        Try
            'setup variables, values from dueDiligence table
            Dim today As String = Date.Now.ToLongDateString '{0}
            Dim dealerName As String = thisDealerName '{1},{15},{17}
            Dim loanContractNumber As String = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("LoanNum") '{2}
            Dim contactDisplayName As String = thisContactDisplayName '{3}
            Dim cashPrice As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("CashPrice_Advance")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("CashPrice_Advance")) '{4}
            Dim additionAdvances As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("OtherAdvance")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("OtherAdvance")) '{7} 
            Dim deposit As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("Deposit")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("Deposit")) '{5}
            Dim tradein As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("Tradein")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("TradeIn")) '{26}
            '====================================================
            Dim brokerage As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("Brokerage")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("Brokerage")) '{22} brokerage
            Dim totalAdvance As Decimal = cashPrice + additionAdvances - deposit - tradein '{6}
            Dim productMargin As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("ProductCommission")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("ProductCommission"))  '{8}
            Dim interestCommission As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("InterestCommission")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("InterestCommission"))  '{9}
            Dim brokeProdMarginIntComm As Decimal = brokerage + productMargin + interestCommission '{10}
            '====================================================
            Dim totalPay As Decimal = totalAdvance + brokeProdMarginIntComm '{11},{19}
            Dim loanRetention As Decimal = (totalAdvance + brokerage) * If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("LoanRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("LoanRetention")) / 100 '{12}
            Dim loanRetentionRate As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("LoanRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("LoanRetention"))
            Dim commRetentionRate As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("CommRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("CommRetention"))
            Dim commRetention As Decimal = (productMargin + interestCommission) * If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("CommRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBS.Position()).Item("CommRetention")) / 100 '{13}
            Dim totalRetention As Decimal = loanRetention + commRetention '{14},{18}
            '====================================================
            Dim paidToBankAccount As Decimal = totalAdvance + brokerage - loanRetention '{16}
            Dim paidToCommAccount As Decimal = productMargin + interestCommission - commRetention '{23}


            'Construct body, get template file
            If returnHtml = True Then
                mailTemplate = My.Resources.EmailDealerRemittance
                'substitute the values
                returnString = String.Format(mailTemplate, today, dealerName, loanContractNumber, contactDisplayName, cashPrice, deposit, totalAdvance, additionAdvances, productMargin, interestCommission, brokeProdMarginIntComm, totalPay, loanRetention, commRetention, totalRetention, dealerName, paidToBankAccount, dealerName, totalRetention, totalPay, Util.GetSignature(True), If(setHeader = True, "450", "95%"), brokerage, paidToCommAccount, loanRetentionRate, commRetentionRate, tradein)

            Else
                mailTemplate = My.Resources.DealerRemittance
                'substitute the values
                returnString = String.Format(mailTemplate, today, dealerName, loanContractNumber, contactDisplayName, cashPrice, deposit, totalAdvance, additionAdvances, productMargin, interestCommission, brokeProdMarginIntComm, totalPay, loanRetention, commRetention, totalRetention, dealerName, paidToBankAccount, dealerName, totalRetention, totalPay, brokerage, paidToCommAccount, loanRetentionRate, commRetentionRate, tradein)
            End If


        Catch ex As Exception
            MsgBox("Error generating mail body:" & vbCrLf & ex.Message)
            returnString = String.Empty
            Return returnString
        End Try

        Return returnString

    End Function



    Private Sub EmailDealerRemittanceForm_Layout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'resize
        txtbxDealer.Size = New Size(formWidth - 114, 20)
        txtbxClient.Size = New Size(formWidth - 114, 20)
        cmbxEmailTo.Size = New Size(formWidth - 114, 20)
        txtbxEmailFrom.Size = New Size(formWidth - 114, 20)
        txtbxEmailSubject.Size = New Size(formWidth - 114, 20)
        WebBrowser1.Size = New Size(formWidth - 40, formHeight - 273)
        lblAttachments.Location = New Point(15, WebBrowser1.Location.Y + WebBrowser1.Size.Height + 6)
        txtbxAttachmentsList.Location = New Point(82, WebBrowser1.Location.Y + WebBrowser1.Size.Height + 6)
        txtbxAttachmentsList.Size = New Size(formWidth - 145, 20)
        btnAttachments.Location = New Point(txtbxAttachmentsList.Location.X + txtbxAttachmentsList.Size.Width + 6, WebBrowser1.Location.Y + WebBrowser1.Size.Height + 6)
        btnCancel.Location = New Point(82, WebBrowser1.Location.Y + WebBrowser1.Size.Height + 26 + 15)
        btnSend.Location = New Point(formWidth - 107, WebBrowser1.Location.Y + WebBrowser1.Size.Height + 26 + 15)

    End Sub
End Class