﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ActiveLabel As System.Windows.Forms.Label
        Dim PasswordLabel As System.Windows.Forms.Label
        Dim UserNameLabel As System.Windows.Forms.Label
        Dim UserIdLabel As System.Windows.Forms.Label
        Dim RoleLabel1 As System.Windows.Forms.Label
        Dim BranchIdLabel As System.Windows.Forms.Label
        Dim BranchIdLabel1 As System.Windows.Forms.Label
        Dim FullNameLabel As System.Windows.Forms.Label
        Dim AddressLabel As System.Windows.Forms.Label
        Dim CityLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AdminForm))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbLists = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpEnquiryMethodList = New System.Windows.Forms.TabPage()
        Me.bnEnquiryMethodList = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem1 = New System.Windows.Forms.ToolStripButton()
        Me.EnquiryMethodListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.BindingNavigatorCountItem1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem1 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSaveEnquiryMethod = New System.Windows.Forms.ToolStripButton()
        Me.EnquiryMethodListDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpPrelimSource = New System.Windows.Forms.TabPage()
        Me.bnPrelimSource = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem3 = New System.Windows.Forms.ToolStripButton()
        Me.PrelimSourceBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem3 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem3 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem3 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem3 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem3 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem3 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem3 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSavePrelimSource = New System.Windows.Forms.ToolStripButton()
        Me.PrelimSourceDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpPrelimResults = New System.Windows.Forms.TabPage()
        Me.bnPrelimResults = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem5 = New System.Windows.Forms.ToolStripButton()
        Me.PrelimResultsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem5 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem5 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem5 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem5 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator15 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem5 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator16 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem5 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem5 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator17 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSavePrelimResults = New System.Windows.Forms.ToolStripButton()
        Me.PrelimResultsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.bnTenancy = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem10 = New System.Windows.Forms.ToolStripButton()
        Me.TypeOfTenancyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem10 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem10 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem10 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem10 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator30 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem10 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator31 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem10 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem10 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator32 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.TypeOfTenancyDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TenancyId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.bnAppSettings = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.AppSettingsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSaveAppValues = New System.Windows.Forms.ToolStripButton()
        Me.AppSettingsDataGridView = New System.Windows.Forms.DataGridView()
        Me.tpLatencyPoints = New System.Windows.Forms.TabPage()
        Me.dgvLatencyPoints = New System.Windows.Forms.DataGridView()
        Me.LogTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.SubjectDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NotesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SortNum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LatencyPointsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.bnLatencyPoints = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbtnLatencyPoints = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbUsers = New System.Windows.Forms.GroupBox()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.tpUsers = New System.Windows.Forms.TabPage()
        Me.lblBranchName = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblFullName = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtbxEmail = New System.Windows.Forms.TextBox()
        Me.lblUserIdValue = New System.Windows.Forms.Label()
        Me.bnUsers = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem2 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem2 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSaveUsers = New System.Windows.Forms.ToolStripButton()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.UserNameTextBox = New System.Windows.Forms.TextBox()
        Me.BranchIdComboBox = New System.Windows.Forms.ComboBox()
        Me.BranchesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.RoleComboBox = New System.Windows.Forms.ComboBox()
        Me.UserRolesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tpBranches = New System.Windows.Forms.TabPage()
        Me.bnBranches = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem6 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem6 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem6 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem6 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem6 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator18 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem6 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator19 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem6 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem6 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator20 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSaveBranches = New System.Windows.Forms.ToolStripButton()
        Me.BranchIdTextBox = New System.Windows.Forms.TextBox()
        Me.FullNameTextBox = New System.Windows.Forms.TextBox()
        Me.AddressTextBox = New System.Windows.Forms.TextBox()
        Me.CityTextBox = New System.Windows.Forms.TextBox()
        Me.tpRoles = New System.Windows.Forms.TabPage()
        Me.bnUserRoles = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem7 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem7 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem7 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem7 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem7 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator21 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem7 = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator22 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem7 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem7 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator23 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnSaveUserRoles = New System.Windows.Forms.ToolStripButton()
        Me.UserRolesDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.EnquiryMethodListTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryMethodListTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.PrelimSourceTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimSourceTableAdapter()
        Me.PrelimResultsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter()
        Me.BranchesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BranchesTableAdapter()
        Me.UserRolesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UserRolesTableAdapter()
        Me.TypeOfTenancyTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfTenancyTableAdapter()
        Me.LatencyPointsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LatencyPointsTableAdapter()
        Me.AppSettingsTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.AppSettingsTableAdapter()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DecimalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BooleanDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CommentsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        ActiveLabel = New System.Windows.Forms.Label()
        PasswordLabel = New System.Windows.Forms.Label()
        UserNameLabel = New System.Windows.Forms.Label()
        UserIdLabel = New System.Windows.Forms.Label()
        RoleLabel1 = New System.Windows.Forms.Label()
        BranchIdLabel = New System.Windows.Forms.Label()
        BranchIdLabel1 = New System.Windows.Forms.Label()
        FullNameLabel = New System.Windows.Forms.Label()
        AddressLabel = New System.Windows.Forms.Label()
        CityLabel = New System.Windows.Forms.Label()
        Me.gbLists.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpEnquiryMethodList.SuspendLayout()
        CType(Me.bnEnquiryMethodList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnEnquiryMethodList.SuspendLayout()
        CType(Me.EnquiryMethodListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryMethodListDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPrelimSource.SuspendLayout()
        CType(Me.bnPrelimSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPrelimSource.SuspendLayout()
        CType(Me.PrelimSourceBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimSourceDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPrelimResults.SuspendLayout()
        CType(Me.bnPrelimResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPrelimResults.SuspendLayout()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimResultsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.bnTenancy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTenancy.SuspendLayout()
        CType(Me.TypeOfTenancyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeOfTenancyDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.bnAppSettings, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnAppSettings.SuspendLayout()
        CType(Me.AppSettingsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AppSettingsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpLatencyPoints.SuspendLayout()
        CType(Me.dgvLatencyPoints, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LatencyPointsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnLatencyPoints, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnLatencyPoints.SuspendLayout()
        Me.gbUsers.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.tpUsers.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnUsers.SuspendLayout()
        CType(Me.BranchesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UserRolesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpBranches.SuspendLayout()
        CType(Me.bnBranches, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnBranches.SuspendLayout()
        Me.tpRoles.SuspendLayout()
        CType(Me.bnUserRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnUserRoles.SuspendLayout()
        CType(Me.UserRolesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'ActiveLabel
        '
        ActiveLabel.AutoSize = True
        ActiveLabel.Location = New System.Drawing.Point(6, 122)
        ActiveLabel.Name = "ActiveLabel"
        ActiveLabel.Size = New System.Drawing.Size(43, 13)
        ActiveLabel.TabIndex = 10
        ActiveLabel.Text = "Active?"
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Location = New System.Drawing.Point(8, 68)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(56, 13)
        PasswordLabel.TabIndex = 4
        PasswordLabel.Text = "Password:"
        '
        'UserNameLabel
        '
        UserNameLabel.AutoSize = True
        UserNameLabel.Location = New System.Drawing.Point(333, 70)
        UserNameLabel.Name = "UserNameLabel"
        UserNameLabel.Size = New System.Drawing.Size(63, 13)
        UserNameLabel.TabIndex = 2
        UserNameLabel.Text = "User Name:"
        '
        'UserIdLabel
        '
        UserIdLabel.AutoSize = True
        UserIdLabel.Location = New System.Drawing.Point(8, 42)
        UserIdLabel.Name = "UserIdLabel"
        UserIdLabel.Size = New System.Drawing.Size(44, 13)
        UserIdLabel.TabIndex = 0
        UserIdLabel.Text = "User Id:"
        '
        'RoleLabel1
        '
        RoleLabel1.AutoSize = True
        RoleLabel1.Location = New System.Drawing.Point(333, 96)
        RoleLabel1.Name = "RoleLabel1"
        RoleLabel1.Size = New System.Drawing.Size(32, 13)
        RoleLabel1.TabIndex = 12
        RoleLabel1.Text = "Role:"
        '
        'BranchIdLabel
        '
        BranchIdLabel.AutoSize = True
        BranchIdLabel.Location = New System.Drawing.Point(8, 94)
        BranchIdLabel.Name = "BranchIdLabel"
        BranchIdLabel.Size = New System.Drawing.Size(56, 13)
        BranchIdLabel.TabIndex = 13
        BranchIdLabel.Text = "Branch Id:"
        '
        'BranchIdLabel1
        '
        BranchIdLabel1.AutoSize = True
        BranchIdLabel1.Location = New System.Drawing.Point(11, 53)
        BranchIdLabel1.Name = "BranchIdLabel1"
        BranchIdLabel1.Size = New System.Drawing.Size(56, 13)
        BranchIdLabel1.TabIndex = 0
        BranchIdLabel1.Text = "Branch Id:"
        '
        'FullNameLabel
        '
        FullNameLabel.AutoSize = True
        FullNameLabel.Location = New System.Drawing.Point(332, 51)
        FullNameLabel.Name = "FullNameLabel"
        FullNameLabel.Size = New System.Drawing.Size(57, 13)
        FullNameLabel.TabIndex = 2
        FullNameLabel.Text = "Full Name:"
        '
        'AddressLabel
        '
        AddressLabel.AutoSize = True
        AddressLabel.Location = New System.Drawing.Point(11, 82)
        AddressLabel.Name = "AddressLabel"
        AddressLabel.Size = New System.Drawing.Size(48, 13)
        AddressLabel.TabIndex = 4
        AddressLabel.Text = "Address:"
        '
        'CityLabel
        '
        CityLabel.AutoSize = True
        CityLabel.Location = New System.Drawing.Point(332, 80)
        CityLabel.Name = "CityLabel"
        CityLabel.Size = New System.Drawing.Size(27, 13)
        CityLabel.TabIndex = 6
        CityLabel.Text = "City:"
        '
        'gbLists
        '
        Me.gbLists.Controls.Add(Me.TabControl1)
        Me.gbLists.Location = New System.Drawing.Point(12, 32)
        Me.gbLists.Name = "gbLists"
        Me.gbLists.Size = New System.Drawing.Size(680, 364)
        Me.gbLists.TabIndex = 0
        Me.gbLists.TabStop = False
        Me.gbLists.Text = "Lists"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpEnquiryMethodList)
        Me.TabControl1.Controls.Add(Me.tpPrelimSource)
        Me.TabControl1.Controls.Add(Me.tpPrelimResults)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.tpLatencyPoints)
        Me.TabControl1.Location = New System.Drawing.Point(6, 19)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(668, 339)
        Me.TabControl1.TabIndex = 0
        '
        'tpEnquiryMethodList
        '
        Me.tpEnquiryMethodList.AutoScroll = True
        Me.tpEnquiryMethodList.Controls.Add(Me.bnEnquiryMethodList)
        Me.tpEnquiryMethodList.Controls.Add(Me.EnquiryMethodListDataGridView)
        Me.tpEnquiryMethodList.Location = New System.Drawing.Point(4, 22)
        Me.tpEnquiryMethodList.Name = "tpEnquiryMethodList"
        Me.tpEnquiryMethodList.Padding = New System.Windows.Forms.Padding(3)
        Me.tpEnquiryMethodList.Size = New System.Drawing.Size(660, 313)
        Me.tpEnquiryMethodList.TabIndex = 0
        Me.tpEnquiryMethodList.Text = "Enquiry Method List"
        Me.tpEnquiryMethodList.UseVisualStyleBackColor = True
        '
        'bnEnquiryMethodList
        '
        Me.bnEnquiryMethodList.AddNewItem = Me.BindingNavigatorAddNewItem1
        Me.bnEnquiryMethodList.BindingSource = Me.EnquiryMethodListBindingSource
        Me.bnEnquiryMethodList.CountItem = Me.BindingNavigatorCountItem1
        Me.bnEnquiryMethodList.DeleteItem = Me.BindingNavigatorDeleteItem1
        Me.bnEnquiryMethodList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem1, Me.BindingNavigatorMovePreviousItem1, Me.BindingNavigatorSeparator3, Me.BindingNavigatorPositionItem1, Me.BindingNavigatorCountItem1, Me.BindingNavigatorSeparator4, Me.BindingNavigatorMoveNextItem1, Me.BindingNavigatorMoveLastItem1, Me.BindingNavigatorSeparator5, Me.BindingNavigatorAddNewItem1, Me.BindingNavigatorDeleteItem1, Me.btnSaveEnquiryMethod})
        Me.bnEnquiryMethodList.Location = New System.Drawing.Point(3, 3)
        Me.bnEnquiryMethodList.MoveFirstItem = Me.BindingNavigatorMoveFirstItem1
        Me.bnEnquiryMethodList.MoveLastItem = Me.BindingNavigatorMoveLastItem1
        Me.bnEnquiryMethodList.MoveNextItem = Me.BindingNavigatorMoveNextItem1
        Me.bnEnquiryMethodList.MovePreviousItem = Me.BindingNavigatorMovePreviousItem1
        Me.bnEnquiryMethodList.Name = "bnEnquiryMethodList"
        Me.bnEnquiryMethodList.PositionItem = Me.BindingNavigatorPositionItem1
        Me.bnEnquiryMethodList.Size = New System.Drawing.Size(654, 25)
        Me.bnEnquiryMethodList.TabIndex = 1
        Me.bnEnquiryMethodList.Text = "BindingNavigator Enquiry Method List"
        '
        'BindingNavigatorAddNewItem1
        '
        Me.BindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem1.Image = CType(resources.GetObject("BindingNavigatorAddNewItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem1.Name = "BindingNavigatorAddNewItem1"
        Me.BindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem1.Text = "Add new"
        '
        'EnquiryMethodListBindingSource
        '
        Me.EnquiryMethodListBindingSource.DataMember = "EnquiryMethodList"
        Me.EnquiryMethodListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem1
        '
        Me.BindingNavigatorCountItem1.Name = "BindingNavigatorCountItem1"
        Me.BindingNavigatorCountItem1.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem1.Text = "of {0}"
        Me.BindingNavigatorCountItem1.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem1
        '
        Me.BindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem1.Image = CType(resources.GetObject("BindingNavigatorDeleteItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem1.Name = "BindingNavigatorDeleteItem1"
        Me.BindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem1.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem1
        '
        Me.BindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem1.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem1.Name = "BindingNavigatorMoveFirstItem1"
        Me.BindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem1.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem1
        '
        Me.BindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem1.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem1.Name = "BindingNavigatorMovePreviousItem1"
        Me.BindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem1.Text = "Move previous"
        '
        'BindingNavigatorSeparator3
        '
        Me.BindingNavigatorSeparator3.Name = "BindingNavigatorSeparator3"
        Me.BindingNavigatorSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem1
        '
        Me.BindingNavigatorPositionItem1.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem1.AutoSize = False
        Me.BindingNavigatorPositionItem1.Name = "BindingNavigatorPositionItem1"
        Me.BindingNavigatorPositionItem1.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem1.Text = "0"
        Me.BindingNavigatorPositionItem1.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator4
        '
        Me.BindingNavigatorSeparator4.Name = "BindingNavigatorSeparator4"
        Me.BindingNavigatorSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem1
        '
        Me.BindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem1.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem1.Name = "BindingNavigatorMoveNextItem1"
        Me.BindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem1.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem1
        '
        Me.BindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem1.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem1.Name = "BindingNavigatorMoveLastItem1"
        Me.BindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem1.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem1.Text = "Move last"
        '
        'BindingNavigatorSeparator5
        '
        Me.BindingNavigatorSeparator5.Name = "BindingNavigatorSeparator5"
        Me.BindingNavigatorSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'btnSaveEnquiryMethod
        '
        Me.btnSaveEnquiryMethod.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSaveEnquiryMethod.Image = CType(resources.GetObject("btnSaveEnquiryMethod.Image"), System.Drawing.Image)
        Me.btnSaveEnquiryMethod.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSaveEnquiryMethod.Name = "btnSaveEnquiryMethod"
        Me.btnSaveEnquiryMethod.Size = New System.Drawing.Size(23, 22)
        Me.btnSaveEnquiryMethod.Text = "&Save"
        '
        'EnquiryMethodListDataGridView
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        Me.EnquiryMethodListDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.EnquiryMethodListDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EnquiryMethodListDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.EnquiryMethodListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.EnquiryMethodListDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.EnquiryMethodListDataGridView.DataSource = Me.EnquiryMethodListBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.EnquiryMethodListDataGridView.DefaultCellStyle = DataGridViewCellStyle3
        Me.EnquiryMethodListDataGridView.Location = New System.Drawing.Point(0, 31)
        Me.EnquiryMethodListDataGridView.MultiSelect = False
        Me.EnquiryMethodListDataGridView.Name = "EnquiryMethodListDataGridView"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EnquiryMethodListDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.EnquiryMethodListDataGridView.Size = New System.Drawing.Size(660, 282)
        Me.EnquiryMethodListDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "EnquiryMethodId"
        Me.DataGridViewTextBoxColumn1.HeaderText = "EnquiryMethodId"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "EnquiryMethod"
        Me.DataGridViewTextBoxColumn2.HeaderText = "EnquiryMethod"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 615
        '
        'tpPrelimSource
        '
        Me.tpPrelimSource.AutoScroll = True
        Me.tpPrelimSource.Controls.Add(Me.bnPrelimSource)
        Me.tpPrelimSource.Controls.Add(Me.PrelimSourceDataGridView)
        Me.tpPrelimSource.Location = New System.Drawing.Point(4, 22)
        Me.tpPrelimSource.Name = "tpPrelimSource"
        Me.tpPrelimSource.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPrelimSource.Size = New System.Drawing.Size(660, 313)
        Me.tpPrelimSource.TabIndex = 1
        Me.tpPrelimSource.Text = "Preliminary Source"
        Me.tpPrelimSource.UseVisualStyleBackColor = True
        '
        'bnPrelimSource
        '
        Me.bnPrelimSource.AddNewItem = Me.BindingNavigatorAddNewItem3
        Me.bnPrelimSource.BindingSource = Me.PrelimSourceBindingSource
        Me.bnPrelimSource.CountItem = Me.BindingNavigatorCountItem3
        Me.bnPrelimSource.DeleteItem = Me.BindingNavigatorDeleteItem3
        Me.bnPrelimSource.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem3, Me.BindingNavigatorMovePreviousItem3, Me.BindingNavigatorSeparator9, Me.BindingNavigatorPositionItem3, Me.BindingNavigatorCountItem3, Me.BindingNavigatorSeparator10, Me.BindingNavigatorMoveNextItem3, Me.BindingNavigatorMoveLastItem3, Me.BindingNavigatorSeparator11, Me.BindingNavigatorAddNewItem3, Me.BindingNavigatorDeleteItem3, Me.btnSavePrelimSource})
        Me.bnPrelimSource.Location = New System.Drawing.Point(3, 3)
        Me.bnPrelimSource.MoveFirstItem = Me.BindingNavigatorMoveFirstItem3
        Me.bnPrelimSource.MoveLastItem = Me.BindingNavigatorMoveLastItem3
        Me.bnPrelimSource.MoveNextItem = Me.BindingNavigatorMoveNextItem3
        Me.bnPrelimSource.MovePreviousItem = Me.BindingNavigatorMovePreviousItem3
        Me.bnPrelimSource.Name = "bnPrelimSource"
        Me.bnPrelimSource.PositionItem = Me.BindingNavigatorPositionItem3
        Me.bnPrelimSource.Size = New System.Drawing.Size(654, 25)
        Me.bnPrelimSource.TabIndex = 3
        Me.bnPrelimSource.Text = "BindingNavigator Preliminary Source"
        '
        'BindingNavigatorAddNewItem3
        '
        Me.BindingNavigatorAddNewItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem3.Image = CType(resources.GetObject("BindingNavigatorAddNewItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem3.Name = "BindingNavigatorAddNewItem3"
        Me.BindingNavigatorAddNewItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem3.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem3.Text = "Add new"
        '
        'PrelimSourceBindingSource
        '
        Me.PrelimSourceBindingSource.DataMember = "PrelimSource"
        Me.PrelimSourceBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'BindingNavigatorCountItem3
        '
        Me.BindingNavigatorCountItem3.Name = "BindingNavigatorCountItem3"
        Me.BindingNavigatorCountItem3.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem3.Text = "of {0}"
        Me.BindingNavigatorCountItem3.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem3
        '
        Me.BindingNavigatorDeleteItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem3.Image = CType(resources.GetObject("BindingNavigatorDeleteItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem3.Name = "BindingNavigatorDeleteItem3"
        Me.BindingNavigatorDeleteItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem3.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem3.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem3
        '
        Me.BindingNavigatorMoveFirstItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem3.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem3.Name = "BindingNavigatorMoveFirstItem3"
        Me.BindingNavigatorMoveFirstItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem3.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem3.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem3
        '
        Me.BindingNavigatorMovePreviousItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem3.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem3.Name = "BindingNavigatorMovePreviousItem3"
        Me.BindingNavigatorMovePreviousItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem3.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem3.Text = "Move previous"
        '
        'BindingNavigatorSeparator9
        '
        Me.BindingNavigatorSeparator9.Name = "BindingNavigatorSeparator9"
        Me.BindingNavigatorSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem3
        '
        Me.BindingNavigatorPositionItem3.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem3.AutoSize = False
        Me.BindingNavigatorPositionItem3.Name = "BindingNavigatorPositionItem3"
        Me.BindingNavigatorPositionItem3.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem3.Text = "0"
        Me.BindingNavigatorPositionItem3.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator10
        '
        Me.BindingNavigatorSeparator10.Name = "BindingNavigatorSeparator10"
        Me.BindingNavigatorSeparator10.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem3
        '
        Me.BindingNavigatorMoveNextItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem3.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem3.Name = "BindingNavigatorMoveNextItem3"
        Me.BindingNavigatorMoveNextItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem3.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem3.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem3
        '
        Me.BindingNavigatorMoveLastItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem3.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem3.Name = "BindingNavigatorMoveLastItem3"
        Me.BindingNavigatorMoveLastItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem3.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem3.Text = "Move last"
        '
        'BindingNavigatorSeparator11
        '
        Me.BindingNavigatorSeparator11.Name = "BindingNavigatorSeparator11"
        Me.BindingNavigatorSeparator11.Size = New System.Drawing.Size(6, 25)
        '
        'btnSavePrelimSource
        '
        Me.btnSavePrelimSource.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSavePrelimSource.Image = CType(resources.GetObject("btnSavePrelimSource.Image"), System.Drawing.Image)
        Me.btnSavePrelimSource.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSavePrelimSource.Name = "btnSavePrelimSource"
        Me.btnSavePrelimSource.Size = New System.Drawing.Size(23, 22)
        Me.btnSavePrelimSource.Text = "&Save"
        '
        'PrelimSourceDataGridView
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        Me.PrelimSourceDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.PrelimSourceDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PrelimSourceDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.PrelimSourceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PrelimSourceDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.PrelimSourceDataGridView.DataSource = Me.PrelimSourceBindingSource
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.PrelimSourceDataGridView.DefaultCellStyle = DataGridViewCellStyle7
        Me.PrelimSourceDataGridView.Location = New System.Drawing.Point(0, 31)
        Me.PrelimSourceDataGridView.MultiSelect = False
        Me.PrelimSourceDataGridView.Name = "PrelimSourceDataGridView"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PrelimSourceDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.PrelimSourceDataGridView.Size = New System.Drawing.Size(657, 282)
        Me.PrelimSourceDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "PrelimSourceId"
        Me.DataGridViewTextBoxColumn3.HeaderText = "PrelimSourceId"
        Me.DataGridViewTextBoxColumn3.MinimumWidth = 2
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 2
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "PrelimSource"
        Me.DataGridViewTextBoxColumn4.HeaderText = "PrelimSource"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 608
        '
        'tpPrelimResults
        '
        Me.tpPrelimResults.AutoScroll = True
        Me.tpPrelimResults.Controls.Add(Me.bnPrelimResults)
        Me.tpPrelimResults.Controls.Add(Me.PrelimResultsDataGridView)
        Me.tpPrelimResults.Location = New System.Drawing.Point(4, 22)
        Me.tpPrelimResults.Name = "tpPrelimResults"
        Me.tpPrelimResults.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPrelimResults.Size = New System.Drawing.Size(660, 313)
        Me.tpPrelimResults.TabIndex = 3
        Me.tpPrelimResults.Text = "Preliminary Results"
        Me.tpPrelimResults.UseVisualStyleBackColor = True
        '
        'bnPrelimResults
        '
        Me.bnPrelimResults.AddNewItem = Me.BindingNavigatorAddNewItem5
        Me.bnPrelimResults.BindingSource = Me.PrelimResultsBindingSource
        Me.bnPrelimResults.CountItem = Me.BindingNavigatorCountItem5
        Me.bnPrelimResults.DeleteItem = Me.BindingNavigatorDeleteItem5
        Me.bnPrelimResults.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem5, Me.BindingNavigatorMovePreviousItem5, Me.BindingNavigatorSeparator15, Me.BindingNavigatorPositionItem5, Me.BindingNavigatorCountItem5, Me.BindingNavigatorSeparator16, Me.BindingNavigatorMoveNextItem5, Me.BindingNavigatorMoveLastItem5, Me.BindingNavigatorSeparator17, Me.BindingNavigatorAddNewItem5, Me.BindingNavigatorDeleteItem5, Me.btnSavePrelimResults})
        Me.bnPrelimResults.Location = New System.Drawing.Point(3, 3)
        Me.bnPrelimResults.MoveFirstItem = Me.BindingNavigatorMoveFirstItem5
        Me.bnPrelimResults.MoveLastItem = Me.BindingNavigatorMoveLastItem5
        Me.bnPrelimResults.MoveNextItem = Me.BindingNavigatorMoveNextItem5
        Me.bnPrelimResults.MovePreviousItem = Me.BindingNavigatorMovePreviousItem5
        Me.bnPrelimResults.Name = "bnPrelimResults"
        Me.bnPrelimResults.PositionItem = Me.BindingNavigatorPositionItem5
        Me.bnPrelimResults.Size = New System.Drawing.Size(654, 25)
        Me.bnPrelimResults.TabIndex = 1
        Me.bnPrelimResults.Text = "BindingNavigator Preliminary Results"
        '
        'BindingNavigatorAddNewItem5
        '
        Me.BindingNavigatorAddNewItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem5.Image = CType(resources.GetObject("BindingNavigatorAddNewItem5.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem5.Name = "BindingNavigatorAddNewItem5"
        Me.BindingNavigatorAddNewItem5.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem5.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem5.Text = "Add new"
        '
        'PrelimResultsBindingSource
        '
        Me.PrelimResultsBindingSource.DataMember = "PrelimResults"
        Me.PrelimResultsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'BindingNavigatorCountItem5
        '
        Me.BindingNavigatorCountItem5.Name = "BindingNavigatorCountItem5"
        Me.BindingNavigatorCountItem5.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem5.Text = "of {0}"
        Me.BindingNavigatorCountItem5.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem5
        '
        Me.BindingNavigatorDeleteItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem5.Image = CType(resources.GetObject("BindingNavigatorDeleteItem5.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem5.Name = "BindingNavigatorDeleteItem5"
        Me.BindingNavigatorDeleteItem5.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem5.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem5.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem5
        '
        Me.BindingNavigatorMoveFirstItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem5.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem5.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem5.Name = "BindingNavigatorMoveFirstItem5"
        Me.BindingNavigatorMoveFirstItem5.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem5.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem5.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem5
        '
        Me.BindingNavigatorMovePreviousItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem5.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem5.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem5.Name = "BindingNavigatorMovePreviousItem5"
        Me.BindingNavigatorMovePreviousItem5.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem5.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem5.Text = "Move previous"
        '
        'BindingNavigatorSeparator15
        '
        Me.BindingNavigatorSeparator15.Name = "BindingNavigatorSeparator15"
        Me.BindingNavigatorSeparator15.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem5
        '
        Me.BindingNavigatorPositionItem5.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem5.AutoSize = False
        Me.BindingNavigatorPositionItem5.Name = "BindingNavigatorPositionItem5"
        Me.BindingNavigatorPositionItem5.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem5.Text = "0"
        Me.BindingNavigatorPositionItem5.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator16
        '
        Me.BindingNavigatorSeparator16.Name = "BindingNavigatorSeparator16"
        Me.BindingNavigatorSeparator16.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem5
        '
        Me.BindingNavigatorMoveNextItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem5.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem5.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem5.Name = "BindingNavigatorMoveNextItem5"
        Me.BindingNavigatorMoveNextItem5.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem5.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem5.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem5
        '
        Me.BindingNavigatorMoveLastItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem5.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem5.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem5.Name = "BindingNavigatorMoveLastItem5"
        Me.BindingNavigatorMoveLastItem5.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem5.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem5.Text = "Move last"
        '
        'BindingNavigatorSeparator17
        '
        Me.BindingNavigatorSeparator17.Name = "BindingNavigatorSeparator17"
        Me.BindingNavigatorSeparator17.Size = New System.Drawing.Size(6, 25)
        '
        'btnSavePrelimResults
        '
        Me.btnSavePrelimResults.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSavePrelimResults.Image = CType(resources.GetObject("btnSavePrelimResults.Image"), System.Drawing.Image)
        Me.btnSavePrelimResults.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSavePrelimResults.Name = "btnSavePrelimResults"
        Me.btnSavePrelimResults.Size = New System.Drawing.Size(23, 22)
        Me.btnSavePrelimResults.Text = "&Save"
        '
        'PrelimResultsDataGridView
        '
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        Me.PrelimResultsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.PrelimResultsDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PrelimResultsDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.PrelimResultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PrelimResultsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.PrelimResultsDataGridView.DataSource = Me.PrelimResultsBindingSource
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.PrelimResultsDataGridView.DefaultCellStyle = DataGridViewCellStyle11
        Me.PrelimResultsDataGridView.Location = New System.Drawing.Point(0, 31)
        Me.PrelimResultsDataGridView.MultiSelect = False
        Me.PrelimResultsDataGridView.Name = "PrelimResultsDataGridView"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PrelimResultsDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.PrelimResultsDataGridView.Size = New System.Drawing.Size(660, 282)
        Me.PrelimResultsDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "PrelimResultId"
        Me.DataGridViewTextBoxColumn7.HeaderText = "PrelimResultId"
        Me.DataGridViewTextBoxColumn7.MinimumWidth = 2
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        Me.DataGridViewTextBoxColumn7.Width = 2
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "PrelimResult"
        Me.DataGridViewTextBoxColumn8.HeaderText = "PrelimResult"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 615
        '
        'TabPage1
        '
        Me.TabPage1.AutoScroll = True
        Me.TabPage1.Controls.Add(Me.bnTenancy)
        Me.TabPage1.Controls.Add(Me.TypeOfTenancyDataGridView)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(660, 313)
        Me.TabPage1.TabIndex = 7
        Me.TabPage1.Text = "Tenancy"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'bnTenancy
        '
        Me.bnTenancy.AddNewItem = Me.BindingNavigatorAddNewItem10
        Me.bnTenancy.BindingSource = Me.TypeOfTenancyBindingSource
        Me.bnTenancy.CountItem = Me.BindingNavigatorCountItem10
        Me.bnTenancy.DeleteItem = Me.BindingNavigatorDeleteItem10
        Me.bnTenancy.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem10, Me.BindingNavigatorMovePreviousItem10, Me.BindingNavigatorSeparator30, Me.BindingNavigatorPositionItem10, Me.BindingNavigatorCountItem10, Me.BindingNavigatorSeparator31, Me.BindingNavigatorMoveNextItem10, Me.BindingNavigatorMoveLastItem10, Me.BindingNavigatorSeparator32, Me.BindingNavigatorAddNewItem10, Me.BindingNavigatorDeleteItem10, Me.SaveToolStripButton})
        Me.bnTenancy.Location = New System.Drawing.Point(3, 3)
        Me.bnTenancy.MoveFirstItem = Me.BindingNavigatorMoveFirstItem10
        Me.bnTenancy.MoveLastItem = Me.BindingNavigatorMoveLastItem10
        Me.bnTenancy.MoveNextItem = Me.BindingNavigatorMoveNextItem10
        Me.bnTenancy.MovePreviousItem = Me.BindingNavigatorMovePreviousItem10
        Me.bnTenancy.Name = "bnTenancy"
        Me.bnTenancy.PositionItem = Me.BindingNavigatorPositionItem10
        Me.bnTenancy.Size = New System.Drawing.Size(654, 25)
        Me.bnTenancy.TabIndex = 1
        Me.bnTenancy.Text = "BindingNavigatorTenancy"
        '
        'BindingNavigatorAddNewItem10
        '
        Me.BindingNavigatorAddNewItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem10.Image = CType(resources.GetObject("BindingNavigatorAddNewItem10.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem10.Name = "BindingNavigatorAddNewItem10"
        Me.BindingNavigatorAddNewItem10.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem10.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem10.Text = "Add new"
        '
        'TypeOfTenancyBindingSource
        '
        Me.TypeOfTenancyBindingSource.DataMember = "TypeOfTenancy"
        Me.TypeOfTenancyBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'BindingNavigatorCountItem10
        '
        Me.BindingNavigatorCountItem10.Name = "BindingNavigatorCountItem10"
        Me.BindingNavigatorCountItem10.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem10.Text = "of {0}"
        Me.BindingNavigatorCountItem10.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem10
        '
        Me.BindingNavigatorDeleteItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem10.Image = CType(resources.GetObject("BindingNavigatorDeleteItem10.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem10.Name = "BindingNavigatorDeleteItem10"
        Me.BindingNavigatorDeleteItem10.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem10.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem10.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem10
        '
        Me.BindingNavigatorMoveFirstItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem10.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem10.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem10.Name = "BindingNavigatorMoveFirstItem10"
        Me.BindingNavigatorMoveFirstItem10.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem10.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem10.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem10
        '
        Me.BindingNavigatorMovePreviousItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem10.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem10.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem10.Name = "BindingNavigatorMovePreviousItem10"
        Me.BindingNavigatorMovePreviousItem10.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem10.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem10.Text = "Move previous"
        '
        'BindingNavigatorSeparator30
        '
        Me.BindingNavigatorSeparator30.Name = "BindingNavigatorSeparator30"
        Me.BindingNavigatorSeparator30.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem10
        '
        Me.BindingNavigatorPositionItem10.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem10.AutoSize = False
        Me.BindingNavigatorPositionItem10.Name = "BindingNavigatorPositionItem10"
        Me.BindingNavigatorPositionItem10.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem10.Text = "0"
        Me.BindingNavigatorPositionItem10.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator31
        '
        Me.BindingNavigatorSeparator31.Name = "BindingNavigatorSeparator31"
        Me.BindingNavigatorSeparator31.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem10
        '
        Me.BindingNavigatorMoveNextItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem10.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem10.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem10.Name = "BindingNavigatorMoveNextItem10"
        Me.BindingNavigatorMoveNextItem10.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem10.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem10.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem10
        '
        Me.BindingNavigatorMoveLastItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem10.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem10.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem10.Name = "BindingNavigatorMoveLastItem10"
        Me.BindingNavigatorMoveLastItem10.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem10.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem10.Text = "Move last"
        '
        'BindingNavigatorSeparator32
        '
        Me.BindingNavigatorSeparator32.Name = "BindingNavigatorSeparator32"
        Me.BindingNavigatorSeparator32.Size = New System.Drawing.Size(6, 25)
        '
        'SaveToolStripButton
        '
        Me.SaveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.SaveToolStripButton.Image = CType(resources.GetObject("SaveToolStripButton.Image"), System.Drawing.Image)
        Me.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveToolStripButton.Name = "SaveToolStripButton"
        Me.SaveToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.SaveToolStripButton.Text = "&Save"
        '
        'TypeOfTenancyDataGridView
        '
        Me.TypeOfTenancyDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TypeOfTenancyDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.TypeOfTenancyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TypeOfTenancyDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn16, Me.TenancyId})
        Me.TypeOfTenancyDataGridView.DataSource = Me.TypeOfTenancyBindingSource
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TypeOfTenancyDataGridView.DefaultCellStyle = DataGridViewCellStyle14
        Me.TypeOfTenancyDataGridView.Location = New System.Drawing.Point(0, 31)
        Me.TypeOfTenancyDataGridView.Name = "TypeOfTenancyDataGridView"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TypeOfTenancyDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.TypeOfTenancyDataGridView.Size = New System.Drawing.Size(657, 279)
        Me.TypeOfTenancyDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "TenancyType"
        Me.DataGridViewTextBoxColumn16.HeaderText = "TenancyType"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'TenancyId
        '
        Me.TenancyId.DataPropertyName = "TenancyId"
        Me.TenancyId.HeaderText = "Id"
        Me.TenancyId.Name = "TenancyId"
        Me.TenancyId.ReadOnly = True
        Me.TenancyId.Visible = False
        Me.TenancyId.Width = 25
        '
        'TabPage2
        '
        Me.TabPage2.AutoScroll = True
        Me.TabPage2.Controls.Add(Me.bnAppSettings)
        Me.TabPage2.Controls.Add(Me.AppSettingsDataGridView)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(660, 313)
        Me.TabPage2.TabIndex = 8
        Me.TabPage2.Text = "AppSettings"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'bnAppSettings
        '
        Me.bnAppSettings.AddNewItem = Nothing
        Me.bnAppSettings.BindingSource = Me.AppSettingsBindingSource
        Me.bnAppSettings.CountItem = Me.BindingNavigatorCountItem
        Me.bnAppSettings.DeleteItem = Nothing
        Me.bnAppSettings.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btnSaveAppValues})
        Me.bnAppSettings.Location = New System.Drawing.Point(3, 3)
        Me.bnAppSettings.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.bnAppSettings.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.bnAppSettings.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.bnAppSettings.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.bnAppSettings.Name = "bnAppSettings"
        Me.bnAppSettings.PositionItem = Me.BindingNavigatorPositionItem
        Me.bnAppSettings.Size = New System.Drawing.Size(654, 25)
        Me.bnAppSettings.TabIndex = 1
        Me.bnAppSettings.Text = "bnApplicationValues"
        '
        'AppSettingsBindingSource
        '
        Me.AppSettingsBindingSource.DataMember = "AppSettings"
        Me.AppSettingsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnSaveAppValues
        '
        Me.btnSaveAppValues.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSaveAppValues.Image = CType(resources.GetObject("btnSaveAppValues.Image"), System.Drawing.Image)
        Me.btnSaveAppValues.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSaveAppValues.Name = "btnSaveAppValues"
        Me.btnSaveAppValues.Size = New System.Drawing.Size(23, 22)
        Me.btnSaveAppValues.Text = "&Save"
        '
        'AppSettingsDataGridView
        '
        Me.AppSettingsDataGridView.AllowUserToAddRows = False
        Me.AppSettingsDataGridView.AllowUserToDeleteRows = False
        Me.AppSettingsDataGridView.AutoGenerateColumns = False
        Me.AppSettingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AppSettingsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn1, Me.NameDataGridViewTextBoxColumn, Me.DecimalDataGridViewTextBoxColumn, Me.TextDataGridViewTextBoxColumn, Me.BooleanDataGridViewCheckBoxColumn, Me.CommentsDataGridViewTextBoxColumn})
        Me.AppSettingsDataGridView.DataSource = Me.AppSettingsBindingSource
        Me.AppSettingsDataGridView.Location = New System.Drawing.Point(0, 31)
        Me.AppSettingsDataGridView.Name = "AppSettingsDataGridView"
        Me.AppSettingsDataGridView.Size = New System.Drawing.Size(657, 282)
        Me.AppSettingsDataGridView.TabIndex = 0
        '
        'tpLatencyPoints
        '
        Me.tpLatencyPoints.Controls.Add(Me.dgvLatencyPoints)
        Me.tpLatencyPoints.Controls.Add(Me.bnLatencyPoints)
        Me.tpLatencyPoints.Location = New System.Drawing.Point(4, 22)
        Me.tpLatencyPoints.Name = "tpLatencyPoints"
        Me.tpLatencyPoints.Size = New System.Drawing.Size(660, 313)
        Me.tpLatencyPoints.TabIndex = 9
        Me.tpLatencyPoints.Text = "Latency Points"
        Me.tpLatencyPoints.UseVisualStyleBackColor = True
        '
        'dgvLatencyPoints
        '
        Me.dgvLatencyPoints.AllowUserToDeleteRows = False
        Me.dgvLatencyPoints.AutoGenerateColumns = False
        Me.dgvLatencyPoints.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLatencyPoints.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LogTypeDataGridViewTextBoxColumn, Me.SubjectDataGridViewTextBoxColumn, Me.NotesDataGridViewTextBoxColumn, Me.SortNum})
        Me.dgvLatencyPoints.DataSource = Me.LatencyPointsBindingSource
        Me.dgvLatencyPoints.Location = New System.Drawing.Point(3, 28)
        Me.dgvLatencyPoints.Name = "dgvLatencyPoints"
        Me.dgvLatencyPoints.Size = New System.Drawing.Size(654, 281)
        Me.dgvLatencyPoints.TabIndex = 7
        '
        'LogTypeDataGridViewTextBoxColumn
        '
        Me.LogTypeDataGridViewTextBoxColumn.DataPropertyName = "LogType"
        Me.LogTypeDataGridViewTextBoxColumn.HeaderText = "LogType"
        Me.LogTypeDataGridViewTextBoxColumn.Items.AddRange(New Object() {"Client", "Loan"})
        Me.LogTypeDataGridViewTextBoxColumn.Name = "LogTypeDataGridViewTextBoxColumn"
        Me.LogTypeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LogTypeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'SubjectDataGridViewTextBoxColumn
        '
        Me.SubjectDataGridViewTextBoxColumn.DataPropertyName = "Subject"
        Me.SubjectDataGridViewTextBoxColumn.HeaderText = "Subject"
        Me.SubjectDataGridViewTextBoxColumn.Name = "SubjectDataGridViewTextBoxColumn"
        Me.SubjectDataGridViewTextBoxColumn.Width = 200
        '
        'NotesDataGridViewTextBoxColumn
        '
        Me.NotesDataGridViewTextBoxColumn.DataPropertyName = "Notes"
        Me.NotesDataGridViewTextBoxColumn.HeaderText = "Notes"
        Me.NotesDataGridViewTextBoxColumn.Name = "NotesDataGridViewTextBoxColumn"
        Me.NotesDataGridViewTextBoxColumn.Width = 250
        '
        'SortNum
        '
        Me.SortNum.DataPropertyName = "SortNum"
        Me.SortNum.HeaderText = "SortNum"
        Me.SortNum.Name = "SortNum"
        Me.SortNum.Width = 70
        '
        'LatencyPointsBindingSource
        '
        Me.LatencyPointsBindingSource.DataMember = "LatencyPoints"
        Me.LatencyPointsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'bnLatencyPoints
        '
        Me.bnLatencyPoints.AddNewItem = Me.ToolStripButton1
        Me.bnLatencyPoints.BindingSource = Me.LatencyPointsBindingSource
        Me.bnLatencyPoints.CountItem = Me.ToolStripLabel1
        Me.bnLatencyPoints.DeleteItem = Me.ToolStripButton2
        Me.bnLatencyPoints.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton3, Me.ToolStripButton4, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.ToolStripButton5, Me.ToolStripButton6, Me.ToolStripSeparator3, Me.ToolStripButton1, Me.ToolStripButton2, Me.tsbtnLatencyPoints})
        Me.bnLatencyPoints.Location = New System.Drawing.Point(0, 0)
        Me.bnLatencyPoints.MoveFirstItem = Me.ToolStripButton3
        Me.bnLatencyPoints.MoveLastItem = Me.ToolStripButton6
        Me.bnLatencyPoints.MoveNextItem = Me.ToolStripButton5
        Me.bnLatencyPoints.MovePreviousItem = Me.ToolStripButton4
        Me.bnLatencyPoints.Name = "bnLatencyPoints"
        Me.bnLatencyPoints.PositionItem = Me.ToolStripTextBox1
        Me.bnLatencyPoints.Size = New System.Drawing.Size(660, 25)
        Me.bnLatencyPoints.TabIndex = 6
        Me.bnLatencyPoints.Text = "BindingNavigator Preliminary Results"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 22)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Delete"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Move first"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Move next"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton6.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton6.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'tsbtnLatencyPoints
        '
        Me.tsbtnLatencyPoints.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbtnLatencyPoints.Image = CType(resources.GetObject("tsbtnLatencyPoints.Image"), System.Drawing.Image)
        Me.tsbtnLatencyPoints.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnLatencyPoints.Name = "tsbtnLatencyPoints"
        Me.tsbtnLatencyPoints.Size = New System.Drawing.Size(23, 22)
        Me.tsbtnLatencyPoints.Text = "&Save"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(281, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Administration"
        '
        'gbUsers
        '
        Me.gbUsers.Controls.Add(Me.TabControl2)
        Me.gbUsers.Location = New System.Drawing.Point(12, 428)
        Me.gbUsers.Name = "gbUsers"
        Me.gbUsers.Size = New System.Drawing.Size(680, 280)
        Me.gbUsers.TabIndex = 2
        Me.gbUsers.TabStop = False
        Me.gbUsers.Text = "Users Administration"
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.tpUsers)
        Me.TabControl2.Controls.Add(Me.tpBranches)
        Me.TabControl2.Controls.Add(Me.tpRoles)
        Me.TabControl2.Location = New System.Drawing.Point(6, 19)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(668, 255)
        Me.TabControl2.TabIndex = 0
        '
        'tpUsers
        '
        Me.tpUsers.Controls.Add(Me.lblBranchName)
        Me.tpUsers.Controls.Add(Me.TextBox1)
        Me.tpUsers.Controls.Add(Me.lblFullName)
        Me.tpUsers.Controls.Add(Me.lblEmail)
        Me.tpUsers.Controls.Add(Me.txtbxEmail)
        Me.tpUsers.Controls.Add(Me.lblUserIdValue)
        Me.tpUsers.Controls.Add(Me.bnUsers)
        Me.tpUsers.Controls.Add(Me.PasswordTextBox)
        Me.tpUsers.Controls.Add(UserNameLabel)
        Me.tpUsers.Controls.Add(BranchIdLabel)
        Me.tpUsers.Controls.Add(Me.UserNameTextBox)
        Me.tpUsers.Controls.Add(Me.BranchIdComboBox)
        Me.tpUsers.Controls.Add(PasswordLabel)
        Me.tpUsers.Controls.Add(UserIdLabel)
        Me.tpUsers.Controls.Add(RoleLabel1)
        Me.tpUsers.Controls.Add(ActiveLabel)
        Me.tpUsers.Controls.Add(Me.ActiveCheckBox)
        Me.tpUsers.Controls.Add(Me.RoleComboBox)
        Me.tpUsers.Location = New System.Drawing.Point(4, 22)
        Me.tpUsers.Name = "tpUsers"
        Me.tpUsers.Padding = New System.Windows.Forms.Padding(3)
        Me.tpUsers.Size = New System.Drawing.Size(660, 229)
        Me.tpUsers.TabIndex = 0
        Me.tpUsers.Text = "Users"
        Me.tpUsers.UseVisualStyleBackColor = True
        '
        'lblBranchName
        '
        Me.lblBranchName.AutoSize = True
        Me.lblBranchName.Location = New System.Drawing.Point(173, 96)
        Me.lblBranchName.Name = "lblBranchName"
        Me.lblBranchName.Size = New System.Drawing.Size(72, 13)
        Me.lblBranchName.TabIndex = 22
        Me.lblBranchName.Text = "Branch Name"
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "FullName", True))
        Me.TextBox1.Location = New System.Drawing.Point(413, 39)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(235, 20)
        Me.TextBox1.TabIndex = 21
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblFullName
        '
        Me.lblFullName.AutoSize = True
        Me.lblFullName.Location = New System.Drawing.Point(333, 42)
        Me.lblFullName.Name = "lblFullName"
        Me.lblFullName.Size = New System.Drawing.Size(55, 13)
        Me.lblFullName.TabIndex = 20
        Me.lblFullName.Text = "Full name:"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(333, 122)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(76, 13)
        Me.lblEmail.TabIndex = 19
        Me.lblEmail.Text = "Email Address:"
        '
        'txtbxEmail
        '
        Me.txtbxEmail.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "emailaddress", True))
        Me.txtbxEmail.Location = New System.Drawing.Point(413, 119)
        Me.txtbxEmail.Name = "txtbxEmail"
        Me.txtbxEmail.Size = New System.Drawing.Size(235, 20)
        Me.txtbxEmail.TabIndex = 18
        '
        'lblUserIdValue
        '
        Me.lblUserIdValue.AutoSize = True
        Me.lblUserIdValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "UserId", True))
        Me.lblUserIdValue.Location = New System.Drawing.Point(74, 42)
        Me.lblUserIdValue.Name = "lblUserIdValue"
        Me.lblUserIdValue.Size = New System.Drawing.Size(38, 13)
        Me.lblUserIdValue.TabIndex = 17
        Me.lblUserIdValue.Text = "UserId"
        '
        'bnUsers
        '
        Me.bnUsers.AddNewItem = Me.BindingNavigatorAddNewItem2
        Me.bnUsers.BindingSource = Me.UsersBindingSource
        Me.bnUsers.CountItem = Me.BindingNavigatorCountItem2
        Me.bnUsers.DeleteItem = Me.BindingNavigatorDeleteItem2
        Me.bnUsers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem2, Me.BindingNavigatorMovePreviousItem2, Me.BindingNavigatorSeparator6, Me.BindingNavigatorPositionItem2, Me.BindingNavigatorCountItem2, Me.BindingNavigatorSeparator7, Me.BindingNavigatorMoveNextItem2, Me.BindingNavigatorMoveLastItem2, Me.BindingNavigatorSeparator8, Me.BindingNavigatorAddNewItem2, Me.BindingNavigatorDeleteItem2, Me.btnSaveUsers})
        Me.bnUsers.Location = New System.Drawing.Point(3, 3)
        Me.bnUsers.MoveFirstItem = Me.BindingNavigatorMoveFirstItem2
        Me.bnUsers.MoveLastItem = Me.BindingNavigatorMoveLastItem2
        Me.bnUsers.MoveNextItem = Me.BindingNavigatorMoveNextItem2
        Me.bnUsers.MovePreviousItem = Me.BindingNavigatorMovePreviousItem2
        Me.bnUsers.Name = "bnUsers"
        Me.bnUsers.PositionItem = Me.BindingNavigatorPositionItem2
        Me.bnUsers.Size = New System.Drawing.Size(654, 25)
        Me.bnUsers.TabIndex = 16
        Me.bnUsers.Text = "BindingNavigatorUsers"
        '
        'BindingNavigatorAddNewItem2
        '
        Me.BindingNavigatorAddNewItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem2.Image = CType(resources.GetObject("BindingNavigatorAddNewItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem2.Name = "BindingNavigatorAddNewItem2"
        Me.BindingNavigatorAddNewItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem2.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem2.Text = "Add new"
        '
        'BindingNavigatorCountItem2
        '
        Me.BindingNavigatorCountItem2.Name = "BindingNavigatorCountItem2"
        Me.BindingNavigatorCountItem2.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem2.Text = "of {0}"
        Me.BindingNavigatorCountItem2.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem2
        '
        Me.BindingNavigatorDeleteItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem2.Image = CType(resources.GetObject("BindingNavigatorDeleteItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem2.Name = "BindingNavigatorDeleteItem2"
        Me.BindingNavigatorDeleteItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem2.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem2.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem2
        '
        Me.BindingNavigatorMoveFirstItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem2.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem2.Name = "BindingNavigatorMoveFirstItem2"
        Me.BindingNavigatorMoveFirstItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem2.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem2.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem2
        '
        Me.BindingNavigatorMovePreviousItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem2.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem2.Name = "BindingNavigatorMovePreviousItem2"
        Me.BindingNavigatorMovePreviousItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem2.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem2.Text = "Move previous"
        '
        'BindingNavigatorSeparator6
        '
        Me.BindingNavigatorSeparator6.Name = "BindingNavigatorSeparator6"
        Me.BindingNavigatorSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem2
        '
        Me.BindingNavigatorPositionItem2.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem2.AutoSize = False
        Me.BindingNavigatorPositionItem2.Name = "BindingNavigatorPositionItem2"
        Me.BindingNavigatorPositionItem2.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem2.Text = "0"
        Me.BindingNavigatorPositionItem2.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator7
        '
        Me.BindingNavigatorSeparator7.Name = "BindingNavigatorSeparator7"
        Me.BindingNavigatorSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem2
        '
        Me.BindingNavigatorMoveNextItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem2.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem2.Name = "BindingNavigatorMoveNextItem2"
        Me.BindingNavigatorMoveNextItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem2.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem2.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem2
        '
        Me.BindingNavigatorMoveLastItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem2.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem2.Name = "BindingNavigatorMoveLastItem2"
        Me.BindingNavigatorMoveLastItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem2.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem2.Text = "Move last"
        '
        'BindingNavigatorSeparator8
        '
        Me.BindingNavigatorSeparator8.Name = "BindingNavigatorSeparator8"
        Me.BindingNavigatorSeparator8.Size = New System.Drawing.Size(6, 25)
        '
        'btnSaveUsers
        '
        Me.btnSaveUsers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSaveUsers.Image = CType(resources.GetObject("btnSaveUsers.Image"), System.Drawing.Image)
        Me.btnSaveUsers.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSaveUsers.Name = "btnSaveUsers"
        Me.btnSaveUsers.Size = New System.Drawing.Size(23, 22)
        Me.btnSaveUsers.Text = "&Save"
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "Password", True))
        Me.PasswordTextBox.Location = New System.Drawing.Point(77, 65)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(235, 20)
        Me.PasswordTextBox.TabIndex = 5
        '
        'UserNameTextBox
        '
        Me.UserNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "UserName", True))
        Me.UserNameTextBox.Location = New System.Drawing.Point(413, 67)
        Me.UserNameTextBox.Name = "UserNameTextBox"
        Me.UserNameTextBox.Size = New System.Drawing.Size(235, 20)
        Me.UserNameTextBox.TabIndex = 3
        '
        'BranchIdComboBox
        '
        Me.BranchIdComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "BranchId", True))
        Me.BranchIdComboBox.DataSource = Me.BranchesBindingSource
        Me.BranchIdComboBox.DisplayMember = "BranchId"
        Me.BranchIdComboBox.FormattingEnabled = True
        Me.BranchIdComboBox.Location = New System.Drawing.Point(77, 91)
        Me.BranchIdComboBox.Name = "BranchIdComboBox"
        Me.BranchIdComboBox.Size = New System.Drawing.Size(90, 21)
        Me.BranchIdComboBox.TabIndex = 14
        Me.BranchIdComboBox.ValueMember = "BranchId"
        '
        'BranchesBindingSource
        '
        Me.BranchesBindingSource.DataMember = "Branches"
        Me.BranchesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'ActiveCheckBox
        '
        Me.ActiveCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.UsersBindingSource, "active", True))
        Me.ActiveCheckBox.Location = New System.Drawing.Point(77, 117)
        Me.ActiveCheckBox.Name = "ActiveCheckBox"
        Me.ActiveCheckBox.Size = New System.Drawing.Size(23, 24)
        Me.ActiveCheckBox.TabIndex = 11
        Me.ActiveCheckBox.UseVisualStyleBackColor = True
        '
        'RoleComboBox
        '
        Me.RoleComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "role", True))
        Me.RoleComboBox.DataSource = Me.UserRolesBindingSource
        Me.RoleComboBox.DisplayMember = "Role"
        Me.RoleComboBox.FormattingEnabled = True
        Me.RoleComboBox.Location = New System.Drawing.Point(413, 93)
        Me.RoleComboBox.Name = "RoleComboBox"
        Me.RoleComboBox.Size = New System.Drawing.Size(235, 21)
        Me.RoleComboBox.TabIndex = 13
        Me.RoleComboBox.ValueMember = "Role"
        '
        'UserRolesBindingSource
        '
        Me.UserRolesBindingSource.DataMember = "UserRoles"
        Me.UserRolesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'tpBranches
        '
        Me.tpBranches.AutoScroll = True
        Me.tpBranches.Controls.Add(Me.bnBranches)
        Me.tpBranches.Controls.Add(BranchIdLabel1)
        Me.tpBranches.Controls.Add(Me.BranchIdTextBox)
        Me.tpBranches.Controls.Add(FullNameLabel)
        Me.tpBranches.Controls.Add(Me.FullNameTextBox)
        Me.tpBranches.Controls.Add(AddressLabel)
        Me.tpBranches.Controls.Add(Me.AddressTextBox)
        Me.tpBranches.Controls.Add(CityLabel)
        Me.tpBranches.Controls.Add(Me.CityTextBox)
        Me.tpBranches.Location = New System.Drawing.Point(4, 22)
        Me.tpBranches.Name = "tpBranches"
        Me.tpBranches.Padding = New System.Windows.Forms.Padding(3)
        Me.tpBranches.Size = New System.Drawing.Size(660, 229)
        Me.tpBranches.TabIndex = 1
        Me.tpBranches.Text = "Branches"
        Me.tpBranches.UseVisualStyleBackColor = True
        '
        'bnBranches
        '
        Me.bnBranches.AddNewItem = Me.BindingNavigatorAddNewItem6
        Me.bnBranches.BindingSource = Me.BranchesBindingSource
        Me.bnBranches.CountItem = Me.BindingNavigatorCountItem6
        Me.bnBranches.DeleteItem = Me.BindingNavigatorDeleteItem6
        Me.bnBranches.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem6, Me.BindingNavigatorMovePreviousItem6, Me.BindingNavigatorSeparator18, Me.BindingNavigatorPositionItem6, Me.BindingNavigatorCountItem6, Me.BindingNavigatorSeparator19, Me.BindingNavigatorMoveNextItem6, Me.BindingNavigatorMoveLastItem6, Me.BindingNavigatorSeparator20, Me.BindingNavigatorAddNewItem6, Me.BindingNavigatorDeleteItem6, Me.btnSaveBranches})
        Me.bnBranches.Location = New System.Drawing.Point(3, 3)
        Me.bnBranches.MoveFirstItem = Me.BindingNavigatorMoveFirstItem6
        Me.bnBranches.MoveLastItem = Me.BindingNavigatorMoveLastItem6
        Me.bnBranches.MoveNextItem = Me.BindingNavigatorMoveNextItem6
        Me.bnBranches.MovePreviousItem = Me.BindingNavigatorMovePreviousItem6
        Me.bnBranches.Name = "bnBranches"
        Me.bnBranches.PositionItem = Me.BindingNavigatorPositionItem6
        Me.bnBranches.Size = New System.Drawing.Size(654, 25)
        Me.bnBranches.TabIndex = 8
        Me.bnBranches.Text = "BindingNavigator Branches"
        '
        'BindingNavigatorAddNewItem6
        '
        Me.BindingNavigatorAddNewItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem6.Image = CType(resources.GetObject("BindingNavigatorAddNewItem6.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem6.Name = "BindingNavigatorAddNewItem6"
        Me.BindingNavigatorAddNewItem6.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem6.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem6.Text = "Add new"
        '
        'BindingNavigatorCountItem6
        '
        Me.BindingNavigatorCountItem6.Name = "BindingNavigatorCountItem6"
        Me.BindingNavigatorCountItem6.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem6.Text = "of {0}"
        Me.BindingNavigatorCountItem6.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem6
        '
        Me.BindingNavigatorDeleteItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem6.Image = CType(resources.GetObject("BindingNavigatorDeleteItem6.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem6.Name = "BindingNavigatorDeleteItem6"
        Me.BindingNavigatorDeleteItem6.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem6.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem6.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem6
        '
        Me.BindingNavigatorMoveFirstItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem6.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem6.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem6.Name = "BindingNavigatorMoveFirstItem6"
        Me.BindingNavigatorMoveFirstItem6.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem6.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem6.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem6
        '
        Me.BindingNavigatorMovePreviousItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem6.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem6.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem6.Name = "BindingNavigatorMovePreviousItem6"
        Me.BindingNavigatorMovePreviousItem6.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem6.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem6.Text = "Move previous"
        '
        'BindingNavigatorSeparator18
        '
        Me.BindingNavigatorSeparator18.Name = "BindingNavigatorSeparator18"
        Me.BindingNavigatorSeparator18.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem6
        '
        Me.BindingNavigatorPositionItem6.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem6.AutoSize = False
        Me.BindingNavigatorPositionItem6.Name = "BindingNavigatorPositionItem6"
        Me.BindingNavigatorPositionItem6.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem6.Text = "0"
        Me.BindingNavigatorPositionItem6.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator19
        '
        Me.BindingNavigatorSeparator19.Name = "BindingNavigatorSeparator19"
        Me.BindingNavigatorSeparator19.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem6
        '
        Me.BindingNavigatorMoveNextItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem6.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem6.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem6.Name = "BindingNavigatorMoveNextItem6"
        Me.BindingNavigatorMoveNextItem6.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem6.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem6.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem6
        '
        Me.BindingNavigatorMoveLastItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem6.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem6.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem6.Name = "BindingNavigatorMoveLastItem6"
        Me.BindingNavigatorMoveLastItem6.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem6.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem6.Text = "Move last"
        '
        'BindingNavigatorSeparator20
        '
        Me.BindingNavigatorSeparator20.Name = "BindingNavigatorSeparator20"
        Me.BindingNavigatorSeparator20.Size = New System.Drawing.Size(6, 25)
        '
        'btnSaveBranches
        '
        Me.btnSaveBranches.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSaveBranches.Image = CType(resources.GetObject("btnSaveBranches.Image"), System.Drawing.Image)
        Me.btnSaveBranches.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSaveBranches.Name = "btnSaveBranches"
        Me.btnSaveBranches.Size = New System.Drawing.Size(23, 22)
        Me.btnSaveBranches.Text = "&Save"
        '
        'BranchIdTextBox
        '
        Me.BranchIdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BranchesBindingSource, "BranchId", True))
        Me.BranchIdTextBox.Location = New System.Drawing.Point(74, 48)
        Me.BranchIdTextBox.Name = "BranchIdTextBox"
        Me.BranchIdTextBox.Size = New System.Drawing.Size(235, 20)
        Me.BranchIdTextBox.TabIndex = 1
        '
        'FullNameTextBox
        '
        Me.FullNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BranchesBindingSource, "FullName", True))
        Me.FullNameTextBox.Location = New System.Drawing.Point(395, 48)
        Me.FullNameTextBox.Name = "FullNameTextBox"
        Me.FullNameTextBox.Size = New System.Drawing.Size(235, 20)
        Me.FullNameTextBox.TabIndex = 3
        '
        'AddressTextBox
        '
        Me.AddressTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BranchesBindingSource, "Address", True))
        Me.AddressTextBox.Location = New System.Drawing.Point(74, 77)
        Me.AddressTextBox.Name = "AddressTextBox"
        Me.AddressTextBox.Size = New System.Drawing.Size(235, 20)
        Me.AddressTextBox.TabIndex = 5
        '
        'CityTextBox
        '
        Me.CityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BranchesBindingSource, "City", True))
        Me.CityTextBox.Location = New System.Drawing.Point(395, 77)
        Me.CityTextBox.Name = "CityTextBox"
        Me.CityTextBox.Size = New System.Drawing.Size(235, 20)
        Me.CityTextBox.TabIndex = 7
        '
        'tpRoles
        '
        Me.tpRoles.AutoScroll = True
        Me.tpRoles.Controls.Add(Me.bnUserRoles)
        Me.tpRoles.Controls.Add(Me.UserRolesDataGridView)
        Me.tpRoles.Location = New System.Drawing.Point(4, 22)
        Me.tpRoles.Name = "tpRoles"
        Me.tpRoles.Padding = New System.Windows.Forms.Padding(3)
        Me.tpRoles.Size = New System.Drawing.Size(660, 229)
        Me.tpRoles.TabIndex = 2
        Me.tpRoles.Text = "User Roles"
        Me.tpRoles.UseVisualStyleBackColor = True
        '
        'bnUserRoles
        '
        Me.bnUserRoles.AddNewItem = Me.BindingNavigatorAddNewItem7
        Me.bnUserRoles.BindingSource = Me.UserRolesBindingSource
        Me.bnUserRoles.CountItem = Me.BindingNavigatorCountItem7
        Me.bnUserRoles.DeleteItem = Me.BindingNavigatorDeleteItem7
        Me.bnUserRoles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem7, Me.BindingNavigatorMovePreviousItem7, Me.BindingNavigatorSeparator21, Me.BindingNavigatorPositionItem7, Me.BindingNavigatorCountItem7, Me.BindingNavigatorSeparator22, Me.BindingNavigatorMoveNextItem7, Me.BindingNavigatorMoveLastItem7, Me.BindingNavigatorSeparator23, Me.BindingNavigatorAddNewItem7, Me.BindingNavigatorDeleteItem7, Me.btnSaveUserRoles})
        Me.bnUserRoles.Location = New System.Drawing.Point(3, 3)
        Me.bnUserRoles.MoveFirstItem = Me.BindingNavigatorMoveFirstItem7
        Me.bnUserRoles.MoveLastItem = Me.BindingNavigatorMoveLastItem7
        Me.bnUserRoles.MoveNextItem = Me.BindingNavigatorMoveNextItem7
        Me.bnUserRoles.MovePreviousItem = Me.BindingNavigatorMovePreviousItem7
        Me.bnUserRoles.Name = "bnUserRoles"
        Me.bnUserRoles.PositionItem = Me.BindingNavigatorPositionItem7
        Me.bnUserRoles.Size = New System.Drawing.Size(654, 25)
        Me.bnUserRoles.TabIndex = 1
        Me.bnUserRoles.Text = "BindingNavigatorUserRoles"
        '
        'BindingNavigatorAddNewItem7
        '
        Me.BindingNavigatorAddNewItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem7.Image = CType(resources.GetObject("BindingNavigatorAddNewItem7.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem7.Name = "BindingNavigatorAddNewItem7"
        Me.BindingNavigatorAddNewItem7.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem7.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem7.Text = "Add new"
        Me.BindingNavigatorAddNewItem7.Visible = False
        '
        'BindingNavigatorCountItem7
        '
        Me.BindingNavigatorCountItem7.Name = "BindingNavigatorCountItem7"
        Me.BindingNavigatorCountItem7.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem7.Text = "of {0}"
        Me.BindingNavigatorCountItem7.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem7
        '
        Me.BindingNavigatorDeleteItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem7.Image = CType(resources.GetObject("BindingNavigatorDeleteItem7.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem7.Name = "BindingNavigatorDeleteItem7"
        Me.BindingNavigatorDeleteItem7.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem7.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem7.Text = "Delete"
        Me.BindingNavigatorDeleteItem7.Visible = False
        '
        'BindingNavigatorMoveFirstItem7
        '
        Me.BindingNavigatorMoveFirstItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem7.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem7.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem7.Name = "BindingNavigatorMoveFirstItem7"
        Me.BindingNavigatorMoveFirstItem7.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem7.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem7.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem7
        '
        Me.BindingNavigatorMovePreviousItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem7.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem7.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem7.Name = "BindingNavigatorMovePreviousItem7"
        Me.BindingNavigatorMovePreviousItem7.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem7.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem7.Text = "Move previous"
        '
        'BindingNavigatorSeparator21
        '
        Me.BindingNavigatorSeparator21.Name = "BindingNavigatorSeparator21"
        Me.BindingNavigatorSeparator21.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem7
        '
        Me.BindingNavigatorPositionItem7.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem7.AutoSize = False
        Me.BindingNavigatorPositionItem7.Name = "BindingNavigatorPositionItem7"
        Me.BindingNavigatorPositionItem7.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem7.Text = "0"
        Me.BindingNavigatorPositionItem7.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator22
        '
        Me.BindingNavigatorSeparator22.Name = "BindingNavigatorSeparator22"
        Me.BindingNavigatorSeparator22.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem7
        '
        Me.BindingNavigatorMoveNextItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem7.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem7.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem7.Name = "BindingNavigatorMoveNextItem7"
        Me.BindingNavigatorMoveNextItem7.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem7.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem7.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem7
        '
        Me.BindingNavigatorMoveLastItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem7.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem7.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem7.Name = "BindingNavigatorMoveLastItem7"
        Me.BindingNavigatorMoveLastItem7.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem7.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem7.Text = "Move last"
        '
        'BindingNavigatorSeparator23
        '
        Me.BindingNavigatorSeparator23.Name = "BindingNavigatorSeparator23"
        Me.BindingNavigatorSeparator23.Size = New System.Drawing.Size(6, 25)
        '
        'btnSaveUserRoles
        '
        Me.btnSaveUserRoles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSaveUserRoles.Image = CType(resources.GetObject("btnSaveUserRoles.Image"), System.Drawing.Image)
        Me.btnSaveUserRoles.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSaveUserRoles.Name = "btnSaveUserRoles"
        Me.btnSaveUserRoles.Size = New System.Drawing.Size(23, 22)
        Me.btnSaveUserRoles.Text = "&Save"
        Me.btnSaveUserRoles.Visible = False
        '
        'UserRolesDataGridView
        '
        Me.UserRolesDataGridView.AllowUserToAddRows = False
        Me.UserRolesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        Me.UserRolesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle17
        Me.UserRolesDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UserRolesDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.UserRolesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UserRolesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.Description})
        Me.UserRolesDataGridView.DataSource = Me.UserRolesBindingSource
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.UserRolesDataGridView.DefaultCellStyle = DataGridViewCellStyle19
        Me.UserRolesDataGridView.Location = New System.Drawing.Point(0, 31)
        Me.UserRolesDataGridView.MultiSelect = False
        Me.UserRolesDataGridView.Name = "UserRolesDataGridView"
        Me.UserRolesDataGridView.ReadOnly = True
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UserRolesDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.UserRolesDataGridView.Size = New System.Drawing.Size(660, 198)
        Me.UserRolesDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Role"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Role"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "PermissionLevel"
        Me.DataGridViewTextBoxColumn11.HeaderText = "PermissionLevel"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 90
        '
        'Description
        '
        Me.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Description.DataPropertyName = "Description"
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        Me.Description.ReadOnly = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 742)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(704, 22)
        Me.StatusStripMessage.TabIndex = 3
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'EnquiryMethodListTableAdapter
        '
        Me.EnquiryMethodListTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Me.EnquiryMethodListTableAdapter
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'PrelimSourceTableAdapter
        '
        Me.PrelimSourceTableAdapter.ClearBeforeFill = True
        '
        'PrelimResultsTableAdapter
        '
        Me.PrelimResultsTableAdapter.ClearBeforeFill = True
        '
        'BranchesTableAdapter
        '
        Me.BranchesTableAdapter.ClearBeforeFill = True
        '
        'UserRolesTableAdapter
        '
        Me.UserRolesTableAdapter.ClearBeforeFill = True
        '
        'TypeOfTenancyTableAdapter
        '
        Me.TypeOfTenancyTableAdapter.ClearBeforeFill = True
        '
        'LatencyPointsTableAdapter
        '
        Me.LatencyPointsTableAdapter.ClearBeforeFill = True
        '
        'AppSettingsTableAdapter1
        '
        Me.AppSettingsTableAdapter1.ClearBeforeFill = True
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Visible = False
        Me.IdDataGridViewTextBoxColumn.Width = 20
        '
        'IdDataGridViewTextBoxColumn1
        '
        Me.IdDataGridViewTextBoxColumn1.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn1.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn1.MinimumWidth = 2
        Me.IdDataGridViewTextBoxColumn1.Name = "IdDataGridViewTextBoxColumn1"
        Me.IdDataGridViewTextBoxColumn1.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn1.Visible = False
        Me.IdDataGridViewTextBoxColumn1.Width = 2
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "Name"
        Me.NameDataGridViewTextBoxColumn.HeaderText = "Name"
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        Me.NameDataGridViewTextBoxColumn.ReadOnly = True
        Me.NameDataGridViewTextBoxColumn.Width = 150
        '
        'DecimalDataGridViewTextBoxColumn
        '
        Me.DecimalDataGridViewTextBoxColumn.DataPropertyName = "Decimal"
        DataGridViewCellStyle16.Format = "N2"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.DecimalDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle16
        Me.DecimalDataGridViewTextBoxColumn.HeaderText = "Decimal"
        Me.DecimalDataGridViewTextBoxColumn.Name = "DecimalDataGridViewTextBoxColumn"
        Me.DecimalDataGridViewTextBoxColumn.Width = 70
        '
        'TextDataGridViewTextBoxColumn
        '
        Me.TextDataGridViewTextBoxColumn.DataPropertyName = "Text"
        Me.TextDataGridViewTextBoxColumn.HeaderText = "Text"
        Me.TextDataGridViewTextBoxColumn.Name = "TextDataGridViewTextBoxColumn"
        '
        'BooleanDataGridViewCheckBoxColumn
        '
        Me.BooleanDataGridViewCheckBoxColumn.DataPropertyName = "Boolean"
        Me.BooleanDataGridViewCheckBoxColumn.HeaderText = "Boolean"
        Me.BooleanDataGridViewCheckBoxColumn.Name = "BooleanDataGridViewCheckBoxColumn"
        Me.BooleanDataGridViewCheckBoxColumn.Width = 55
        '
        'CommentsDataGridViewTextBoxColumn
        '
        Me.CommentsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CommentsDataGridViewTextBoxColumn.DataPropertyName = "Comments"
        Me.CommentsDataGridViewTextBoxColumn.HeaderText = "Comments"
        Me.CommentsDataGridViewTextBoxColumn.Name = "CommentsDataGridViewTextBoxColumn"
        '
        'AdminForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 764)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.gbUsers)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.gbLists)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AdminForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Administration Form"
        Me.gbLists.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.tpEnquiryMethodList.ResumeLayout(False)
        Me.tpEnquiryMethodList.PerformLayout()
        CType(Me.bnEnquiryMethodList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnEnquiryMethodList.ResumeLayout(False)
        Me.bnEnquiryMethodList.PerformLayout()
        CType(Me.EnquiryMethodListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryMethodListDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPrelimSource.ResumeLayout(False)
        Me.tpPrelimSource.PerformLayout()
        CType(Me.bnPrelimSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPrelimSource.ResumeLayout(False)
        Me.bnPrelimSource.PerformLayout()
        CType(Me.PrelimSourceBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimSourceDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPrelimResults.ResumeLayout(False)
        Me.tpPrelimResults.PerformLayout()
        CType(Me.bnPrelimResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPrelimResults.ResumeLayout(False)
        Me.bnPrelimResults.PerformLayout()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimResultsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.bnTenancy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTenancy.ResumeLayout(False)
        Me.bnTenancy.PerformLayout()
        CType(Me.TypeOfTenancyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeOfTenancyDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.bnAppSettings, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnAppSettings.ResumeLayout(False)
        Me.bnAppSettings.PerformLayout()
        CType(Me.AppSettingsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AppSettingsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpLatencyPoints.ResumeLayout(False)
        Me.tpLatencyPoints.PerformLayout()
        CType(Me.dgvLatencyPoints, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LatencyPointsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnLatencyPoints, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnLatencyPoints.ResumeLayout(False)
        Me.bnLatencyPoints.PerformLayout()
        Me.gbUsers.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.tpUsers.ResumeLayout(False)
        Me.tpUsers.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnUsers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnUsers.ResumeLayout(False)
        Me.bnUsers.PerformLayout()
        CType(Me.BranchesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UserRolesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpBranches.ResumeLayout(False)
        Me.tpBranches.PerformLayout()
        CType(Me.bnBranches, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnBranches.ResumeLayout(False)
        Me.bnBranches.PerformLayout()
        Me.tpRoles.ResumeLayout(False)
        Me.tpRoles.PerformLayout()
        CType(Me.bnUserRoles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnUserRoles.ResumeLayout(False)
        Me.bnUserRoles.PerformLayout()
        CType(Me.UserRolesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbLists As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryMethodListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryMethodListTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryMethodListTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpEnquiryMethodList As System.Windows.Forms.TabPage
    Friend WithEvents EnquiryMethodListDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents tpPrelimSource As System.Windows.Forms.TabPage
    Friend WithEvents bnEnquiryMethodList As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSaveEnquiryMethod As System.Windows.Forms.ToolStripButton
    Friend WithEvents PrelimSourceBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimSourceTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimSourceTableAdapter
    Friend WithEvents PrelimSourceDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents bnPrelimSource As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem3 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem3 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnSavePrelimSource As System.Windows.Forms.ToolStripButton
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpPrelimResults As System.Windows.Forms.TabPage
    Friend WithEvents PrelimResultsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimResultsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter
    Friend WithEvents PrelimResultsDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents bnPrelimResults As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem5 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem5 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnSavePrelimResults As System.Windows.Forms.ToolStripButton
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbUsers As System.Windows.Forms.GroupBox
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents tpUsers As System.Windows.Forms.TabPage
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UserNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BranchIdComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents RoleComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents tpBranches As System.Windows.Forms.TabPage
    Friend WithEvents bnUsers As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem2 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnSaveUsers As System.Windows.Forms.ToolStripButton
    Friend WithEvents BranchesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BranchesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BranchesTableAdapter
    Friend WithEvents lblUserIdValue As System.Windows.Forms.Label
    Friend WithEvents bnBranches As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem6 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem6 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BranchIdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FullNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AddressTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveBranches As System.Windows.Forms.ToolStripButton
    Friend WithEvents tpRoles As System.Windows.Forms.TabPage
    Friend WithEvents UserRolesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UserRolesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UserRolesTableAdapter
    Friend WithEvents UserRolesDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents bnUserRoles As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem7 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem7 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnSaveUserRoles As System.Windows.Forms.ToolStripButton
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TypeOfTenancyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TypeOfTenancyTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfTenancyTableAdapter
    Friend WithEvents TypeOfTenancyDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents bnTenancy As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem10 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator30 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem10 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator31 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator32 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TenancyId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents AppSettingsDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents bnAppSettings As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnSaveAppValues As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtbxEmail As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents lblFullName As System.Windows.Forms.Label
    Friend WithEvents lblBranchName As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpLatencyPoints As System.Windows.Forms.TabPage
    Friend WithEvents dgvLatencyPoints As System.Windows.Forms.DataGridView
    Friend WithEvents bnLatencyPoints As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbtnLatencyPoints As System.Windows.Forms.ToolStripButton
    Friend WithEvents LatencyPointsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LatencyPointsTableAdapter
    Friend WithEvents LatencyPointsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LogTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents SubjectDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NotesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SortNum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AppSettingsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AppSettingsTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.AppSettingsTableAdapter
    Friend WithEvents IdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DecimalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BooleanDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CommentsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
