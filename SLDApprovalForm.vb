﻿Public Class SLDApprovalForm
    Dim thisEnquiryId As Integer
    Dim thisApplicationCode As String

    'Pass Variable Form Load
    Friend Sub PassVariable(ByVal EnquiryIdVar As Integer, ByVal ClientSalutationVar As String, ByVal ApplicationCode As String)
        thisEnquiryId = EnquiryIdVar
        thisApplicationCode = ApplicationCode
        'loads data into the 'EnquiryWorkSheetDataSet.Users' table. You can move, or remove it, as needed.
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)

        Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, EnquiryIdVar)

        'Set form title
        Me.Text = Me.Text & " - " & ClientSalutationVar

    End Sub

    Private Sub SLDApprovalForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbxLoanManager.SelectedIndex = -1
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.EnquiryBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub


    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim commentId As Integer
            'Get Row in Enquiry dataset
            Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            EnquiryRow.BeginEdit()
            EnquiryRow.WizardStatus = 7
            'update approval status 0 = nothing, 1 = declined, 2 = approved
            EnquiryRow.ApprovalStatus = 2
            EnquiryRow.ApproverName = LoggedinName
            EnquiryRow.DDAprovalDate = Date.Now 'used field DDApprovalDate to store Split Loan Drawdown date
            'update Current Status
            EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_AUDIT
            EnquiryRow.CurrentStatusSetDate = DateTime.Now
            'Set Original Enquiry Time
            Dim OriginalTime As DateTime
            OriginalTime = EnquiryRow.DateTime
            'calculate time difference for 30min status
            Dim StatusTimeDiff As Integer
            StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
            If StatusTimeDiff < 30 Then
                EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_AUDIT
            End If
            'save changes
            Me.Validate()
            EnquiryRow.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

            'create log note
            Dim CommentStr As String
            CommentStr = "VERIFICATION:Approving this Split Loan Drawdown I have checked the Terms and Conditions of the loan and the drawdown complies with the documentation." & _
            vbCrLf & "Approved Split Loan Drawdown and Status changed to " & Constants.CONST_CURRENT_STATUS_AUDIT
            'Dim commentId As Integer
            commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

            ''set OAC Status
            ''Check is possible to Change Remote Status
            'Dim systemCanChangeRemoteStatus As Boolean
            'Dim retTTResult As New TrackResult
            'Dim onlineApplicationService As New OnlineApplication
            'Dim thisAppsettings As New AppSettings
            'Dim note As String = String.Empty
            'systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
            'If systemCanChangeRemoteStatus = True Then
            '    'Change Status in OAC
            '    Dim actionType As Integer = OACWebService.ApplicationActionType.Audit
            '    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
            '    If retTTResult.Status = False Then 'error condition
            '        commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
            '        MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
            '    End If

            'End If

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Catch ex As Exception
            MsgBox("Split Loan Drawdown Approval caused an error:" & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub btnDeclined_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeclined.Click
        Try
            'Get Row in Enquiry dataset
            Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            'Application declined
            EnquiryRow.BeginEdit()
            EnquiryRow.WizardStatus = 7
            'update approval status 0 = nothing, 1 = declined, 2 = approved
            EnquiryRow.ApprovalStatus = 1
            EnquiryRow.ApproverName = LoggedinName
            EnquiryRow.DDAprovalDate = Date.Now
            'update Current Status
            EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DECLINED
            EnquiryRow.CurrentStatusSetDate = DateTime.Now
            'Set Original Enquiry Time
            Dim OriginalTime As DateTime
            OriginalTime = EnquiryRow.DateTime
            'calculate time difference for 30min status
            Dim StatusTimeDiff As Integer
            StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
            If StatusTimeDiff < 30 Then
                EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_DECLINED
            End If

            'create log note
            Dim DateStr As String
            Dim NewCommentStr As String
            Dim CommentStr As String
            Dim LoanComments As String = EnquiryRow.LoanComments
            'Create Date string
            DateStr = "-- " & DateTime.Now.ToString("f")
            'Add Date string to comments
            CommentStr = DateStr & " | " & LoggedinName & vbCrLf & "Declined Split Loan Drawdown and Status changed to " & Constants.CONST_CURRENT_STATUS_DECLINED & vbCrLf & vbCrLf
            'check loan comments from dataset exist
            If Not LoanComments = "" Then
                NewCommentStr = CommentStr & LoanComments
            Else
                NewCommentStr = CommentStr
            End If
            'update LoanComments 
            EnquiryRow.LoanComments = NewCommentStr
            'save changes
            Me.Validate()
            EnquiryRow.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

            Me.DialogResult = System.Windows.Forms.DialogResult.No
        Catch ex As Exception
            MsgBox("Split Loan Drawdown Approval caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub
End Class