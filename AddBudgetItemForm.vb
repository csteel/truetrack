﻿Public Class AddBudgetItemForm
    ' set New Enquiry Id 
    Dim ThisEnquiryId As Integer
    'set SecurityId
    Dim ThisBudgetId As Integer

    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer, Optional ByVal LoadBudgetVar As Integer = 0)
        ThisEnquiryId = LoadEnquiryVar
        ThisBudgetId = LoadBudgetVar

        Try
            If ThisBudgetId <> 0 Then
                Me.BudgetTableAdapter.FillByBudgetId(Me.EnquiryWorkSheetDataSet.Budget, ThisBudgetId)
                btnAddBudgetItem.Visible = False
                btnUpdate.Visible = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAddBudgetItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBudgetItem.Click
        Try
            Me.Validate()
            Me.BudgetBindingSource.EndEdit()
            'Me.BudgetTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Budget)
            Me.BudgetTableAdapter.InsertWithEnquiryId(ThisEnquiryId, cmbxBudgetType.Text, txtbxDescription.Text, txtbxBudgetAmount.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Me.Validate()
            Me.BudgetBindingSource.EndEdit()
            Me.BudgetTableAdapter.Update(EnquiryWorkSheetDataSet.Budget)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class