﻿Imports System.Data.SqlClient

Public Class AddSecurityForm
    ' set New Enquiry Id 
    Dim thisEnquiryId As Integer
    'set SecurityId
    Dim thisSecurityId As Integer
    'set EnquiryManagerId
    Dim thisEnquiryManagerId As Integer
    Dim ckbxSecurityInsuranceSwitch As Boolean = False 'record if checkbox changed state
    Dim ckbxSecurityValuedSwitch As Boolean = False 'record if checkbox changed state
    Dim ThisCurrentStatus As String

    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer, ByVal LoadEnquiryManagerVar As Integer, Optional ByVal LoadSecurityVar As Integer = 0)
        'Remove Handlers
        RemoveHandler ckbxSecurityValued.CheckedChanged, AddressOf ckbxSecurity_CheckedChanged
        RemoveHandler ckbxSecurityInsurance.CheckedChanged, AddressOf ckbxSecurityInsurance_CheckedChanged

        thisEnquiryId = LoadEnquiryVar
        thisEnquiryManagerId = LoadEnquiryManagerVar
        thisSecurityId = LoadSecurityVar
        'MsgBox("This Enquiry Id = " + CType(ThisEnquiryId, String) + vbCrLf + "This Security Id = " + CType(ThisSecurityId, String))
        'get Current status
        Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection As New SqlConnection(connectionString)
        Dim selectStatement As String = "SELECT Enquiry.CurrentStatus FROM Enquiry WHERE EnquiryId = " & thisEnquiryId
        'MsgBox("Select Statement = " & selectStatement)
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        Dim enquiryTempDataAdapter As New SqlDataAdapter(selectCommand)
        Dim enquiryTempDataSet As New DataSet
        Dim enquiryTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            enquiryTempDataAdapter.Fill(enquiryTempDataTable)
            'if no matching rows .....
            If enquiryTempDataTable.Rows.Count = 0 Then
                MessageBox.Show("No CurrentStatus, please try again.")
                ' clear the dataTable and the Connect information
                enquiryTempDataAdapter = Nothing
                enquiryTempDataTable.Clear()
                'if there is a matching row
            ElseIf enquiryTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim enquiryTempDataRow As DataRow = enquiryTempDataTable.Rows(0)
                ThisCurrentStatus = enquiryTempDataRow.Item(0)
                ' clear the dataTable and the Connect information
                enquiryTempDataAdapter = Nothing
                enquiryTempDataTable.Clear()
            End If
            'close the connection
            If connection.State <> ConnectionState.Closed Then
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'loads data into the 'EnquiryWorkSheetDataSet.SecurityTypes' table.
        Me.SecurityTypesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.SecurityTypes)
        Try
            If thisSecurityId <> 0 Then
                Me.SecurityTableAdapter.FillBySecurityId(Me.EnquiryWorkSheetDataSet.Security, thisSecurityId)
                btnAddSecurity.Visible = False
                btnUpdate.Visible = True
            End If
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        '' check that user is EnquiryManager or a Manager
        'If LoggedinId = ThisEnquiryManagerId Or LoggedinPermissionLevel > 3 Then
        '    'enable ckbxs
        '    ckbxSecurityInsurance.Enabled = True
        '    ckbxSecurity.Enabled = True
        'Else
        '    'disable ckbxs
        '    ckbxSecurityInsurance.Enabled = False
        '    ckbxSecurity.Enabled = False
        'End If

        'check CurrentStatus and disable controls
        Select ThisCurrentStatus
            Case Constants.CONST_CURRENT_STATUS_DECLINED  '"Declined"
                disableControls()
            Case Constants.CONST_CURRENT_STATUS_CAD_APPROVED '"Waiting Sign-up"
                disableControls()
            Case Constants.CONST_CURRENT_STATUS_AUDIT '"Audit"
                disableControls()
            Case Constants.CONST_CURRENT_STATUS_AUDIT_FAILED '"Audit Failed"
                disableControls()
            Case Constants.CONST_CURRENT_STATUS_PAYOUT '"Payout"
                disableControls()
            Case Constants.CONST_CURRENT_STATUS_COMPLETED '"Completed"
                disableControls()
            Case Constants.CONST_CURRENT_STATUS_EXPORTED '"Exported"
                disableControls()
            Case Else

        End Select

        '*************** Set permissions
        Select Case LoggedinPermissionLevel
            Case Is < 3 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- < 3
                Panel1.Enabled = False
                btnAddSecurity.Enabled = False
                btnUpdate.Enabled = False
            Case Else '>= 3

        End Select
        '*************** end of Set permissions

    End Sub

    Private Sub AddSecurityForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Add Handlers
        AddHandler ckbxSecurityValued.CheckedChanged, AddressOf ckbxSecurity_CheckedChanged
        AddHandler ckbxSecurityInsurance.CheckedChanged, AddressOf ckbxSecurityInsurance_CheckedChanged
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSecurity.Click
        'Check Values are filled out
        Dim checkFieldsMessage As String = ""
        If txtbxDescription.Text Is DBNull.Value Or Not txtbxDescription.Text.Length > 0 Then
            checkFieldsMessage = "Please enter a Description" + vbCrLf
        End If
        
        If txtbxTodayValue.Text Is DBNull.Value Or Not txtbxTodayValue.Text.Length > 0 Then
            checkFieldsMessage = "Please enter a Today Value" + vbCrLf
        End If
        If txtbxLendingPercentage.Text Is DBNull.Value Or Not txtbxLendingPercentage.Text.Length > 0 Then
            checkFieldsMessage = "Please enter a Lending Percentage" + vbCrLf
        End If

        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Else
            Try
                'MsgBox("Lending Value = " + lblLendingValue.Text)
                'MsgBox("Security Type = " & cmbxSecurityType.SelectedValue.ToString)
                Me.Validate()
                Me.SecurityBindingSource.EndEdit()
                'add new security
                Me.SecurityTableAdapter.FillByNewSecurity(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId, cmbxSecurityType.SelectedValue.ToString, txtbxDescription.Text, dtpLastvalued.Value, txtbxTodayValue.Text, txtbxTodayValuer.Text, txtbxLendingPercentage.Text, lblLendingValue.Text, txtbxComments.Text, ckbxSecurityInsurance.CheckState, txtbxInsuranceComments.Text, ckbxSecurityValued.CheckState)
                'get the SecurityId generated
                thisSecurityId = EnquiryWorkSheetDataSet.Security.Rows(SecurityBindingSource.Position()).Item("SecurityId")

                'create Log notes if ckbx changed
                If ckbxSecurityInsuranceSwitch = True Then
                    Dim text As String = ""
                    'get current checkedstate
                    Try
                        If ckbxSecurityInsurance.CheckState = CheckState.Checked Then
                            text = "Security - " & txtbxDescription.Text & ": Security is Insured; Status Changed to Confirmed"
                            'Create log note
                            CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                        ElseIf ckbxSecurityInsurance.CheckState = CheckState.Unchecked Then
                            text = "Security - " & txtbxDescription.Text & ": Security is Insured; Status Changed to Not Confirmed"
                            'Create log note
                            CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                        End If
                    Catch ex As Exception
                        MsgBox("ERROR AddSecurityForm: " & ex.Message)
                    End Try
                    'reset switch
                    ckbxSecurityInsuranceSwitch = False
                End If
                'create Log notes if ckbx changed
                If ckbxSecurityValuedSwitch = True Then
                    Dim text As String = ""
                    ' I am satisfied that 'Today's Value' represents the true value of the security
                    'get current checkedstate
                    Try
                        If ckbxSecurityValued.CheckState = CheckState.Checked Then
                            text = "Security - " & txtbxDescription.Text & ": Satisfied security is Valued Correctly; Status Changed to Confirmed"
                            'Create log note
                            CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                        ElseIf ckbxSecurityValued.CheckState = CheckState.Unchecked Then
                            text = "Security - " & txtbxDescription.Text & ": Satisfied security is Valued Correctly; Status Changed to Not Confirmed"
                            'Create log note
                            CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                        End If
                    Catch ex As Exception
                        MsgBox("ERROR AddSecurityForm: " & ex.Message)
                    End Try
                    'reset switch
                    ckbxSecurityValuedSwitch = False
                End If

            Catch ex As Exception
                MsgBox("ERROR AddSecurityForm: " & ex.Message)
            End Try

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        End If
    End Sub

    Private Sub txtbxLendingPercentage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxLendingPercentage.TextChanged
        Try
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
            Dim LendFactor As Decimal
            If txtbxLendingPercentage.Text <> "" And txtbxTodayValue.Text <> "" Then
                LendFactor = CType(txtbxLendingPercentage.Text, Decimal)
                If LendFactor > 1 Then
                    LendFactor = LendFactor / 100
                End If
                Dim TodayVal As Decimal
                Dim LendVal As Decimal
                TodayVal = CType(txtbxTodayValue.Text, Decimal)
                LendVal = TodayVal * LendFactor
                lblLendingValue.Text = LendVal.ToString("C")
            Else
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Today Value And/Or Lending percentage need to be filled in!"
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub txtbxTodayValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxTodayValue.TextChanged
        Try
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
            Dim LendFactor As Decimal
            If txtbxLendingPercentage.Text <> "" And txtbxTodayValue.Text <> "" Then
                LendFactor = CType(txtbxLendingPercentage.Text, Decimal)
                If LendFactor > 1 Then
                    LendFactor = LendFactor / 100
                End If
                Dim TodayVal As Decimal
                Dim LendVal As Decimal
                TodayVal = CType(txtbxTodayValue.Text, Decimal)
                LendVal = TodayVal * LendFactor
                lblLendingValue.Text = LendVal.ToString("C")
            Else
                lblLendingValue.Text = "0"
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Today Value And/Or Lending percentage need to be filled in!"
            End If

        Catch ex As Exception
            MsgBox("ERROR AddSecurityForm: " & ex.Message)
        End Try




    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.SecurityBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'Check Values are filled out
        
        If txtbxTodayValue.Text = "" Then
            txtbxTodayValue.Text = "0"
        End If
        If txtbxLendingPercentage.Text = "" Then
            txtbxLendingPercentage.Text = "0"
        End If
        Try
            'create Log notes if ckbx changed
            If ckbxSecurityInsuranceSwitch = True Then
                Dim text As String = ""
                'get current checkedstate
                Try
                    If ckbxSecurityInsurance.CheckState = CheckState.Checked Then
                        text = "Security - " & txtbxDescription.Text & ": Security is Insured; Status Changed to Confirmed"
                        'Create log note
                        CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                    ElseIf ckbxSecurityInsurance.CheckState = CheckState.Unchecked Then
                        text = "Security - " & txtbxDescription.Text & ": Security is Insured; Status Changed to Not Confirmed"
                        'Create log note
                        CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                    End If
                Catch ex As Exception
                    MsgBox("ERROR AddSecurityForm: " & ex.Message)
                End Try
                'reset switch
                ckbxSecurityInsuranceSwitch = False
            End If
            'create Log notes if ckbx changed
            If ckbxSecurityValuedSwitch = True Then
                Dim text As String = ""
                ' I am satisfied that 'Today's Value' represents the true value of the security
                'get current checkedstate
                Try
                    If ckbxSecurityValued.CheckState = CheckState.Checked Then
                        text = "Security - " & txtbxDescription.Text & ": Satisfied security is Valued Correctly; Status Changed to Confirmed"
                        'Create log note
                        CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                    ElseIf ckbxSecurityValued.CheckState = CheckState.Unchecked Then
                        text = "Security - " & txtbxDescription.Text & ": Satisfied security is Valued Correctly; Status Changed to Not Confirmed"
                        'Create log note
                        CreateLogNote(thisEnquiryId, LoggedinName, text, "SecurityComments")
                    End If
                Catch ex As Exception
                    MsgBox("ERROR AddSecurityForm: " & ex.Message)
                End Try
                'reset switch
                ckbxSecurityValuedSwitch = False
            End If

            'Update Security Table
            Me.Validate()
            Me.SecurityBindingSource.EndEdit()
            Me.SecurityTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Security)

        Catch ex As Exception
            MsgBox("ERROR AddSecurityForm: " & ex.Message)
        End Try

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub


    Private Sub ckbxSecurityInsurance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxSecurityInsurance.CheckedChanged
        'record checkbox changed state
        ckbxSecurityInsuranceSwitch = True
    End Sub


    Private Sub ckbxSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxSecurityValued.CheckedChanged
        'record checkbox changed state
        ckbxSecurityValuedSwitch = True

    End Sub

    Private Sub disableControls()
        cmbxSecurityType.Enabled = False
        txtbxDescription.IsReadOnly = True
        dtpLastvalued.Enabled = False
        txtbxTodayValuer.ReadOnly = True
        txtbxTodayValue.ReadOnly = True
        txtbxLendingPercentage.ReadOnly = True
        txtbxComments.IsReadOnly = True
        ckbxSecurityValued.Enabled = False
        txtbxInsuranceComments.IsReadOnly = True
        ckbxSecurityInsurance.Enabled = False
        btnAddSecurity.Visible = False
        btnUpdate.Visible = False
    End Sub


End Class