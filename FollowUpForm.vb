﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Runtime.InteropServices

Public Class FollowUpForm

    Dim thisEnquiryId As Integer ' set New Enquiry Id 
    Dim thisCurrentStatus As String
    Dim thisWizardStatus As Integer ' set New Wizard Status 
    Dim thisClientId As Integer ' set New Client Id 
    Dim thisDealerId As String ' set Dealer Id
    Dim thisEnquiryManagerId As Integer
    Dim thisDealerName As String
    Dim thisEnquiryCode As String 'set Enquiry Code
    Dim thisContactDisplayName As String
    Dim contactName As String
    Dim contactLastName As String
    Dim contactAddress As String
    Dim thisEnquiryResult As String = ""
    Dim thisEnquiryType As Integer
    Dim formTitle As String = Me.Text
    'user settings
    Dim newLocX As Integer
    Dim newLocY As Integer

    'Loads form with parameters from PreliminaryForm1
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)
        'disable Handlers
        'RemoveHandler EnquiryResultsComboBox.SelectedIndexChanged, AddressOf EnquiryResultsComboBox_SelectedIndexChanged
        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)

        ' setup the reference variables
        thisEnquiryId = loadEnquiryVar
        'set the form tag with EnquiryId
        Me.Tag = thisEnquiryId
        'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
        Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
        'Get current status
        thisCurrentStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentStatus")
        'Get Wizard Status
        thisWizardStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("WizardStatus")
        'get Dealer Id
        thisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
        'get Enquiry Code
        thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
        'get EnquiryManagerId
        thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
        'get type of Enquiry
        thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher   
        If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            lblLoanAmount.Visible = False
        Else
            lblLoanAmount.Visible = True
        End If
        'set EnquiryType label
        lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
        'Load User table
        UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
        'get UserName
        lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
        'loads data into the 'EnquiryWorkSheetDataSet.PrelimResults' table.
        Me.PrelimResultsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimResults)
        'load data into the 'EnquiryWorkSheetDataSet.Activedealers' table for this Dealer Id.
        Me.ActiveDealersTableAdapter.FillByDealerId(Me.EnquiryWorkSheetDataSet.ActiveDealers, thisDealerId)
        'load the Enquiry Comments
        Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
        'Get Contact Display Name
        thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
        'Set form title
        If My.Settings.IsTest = True Then
            Me.Text = "Test Application | " & thisContactDisplayName & " - " & formTitle
        Else
            Me.Text = thisContactDisplayName & " - " & formTitle
        End If
        '*************** Contact details
        contactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
        contactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " & _
        EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & contactLastName
        'set lblContactName
        lblContactName.Text = contactName
        'Contact address
        contactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
        lblContactAddress.Text = contactAddress

        '*************** Get Dealer name
        Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection2 As New SqlConnection(connectionString2)
        Dim selectStatement2 As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & thisDealerId & "'"
        'MsgBox("Select Statement = " & selectStatement1)
        Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
        Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand2)
        Dim dealerTempDataSet As New DataSet
        Dim dealerTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            dealerTempDataAdapter.Fill(dealerTempDataTable)
            'if no matching rows .....
            If dealerTempDataTable.Rows.Count = 0 Then
                MessageBox.Show("No Dealer Name, please try again.")
                'clear the dataTable and the Connect information
                dealerTempDataAdapter = Nothing
                dealerTempDataTable.Clear()
                'if there is a matching row
            ElseIf dealerTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                thisDealerName = dealerTempDataRow.Item(0)
                'clear the dataTable and the Connect information
                dealerTempDataAdapter = Nothing
                dealerTempDataTable.Clear()
            End If
            'close the connection
            If connection2.State <> ConnectionState.Closed Then
                connection2.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        lblDealerName.Text = thisDealerName
        '*************** end of Get Dealer name
        '*************** Set permissions
        'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
        Select Case LoggedinPermissionLevel
            Case Is = 1 'Readonly
                UpdateAllToolStripMenuItem.Visible = False
                btnSaveAndExit.Enabled = False
                btnWorksheetWizard.Enabled = False

            Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- 2
                ApprovalFormToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                btnWorksheetWizard.Enabled = False

            Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                'Can see /use Archive Enquiry in File menu for their user status
                '--------------------------------------- 2
                ApprovalFormToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                EndEnquiryToolStripMenuItem.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
            Case Is = 4
                '--------------------------------------- 2
                ApprovalFormToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                EndEnquiryToolStripMenuItem.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True

            Case Is = 5
                'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                '--------------------------------------- 2
                ApprovalFormToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                EndEnquiryToolStripMenuItem.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True

            Case Is > 5 'Systems Manager Level, Open.
                '--------------------------------------- 2
                ApprovalFormToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                EndEnquiryToolStripMenuItem.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True

            Case Else 'Readonly
                UpdateAllToolStripMenuItem.Visible = False
                btnSaveAndExit.Enabled = False
                btnWorksheetWizard.Enabled = False

        End Select
        '*************** end of Set permissions
        '*************** set documents button visibility
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
        'Check directory exists
        If Directory.Exists(folderPath) Then
            'get new image
            btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
            'activate drag and drop
            Me.btnDocs.AllowDrop = True
        Else
            'get new image
            btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
            'deactivate drag and drop
            Me.btnDocs.AllowDrop = False
        End If
        '*************** end of set documents button visibility

        'set StatusStrip text
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."


    End Sub

    'Brings the thread that created the specified window into the foreground and activates the window. 
    'Keyboard input is directed to the window, and various visual cues are changed for the user. 
    'The system assigns a slightly higher priority to the thread that created the foreground window than it does to other threads. 
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function SetForegroundWindow(ByVal hwnd As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function ShowWindow(ByVal hwnd As IntPtr, ByVal cmdshow As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function SetWindowPos(ByVal hwnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As UInteger) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function




    Private Sub FormRefresh()
        SuspendLayout()
        Call PassVariable(thisEnquiryId)
        ResumeLayout()

    End Sub

    Private Sub FollowUpForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SuspendLayout()

        'check if data has changed
        Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not bindSrc2.Current Is Nothing Then
            rowView2 = CType(bindSrc2.Current, DataRowView)
            'test if any fields have changed
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        bindSrc2.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If

        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            newLocX = 0
        Else
            newLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            newLocY = 0
        Else
            newLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.PrelimFormLocation = New Point(newLocX, newLocY)
        My.Settings.PrelimFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
    End Sub

    Private Sub btnSaveAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndExit.Click
        Cursor.Current = Cursors.WaitCursor
        'check Status
        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PRELIMINARY Then 'Preliminary
            ''update fields
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
            enquiryRow.BeginEdit()
            'update Current Status
            enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_FOLLOW_UP 'Follow-up
            'Set Original Enquiry Time
            Dim originalTime As DateTime
            originalTime = enquiryRow.DateTime
            'calculate time difference for 30min status
            Dim statusTimeDiff As Integer
            statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
            If statusTimeDiff < 30 Then
                enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_FOLLOW_UP 'Follow-up
            End If
            'create log note
            'Dim dateStr As String
            'Dim newCommentStr As String
            Dim commentStr As String
            'Dim loanComments As String = enquiryRow.LoanComments
            ''Create Date string
            'dateStr = "-- " & DateTime.Now.ToString("f")
            ''Add Date string to comments
            'commentStr = dateStr & " | " & LoggedinName & vbCrLf & "Status changed to Follow-up" & vbCrLf & vbCrLf
            ''check loan comments from dataset exist
            'If Not loanComments = "" Then
            '    newCommentStr = commentStr & loanComments
            'Else
            '    newCommentStr = commentStr
            'End If
            ''update LoanComments 
            'enquiryRow.LoanComments = newCommentStr
            ''save changes

            Dim commentId As Integer
            commentStr = "Status changed to " & Constants.CONST_CURRENT_STATUS_FOLLOW_UP
            commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

        End If

        'save changes
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default
        'close form
        Me.Close()
    End Sub

    Private Sub btnWorksheetWizard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWorksheetWizard.Click
        Dim launchWizard As Boolean = False
        Try
            Dim progressStatus As Boolean = False
            'leave if bindingsource.current is nothing
            If Not EnquiryBindingSource.Current Is Nothing Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Saving changed data."
                Cursor.Current = Cursors.WaitCursor
                'update fields
                Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                'check if LoanReason(Type of Loan) is "Personal Loan (Top-up)"
                'if is then ask for "CurrentLoanAmt"
                If enquiryRow.EnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or enquiryRow.EnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or enquiryRow.EnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or enquiryRow.EnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Or enquiryRow.EnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or enquiryRow.EnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Then
                    'ask for CurrentLoanAmt
                    'launch ChangeDealerForm and pass Enquiry Id
                    Dim addCurrentLoanAmtFrm As New AddCurrentLoanAmtForm
                    addCurrentLoanAmtFrm.PassVariable(thisEnquiryId)
                    'ChangeDealerFrm.ShowDialog()
                    If (addCurrentLoanAmtFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                        progressStatus = True
                    Else
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Entering of the Current Loan Amount cancelled, unable to progress.  Status: Ready."
                        progressStatus = False
                    End If
                Else
                    progressStatus = True
                End If


            Else
                'no current record in binding source - do not proceed
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                progressStatus = False
            End If

            If progressStatus = True Then
                Cursor.Current = Cursors.WaitCursor

                'check id QRGList table entry exists, if not then Insert with ThisEnquiryId to create record
                Dim insertResult As Integer
                Try
                    insertResult = Me.QrgListTableAdapter1.InsertWithEnquiryId(Me.EnquiryWorkSheetDataSet.QRGList, thisEnquiryId)
                    If insertResult = 1 Then
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Successfully created New Quick Reference Guide.   Status: Ready."
                    ElseIf insertResult = 0 Then
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Quick Reference Guide already exists.  Status: Ready."
                    Else
                        MessageBox.Show("Error creating Quick Reference Guide!", "Enquiry Folder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Error creating Quick Reference Guide."
                    End If
                Catch ex As Exception
                    MsgBox("ERROR: QRGListTableAdapter caused an error" & vbCrLf & ex.Message)
                End Try

                Try
                    'create Enquiry folder on shared drive
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Creating Shared Folder."

                    'get WorksheetSharedFolder string
                    Dim wkshtStr As String = Switch.GetWorksheetSharedFolder
                    Dim fname As String = wkshtStr & "\" & thisEnquiryCode
                    If Directory.Exists(wkshtStr) Then 'check the shared folder exists
                        'Determine whether the directory exists.
                        If Directory.Exists(fname) Then
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Blue
                            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
                        Else
                            'create directory
                            IO.Directory.CreateDirectory(fname)
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
                        End If
                        launchWizard = True
                    Else
                        MessageBox.Show("Path to Shared Folder does not exist!" & vbCrLf & "The Enquiry Folder was not created" & vbCrLf & _
                        "Check your network connections.", "Enquiry Folder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        launchWizard = True

                    End If
                Catch ex As UnauthorizedAccessException
                    MsgBox("UnauthorizedAccessException = " & ex.Message)
                Catch ex As Exception
                    'Console.WriteLine("The process failed: {0}.", e.ToString())
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "The process failed: {0}." + ex.ToString()
                    'MsgBox(ex.Message)
                End Try

            End If

            If launchWizard = True Then
                Try
                    Cursor.Current = Cursors.WaitCursor
                    'launch WkShtWizForm1 and pass Reference Id
                    Dim wkShtWizFrm1 As New WkShtWizForm1
                    wkShtWizFrm1.PassVariable(thisEnquiryId)
                    wkShtWizFrm1.Show()
                    Cursor.Current = Cursors.Default
                    'Close this Form
                    Me.Close()
                Catch ex As Exception
                    MsgBox("Launching the Due Diligence wizard caused an error" & vbCrLf & ex.Message)
                End Try
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnWebApp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWebApp.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim progressStatus As Boolean = False
            Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
            'Dim result As DialogResult
            'leave if bindingsource.current is nothing
            If Not bindSrc2.Current Is Nothing Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Saving changed data."
                Cursor.Current = Cursors.WaitCursor
                'save changes
                Me.Validate()
                bindSrc2.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                Cursor.Current = Cursors.Default
                progressStatus = True
            Else
                progressStatus = False
            End If

            If progressStatus = True Then
                Cursor.Current = Cursors.WaitCursor
                'Activate the MembersCentre link
                Dim membersCentreUtilObject As New WebUtil
                membersCentreUtilObject.MCLink()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = membersCentreUtilObject.GetLabelForeColor
                ToolStripStatusLabel1.Text = membersCentreUtilObject.GetLabelText

            Else
                'MsgBox("No record to save, please enter information")
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No record to save, please enter information!"
            End If
            Cursor.Current = Cursors.Default

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FollowUpForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            'txtbxFollowUpComments.Size = New Size(formWidth - 54, formHeight - 292)
            dgvEnquiryComments.Size = New Size(formWidth - 40, formHeight - 268)
            btnWorksheetWizard.Location = New Point(400 - ((960 - formWidth) / 2), 12)

        End If
    End Sub

    'Private Sub EnquiryResultsComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnquiryResultsComboBox.SelectedIndexChanged
    '    'disable Handlers
    '    RemoveHandler EnquiryResultsComboBox.SelectedIndexChanged, AddressOf EnquiryResultsComboBox_SelectedIndexChanged
    '    Dim ProgressStatus As Boolean = False
    '    Try
    '        If EnquiryResultsComboBox.Text = "Enquiry/Application declined" Then
    '            'save current info
    '            ThisEnquiryResult = EnquiryResultsComboBox.Text
    '            'save changes
    '            Cursor.Current = Cursors.WaitCursor

    '            Me.Validate()
    '            Me.EnquiryBindingSource.EndEdit()
    '            Me.ClientBindingSource.EndEdit()
    '            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '            Cursor.Current = Cursors.Default

    '            Dim DeclinedFrm As New DeclinedForm
    '            DeclinedFrm.PassVariable(ThisEnquiryId, ThisClientSalutation, ThisEnquiryResult)
    '            DeclinedFrm.ShowDialog(Me)
    '            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

    '                DeclinedFrm.Dispose()

    '                ProgressStatus = True

    '            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
    '                DeclinedFrm.Dispose()
    '                'User choose Cancel, do nothing
    '                ProgressStatus = False
    '                'update Status Strip
    '                ToolStripStatusLabel1.ForeColor = Color.Black
    '                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
    '            End If
    '            'Progress Status
    '            If ProgressStatus = True Then

    '                Try
    '                    Cursor.Current = Cursors.WaitCursor
    '                    'launch  Standard Worksheet Form
    '                    Dim AppFrm As New AppForm
    '                    AppFrm.PassVariable(ThisEnquiryId)
    '                    AppFrm.Show()
    '                    Cursor.Current = Cursors.Default
    '                    'Close this Form
    '                    Me.Close()
    '                Catch ex As Exception
    '                    MsgBox(ex.Message)
    '                End Try
    '            Else
    '                'add Handlers
    '                AddHandler EnquiryResultsComboBox.SelectedIndexChanged, AddressOf EnquiryResultsComboBox_SelectedIndexChanged
    '            End If

    '        End If
    '    Catch ex As Exception
    '        MsgBox("EnquiryResultsComboBox_SelectedIndexChanged caused an error:" & vbCrLf & ex.Message)
    '    End Try
    'End Sub

    Private Sub FollowUpForm_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Dim meNewLocX As Integer
        Dim meNewLocY As Integer

        If Me.Location.X < 0 Then
            meNewLocX = 0
        Else
            meNewLocX = Me.Location.X
        End If
        If Me.Location.Y < 0 Then
            meNewLocY = 0
        Else
            meNewLocY = Me.Location.Y
        End If
        Me.Location = New Point(meNewLocX, meNewLocY)
    End Sub

    '******************************* MenuStrip code
#Region "MenuStrip"

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.EnquiryBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim editDetailsFrm As New EditDetailsForm
            editDetailsFrm.PassVariable(thisEnquiryId)
            'AddSecurityFrm.ShowDialog(Me)
            If (editDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                ''loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
                'Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
                ''get Dealer Id
                'ThisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
                ''*************** Get Dealer name
                'Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                'Dim connection2 As New SqlConnection(connectionString2)
                'Dim selectStatement2 As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & ThisDealerId & "'"
                ''MsgBox("Select Statement = " & selectStatement1)
                'Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                'Dim DealerTempDataAdapter As New SqlDataAdapter(selectCommand2)
                'Dim DealerTempDataSet As New DataSet
                'Dim DealerTempDataTable As New DataTable
                'Try
                '    'dumps results into datatable LoginDataTable
                '    DealerTempDataAdapter.Fill(DealerTempDataTable)
                '    'if no matching rows .....
                '    If DealerTempDataTable.Rows.Count = 0 Then
                '        MessageBox.Show("No Dealer Name, please try again.")
                '        'clear the dataTable and the Connect information
                '        DealerTempDataAdapter = Nothing
                '        DealerTempDataTable.Clear()
                '        'if there is a matching row
                '    ElseIf DealerTempDataTable.Rows.Count = 1 Then
                '        'get active value
                '        Dim DealerTempDataRow As DataRow = DealerTempDataTable.Rows(0)
                '        ThisDealerName = DealerTempDataRow.Item(0)
                '        'clear the dataTable and the Connect information
                '        DealerTempDataAdapter = Nothing
                '        DealerTempDataTable.Clear()
                '    End If
                '    'close the connection
                '    If connection2.State <> ConnectionState.Closed Then
                '        connection2.Close()
                '    End If

                'Catch ex As Exception
                '    MsgBox(ex.Message)
                'End Try
                ''MsgBox("Dealer Name = " & ThisDealerName)
                'lblDealerName.Text = ThisDealerName
                FormRefresh()

                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            editDetailsFrm.Dispose()
            'Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
            ''loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            'Me.ClientTableAdapter.FillByClientId(Me.EnquiryWorkSheetDataSet.Client, ThisClientId)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Checking Names."
        'Try
        '    'Check String has at least 2 Characters
        '    If ClientLastName.Length >= 2 Then
        '        'Get the String to search for and load Project wide variable
        '        Try
        '            SearchForLastNameLike = ClientLastName.Substring(0, 2) ' "%" added by procedure call
        '            'MsgBox(SearchForLastNameLike)
        '            'Call the Results form
        '            Dim CheckNamesResultsFrm As New CheckNamesResultsForm()
        '            CheckNamesResultsFrm.Show()
        '        Catch ex As Exception
        '            MsgBox(ex.Message)
        '        End Try
        '    Else
        '        'update Status Strip
        '        ToolStripStatusLabel1.ForeColor = Color.Red
        '        ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
        '    End If
        'Catch ex As Exception
        '    'update Status Strip
        '    ToolStripStatusLabel1.ForeColor = Color.Red
        '    ToolStripStatusLabel1.Text = "ERROR:  " & ex.Message
        '    'MsgBox(ex.Message)

        'End Try
        Try
            'Check String has at least 2 Characters
            If contactLastName.Length >= 2 Then
                'Get the String to search for and load Project wide variable
                Try
                    'Call DisplayFinPowerNamesForm
                    Dim displayFinPowerNamesFrm As New DisplayFinPowerNamesForm
                    displayFinPowerNamesFrm.PassVariable(contactLastName.Substring(0, 2))
                    displayFinPowerNamesFrm.Show()
                Catch ex As Exception
                    MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
                End Try
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
            End If
        Catch ex As Exception
            MsgBox("btnCheckNames ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'Try
        '    'Check String has at least 2 Characters
        '    If ClientLastName.Length >= 2 Then
        '        'Get the String to search for and load Project wide variable
        '        Try
        '            SearchForLastNameLike = ClientLastName.Substring(0, 2) ' "%" added by procedure call
        '            'MsgBox(SearchForLastNameLike)
        '            'Call the Results form
        '            Dim CheckNamesEnquiryResultsFrm As New CheckNamesEnquiryResultsForm
        '            CheckNamesEnquiryResultsFrm.PassVariable(SearchForLastNameLike)
        '            CheckNamesEnquiryResultsFrm.Show()
        '        Catch ex As Exception
        '            MsgBox(ex.Message)
        '        End Try
        '    Else
        '        'update Status Strip
        '        ToolStripStatusLabel1.ForeColor = Color.Red
        '        ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
        'Get Names from TrueTrack Enquiries including Archived Enquiries
        Try
            'Check String has at least 2 Characters
            If contactLastName.Length >= 2 Then
                'Get the String to search for and load Project wide variable
                Try
                    'Call the Results form
                    Dim displayAllNamesFrm As New DisplayAllNamesForm
                    displayAllNamesFrm.PassVariable(contactLastName.Substring(0, 2))
                    displayAllNamesFrm.Show()
                Catch ex As Exception
                    MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
                End Try
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
            End If
        Catch ex As Exception
            MsgBox("btnCheckEnquiries ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub
    '************ end of Check Names

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim displayDealersFrm As New DisplayDealersForm
            displayDealersFrm.PassVariable(thisDealerId)
            displayDealersFrm.ShowDialog()
            displayDealersFrm.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        FormRefresh()
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim addCommentFrm As New AddCommentForm()
        addCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        addCommentFrm.Dispose()

       
    End Sub

    Private Sub EditCandDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCandDToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim editDetailsFrm As New EditDetailsForm
            editDetailsFrm.PassVariable(thisEnquiryId)
            'AddSecurityFrm.ShowDialog(Me)
            If (editDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                ''loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
                'Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
                ''get Dealer Id
                'ThisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
                ''*************** Get Dealer name
                'Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                'Dim connection2 As New SqlConnection(connectionString2)
                'Dim selectStatement2 As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & ThisDealerId & "'"
                ''MsgBox("Select Statement = " & selectStatement1)
                'Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                'Dim DealerTempDataAdapter As New SqlDataAdapter(selectCommand2)
                'Dim DealerTempDataSet As New DataSet
                'Dim DealerTempDataTable As New DataTable
                'Try
                '    'dumps results into datatable LoginDataTable
                '    DealerTempDataAdapter.Fill(DealerTempDataTable)
                '    'if no matching rows .....
                '    If DealerTempDataTable.Rows.Count = 0 Then
                '        MessageBox.Show("No Dealer Name, please try again.")
                '        'clear the dataTable and the Connect information
                '        DealerTempDataAdapter = Nothing
                '        DealerTempDataTable.Clear()
                '        'if there is a matching row
                '    ElseIf DealerTempDataTable.Rows.Count = 1 Then
                '        'get active value
                '        Dim DealerTempDataRow As DataRow = DealerTempDataTable.Rows(0)
                '        ThisDealerName = DealerTempDataRow.Item(0)
                '        'clear the dataTable and the Connect information
                '        DealerTempDataAdapter = Nothing
                '        DealerTempDataTable.Clear()
                '    End If
                '    'close the connection
                '    If connection2.State <> ConnectionState.Closed Then
                '        connection2.Close()
                '    End If

                'Catch ex As Exception
                '    MsgBox(ex.Message)
                'End Try
                ''MsgBox("Dealer Name = " & ThisDealerName)
                'lblDealerName.Text = ThisDealerName

                FormRefresh()

                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            editDetailsFrm.Dispose()

            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim addCommentFrm As New AddCommentForm()
        addCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        addCommentFrm.Dispose()
    End Sub

    Private Sub EmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailToolStripMenuItem.Click
        'disable Handlers
        'RemoveHandler EnquiryResultsComboBox.SelectedIndexChanged, AddressOf EnquiryResultsComboBox_SelectedIndexChanged
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim emailFrm As New EmailForm
        emailFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If emailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        emailFrm.Dispose()

    End Sub

    Private Sub ApprovalFormToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem1.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub


    Private Sub WWWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WWWToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'launch internet browser for web application
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Launching web browser."
            System.Diagnostics.Process.Start(My.Settings.YFL_website)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            Cursor.Current = Cursors.Default
            MsgBox("Launching web browser problem: " & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'Get Names from TrueTrack Enquiries including Archived Enquiries
        Try
            'Check String has at least 2 Characters
            If contactLastName.Length >= 2 Then
                'Get the String to search for and load Project wide variable
                Try
                    'Call the Results form
                    Dim displayAllNamesFrm As New DisplayAllNamesForm
                    displayAllNamesFrm.PassVariable(contactLastName.Substring(0, 2))
                    displayAllNamesFrm.Show()
                Catch ex As Exception
                    MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
                End Try
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
            End If
        Catch ex As Exception
            MsgBox("btnCheckEnquiries ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

#End Region
    '******************************* end of MenuStrip code

    Private Sub EndEnquiryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EndEnquiryToolStripMenuItem.Click
        Dim progressStatus As Boolean = False
        Try
            'save changes
            Cursor.Current = Cursors.WaitCursor
            thisEnquiryResult = "Enquiry Ended"

            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            Cursor.Current = Cursors.Default

            Dim declinedFrm As New DeclinedForm
            declinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            declinedFrm.ShowDialog(Me)
            If declinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                declinedFrm.Dispose()

                progressStatus = True

            ElseIf declinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                declinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
            End If
            'Progress Status
            If progressStatus = True Then

                Try
                    Cursor.Current = Cursors.WaitCursor
                    'launch  Standard Worksheet Form
                    Dim appFrm As New AppForm
                    appFrm.PassVariable(thisEnquiryId, False)
                    appFrm.Show()
                    Cursor.Current = Cursors.Default
                    'Close this Form
                    Me.Close()
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                'add Handlers
                'AddHandler EnquiryResultsComboBox.SelectedIndexChanged, AddressOf EnquiryResultsComboBox_SelectedIndexChanged
            End If

        Catch ex As Exception
            MsgBox("End Enquiry caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    '**************************** Drag & Drop Folders
    Private Sub btnDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocs.Click
        Dim wkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = wkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        Else
            Try
                'create directory
                IO.Directory.CreateDirectory(fname)
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
                'refresh form
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
            Catch ex As System.IO.DirectoryNotFoundException
                ' Let the user know that the directory did not exist.
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
    End Sub

    Private Sub btnDocs_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Drag and drop successful.    Status: Ready"
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox("Drag and Drop caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnDocs_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnViewDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDocs.Click
        Dim wkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = wkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Not Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Shared Folder does not exist!."
        Else
            Dim viewDocsFrm As New ViewDocsForm
            viewDocsFrm.PassVariable(thisEnquiryCode, thisContactDisplayName)
            viewDocsFrm.ShowDialog(Me)
            If viewDocsFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                viewDocsFrm.Dispose()
            Else
                viewDocsFrm.Dispose()
            End If
        End If
    End Sub
    '**************************** Drag & Drop Folders

#Region "Enquiry number label"
    Private Sub lblEnquiryNumberValue_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lblEnquiryNumberValue.MouseDoubleClick
        lblEnquiryNumberValue.BackColor = Color.LightPink
        Clipboard.SetDataObject(lblEnquiryNumberValue.Text, False)

        'lblEnquiryNumberValue.BackColor = Color.LightBlue
    End Sub


    Private Sub lblEnquiryNumberValue_MouseHover(sender As Object, e As EventArgs) Handles lblEnquiryNumberValue.MouseHover
        Cursor.Current = Cursors.Hand
        lblEnquiryNumberValue.BackColor = Color.LightBlue
    End Sub

    Private Sub lblEnquiryNumberValue_MouseLeave(sender As Object, e As EventArgs) Handles lblEnquiryNumberValue.MouseLeave
        Cursor.Current = Cursors.Default
        lblEnquiryNumberValue.BackColor = SystemColors.Control
    End Sub
#End Region


    Private Sub FollowUpForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        
    End Sub

    Private Sub btnAddComment_Click(sender As Object, e As EventArgs) Handles btnAddComment.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim addCommentFrm As New AddCommentForm()
        addCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        addCommentFrm.Dispose()

    End Sub

    Private Sub dgvEnquiryComments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEnquiryComments.DoubleClick

        Dim selectedRowCommentId As Integer = 0
        Dim selectedRowView As Data.DataRowView
        Dim selectedRow As EnquiryWorkSheetDataSet.EnquiryCommentRow
        selectedRowView = CType(EnquiryCommentBS.Current, System.Data.DataRowView)
        If selectedRowView.Row IsNot Nothing Then
            selectedRow = selectedRowView.Row
            selectedRowCommentId = selectedRow.Id

            'launch Add Comment Form
            Dim addCommentFrm As New AddCommentForm()
            If ApplicationAccessRights.CanEditComments Then
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.Edit)
            Else
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.ReadOnly)
            End If
            ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
            If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'Call the function you used to populate the data grid..
                Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment added."
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment cancelled."
            End If
            'When Add Comment Form closed
            addCommentFrm.Dispose()

        End If

    End Sub
End Class