﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NetworkForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NetworkForm))
        Me.pnlPing = New System.Windows.Forms.Panel()
        Me.btnPing = New System.Windows.Forms.Button()
        Me.txtbxPing = New System.Windows.Forms.TextBox()
        Me.StatusStripPingMessage = New System.Windows.Forms.StatusStrip()
        Me.StatusStripPingLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblPing = New System.Windows.Forms.Label()
        Me.pnlPing.SuspendLayout()
        Me.StatusStripPingMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlPing
        '
        Me.pnlPing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPing.Controls.Add(Me.btnPing)
        Me.pnlPing.Controls.Add(Me.txtbxPing)
        Me.pnlPing.Controls.Add(Me.StatusStripPingMessage)
        Me.pnlPing.Controls.Add(Me.lblPing)
        Me.pnlPing.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlPing.Location = New System.Drawing.Point(0, 0)
        Me.pnlPing.Name = "pnlPing"
        Me.pnlPing.Size = New System.Drawing.Size(704, 78)
        Me.pnlPing.TabIndex = 0
        '
        'btnPing
        '
        Me.btnPing.Location = New System.Drawing.Point(341, 23)
        Me.btnPing.Name = "btnPing"
        Me.btnPing.Size = New System.Drawing.Size(75, 23)
        Me.btnPing.TabIndex = 3
        Me.btnPing.Text = "Ping"
        Me.btnPing.UseVisualStyleBackColor = True
        '
        'txtbxPing
        '
        Me.txtbxPing.Location = New System.Drawing.Point(15, 25)
        Me.txtbxPing.Name = "txtbxPing"
        Me.txtbxPing.Size = New System.Drawing.Size(320, 20)
        Me.txtbxPing.TabIndex = 2
        '
        'StatusStripPingMessage
        '
        Me.StatusStripPingMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusStripPingLabel})
        Me.StatusStripPingMessage.Location = New System.Drawing.Point(0, 54)
        Me.StatusStripPingMessage.Name = "StatusStripPingMessage"
        Me.StatusStripPingMessage.Size = New System.Drawing.Size(702, 22)
        Me.StatusStripPingMessage.TabIndex = 1
        '
        'StatusStripPingLabel
        '
        Me.StatusStripPingLabel.Name = "StatusStripPingLabel"
        Me.StatusStripPingLabel.Size = New System.Drawing.Size(80, 17)
        Me.StatusStripPingLabel.Text = "Status:  Ready"
        '
        'lblPing
        '
        Me.lblPing.AutoSize = True
        Me.lblPing.Location = New System.Drawing.Point(12, 9)
        Me.lblPing.Name = "lblPing"
        Me.lblPing.Size = New System.Drawing.Size(323, 13)
        Me.lblPing.TabIndex = 0
        Me.lblPing.Text = "Enter the URL, computer name, or IP number of the server to ping. "
        '
        'NetworkForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 564)
        Me.Controls.Add(Me.pnlPing)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NetworkForm"
        Me.Text = "Network Testing"
        Me.pnlPing.ResumeLayout(False)
        Me.pnlPing.PerformLayout()
        Me.StatusStripPingMessage.ResumeLayout(False)
        Me.StatusStripPingMessage.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlPing As System.Windows.Forms.Panel
    Friend WithEvents StatusStripPingMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents lblPing As System.Windows.Forms.Label
    Friend WithEvents btnPing As System.Windows.Forms.Button
    Friend WithEvents txtbxPing As System.Windows.Forms.TextBox
    Friend WithEvents StatusStripPingLabel As System.Windows.Forms.ToolStripStatusLabel
End Class
