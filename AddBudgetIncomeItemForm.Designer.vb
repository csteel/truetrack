﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddBudgetIncomeItemForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddBudgetIncomeItemForm))
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.BudgetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.txtbxBudgetAmount = New System.Windows.Forms.TextBox()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAddBudgetItem = New System.Windows.Forms.Button()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.lblPer = New System.Windows.Forms.Label()
        Me.cmbxPeriod = New System.Windows.Forms.ComboBox()
        Me.lblequals = New System.Windows.Forms.Label()
        Me.lblMonthlyValue = New System.Windows.Forms.Label()
        Me.lblPerMonth = New System.Windows.Forms.Label()
        Me.lblComments = New System.Windows.Forms.Label()
        Me.BudgetTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter()
        Me.txtbxComments = New AppWhShtB.SpellBox()
        Me.txtbxDescription = New AppWhShtB.SpellBox()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BudgetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BudgetBindingSource
        '
        Me.BudgetBindingSource.DataMember = "Budget"
        Me.BudgetBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Description"
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.Location = New System.Drawing.Point(14, 49)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(43, 13)
        Me.lblAmount.TabIndex = 45
        Me.lblAmount.Text = "Amount"
        '
        'txtbxBudgetAmount
        '
        Me.txtbxBudgetAmount.Location = New System.Drawing.Point(63, 46)
        Me.txtbxBudgetAmount.Name = "txtbxBudgetAmount"
        Me.txtbxBudgetAmount.Size = New System.Drawing.Size(100, 20)
        Me.txtbxBudgetAmount.TabIndex = 2
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(101, 147)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 11
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        Me.btnUpdate.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.Location = New System.Drawing.Point(396, 147)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnAddBudgetItem
        '
        Me.btnAddBudgetItem.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddBudgetItem.Location = New System.Drawing.Point(19, 147)
        Me.btnAddBudgetItem.Name = "btnAddBudgetItem"
        Me.btnAddBudgetItem.Size = New System.Drawing.Size(75, 23)
        Me.btnAddBudgetItem.TabIndex = 10
        Me.btnAddBudgetItem.Text = "Add"
        Me.btnAddBudgetItem.UseVisualStyleBackColor = False
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'lblPer
        '
        Me.lblPer.AutoSize = True
        Me.lblPer.Location = New System.Drawing.Point(169, 49)
        Me.lblPer.Name = "lblPer"
        Me.lblPer.Size = New System.Drawing.Size(22, 13)
        Me.lblPer.TabIndex = 68
        Me.lblPer.Text = "per"
        '
        'cmbxPeriod
        '
        Me.cmbxPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxPeriod.FormattingEnabled = True
        Me.cmbxPeriod.Items.AddRange(New Object() {"weekly", "fortnightly", "4 weekly", "monthly"})
        Me.cmbxPeriod.Location = New System.Drawing.Point(197, 46)
        Me.cmbxPeriod.Name = "cmbxPeriod"
        Me.cmbxPeriod.Size = New System.Drawing.Size(83, 21)
        Me.cmbxPeriod.TabIndex = 3
        '
        'lblequals
        '
        Me.lblequals.AutoSize = True
        Me.lblequals.Location = New System.Drawing.Point(286, 49)
        Me.lblequals.Name = "lblequals"
        Me.lblequals.Size = New System.Drawing.Size(38, 13)
        Me.lblequals.TabIndex = 70
        Me.lblequals.Text = "equals"
        '
        'lblMonthlyValue
        '
        Me.lblMonthlyValue.AutoSize = True
        Me.lblMonthlyValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BudgetBindingSource, "IncomeValue", True))
        Me.lblMonthlyValue.Location = New System.Drawing.Point(330, 49)
        Me.lblMonthlyValue.Name = "lblMonthlyValue"
        Me.lblMonthlyValue.Size = New System.Drawing.Size(34, 13)
        Me.lblMonthlyValue.TabIndex = 71
        Me.lblMonthlyValue.Text = "$0.00"
        '
        'lblPerMonth
        '
        Me.lblPerMonth.AutoSize = True
        Me.lblPerMonth.Location = New System.Drawing.Point(405, 49)
        Me.lblPerMonth.Name = "lblPerMonth"
        Me.lblPerMonth.Size = New System.Drawing.Size(54, 13)
        Me.lblPerMonth.TabIndex = 72
        Me.lblPerMonth.Text = "per month"
        '
        'lblComments
        '
        Me.lblComments.AutoSize = True
        Me.lblComments.Location = New System.Drawing.Point(14, 80)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(56, 13)
        Me.lblComments.TabIndex = 74
        Me.lblComments.Text = "Comments"
        '
        'BudgetTableAdapter
        '
        Me.BudgetTableAdapter.ClearBeforeFill = True
        '
        'txtbxComments
        '
        Me.txtbxComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BudgetBindingSource, "Comments", True))
        Me.txtbxComments.IsReadOnly = False
        Me.txtbxComments.Location = New System.Drawing.Point(15, 96)
        Me.txtbxComments.MaxLength = 0
        Me.txtbxComments.MultiLine = True
        Me.txtbxComments.Name = "txtbxComments"
        Me.txtbxComments.SelectedText = ""
        Me.txtbxComments.SelectionLength = 0
        Me.txtbxComments.SelectionStart = 0
        Me.txtbxComments.Size = New System.Drawing.Size(452, 45)
        Me.txtbxComments.TabIndex = 4
        Me.txtbxComments.WordWrap = True
        Me.txtbxComments.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxDescription
        '
        Me.txtbxDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BudgetBindingSource, "Description", True))
        Me.txtbxDescription.IsReadOnly = False
        Me.txtbxDescription.Location = New System.Drawing.Point(15, 20)
        Me.txtbxDescription.MaxLength = 100
        Me.txtbxDescription.Name = "txtbxDescription"
        Me.txtbxDescription.SelectedText = ""
        Me.txtbxDescription.SelectionLength = 0
        Me.txtbxDescription.SelectionStart = 0
        Me.txtbxDescription.Size = New System.Drawing.Size(452, 20)
        Me.txtbxDescription.TabIndex = 1
        Me.txtbxDescription.Child = New System.Windows.Controls.TextBox()
        '
        'AddBudgetIncomeItemForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(483, 182)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtbxDescription)
        Me.Controls.Add(Me.txtbxComments)
        Me.Controls.Add(Me.lblComments)
        Me.Controls.Add(Me.lblPerMonth)
        Me.Controls.Add(Me.lblMonthlyValue)
        Me.Controls.Add(Me.lblequals)
        Me.Controls.Add(Me.cmbxPeriod)
        Me.Controls.Add(Me.lblPer)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAddBudgetItem)
        Me.Controls.Add(Me.txtbxBudgetAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.Label2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AddBudgetIncomeItemForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Budget Income Item Form"
        Me.TopMost = True
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BudgetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtbxBudgetAmount As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnAddBudgetItem As System.Windows.Forms.Button
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents lblPer As System.Windows.Forms.Label
    Friend WithEvents cmbxPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblequals As System.Windows.Forms.Label
    Friend WithEvents lblMonthlyValue As System.Windows.Forms.Label
    Friend WithEvents lblPerMonth As System.Windows.Forms.Label
    Friend WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents BudgetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BudgetTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter
    Friend WithEvents txtbxComments As AppWhShtB.SpellBox
    Friend WithEvents txtbxDescription As AppWhShtB.SpellBox
End Class
