﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RtfMsgBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RtfMsgBox))
        Me.btnOK = New System.Windows.Forms.Button()
        Me.rtbMsg = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.YellowGreen
        Me.btnOK.Location = New System.Drawing.Point(214, 213)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'rtbMsg
        '
        Me.rtbMsg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbMsg.Location = New System.Drawing.Point(13, 13)
        Me.rtbMsg.Name = "rtbMsg"
        Me.rtbMsg.Size = New System.Drawing.Size(459, 194)
        Me.rtbMsg.TabIndex = 1
        Me.rtbMsg.Text = ""
        '
        'RtfMsgBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(484, 248)
        Me.ControlBox = False
        Me.Controls.Add(Me.rtbMsg)
        Me.Controls.Add(Me.btnOK)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(960, 960)
        Me.MinimumSize = New System.Drawing.Size(250, 250)
        Me.Name = "RtfMsgBox"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RtfMsgBox"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents rtbMsg As System.Windows.Forms.RichTextBox
End Class
