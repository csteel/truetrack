﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

Public Class MembersCentreArchive
    Private request As WebRequest
    Private _service = New ExchangeService(ExchangeVersion.Exchange2007_SP1)
    Private findResults As FindItemsResults(Of Item)
    Private User As String
    Private PassKey As String
    Private ArchiveEmailId As String
    Private Const CONST_FETCH_MAX_VALUE As Integer = 100 'No. of mails displayed by the search at one go
    Private generateTraceLog As Boolean = False


    Private Sub InitiateExchangeService()
        ArchiveEmailId = My.Settings.ArchiveMailId
        _service.Credentials = New WebCredentials(User, PassKey)
        '_service.AutodiscoverUrl(User & "@yesfinance.co.nz")
        Dim myUri As New Uri(My.Settings.ExchangeServerUri)
        _service.Url = myUri

        'Set the tracing properties to enable tracing
        If generateTraceLog = True Then
            Dim tc As New EWSTrace.TraceListener
            _service.TraceFlags = TraceFlags.All
            _service.TraceEnabled = True
            _service.TraceListener = tc
        End If
        

        ServicePointManager.ServerCertificateValidationCallback = _
                  Function(se As Object, _
                  cert As System.Security.Cryptography.X509Certificates.X509Certificate, _
                  chain As System.Security.Cryptography.X509Certificates.X509Chain, _
                  sslerror As System.Net.Security.SslPolicyErrors) True



    End Sub


    Function getUserData() As Boolean
        Try
            User = WebUtil.UserId
            PassKey = WebUtil.PassKey(User)

            Return 1
        Catch ex As Exception
            MessageBox.Show("Error in retrieving user information." & vbCrLf & ex.Message, "Applications Archive", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return 0

        End Try

    End Function

    ''' <summary>
    ''' Searches and binds
    ''' </summary>
    ''' <remarks></remarks>
    Sub searchEWS()
        Try
            ' Connect server
            InitiateExchangeService()
            '==============================================
            'Build Search Filter Criteria
            '==============================================
            Dim SearchFilterCollection As New List(Of SearchFilter)
            If Not txtApplicationCode.Text.Trim = "" Then
                SearchFilterCollection.Add(New SearchFilter.ContainsSubstring(ItemSchema.Subject, txtApplicationCode.Text.Trim))
            End If
            If Not txtLastName.Text.Trim = "" Then
                SearchFilterCollection.Add(New SearchFilter.ContainsSubstring(ItemSchema.Subject, txtLastName.Text.Trim))
            End If

            ' Create the search filter.
            Dim searchFilterArchiveMail As SearchFilter = New SearchFilter.SearchFilterCollection(LogicalOperator.Or, SearchFilterCollection.ToArray())

            'Instantiate ArchiveMail folder
            Dim InboxId As FolderId
            InboxId = New FolderId(WellKnownFolderName.Inbox, ArchiveEmailId)

            'Apply Search criteria in Exchange Folder; returns MAX 500 applications from Exchange
            findResults = _service.FindItems(InboxId, searchFilterArchiveMail, New ItemView(CONST_FETCH_MAX_VALUE))

            'get the Body of the mail
            If findResults.Items.Count > 0 Then
                _service.LoadPropertiesForItems(findResults, New PropertySet(
                    BasePropertySet.FirstClassProperties,
                    ItemSchema.Subject, ItemSchema.Categories, ItemSchema.Body))
            End If

            '==============================================
            'Build datatable and bind to grid
            '==============================================
            dgvMails.DataSource = BuildDataTable()

            For Each row As DataGridViewRow In dgvMails.Rows
                row.Height = 20
                row.Resizable = DataGridViewTriState.False
            Next

            lblFormStatus2.Text = "Search returned " & dgvMails.Rows.Count & " records"

            'Formatting the Grid
            SetDatagridColumns()
        Catch ex As Exception
            If generateTraceLog = False Then
                MessageBox.Show("Error in retrieving Application information." & vbCrLf & ex.Message & vbCrLf & vbCrLf & "Try again to generate a Trace Log", "Applications Archive", MessageBoxButtons.OK, MessageBoxIcon.Error)
                generateTraceLog = True
            Else
                Dim logPath As String = System.AppDomain.CurrentDomain.BaseDirectory
                logPath = logPath + "EWSLog.txt"
                MessageBox.Show("Error in retrieving Application information." & vbCrLf & ex.Message & vbCrLf & vbCrLf & "A Trace Log was generated" & vbCrLf & "Log location: " & logPath, "Applications Archive", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            
        End Try
    End Sub

    'Retrieve data from search Results and builds dataTable
    Private Function BuildDataTable() As DataTable

        Dim table As New DataTable
        ' Create four typed columns in the DataTable.
        table.Columns.Add("Date", GetType(String))
        table.Columns.Add("Application Subject", GetType(String))

        For Each item As Item In findResults.Items
            If TypeOf item Is EmailMessage Then
                table.Rows.Add(TryCast(item, EmailMessage).DateTimeCreated, TryCast(item, EmailMessage).Subject)
            End If

        Next

        Return table

    End Function


    'Formatting the Grid
    Private Sub SetDatagridColumns()

        Dim column As DataGridViewColumn
        Dim style As DataGridViewCellStyle = New DataGridViewCellStyle()

        dgvMails.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvMails.MultiSelect = False

        style.Alignment = DataGridViewContentAlignment.MiddleCenter
        style.ForeColor = Color.Black
        style.BackColor = System.Drawing.SystemColors.Window
        style.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        dgvMails.ColumnHeadersVisible = True
        dgvMails.AutoGenerateColumns = False

        For Each column In dgvMails.Columns
            column.HeaderCell.Style = style
        Next

        If dgvMails.Columns.Count > 1 Then
            dgvMails.ColumnHeadersVisible = True
            dgvMails.Columns(0).HeaderText = "Date"
            dgvMails.Columns(1).HeaderText = "Subject"
            dgvMails.Columns(0).ReadOnly = True
            dgvMails.Columns(1).ReadOnly = True
            dgvMails.Columns(0).Width = (dgvMails.Width / 6)
            dgvMails.Columns(1).Width = ((dgvMails.Width / 6) * 5) - If(dgvMails.Rows.Count > 7, 20, 4)
        End If

    End Sub


    'Apply Search Criteria
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            RemoveHandler btnSearch.Click, AddressOf btnSearch_Click
            Cursor.Current = Cursors.WaitCursor
            Application.DoEvents()
            Me.WebBrowserArchive.DocumentText = ""
            dgvMails.DataSource = Nothing

            If txtApplicationCode.Text.Trim = "" And txtLastName.Text.Trim = "" Then
                MessageBox.Show("Please enter search criteria")
                Exit Sub
            End If
            lblFormStatus2.Text = "Fetching applications"

            Application.DoEvents()

            searchEWS()

        Finally
            AddHandler btnSearch.Click, AddressOf btnSearch_Click
            Cursor.Current = Cursors.Default
        End Try

    End Sub

    Private Sub MembersCentreArchive_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'get minimumSize
        Dim minFormHeight As Integer = Me.MinimumSize.Height - 1
        Dim minFormWidth As Integer = Me.MinimumSize.Width - 1
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            'resize DataGridView
            WebBrowserPanel.Size = New Size(formWidth - 22, formHeight - 296)
        End If
    End Sub



    Private Sub MembersCentreArchive_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.Show()
            Application.DoEvents()
            'txtApplicationCode.Text = "YFLM001496"
            getUserData()

            If Not txtApplicationCode.Text.Trim = "" Then
                lblFormStatus2.Text = "Fetching applications"
                Application.DoEvents()
                searchEWS()
            End If

        Finally

            Me.Cursor = Cursors.Default

        End Try
    End Sub

    'Shows the Body of the mail in the WebBrowser
    Private Sub dgvMails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMails.DoubleClick
        Dim iKount As Integer = 0

        If dgvMails.CurrentRow Is Nothing Then
            Me.WebBrowserArchive.DocumentText = ""
        Else
            iKount = dgvMails.CurrentRow.Index
            Dim selectedItem As Item = findResults.Items(iKount)
            Me.WebBrowserArchive.DocumentText = TryCast(selectedItem, EmailMessage).Body.Text
        End If

    End Sub


End Class
