﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Drawing.Printing 'for printing operations

Public Class WkShtWizForm1 'Progress Due Diligence

    Dim thisEnquiryId As Integer
    Dim thisEnquiryCode As String
    Dim thisWizardStatus As Integer
    Dim thisApplicationCode As String = ""
    Dim thisEnquiryType As Integer
    Dim thisIsArchived As Boolean 'non-destructive archive
    Dim qrgStatus As Integer 'set QRG Status for colour
    Dim thisContactDisplayName As String
    Dim contactName As String
    Dim contactLastName As String
    Dim contactAddress As String
    Dim thisDealerId As String
    Dim thisDealerName As String
    Dim thisEnquiryManagerId As Integer
    Dim thisEnquiryResult As String = ""
    Dim progressStatus As Boolean = False
    Dim formTitle As String = "Due-diligence Main" 'Me.Text
    Dim appCodeChanged As Boolean = False
    Dim docsReceived As Boolean = False
    Dim thisCurrentStatus As String = String.Empty
    'define print page info
    Private PrintPageSettings As New PageSettings
    Private StringToPrint As String
    Private PrintFont As New Font("Arial", 8)
    Private FinishedString As String
    'Edit function
    Dim detailsEditToggle As Boolean = False
    'user settings
    Dim newLocX As Integer
    Dim newLocY As Integer
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Loads form with parameters from PreliminaryForm1
    Friend Async Sub PassVariable(ByVal loadEnquiryVar As Integer)
        SuspendLayout()
        'Remove Handlers
        RemoveHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged
        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)
        Dim compareResultTask As Threading.Tasks.Task(Of TrackResult)

        Try
            ' setup the reference variables
            thisEnquiryId = loadEnquiryVar
            'set the form tag with EnquiryId
            Me.Tag = thisEnquiryId
            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, loadEnquiryVar)
            thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
            'get EnquiryManagerId
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            'get type of Enquiry
            thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher   
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                lblLoanAmount.Visible = False
            Else
                lblLoanAmount.Visible = True
            End If
            'set EnquiryType label
            lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
            'Load User table
            UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            'get UserName
            lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
            'Get Wizard Status
            thisWizardStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("WizardStatus")
            'get Application Code
            thisApplicationCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ApplicationCode")
            'get Dealer ID
            thisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
            docsReceived = If(IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DocsReceived")), False, EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DocsReceived"))
            thisCurrentStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentStatus")
            thisIsArchived = If(IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("IsArchived")), False, EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("IsArchived"))

            'load the Enquiry Comments
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, loadEnquiryVar)
            '************************* Compare status with OAC status
            If Not IsDBNull(thisApplicationCode) And Not thisApplicationCode = String.Empty Then
                Dim onlineApplicationService As New OnlineApplication
                'await later in the sub
                Try
                    compareResultTask = onlineApplicationService.CompareStatus(thisApplicationCode, thisCurrentStatus, thisIsArchived)
                Catch ex As Exception
                    Log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MessageBox.Show(ex.Message, "Status comparison", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
            '************************* Set LoanReason display options
            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurrentLoan.Visible = True
                lblCurrentLoanAmt.Visible = True
            Else
                lblCurrentLoan.Visible = False
                lblCurrentLoanAmt.Visible = False
            End If

            '************************* Get Dealer name
            Try
                Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection As New SqlConnection(connectionString)
                Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & thisDealerId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand As New SqlCommand(selectStatement, connection)
                Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand)
                Dim dealerTempDataSet As New DataSet
                Dim dealerTempDataTable As New DataTable
                'dumps results into datatable LoginDataTable
                dealerTempDataAdapter.Fill(dealerTempDataTable)
                'if no matching rows .....
                If dealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf dealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                    thisDealerName = dealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    lblDealerName.Text = thisDealerName
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                Log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("Get Dealer name caused an error:" & vbCrLf & ex.Message)
            End Try

            '************************* set documents button visibility
            'Get String to WorkSheet Drive
            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
            Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
            'Check directory exists
            If Directory.Exists(folderPath) Then
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
            Else
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
                'deactivate drag and drop
                Me.btnDocs.AllowDrop = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Path to Shared Folder does not exist! Check your network connections."
            End If

            '************************* Get Contact Display Name
            thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
            '************************* Set form title
            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & thisContactDisplayName & " - " & formTitle
                Me.BackColor = Color.MistyRose
            Else
                Me.Text = thisContactDisplayName & " - " & formTitle
            End If
            '************************* Cannotate Contact Name           
            contactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
            contactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & contactLastName
            'set lblContactName
            lblContactName.Text = contactName
            'Contact address
            contactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
            lblContactAddress.Text = contactAddress

            '************************* Set btnGenAppCode state
            If Not thisApplicationCode = "" Then
                'disable button
                btnGenAppCode.Enabled = False
            End If
            txtbxAppCodeValue.Text = thisApplicationCode
            '************************* mainCustomerType
            'set datasource for combo box using the Enum members 05/07/2016 Christopher
            cmbxMainCustType.DisplayMember = "Value"
            cmbxMainCustType.DataSource = MyEnums.ToList(GetType(MyEnums.mainCustomerType))
            If Not IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")) Then
                cmbxMainCustType.SelectedIndex = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")
            Else
                cmbxMainCustType.SelectedIndex = -1
            End If

            '*****************************  get QRGList status
            Try
                Dim connectionString1 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection1 As New SqlConnection(connectionString1)
                Dim selectStatement1 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement1)
                Dim selectCommand1 As New SqlCommand(selectStatement1, connection1)
                Dim qrgListTempDataAdapter As New SqlDataAdapter(selectCommand1)
                Dim qrgListTempDataSet As New DataSet
                Dim qrgListTempDataTable As New DataTable

                'dumps results into datatable LoginDataTable
                qrgListTempDataAdapter.Fill(qrgListTempDataTable)
                'if no matching rows .....
                If qrgListTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("ERROR: No QRG List, please try again.")
                    'clear the dataTable and the Connect information
                    qrgListTempDataAdapter = Nothing
                    qrgListTempDataTable.Clear()
                    'if there is a matching row
                ElseIf qrgListTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim qrgListTempDataRow As DataRow = qrgListTempDataTable.Rows(0)
                    qrgStatus = qrgListTempDataRow.Item(0)
                    'MsgBox("QRG Status = " & QRGStatus)
                    'clear the dataTable and the Connect information
                    qrgListTempDataAdapter = Nothing
                    qrgListTempDataTable.Clear()
                End If
                'close the connection
                If connection1.State <> ConnectionState.Closed Then
                    connection1.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("get QRGList status caused an error:" & vbCrLf & ex.Message)
            End Try

            '************************* set QRGStatus button colour
            Select Case qrgStatus
                Case 0
                    btnQRGList.BackColor = Color.Transparent
                Case 1
                    btnQRGList.BackColor = Color.Tomato
                Case 2
                    btnQRGList.BackColor = Color.LightGreen
                Case Else
                    btnQRGList.BackColor = Color.Transparent
            End Select

            '************************* check if Split Loan Drawdown
            If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                btnNext.Enabled = False
                btnFinish.Enabled = True
            Else
                btnNext.Enabled = True
                btnFinish.Enabled = False
            End If

            '************************* Set permissions
            'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
            Select Case LoggedinPermissionLevel
                Case Is = 1 'Readonly
                    UpdateAllToolStripMenuItem.Visible = False
                    btnSaveAndExit.Enabled = False
                    btnGenAppCode.Enabled = False
                    btnAddComment.Enabled = False
                    btnFinish.Enabled = False
                    btnDocs.Enabled = False
                    cmbxMainCustType.Enabled = False
                    txtbxAppCodeValue.ReadOnly = True

                Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    btnGenAppCode.Enabled = False
                    btnFinish.Enabled = False
                    cmbxMainCustType.Enabled = False
                    txtbxAppCodeValue.ReadOnly = True

                Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                    'Can see /use Archive Enquiry in File menu for their user status
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEnquiryToolStripMenuItem.Visible = True
                    DocumentsReceivedToolStripMenuItem.Visible = True
                    DocsReceivedImg.Visible = True
                    If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                        btnFinish.Enabled = True
                    End If

                Case Is = 4
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEnquiryToolStripMenuItem.Visible = True
                    DocumentsReceivedToolStripMenuItem.Visible = True
                    DocsReceivedImg.Visible = True
                    If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                        btnFinish.Enabled = True
                    End If

                Case Is = 5
                    'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEnquiryToolStripMenuItem.Visible = True
                    DocumentsReceivedToolStripMenuItem.Visible = True
                    DocsReceivedImg.Visible = True
                    If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                        btnFinish.Enabled = True
                    End If

                Case Is > 5 'Systems Manager Level, Open.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEnquiryToolStripMenuItem.Visible = True
                    DocumentsReceivedToolStripMenuItem.Visible = True
                    DocsReceivedImg.Visible = True
                    If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                        btnFinish.Enabled = True
                    End If

                Case Else 'Readonly
                    UpdateAllToolStripMenuItem.Visible = False
                    btnSaveAndExit.Enabled = False
                    btnGenAppCode.Enabled = False
                    btnAddComment.Enabled = False
                    btnFinish.Enabled = False
                    btnDocs.Enabled = False
                    cmbxMainCustType.Enabled = False
                    txtbxAppCodeValue.ReadOnly = True

            End Select
            '************************* end of Set permissions
            '************************* Universal Deny/negative settings
            If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_FOLLOW_UP Or docsReceived = True Then
                DocumentsReceivedToolStripMenuItem.Text = "Reset Docs received"
                'DocumentsReceivedToolStripMenuItem.Visible = False
                DocsReceivedImg.Visible = False
            End If


            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("PassVariable caused an error:" & vbCrLf & ex.Message)
        End Try

        '************************* Add handlers
        AddHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged
        ResumeLayout()

        '************************* Compare status with OAC status
        If Not IsDBNull(thisApplicationCode) And Not thisApplicationCode = String.Empty Then
            Try
                Dim compareResult As New TrackResult
                compareResult = Await compareResultTask
                If compareResult.ErrorCode > 0 Then
                    MessageBox.Show(compareResult.ErrorMessage, "Status comparison", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                End If
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Status comparison", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
        


    End Sub 'Form PassVariable

    Private Sub WkShtWizForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Remove Handlers
        RemoveHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged
        Me.EnquiryBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Cursor.Current = Cursors.WaitCursor
        'Remove Handlers
        RemoveHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        Application.DoEvents()

        Dim checkFieldsMessage As String = ""
        Try
            'Check Fields have value
            If txtbxAppCodeValue.Text.ToString = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Application Code" + vbCrLf
            End If
            If cmbxMainCustType.SelectedValue Is Nothing Then
                checkFieldsMessage = checkFieldsMessage + "Please select Customer Type" + vbCrLf
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


        If Not checkFieldsMessage = "" Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: There are errors please view message box."
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Else
            Try
                If SaveAppCode() = True Then
                    'update fields
                    Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                    'reload data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
                    Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
                    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    If Not enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE Then
                        'update fields
                        enquiryRow.BeginEdit()
                        'update WizardStatus
                        enquiryRow.WizardStatus = 2
                        'update Current Status
                        enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE 'Due Diligence
                        enquiryRow.CurrentStatusSetDate = DateTime.Now
                        'Set Original Enquiry Time
                        Dim originalTime As DateTime
                        originalTime = enquiryRow.DateTime
                        'calculate time difference for 30min status
                        Dim statusTimeDiff As Integer
                        statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
                        If statusTimeDiff < 30 Then
                            enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE 'Due Diligence
                        End If

                        'create log note
                        Dim commentStr As String
                        Dim commentId As Integer
                        commentStr = "Status changed to " & Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

                        'save changes
                        Me.Validate()
                        EnquiryBindingSource.EndEdit()
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                    End If

                    '************************************
                    'launch next WkShtWizForm and pass Enquiry Id
                    'update Status Strip
                    ToolStripStatusLabel1.Text = "Launching next form."
                    Application.DoEvents()

                    If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then

                        sw = New Stopwatch
                        sw.Start()
                        log.Debug("BtnNextClick: Launching next form - " & sw.Elapsed.ToString & ". ")

                        Dim wkShtWizFrm3 As New WkShtWizForm3
                        wkShtWizFrm3.PassVariable(thisEnquiryId)
                        wkShtWizFrm3.Show()
                    Else
                        Dim wkShtWizFrm2 As New WkShtWizForm2
                        wkShtWizFrm2.PassVariable(thisEnquiryId)
                        wkShtWizFrm2.Show()
                    End If
                    'Close this Form
                    Me.Close()
                Else
                    'MsgBox("No record to save, please enter information")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Error Saving!"
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
        End If
        Cursor.Current = Cursors.Default
    End Sub

    'Private Sub btnCheckNames_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckNames.Click
    '    'update Status Strip
    '    ToolStripStatusLabel1.ForeColor = Color.Black
    '    ToolStripStatusLabel1.Text = "Checking Names."
    '    Try
    '        'Check String has at least 2 Characters
    '        If ClientLastName.Length >= 2 Then
    '            'Get the String to search for and load Project wide variable
    '            Try
    '                SearchForLastNameLike = ClientLastName.Substring(0, 2) '& "%"
    '                'MsgBox(SearchForLastNameLike)
    '                'Call the Results form
    '                Dim CheckNamesResultsFrm As New CheckNamesResultsForm()
    '                CheckNamesResultsFrm.ShowDialog()
    '            Catch ex As Exception
    '                MsgBox(ex.Message)
    '            End Try
    '        Else
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
    '            'lblError.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
    '            'lblError.ForeColor = Color.Red
    '            'lblError.Visible = True
    '        End If
    '    Catch ex As Exception
    '        'update Status Strip
    '        ToolStripStatusLabel1.ForeColor = Color.Red
    '        ToolStripStatusLabel1.Text = "ERROR:  " & ex.Message
    '        'MsgBox(ex.Message)
    '        'lblError.Text = "ERROR:  " & ex.Message
    '    End Try
    'End Sub

    'Private Sub btnCheckEnquiries_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckEnquiries.Click
    '    Try
    '        'Check String has at least 2 Characters
    '        If ClientLastName.Length >= 2 Then
    '            'Get the String to search for and load Project wide variable
    '            Try
    '                SearchForLastNameLike = ClientLastName.Substring(0, 2)
    '                'MsgBox(SearchForLastNameLike)
    '                'Call the Results form
    '                Dim CheckNamesEnquiryResultsFrm As New CheckNamesEnquiryResultsForm
    '                CheckNamesEnquiryResultsFrm.PassVariable(SearchForLastNameLike)
    '                CheckNamesEnquiryResultsFrm.Show()
    '            Catch ex As Exception
    '                MsgBox(ex.Message)
    '            End Try
    '        Else
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnAddComment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddComment.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim addCommentFrm As New AddCommentForm()
        addCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        addCommentFrm.Dispose()

    End Sub

    Private Sub FormRefresh()
        Call PassVariable(thisEnquiryId)
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FormRefresh()

    End Sub



    Private Sub btnGenAppCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenAppCode.Click
        'disable button so can not multiple click
        btnGenAppCode.Enabled = False
        Try
            'test ApplicationCodeGeneratorTable to see if there is already a record
            'fill table adapter
            Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.ApplicationCodeGenerator, thisEnquiryId)
            'Check there is Data in table
            If EnquiryWorkSheetDataSet.ApplicationCodeGenerator.Rows.Count > 0 Then
                'get Application Code
                thisApplicationCode = EnquiryWorkSheetDataSet.ApplicationCodeGenerator.Rows(ApplicationCodeGeneratorBindingSource.Position()).Item("ApplicationCode")
                'MsgBox(ThisApplicationCode)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Application Code already exists.  Status: Ready"
            Else
                'Make a new Enquiry
                Me.ApplicationCodeGeneratorTableAdapter.InsertByEnquiryId(thisEnquiryId)
                'fill table adapter
                Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.ApplicationCodeGenerator, thisEnquiryId)
                'Check there is Data in table
                If EnquiryWorkSheetDataSet.ApplicationCodeGenerator.Rows.Count > 0 Then
                    'get Application Code
                    thisApplicationCode = EnquiryWorkSheetDataSet.ApplicationCodeGenerator.Rows(ApplicationCodeGeneratorBindingSource.Position()).Item("ApplicationCode")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Application Code Generated and Enquiry Updated.  Status: Ready"
                Else
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Problem with Application Code Generation: No rows in data table!"
                    If thisApplicationCode = "" Then
                        'enable button
                        btnGenAppCode.Enabled = True
                    End If
                End If
            End If

            txtbxAppCodeValue.Text = thisApplicationCode
            appCodeChanged = True
            'update Enquiry table
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "Application Code Generation Error:  " & ex.Message
        End Try


    End Sub

    Private Sub txtbxAppCodeValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxAppCodeValue.TextChanged
        'set Generate Application Code button state
        If txtbxAppCodeValue.Text = "" Then
            'enable button
            btnGenAppCode.Enabled = True
        Else
            'disable button
            btnGenAppCode.Enabled = False
        End If
        appCodeChanged = True
    End Sub




    Private Sub btnSaveAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndExit.Click
        Cursor.Current = Cursors.WaitCursor
        Dim appCodeSaved As Boolean = False
        'Remove Handlers
        RemoveHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged
        Try
            'test data
            If Not txtbxAppCodeValue.Text.ToString = "" Then
                appCodeSaved = SaveAppCode()
            End If

            If appCodeSaved = True And Not cmbxMainCustType.SelectedValue Is Nothing Then
                'update fields
                Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                Dim originalTime As DateTime
                Dim statusTimeDiff As Integer
                Dim commentStr As String

                Try
                    'reload data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
                    Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
                    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    If Not enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE Then
                        'update fields
                        enquiryRow.BeginEdit()
                        'update WizardStatus
                        enquiryRow.WizardStatus = 2
                        'update Current Status
                        enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE 'Due Diligence
                        enquiryRow.CurrentStatusSetDate = DateTime.Now
                        'Set Original Enquiry Time
                        originalTime = enquiryRow.DateTime
                        'calculate time difference for 30min status
                        statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
                        If statusTimeDiff < 30 Then
                            enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE 'Due Diligence
                        End If

                        'create log note                   
                        Dim commentId As Integer
                        commentStr = "Status changed to " & Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

                        'save changes
                        Me.Validate()
                        EnquiryBindingSource.EndEdit()
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                    End If


                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("ERROR on Save and Exit: " + vbCrLf + ex.Message)
                End Try

                '************************************
            End If
            'close form
            Me.Close()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Save and Exit: " + vbCrLf + ex.Message)
        End Try

        AddHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub WkShtWizForm1_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'resize
        tlpApplication.Size = New Size(formWidth - 40, 30)
        'txtbxLoanProgress.Size = New Size(formWidth - 40, formHeight - 264)
        dgvEnquiryComments.Size = New Size(formWidth - 40, formHeight - 268)
        'End If
    End Sub

    Private Sub WkShtWizForm1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SuspendLayout()
        'Remove Handlers
        RemoveHandler cmbxMainCustType.SelectedIndexChanged, AddressOf cmbxMainCustType_SelectedIndexChanged

        'check if data has changed
        'Dim bindSrc1 As BindingSource = Me.ClientBindingSource
        Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not bindSrc2.Current Is Nothing Then 'And Not bindSrc1.Current Is Nothing Then
            rowView2 = CType(bindSrc2.Current, DataRowView)
            'test if any fields have changed
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        'update ApplicationCode
                        If Not String.IsNullOrEmpty(Trim(txtbxAppCodeValue.Text)) Then
                            thisApplicationCode = Trim(txtbxAppCodeValue.Text)

                            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                            enquiryRow.BeginEdit()
                            enquiryRow.ApplicationCode = thisApplicationCode
                        End If
                        Me.Validate()
                        'bindSrc1.EndEdit()
                        bindSrc2.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If

        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            newLocX = 0
        Else
            newLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            newLocY = 0
        Else
            newLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.PrelimFormLocation = New Point(newLocX, newLocY)
        My.Settings.PrelimFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings

    End Sub

    '******************* Documents
    Private Sub btnDocs_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Drag and drop successful.    Status: Ready"
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Drag and Drop caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnDocs_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        Else
            Try
                'create directory
                IO.Directory.CreateDirectory(fname)
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
                'refresh form
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
            Catch ex As System.IO.DirectoryNotFoundException
                ' Let the user know that the directory did not exist.
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

        End If
    End Sub
    '*************** End of Documents
    '*************** Buttons **************


    Private Sub btnQRGList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQRGList.Click

        Try
            'MsgBox("ThisQRGListId = " & ThisQRGListId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Quick reference Guide List."
            Dim QRGFrm As New QRGForm
            QRGFrm.PassVariable(thisEnquiryId, thisEnquiryCode, thisContactDisplayName)
            'AddSecurityFrm.ShowDialog(Me)
            If (QRGFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                '*****************************  get QRGList status
                Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection2 As New SqlConnection(connectionString2)
                Dim selectStatement2 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand2)
                Dim QRGListTempDataSet As New DataSet
                Dim QRGListTempDataTable As New DataTable
                Try
                    'dumps results into datatable LoginDataTable
                    QRGListTempDataAdapter.Fill(QRGListTempDataTable)
                    'if no matching rows .....
                    If QRGListTempDataTable.Rows.Count = 0 Then
                        MessageBox.Show("ERROR: No QRG List, please try again.")
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                        'if there is a matching row
                    ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                        'get  QRGStatus value
                        Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                        qrgStatus = QRGListTempDataRow.Item(0)
                        'MsgBox("QRG Status = " & QRGStatus)
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                    End If
                    'close the connection
                    If connection2.State <> ConnectionState.Closed Then
                        connection2.Close()
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

                'set QRGStatus button colour
                Select Case qrgStatus
                    Case 0
                        btnQRGList.BackColor = Color.Transparent
                    Case 1
                        btnQRGList.BackColor = Color.Tomato
                    Case 2
                        btnQRGList.BackColor = Color.LightGreen
                    Case Else
                        btnQRGList.BackColor = Color.Transparent
                End Select
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Quick reference Guide List updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Quick reference Guide List cancelled. Status: Ready."
            End If
            QRGFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub btnViewDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Not Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Shared Folder does not exist!."
        Else
            Dim ViewDocsFrm As New ViewDocsForm
            ViewDocsFrm.PassVariable(thisEnquiryCode, thisContactDisplayName)
            ViewDocsFrm.ShowDialog(Me)
            If ViewDocsFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                ViewDocsFrm.Dispose()
            Else
                ViewDocsFrm.Dispose()
            End If
        End If

    End Sub



    Private Sub WkShtWizForm1_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Dim MeNewLocX As Integer
        Dim MeNewLocY As Integer

        If Me.Location.X < 0 Then
            MeNewLocX = 0
        Else
            MeNewLocX = Me.Location.X
        End If
        If Me.Location.Y < 0 Then
            MeNewLocY = 0
        Else
            MeNewLocY = Me.Location.Y
        End If
        Me.Location = New Point(MeNewLocX, MeNewLocY)
    End Sub

    '******************************* MenuStrip code
#Region "MenuStrip"

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub


    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim DisplayDealersFrm As New DisplayDealersForm
            DisplayDealersFrm.PassVariable(thisDealerId)
            DisplayDealersFrm.ShowDialog()
            DisplayDealersFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '************ end of Check Names

    Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor

        If SaveAppCode() = True Then
            FormRefresh()
        End If
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EditCandDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCandDToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim editDetailsFrm As New EditDetailsForm
            editDetailsFrm.PassVariable(thisEnquiryId)
            If (editDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            editDetailsFrm.Dispose()
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailFrm As New EmailForm
        EmailFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailFrm.Dispose()

    End Sub

    Private Sub ApprovalFormToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem1.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub EmailApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailApprovalToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailAppFrm As New EmailApprovalForm
        EmailAppFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailAppFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailAppFrm.Dispose()
    End Sub

    Private Sub WWWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WWWToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        'launch internet browser for web application
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching web browser."
        System.Diagnostics.Process.Start(My.Settings.YFL_website)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."
        Cursor.Current = Cursors.Default
    End Sub


    Private Sub DeclinedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeclinedToolStripMenuItem.Click
        thisEnquiryResult = Constants.CONST_CURRENT_STATUS_DECLINED '"Declined"
        progressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        Dim rowView As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            rowView = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                DeclinedFrm.Dispose()
                progressStatus = True
            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If


    End Sub

    Private Sub WithdrawnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawnToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        thisEnquiryResult = Constants.CONST_CURRENT_STATUS_WITHDRAWN '"Withdrawn"
        progressStatus = False
        'check if data has changed
        Dim rowView As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            rowView = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        '###########################
        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                DeclinedFrm.Dispose()
                progressStatus = True
            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Withdrawing of Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub MCMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MCMenuItem.Click
        Dim MembersCentreUtilObject As New WebUtil
        MembersCentreUtilObject.MCLink()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = MembersCentreUtilObject.GetLabelForeColor
        ToolStripStatusLabel1.Text = MembersCentreUtilObject.GetLabelText
    End Sub

    Private Sub CommentsOnlyToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CommentsOnlyToolStripMenuItem1.Click
        Try
            'Specify current page settings
            PrintDocument1.DefaultPageSettings = PrintPageSettings
            FinishedString = "Comments Report for Enquiry " & thisEnquiryCode & vbCrLf & vbCrLf & Util.GetEnquiryComments(thisEnquiryId) & vbCrLf & vbCrLf & "Printed by: " & LoggedinName & vbCrLf & DateTime.Now.ToString
            StringToPrint = FinishedString

            PrintPreviewDialog1.Document = PrintDocument1
            PrintPreviewDialog1.ShowDialog()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.Message, "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim numChars As Integer
        Dim numLines As Integer
        Dim stringForPage As String
        Dim strFormat As New StringFormat
        'Based on page setup, define drawable rectangle on page
        Dim rectDraw As New RectangleF(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, e.MarginBounds.Height)
        'Define area to determine how much text can fit on a page
        'Make height one line shorter to ensure text doesn't clip
        Dim sizeMeasure As New SizeF(e.MarginBounds.Width, e.MarginBounds.Height - PrintFont.GetHeight(e.Graphics))

        'When drawing long strings, break between words
        strFormat.Trimming = StringTrimming.Word
        'Compute how many chars and lines can fit based on sizeMeasure
        e.Graphics.MeasureString(StringToPrint, PrintFont, sizeMeasure, strFormat, numChars, numLines)
        'Compute string that will fit on page
        stringForPage = StringToPrint.Substring(0, numChars)
        'Print string on current page
        e.Graphics.DrawString(stringForPage, PrintFont, Brushes.Black, rectDraw, strFormat)
        'If there is more text, indicate there are more pages
        If numChars < StringToPrint.Length Then
            'Subtract text from string that has been printed
            StringToPrint = StringToPrint.Substring(numChars)
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            'All text has been printed, so restore string
            StringToPrint = FinishedString
        End If
    End Sub


    Private Async Sub DocumentsReceivedToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocumentsReceivedToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        If Not EnquiryBindingSource.Current Is Nothing Then
            Dim commentId As Integer
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            If docsReceived = False Then
                enquiryRow.BeginEdit()
                enquiryRow.DocsReceived = True
                docsReceived = True
            Else
                Dim result As DialogResult = MessageBox.Show("This will unlock the OAC and set it to Status of ‘Waiting for Docs’." & vbCrLf & "You will need to receive the documents again before TrueTrack can pass CAD approval.", "Reset Documents Received", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                If result = DialogResult.Cancel Then
                    Exit Sub
                ElseIf result = DialogResult.OK Then
                    Cursor.Current = Cursors.WaitCursor
                    Application.DoEvents()
                    enquiryRow.BeginEdit()
                    enquiryRow.DocsReceived = False
                    docsReceived = False
                End If

            End If
            'save changes
            Try
                Me.Validate()
                enquiryRow.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                If docsReceived = True Then
                    DocumentsReceivedToolStripMenuItem.Text = "Reset Docs received"
                    DocsReceivedImg.Visible = False
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Documents received")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Documents received saved successfully."
                Else
                    DocumentsReceivedToolStripMenuItem.Text = "Documents received"
                    DocsReceivedImg.Visible = True
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Documents received reset.")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Documents received reset."
                End If
                Application.DoEvents()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Update Status Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try 'Check is possible to Change Remote Status
            Dim systemCanChangeRemoteStatus As Boolean
            Dim thisAppsettings As New AppSettings
            Dim retTTResult As New TrackResult
            Dim onlineApplicationService As New OnlineApplication
            Dim actionType As Integer
            Try
                systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus

                If systemCanChangeRemoteStatus = True Then
                    If docsReceived = True Then
                        'Change Status in OAC
                        actionType = OACWebService.ApplicationActionType.DocumentsReceived
                        retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, String.Empty)
                        If retTTResult.Status = False Then 'error condition
                            commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                            MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Else
                        'Change Status to WaitingForDocs in OAC
                        Try
                            retTTResult = Await onlineApplicationService.SetStatusWaitingForDocs(thisApplicationCode)
                            If retTTResult.ErrorCode > 0 Then
                                MessageBox.Show(retTTResult.ErrorMessage, "Set Status WaitingForDocs", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                            Else
                                'action was successful, no need to do anything else
                            End If
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            MessageBox.Show(ex.Message, "Set Status WaitingForDocs", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Try
                        If retTTResult.Status = False Then 'error condition
                            commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                            MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Change Remote Status Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Cursor.Current = Cursors.WaitCursor
            Application.DoEvents()

            FormRefresh()
        End If
        Cursor.Current = Cursors.Default

    End Sub

#End Region
    '******************************* end of MenuStrip code

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Cursor.Current = Cursors.WaitCursor
        Dim checkFieldsMessage As String = ""
        Try
            'Check Fields have value
            If txtbxAppCodeValue.Text.ToString = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Application Code" + vbCrLf
            End If
            If cmbxMainCustType.SelectedValue Is Nothing Then
                checkFieldsMessage = checkFieldsMessage + "Please select Customer Type" + vbCrLf
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        If Not Me.EnquiryBindingSource.Current Is Nothing Then
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
            If enquiryRow.IsDocsReceivedNull Then
                'no docs received
                MessageBox.Show("Documents have not been received.", "Loan documents", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                If enquiryRow.DocsReceived = True Then
                    If Not checkFieldsMessage = "" Then
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "ERROR: There are errors please view message box."
                        MsgBox(checkFieldsMessage)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Status: Ready."
                    Else
                        If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                            'save changes
                            Try
                                Me.Validate()
                                'ClientBindingSource.EndEdit()
                                EnquiryBindingSource.EndEdit()
                                Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Black
                                ToolStripStatusLabel1.Text = "Changes saved successfully."
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox(ex.Message)
                            End Try


                            Try
                                Dim dDApprovalFrm As New DDApprovalForm
                                dDApprovalFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryType)
                                dDApprovalFrm.ShowDialog(Me)
                                If dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                                    dDApprovalFrm.Dispose()
                                    'update Status Strip
                                    ToolStripStatusLabel1.ForeColor = Color.Black
                                    ToolStripStatusLabel1.Text = "Changes saved successfully."
                                    progressStatus = True
                                ElseIf dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.No Then
                                    dDApprovalFrm.Dispose()
                                    'update Status Strip
                                    ToolStripStatusLabel1.ForeColor = Color.DarkGreen
                                    ToolStripStatusLabel1.Text = "Application Declined."
                                    progressStatus = True
                                ElseIf dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                                    dDApprovalFrm.Dispose()
                                    'User choose Cancel, do nothing
                                    progressStatus = False
                                    'update Status Strip
                                    ToolStripStatusLabel1.ForeColor = Color.Black
                                    ToolStripStatusLabel1.Text = "Approving Due Diligence cancelled. Status: Ready."
                                End If
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
                            End Try

                            If progressStatus = True Then
                                Cursor.Current = Cursors.WaitCursor
                                'launch  Standard Worksheet Form
                                Try
                                    Dim AppFrm As New AppForm
                                    AppFrm.PassVariable(thisEnquiryId)
                                    AppFrm.Show()
                                    Cursor.Current = Cursors.Default
                                    'Close this Form
                                    Me.Close()
                                Catch ex As Exception
                                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                    MsgBox(ex.Message)
                                End Try
                            End If

                        End If
                    End If
                Else
                    'no docs received
                    MessageBox.Show("Documents have not been received.", "Loan documents", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Else
            'no current record in binding source - do not proceed
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "No data entered, please enter data!"
        End If
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub cmbxMainCustType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxMainCustType.SelectedIndexChanged
        Cursor.Current = Cursors.WaitCursor
        'disable button until update complete
        SetButtonState(False)
        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
        enquiryRow.BeginEdit()
        'EnquiryRow.mainCustomerType = DirectCast(cmbxMainCustType.SelectedValue, KeyValuePair(Of Integer, String)).Key()
        enquiryRow.mainCustomerType = cmbxMainCustType.SelectedValue.key
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        'Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet)
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        SetButtonState(True)
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub lblEnquiryNumberValue_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lblEnquiryNumberValue.MouseDoubleClick
        lblEnquiryNumberValue.BackColor = Color.LightPink
        Clipboard.SetDataObject(lblEnquiryNumberValue.Text, False)

        'lblEnquiryNumberValue.BackColor = Color.LightBlue
    End Sub


    Private Sub lblEnquiryNumberValue_MouseHover(sender As Object, e As EventArgs) Handles lblEnquiryNumberValue.MouseHover
        Cursor.Current = Cursors.Hand
        lblEnquiryNumberValue.BackColor = Color.LightBlue
    End Sub

    Private Sub lblEnquiryNumberValue_MouseLeave(sender As Object, e As EventArgs) Handles lblEnquiryNumberValue.MouseLeave
        Cursor.Current = Cursors.Default
        lblEnquiryNumberValue.BackColor = SystemColors.Control
    End Sub

    ''' <summary>
    ''' Save Application Code and Update All
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    ''' 
    Function SaveAppCode() As Boolean
        progressStatus = False
        Dim bindSrc1 As BindingSource = Me.EnquiryBindingSource
        'leave if bindingsource.current is nothing (no data)
        If Not bindSrc1.Current Is Nothing Then
            'update ApplicationCode
            'If Not String.IsNullOrEmpty(Trim(txtbxAppCodeValue.Text)) Then
            If Not Trim(txtbxAppCodeValue.Text) Is Nothing Then
                thisApplicationCode = Trim(txtbxAppCodeValue.Text)
                'Check ApplicationCode does not exist for another enquiry
                Dim intCount As Integer = 0
                Using mycon As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)
                    Dim myCommand As New SqlCommand("FindApplicationCode", mycon)
                    myCommand.CommandType = CommandType.StoredProcedure
                    myCommand.Parameters.AddWithValue("@appCode ", thisApplicationCode)
                    myCommand.Parameters.AddWithValue("@enquiryId ", thisEnquiryId)
                    Dim RetValue As SqlParameter = myCommand.Parameters.Add("RetValue", SqlDbType.Int)
                    RetValue.Direction = ParameterDirection.ReturnValue
                    mycon.Open()
                    myCommand.ExecuteScalar()
                    intCount = CInt(RetValue.Value)
                End Using
                If intCount > 0 Then
                    MessageBox.Show("Application Code already exists" & vbCrLf & "Check for a duplicate Enquiry or amend your Application Code", "Application Code Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "ERROR: Application Code already exists."
                    progressStatus = False
                Else
                    Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    enquiryRow.BeginEdit()
                    enquiryRow.ApplicationCode = thisApplicationCode
                    'save changes
                    Try
                        Me.Validate()
                        enquiryRow.EndEdit()
                        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                        progressStatus = True
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try

                End If
               
            End If

        Else
            'no current record in binding source - do not proceed
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "No data entered, please enter data!"
            progressStatus = False
        End If

        Return progressStatus

    End Function

    ''' <summary>
    ''' Enable/disable buttons: False sets to disabled
    ''' </summary>
    ''' <param name="state">Boolean</param>
    ''' <remarks></remarks>
    Private Sub SetButtonState(ByVal state As Boolean)
        If state = False Then
            btnSaveAndExit.Enabled = False
            btnGenAppCode.Enabled = False
            btnAddComment.Enabled = False
            btnFinish.Enabled = False
            btnDocs.Enabled = False
            cmbxMainCustType.Enabled = False
            txtbxAppCodeValue.ReadOnly = True
        Else
            btnSaveAndExit.Enabled = True
            btnGenAppCode.Enabled = True
            btnAddComment.Enabled = True
            btnFinish.Enabled = True
            btnDocs.Enabled = True
            cmbxMainCustType.Enabled = True
            txtbxAppCodeValue.ReadOnly = False
        End If
    End Sub

#Region "Editing"


    Private Sub dgvEnquiryComments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEnquiryComments.DoubleClick

        Dim selectedRowCommentId As Integer = 0
        Dim selectedRowView As Data.DataRowView
        Dim selectedRow As EnquiryWorkSheetDataSet.EnquiryCommentRow
        selectedRowView = CType(EnquiryCommentBS.Current, System.Data.DataRowView)
        If selectedRowView.Row IsNot Nothing Then
            selectedRow = selectedRowView.Row
            selectedRowCommentId = selectedRow.Id

            'launch Add Comment Form
            Dim addCommentFrm As New AddCommentForm()
            If ApplicationAccessRights.CanEditComments Then
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.Edit)
            Else
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.ReadOnly)
            End If
            ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
            If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'Call the function you used to populate the data grid..
                Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment added."
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment cancelled."
            End If
            'When Add Comment Form closed
            addCommentFrm.Dispose()

        End If

    End Sub

    Private Sub DocsReceivedImg_Click(sender As Object, e As EventArgs) Handles DocsReceivedImg.Click
        DocumentsReceivedToolStripMenuItem_Click(sender, e)
    End Sub

#End Region

#Region "Information buttons"
    Private Sub pbNC_Click(sender As Object, e As EventArgs) Handles pbNC.Click
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\DocNameConvention.rtf", "Document Naming Conventions", MyEnums.MsgBoxMode.FilePath, 600)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()
    End Sub

    Private Sub pbNC_MouseHover(sender As Object, e As EventArgs) Handles pbNC.MouseHover
        pbNC.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbNC_MouseLeave(sender As Object, e As EventArgs) Handles pbNC.MouseLeave
        pbNC.Image = My.Resources.questionBsm
    End Sub


#End Region

   
   
End Class