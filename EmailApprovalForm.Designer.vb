﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmailApprovalForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EmailApprovalForm))
        Me.lblEmailTo = New System.Windows.Forms.Label()
        Me.cmbxEmailTo = New System.Windows.Forms.ComboBox()
        Me.lblAttachments = New System.Windows.Forms.Label()
        Me.txtbxAttachmentsList = New System.Windows.Forms.TextBox()
        Me.btnAttachments = New System.Windows.Forms.Button()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.lblSignatureLabel = New System.Windows.Forms.Label()
        Me.lblSignature = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.lblSubject = New System.Windows.Forms.Label()
        Me.txtbxEmailSubject = New System.Windows.Forms.TextBox()
        Me.txtbxEmailFrom = New System.Windows.Forms.TextBox()
        Me.lblEmailFrom = New System.Windows.Forms.Label()
        Me.lblConditions = New System.Windows.Forms.Label()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.cmbxResult = New System.Windows.Forms.ComboBox()
        Me.lblPostScript = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.txtbxDealer = New System.Windows.Forms.TextBox()
        Me.lblClient = New System.Windows.Forms.Label()
        Me.txtbxClient = New System.Windows.Forms.TextBox()
        Me.DealerContactDetailsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter()
        Me.DealerContactDetailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.EnquiryTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.EnquiryBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.rtbConditions = New AppWhShtB.SpellBox()
        Me.lblCC = New System.Windows.Forms.Label()
        Me.cmbxCC = New System.Windows.Forms.ComboBox()
        Me.pbEmail = New System.Windows.Forms.PictureBox()
        CType(Me.DealerContactDetailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblEmailTo
        '
        Me.lblEmailTo.AutoSize = True
        Me.lblEmailTo.Location = New System.Drawing.Point(29, 85)
        Me.lblEmailTo.Name = "lblEmailTo"
        Me.lblEmailTo.Size = New System.Drawing.Size(47, 13)
        Me.lblEmailTo.TabIndex = 34
        Me.lblEmailTo.Text = "Email to:"
        '
        'cmbxEmailTo
        '
        Me.cmbxEmailTo.FormattingEnabled = True
        Me.cmbxEmailTo.Location = New System.Drawing.Point(82, 82)
        Me.cmbxEmailTo.Name = "cmbxEmailTo"
        Me.cmbxEmailTo.Size = New System.Drawing.Size(439, 21)
        Me.cmbxEmailTo.TabIndex = 33
        '
        'lblAttachments
        '
        Me.lblAttachments.AutoSize = True
        Me.lblAttachments.Location = New System.Drawing.Point(10, 486)
        Me.lblAttachments.Name = "lblAttachments"
        Me.lblAttachments.Size = New System.Drawing.Size(66, 13)
        Me.lblAttachments.TabIndex = 32
        Me.lblAttachments.Text = "Attachments"
        '
        'txtbxAttachmentsList
        '
        Me.txtbxAttachmentsList.Location = New System.Drawing.Point(82, 483)
        Me.txtbxAttachmentsList.Name = "txtbxAttachmentsList"
        Me.txtbxAttachmentsList.ReadOnly = True
        Me.txtbxAttachmentsList.Size = New System.Drawing.Size(408, 20)
        Me.txtbxAttachmentsList.TabIndex = 31
        '
        'btnAttachments
        '
        Me.btnAttachments.BackColor = System.Drawing.Color.Transparent
        Me.btnAttachments.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAttachments.Image = Global.AppWhShtB.My.Resources.Resources.paperclip16a
        Me.btnAttachments.Location = New System.Drawing.Point(496, 480)
        Me.btnAttachments.Name = "btnAttachments"
        Me.btnAttachments.Size = New System.Drawing.Size(23, 23)
        Me.btnAttachments.TabIndex = 30
        Me.btnAttachments.UseVisualStyleBackColor = False
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.Location = New System.Drawing.Point(117, 5)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(303, 13)
        Me.lblInfo.TabIndex = 29
        Me.lblInfo.Text = "Email will be sent in Plain Text, does not support formatted text."
        '
        'lblSignatureLabel
        '
        Me.lblSignatureLabel.AutoSize = True
        Me.lblSignatureLabel.Location = New System.Drawing.Point(21, 517)
        Me.lblSignatureLabel.Name = "lblSignatureLabel"
        Me.lblSignatureLabel.Size = New System.Drawing.Size(55, 13)
        Me.lblSignatureLabel.TabIndex = 28
        Me.lblSignatureLabel.Text = "Signature:"
        '
        'lblSignature
        '
        Me.lblSignature.AutoSize = True
        Me.lblSignature.Location = New System.Drawing.Point(96, 517)
        Me.lblSignature.Name = "lblSignature"
        Me.lblSignature.Size = New System.Drawing.Size(52, 13)
        Me.lblSignature.TabIndex = 27
        Me.lblSignature.Text = "Signature"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(82, 589)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 26
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSend
        '
        Me.btnSend.BackColor = System.Drawing.Color.PaleGreen
        Me.btnSend.Location = New System.Drawing.Point(446, 589)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 25
        Me.btnSend.Text = "Send"
        Me.btnSend.UseVisualStyleBackColor = False
        '
        'lblSubject
        '
        Me.lblSubject.AutoSize = True
        Me.lblSubject.Location = New System.Drawing.Point(30, 165)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(46, 13)
        Me.lblSubject.TabIndex = 22
        Me.lblSubject.Text = "Subject:"
        '
        'txtbxEmailSubject
        '
        Me.txtbxEmailSubject.Location = New System.Drawing.Point(82, 162)
        Me.txtbxEmailSubject.Name = "txtbxEmailSubject"
        Me.txtbxEmailSubject.Size = New System.Drawing.Size(439, 20)
        Me.txtbxEmailSubject.TabIndex = 21
        '
        'txtbxEmailFrom
        '
        Me.txtbxEmailFrom.Location = New System.Drawing.Point(82, 136)
        Me.txtbxEmailFrom.Name = "txtbxEmailFrom"
        Me.txtbxEmailFrom.ReadOnly = True
        Me.txtbxEmailFrom.Size = New System.Drawing.Size(439, 20)
        Me.txtbxEmailFrom.TabIndex = 20
        '
        'lblEmailFrom
        '
        Me.lblEmailFrom.AutoSize = True
        Me.lblEmailFrom.Location = New System.Drawing.Point(18, 139)
        Me.lblEmailFrom.Name = "lblEmailFrom"
        Me.lblEmailFrom.Size = New System.Drawing.Size(58, 13)
        Me.lblEmailFrom.TabIndex = 19
        Me.lblEmailFrom.Text = "Email from:"
        '
        'lblConditions
        '
        Me.lblConditions.AutoSize = True
        Me.lblConditions.Location = New System.Drawing.Point(17, 218)
        Me.lblConditions.Name = "lblConditions"
        Me.lblConditions.Size = New System.Drawing.Size(59, 13)
        Me.lblConditions.TabIndex = 35
        Me.lblConditions.Text = "Conditions:"
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.Location = New System.Drawing.Point(36, 191)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(40, 13)
        Me.lblResult.TabIndex = 36
        Me.lblResult.Text = "Result:"
        '
        'cmbxResult
        '
        Me.cmbxResult.FormattingEnabled = True
        Me.cmbxResult.Items.AddRange(New Object() {"Pending", "Approval with full recourse", "Approval with partial recourse", "Approval with no recourse", "Declined"})
        Me.cmbxResult.Location = New System.Drawing.Point(82, 188)
        Me.cmbxResult.Name = "cmbxResult"
        Me.cmbxResult.Size = New System.Drawing.Size(439, 21)
        Me.cmbxResult.TabIndex = 37
        '
        'lblPostScript
        '
        Me.lblPostScript.AutoSize = True
        Me.lblPostScript.Location = New System.Drawing.Point(79, 418)
        Me.lblPostScript.Name = "lblPostScript"
        Me.lblPostScript.Size = New System.Drawing.Size(163, 52)
        Me.lblPostScript.TabIndex = 38
        Me.lblPostScript.Text = "Please respond to this email with:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A detailed description of goods" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Payment star" & _
    "t date" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Delivery date"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(35, 33)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 39
        Me.lblDealer.Text = "Dealer:"
        '
        'txtbxDealer
        '
        Me.txtbxDealer.Location = New System.Drawing.Point(82, 30)
        Me.txtbxDealer.Name = "txtbxDealer"
        Me.txtbxDealer.Size = New System.Drawing.Size(439, 20)
        Me.txtbxDealer.TabIndex = 40
        '
        'lblClient
        '
        Me.lblClient.AutoSize = True
        Me.lblClient.Location = New System.Drawing.Point(40, 59)
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Size = New System.Drawing.Size(36, 13)
        Me.lblClient.TabIndex = 41
        Me.lblClient.Text = "Client:"
        '
        'txtbxClient
        '
        Me.txtbxClient.Location = New System.Drawing.Point(82, 56)
        Me.txtbxClient.Name = "txtbxClient"
        Me.txtbxClient.Size = New System.Drawing.Size(439, 20)
        Me.txtbxClient.TabIndex = 42
        '
        'DealerContactDetailsTableAdapter
        '
        Me.DealerContactDetailsTableAdapter.ClearBeforeFill = True
        '
        'DealerContactDetailsBindingSource
        '
        Me.DealerContactDetailsBindingSource.DataMember = "DealerContactDetails"
        Me.DealerContactDetailsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EnquiryTableAdapter1
        '
        Me.EnquiryTableAdapter1.ClearBeforeFill = True
        '
        'EnquiryBindingSource1
        '
        Me.EnquiryBindingSource1.DataMember = "Enquiry"
        Me.EnquiryBindingSource1.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'rtbConditions
        '
        Me.rtbConditions.IsReadOnly = False
        Me.rtbConditions.Location = New System.Drawing.Point(82, 218)
        Me.rtbConditions.MaxLength = 0
        Me.rtbConditions.MultiLine = True
        Me.rtbConditions.Name = "rtbConditions"
        Me.rtbConditions.SelectedText = ""
        Me.rtbConditions.SelectionLength = 0
        Me.rtbConditions.SelectionStart = 0
        Me.rtbConditions.Size = New System.Drawing.Size(437, 197)
        Me.rtbConditions.TabIndex = 43
        Me.rtbConditions.WordWrap = True
        Me.rtbConditions.Child = New System.Windows.Controls.TextBox()
        '
        'lblCC
        '
        Me.lblCC.AutoSize = True
        Me.lblCC.Location = New System.Drawing.Point(52, 112)
        Me.lblCC.Name = "lblCC"
        Me.lblCC.Size = New System.Drawing.Size(24, 13)
        Me.lblCC.TabIndex = 45
        Me.lblCC.Text = "CC:"
        '
        'cmbxCC
        '
        Me.cmbxCC.FormattingEnabled = True
        Me.cmbxCC.Location = New System.Drawing.Point(82, 109)
        Me.cmbxCC.Name = "cmbxCC"
        Me.cmbxCC.Size = New System.Drawing.Size(439, 21)
        Me.cmbxCC.TabIndex = 44
        '
        'pbEmail
        '
        Me.pbEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbEmail.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbEmail.Location = New System.Drawing.Point(503, 5)
        Me.pbEmail.Name = "pbEmail"
        Me.pbEmail.Size = New System.Drawing.Size(18, 18)
        Me.pbEmail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbEmail.TabIndex = 46
        Me.pbEmail.TabStop = False
        '
        'EmailApprovalForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(537, 624)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbEmail)
        Me.Controls.Add(Me.lblCC)
        Me.Controls.Add(Me.cmbxCC)
        Me.Controls.Add(Me.rtbConditions)
        Me.Controls.Add(Me.txtbxClient)
        Me.Controls.Add(Me.lblClient)
        Me.Controls.Add(Me.txtbxDealer)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblPostScript)
        Me.Controls.Add(Me.cmbxResult)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.lblConditions)
        Me.Controls.Add(Me.lblEmailTo)
        Me.Controls.Add(Me.cmbxEmailTo)
        Me.Controls.Add(Me.lblAttachments)
        Me.Controls.Add(Me.txtbxAttachmentsList)
        Me.Controls.Add(Me.btnAttachments)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.lblSignatureLabel)
        Me.Controls.Add(Me.lblSignature)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.lblSubject)
        Me.Controls.Add(Me.txtbxEmailSubject)
        Me.Controls.Add(Me.txtbxEmailFrom)
        Me.Controls.Add(Me.lblEmailFrom)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(553, 612)
        Me.Name = "EmailApprovalForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Email Approval"
        CType(Me.DealerContactDetailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbEmail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblEmailTo As System.Windows.Forms.Label
    Friend WithEvents cmbxEmailTo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAttachments As System.Windows.Forms.Label
    Friend WithEvents txtbxAttachmentsList As System.Windows.Forms.TextBox
    Friend WithEvents btnAttachments As System.Windows.Forms.Button
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents lblSignatureLabel As System.Windows.Forms.Label
    Friend WithEvents lblSignature As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents lblSubject As System.Windows.Forms.Label
    Friend WithEvents txtbxEmailSubject As System.Windows.Forms.TextBox
    Friend WithEvents txtbxEmailFrom As System.Windows.Forms.TextBox
    Friend WithEvents lblEmailFrom As System.Windows.Forms.Label
    Friend WithEvents lblConditions As System.Windows.Forms.Label
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cmbxResult As System.Windows.Forms.ComboBox
    Friend WithEvents lblPostScript As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents txtbxDealer As System.Windows.Forms.TextBox
    Friend WithEvents lblClient As System.Windows.Forms.Label
    Friend WithEvents txtbxClient As System.Windows.Forms.TextBox
    Friend WithEvents DealerContactDetailsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter
    Friend WithEvents DealerContactDetailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ClientTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ClientTableAdapter
    Friend WithEvents rtbConditions As AppWhShtB.SpellBox
    Friend WithEvents lblCC As System.Windows.Forms.Label
    Friend WithEvents cmbxCC As System.Windows.Forms.ComboBox
    Friend WithEvents pbEmail As System.Windows.Forms.PictureBox

End Class
