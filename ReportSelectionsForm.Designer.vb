﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EnquiryReportSelectionsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EnquiryReportSelectionsForm))
        Me.gpbxdateRange = New System.Windows.Forms.GroupBox()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.gpbxActiveStatus = New System.Windows.Forms.GroupBox()
        Me.rbtnBoth = New System.Windows.Forms.RadioButton()
        Me.rbtnActive = New System.Windows.Forms.RadioButton()
        Me.gpbxSorting = New System.Windows.Forms.GroupBox()
        Me.cmbxOrderBy = New System.Windows.Forms.ComboBox()
        Me.lblOrderBy = New System.Windows.Forms.Label()
        Me.cmbxGroupBy = New System.Windows.Forms.ComboBox()
        Me.lblGroupBy = New System.Windows.Forms.Label()
        Me.gpbxFilter = New System.Windows.Forms.GroupBox()
        Me.ckbxUsers = New System.Windows.Forms.CheckBox()
        Me.lstbxUsers = New System.Windows.Forms.ListBox()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gpbxdateRange.SuspendLayout()
        Me.gpbxActiveStatus.SuspendLayout()
        Me.gpbxSorting.SuspendLayout()
        Me.gpbxFilter.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbxdateRange
        '
        Me.gpbxdateRange.Controls.Add(Me.DateTimePicker2)
        Me.gpbxdateRange.Controls.Add(Me.lblEndDate)
        Me.gpbxdateRange.Controls.Add(Me.DateTimePicker1)
        Me.gpbxdateRange.Controls.Add(Me.lblStartDate)
        Me.gpbxdateRange.Location = New System.Drawing.Point(12, 12)
        Me.gpbxdateRange.Name = "gpbxdateRange"
        Me.gpbxdateRange.Size = New System.Drawing.Size(410, 58)
        Me.gpbxdateRange.TabIndex = 0
        Me.gpbxdateRange.TabStop = False
        Me.gpbxdateRange.Text = "Date Range"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "d/m/yyyy"
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(287, 22)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 3
        '
        'lblEndDate
        '
        Me.lblEndDate.AutoSize = True
        Me.lblEndDate.Location = New System.Drawing.Point(229, 25)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(52, 13)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "d/m/yyyy"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(67, 22)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 1
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Location = New System.Drawing.Point(6, 25)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(55, 13)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        '
        'gpbxActiveStatus
        '
        Me.gpbxActiveStatus.Controls.Add(Me.rbtnBoth)
        Me.gpbxActiveStatus.Controls.Add(Me.rbtnActive)
        Me.gpbxActiveStatus.Location = New System.Drawing.Point(12, 76)
        Me.gpbxActiveStatus.Name = "gpbxActiveStatus"
        Me.gpbxActiveStatus.Size = New System.Drawing.Size(410, 50)
        Me.gpbxActiveStatus.TabIndex = 1
        Me.gpbxActiveStatus.TabStop = False
        Me.gpbxActiveStatus.Text = "Enquiries"
        '
        'rbtnBoth
        '
        Me.rbtnBoth.AutoSize = True
        Me.rbtnBoth.Checked = True
        Me.rbtnBoth.Location = New System.Drawing.Point(232, 19)
        Me.rbtnBoth.Name = "rbtnBoth"
        Me.rbtnBoth.Size = New System.Drawing.Size(104, 17)
        Me.rbtnBoth.TabIndex = 2
        Me.rbtnBoth.TabStop = True
        Me.rbtnBoth.Text = "Include Archives"
        Me.rbtnBoth.UseVisualStyleBackColor = True
        '
        'rbtnActive
        '
        Me.rbtnActive.AutoSize = True
        Me.rbtnActive.Location = New System.Drawing.Point(9, 19)
        Me.rbtnActive.Name = "rbtnActive"
        Me.rbtnActive.Size = New System.Drawing.Size(59, 17)
        Me.rbtnActive.TabIndex = 0
        Me.rbtnActive.TabStop = True
        Me.rbtnActive.Text = "Current"
        Me.rbtnActive.UseVisualStyleBackColor = True
        '
        'gpbxSorting
        '
        Me.gpbxSorting.Controls.Add(Me.cmbxOrderBy)
        Me.gpbxSorting.Controls.Add(Me.lblOrderBy)
        Me.gpbxSorting.Controls.Add(Me.cmbxGroupBy)
        Me.gpbxSorting.Controls.Add(Me.lblGroupBy)
        Me.gpbxSorting.Location = New System.Drawing.Point(12, 132)
        Me.gpbxSorting.Name = "gpbxSorting"
        Me.gpbxSorting.Size = New System.Drawing.Size(410, 85)
        Me.gpbxSorting.TabIndex = 2
        Me.gpbxSorting.TabStop = False
        Me.gpbxSorting.Text = "Sorting"
        '
        'cmbxOrderBy
        '
        Me.cmbxOrderBy.FormattingEnabled = True
        Me.cmbxOrderBy.Items.AddRange(New Object() {"None", "Current Status", "Date", "Dealer", "Enquiry Code", "Loan Amount", "Source", "Type of Loan", "Manager"})
        Me.cmbxOrderBy.Location = New System.Drawing.Point(96, 49)
        Me.cmbxOrderBy.Name = "cmbxOrderBy"
        Me.cmbxOrderBy.Size = New System.Drawing.Size(303, 21)
        Me.cmbxOrderBy.TabIndex = 3
        '
        'lblOrderBy
        '
        Me.lblOrderBy.AutoSize = True
        Me.lblOrderBy.Location = New System.Drawing.Point(6, 52)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(48, 13)
        Me.lblOrderBy.TabIndex = 2
        Me.lblOrderBy.Text = "Order By"
        '
        'cmbxGroupBy
        '
        Me.cmbxGroupBy.FormattingEnabled = True
        Me.cmbxGroupBy.Items.AddRange(New Object() {"None", "Manager", "Source", "Current Status", "Dealer", "Type of Loan"})
        Me.cmbxGroupBy.Location = New System.Drawing.Point(96, 22)
        Me.cmbxGroupBy.Name = "cmbxGroupBy"
        Me.cmbxGroupBy.Size = New System.Drawing.Size(303, 21)
        Me.cmbxGroupBy.TabIndex = 1
        '
        'lblGroupBy
        '
        Me.lblGroupBy.AutoSize = True
        Me.lblGroupBy.Location = New System.Drawing.Point(6, 25)
        Me.lblGroupBy.Name = "lblGroupBy"
        Me.lblGroupBy.Size = New System.Drawing.Size(51, 13)
        Me.lblGroupBy.TabIndex = 0
        Me.lblGroupBy.Text = "Group By"
        '
        'gpbxFilter
        '
        Me.gpbxFilter.Controls.Add(Me.ckbxUsers)
        Me.gpbxFilter.Controls.Add(Me.lstbxUsers)
        Me.gpbxFilter.Location = New System.Drawing.Point(12, 223)
        Me.gpbxFilter.Name = "gpbxFilter"
        Me.gpbxFilter.Size = New System.Drawing.Size(410, 116)
        Me.gpbxFilter.TabIndex = 3
        Me.gpbxFilter.TabStop = False
        Me.gpbxFilter.Text = "Filter Information"
        '
        'ckbxUsers
        '
        Me.ckbxUsers.AutoSize = True
        Me.ckbxUsers.Location = New System.Drawing.Point(9, 19)
        Me.ckbxUsers.Name = "ckbxUsers"
        Me.ckbxUsers.Size = New System.Drawing.Size(73, 17)
        Me.ckbxUsers.TabIndex = 3
        Me.ckbxUsers.Text = "Managers"
        Me.ckbxUsers.UseVisualStyleBackColor = True
        '
        'lstbxUsers
        '
        Me.lstbxUsers.DataSource = Me.UsersBindingSource
        Me.lstbxUsers.DisplayMember = "UserName"
        Me.lstbxUsers.Enabled = False
        Me.lstbxUsers.FormattingEnabled = True
        Me.lstbxUsers.Location = New System.Drawing.Point(96, 19)
        Me.lstbxUsers.MultiColumn = True
        Me.lstbxUsers.Name = "lstbxUsers"
        Me.lstbxUsers.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstbxUsers.Size = New System.Drawing.Size(303, 82)
        Me.lstbxUsers.TabIndex = 2
        Me.lstbxUsers.ValueMember = "UserName"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(12, 345)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 4
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(347, 345)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 392)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(434, 22)
        Me.StatusStripMessage.TabIndex = 6
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(83, 17)
        Me.ToolStripStatusLabel1.Text = "Status:  Ready."
        '
        'EnquiryReportSelectionsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 414)
        Me.ControlBox = False
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.gpbxFilter)
        Me.Controls.Add(Me.gpbxSorting)
        Me.Controls.Add(Me.gpbxActiveStatus)
        Me.Controls.Add(Me.gpbxdateRange)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EnquiryReportSelectionsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enquiry Report Selections"
        Me.gpbxdateRange.ResumeLayout(False)
        Me.gpbxdateRange.PerformLayout()
        Me.gpbxActiveStatus.ResumeLayout(False)
        Me.gpbxActiveStatus.PerformLayout()
        Me.gpbxSorting.ResumeLayout(False)
        Me.gpbxSorting.PerformLayout()
        Me.gpbxFilter.ResumeLayout(False)
        Me.gpbxFilter.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gpbxdateRange As System.Windows.Forms.GroupBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents gpbxActiveStatus As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnBoth As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnActive As System.Windows.Forms.RadioButton
    Friend WithEvents gpbxSorting As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxGroupBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroupBy As System.Windows.Forms.Label
    Friend WithEvents cmbxOrderBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents gpbxFilter As System.Windows.Forms.GroupBox
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents lstbxUsers As System.Windows.Forms.ListBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ckbxUsers As System.Windows.Forms.CheckBox
End Class
