﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAddDTSB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FirstNameLabel As System.Windows.Forms.Label
        Dim LastNameLabel As System.Windows.Forms.Label
        Dim DateOfBirthLabel As System.Windows.Forms.Label
        Dim IdVerifiedLabel As System.Windows.Forms.Label
        Dim AddressVerifiedLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormAddDTSB))
        Me.FirstNameTextBox = New System.Windows.Forms.TextBox()
        Me.DTSBBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.LastNameTextBox = New System.Windows.Forms.TextBox()
        Me.DateOfBirthDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.IdVerifiedCheckBox = New System.Windows.Forms.CheckBox()
        Me.AddressVerifiedCheckBox = New System.Windows.Forms.CheckBox()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.DTSBTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter()
        Me.ckbxIsaPEP = New System.Windows.Forms.CheckBox()
        Me.lblIsaPEP = New System.Windows.Forms.Label()
        Me.lblAMLRisk = New System.Windows.Forms.Label()
        Me.cmbxAMLRisk = New System.Windows.Forms.ComboBox()
        Me.EnumAMLRiskBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Enum_AMLRiskTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.Enum_AMLRiskTableAdapter()
        FirstNameLabel = New System.Windows.Forms.Label()
        LastNameLabel = New System.Windows.Forms.Label()
        DateOfBirthLabel = New System.Windows.Forms.Label()
        IdVerifiedLabel = New System.Windows.Forms.Label()
        AddressVerifiedLabel = New System.Windows.Forms.Label()
        CType(Me.DTSBBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnumAMLRiskBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FirstNameLabel
        '
        FirstNameLabel.AutoSize = True
        FirstNameLabel.Location = New System.Drawing.Point(28, 30)
        FirstNameLabel.Name = "FirstNameLabel"
        FirstNameLabel.Size = New System.Drawing.Size(60, 13)
        FirstNameLabel.TabIndex = 18
        FirstNameLabel.Text = "First Name:"
        '
        'LastNameLabel
        '
        LastNameLabel.AutoSize = True
        LastNameLabel.Location = New System.Drawing.Point(28, 56)
        LastNameLabel.Name = "LastNameLabel"
        LastNameLabel.Size = New System.Drawing.Size(61, 13)
        LastNameLabel.TabIndex = 20
        LastNameLabel.Text = "Last Name:"
        '
        'DateOfBirthLabel
        '
        DateOfBirthLabel.AutoSize = True
        DateOfBirthLabel.Location = New System.Drawing.Point(28, 83)
        DateOfBirthLabel.Name = "DateOfBirthLabel"
        DateOfBirthLabel.Size = New System.Drawing.Size(71, 13)
        DateOfBirthLabel.TabIndex = 22
        DateOfBirthLabel.Text = "Date Of Birth:"
        '
        'IdVerifiedLabel
        '
        IdVerifiedLabel.AutoSize = True
        IdVerifiedLabel.Location = New System.Drawing.Point(28, 110)
        IdVerifiedLabel.Name = "IdVerifiedLabel"
        IdVerifiedLabel.Size = New System.Drawing.Size(57, 13)
        IdVerifiedLabel.TabIndex = 24
        IdVerifiedLabel.Text = "Id Verified:"
        '
        'AddressVerifiedLabel
        '
        AddressVerifiedLabel.AutoSize = True
        AddressVerifiedLabel.Location = New System.Drawing.Point(28, 140)
        AddressVerifiedLabel.Name = "AddressVerifiedLabel"
        AddressVerifiedLabel.Size = New System.Drawing.Size(86, 13)
        AddressVerifiedLabel.TabIndex = 26
        AddressVerifiedLabel.Text = "Address Verified:"
        '
        'FirstNameTextBox
        '
        Me.FirstNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DTSBBindingSource, "FirstName", True))
        Me.FirstNameTextBox.Location = New System.Drawing.Point(120, 27)
        Me.FirstNameTextBox.Name = "FirstNameTextBox"
        Me.FirstNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.FirstNameTextBox.TabIndex = 19
        '
        'DTSBBindingSource
        '
        Me.DTSBBindingSource.DataMember = "DTSB"
        Me.DTSBBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LastNameTextBox
        '
        Me.LastNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DTSBBindingSource, "LastName", True))
        Me.LastNameTextBox.Location = New System.Drawing.Point(120, 53)
        Me.LastNameTextBox.Name = "LastNameTextBox"
        Me.LastNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.LastNameTextBox.TabIndex = 21
        '
        'DateOfBirthDateTimePicker
        '
        Me.DateOfBirthDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DTSBBindingSource, "DateOfBirth", True))
        Me.DateOfBirthDateTimePicker.Location = New System.Drawing.Point(120, 79)
        Me.DateOfBirthDateTimePicker.Name = "DateOfBirthDateTimePicker"
        Me.DateOfBirthDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DateOfBirthDateTimePicker.TabIndex = 23
        '
        'IdVerifiedCheckBox
        '
        Me.IdVerifiedCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DTSBBindingSource, "IdVerified", True))
        Me.IdVerifiedCheckBox.Location = New System.Drawing.Point(192, 105)
        Me.IdVerifiedCheckBox.Name = "IdVerifiedCheckBox"
        Me.IdVerifiedCheckBox.Size = New System.Drawing.Size(45, 24)
        Me.IdVerifiedCheckBox.TabIndex = 25
        Me.IdVerifiedCheckBox.UseVisualStyleBackColor = True
        '
        'AddressVerifiedCheckBox
        '
        Me.AddressVerifiedCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DTSBBindingSource, "AddressVerified", True))
        Me.AddressVerifiedCheckBox.Location = New System.Drawing.Point(192, 135)
        Me.AddressVerifiedCheckBox.Name = "AddressVerifiedCheckBox"
        Me.AddressVerifiedCheckBox.Size = New System.Drawing.Size(45, 24)
        Me.AddressVerifiedCheckBox.TabIndex = 27
        Me.AddressVerifiedCheckBox.UseVisualStyleBackColor = True
        '
        'btnInsert
        '
        Me.btnInsert.BackColor = System.Drawing.Color.PaleGreen
        Me.btnInsert.Location = New System.Drawing.Point(12, 239)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(75, 23)
        Me.btnInsert.TabIndex = 30
        Me.btnInsert.Text = "Add"
        Me.btnInsert.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(264, 239)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 29
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(11, 239)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 28
        Me.btnSave.Text = "Save"
        Me.btnSave.UseCompatibleTextRendering = True
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'DTSBTableAdapter
        '
        Me.DTSBTableAdapter.ClearBeforeFill = True
        '
        'ckbxIsaPEP
        '
        Me.ckbxIsaPEP.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DTSBBindingSource, "IsaPEP", True))
        Me.ckbxIsaPEP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ckbxIsaPEP.Location = New System.Drawing.Point(192, 202)
        Me.ckbxIsaPEP.Name = "ckbxIsaPEP"
        Me.ckbxIsaPEP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ckbxIsaPEP.Size = New System.Drawing.Size(45, 14)
        Me.ckbxIsaPEP.TabIndex = 31
        Me.ckbxIsaPEP.UseVisualStyleBackColor = True
        '
        'lblIsaPEP
        '
        Me.lblIsaPEP.AutoSize = True
        Me.lblIsaPEP.Location = New System.Drawing.Point(28, 202)
        Me.lblIsaPEP.Name = "lblIsaPEP"
        Me.lblIsaPEP.Size = New System.Drawing.Size(150, 13)
        Me.lblIsaPEP.TabIndex = 32
        Me.lblIsaPEP.Text = "Is a politically exposed person:"
        '
        'lblAMLRisk
        '
        Me.lblAMLRisk.AutoSize = True
        Me.lblAMLRisk.Location = New System.Drawing.Point(28, 169)
        Me.lblAMLRisk.Name = "lblAMLRisk"
        Me.lblAMLRisk.Size = New System.Drawing.Size(78, 13)
        Me.lblAMLRisk.TabIndex = 33
        Me.lblAMLRisk.Text = "AML/CFT Risk"
        '
        'cmbxAMLRisk
        '
        Me.cmbxAMLRisk.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DTSBBindingSource, "AMLRisk", True))
        Me.cmbxAMLRisk.DataSource = Me.EnumAMLRiskBindingSource
        Me.cmbxAMLRisk.DisplayMember = "Description"
        Me.cmbxAMLRisk.FormattingEnabled = True
        Me.cmbxAMLRisk.Location = New System.Drawing.Point(192, 166)
        Me.cmbxAMLRisk.Name = "cmbxAMLRisk"
        Me.cmbxAMLRisk.Size = New System.Drawing.Size(121, 21)
        Me.cmbxAMLRisk.TabIndex = 34
        Me.cmbxAMLRisk.ValueMember = "Enum"
        '
        'EnumAMLRiskBindingSource
        '
        Me.EnumAMLRiskBindingSource.DataMember = "Enum_AMLRisk"
        Me.EnumAMLRiskBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'Enum_AMLRiskTableAdapter
        '
        Me.Enum_AMLRiskTableAdapter.ClearBeforeFill = True
        '
        'FormAddDTSB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(351, 275)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbxAMLRisk)
        Me.Controls.Add(Me.lblAMLRisk)
        Me.Controls.Add(Me.lblIsaPEP)
        Me.Controls.Add(Me.ckbxIsaPEP)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(FirstNameLabel)
        Me.Controls.Add(Me.FirstNameTextBox)
        Me.Controls.Add(LastNameLabel)
        Me.Controls.Add(Me.LastNameTextBox)
        Me.Controls.Add(DateOfBirthLabel)
        Me.Controls.Add(Me.DateOfBirthDateTimePicker)
        Me.Controls.Add(IdVerifiedLabel)
        Me.Controls.Add(Me.IdVerifiedCheckBox)
        Me.Controls.Add(AddressVerifiedLabel)
        Me.Controls.Add(Me.AddressVerifiedCheckBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormAddDTSB"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add DTSB"
        CType(Me.DTSBBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnumAMLRiskBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FirstNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LastNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateOfBirthDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents IdVerifiedCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents AddressVerifiedCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DTSBBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents DTSBTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
    Friend WithEvents ckbxIsaPEP As System.Windows.Forms.CheckBox
    Friend WithEvents lblIsaPEP As System.Windows.Forms.Label
    Friend WithEvents lblAMLRisk As System.Windows.Forms.Label
    Friend WithEvents cmbxAMLRisk As System.Windows.Forms.ComboBox
    Friend WithEvents EnumAMLRiskBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Enum_AMLRiskTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.Enum_AMLRiskTableAdapter
End Class
