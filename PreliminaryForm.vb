﻿Imports System.IO
Imports System.Runtime.InteropServices

Public Class PreliminaryForm
    'Set Form wide scope variables
    ' set New Enquiry Id 
    Dim thisEnquiryId As Integer = 0
    ' set QRGList Id
    Dim thisEnquiryCode As String
    Dim thisQrgListId As Integer
    'Set New Enquiry Time
    Dim originalTime As DateTime = Date.Now
    'set Status30mins
    Dim status30 As String
    Dim thisLoadEnquiryVar As Integer 'record LoadEnquiryVar
    Dim formExitCheck As Boolean = False 'set switch for formclosing event
    'Dim ThisLoanReason As String
    Dim thisEnquiryType As Integer
    Dim newLocX As Integer
    Dim newLocY As Integer
    '28/03/2017 to fix slow loading of comments textbox
    Dim prelimComments As String = String.Empty
    Dim prelimCurrentLoanAmt As Integer = 0
    Dim progressStatus As Boolean = False
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Load Form
    Friend Sub PassVariable(Optional ByVal loadEnquiryVar As Integer = 0)
        'Remove Handlers
        RemoveHandler txtbxFirstName.TextChanged, AddressOf txtbxFirstName_TextChanged

        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)
        thisLoadEnquiryVar = loadEnquiryVar
        'load lists
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.PrelimSource' table.
            Me.PrelimSourceTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimSource)
            'loads data into the 'EnquiryWorkSheetDataSet.ActiveDealers' table. 
            Me.ActiveDealersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.ActiveDealers)
            'loads data into the 'EnquiryWorkSheetDataSet.EnquiryMethodList' table.
            Me.EnquiryMethodListTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.EnquiryMethodList)

            'load PrelimResults' table.
            Me.PrelimResultsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimResults)
            'load NZTowns' table.
            Me.NZTownsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.NZTowns)
            'load Suburbs table
            Me.SuburbListTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.SuburbList)
            'load LoanPurpose' table.
            Me.LoanPurposeTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.LoanPurpose)
            'load ActiveEnquiryClients' table.
            Me.ActiveEnquiryClientsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.ActiveEnquiryClients)
            'load DisplayAllNames' table. 
            Me.DisplayAllNamesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DisplayAllNames)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        Try
            'set datasource for combo box using the Enum members
            cmbxEnquiryType.DisplayMember = "Value" '10/05/2016 Christopher
            cmbxEnquiryType.DataSource = MyEnums.ToList(GetType(MyEnums.EnquiryType)) '10/05/2016  Christopher

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


        Try
            If thisLoadEnquiryVar = 0 Then
                lblDealerCode.Text = ""

                'Me.LoanPurposeBindingSource.Sort = "Sort" 'causes an error --Sort string contains a property that is not in the IBindingList.--
                'set inital value
                cmbxMethodEnquiry.SelectedIndex = -1
                cmbxPrelimSource.SelectedIndex = -1
                cmbxDealerId.SelectedIndex = -1
                cmbxLastName.SelectedIndex = -1
                cmbxSuburb.SelectedIndex = -1
                cmbxTown.SelectedIndex = -1
                cmbxLoanPurpose.SelectedIndex = -1
                cmbxEnquiryType.SelectedIndex = -1 '10/05/2016 Christopher
                cmbxPrelimResults.SelectedIndex = -1

            Else
                'Enquiry exists
                thisEnquiryId = thisLoadEnquiryVar
                'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
                Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
                If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId") = "" Then
                    lblDealerCode.Text = ""
                End If
                thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
                If Not IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType")) Then
                    cmbxEnquiryType.SelectedIndex = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher
                Else
                    cmbxEnquiryType.SelectedIndex = -1
                End If

                If Not String.IsNullOrEmpty(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanPurpose")) Then
                    cmbxLoanPurpose.SelectedValue = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanPurpose") '10/05/2016 Christopher
                Else
                    cmbxLoanPurpose.SelectedIndex = -1
                End If
                'If Not String.IsNullOrEmpty(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanComments")) Then
                '    prelimComments = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanComments")
                'End If
                prelimComments = Util.GetEnquiryComments(thisEnquiryId)

                If Not String.IsNullOrEmpty(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("PrelimResults")) Then
                    cmbxPrelimResults.SelectedValue = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("PrelimResults")
                Else
                    cmbxPrelimResults.SelectedIndex = -1
                End If

                originalTime = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DateTime")
            End If
            'Set Form title
            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & Me.Text
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'set StatusStrip text
        Try
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Catch ex As Exception
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR:  " & ex.Message
        End Try

    End Sub 'End PassVariable

    Private Sub PreliminaryForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Add Handlers
        AddHandler txtbxFirstName.TextChanged, AddressOf txtbxFirstName_TextChanged
        Me.Activate()
    End Sub 'End Load Form


    'Brings the thread that created the specified window into the foreground and activates the window. 
    'Keyboard input is directed to the window, and various visual cues are changed for the user. 
    'The system assigns a slightly higher priority to the thread that created the foreground window than it does to other threads. 
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function SetForegroundWindow(ByVal hwnd As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function ShowWindow(ByVal hwnd As IntPtr, ByVal cmdshow As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function SetWindowPos(ByVal hwnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As UInteger) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    ''' <summary>
    ''' Validates fields in form
    ''' </summary>
    ''' <param name="checkComment">for Post Comment Validation</param>
    ''' <returns>FieldMessage</returns>
    ''' <remarks></remarks>
    Private Function ValidateFields(Optional ByVal checkComment As Boolean = False) As Util.FieldMessage
        Dim ourMessage As Util.FieldMessage
        ourMessage = New Util.FieldMessage
        Dim checkFieldsMessage As String = String.Empty
        Try
            'Check Field have value
            If cmbxMethodEnquiry.SelectedValue = "" Then
                checkFieldsMessage = "Please select the Method of Enquiry" + vbCrLf
            End If
            If cmbxDealerId.SelectedValue Is Nothing Or cmbxDealerId.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please select Dealer" + vbCrLf
            End If
            If cmbxTitle.SelectedItem = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Title" + vbCrLf
            End If
            If txtbxFirstName.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter First name of enquirer" + vbCrLf
            End If
            If cmbxLastName.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Last name of enquirer" + vbCrLf
            End If
            If txtbxPrelimContact.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Contact number" + vbCrLf
            End If
            If cmbxSuburb.SelectedValue Is Nothing And cmbxSuburb.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please select the Suburb" + vbCrLf
            End If
            If cmbxPrelimSource.SelectedValue = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Contact source" + vbCrLf
            End If
            If cmbxEnquiryType.SelectedValue Is Nothing Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Type of Enquiry" + vbCrLf
            End If
            If Not cmbxEnquiryType.SelectedIndex = MyEnums.EnquiryType.FinanceFacility Then
                If txtbxAmount.Text = "" Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter the Amount wanted to borrow" + vbCrLf
                Else
                    If Not CInt(txtbxAmount.Text) > 0 Then
                        checkFieldsMessage = checkFieldsMessage + "Please enter the Amount wanted to borrow" + vbCrLf
                    End If
                End If
                If cmbxLoanPurpose.SelectedValue Is Nothing Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter Loan Purpose for Enquiry" + vbCrLf
                End If
            End If
            'after comments
            If checkComment = True Then
                If cmbxPrelimResults.SelectedValue Is Nothing Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter the result of this enquiry" + vbCrLf
                End If
            End If
            If checkComment = True Then
                'If Not txtbxPrelimComments.Text.Length > 0 Then
                If String.IsNullOrEmpty(prelimComments) Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter a comment" + vbCrLf
                End If
            End If


        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            'MsgBox(ex.Message)
            ourMessage.FieldError = ex.Message
        End Try

        ourMessage.Message = checkFieldsMessage

        Return ourMessage
    End Function

    Private Sub btnSaveAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndExit.Click
        Cursor.Current = Cursors.WaitCursor
        Dim fieldValidation As Util.FieldMessage = ValidateFields(True)
        SuspendLayout()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        Application.DoEvents()

        If Not fieldValidation.FieldError Then
            If Not fieldValidation.Message = Nothing Then
                MsgBox(fieldValidation.Message)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
            Else
                Cursor.Current = Cursors.WaitCursor
                'SaveEnquiry()
                NewSaveEnquiry()

                If progressStatus = True Then
                    formExitCheck = True
                    'Close this Form
                    Me.Close()

                Else
                    'MsgBox("No record to save, please enter information")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No record to save, please enter information!"

                End If

                Cursor.Current = Cursors.Default

            End If
        Else
            MsgBox(fieldValidation.FieldError)
        End If
        ResumeLayout()
        Cursor.Current = Cursors.Default

    End Sub

    'Private Sub cmbxTitle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxTitle.SelectedIndexChanged

    '    Try
    '        Dim Char1FirstName As String
    '        'Check String has at least 1 Character
    '        If txtbxFirstName.Text.Length >= 1 Then
    '            'Get the  1st Character 
    '            Char1FirstName = txtbxFirstName.Text.Substring(0, 1) & " "
    '        Else
    '            Char1FirstName = ""
    '        End If
    '        txtbxSalutation.Text = cmbxTitle.Text & " " & Char1FirstName & cmbxLastName.Text
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub

    Private Sub txtbxFirstName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxFirstName.TextChanged
        Try
            Dim char1FirstName As String
            'Check String has at least 1 Character
            If txtbxFirstName.Text.Length > 0 Then
                'Get the  1st Character and convert 1st character to upper
                char1FirstName = txtbxFirstName.Text.Substring(0, 1).ToUpper
            Else
                char1FirstName = ""
            End If
            'txtbxSalutation.Text = cmbxTitle.Text & " " & Char1FirstName & cmbxLastName.Text
            txtbxSalutation.Text = char1FirstName & " " & cmbxLastName.Text
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtbxFirstName_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxFirstName.LostFocus
        Dim char1FirstName As String
        Dim restofFirstName As String
        'Check String has at least 1 Character
        If txtbxFirstName.Text.Length > 0 Then
            'Get the  1st Character and convert 1st character to upper
            char1FirstName = txtbxFirstName.Text.Substring(0, 1).ToUpper
            restofFirstName = txtbxFirstName.Text.Substring(1)
            txtbxFirstName.Text = char1FirstName & restofFirstName
        End If
    End Sub


    Private Sub cmbxLastName_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbxLastName.LostFocus
        Dim char1Name As String
        Dim restofName As String
        Dim char1FirstName As String
        'Check String has at least 1 Character
        If cmbxLastName.Text.Length > 0 Then
            'Get the  1st Character and convert 1st character to upper
            char1Name = cmbxLastName.Text.Substring(0, 1).ToUpper
            restofName = cmbxLastName.Text.Substring(1)
            cmbxLastName.Text = char1Name & restofName
            'Now check if to activate Cancel button    
            If thisEnquiryId = 0 Then
                Try
                    Dim columnString As String = "ContactLastName = '" & cmbxLastName.Text.Replace("'", "''") & "'"
                    'SELECT Enquiry.ContactLastName WHERE (dbo.Enquiry.Archived = 0)
                    Dim numberOfOccurences As Integer = CInt(EnquiryWorkSheetDataSet.ActiveEnquiryClients.Compute("Count(ContactLastName)", columnString))
                    If numberOfOccurences > 0 Then
                        'active cancel button
                        btnCancelEnquiry.Visible = True
                        gpbxQuestions.BackColor = Color.MistyRose
                        Me.BackColor = Color.MistyRose
                        Panel2.BackColor = Color.MistyRose
                    Else
                        btnCancelEnquiry.Visible = False
                        gpbxQuestions.BackColor = Color.Empty
                        Me.BackColor = Color.Empty
                        Panel2.BackColor = Color.Empty
                    End If
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
            End If
            'set Salutation            
            Try
                'Check String has at least 1 Character
                If txtbxFirstName.Text.Length > 0 Then
                    'Get the  1st Character 
                    char1FirstName = txtbxFirstName.Text.Substring(0, 1).ToUpper
                Else
                    char1FirstName = ""
                End If
                'Check String has at least 1 Character
                If cmbxLastName.Text.Length > 0 Then
                    'Convert 1st character to upper, previously done
                    txtbxSalutation.Text = char1FirstName & " " & char1Name & restofName
                Else
                    txtbxSalutation.Text = char1FirstName
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

        End If

    End Sub

    ''' <summary>
    ''' Check Names from finPower database (DisplayFinPowerNames table)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCheckNames_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckNames.Click
        'Check Names from finPower database (DisplayFinPowerNames table)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Check String has at least 2 Characters
            If cmbxLastName.Text.Length >= 2 Then
                'Get the String to search for and load Project wide variable
                Try
                    'Call DisplayFinPowerNamesForm
                    Dim displayFinPowerNamesFrm As New DisplayFinPowerNamesForm
                    displayFinPowerNamesFrm.PassVariable(cmbxLastName.Text, txtbxSalutation.Text)
                    displayFinPowerNamesFrm.Show()
                Catch ex As Exception
                    MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
                End Try
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
            End If
        Catch ex As Exception
            log.Error("btnCheckNames ERROR:  " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("btnCheckNames ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub BtnCheckEnquiriesClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckEnquiries.Click
        'Get Names from TrueTrack Enquiries including Archived Enquiries
        Try
            'Check String has at least 2 Characters
            If cmbxLastName.Text.Length >= 2 Then
                'Get the String to search for and load Project wide variable
                Try
                    'Call the Results form
                    Dim displayAllNamesFrm As New DisplayAllNamesForm
                    'restrict results to LastName (full) and Displayname for all enquiries
                    displayAllNamesFrm.PassVariable(cmbxLastName.Text, False, True, txtbxSalutation.Text)
                    displayAllNamesFrm.Show()
                Catch ex As Exception
                    log.Error("SearchForLastNameLike ERROR:  " & ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
                End Try
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  Last Name Value must be at least 2 characters long!"
            End If
        Catch ex As Exception
            log.Error("btnCheckEnquiries ERROR:  " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("btnCheckEnquiries ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnWebApp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWebApp.Click
        Cursor.Current = Cursors.WaitCursor
        Dim fieldValidation As Util.FieldMessage = ValidateFields(True)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."

        If Not fieldValidation.FieldError Then
            If Not fieldValidation.Message = Nothing Then
                MsgBox(fieldValidation.Message)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
            Else

                Cursor.Current = Cursors.WaitCursor
                'SaveEnquiry(True)
                NewSaveEnquiry(True)

                If progressStatus = True Then
                    'Activate the MembersCentre link
                    Dim membersCentreUtilObject As New WebUtil
                    membersCentreUtilObject.MCLink()
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = membersCentreUtilObject.GetLabelForeColor
                    ToolStripStatusLabel1.Text = membersCentreUtilObject.GetLabelText

                    Cursor.Current = Cursors.Default

                    'launch FollowUpForm and pass Enquiry Id
                    Dim followUpFrm1 As New FollowUpForm
                    Try
                        followUpFrm1.PassVariable(thisEnquiryId)
                        followUpFrm1.Show()
                        'set switch for formclosing event
                        formExitCheck = True

                        'Close this Form
                        Me.Close()
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try

                Else
                    'MsgBox("No record to save, please enter information")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No record to save, please enter information!"
                End If

            End If
            Cursor.Current = Cursors.Default
        Else
            MsgBox(fieldValidation.FieldError)
        End If
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub btnWorksheetWizard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWorksheetWizard.Click
        Cursor.Current = Cursors.WaitCursor
        Dim fieldValidation As Util.FieldMessage = ValidateFields(True)

        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        Application.DoEvents()

        If Not fieldValidation.FieldError Then
            If Not fieldValidation.Message = Nothing Then
                MsgBox(fieldValidation.Message)
            Else
                Cursor.Current = Cursors.WaitCursor
                'SaveEnquiry(True)
                NewSaveEnquiry(True)

                If progressStatus = True Then
                    'check id QRGList table entry exists, if not then Insert with ThisEnquiryId to create record
                    Dim insertResult As Integer
                    Try
                        insertResult = Me.QrgListTableAdapter1.InsertWithEnquiryId(Me.EnquiryWorkSheetDataSet.QRGList, thisEnquiryId)
                        If insertResult = 1 Then
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Successfully created New Quick Reference Guide.   Status: Ready."
                            Application.DoEvents()
                        ElseIf insertResult = 0 Then
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Quick Reference Guide already exists.  Status: Ready."
                            Application.DoEvents()
                        Else
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Error creating Quick Reference Guide."
                            Application.DoEvents()
                        End If
                    Catch ex As Exception
                        log.Error("ERROR: QRGListTableAdapter caused an error" & ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("ERROR: QRGListTableAdapter caused an error" & vbCrLf & ex.Message)
                    End Try
                    Try
                        'create Enquiry folder on shared drive
                        ''update Status Strip
                        'ToolStripStatusLabel1.ForeColor = Color.Black
                        'ToolStripStatusLabel1.Text = "Creating Shared Folder."

                        'get WorksheetSharedFolder string
                        Dim wkshtStr As String = Switch.GetWorksheetSharedFolder
                        Dim fname As String = wkshtStr & "\" & thisEnquiryCode
                        If Directory.Exists(wkshtStr) Then 'check the shared folder exists
                            'Determine whether the directory exists.
                            If Directory.Exists(fname) Then
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Blue
                                ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
                                Application.DoEvents()
                            Else
                                'create directory
                                IO.Directory.CreateDirectory(fname)
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Black
                                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
                                Application.DoEvents()
                            End If
                            Cursor.Current = Cursors.WaitCursor
                            'launch WkShtWizForm1 and pass Reference Id
                            Dim wkShtWizFrm1 As New WkShtWizForm1
                            wkShtWizFrm1.PassVariable(thisEnquiryId)
                            wkShtWizFrm1.Show()
                            Cursor.Current = Cursors.Default
                            'set switch for formclosing event
                            formExitCheck = True
                            'Close this Form
                            Me.Close()
                        Else
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Path to Shared Folder does not exist! Check your network connections."
                            Application.DoEvents()
                        End If
                    Catch ex As UnauthorizedAccessException
                        log.Error("UnauthorizedAccessException = " & ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("UnauthorizedAccessException = " & ex.Message)
                    Catch ex As Exception
                        'Console.WriteLine("The process failed: {0}.", e.ToString())
                        log.Error("The process failed: {0}. " & ex.Message & " : " & ex.TargetSite.ToString)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "The process failed: {0}." + ex.ToString()
                        'MsgBox(ex.Message)
                    End Try
                Else
                    'MsgBox("No record to save, please enter information")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No record to save, please enter information!"
                    Cursor.Current = Cursors.Default
                End If



            End If
        Else
            MsgBox(fieldValidation.FieldError)
        End If
        Cursor.Current = Cursors.Default

    End Sub

    'Private Sub cmbxTown_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxTown.SelectedIndexChanged
    '    Try
    '        Dim ChosenTown As String
    '        ChosenTown = CStr(cmbxTown.SelectedValue)
    '        'This line of code loads data into the 'SuburbList' table for ChosenTown
    '        Me.TableAdapterManager.SuburbListTableAdapter.FillByCity(Me.EnquiryWorkSheetDataSet.SuburbList, ChosenTown)
    '        'test
    '        'Dim SetTown As String = EnquiryWorkSheetDataSet.SuburbList.Rows(SuburbListBindingSource.Position()).Item("City")
    '        'MsgBox("Set Town = " & SetTown)
    '        Me.SuburbListBindingSource.ResetBindings(False)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub

    'Form Closing Events
    Private Sub PreliminaryForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'check for FormExitCheck switch
        If formExitCheck = False Then
            Dim fieldValidation As Util.FieldMessage = ValidateFields(True)

            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Checking data."
            Application.DoEvents()

            If Not fieldValidation.FieldError Then
                If Not fieldValidation.Message = Nothing Then
                    MsgBox(fieldValidation.Message)
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Status: Ready."
                    e.Cancel = True
                Else
                    MessageBox.Show("Use 'Save and Exit' button to exit this form.", "Exiting Form", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    e.Cancel = True
                End If
            Else
                MsgBox(fieldValidation.FieldError)
                e.Cancel = True
            End If
        Else
            SuspendLayout()
            'disable handlers
            'RemoveHandler cmbxTown.SelectedIndexChanged, AddressOf cmbxTown_SelectedIndexChanged
            'RemoveHandler cmbxTitle.SelectedIndexChanged, AddressOf cmbxTitle_SelectedIndexChanged

            '******************** check user settings
            'check user location settings
            If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
                newLocX = 0
            Else
                newLocX = Me.Location.X
            End If
            If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
                newLocY = 0
            Else
                newLocY = Me.Location.Y
            End If
            'save user settings
            My.Settings.PrelimFormLocation = New Point(newLocX, newLocY)
            My.Settings.PrelimFormSize = Me.ClientSize
            My.Settings.Save()
            '******************** end of check user settings


        End If

    End Sub

    Private Sub btnAddComment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddComment.Click
        'launch Add Comment Form
        Dim addPrelimCommentFrm As New AddPrelimCommentForm()
        addPrelimCommentFrm.PassVariable(prelimComments)
        Dim result As DialogResult = addPrelimCommentFrm.ShowDialog(Me)
        If result = System.Windows.Forms.DialogResult.OK Then
            prelimComments = addPrelimCommentFrm.PrelimComment
            txtbxPrelimComments.Text = prelimComments
        End If

        'When Add Comment Form closed
        addPrelimCommentFrm.Dispose()

    End Sub

    'Private Sub cmbxSuburb_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxSuburb.SelectedIndexChanged
    '    Try
    '        Dim SetPostcode As String
    '        Dim SetComment As String
    '        'Get Selected Row
    '        Dim SelectedRowView As Data.DataRowView
    '        Dim SelectedRow As EnquiryWorkSheetDataSet.SuburbListRow
    '        SelectedRowView = CType(SuburbListBindingSource.Current, System.Data.DataRowView)
    '        SelectedRow = CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.SuburbListRow)
    '        SetPostcode = SelectedRow.Postcode
    '        txtbxPostCode.Text = SetPostcode
    '        SetComment = SelectedRow.Comments
    '        CommentsTextBox.Text = SetComment

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub

    Private Sub PreliminaryForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height

        lblTitle.Location = New Point((formWidth - lblTitle.Width) / 2, 5)
        Panel2.Size = New Size(formWidth - 16, 86)
        gpbxQuestions.Size = New Size(formWidth - 40, formHeight - 205)
        tlpName.Size = New Size(formWidth - 52, 59)
        tlpQuestions.Size = New Size(formWidth - 52, 130)
        txtbxPrelimComments.Size = New Size(formWidth - 52, formHeight - 496)
        lblResult.Location = New Point(6, formHeight - 226)
        cmbxPrelimResults.Size = New Size(formWidth - 158, 21)
        btnWorksheetWizard.Location = New Point(374 - (960 - formWidth) / 2, 12)


    End Sub


    ''' <summary>
    ''' Handle character input for Loan Amount
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtbxAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbxAmount.KeyPress
        'If (Char.IsControl(e.KeyChar) = False) Then
        '    If (Char.IsDigit(e.KeyChar)) Then
        '        'do nothing
        '    Else
        '        e.Handled = True
        '        MsgBox("Sorry Only Digits (numbers) Allowed!!", _
        '               MsgBoxStyle.Information, "Verify")
        '        txtbxAmount.Focus()
        '    End If
        'End If

        If Not Char.IsControl(e.KeyChar) And Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = "." And Not e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = True
            MsgBox("Sorry Only Digits (numbers) Allowed!!", MsgBoxStyle.Information, "Verify")
            txtbxAmount.Focus()
        Else
            'do nothing
        End If

    End Sub



    Private Sub btnCancelEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelEnquiry.Click

        'launch CancelPrelimEnquiryForm
        Dim cancelPrelimEnquiryFrm As New CancelPrelimEnquiryForm
        'CancelPrelimEnquiryFrm.PassVariable(ThisEnquiryId)
        cancelPrelimEnquiryFrm.ShowDialog(Me)
        If cancelPrelimEnquiryFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

            cancelPrelimEnquiryFrm.Dispose()
            formExitCheck = True
            'Close this Form
            Me.Close()
        Else
            cancelPrelimEnquiryFrm.Dispose()

        End If

    End Sub

#Region "Information"

    Private Sub PbSearchEnquiresClick(sender As Object, e As EventArgs) Handles pbSearchEnquires.Click
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\PreliminaryForm.rtf", "Preliminary Form", MyEnums.MsgBoxMode.FilePath)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()

    End Sub

    Private Sub PbSearchEnquiresMouseHover(sender As Object, e As EventArgs) Handles pbSearchEnquires.MouseHover
        pbSearchEnquires.Image = My.Resources.questionGsm
    End Sub

    Private Sub PbSearchEnquiresMouseLeave(sender As Object, e As EventArgs) Handles pbSearchEnquires.MouseLeave
        pbSearchEnquires.Image = My.Resources.questionBsm
    End Sub

#End Region



    Private Sub cmbxEnquiryType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxEnquiryType.SelectedIndexChanged
        If cmbxEnquiryType.SelectedIndex = MyEnums.EnquiryType.FinanceFacility Then
            lblBorrow.Visible = False
            txtbxAmount.Visible = False
            lblPurpose.Visible = False
            cmbxLoanPurpose.Visible = False
        Else
            lblBorrow.Visible = True
            txtbxAmount.Visible = True
            lblPurpose.Visible = True
            cmbxLoanPurpose.Visible = True
        End If
    End Sub

    ' ''' <summary>
    ' ''' Save Enquiry
    ' ''' </summary>
    ' ''' <param name="getCurrentLoanAmt">Used to trigger if need to ask for CurrentLoanAmt</param>
    ' ''' <remarks></remarks>
    'Private Sub SaveEnquiry(Optional ByVal getCurrentLoanAmt As Boolean = False)

    '    progressStatus = False
    '    Dim prelimMethod As String = cmbxMethodEnquiry.Text
    '    Dim dealerId As String = cmbxDealerId.SelectedValue.ToString
    '    Dim contactTitle As String = cmbxTitle.Text
    '    Dim contactFirstName As String = txtbxFirstName.Text
    '    Dim contactLastName As String = cmbxLastName.Text
    '    Dim contactDisplayName As String = txtbxSalutation.Text
    '    Dim contactSuburb As String = cmbxSuburb.Text
    '    Dim contactCity As String = cmbxTown.Text
    '    Dim contactPhone As String = txtbxPrelimContact.Text
    '    Dim contactEmail As String = txtContactEmail.Text
    '    Dim prelimSource As String = cmbxPrelimSource.Text
    '    Dim enquiryType As Integer = cmbxEnquiryType.SelectedIndex
    '    Dim prelimResult As String = cmbxPrelimResults.Text
    '    Dim loanPurpose As String = cmbxLoanPurpose.Text
    '    Dim pAmount As Decimal
    '    If txtbxAmount.Text.Length > 0 Then
    '        Try
    '            pAmount = CDec(txtbxAmount.Text)
    '        Catch exception As System.OverflowException
    '            MsgBox("ERROR: Preliminary Information: Overflow in string-to-decimal conversion for txtbxAmount.Text value")
    '            Exit Sub
    '        Catch exception As System.FormatException
    '            MsgBox("ERROR: Preliminary Information: The string is not formatted as a decimal for txtbxAmount.Text value")
    '            Exit Sub
    '        Catch exception As System.ArgumentException
    '            MsgBox("ERROR: Preliminary Information: The string is null for txtbxAmount.Text value")
    '            Exit Sub
    '        Catch ex As Exception
    '            MsgBox("ERROR: Preliminary Information: Error converting txtbxAmount.Text value to decimal")
    '            Exit Sub
    '        End Try
    '    Else
    '        pAmount = 0.0
    '    End If

    '    'check if EnquiryId exists
    '    If thisEnquiryId = 0 Then 'no EnquiryId

    '        Try
    '            'Make a new Enquiry
    '            Me.EnquiryTableAdapter.FillByNewEnquiry(Me.EnquiryWorkSheetDataSet.Enquiry, "Preliminary", "Preliminary", LoggedinId, LoggedinBranchId, originalTime)
    '            'Set NewEnquiryId variable
    '            thisEnquiryId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryId")
    '            thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
    '            'update fields
    '            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
    '            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '            enquiryRow.BeginEdit()
    '            enquiryRow.EnquiryType = enquiryType
    '            If Not cmbxEnquiryType.SelectedIndex = MyEnums.EnquiryType.FinanceFacility Then
    '                enquiryRow.PrelimAmount = pAmount
    '                enquiryRow.LoanValue = pAmount
    '                enquiryRow.LoanPurpose = loanPurpose
    '            End If

    '            'update WizardStatus
    '            enquiryRow.WizardStatus = MyEnums.WizardStatus.FollowUp '1
    '            'update dealer Id
    '            enquiryRow.DealerId = dealerId
    '            '18/05/2016 Change Contact to Enquiry Table
    '            enquiryRow.ContactPhone = contactPhone
    '            enquiryRow.ContactEmail = contactEmail
    '            enquiryRow.ContactTitle = contactTitle
    '            enquiryRow.ContactFirstName = contactFirstName
    '            enquiryRow.ContactLastName = contactLastName
    '            enquiryRow.ContactDisplayName = contactDisplayName
    '            enquiryRow.ContactSuburb = contactSuburb
    '            enquiryRow.ContactCity = contactCity
    '            enquiryRow.PrelimSource = prelimSource
    '            enquiryRow.PrelimMethod = prelimMethod
    '            enquiryRow.PrelimResult = prelimResult
    '            'update Current Status
    '            enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_FOLLOW_UP 'changed 3.2.3
    '            'calculate time difference for 30min status
    '            Dim statusTimeDiff As Integer
    '            statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
    '            If statusTimeDiff < 30 Then
    '                enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_FOLLOW_UP 'changed 3.2.3
    '            End If
    '            'Save current changed data
    '            Me.Validate()
    '            EnquiryBindingSource.EndEdit()
    '            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

    '            'create log note
    '            Dim commentStr As String
    '            Dim newCommentStr As String
    '            commentStr = "Source: " & prelimSource & " | Preliminary result: " & prelimResult & vbCrLf & "Status changed to " & Constants.CONST_CURRENT_STATUS_FOLLOW_UP
    '            If Not String.IsNullOrEmpty(prelimComments) Then
    '                newCommentStr = commentStr & vbCrLf & prelimComments
    '            Else
    '                newCommentStr = commentStr
    '            End If
    '            Dim commentId As Integer 'returned by SetComment
    '            commentId = Util.SetComment(thisEnquiryId, LoggedinName, newCommentStr)

    '            If getCurrentLoanAmt = True Then
    '                'check if LoanReason(Type of Loan) is "Personal Loan (Top-up)"
    '                'if is then ask for "CurrentLoanAmt"
    '                If enquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or enquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Then
    '                    'ask for CurrentLoanAmt
    '                    'launch ChangeDealerForm and pass Enquiry Id
    '                    Dim addCurrentLoanAmtFrm As New AddCurrentLoanAmtForm
    '                    addCurrentLoanAmtFrm.PassVariable(thisEnquiryId)
    '                    'ChangeDealerFrm.ShowDialog()
    '                    If (addCurrentLoanAmtFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '                        progressStatus = True
    '                    Else
    '                        'update Status Strip
    '                        ToolStripStatusLabel1.ForeColor = Color.Red
    '                        ToolStripStatusLabel1.Text = "Entering of the Current Loan Amount cancelled, unable to progress.  Status: Ready."
    '                        progressStatus = False
    '                    End If
    '                Else
    '                    progressStatus = True
    '                End If
    '            Else
    '                progressStatus = True
    '            End If
    '            Cursor.Current = Cursors.Default

    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        End Try
    '    Else
    '        Try 'EnquiryId exists
    '            Cursor.Current = Cursors.WaitCursor
    '            'leave if bindingsource.current is nothing
    '            'update fields
    '            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
    '            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '            enquiryRow.BeginEdit()
    '            enquiryRow.EnquiryType = enquiryType
    '            If Not cmbxEnquiryType.SelectedIndex = MyEnums.EnquiryType.FinanceFacility Then
    '                enquiryRow.PrelimAmount = pAmount
    '                enquiryRow.LoanValue = pAmount
    '                enquiryRow.LoanPurpose = loanPurpose
    '            End If
    '            'update WizardStatus
    '            enquiryRow.WizardStatus = MyEnums.WizardStatus.FollowUp '1
    '            'update dealer Id
    '            enquiryRow.DealerId = dealerId
    '            'update Current Status
    '            enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_FOLLOW_UP 'changed 3.2.3
    '            'calculate time difference for 30min status
    '            Dim statusTimeDiff As Integer
    '            statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
    '            If statusTimeDiff < 30 Then
    '                enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_FOLLOW_UP 'changed 3.2.3
    '            End If
    '            'save changes
    '            Me.Validate()
    '            EnquiryBindingSource.EndEdit()
    '            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

    '            'create log note
    '            Dim newCommentStr As String
    '            Dim commentStr As String
    '            commentStr = "Source: " & prelimSource & " | Preliminary result: " & prelimResult & vbCrLf & "Status changed to " & Constants.CONST_CURRENT_STATUS_FOLLOW_UP
    '            If Not String.IsNullOrEmpty(prelimComments) Then
    '                newCommentStr = commentStr & vbCrLf & prelimComments
    '            Else
    '                newCommentStr = commentStr
    '            End If
    '            Dim commentId As Integer
    '            commentId = Util.SetComment(thisEnquiryId, LoggedinName, newCommentStr)

    '            If getCurrentLoanAmt = True Then
    '                'check if LoanReason(Type of Loan) is "Personal Loan (Top-up)"
    '                'if is then ask for "CurrentLoanAmt"
    '                If enquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or enquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Then
    '                    'ask for CurrentLoanAmt
    '                    'launch ChangeDealerForm and pass Enquiry Id
    '                    Dim addCurrentLoanAmtFrm As New AddCurrentLoanAmtForm
    '                    addCurrentLoanAmtFrm.PassVariable(thisEnquiryId)
    '                    'ChangeDealerFrm.ShowDialog()
    '                    If (addCurrentLoanAmtFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '                        progressStatus = True
    '                    Else
    '                        'update Status Strip
    '                        ToolStripStatusLabel1.ForeColor = Color.Red
    '                        ToolStripStatusLabel1.Text = "Entering of the Current Loan Amount cancelled, unable to progress.  Status: Ready."
    '                        progressStatus = False
    '                    End If
    '                Else
    '                    progressStatus = True
    '                End If
    '            Else
    '                progressStatus = True
    '            End If
    '            Cursor.Current = Cursors.Default

    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        End Try
    '    End If
    'End Sub

    ''' <summary>
    ''' Format the loan amount
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtbxAmount_LostFocus(sender As Object, e As EventArgs) Handles txtbxAmount.LostFocus
        Dim tmpDecimal As Decimal
        tmpDecimal = txtbxAmount.Text
        txtbxAmount.Text = [String].Format("{0:n0}", tmpDecimal)
    End Sub

    ''' <summary>
    ''' Save Enquiry
    ''' </summary>
    ''' <param name="getCurrentLoanAmt">Used to trigger if need to ask for CurrentLoanAmt</param>
    ''' <remarks></remarks>
    Private Sub NewSaveEnquiry(Optional ByVal getCurrentLoanAmt As Boolean = False)
        progressStatus = False
        Dim prelimMethod As String = cmbxMethodEnquiry.Text
        Dim dealerId As String = cmbxDealerId.SelectedValue.ToString
        Dim contactTitle As String = cmbxTitle.Text
        Dim contactFirstName As String = txtbxFirstName.Text
        Dim contactLastName As String = cmbxLastName.Text
        Dim contactDisplayName As String = txtbxSalutation.Text
        Dim contactSuburb As String = cmbxSuburb.Text
        Dim contactCity As String = cmbxTown.Text
        Dim contactPhone As String = txtbxPrelimContact.Text
        Dim contactEmail As String = txtContactEmail.Text
        Dim prelimSource As String = cmbxPrelimSource.Text
        Dim enquiryType As Integer = cmbxEnquiryType.SelectedIndex
        Dim prelimResult As String = cmbxPrelimResults.Text
        Dim loanPurpose As String = cmbxLoanPurpose.Text
        Dim pAmount As Decimal
        If txtbxAmount.Text.Length > 0 Then
            Try
                pAmount = CDec(txtbxAmount.Text)
            Catch exception As System.OverflowException
                MsgBox("ERROR: Preliminary Information: Overflow in string-to-decimal conversion for txtbxAmount.Text value")
                Exit Sub
            Catch exception As System.FormatException
                MsgBox("ERROR: Preliminary Information: The string is not formatted as a decimal for txtbxAmount.Text value")
                Exit Sub
            Catch exception As System.ArgumentException
                MsgBox("ERROR: Preliminary Information: The string is null for txtbxAmount.Text value")
                Exit Sub
            Catch ex As Exception
                MsgBox("ERROR: Preliminary Information: Error converting txtbxAmount.Text value to decimal")
                Exit Sub
            End Try
        Else
            pAmount = 0.0
        End If

        If cmbxEnquiryType.SelectedIndex = MyEnums.EnquiryType.FinanceFacility Then
            pAmount = 0.0
            loanPurpose = String.Empty
        End If


        Dim currentStatus As String = Constants.CONST_CURRENT_STATUS_FOLLOW_UP
        Dim status30mins As String = Constants.CONST_CURRENT_STATUS_PRELIMINARY
        'calculate time difference for 30min status
        Dim statusTimeDiff As Integer
        statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
        If statusTimeDiff < 30 Then
            status30mins = Constants.CONST_CURRENT_STATUS_FOLLOW_UP
        End If

        'check if EnquiryId exists
        If thisEnquiryId = 0 Then 'no EnquiryId
            Try
                'Make a new Enquiry
                Me.EnquiryTableAdapter.FillByPreliminaryEnquiry(EnquiryWorkSheetDataSet.Enquiry, enquiryType, pAmount, pAmount, loanPurpose, dealerId, contactPhone, contactEmail, contactTitle, contactFirstName, contactLastName, contactDisplayName, contactSuburb, contactCity, prelimSource, prelimMethod, prelimResult, currentStatus, status30mins, LoggedinId, LoggedinBranchId, originalTime)
                'Set NewEnquiryId variable
                thisEnquiryId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryId")
                thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
                'create log note
                Dim commentStr As String
                Dim newCommentStr As String
                commentStr = "Source: " & prelimSource & " | Preliminary result: " & prelimResult & vbCrLf & "Status changed to " & Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                If Not String.IsNullOrEmpty(prelimComments) Then
                    newCommentStr = commentStr & vbCrLf & prelimComments
                Else
                    newCommentStr = commentStr
                End If
                Dim commentId As Integer 'returned by SetComment
                commentId = Util.SetComment(thisEnquiryId, LoggedinName, newCommentStr)
                'ask for "CurrentLoanAmt?"
                If getCurrentLoanAmt = True Then
                    'check if LoanReason(Type of Loan) is "Personal Loan (Top-up)"
                    'if is then ask for "CurrentLoanAmt"
                    If enquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or enquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Then
                        'ask for CurrentLoanAmt
                        'launch ChangeDealerForm and pass Enquiry Id
                        Dim addCurrentLoanAmtFrm As New AddCurrentLoanAmtForm
                        addCurrentLoanAmtFrm.PassVariable(thisEnquiryId)
                        'ChangeDealerFrm.ShowDialog()
                        If (addCurrentLoanAmtFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                            progressStatus = True
                        Else
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Entering of the Current Loan Amount cancelled, unable to progress.  Status: Ready."
                            progressStatus = False
                        End If
                    Else
                        progressStatus = True
                    End If
                Else
                    progressStatus = True
                End If
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Create Enquiry error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        Else
            Try 'EnquiryId exists
                'leave if bindingsource.current is nothing
                If Not EnquiryBindingSource.Current Is Nothing Then
                    'update fields
                    Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    enquiryRow.BeginEdit()
                    enquiryRow.EnquiryType = enquiryType
                    If Not cmbxEnquiryType.SelectedIndex = MyEnums.EnquiryType.FinanceFacility Then
                        enquiryRow.PrelimAmount = pAmount
                        enquiryRow.LoanValue = pAmount
                        enquiryRow.LoanPurpose = loanPurpose
                    End If
                    'update WizardStatus
                    enquiryRow.WizardStatus = MyEnums.WizardStatus.FollowUp '1
                    'update dealer Id
                    enquiryRow.DealerId = dealerId
                    'update Current Status
                    enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                    'calculate time difference for 30min status
                    statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
                    If statusTimeDiff < 30 Then
                        enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                    End If
                    'save changes
                    Me.Validate()
                    EnquiryBindingSource.EndEdit()
                    'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                    Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                    thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")

                    'create log note
                    Dim newCommentStr As String
                    Dim commentStr As String
                    commentStr = "Source: " & prelimSource & " | Preliminary result: " & prelimResult & vbCrLf & "Status changed to " & Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                    If Not String.IsNullOrEmpty(prelimComments) Then
                        newCommentStr = commentStr & vbCrLf & prelimComments
                    Else
                        newCommentStr = commentStr
                    End If
                    Dim commentId As Integer
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, newCommentStr)

                    If getCurrentLoanAmt = True Then
                        'check if LoanReason(Type of Loan) is "Personal Loan (Top-up)"
                        'if is then ask for "CurrentLoanAmt"
                        If enquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or enquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Or enquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or enquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Then
                            'ask for CurrentLoanAmt
                            'launch ChangeDealerForm and pass Enquiry Id
                            Dim addCurrentLoanAmtFrm As New AddCurrentLoanAmtForm
                            addCurrentLoanAmtFrm.PassVariable(thisEnquiryId)
                            'ChangeDealerFrm.ShowDialog()
                            If (addCurrentLoanAmtFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                                progressStatus = True
                            Else
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Red
                                ToolStripStatusLabel1.Text = "Entering of the Current Loan Amount cancelled, unable to progress.  Status: Ready."
                                progressStatus = False
                            End If
                        Else
                            progressStatus = True
                        End If
                    Else
                        progressStatus = True
                    End If
                Else
                    MessageBox.Show("No Bindingsource found", "Update error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Update error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

    End Sub


End Class