﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRenameFile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRenameFile))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lblExistingName = New System.Windows.Forms.Label()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.txtNewName = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblExistingFileNameExt = New System.Windows.Forms.Label()
        Me.lblFileNameExt = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lbl1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblExistingName, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lbl2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtNewName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblExistingFileNameExt, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblFileNameExt, 2, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(13, 13)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(288, 54)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lbl1
        '
        Me.lbl1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl1.AutoSize = True
        Me.lbl1.Location = New System.Drawing.Point(3, 7)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(87, 13)
        Me.lbl1.TabIndex = 0
        Me.lbl1.Text = "existing file name"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExistingName
        '
        Me.lblExistingName.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblExistingName.AutoSize = True
        Me.lblExistingName.Location = New System.Drawing.Point(201, 7)
        Me.lblExistingName.Name = "lblExistingName"
        Me.lblExistingName.Size = New System.Drawing.Size(39, 13)
        Me.lblExistingName.TabIndex = 1
        Me.lblExistingName.Text = "Label2"
        Me.lblExistingName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl2
        '
        Me.lbl2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl2.AutoSize = True
        Me.lbl2.Location = New System.Drawing.Point(3, 34)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(72, 13)
        Me.lbl2.TabIndex = 2
        Me.lbl2.Text = "new file name"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNewName
        '
        Me.txtNewName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNewName.Location = New System.Drawing.Point(113, 30)
        Me.txtNewName.Name = "txtNewName"
        Me.txtNewName.Size = New System.Drawing.Size(127, 20)
        Me.txtNewName.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.YellowGreen
        Me.btnSave.Location = New System.Drawing.Point(226, 85)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.Location = New System.Drawing.Point(13, 85)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'lblExistingFileNameExt
        '
        Me.lblExistingFileNameExt.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblExistingFileNameExt.AutoSize = True
        Me.lblExistingFileNameExt.Location = New System.Drawing.Point(246, 7)
        Me.lblExistingFileNameExt.Name = "lblExistingFileNameExt"
        Me.lblExistingFileNameExt.Size = New System.Drawing.Size(39, 13)
        Me.lblExistingFileNameExt.TabIndex = 4
        Me.lblExistingFileNameExt.Text = "Label1"
        Me.lblExistingFileNameExt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFileNameExt
        '
        Me.lblFileNameExt.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblFileNameExt.AutoSize = True
        Me.lblFileNameExt.Location = New System.Drawing.Point(246, 34)
        Me.lblFileNameExt.Name = "lblFileNameExt"
        Me.lblFileNameExt.Size = New System.Drawing.Size(39, 13)
        Me.lblFileNameExt.TabIndex = 5
        Me.lblFileNameExt.Text = "Label1"
        Me.lblFileNameExt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmRenameFile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(315, 122)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRenameFile"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Rename your file"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lblExistingName As System.Windows.Forms.Label
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents txtNewName As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblExistingFileNameExt As System.Windows.Forms.Label
    Friend WithEvents lblFileNameExt As System.Windows.Forms.Label
End Class
