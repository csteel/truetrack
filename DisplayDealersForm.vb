﻿Public Class DisplayDealersForm

    Friend Sub PassVariable(Optional ByVal DealerIdLike As String = "")
        Cursor.Current = Cursors.WaitCursor
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.finPowerNames' table.
            Me.DealerContactDetailsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DealerContactDetails)
        Catch ex As Exception
            MsgBox("Filling DisplayFinPowerNames Datatable caused an error:" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
        'set filter
        If DealerIdLike.Length > 0 Then
            txtbxDealerIdFilter.Text = DealerIdLike
            DealerContactDetailsBindingSource.Filter = "DealerId like '" & DealerIdLike & "%'"
        End If

    End Sub

    Private Sub DisplayDealersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

#Region "Filters"
    Private Sub txtbxDealerIdFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxDealerIdFilter.TextChanged
        DealerContactDetailsBindingSource.Filter = "DealerId like '" & txtbxDealerIdFilter.Text & "%'"
    End Sub

    Private Sub txtbxNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxNameFilter.TextChanged
        DealerContactDetailsBindingSource.Filter = "Name like '" & txtbxNameFilter.Text & "%'"
    End Sub

    Private Sub txtbxLegalNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxLegalNameFilter.TextChanged
        DealerContactDetailsBindingSource.Filter = "LegalName like '" & txtbxLegalNameFilter.Text & "%'"
    End Sub

    Private Sub txtbxContactMethodFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxContactMethodFilter.TextChanged
        DealerContactDetailsBindingSource.Filter = "ContactMethod like '" & txtbxContactMethodFilter.Text & "%'"
    End Sub

#End Region

    Private Sub DealerContactDetailsDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DealerContactDetailsDataGridView.DoubleClick
        Cursor.Current = Cursors.WaitCursor
        Try
            If DealerContactDetailsDataGridView.Rows.Count > 0 Then
                Dim row As DataGridViewRow = DealerContactDetailsDataGridView.CurrentRow
                Dim cell0 As DataGridViewCell = row.Cells(0)
                Dim cell4 As DataGridViewCell = row.Cells(4)
                Dim cell5 As DataGridViewCell = row.Cells(5)
                Dim GridDealerId As String = CStr(cell0.Value)
                Dim GridContactMethod As String = cell4.Value
                Dim GridDealerEmail As String = cell5.Value
                'MsgBox("DealerId = " & GridDealerId)
                'MsgBox("DealerContactMethod = " & GridContactMethod)
                'MsgBox("DealerEmail = " & GridDealerEmail)
                If GridContactMethod = "Email" Then
                    Dim EmailFrm As New EmailForm
                    EmailFrm.PassVariable(0, GridDealerId)
                    'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
                    If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                        
                    Else
                        
                    End If
                    EmailFrm.Dispose()
                End If


            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class