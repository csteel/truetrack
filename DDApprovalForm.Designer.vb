﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DDApprovalForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DDApprovalForm))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbxLoanManager = New System.Windows.Forms.ComboBox
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
        Me.btnApprove = New System.Windows.Forms.Button
        Me.btnDeclined = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Loan Manager?"
        Me.Label1.Visible = False
        '
        'cmbxLoanManager
        '
        Me.cmbxLoanManager.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.EnquiryBindingSource, "EnquiryManagerId", True))
        Me.cmbxLoanManager.DataSource = Me.UsersBindingSource
        Me.cmbxLoanManager.DisplayMember = "FullName"
        Me.cmbxLoanManager.FormattingEnabled = True
        Me.cmbxLoanManager.Location = New System.Drawing.Point(101, 9)
        Me.cmbxLoanManager.Name = "cmbxLoanManager"
        Me.cmbxLoanManager.Size = New System.Drawing.Size(128, 21)
        Me.cmbxLoanManager.TabIndex = 1
        Me.cmbxLoanManager.ValueMember = "UserId"
        Me.cmbxLoanManager.Visible = False
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter
        '
        'btnApprove
        '
        Me.btnApprove.BackColor = System.Drawing.Color.LightGreen
        Me.btnApprove.Location = New System.Drawing.Point(38, 167)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.Size = New System.Drawing.Size(75, 23)
        Me.btnApprove.TabIndex = 2
        Me.btnApprove.Text = "Approve"
        Me.btnApprove.UseVisualStyleBackColor = False
        '
        'btnDeclined
        '
        Me.btnDeclined.BackColor = System.Drawing.Color.MistyRose
        Me.btnDeclined.Location = New System.Drawing.Point(190, 167)
        Me.btnDeclined.Name = "btnDeclined"
        Me.btnDeclined.Size = New System.Drawing.Size(75, 23)
        Me.btnDeclined.TabIndex = 3
        Me.btnDeclined.Text = "Declined"
        Me.btnDeclined.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(342, 167)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(235, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "(The person who is credited with this loan.)"
        Me.Label2.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(168, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 17)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "VERIFICATION:"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(72, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(310, 64)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Approving this application I am satisfied the company's criteria has been met and" & _
            " is within my C.A.D."
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DDApprovalForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 214)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDeclined)
        Me.Controls.Add(Me.btnApprove)
        Me.Controls.Add(Me.cmbxLoanManager)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DDApprovalForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Due Diligence Approval"
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbxLoanManager As System.Windows.Forms.ComboBox
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents btnApprove As System.Windows.Forms.Button
    Friend WithEvents btnDeclined As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
