﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddBudgetItemForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddBudgetItemForm))
        Me.cmbxBudgetType = New System.Windows.Forms.ComboBox
        Me.BudgetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet
        Me.lblBudgetType = New System.Windows.Forms.Label
        Me.txtbxDescription = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtbxBudgetAmount = New System.Windows.Forms.TextBox
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.lblError = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnAddBudgetItem = New System.Windows.Forms.Button
        Me.BudgetTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
        Me.lblPerMonth = New System.Windows.Forms.Label
        CType(Me.BudgetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbxBudgetType
        '
        Me.cmbxBudgetType.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BudgetBindingSource, "Type", True))
        Me.cmbxBudgetType.FormattingEnabled = True
        Me.cmbxBudgetType.Items.AddRange(New Object() {"", "expense", "income"})
        Me.cmbxBudgetType.Location = New System.Drawing.Point(12, 25)
        Me.cmbxBudgetType.Name = "cmbxBudgetType"
        Me.cmbxBudgetType.Size = New System.Drawing.Size(97, 21)
        Me.cmbxBudgetType.TabIndex = 1
        '
        'BudgetBindingSource
        '
        Me.BudgetBindingSource.DataMember = "Budget"
        Me.BudgetBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblBudgetType
        '
        Me.lblBudgetType.AutoSize = True
        Me.lblBudgetType.Location = New System.Drawing.Point(12, 9)
        Me.lblBudgetType.Name = "lblBudgetType"
        Me.lblBudgetType.Size = New System.Drawing.Size(68, 13)
        Me.lblBudgetType.TabIndex = 1
        Me.lblBudgetType.Text = "Budget Type"
        '
        'txtbxDescription
        '
        Me.txtbxDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BudgetBindingSource, "Description", True))
        Me.txtbxDescription.Location = New System.Drawing.Point(15, 75)
        Me.txtbxDescription.Name = "txtbxDescription"
        Me.txtbxDescription.Size = New System.Drawing.Size(452, 20)
        Me.txtbxDescription.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Description"
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.Location = New System.Drawing.Point(120, 28)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(43, 13)
        Me.lblAmount.TabIndex = 45
        Me.lblAmount.Text = "Amount"
        '
        'txtbxBudgetAmount
        '
        Me.txtbxBudgetAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BudgetBindingSource, "Value", True))
        Me.txtbxBudgetAmount.Location = New System.Drawing.Point(169, 25)
        Me.txtbxBudgetAmount.Name = "txtbxBudgetAmount"
        Me.txtbxBudgetAmount.Size = New System.Drawing.Size(100, 20)
        Me.txtbxBudgetAmount.TabIndex = 2
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(97, 111)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 5
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        Me.btnUpdate.Visible = False
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Location = New System.Drawing.Point(178, 116)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(56, 13)
        Me.lblError.TabIndex = 66
        Me.lblError.Text = "Error code"
        Me.lblError.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.Location = New System.Drawing.Point(392, 111)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnAddBudgetItem
        '
        Me.btnAddBudgetItem.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddBudgetItem.Location = New System.Drawing.Point(15, 111)
        Me.btnAddBudgetItem.Name = "btnAddBudgetItem"
        Me.btnAddBudgetItem.Size = New System.Drawing.Size(75, 23)
        Me.btnAddBudgetItem.TabIndex = 4
        Me.btnAddBudgetItem.Text = "Add"
        Me.btnAddBudgetItem.UseVisualStyleBackColor = False
        '
        'BudgetTableAdapter
        '
        Me.BudgetTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Me.BudgetTableAdapter
        Me.TableAdapterManager.ClientContactMethodTableAdapter = Nothing
        Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.QueryTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'lblPerMonth
        '
        Me.lblPerMonth.AutoSize = True
        Me.lblPerMonth.Location = New System.Drawing.Point(275, 28)
        Me.lblPerMonth.Name = "lblPerMonth"
        Me.lblPerMonth.Size = New System.Drawing.Size(54, 13)
        Me.lblPerMonth.TabIndex = 68
        Me.lblPerMonth.Text = "per month"
        '
        'AddBudgetItemForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(483, 149)
        Me.Controls.Add(Me.lblPerMonth)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAddBudgetItem)
        Me.Controls.Add(Me.txtbxBudgetAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.txtbxDescription)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblBudgetType)
        Me.Controls.Add(Me.cmbxBudgetType)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AddBudgetItemForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Item Form"
        CType(Me.BudgetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbxBudgetType As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudgetType As System.Windows.Forms.Label
    Friend WithEvents txtbxDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtbxBudgetAmount As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnAddBudgetItem As System.Windows.Forms.Button
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents BudgetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BudgetTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents lblPerMonth As System.Windows.Forms.Label
End Class
