﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddSecurityForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddSecurityForm))
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.txtbxTodayValuer = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnAddSecurity = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtbxLendingPercentage = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtbxTodayValue = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SecurityTypesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SecurityTypesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTypesTableAdapter()
        Me.SecurityTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.lblLendingValue = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.cmbxSecurityType = New System.Windows.Forms.ComboBox()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblSecurityComments = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtbxDescription = New AppWhShtB.SpellBox()
        Me.txtbxInsuranceComments = New AppWhShtB.SpellBox()
        Me.txtbxComments = New AppWhShtB.SpellBox()
        Me.dtpLastvalued = New System.Windows.Forms.DateTimePicker()
        Me.ckbxSecurityValued = New System.Windows.Forms.CheckBox()
        Me.lblInsuranceComments = New System.Windows.Forms.Label()
        Me.ckbxSecurityInsurance = New System.Windows.Forms.CheckBox()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityTypesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.Location = New System.Drawing.Point(518, 368)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 27
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "Security"
        Me.SecurityBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtbxTodayValuer
        '
        Me.txtbxTodayValuer.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "TodayValuer", True))
        Me.txtbxTodayValuer.Location = New System.Drawing.Point(211, 63)
        Me.txtbxTodayValuer.Name = "txtbxTodayValuer"
        Me.txtbxTodayValuer.Size = New System.Drawing.Size(126, 20)
        Me.txtbxTodayValuer.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(211, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 13)
        Me.Label9.TabIndex = 56
        Me.Label9.Text = "Valuer"
        '
        'btnAddSecurity
        '
        Me.btnAddSecurity.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddSecurity.Location = New System.Drawing.Point(12, 368)
        Me.btnAddSecurity.Name = "btnAddSecurity"
        Me.btnAddSecurity.Size = New System.Drawing.Size(75, 23)
        Me.btnAddSecurity.TabIndex = 25
        Me.btnAddSecurity.Text = "Add"
        Me.btnAddSecurity.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 95)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 53
        Me.Label8.Text = "Lending Value"
        '
        'txtbxLendingPercentage
        '
        Me.txtbxLendingPercentage.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "LendingPercentage", True))
        Me.txtbxLendingPercentage.Location = New System.Drawing.Point(451, 63)
        Me.txtbxLendingPercentage.Name = "txtbxLendingPercentage"
        Me.txtbxLendingPercentage.Size = New System.Drawing.Size(65, 20)
        Me.txtbxLendingPercentage.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(452, 47)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "Lending%"
        '
        'txtbxTodayValue
        '
        Me.txtbxTodayValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "TodayValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxTodayValue.Location = New System.Drawing.Point(357, 63)
        Me.txtbxTodayValue.Name = "txtbxTodayValue"
        Me.txtbxTodayValue.Size = New System.Drawing.Size(75, 20)
        Me.txtbxTodayValue.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(354, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 13)
        Me.Label4.TabIndex = 45
        Me.Label4.Text = "Value"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Date Valued"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(211, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Description"
        '
        'SecurityTypesBindingSource
        '
        Me.SecurityTypesBindingSource.DataMember = "SecurityTypes"
        Me.SecurityTypesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Security Type"
        '
        'SecurityTypesTableAdapter
        '
        Me.SecurityTypesTableAdapter.ClearBeforeFill = True
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.SecurityTypesTableAdapter = Me.SecurityTypesTableAdapter
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'lblLendingValue
        '
        Me.lblLendingValue.AutoSize = True
        Me.lblLendingValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "LendingValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLendingValue.Location = New System.Drawing.Point(93, 95)
        Me.lblLendingValue.Name = "lblLendingValue"
        Me.lblLendingValue.Size = New System.Drawing.Size(20, 13)
        Me.lblLendingValue.TabIndex = 62
        Me.lblLendingValue.Text = "LV"
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(95, 368)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 26
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        Me.btnUpdate.Visible = False
        '
        'cmbxSecurityType
        '
        Me.cmbxSecurityType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxSecurityType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxSecurityType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.SecurityBindingSource, "Type", True))
        Me.cmbxSecurityType.DataSource = Me.SecurityTypesBindingSource
        Me.cmbxSecurityType.DisplayMember = "Description"
        Me.cmbxSecurityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbxSecurityType.FormattingEnabled = True
        Me.cmbxSecurityType.Location = New System.Drawing.Point(12, 23)
        Me.cmbxSecurityType.Name = "cmbxSecurityType"
        Me.cmbxSecurityType.Size = New System.Drawing.Size(180, 21)
        Me.cmbxSecurityType.TabIndex = 1
        Me.cmbxSecurityType.ValueMember = "SecurityTypeId"
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 397)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(605, 22)
        Me.StatusStripMessage.TabIndex = 63
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(77, 17)
        Me.ToolStripStatusLabel1.Text = "Status: Ready"
        '
        'lblSecurityComments
        '
        Me.lblSecurityComments.AutoSize = True
        Me.lblSecurityComments.Location = New System.Drawing.Point(12, 130)
        Me.lblSecurityComments.Name = "lblSecurityComments"
        Me.lblSecurityComments.Size = New System.Drawing.Size(127, 13)
        Me.lblSecurityComments.TabIndex = 65
        Me.lblSecurityComments.Text = "Security Value Comments"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtbxDescription)
        Me.Panel1.Controls.Add(Me.txtbxInsuranceComments)
        Me.Panel1.Controls.Add(Me.txtbxComments)
        Me.Panel1.Controls.Add(Me.dtpLastvalued)
        Me.Panel1.Controls.Add(Me.ckbxSecurityValued)
        Me.Panel1.Controls.Add(Me.lblInsuranceComments)
        Me.Panel1.Controls.Add(Me.ckbxSecurityInsurance)
        Me.Panel1.Controls.Add(Me.lblSecurityComments)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cmbxSecurityType)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lblLendingValue)
        Me.Panel1.Controls.Add(Me.txtbxTodayValuer)
        Me.Panel1.Controls.Add(Me.txtbxTodayValue)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtbxLendingPercentage)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(605, 362)
        Me.Panel1.TabIndex = 1
        '
        'txtbxDescription
        '
        Me.txtbxDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "Description", True))
        Me.txtbxDescription.IsReadOnly = False
        Me.txtbxDescription.Location = New System.Drawing.Point(211, 24)
        Me.txtbxDescription.MaxLength = 0
        Me.txtbxDescription.Name = "txtbxDescription"
        Me.txtbxDescription.SelectedText = ""
        Me.txtbxDescription.SelectionLength = 0
        Me.txtbxDescription.SelectionStart = 0
        Me.txtbxDescription.Size = New System.Drawing.Size(382, 20)
        Me.txtbxDescription.TabIndex = 2
        Me.txtbxDescription.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxInsuranceComments
        '
        Me.txtbxInsuranceComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "InsuranceComments", True))
        Me.txtbxInsuranceComments.IsReadOnly = False
        Me.txtbxInsuranceComments.Location = New System.Drawing.Point(12, 263)
        Me.txtbxInsuranceComments.MaxLength = 0
        Me.txtbxInsuranceComments.MultiLine = True
        Me.txtbxInsuranceComments.Name = "txtbxInsuranceComments"
        Me.txtbxInsuranceComments.SelectedText = ""
        Me.txtbxInsuranceComments.SelectionLength = 0
        Me.txtbxInsuranceComments.SelectionStart = 0
        Me.txtbxInsuranceComments.Size = New System.Drawing.Size(581, 70)
        Me.txtbxInsuranceComments.TabIndex = 14
        Me.txtbxInsuranceComments.WordWrap = True
        Me.txtbxInsuranceComments.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxComments
        '
        Me.txtbxComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SecurityBindingSource, "Comments", True))
        Me.txtbxComments.IsReadOnly = False
        Me.txtbxComments.Location = New System.Drawing.Point(12, 146)
        Me.txtbxComments.MaxLength = 0
        Me.txtbxComments.MultiLine = True
        Me.txtbxComments.Name = "txtbxComments"
        Me.txtbxComments.SelectedText = ""
        Me.txtbxComments.SelectionLength = 0
        Me.txtbxComments.SelectionStart = 0
        Me.txtbxComments.Size = New System.Drawing.Size(581, 70)
        Me.txtbxComments.TabIndex = 12
        Me.txtbxComments.WordWrap = True
        Me.txtbxComments.Child = New System.Windows.Controls.TextBox()
        '
        'dtpLastvalued
        '
        Me.dtpLastvalued.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SecurityBindingSource, "LastValued", True))
        Me.dtpLastvalued.Location = New System.Drawing.Point(15, 63)
        Me.dtpLastvalued.Name = "dtpLastvalued"
        Me.dtpLastvalued.Size = New System.Drawing.Size(177, 20)
        Me.dtpLastvalued.TabIndex = 3
        '
        'ckbxSecurityValued
        '
        Me.ckbxSecurityValued.AutoSize = True
        Me.ckbxSecurityValued.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.SecurityBindingSource, "SecurityValued", True))
        Me.ckbxSecurityValued.Location = New System.Drawing.Point(33, 222)
        Me.ckbxSecurityValued.Name = "ckbxSecurityValued"
        Me.ckbxSecurityValued.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxSecurityValued.Size = New System.Drawing.Size(504, 17)
        Me.ckbxSecurityValued.TabIndex = 13
        Me.ckbxSecurityValued.Text = "I am satisfied that 'Today's Value' represents the true value of the security and" & _
    " is within YFL guidelines"
        Me.ckbxSecurityValued.UseVisualStyleBackColor = True
        '
        'lblInsuranceComments
        '
        Me.lblInsuranceComments.AutoSize = True
        Me.lblInsuranceComments.Location = New System.Drawing.Point(12, 247)
        Me.lblInsuranceComments.Name = "lblInsuranceComments"
        Me.lblInsuranceComments.Size = New System.Drawing.Size(147, 13)
        Me.lblInsuranceComments.TabIndex = 68
        Me.lblInsuranceComments.Text = "Security Insurance Comments"
        '
        'ckbxSecurityInsurance
        '
        Me.ckbxSecurityInsurance.AutoSize = True
        Me.ckbxSecurityInsurance.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.SecurityBindingSource, "SecurityInsured", True))
        Me.ckbxSecurityInsurance.Location = New System.Drawing.Point(242, 339)
        Me.ckbxSecurityInsurance.Name = "ckbxSecurityInsurance"
        Me.ckbxSecurityInsurance.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxSecurityInsurance.Size = New System.Drawing.Size(314, 17)
        Me.ckbxSecurityInsurance.TabIndex = 15
        Me.ckbxSecurityInsurance.Text = "I am satisfied the insurance is adequate to cover security loss"
        Me.ckbxSecurityInsurance.UseVisualStyleBackColor = True
        '
        'AddSecurityForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(605, 419)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAddSecurity)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AddSecurityForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add a Security"
        Me.TopMost = True
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityTypesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtbxTodayValuer As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnAddSecurity As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtbxLendingPercentage As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtbxTodayValue As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents SecurityTypesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTypesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTypesTableAdapter
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents lblLendingValue As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents cmbxSecurityType As System.Windows.Forms.ComboBox
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblSecurityComments As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblInsuranceComments As System.Windows.Forms.Label
    Friend WithEvents ckbxSecurityInsurance As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxSecurityValued As System.Windows.Forms.CheckBox
    Friend WithEvents dtpLastvalued As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtbxComments As AppWhShtB.SpellBox
    Friend WithEvents txtbxInsuranceComments As AppWhShtB.SpellBox
    Friend WithEvents txtbxDescription As AppWhShtB.SpellBox
End Class
