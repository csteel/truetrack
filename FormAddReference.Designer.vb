﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAddReference
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim lblRefLabel As System.Windows.Forms.Label
        Dim lblRefPay As System.Windows.Forms.Label
        Dim lblRefSpend As System.Windows.Forms.Label
        Dim lblRefAccount As System.Windows.Forms.Label
        Dim lblRefName As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormAddReference))
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.txtRefComments = New System.Windows.Forms.TextBox()
        Me.txtRefPay = New System.Windows.Forms.TextBox()
        Me.txtRefSpend = New System.Windows.Forms.TextBox()
        Me.txtRefAccount = New System.Windows.Forms.TextBox()
        Me.txtRefName = New System.Windows.Forms.TextBox()
        Me.TradeReferencesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.TradeReferencesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter()
        lblRefLabel = New System.Windows.Forms.Label()
        lblRefPay = New System.Windows.Forms.Label()
        lblRefSpend = New System.Windows.Forms.Label()
        lblRefAccount = New System.Windows.Forms.Label()
        lblRefName = New System.Windows.Forms.Label()
        CType(Me.TradeReferencesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblRefLabel
        '
        lblRefLabel.AutoSize = True
        lblRefLabel.Location = New System.Drawing.Point(23, 122)
        lblRefLabel.Name = "lblRefLabel"
        lblRefLabel.Size = New System.Drawing.Size(192, 13)
        lblRefLabel.TabIndex = 34
        lblRefLabel.Text = "Other comments about this relationship:"
        '
        'lblRefPay
        '
        lblRefPay.AutoSize = True
        lblRefPay.Location = New System.Drawing.Point(108, 96)
        lblRefPay.Name = "lblRefPay"
        lblRefPay.Size = New System.Drawing.Size(107, 13)
        lblRefPay.TabIndex = 32
        lblRefPay.Text = "Do they pay on time?"
        '
        'lblRefSpend
        '
        lblRefSpend.AutoSize = True
        lblRefSpend.Location = New System.Drawing.Point(36, 70)
        lblRefSpend.Name = "lblRefSpend"
        lblRefSpend.Size = New System.Drawing.Size(179, 13)
        lblRefSpend.TabIndex = 30
        lblRefSpend.Text = "How much do they spend (monthly)?"
        '
        'lblRefAccount
        '
        lblRefAccount.AutoSize = True
        lblRefAccount.Location = New System.Drawing.Point(9, 44)
        lblRefAccount.Name = "lblRefAccount"
        lblRefAccount.Size = New System.Drawing.Size(206, 13)
        lblRefAccount.TabIndex = 28
        lblRefAccount.Text = "Do they have a current account with you?"
        '
        'lblRefName
        '
        lblRefName.AutoSize = True
        lblRefName.Location = New System.Drawing.Point(132, 18)
        lblRefName.Name = "lblRefName"
        lblRefName.Size = New System.Drawing.Size(83, 13)
        lblRefName.TabIndex = 26
        lblRefName.Text = "Business Name:"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(192, 191)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 38
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(531, 191)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 37
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(111, 191)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(75, 23)
        Me.btnInsert.TabIndex = 36
        Me.btnInsert.Text = "Create"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'txtRefComments
        '
        Me.txtRefComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Comments", True))
        Me.txtRefComments.Location = New System.Drawing.Point(221, 119)
        Me.txtRefComments.Multiline = True
        Me.txtRefComments.Name = "txtRefComments"
        Me.txtRefComments.Size = New System.Drawing.Size(473, 51)
        Me.txtRefComments.TabIndex = 35
        '
        'txtRefPay
        '
        Me.txtRefPay.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Pay", True))
        Me.txtRefPay.Location = New System.Drawing.Point(221, 93)
        Me.txtRefPay.Name = "txtRefPay"
        Me.txtRefPay.Size = New System.Drawing.Size(208, 20)
        Me.txtRefPay.TabIndex = 33
        '
        'txtRefSpend
        '
        Me.txtRefSpend.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Spend", True))
        Me.txtRefSpend.Location = New System.Drawing.Point(221, 67)
        Me.txtRefSpend.Name = "txtRefSpend"
        Me.txtRefSpend.Size = New System.Drawing.Size(208, 20)
        Me.txtRefSpend.TabIndex = 31
        '
        'txtRefAccount
        '
        Me.txtRefAccount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "Account", True))
        Me.txtRefAccount.Location = New System.Drawing.Point(221, 41)
        Me.txtRefAccount.Name = "txtRefAccount"
        Me.txtRefAccount.Size = New System.Drawing.Size(208, 20)
        Me.txtRefAccount.TabIndex = 29
        '
        'txtRefName
        '
        Me.txtRefName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TradeReferencesBindingSource, "BusinessName", True))
        Me.txtRefName.Location = New System.Drawing.Point(221, 15)
        Me.txtRefName.Name = "txtRefName"
        Me.txtRefName.Size = New System.Drawing.Size(208, 20)
        Me.txtRefName.TabIndex = 27
        '
        'TradeReferencesBindingSource
        '
        Me.TradeReferencesBindingSource.DataMember = "TradeReferences"
        Me.TradeReferencesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TradeReferencesTableAdapter
        '
        Me.TradeReferencesTableAdapter.ClearBeforeFill = True
        '
        'FormAddReference
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(703, 229)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(lblRefLabel)
        Me.Controls.Add(Me.txtRefComments)
        Me.Controls.Add(lblRefPay)
        Me.Controls.Add(Me.txtRefPay)
        Me.Controls.Add(lblRefSpend)
        Me.Controls.Add(Me.txtRefSpend)
        Me.Controls.Add(lblRefAccount)
        Me.Controls.Add(Me.txtRefAccount)
        Me.Controls.Add(lblRefName)
        Me.Controls.Add(Me.txtRefName)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormAddReference"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Trade Reference"
        CType(Me.TradeReferencesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents txtRefComments As System.Windows.Forms.TextBox
    Friend WithEvents txtRefPay As System.Windows.Forms.TextBox
    Friend WithEvents txtRefSpend As System.Windows.Forms.TextBox
    Friend WithEvents txtRefAccount As System.Windows.Forms.TextBox
    Friend WithEvents txtRefName As System.Windows.Forms.TextBox
    Friend WithEvents TradeReferencesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents TradeReferencesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
End Class
