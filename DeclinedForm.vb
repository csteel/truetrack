﻿Public Class DeclinedForm
    Dim thisEnquiryId As Integer
    Dim thisEnquiryResult As String
    Dim thisCurrentStatus As String = ""
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Pass Variable Form Load
    Friend Sub PassVariable(ByVal enquiryIdVar As Integer, ByVal contactSalutationVar As String, ByVal thisEnquiryResultVar As String)
        thisEnquiryId = enquiryIdVar
        thisEnquiryResult = thisEnquiryResultVar
        Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, enquiryIdVar)
        'Set form title
        Me.Text = contactSalutationVar & " - " & Me.Text

        Select Case thisEnquiryResult
            Case Is = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED
                lblDeclinedTitle.Text = "Ending Enquiry"
                lblDeclinedExplain.Text = "Please enter reason for ending the Enquiry"
                btnDecline.Text = "End"
            Case Is = Constants.CONST_CURRENT_STATUS_DECLINED
                lblDeclinedTitle.Text = "Declining Application"
                lblDeclinedExplain.Text = "Please enter reason for declining the Application"
                btnDecline.Text = "Decline"
            Case Is = Constants.CONST_CURRENT_STATUS_WITHDRAWN
                lblDeclinedTitle.Text = "Client Withdrawing Application"
                lblDeclinedExplain.Text = "Please enter reason for client withdrawing the Application"
                btnDecline.Text = "Withdraw"
            Case Else

        End Select

    End Sub


    Private Async Sub btnDecline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        Cursor.Current = Cursors.WaitCursor
        Dim commentId As Integer
        Dim onlineApplicationService As New OnlineApplication
        Dim retTTResult As New TrackResult
        Dim thisApplicationCode As String
        Dim note As String
        'Get Row in Enquiry dataset
        Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
        enquiryDt = enquiryTa.GetDataByEnquiryId(thisEnquiryId)
        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        If enquiryDt.Rows.Count > 0 Then
            enquiryRow = enquiryDt.Rows(0)
            'Application declined
            thisApplicationCode = enquiryRow.ApplicationCode
            note = txtbxDeclineExplain.Text
            Try
                enquiryRow.BeginEdit()
                'update Current Status 
                Select Case thisEnquiryResult
                    Case Is = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED 'Enquiry Ended"
                        thisCurrentStatus = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED
                        'update approval status 0 = nothing, 1 = declined, 2 = approved
                        enquiryRow.ApprovalStatus = MyEnums.ApprovalStatus.Nothing
                    Case Is = Constants.CONST_CURRENT_STATUS_DECLINED 'Declined
                        thisCurrentStatus = Constants.CONST_CURRENT_STATUS_DECLINED
                        'check OAC application status is not same as case, if is then no need to call SetStatus
                        Dim systemCanChangeRemoteStatus As Boolean
                        Dim thisAppsettings As New AppSettings
                        'Check is possible to Change Remote Status
                        systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                        If systemCanChangeRemoteStatus = True Then
                            Dim oacStatusResult As New TrackResult
                            oacStatusResult = Await onlineApplicationService.GetOacStatus(thisApplicationCode)
                            If oacStatusResult.ErrorCode = MyEnums.ResponseErrorCode.None Then
                                If oacStatusResult.ResultIntegerValue = OACWebService.ApplicationStatusType.Declined Then
                                    'no need to call SetStatus
                                Else
                                    enquiryRow.ApprovalStatus = MyEnums.ApprovalStatus.Declined
                                    'Change Status in OAC
                                    Dim actionType As Integer = OACWebService.ApplicationActionType.Decline
                                    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                                    If retTTResult.Status = False Then 'error condition
                                        log.Error(retTTResult.ErrorMessage & " : " & "Change Status in OAC")
                                        'commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                                        MessageBox.Show("There has been an error in the OAC and it has returned a message!" & vbNewLine & retTTResult.ErrorMessage & vbNewLine & vbNewLine & "This process has been aborted. Please contact your administrator.", retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Abort
                                        Me.DialogResult = System.Windows.Forms.DialogResult.Abort
                                    End If
                                End If

                            Else 'error condition
                                log.Error(oacStatusResult.ErrorMessage & " : " & "Get Status in OAC")
                                'continue or abort?
                                Dim msgResult As System.Windows.Forms.DialogResult
                                msgResult = MessageBox.Show("There has been an error in the OAC and it has returned a message!" & vbNewLine & oacStatusResult.ErrorMessage & vbCrLf & vbCrLf & "Do you wish to continue?", oacStatusResult.StatusMessage, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                                If msgResult = System.Windows.Forms.DialogResult.Yes Then
                                    'continue
                                Else
                                    Me.DialogResult = System.Windows.Forms.DialogResult.Abort
                                End If
                            End If
                        End If

                    Case Is = Constants.CONST_CURRENT_STATUS_WITHDRAWN 'Withdrawn
                        thisCurrentStatus = Constants.CONST_CURRENT_STATUS_WITHDRAWN
                        'check OAC application status is not same as case, if is then no need to call SetStatus
                        Dim systemCanChangeRemoteStatus As Boolean
                        Dim thisAppsettings As New AppSettings
                        'Check is possible to Change Remote Status
                        systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                        If systemCanChangeRemoteStatus = True Then
                            Dim oacStatusResult As New TrackResult
                            oacStatusResult = Await onlineApplicationService.GetOacStatus(thisApplicationCode)
                            If oacStatusResult.ErrorCode = MyEnums.ResponseErrorCode.None Then
                                If oacStatusResult.ResultIntegerValue = OACWebService.ApplicationStatusType.Withdrawn Then
                                    'no need to call SetStatus
                                Else
                                    'enquiryRow.ApprovalStatus = MyEnums.ApprovalStatus.Declined
                                    'Change Status in OAC
                                    Dim actionType As Integer = OACWebService.ApplicationActionType.Withdraw
                                    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                                    If retTTResult.Status = False Then 'error condition
                                        log.Error(retTTResult.ErrorMessage & " : " & "Change Status in OAC")
                                        'commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                                        MessageBox.Show("There has been an error in the OAC and it has returned a message!" & vbNewLine & retTTResult.ErrorMessage & vbNewLine & vbNewLine & "This process has been aborted. Please contact your administrator.", retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Abort
                                        Me.DialogResult = System.Windows.Forms.DialogResult.Abort
                                    End If
                                End If
                            Else 'error condition
                                log.Error(oacStatusResult.ErrorMessage & " : " & "Get Status in OAC")
                                'continue or abort?
                                Dim msgResult As System.Windows.Forms.DialogResult
                                msgResult = MessageBox.Show("There has been an error in the OAC and it has returned a message!" & vbNewLine & oacStatusResult.ErrorMessage & vbCrLf & vbCrLf & "Do you wish to continue?", oacStatusResult.StatusMessage, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                                If msgResult = System.Windows.Forms.DialogResult.Yes Then
                                    'continue
                                Else
                                    Me.DialogResult = System.Windows.Forms.DialogResult.Abort
                                End If
                            End If
                        End If



                        'Case Is = Constants.CONST_CURRENT_STATUS_WITHDRAWN 'Withdrawn
                        '        thisCurrentStatus = Constants.CONST_CURRENT_STATUS_WITHDRAWN
                        '        'Check is possible to Change Remote Status
                        '        Dim systemCanChangeRemoteStatus As Boolean
                        '        Dim thisAppsettings As New AppSettings
                        '        systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                        '        If systemCanChangeRemoteStatus = True Then
                        '            'Change Status in OAC
                        '            Dim actionType As Integer = OACWebService.ApplicationActionType.Withdraw
                        '            retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                        '            If retTTResult.Status = False Then 'error condition
                        '                commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                        '                MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        '            End If
                        '        End If
                    Case Else
                        thisCurrentStatus = "Unknown"
                        MsgBox("Status unknown, please try again")
                        Me.DialogResult = System.Windows.Forms.DialogResult.Abort
                End Select

                enquiryRow.WizardStatus = MyEnums.WizardStatus.AppForm
                enquiryRow.ApproverName = LoggedinName
                enquiryRow.CurrentStatus = thisCurrentStatus
                enquiryRow.CurrentStatusSetDate = DateTime.Now
                'Set Original Enquiry Time
                Dim OriginalTime As DateTime
                OriginalTime = enquiryRow.DateTime
                'calculate time difference for 30min status
                Dim StatusTimeDiff As Integer
                StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
                If StatusTimeDiff < 30 Then
                    enquiryRow.Status30mins = thisCurrentStatus
                End If
                'change Active status
                enquiryRow.ActiveStatus = False
                'save changes
                Validate()
                enquiryRow.EndEdit()
                enquiryTa.Update(enquiryDt)
                'EnquiryWorkSheetDataSet.AcceptChanges()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Declined Form", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.DialogResult = System.Windows.Forms.DialogResult.Abort
            End Try

            'create log note
            Dim CommentStr As String = ""
            Select Case thisCurrentStatus
                Case Is = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED
                    CommentStr = txtbxDeclineExplain.Text & vbCrLf & "Ended Enquiry: Status changed to Enquiry Ended"
                Case Is = Constants.CONST_CURRENT_STATUS_DECLINED
                    CommentStr = txtbxDeclineExplain.Text & vbCrLf & "Declined Application: Status changed to Declined"
                Case Is = Constants.CONST_CURRENT_STATUS_WITHDRAWN
                    CommentStr = txtbxDeclineExplain.Text & vbCrLf & "Client withdrawn from Application: Status changed to Withdrawn"
                Case Else
                    CommentStr = txtbxDeclineExplain.Text & vbCrLf & "Status changed to Unknown"
            End Select

            commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)


            Cursor.Current = Cursors.Default
            Me.DialogResult = System.Windows.Forms.DialogResult.OK

        Else
            MessageBox.Show("No Enquiry found!", "Declined Form", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.DialogResult = System.Windows.Forms.DialogResult.Abort
        End If

    End Sub



    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub
End Class