﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmailDealerRemittanceForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EmailDealerRemittanceForm))
        Me.txtbxClient = New System.Windows.Forms.TextBox()
        Me.lblClient = New System.Windows.Forms.Label()
        Me.txtbxDealer = New System.Windows.Forms.TextBox()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblEmailTo = New System.Windows.Forms.Label()
        Me.cmbxEmailTo = New System.Windows.Forms.ComboBox()
        Me.lblAttachments = New System.Windows.Forms.Label()
        Me.txtbxAttachmentsList = New System.Windows.Forms.TextBox()
        Me.btnAttachments = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.lblSubject = New System.Windows.Forms.Label()
        Me.txtbxEmailSubject = New System.Windows.Forms.TextBox()
        Me.txtbxEmailFrom = New System.Windows.Forms.TextBox()
        Me.lblEmailFrom = New System.Windows.Forms.Label()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.DealerContactDetailsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter()
        Me.DealerContactDetailsBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.EnquiryBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.DueDiligenceTA = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DueDiligenceTableAdapter()
        Me.DueDiligenceBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.PayoutTA = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PayoutTableAdapter()
        Me.PayoutBS = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.DealerContactDetailsBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DueDiligenceBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PayoutBS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtbxClient
        '
        Me.txtbxClient.Location = New System.Drawing.Point(82, 55)
        Me.txtbxClient.Name = "txtbxClient"
        Me.txtbxClient.Size = New System.Drawing.Size(439, 20)
        Me.txtbxClient.TabIndex = 65
        '
        'lblClient
        '
        Me.lblClient.AutoSize = True
        Me.lblClient.Location = New System.Drawing.Point(15, 58)
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Size = New System.Drawing.Size(36, 13)
        Me.lblClient.TabIndex = 64
        Me.lblClient.Text = "Client:"
        '
        'txtbxDealer
        '
        Me.txtbxDealer.Location = New System.Drawing.Point(82, 29)
        Me.txtbxDealer.Name = "txtbxDealer"
        Me.txtbxDealer.Size = New System.Drawing.Size(439, 20)
        Me.txtbxDealer.TabIndex = 63
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 32)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 62
        Me.lblDealer.Text = "Dealer:"
        '
        'lblEmailTo
        '
        Me.lblEmailTo.AutoSize = True
        Me.lblEmailTo.Location = New System.Drawing.Point(15, 84)
        Me.lblEmailTo.Name = "lblEmailTo"
        Me.lblEmailTo.Size = New System.Drawing.Size(47, 13)
        Me.lblEmailTo.TabIndex = 57
        Me.lblEmailTo.Text = "Email to:"
        '
        'cmbxEmailTo
        '
        Me.cmbxEmailTo.FormattingEnabled = True
        Me.cmbxEmailTo.Location = New System.Drawing.Point(82, 81)
        Me.cmbxEmailTo.Name = "cmbxEmailTo"
        Me.cmbxEmailTo.Size = New System.Drawing.Size(439, 21)
        Me.cmbxEmailTo.TabIndex = 56
        '
        'lblAttachments
        '
        Me.lblAttachments.AutoSize = True
        Me.lblAttachments.Location = New System.Drawing.Point(15, 530)
        Me.lblAttachments.Name = "lblAttachments"
        Me.lblAttachments.Size = New System.Drawing.Size(66, 13)
        Me.lblAttachments.TabIndex = 55
        Me.lblAttachments.Text = "Attachments"
        '
        'txtbxAttachmentsList
        '
        Me.txtbxAttachmentsList.Location = New System.Drawing.Point(82, 527)
        Me.txtbxAttachmentsList.Name = "txtbxAttachmentsList"
        Me.txtbxAttachmentsList.ReadOnly = True
        Me.txtbxAttachmentsList.Size = New System.Drawing.Size(408, 20)
        Me.txtbxAttachmentsList.TabIndex = 54
        '
        'btnAttachments
        '
        Me.btnAttachments.BackColor = System.Drawing.Color.Transparent
        Me.btnAttachments.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAttachments.Image = Global.AppWhShtB.My.Resources.Resources.paperclip16a
        Me.btnAttachments.Location = New System.Drawing.Point(496, 524)
        Me.btnAttachments.Name = "btnAttachments"
        Me.btnAttachments.Size = New System.Drawing.Size(23, 23)
        Me.btnAttachments.TabIndex = 53
        Me.btnAttachments.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(82, 564)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 49
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSend
        '
        Me.btnSend.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSend.BackColor = System.Drawing.Color.PaleGreen
        Me.btnSend.Location = New System.Drawing.Point(446, 564)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 48
        Me.btnSend.Text = "Send"
        Me.btnSend.UseVisualStyleBackColor = False
        '
        'lblSubject
        '
        Me.lblSubject.AutoSize = True
        Me.lblSubject.Location = New System.Drawing.Point(15, 136)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(46, 13)
        Me.lblSubject.TabIndex = 47
        Me.lblSubject.Text = "Subject:"
        '
        'txtbxEmailSubject
        '
        Me.txtbxEmailSubject.Location = New System.Drawing.Point(82, 133)
        Me.txtbxEmailSubject.Name = "txtbxEmailSubject"
        Me.txtbxEmailSubject.Size = New System.Drawing.Size(439, 20)
        Me.txtbxEmailSubject.TabIndex = 46
        '
        'txtbxEmailFrom
        '
        Me.txtbxEmailFrom.Location = New System.Drawing.Point(82, 107)
        Me.txtbxEmailFrom.Name = "txtbxEmailFrom"
        Me.txtbxEmailFrom.ReadOnly = True
        Me.txtbxEmailFrom.Size = New System.Drawing.Size(439, 20)
        Me.txtbxEmailFrom.TabIndex = 45
        '
        'lblEmailFrom
        '
        Me.lblEmailFrom.AutoSize = True
        Me.lblEmailFrom.Location = New System.Drawing.Point(15, 110)
        Me.lblEmailFrom.Name = "lblEmailFrom"
        Me.lblEmailFrom.Size = New System.Drawing.Size(58, 13)
        Me.lblEmailFrom.TabIndex = 44
        Me.lblEmailFrom.Text = "Email from:"
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 160)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(513, 361)
        Me.WebBrowser1.TabIndex = 69
        '
        'DealerContactDetailsTableAdapter
        '
        Me.DealerContactDetailsTableAdapter.ClearBeforeFill = True
        '
        'DealerContactDetailsBS
        '
        Me.DealerContactDetailsBS.DataMember = "DealerContactDetails"
        Me.DealerContactDetailsBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'EnquiryBS
        '
        Me.EnquiryBS.DataMember = "Enquiry"
        Me.EnquiryBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DueDiligenceTA
        '
        Me.DueDiligenceTA.ClearBeforeFill = True
        '
        'DueDiligenceBS
        '
        Me.DueDiligenceBS.DataMember = "DueDiligence"
        Me.DueDiligenceBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'PayoutTA
        '
        Me.PayoutTA.ClearBeforeFill = True
        '
        'PayoutBS
        '
        Me.PayoutBS.DataMember = "Payout"
        Me.PayoutBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EmailDealerRemittanceForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.EmailRemittanceFormSize
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.txtbxClient)
        Me.Controls.Add(Me.lblClient)
        Me.Controls.Add(Me.txtbxDealer)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblEmailTo)
        Me.Controls.Add(Me.cmbxEmailTo)
        Me.Controls.Add(Me.lblAttachments)
        Me.Controls.Add(Me.txtbxAttachmentsList)
        Me.Controls.Add(Me.btnAttachments)
        Me.Controls.Add(Me.lblSubject)
        Me.Controls.Add(Me.txtbxEmailSubject)
        Me.Controls.Add(Me.txtbxEmailFrom)
        Me.Controls.Add(Me.lblEmailFrom)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "EmailDealerRemittance", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "EmailRemittanceFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.EmailDealerRemittance
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(553, 612)
        Me.Name = "EmailDealerRemittanceForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Email Dealer Remittance"
        CType(Me.DealerContactDetailsBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DueDiligenceBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PayoutBS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtbxClient As System.Windows.Forms.TextBox
    Friend WithEvents lblClient As System.Windows.Forms.Label
    Friend WithEvents txtbxDealer As System.Windows.Forms.TextBox
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblEmailTo As System.Windows.Forms.Label
    Friend WithEvents cmbxEmailTo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAttachments As System.Windows.Forms.Label
    Friend WithEvents txtbxAttachmentsList As System.Windows.Forms.TextBox
    Friend WithEvents btnAttachments As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents lblSubject As System.Windows.Forms.Label
    Friend WithEvents txtbxEmailSubject As System.Windows.Forms.TextBox
    Friend WithEvents txtbxEmailFrom As System.Windows.Forms.TextBox
    Friend WithEvents lblEmailFrom As System.Windows.Forms.Label
    Friend WithEvents DealerContactDetailsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter
    Friend WithEvents DealerContactDetailsBS As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBS As System.Windows.Forms.BindingSource
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents DueDiligenceTA As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DueDiligenceTableAdapter
    Friend WithEvents DueDiligenceBS As System.Windows.Forms.BindingSource
    Friend WithEvents PayoutTA As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PayoutTableAdapter
    Friend WithEvents PayoutBS As System.Windows.Forms.BindingSource
End Class
