﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ValidateXML
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ValidateXML))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvContactsXML = New System.Windows.Forms.DataGridView()
        Me.colCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.lblXMLInstruction = New System.Windows.Forms.Label()
        Me.lstClientName = New System.Windows.Forms.ListBox()
        Me.lblClientName = New System.Windows.Forms.Label()
        Me.lblPostCode = New System.Windows.Forms.Label()
        Me.txtXmlPostCode = New System.Windows.Forms.TextBox()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.txtXmlCity = New System.Windows.Forms.TextBox()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.txtXmlAddress = New System.Windows.Forms.TextBox()
        Me.lblDOB = New System.Windows.Forms.Label()
        Me.txtDOB = New System.Windows.Forms.TextBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.btnCommit = New System.Windows.Forms.Button()
        Me.dgvUpdated = New System.Windows.Forms.DataGridView()
        Me.colRemove = New System.Windows.Forms.DataGridViewImageColumn()
        Me.colContactMethod = New System.Windows.Forms.DataGridViewImageColumn()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gpbxfinPower = New System.Windows.Forms.GroupBox()
        Me.dgvNames = New System.Windows.Forms.DataGridView()
        Me.Check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.lblDatafinPOWER = New System.Windows.Forms.Label()
        Me.lblClientInstruction = New System.Windows.Forms.Label()
        Me.gpbxMC = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtXmlManager = New System.Windows.Forms.TextBox()
        Me.lblXmlManager = New System.Windows.Forms.Label()
        Me.lblPostal = New System.Windows.Forms.Label()
        Me.lblPhysical = New System.Windows.Forms.Label()
        Me.lblPostalSuburb = New System.Windows.Forms.Label()
        Me.txtXmlPostalSuburb = New System.Windows.Forms.TextBox()
        Me.lblPostalPostcode = New System.Windows.Forms.Label()
        Me.txtXmlPostalPostcode = New System.Windows.Forms.TextBox()
        Me.lblPostalCity = New System.Windows.Forms.Label()
        Me.txtXmlPostalCity = New System.Windows.Forms.TextBox()
        Me.ibiPostalAddress = New System.Windows.Forms.Label()
        Me.txtXmlPostalAddress = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtXmlSuburb = New System.Windows.Forms.TextBox()
        Me.gpbxfinPowDetails = New System.Windows.Forms.GroupBox()
        Me.gpbxExistClientDetails = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFinManager = New System.Windows.Forms.TextBox()
        Me.chkFinManager = New System.Windows.Forms.CheckBox()
        Me.lblFinPostalSuburb = New System.Windows.Forms.Label()
        Me.txtFinPostalSuburb = New System.Windows.Forms.TextBox()
        Me.lblPostalAddressText = New System.Windows.Forms.Label()
        Me.chkFinPostalAddress = New System.Windows.Forms.CheckBox()
        Me.lblFinPostalPostcode = New System.Windows.Forms.Label()
        Me.txtFinPostalPostcode = New System.Windows.Forms.TextBox()
        Me.lblFinPostalCity = New System.Windows.Forms.Label()
        Me.txtFinPostalCity = New System.Windows.Forms.TextBox()
        Me.lblFinPostalAddress = New System.Windows.Forms.Label()
        Me.txtFinPostalAddress = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtfinSuburb = New System.Windows.Forms.TextBox()
        Me.lblAddressText = New System.Windows.Forms.Label()
        Me.chkFinAddress = New System.Windows.Forms.CheckBox()
        Me.lblInstruction = New System.Windows.Forms.Label()
        Me.lblContactMethods = New System.Windows.Forms.Label()
        Me.lblfinPostCode = New System.Windows.Forms.Label()
        Me.txtfinPostCode = New System.Windows.Forms.TextBox()
        Me.lblfinCity = New System.Windows.Forms.Label()
        Me.txtfinCity = New System.Windows.Forms.TextBox()
        Me.lblfinAddress = New System.Windows.Forms.Label()
        Me.txtfinAddress = New System.Windows.Forms.TextBox()
        Me.dgvContactMethodFin = New System.Windows.Forms.DataGridView()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gpbxSave = New System.Windows.Forms.GroupBox()
        Me.btnAddtoGrid = New System.Windows.Forms.Button()
        Me.lblUnselectedInstruction = New System.Windows.Forms.Label()
        CType(Me.dgvContactsXML, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvUpdated, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxfinPower.SuspendLayout()
        CType(Me.dgvNames, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxMC.SuspendLayout()
        Me.gpbxfinPowDetails.SuspendLayout()
        Me.gpbxExistClientDetails.SuspendLayout()
        CType(Me.dgvContactMethodFin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxSave.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(400, 124)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Contact Methods"
        '
        'dgvContactsXML
        '
        Me.dgvContactsXML.AllowUserToAddRows = False
        Me.dgvContactsXML.AllowUserToDeleteRows = False
        Me.dgvContactsXML.AllowUserToOrderColumns = True
        Me.dgvContactsXML.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvContactsXML.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvContactsXML.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvContactsXML.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContactsXML.ColumnHeadersVisible = False
        Me.dgvContactsXML.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colCheck})
        Me.dgvContactsXML.Location = New System.Drawing.Point(403, 142)
        Me.dgvContactsXML.MultiSelect = False
        Me.dgvContactsXML.Name = "dgvContactsXML"
        Me.dgvContactsXML.RowHeadersVisible = False
        Me.dgvContactsXML.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        Me.dgvContactsXML.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvContactsXML.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvContactsXML.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContactsXML.Size = New System.Drawing.Size(335, 104)
        Me.dgvContactsXML.TabIndex = 46
        Me.dgvContactsXML.TabStop = False
        '
        'colCheck
        '
        Me.colCheck.Frozen = True
        Me.colCheck.HeaderText = ""
        Me.colCheck.Name = "colCheck"
        Me.colCheck.Width = 30
        '
        'lblXMLInstruction
        '
        Me.lblXMLInstruction.AutoSize = True
        Me.lblXMLInstruction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblXMLInstruction.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblXMLInstruction.Location = New System.Drawing.Point(6, 16)
        Me.lblXMLInstruction.Name = "lblXMLInstruction"
        Me.lblXMLInstruction.Size = New System.Drawing.Size(408, 13)
        Me.lblXMLInstruction.TabIndex = 45
        Me.lblXMLInstruction.Text = "(Select Name to show matches from finPower below and show OAC customer details)"
        '
        'lstClientName
        '
        Me.lstClientName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstClientName.FormattingEnabled = True
        Me.lstClientName.Location = New System.Drawing.Point(16, 49)
        Me.lstClientName.Name = "lstClientName"
        Me.lstClientName.Size = New System.Drawing.Size(344, 56)
        Me.lstClientName.TabIndex = 31
        '
        'lblClientName
        '
        Me.lblClientName.AutoSize = True
        Me.lblClientName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientName.Location = New System.Drawing.Point(23, 33)
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.Size = New System.Drawing.Size(112, 13)
        Me.lblClientName.TabIndex = 30
        Me.lblClientName.Text = "OAC Customer Names"
        '
        'lblPostCode
        '
        Me.lblPostCode.AutoSize = True
        Me.lblPostCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCode.Location = New System.Drawing.Point(250, 157)
        Me.lblPostCode.Name = "lblPostCode"
        Me.lblPostCode.Size = New System.Drawing.Size(53, 13)
        Me.lblPostCode.TabIndex = 15
        Me.lblPostCode.Text = "PostCode"
        '
        'txtXmlPostCode
        '
        Me.txtXmlPostCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlPostCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlPostCode.Location = New System.Drawing.Point(305, 154)
        Me.txtXmlPostCode.MaxLength = 5
        Me.txtXmlPostCode.Name = "txtXmlPostCode"
        Me.txtXmlPostCode.ReadOnly = True
        Me.txtXmlPostCode.Size = New System.Drawing.Size(55, 20)
        Me.txtXmlPostCode.TabIndex = 14
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(144, 157)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(24, 13)
        Me.lblCity.TabIndex = 13
        Me.lblCity.Text = "City"
        '
        'txtXmlCity
        '
        Me.txtXmlCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlCity.Location = New System.Drawing.Point(170, 154)
        Me.txtXmlCity.Name = "txtXmlCity"
        Me.txtXmlCity.ReadOnly = True
        Me.txtXmlCity.Size = New System.Drawing.Size(80, 20)
        Me.txtXmlCity.TabIndex = 12
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(13, 131)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(45, 13)
        Me.lblAddress.TabIndex = 11
        Me.lblAddress.Text = "Address"
        '
        'txtXmlAddress
        '
        Me.txtXmlAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlAddress.Location = New System.Drawing.Point(64, 128)
        Me.txtXmlAddress.MaxLength = 100
        Me.txtXmlAddress.Name = "txtXmlAddress"
        Me.txtXmlAddress.ReadOnly = True
        Me.txtXmlAddress.Size = New System.Drawing.Size(296, 20)
        Me.txtXmlAddress.TabIndex = 10
        '
        'lblDOB
        '
        Me.lblDOB.AutoSize = True
        Me.lblDOB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDOB.Location = New System.Drawing.Point(435, 104)
        Me.lblDOB.Name = "lblDOB"
        Me.lblDOB.Size = New System.Drawing.Size(30, 13)
        Me.lblDOB.TabIndex = 6
        Me.lblDOB.Text = "DOB"
        Me.lblDOB.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDOB
        '
        Me.txtDOB.BackColor = System.Drawing.SystemColors.Window
        Me.txtDOB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDOB.Location = New System.Drawing.Point(471, 101)
        Me.txtDOB.Name = "txtDOB"
        Me.txtDOB.ReadOnly = True
        Me.txtDOB.Size = New System.Drawing.Size(85, 20)
        Me.txtDOB.TabIndex = 5
        Me.txtDOB.TabStop = False
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(407, 78)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 13)
        Me.lblLastName.TabIndex = 4
        Me.lblLastName.Text = "Last Name"
        Me.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLastName
        '
        Me.txtLastName.BackColor = System.Drawing.SystemColors.Window
        Me.txtLastName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.Location = New System.Drawing.Point(471, 75)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.ReadOnly = True
        Me.txtLastName.Size = New System.Drawing.Size(228, 20)
        Me.txtLastName.TabIndex = 3
        Me.txtLastName.TabStop = False
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(408, 52)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(57, 13)
        Me.lblFirstName.TabIndex = 2
        Me.lblFirstName.Text = "First Name"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFirstName
        '
        Me.txtFirstName.BackColor = System.Drawing.SystemColors.Window
        Me.txtFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.Location = New System.Drawing.Point(471, 49)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.ReadOnly = True
        Me.txtFirstName.Size = New System.Drawing.Size(228, 20)
        Me.txtFirstName.TabIndex = 0
        Me.txtFirstName.TabStop = False
        '
        'btnCommit
        '
        Me.btnCommit.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCommit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCommit.Location = New System.Drawing.Point(636, 798)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(123, 28)
        Me.btnCommit.TabIndex = 23
        Me.btnCommit.Text = "Commit"
        Me.btnCommit.UseVisualStyleBackColor = True
        '
        'dgvUpdated
        '
        Me.dgvUpdated.AllowUserToAddRows = False
        Me.dgvUpdated.AllowUserToDeleteRows = False
        Me.dgvUpdated.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUpdated.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvUpdated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUpdated.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colRemove, Me.colContactMethod})
        Me.dgvUpdated.Location = New System.Drawing.Point(6, 19)
        Me.dgvUpdated.MultiSelect = False
        Me.dgvUpdated.Name = "dgvUpdated"
        Me.dgvUpdated.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ScrollBar
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUpdated.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvUpdated.RowHeadersVisible = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgvUpdated.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvUpdated.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUpdated.Size = New System.Drawing.Size(739, 88)
        Me.dgvUpdated.TabIndex = 26
        '
        'colRemove
        '
        Me.colRemove.Frozen = True
        Me.colRemove.HeaderText = ""
        Me.colRemove.Image = Global.AppWhShtB.My.Resources.Resources.delete16
        Me.colRemove.Name = "colRemove"
        Me.colRemove.ReadOnly = True
        Me.colRemove.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colRemove.ToolTipText = "Delete from selection"
        Me.colRemove.Width = 30
        '
        'colContactMethod
        '
        Me.colContactMethod.Frozen = True
        Me.colContactMethod.HeaderText = ""
        Me.colContactMethod.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.colContactMethod.Name = "colContactMethod"
        Me.colContactMethod.ReadOnly = True
        Me.colContactMethod.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colContactMethod.ToolTipText = "Show selected client details"
        Me.colContactMethod.Width = 30
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(478, 798)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(123, 28)
        Me.btnCancel.TabIndex = 28
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'gpbxfinPower
        '
        Me.gpbxfinPower.BackColor = System.Drawing.Color.Thistle
        Me.gpbxfinPower.Controls.Add(Me.dgvNames)
        Me.gpbxfinPower.Controls.Add(Me.lblDatafinPOWER)
        Me.gpbxfinPower.Controls.Add(Me.lblClientInstruction)
        Me.gpbxfinPower.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.gpbxfinPower.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxfinPower.Location = New System.Drawing.Point(15, 264)
        Me.gpbxfinPower.Name = "gpbxfinPower"
        Me.gpbxfinPower.Size = New System.Drawing.Size(751, 142)
        Me.gpbxfinPower.TabIndex = 30
        Me.gpbxfinPower.TabStop = False
        '
        'dgvNames
        '
        Me.dgvNames.AllowUserToAddRows = False
        Me.dgvNames.AllowUserToDeleteRows = False
        Me.dgvNames.AllowUserToOrderColumns = True
        Me.dgvNames.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvNames.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvNames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNames.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Check})
        Me.dgvNames.Location = New System.Drawing.Point(16, 30)
        Me.dgvNames.MultiSelect = False
        Me.dgvNames.Name = "dgvNames"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ScrollBar
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvNames.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvNames.RowHeadersVisible = False
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvNames.RowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvNames.Size = New System.Drawing.Size(722, 105)
        Me.dgvNames.TabIndex = 45
        '
        'Check
        '
        Me.Check.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check.Frozen = True
        Me.Check.HeaderText = ""
        Me.Check.Name = "Check"
        Me.Check.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Check.Width = 30
        '
        'lblDatafinPOWER
        '
        Me.lblDatafinPOWER.AutoSize = True
        Me.lblDatafinPOWER.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatafinPOWER.Location = New System.Drawing.Point(23, -1)
        Me.lblDatafinPOWER.Name = "lblDatafinPOWER"
        Me.lblDatafinPOWER.Size = New System.Drawing.Size(147, 13)
        Me.lblDatafinPOWER.TabIndex = 46
        Me.lblDatafinPOWER.Text = "Matches from finPOWER"
        '
        'lblClientInstruction
        '
        Me.lblClientInstruction.AutoSize = True
        Me.lblClientInstruction.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblClientInstruction.Location = New System.Drawing.Point(23, 14)
        Me.lblClientInstruction.Name = "lblClientInstruction"
        Me.lblClientInstruction.Size = New System.Drawing.Size(300, 13)
        Me.lblClientInstruction.TabIndex = 47
        Me.lblClientInstruction.Text = "(Select checkbox to use existing finPower client in application)"
        '
        'gpbxMC
        '
        Me.gpbxMC.BackColor = System.Drawing.Color.YellowGreen
        Me.gpbxMC.Controls.Add(Me.Label4)
        Me.gpbxMC.Controls.Add(Me.txtXmlManager)
        Me.gpbxMC.Controls.Add(Me.lblXmlManager)
        Me.gpbxMC.Controls.Add(Me.lblPostal)
        Me.gpbxMC.Controls.Add(Me.lblPhysical)
        Me.gpbxMC.Controls.Add(Me.lblPostalSuburb)
        Me.gpbxMC.Controls.Add(Me.txtXmlPostalSuburb)
        Me.gpbxMC.Controls.Add(Me.lblPostalPostcode)
        Me.gpbxMC.Controls.Add(Me.txtXmlPostalPostcode)
        Me.gpbxMC.Controls.Add(Me.lblPostalCity)
        Me.gpbxMC.Controls.Add(Me.txtXmlPostalCity)
        Me.gpbxMC.Controls.Add(Me.ibiPostalAddress)
        Me.gpbxMC.Controls.Add(Me.txtXmlPostalAddress)
        Me.gpbxMC.Controls.Add(Me.dgvContactsXML)
        Me.gpbxMC.Controls.Add(Me.Label2)
        Me.gpbxMC.Controls.Add(Me.txtXmlSuburb)
        Me.gpbxMC.Controls.Add(Me.Label1)
        Me.gpbxMC.Controls.Add(Me.lblPostCode)
        Me.gpbxMC.Controls.Add(Me.txtXmlPostCode)
        Me.gpbxMC.Controls.Add(Me.lblXMLInstruction)
        Me.gpbxMC.Controls.Add(Me.lblCity)
        Me.gpbxMC.Controls.Add(Me.lstClientName)
        Me.gpbxMC.Controls.Add(Me.txtXmlCity)
        Me.gpbxMC.Controls.Add(Me.lblClientName)
        Me.gpbxMC.Controls.Add(Me.lblAddress)
        Me.gpbxMC.Controls.Add(Me.txtFirstName)
        Me.gpbxMC.Controls.Add(Me.txtXmlAddress)
        Me.gpbxMC.Controls.Add(Me.lblFirstName)
        Me.gpbxMC.Controls.Add(Me.txtLastName)
        Me.gpbxMC.Controls.Add(Me.lblLastName)
        Me.gpbxMC.Controls.Add(Me.txtDOB)
        Me.gpbxMC.Controls.Add(Me.lblDOB)
        Me.gpbxMC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxMC.Location = New System.Drawing.Point(15, 0)
        Me.gpbxMC.Name = "gpbxMC"
        Me.gpbxMC.Size = New System.Drawing.Size(751, 263)
        Me.gpbxMC.TabIndex = 31
        Me.gpbxMC.TabStop = False
        Me.gpbxMC.Text = "Customers from OAC"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(505, 124)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(202, 13)
        Me.Label4.TabIndex = 72
        Me.Label4.Text = "(Select the contact methods to be saved)"
        '
        'txtXmlManager
        '
        Me.txtXmlManager.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlManager.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlManager.Location = New System.Drawing.Point(639, 101)
        Me.txtXmlManager.Name = "txtXmlManager"
        Me.txtXmlManager.ReadOnly = True
        Me.txtXmlManager.Size = New System.Drawing.Size(60, 20)
        Me.txtXmlManager.TabIndex = 60
        Me.txtXmlManager.TabStop = False
        '
        'lblXmlManager
        '
        Me.lblXmlManager.AutoSize = True
        Me.lblXmlManager.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblXmlManager.Location = New System.Drawing.Point(584, 104)
        Me.lblXmlManager.Name = "lblXmlManager"
        Me.lblXmlManager.Size = New System.Drawing.Size(49, 13)
        Me.lblXmlManager.TabIndex = 61
        Me.lblXmlManager.Text = "Manager"
        Me.lblXmlManager.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPostal
        '
        Me.lblPostal.AutoSize = True
        Me.lblPostal.Location = New System.Drawing.Point(16, 184)
        Me.lblPostal.Name = "lblPostal"
        Me.lblPostal.Size = New System.Drawing.Size(91, 13)
        Me.lblPostal.TabIndex = 59
        Me.lblPostal.Text = "Postal Address"
        '
        'lblPhysical
        '
        Me.lblPhysical.AutoSize = True
        Me.lblPhysical.Location = New System.Drawing.Point(16, 112)
        Me.lblPhysical.Name = "lblPhysical"
        Me.lblPhysical.Size = New System.Drawing.Size(103, 13)
        Me.lblPhysical.TabIndex = 58
        Me.lblPhysical.Text = "Physical Address"
        '
        'lblPostalSuburb
        '
        Me.lblPostalSuburb.AutoSize = True
        Me.lblPostalSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalSuburb.Location = New System.Drawing.Point(13, 229)
        Me.lblPostalSuburb.Name = "lblPostalSuburb"
        Me.lblPostalSuburb.Size = New System.Drawing.Size(41, 13)
        Me.lblPostalSuburb.TabIndex = 57
        Me.lblPostalSuburb.Text = "Suburb"
        '
        'txtXmlPostalSuburb
        '
        Me.txtXmlPostalSuburb.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlPostalSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlPostalSuburb.Location = New System.Drawing.Point(64, 226)
        Me.txtXmlPostalSuburb.Name = "txtXmlPostalSuburb"
        Me.txtXmlPostalSuburb.ReadOnly = True
        Me.txtXmlPostalSuburb.Size = New System.Drawing.Size(80, 20)
        Me.txtXmlPostalSuburb.TabIndex = 56
        '
        'lblPostalPostcode
        '
        Me.lblPostalPostcode.AutoSize = True
        Me.lblPostalPostcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalPostcode.Location = New System.Drawing.Point(250, 229)
        Me.lblPostalPostcode.Name = "lblPostalPostcode"
        Me.lblPostalPostcode.Size = New System.Drawing.Size(53, 13)
        Me.lblPostalPostcode.TabIndex = 55
        Me.lblPostalPostcode.Text = "PostCode"
        '
        'txtXmlPostalPostcode
        '
        Me.txtXmlPostalPostcode.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlPostalPostcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlPostalPostcode.Location = New System.Drawing.Point(305, 226)
        Me.txtXmlPostalPostcode.MaxLength = 5
        Me.txtXmlPostalPostcode.Name = "txtXmlPostalPostcode"
        Me.txtXmlPostalPostcode.ReadOnly = True
        Me.txtXmlPostalPostcode.Size = New System.Drawing.Size(55, 20)
        Me.txtXmlPostalPostcode.TabIndex = 54
        '
        'lblPostalCity
        '
        Me.lblPostalCity.AutoSize = True
        Me.lblPostalCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalCity.Location = New System.Drawing.Point(144, 229)
        Me.lblPostalCity.Name = "lblPostalCity"
        Me.lblPostalCity.Size = New System.Drawing.Size(24, 13)
        Me.lblPostalCity.TabIndex = 53
        Me.lblPostalCity.Text = "City"
        '
        'txtXmlPostalCity
        '
        Me.txtXmlPostalCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlPostalCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlPostalCity.Location = New System.Drawing.Point(170, 226)
        Me.txtXmlPostalCity.Name = "txtXmlPostalCity"
        Me.txtXmlPostalCity.ReadOnly = True
        Me.txtXmlPostalCity.Size = New System.Drawing.Size(80, 20)
        Me.txtXmlPostalCity.TabIndex = 52
        '
        'ibiPostalAddress
        '
        Me.ibiPostalAddress.AutoSize = True
        Me.ibiPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ibiPostalAddress.Location = New System.Drawing.Point(13, 203)
        Me.ibiPostalAddress.Name = "ibiPostalAddress"
        Me.ibiPostalAddress.Size = New System.Drawing.Size(45, 13)
        Me.ibiPostalAddress.TabIndex = 51
        Me.ibiPostalAddress.Text = "Address"
        '
        'txtXmlPostalAddress
        '
        Me.txtXmlPostalAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlPostalAddress.Location = New System.Drawing.Point(64, 200)
        Me.txtXmlPostalAddress.MaxLength = 100
        Me.txtXmlPostalAddress.Name = "txtXmlPostalAddress"
        Me.txtXmlPostalAddress.ReadOnly = True
        Me.txtXmlPostalAddress.Size = New System.Drawing.Size(296, 20)
        Me.txtXmlPostalAddress.TabIndex = 50
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 157)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "Suburb"
        '
        'txtXmlSuburb
        '
        Me.txtXmlSuburb.BackColor = System.Drawing.SystemColors.Window
        Me.txtXmlSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlSuburb.Location = New System.Drawing.Point(64, 154)
        Me.txtXmlSuburb.Name = "txtXmlSuburb"
        Me.txtXmlSuburb.ReadOnly = True
        Me.txtXmlSuburb.Size = New System.Drawing.Size(80, 20)
        Me.txtXmlSuburb.TabIndex = 48
        '
        'gpbxfinPowDetails
        '
        Me.gpbxfinPowDetails.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxfinPowDetails.Controls.Add(Me.dgvUpdated)
        Me.gpbxfinPowDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxfinPowDetails.Location = New System.Drawing.Point(15, 672)
        Me.gpbxfinPowDetails.Name = "gpbxfinPowDetails"
        Me.gpbxfinPowDetails.Size = New System.Drawing.Size(751, 114)
        Me.gpbxfinPowDetails.TabIndex = 32
        Me.gpbxfinPowDetails.TabStop = False
        Me.gpbxfinPowDetails.Text = "Selections of existing information you are keeping for existing client"
        '
        'gpbxExistClientDetails
        '
        Me.gpbxExistClientDetails.BackColor = System.Drawing.Color.LightSteelBlue
        Me.gpbxExistClientDetails.Controls.Add(Me.Label5)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtFinManager)
        Me.gpbxExistClientDetails.Controls.Add(Me.chkFinManager)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblFinPostalSuburb)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtFinPostalSuburb)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblPostalAddressText)
        Me.gpbxExistClientDetails.Controls.Add(Me.chkFinPostalAddress)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblFinPostalPostcode)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtFinPostalPostcode)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblFinPostalCity)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtFinPostalCity)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblFinPostalAddress)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtFinPostalAddress)
        Me.gpbxExistClientDetails.Controls.Add(Me.Label3)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtfinSuburb)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblAddressText)
        Me.gpbxExistClientDetails.Controls.Add(Me.chkFinAddress)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblInstruction)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblContactMethods)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblfinPostCode)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtfinPostCode)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblfinCity)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtfinCity)
        Me.gpbxExistClientDetails.Controls.Add(Me.lblfinAddress)
        Me.gpbxExistClientDetails.Controls.Add(Me.txtfinAddress)
        Me.gpbxExistClientDetails.Controls.Add(Me.dgvContactMethodFin)
        Me.gpbxExistClientDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxExistClientDetails.Location = New System.Drawing.Point(15, 407)
        Me.gpbxExistClientDetails.Name = "gpbxExistClientDetails"
        Me.gpbxExistClientDetails.Size = New System.Drawing.Size(751, 207)
        Me.gpbxExistClientDetails.TabIndex = 33
        Me.gpbxExistClientDetails.TabStop = False
        Me.gpbxExistClientDetails.Text = "Selected finPower client details"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(550, 181)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(145, 13)
        Me.Label5.TabIndex = 93
        Me.Label5.Text = "(Select to keep this manager)"
        '
        'txtFinManager
        '
        Me.txtFinManager.BackColor = System.Drawing.SystemColors.Window
        Me.txtFinManager.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinManager.Location = New System.Drawing.Point(484, 178)
        Me.txtFinManager.Name = "txtFinManager"
        Me.txtFinManager.ReadOnly = True
        Me.txtFinManager.Size = New System.Drawing.Size(60, 20)
        Me.txtFinManager.TabIndex = 91
        '
        'chkFinManager
        '
        Me.chkFinManager.AutoSize = True
        Me.chkFinManager.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFinManager.Location = New System.Drawing.Point(403, 181)
        Me.chkFinManager.Name = "chkFinManager"
        Me.chkFinManager.Size = New System.Drawing.Size(75, 17)
        Me.chkFinManager.TabIndex = 90
        Me.chkFinManager.Text = "Manager"
        Me.chkFinManager.UseVisualStyleBackColor = True
        '
        'lblFinPostalSuburb
        '
        Me.lblFinPostalSuburb.AutoSize = True
        Me.lblFinPostalSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinPostalSuburb.Location = New System.Drawing.Point(13, 181)
        Me.lblFinPostalSuburb.Name = "lblFinPostalSuburb"
        Me.lblFinPostalSuburb.Size = New System.Drawing.Size(41, 13)
        Me.lblFinPostalSuburb.TabIndex = 89
        Me.lblFinPostalSuburb.Text = "Suburb"
        '
        'txtFinPostalSuburb
        '
        Me.txtFinPostalSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinPostalSuburb.Location = New System.Drawing.Point(64, 178)
        Me.txtFinPostalSuburb.Name = "txtFinPostalSuburb"
        Me.txtFinPostalSuburb.Size = New System.Drawing.Size(80, 20)
        Me.txtFinPostalSuburb.TabIndex = 88
        '
        'lblPostalAddressText
        '
        Me.lblPostalAddressText.AutoSize = True
        Me.lblPostalAddressText.BackColor = System.Drawing.Color.LightSteelBlue
        Me.lblPostalAddressText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalAddressText.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblPostalAddressText.Location = New System.Drawing.Point(21, 116)
        Me.lblPostalAddressText.Name = "lblPostalAddressText"
        Me.lblPostalAddressText.Size = New System.Drawing.Size(324, 13)
        Me.lblPostalAddressText.TabIndex = 86
        Me.lblPostalAddressText.Text = "(Select to use this Address for existing client else leave unchecked)"
        '
        'chkFinPostalAddress
        '
        Me.chkFinPostalAddress.AutoSize = True
        Me.chkFinPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFinPostalAddress.Location = New System.Drawing.Point(24, 134)
        Me.chkFinPostalAddress.Name = "chkFinPostalAddress"
        Me.chkFinPostalAddress.Size = New System.Drawing.Size(110, 17)
        Me.chkFinPostalAddress.TabIndex = 87
        Me.chkFinPostalAddress.Text = "Postal Address"
        Me.chkFinPostalAddress.UseVisualStyleBackColor = True
        '
        'lblFinPostalPostcode
        '
        Me.lblFinPostalPostcode.AutoSize = True
        Me.lblFinPostalPostcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinPostalPostcode.Location = New System.Drawing.Point(250, 181)
        Me.lblFinPostalPostcode.Name = "lblFinPostalPostcode"
        Me.lblFinPostalPostcode.Size = New System.Drawing.Size(53, 13)
        Me.lblFinPostalPostcode.TabIndex = 85
        Me.lblFinPostalPostcode.Text = "PostCode"
        '
        'txtFinPostalPostcode
        '
        Me.txtFinPostalPostcode.BackColor = System.Drawing.SystemColors.Window
        Me.txtFinPostalPostcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinPostalPostcode.Location = New System.Drawing.Point(305, 178)
        Me.txtFinPostalPostcode.Name = "txtFinPostalPostcode"
        Me.txtFinPostalPostcode.ReadOnly = True
        Me.txtFinPostalPostcode.Size = New System.Drawing.Size(55, 20)
        Me.txtFinPostalPostcode.TabIndex = 84
        '
        'lblFinPostalCity
        '
        Me.lblFinPostalCity.AutoSize = True
        Me.lblFinPostalCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinPostalCity.Location = New System.Drawing.Point(144, 181)
        Me.lblFinPostalCity.Name = "lblFinPostalCity"
        Me.lblFinPostalCity.Size = New System.Drawing.Size(24, 13)
        Me.lblFinPostalCity.TabIndex = 83
        Me.lblFinPostalCity.Text = "City"
        '
        'txtFinPostalCity
        '
        Me.txtFinPostalCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtFinPostalCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinPostalCity.Location = New System.Drawing.Point(170, 178)
        Me.txtFinPostalCity.Name = "txtFinPostalCity"
        Me.txtFinPostalCity.ReadOnly = True
        Me.txtFinPostalCity.Size = New System.Drawing.Size(80, 20)
        Me.txtFinPostalCity.TabIndex = 82
        '
        'lblFinPostalAddress
        '
        Me.lblFinPostalAddress.AutoSize = True
        Me.lblFinPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinPostalAddress.Location = New System.Drawing.Point(13, 155)
        Me.lblFinPostalAddress.Name = "lblFinPostalAddress"
        Me.lblFinPostalAddress.Size = New System.Drawing.Size(45, 13)
        Me.lblFinPostalAddress.TabIndex = 81
        Me.lblFinPostalAddress.Text = "Address"
        '
        'txtFinPostalAddress
        '
        Me.txtFinPostalAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtFinPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinPostalAddress.Location = New System.Drawing.Point(64, 152)
        Me.txtFinPostalAddress.Name = "txtFinPostalAddress"
        Me.txtFinPostalAddress.ReadOnly = True
        Me.txtFinPostalAddress.Size = New System.Drawing.Size(296, 20)
        Me.txtFinPostalAddress.TabIndex = 80
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 79
        Me.Label3.Text = "Suburb"
        '
        'txtfinSuburb
        '
        Me.txtfinSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfinSuburb.Location = New System.Drawing.Point(64, 81)
        Me.txtfinSuburb.Name = "txtfinSuburb"
        Me.txtfinSuburb.Size = New System.Drawing.Size(80, 20)
        Me.txtfinSuburb.TabIndex = 78
        '
        'lblAddressText
        '
        Me.lblAddressText.AutoSize = True
        Me.lblAddressText.BackColor = System.Drawing.Color.LightSteelBlue
        Me.lblAddressText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddressText.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblAddressText.Location = New System.Drawing.Point(21, 19)
        Me.lblAddressText.Name = "lblAddressText"
        Me.lblAddressText.Size = New System.Drawing.Size(324, 13)
        Me.lblAddressText.TabIndex = 72
        Me.lblAddressText.Text = "(Select to use this Address for existing client else leave unchecked)"
        '
        'chkFinAddress
        '
        Me.chkFinAddress.AutoSize = True
        Me.chkFinAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFinAddress.Location = New System.Drawing.Point(24, 37)
        Me.chkFinAddress.Name = "chkFinAddress"
        Me.chkFinAddress.Size = New System.Drawing.Size(122, 17)
        Me.chkFinAddress.TabIndex = 74
        Me.chkFinAddress.Text = "Physical Address"
        Me.chkFinAddress.UseVisualStyleBackColor = True
        '
        'lblInstruction
        '
        Me.lblInstruction.AutoSize = True
        Me.lblInstruction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstruction.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblInstruction.Location = New System.Drawing.Point(518, 38)
        Me.lblInstruction.Name = "lblInstruction"
        Me.lblInstruction.Size = New System.Drawing.Size(194, 13)
        Me.lblInstruction.TabIndex = 71
        Me.lblInstruction.Text = "(Select the contact methods to be kept)"
        '
        'lblContactMethods
        '
        Me.lblContactMethods.AutoSize = True
        Me.lblContactMethods.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactMethods.Location = New System.Drawing.Point(400, 37)
        Me.lblContactMethods.Name = "lblContactMethods"
        Me.lblContactMethods.Size = New System.Drawing.Size(103, 13)
        Me.lblContactMethods.TabIndex = 70
        Me.lblContactMethods.Text = "Contact Methods"
        '
        'lblfinPostCode
        '
        Me.lblfinPostCode.AutoSize = True
        Me.lblfinPostCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfinPostCode.Location = New System.Drawing.Point(250, 84)
        Me.lblfinPostCode.Name = "lblfinPostCode"
        Me.lblfinPostCode.Size = New System.Drawing.Size(53, 13)
        Me.lblfinPostCode.TabIndex = 69
        Me.lblfinPostCode.Text = "PostCode"
        '
        'txtfinPostCode
        '
        Me.txtfinPostCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtfinPostCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfinPostCode.Location = New System.Drawing.Point(305, 81)
        Me.txtfinPostCode.Name = "txtfinPostCode"
        Me.txtfinPostCode.ReadOnly = True
        Me.txtfinPostCode.Size = New System.Drawing.Size(55, 20)
        Me.txtfinPostCode.TabIndex = 68
        '
        'lblfinCity
        '
        Me.lblfinCity.AutoSize = True
        Me.lblfinCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfinCity.Location = New System.Drawing.Point(144, 84)
        Me.lblfinCity.Name = "lblfinCity"
        Me.lblfinCity.Size = New System.Drawing.Size(24, 13)
        Me.lblfinCity.TabIndex = 67
        Me.lblfinCity.Text = "City"
        '
        'txtfinCity
        '
        Me.txtfinCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtfinCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfinCity.Location = New System.Drawing.Point(170, 81)
        Me.txtfinCity.Name = "txtfinCity"
        Me.txtfinCity.ReadOnly = True
        Me.txtfinCity.Size = New System.Drawing.Size(80, 20)
        Me.txtfinCity.TabIndex = 66
        '
        'lblfinAddress
        '
        Me.lblfinAddress.AutoSize = True
        Me.lblfinAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfinAddress.Location = New System.Drawing.Point(13, 58)
        Me.lblfinAddress.Name = "lblfinAddress"
        Me.lblfinAddress.Size = New System.Drawing.Size(45, 13)
        Me.lblfinAddress.TabIndex = 65
        Me.lblfinAddress.Text = "Address"
        '
        'txtfinAddress
        '
        Me.txtfinAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtfinAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfinAddress.Location = New System.Drawing.Point(64, 55)
        Me.txtfinAddress.Name = "txtfinAddress"
        Me.txtfinAddress.ReadOnly = True
        Me.txtfinAddress.Size = New System.Drawing.Size(296, 20)
        Me.txtfinAddress.TabIndex = 64
        '
        'dgvContactMethodFin
        '
        Me.dgvContactMethodFin.AllowUserToAddRows = False
        Me.dgvContactMethodFin.AllowUserToDeleteRows = False
        Me.dgvContactMethodFin.AllowUserToOrderColumns = True
        Me.dgvContactMethodFin.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvContactMethodFin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvContactMethodFin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvContactMethodFin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContactMethodFin.ColumnHeadersVisible = False
        Me.dgvContactMethodFin.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewCheckBoxColumn1})
        Me.dgvContactMethodFin.Location = New System.Drawing.Point(403, 55)
        Me.dgvContactMethodFin.MultiSelect = False
        Me.dgvContactMethodFin.Name = "dgvContactMethodFin"
        Me.dgvContactMethodFin.RowHeadersVisible = False
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText
        Me.dgvContactMethodFin.RowsDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvContactMethodFin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvContactMethodFin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContactMethodFin.Size = New System.Drawing.Size(333, 118)
        Me.dgvContactMethodFin.TabIndex = 63
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.HeaderText = ""
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.Width = 30
        '
        'gpbxSave
        '
        Me.gpbxSave.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxSave.Controls.Add(Me.btnAddtoGrid)
        Me.gpbxSave.Controls.Add(Me.lblUnselectedInstruction)
        Me.gpbxSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxSave.Location = New System.Drawing.Point(15, 615)
        Me.gpbxSave.Name = "gpbxSave"
        Me.gpbxSave.Size = New System.Drawing.Size(751, 56)
        Me.gpbxSave.TabIndex = 92
        Me.gpbxSave.TabStop = False
        Me.gpbxSave.Text = "Save"
        '
        'btnAddtoGrid
        '
        Me.btnAddtoGrid.BackColor = System.Drawing.Color.YellowGreen
        Me.btnAddtoGrid.FlatAppearance.BorderColor = System.Drawing.Color.Red
        Me.btnAddtoGrid.FlatAppearance.BorderSize = 2
        Me.btnAddtoGrid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddtoGrid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAddtoGrid.Location = New System.Drawing.Point(20, 14)
        Me.btnAddtoGrid.Name = "btnAddtoGrid"
        Me.btnAddtoGrid.Size = New System.Drawing.Size(206, 35)
        Me.btnAddtoGrid.TabIndex = 77
        Me.btnAddtoGrid.Text = "Click to save your selections (if any) and use the selected finPower client"
        Me.btnAddtoGrid.UseVisualStyleBackColor = False
        '
        'lblUnselectedInstruction
        '
        Me.lblUnselectedInstruction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnselectedInstruction.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblUnselectedInstruction.Location = New System.Drawing.Point(244, 15)
        Me.lblUnselectedInstruction.Name = "lblUnselectedInstruction"
        Me.lblUnselectedInstruction.Size = New System.Drawing.Size(486, 26)
        Me.lblUnselectedInstruction.TabIndex = 73
        Me.lblUnselectedInstruction.Text = "The customer to will use their OAC details unless you have selected an existing c" & _
    "lient in 'Matches from finPOWER', optionally selected existing details and then " & _
    "saved your selections."
        '
        'ValidateXML
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 832)
        Me.Controls.Add(Me.gpbxfinPower)
        Me.Controls.Add(Me.gpbxSave)
        Me.Controls.Add(Me.gpbxExistClientDetails)
        Me.Controls.Add(Me.gpbxfinPowDetails)
        Me.Controls.Add(Me.gpbxMC)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnCommit)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ValidateXML"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Possible Client matches for importing into finPower"
        CType(Me.dgvContactsXML, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvUpdated, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxfinPower.ResumeLayout(False)
        Me.gpbxfinPower.PerformLayout()
        CType(Me.dgvNames, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxMC.ResumeLayout(False)
        Me.gpbxMC.PerformLayout()
        Me.gpbxfinPowDetails.ResumeLayout(False)
        Me.gpbxExistClientDetails.ResumeLayout(False)
        Me.gpbxExistClientDetails.PerformLayout()
        CType(Me.dgvContactMethodFin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxSave.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblDOB As System.Windows.Forms.Label
    Friend WithEvents txtDOB As System.Windows.Forms.TextBox
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents lblPostCode As System.Windows.Forms.Label
    Friend WithEvents txtXmlPostCode As System.Windows.Forms.TextBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtXmlCity As System.Windows.Forms.TextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtXmlAddress As System.Windows.Forms.TextBox
    Friend WithEvents btnCommit As System.Windows.Forms.Button
    Friend WithEvents dgvUpdated As System.Windows.Forms.DataGridView
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lstClientName As System.Windows.Forms.ListBox
    Friend WithEvents lblClientName As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblXMLInstruction As System.Windows.Forms.Label
    Friend WithEvents gpbxfinPower As System.Windows.Forms.GroupBox
    Friend WithEvents dgvNames As System.Windows.Forms.DataGridView
    Friend WithEvents lblDatafinPOWER As System.Windows.Forms.Label
    Friend WithEvents lblClientInstruction As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvContactsXML As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxMC As System.Windows.Forms.GroupBox
    Friend WithEvents gpbxfinPowDetails As System.Windows.Forms.GroupBox
    Friend WithEvents gpbxExistClientDetails As System.Windows.Forms.GroupBox
    Friend WithEvents lblUnselectedInstruction As System.Windows.Forms.Label
    Friend WithEvents btnAddtoGrid As System.Windows.Forms.Button
    Friend WithEvents lblAddressText As System.Windows.Forms.Label
    Friend WithEvents chkFinAddress As System.Windows.Forms.CheckBox
    Friend WithEvents lblInstruction As System.Windows.Forms.Label
    Friend WithEvents lblContactMethods As System.Windows.Forms.Label
    Friend WithEvents lblfinPostCode As System.Windows.Forms.Label
    Friend WithEvents txtfinPostCode As System.Windows.Forms.TextBox
    Friend WithEvents lblfinCity As System.Windows.Forms.Label
    Friend WithEvents txtfinCity As System.Windows.Forms.TextBox
    Friend WithEvents lblfinAddress As System.Windows.Forms.Label
    Friend WithEvents txtfinAddress As System.Windows.Forms.TextBox
    Friend WithEvents dgvContactMethodFin As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtXmlSuburb As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtfinSuburb As System.Windows.Forms.TextBox
    Friend WithEvents colCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lblPostal As System.Windows.Forms.Label
    Friend WithEvents lblPhysical As System.Windows.Forms.Label
    Friend WithEvents lblPostalSuburb As System.Windows.Forms.Label
    Friend WithEvents txtXmlPostalSuburb As System.Windows.Forms.TextBox
    Friend WithEvents lblPostalPostcode As System.Windows.Forms.Label
    Friend WithEvents txtXmlPostalPostcode As System.Windows.Forms.TextBox
    Friend WithEvents lblPostalCity As System.Windows.Forms.Label
    Friend WithEvents txtXmlPostalCity As System.Windows.Forms.TextBox
    Friend WithEvents ibiPostalAddress As System.Windows.Forms.Label
    Friend WithEvents txtXmlPostalAddress As System.Windows.Forms.TextBox
    Friend WithEvents lblFinPostalSuburb As System.Windows.Forms.Label
    Friend WithEvents txtFinPostalSuburb As System.Windows.Forms.TextBox
    Friend WithEvents lblPostalAddressText As System.Windows.Forms.Label
    Friend WithEvents chkFinPostalAddress As System.Windows.Forms.CheckBox
    Friend WithEvents lblFinPostalPostcode As System.Windows.Forms.Label
    Friend WithEvents txtFinPostalPostcode As System.Windows.Forms.TextBox
    Friend WithEvents lblFinPostalCity As System.Windows.Forms.Label
    Friend WithEvents txtFinPostalCity As System.Windows.Forms.TextBox
    Friend WithEvents lblFinPostalAddress As System.Windows.Forms.Label
    Friend WithEvents txtFinPostalAddress As System.Windows.Forms.TextBox
    Friend WithEvents Check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents txtXmlManager As System.Windows.Forms.TextBox
    Friend WithEvents lblXmlManager As System.Windows.Forms.Label
    Friend WithEvents txtFinManager As System.Windows.Forms.TextBox
    Friend WithEvents chkFinManager As System.Windows.Forms.CheckBox
    Friend WithEvents gpbxSave As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents colRemove As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colContactMethod As System.Windows.Forms.DataGridViewImageColumn
End Class
