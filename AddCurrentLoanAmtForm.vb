﻿Public Class AddCurrentLoanAmtForm
    Dim thisEnquiryId As Integer

    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)
        thisEnquiryId = loadEnquiryVar
        'Dim ThisClientId As Integer ' set New Client Id 
        'Load DataTable
        Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
        'get Client Id
        'ThisClientId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ClientId")
        'loads data into the 'EnquiryWorkSheetDataSet.Client' table for this Client Id.
        'Me.ClientTableAdapter.FillByClientId(Me.EnquiryWorkSheetDataSet.Client, ThisClientId)

    End Sub

    Private Sub AddCurrentLoanAmtForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnquiryBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'check data in field
        Dim checkFieldsMessage As String = ""
        Try
            If txtbxCurrentLoanAmtValue.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter the Amount of the Current Loan" + vbCrLf
            Else
                If Not CInt(txtbxCurrentLoanAmtValue.Text) > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter the Amount of the Current Loan" + vbCrLf
                End If
            End If
        Catch ex As Exception
            MsgBox("Checking the Amount of the Current Loan caused an error: " & ex.Message)
        End Try

        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            DialogResult = System.Windows.Forms.DialogResult.None
        Else
            Try
                'save
                Me.Validate()
                EnquiryBindingSource.EndEdit()
                Me.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)

                'close form
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Catch ex As Exception
                MsgBox("Adding Amount of the Current Loan caused an error: " & ex.Message)
            End Try
        End If
    End Sub

    Private Sub txtbxCurrentLoanAmtValue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbxCurrentLoanAmtValue.KeyPress
        'Any character other than a digit or a single decimal point to be rejected as user input
        If txtbxCurrentLoanAmtValue.Text.Contains(".") Then
            If Not Char.IsControl(e.KeyChar) And Not Char.IsDigit(e.KeyChar) Then
                e.Handled = True
            End If
        Else
            'If we don't already have a decimal point, accept control keys, digits, OR a decimal point
            If Not Char.IsControl(e.KeyChar) And Not Char.IsDigit(e.KeyChar) And Not Char.Equals(e.KeyChar, ".") Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtbxCurrentLoanAmtValue_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxCurrentLoanAmtValue.LostFocus
        If txtbxCurrentLoanAmtValue.Text.Length > 0 Then
            If CInt(txtbxCurrentLoanAmtValue.Text) > 0 Then
                btnSave.Visible = True
            Else
                btnSave.Visible = False
            End If
        Else
            btnSave.Visible = False
        End If
    End Sub



End Class