﻿Imports System.Data.SqlClient

Public Class DeleteEnquiryForm


    Private Sub DeleteEnquiryForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub




    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim ThisEnquiryCode As String = txtbxEnquiryCode.Text
        Dim ReturnBool As Boolean = False

        ReturnBool = Delete_Enquiry(ThisEnquiryCode)
        If ReturnBool = True Then
            'Delete Enquiry successful
            MessageBox.Show("Enquiry Deletion was successful", "Enquiry Deletion", MessageBoxButtons.OK, MessageBoxIcon.None)
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Else
            Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub



    Private Sub txtbxEnquiryCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxEnquiryCode.LostFocus
        Dim ThisEnquiryCode As String = txtbxEnquiryCode.Text
        Dim ThisSalutation As String
        'check enquiry exists in Enquiry table
        Try
            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT ContactDisplayName FROM ActiveClientEnquiries WHERE  EnquiryCode = '" & ThisEnquiryCode & "'"
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim ThisEnquiryDataAdapter As New SqlDataAdapter(selectCommand)
            Dim ThisEnquiryDataSet As New DataSet
            Dim ThisEnquiryDataTable As New DataTable

            'dumps results into datatable CommentsDataTable
            ThisEnquiryDataAdapter.Fill(ThisEnquiryDataTable)
            'if no matching rows .....
            If ThisEnquiryDataTable.Rows.Count = 0 Then
                ' clear the dataTable and the Connect information
                ThisEnquiryDataAdapter = Nothing
                ThisEnquiryDataTable.Clear()
                connection.Dispose()
                btnDelete.Visible = False
                lblDisplayName.ForeColor = Color.Red
                lblDisplayName.Text = "Enquiry does not exist."
                'if there is a matching row
            ElseIf ThisEnquiryDataTable.Rows.Count = 1 Then
                'get EnquiryCode value
                Dim ThisEnquiryDataRow As DataRow = ThisEnquiryDataTable.Rows(0)
                If ThisEnquiryDataRow.Item(0) IsNot DBNull.Value Then
                    ThisSalutation = ThisEnquiryDataRow.Item(0)
                    ' clear the dataTable and the Connect information
                    ThisEnquiryDataAdapter = Nothing
                    ThisEnquiryDataTable.Clear()
                    connection.Dispose()
                    lblDisplayName.ForeColor = Color.Black
                    lblDisplayName.Text = ThisSalutation
                    btnDelete.Visible = True
                Else
                    ThisSalutation = "No Display Name found!"
                    ' clear the dataTable and the Connect information
                    ThisEnquiryDataAdapter = Nothing
                    ThisEnquiryDataTable.Clear()
                    connection.Dispose()
                    lblDisplayName.ForeColor = Color.Red
                    lblDisplayName.Text = ThisSalutation
                    btnDelete.Visible = True
                End If
            Else
                ThisEnquiryDataAdapter = Nothing
                ThisEnquiryDataTable.Clear()
                connection.Dispose()
                lblDisplayName.ForeColor = Color.Red
                lblDisplayName.Text = "Unknown"
                btnDelete.Visible = True
            End If
        Catch ex As Exception
            MsgBox("Checking Enquiry exists in Enquiry table caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub
End Class