﻿Public Class AddPrelimCommentForm
    ' set New Enquiry Id 
    Dim ThisEnquiryId As Integer
    Dim pComments As String = String.Empty
    Dim SaveToolStripButtonClicked As Boolean = False

    

    Friend Sub PassVariable(Optional ByVal comments As String = "")
        'setup the reference variables
        pComments = comments
        'get user settings (on the Load Form Event)
        Me.ClientSize = New Size(My.Settings.AddPrelimCommentFormSize)
        Me.Location = New Point(My.Settings.AddPrelimCommentFormLocation)

    End Sub

    Private Sub EnquiryBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnquiryBindingNavigatorSaveItem.Click
        Try
            'Dim DateStr As String
            Dim CommentStr As String
            'set SaveToolStripButtonClicked
            SaveToolStripButtonClicked = True
            ''Create Date string
            'DateStr = "-- " & DateTime.Now.ToString("f")
            ''Add Date string to comments
            'CommentStr = DateStr & " | " & LoggedinName & vbCrLf & txtbxPrelimComment.Text & vbCrLf & vbCrLf
            CommentStr = txtbxPrelimComment.Text 'date etc is now added when comment is saved by preliminary form

            'check if loan comments exist
            If Not String.IsNullOrEmpty(pComments) Then
                PrelimComment = pComments & vbCrLf & vbCrLf & CommentStr
            Else
                PrelimComment = CommentStr
            End If
            
            'Close Form
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub CutToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CutToolStripButton.Click
        If txtbxPrelimComment.SelectedText <> "" Then
            ' Cut the selected text in the control and paste it into the Clipboard.
            txtbxPrelimComment.Cut()
        End If

    End Sub

    Private Sub CopyToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripButton.Click
        ' Ensure that text is selected in the text box.   
        If txtbxPrelimComment.SelectionLength > 0 Then
            ' Copy the selected text to the Clipboard.
            txtbxPrelimComment.Copy()
        End If

    End Sub

    Private Sub PasteToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripButton.Click
        ' Determine if there is any text in the Clipboard to paste into the text box.
        If Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) = True Then
            ' Determine if any text is selected in the text box.
            If txtbxPrelimComment.SelectionLength > 0 Then
                ' Ask user if they want to paste over currently selected text.
                If MessageBox.Show("Do you want to paste over current selection?", _
                    "Cut Example", MessageBoxButtons.YesNo) = DialogResult.No Then
                    ' Move selection to the point after the current selection and paste.
                    txtbxPrelimComment.SelectionStart = txtbxPrelimComment.SelectionStart + txtbxPrelimComment.SelectionLength
                End If
            End If
            ' Paste current text in Clipboard into text box.
            txtbxPrelimComment.Paste()
        End If

    End Sub

    Private Sub UndoToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UndoToolStripButton.Click
        ' Determine if last operation can be undone in text box.   
        If txtbxPrelimComment.CanUndo = True Then
            ' Undo the last operation.
            txtbxPrelimComment.Undo()
            ' Clear the undo buffer to prevent last action from being redone.
            'txtbxPrelimComment.ClearUndo()
        End If
    End Sub

    Private Sub CancelToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelToolStripButton.Click
        Me.EnquiryBindingSource.CancelEdit()
        'Close Form
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

   
    Private Sub AddPrelimCommentForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            'leave if txtbxPrelimComment.Text is empty (no data)
            If Not txtbxPrelimComment.Text = "" Then
                If SaveToolStripButtonClicked = False Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or _
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        Try
                            'Dim DateStr As String
                            Dim CommentStr As String
                            ''Dim NewCommentStr As String
                            ''Create Date string
                            'DateStr = "-- " & DateTime.Now.ToString("f")
                            ''Add Date string to comments
                            'CommentStr = DateStr & " | " & LoggedinName & vbCrLf & txtbxPrelimComment.Text & vbCrLf & vbCrLf
                            CommentStr = txtbxPrelimComment.Text 'date etc is now added when comment is saved by preliminary form
                            'check if loan comments exist
                            If Not String.IsNullOrEmpty(pComments) Then
                                PrelimComment = pComments & vbCrLf & vbCrLf & CommentStr
                            Else
                                PrelimComment = CommentStr
                            End If
                            'Close Form
                            Me.DialogResult = System.Windows.Forms.DialogResult.OK
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Else
                        ' User choose No. Do nothing
                    End If
                End If
                    'no data entered - proceed
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'save user settings (on the FormClosing Form Event)
        My.Settings.AddPrelimCommentFormSize = New Size(Me.ClientSize)
        My.Settings.AddPrelimCommentFormLocation = New Point(Me.Location)


    End Sub

    Private Sub AddPrelimCommentForm_Layout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        txtbxPrelimComment.Size = New Size(Me.Width - 40, Me.Height - 78)
    End Sub

    Private preComment As String
    Public Property PrelimComment() As String
        Get
            If String.IsNullOrEmpty(preComment) Then
                preComment = String.Empty
            End If
            Return preComment
        End Get
        Set(ByVal value As String)
            preComment = value
        End Set
    End Property

   
End Class