﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.Threading.Tasks
Imports AnimatedCircle

Public Class WkShtWizForm3 'Customer Due Diligence
    ' set New Enquiry Id 
    Dim thisEnquiryId As Integer
    Dim thisEnquiryCode As String ' set Enquiry Code
    Dim thisEnquiryManagerId As Integer 'set EnquiryManagerId
    'Dim ThisLoanReason As String
    Dim thisEnquiryType As Integer
    ' set New Wizard Status 
    Dim thisWizardStatus As Integer
    Dim qrgStatus As Integer 'set QRG Status for colour
    Dim thisTypeOfCustomer As Integer 'set Type of Customer for loaded customer
    Dim testDate As Date = #1/1/1900#
    Dim loadedCustomerId As Integer = 0
    Dim custSalutation As String = String.Empty
    Dim dtsbType23 As Integer
    Dim dtsbType01 As Integer

    Dim thisContactDisplayName As String
    Dim contactName As String
    Dim contactLastName As String
    Dim contactAddress As String
    Dim thisTypeOfMainCustomer As Integer ' set Type of Main Client
    Dim thisDealerId As String
    Dim thisDealerName As String
    Dim thisEnquiryResult As String = ""
    Dim progressStatus As Boolean = False
    Dim formTitle As String = "Due-diligence Profile" 'Me.Text
    Dim systemCanImportCustomers As Boolean = False 'appsetting
    Dim creditQuestion1State As Boolean 'track state of CreditQuestion1
    Dim creditQuestion2State As Boolean 'track state of CreditQuestion2
    Dim creditQuestion3State As Boolean 'track state of CreditQuestion3
    Dim creditQuestionStateChanged As Boolean = False 'track if a radio button state changed and not saved
    Dim customerTypeStateChanged As Boolean = False 'track if a CustomerType state changed from current
    Dim customerDataStateChanged As Boolean = False 'track if a data state changed and not been saved
    Dim rbPepState As Integer = 0 'Track state of IsaPEP 0=null, 1=false, 2=true
    Dim rbPepStateChanged As Boolean = False 'Track whether IsaPep radio buttons have been activated/changed
    'sizing
    Dim custCount As Integer
    Dim custCountDtsbType23 As Integer 'number of records to be displayed in DtsbType2 grid. Used for sizing of grid 
    Dim custCountDtsbType01 As Integer
    Dim refCount As Integer 'number of records in reference bindingsource
    Dim thisRefCount As Integer 'number of records to be displayed in reference datarepeater
    'user settings
    Dim newLocX As Integer
    Dim newLocY As Integer
    'Define the cancellation token.
    Dim cancellationTokenSource As CancellationTokenSource ' = New CancellationTokenSource()
    Dim asyncAnimation As AnimatedCircle.AsyncAnimation
    Dim thisApplicationCode As String
    Dim thisMinLoanValueSecurityInsured As Decimal
    Dim docsReceived As Boolean = False
    'store form load information
    Dim frmLoadInfo As System.Text.StringBuilder = New System.Text.StringBuilder
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        frmLoadInfo.Clear()
        frmLoadInfo.Append("Initiaised Component (Form) - " & sw.Elapsed.ToString & ". ")

    End Sub



    'Loads form with parameters
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)

        frmLoadInfo.Append("PassVarible: Loading new form - " & sw.Elapsed.ToString & ". ")
        ToolStripStatusLabel1.Text = "Loading new form."
        Application.DoEvents()

        SuspendLayout()
        RemoveHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        RemoveHandler cmbxAcceptanceMethod.SelectedIndexChanged, AddressOf CmbxAcceptanceMethodSelectedIndexChanged
        RemoveHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf CmbxRiskAssessSelectedIndexChanged
        RemoveHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
        RemoveHandler cmbxTypeOfId.SelectedIndexChanged, AddressOf cmbxTypeOfId_SelectedIndexChanged
        RemoveHandler cmbxTypeDL.SelectedIndexChanged, AddressOf cmbxTypeDL_SelectedIndexChanged
        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)

        ' setup the reference variables
        thisEnquiryId = loadEnquiryVar
        'set the form tag with EnquiryId
        Me.Tag = thisEnquiryId
        'Display step
        frmLoadInfo.Append("Loading data - " & sw.Elapsed.ToString & ". ")
        ToolStripStatusLabel1.Text = "Loading data."

        Try
            'loads data into the 'EnquiryWorkSheetDataSet.TypeOfTenancy' table
            Me.TypeOfTenancyTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.TypeOfTenancy)
            'loads data into the 'EnquiryWorkSheetDataSet.List_AMLRisk' table.
            Me.List_AMLRiskTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.List_AMLRisk)
            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
            'get EnquiryManagerId
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            thisApplicationCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ApplicationCode")
            docsReceived = If(IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DocsReceived")), False, EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DocsReceived"))
            'Load User table
            UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            'get UserName
            lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
            'get type of Enquiry
            thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType")
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                lblLoanAmount.Visible = False
            Else
                lblLoanAmount.Visible = True
            End If
            'set EnquiryType label
            lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
            'Get Wizard Status
            thisWizardStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("WizardStatus")
            'get Dealer ID
            thisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
            thisTypeOfMainCustomer = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")
            'Load in Customers for Enquiry
            Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
            'number of customers for setting element sizing
            custCount = EnquiryCustomersBindingSource.Count
            'load CustomerEnquiry datatable for use with cmbxCustEnquiryType
            Me.CustomerEnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.CustomerEnquiry, thisEnquiryId)

            'Get Contact Display Name
            thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
            'Set form title
            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & thisContactDisplayName & " - " & formTitle
                pnlCompany.BackColor = Color.MistyRose
                pnlIndividual.BackColor = Color.MistyRose
                pnlHeader.BackColor = Color.MistyRose
                pnlFooter.BackColor = Color.MistyRose
                pnlCustomerType.BackColor = Color.MistyRose
            Else
                Me.Text = thisContactDisplayName & " - " & formTitle
            End If
            '*************** Set EnquiryType display options for CurrentLoanAmt 
            'Display step
            frmLoadInfo.Append("Setting EnquiryType display options for CurrentLoanAmt - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Setting EnquiryType display options for CurrentLoanAmt."

            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurrentLoan.Visible = True
                lblCurrentLoanAmt.Visible = True
            Else
                lblCurrentLoan.Visible = False
                lblCurrentLoanAmt.Visible = False
            End If

            '************************* Cannotate Contact Name
            'Display step
            frmLoadInfo.Append("Contact Name and address - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Contact Name and address."

            contactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
            contactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " &
            EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & contactLastName
            'set lblContactName
            lblContactName.Text = contactName

            '*************** Cannotate Contact address
            contactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
            lblContactAddress.Text = contactAddress

            '*************** Get Dealer name
            'Display step
            frmLoadInfo.Append("Getting Dealer name - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Getting Dealer name"

            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & thisDealerId & "'"
            'MsgBox("Select Statement = " & selectStatement)
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand)
            Dim dealerTempDataSet As New DataSet
            Dim dealerTempDataTable As New DataTable
            Try
                'dumps results into datatable LoginDataTable
                dealerTempDataAdapter.Fill(dealerTempDataTable)
                'if no matching rows .....
                If dealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf dealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                    thisDealerName = dealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
            'lblDealerName.Text = thisDealerName
            '**** appSettings
            'Display step
            frmLoadInfo.Append("Getting AppSettings - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Getting AppSettings"

            Try
                Dim thisAppsettings As New AppSettings
                systemCanImportCustomers = thisAppsettings.SystemCanImportCustomers
            Catch ex As Exception
                log.Error("Load Form: Get Application values generated an exception message:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
                MsgBox("Load Form: Get Application values generated an exception message:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            End Try

            If systemCanImportCustomers Then
                btnImport.Visible = True
            Else
                btnImport.Visible = False
            End If

            '*************** set datasource for combo box using the Enum members
            'Display step
            frmLoadInfo.Append("Setting datasource for combo boxes - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Setting datasource for combo boxes"

            cmbxCustEnquiryType.DisplayMember = "Value"
            cmbxCustEnquiryType.DataSource = MyEnums.ToList(GetType(MyEnums.CustomerEnquiryType))
            cmbxAcceptanceMethod.DisplayMember = "Value"
            cmbxAcceptanceMethod.DataSource = MyEnums.ToList(GetType(MyEnums.CustAcceptanceMethod))
            cmbxCustomerType.DisplayMember = "Value"
            cmbxCustomerType.DataSource = MyEnums.ToList(GetType(MyEnums.CustomerType))
            '**** Individual panel
            'cmbxCustomerEnquiryType.DisplayMember = "Value"
            'cmbxCustomerEnquiryType.DataSource = MyEnums.ToList(GetType(MyEnums.CustomerEnquiryType))
            'cmbxIndividAcceptanceMethod.DisplayMember = "Value"
            'cmbxIndividAcceptanceMethod.DataSource = MyEnums.ToList(GetType(MyEnums.CustAcceptanceMethod))
            '**** Company panel
            'cmbxCompanyEnquiryType.DisplayMember = "Value"
            'cmbxCompanyEnquiryType.DataSource = MyEnums.ToList(GetType(MyEnums.CustomerEnquiryType))            
            'cmbxCompanyAcceptanceMethod.DisplayMember = "Value"
            'cmbxCompanyAcceptanceMethod.DataSource = MyEnums.ToList(GetType(MyEnums.CustAcceptanceMethod))            

            '**** load default (first) customer
            'Display step
            frmLoadInfo.Append("Loading default customer - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Loading default customer"

            If custCount > 0 Then
                Dim ecRowView As Data.DataRowView
                Dim ecRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
                ecRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
                If ecRowView.Row IsNot Nothing Then
                    ecRow = ecRowView.Row
                    'load customer table
                    Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, ecRow.CustomerId)
                    loadedCustomerId = ecRow.CustomerId 'Current customer to be displayed
                    cmbxCustomerType.SelectedIndex = ecRow.CustomerType
                    'set customer panels
                    SetCustomerButtons(True)
                    SetCustomerTypePanel(ecRow.CustomerType, loadedCustomerId, ecRow.Type)
                    pnlCustomerType.Visible = True
                    pnlCustomerType.Enabled = True
                End If

                btnFinish.Enabled = True
            Else
                btnFinish.Enabled = False
                SetCustomerButtons(False)
            End If

            '***************  Docs folder
            'Display step
            frmLoadInfo.Append("Setting up Docs folder - " & sw.Elapsed.ToString & ". ")
            ToolStripStatusLabel1.Text = "Setting up Docs folder"

            'Get String to WorkSheet Drive
            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
            Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
            'Check directory exists
            If Directory.Exists(folderPath) Then
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
            Else
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
                'deactivate drag and drop
                Me.btnDocs.AllowDrop = False
            End If

        Catch ex As Exception
            log.Error(ex.Message)
            MsgBox(ex.Message)
        End Try

        '*****************************  get QRGList status
        'Display step
        frmLoadInfo.Append("Setting QRGList - " & sw.Elapsed.ToString & ". ")
        ToolStripStatusLabel1.Text = "Setting QRGList"

        Dim connectionString1 As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection1 As New SqlConnection(connectionString1)
        Dim selectStatement1 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
        'MsgBox("Select Statement = " & selectStatement1)
        Dim selectCommand1 As New SqlCommand(selectStatement1, connection1)
        Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand1)
        Dim QRGListTempDataSet As New DataSet
        Dim QRGListTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            QRGListTempDataAdapter.Fill(QRGListTempDataTable)
            'if no matching rows .....
            If QRGListTempDataTable.Rows.Count = 0 Then
                MessageBox.Show("ERROR: No QRG List, please try again.")
                'clear the dataTable and the Connect information
                QRGListTempDataAdapter = Nothing
                QRGListTempDataTable.Clear()
                'if there is a matching row
            ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                qrgStatus = QRGListTempDataRow.Item(0)
                'MsgBox("QRG Status = " & QRGStatus)
                'clear the dataTable and the Connect information
                QRGListTempDataAdapter = Nothing
                QRGListTempDataTable.Clear()
            End If
            'close the connection
            If connection1.State <> ConnectionState.Closed Then
                connection1.Close()
            End If

        Catch ex As Exception
            log.Error(ex.Message)
            MsgBox(ex.Message)
        End Try

        'set QRGStatus button colour
        Select Case qrgStatus
            Case 0
                btnQRGList.BackColor = Color.Transparent
            Case 1
                btnQRGList.BackColor = Color.Tomato
            Case 2
                btnQRGList.BackColor = Color.LightGreen
            Case Else
                btnQRGList.BackColor = Color.Transparent
        End Select

        '*************** Set permissions
        'Display step
        frmLoadInfo.Append("Settings permissions - " & sw.Elapsed.ToString & ". ")
        ToolStripStatusLabel1.Text = "Settings permissions"

        'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
        Select Case LoggedinPermissionLevel
            Case Is = 1 'Readonly
                UpdateAllToolStripMenuItem.Visible = False
                'btnSaveAndExit.Enabled = False
                btnFinish.Enabled = False
                btnDocs.Enabled = False
                gpbxTenancy.Enabled = False
                gpbxEmployment.Enabled = False
                'ckbxSelfEmployed.Enabled = False
                ckbxBeneficiary.Enabled = False
                rtxtbxStabilityOther.IsReadOnly = True
                'gpbxJointApp.Enabled = False
                gpbxIdentity.Enabled = False
                pnlCustomerType.Enabled = False
                btnAddCustomer.Visible = False
                btnImport.Visible = False

            Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                '--------------------------------------
                btnFinish.Enabled = False
                gpbxTenancy.Enabled = False
                gpbxEmployment.Enabled = False
                'ckbxSelfEmployed.Enabled = False
                ckbxBeneficiary.Enabled = False
                rtxtbxStabilityOther.IsReadOnly = True
                'gpbxJointApp.Enabled = False
                gpbxIdentity.Enabled = False
                pnlCustomerType.Enabled = False
                btnAddCustomer.Visible = False
                btnImport.Visible = False

            Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                'Can see /use Archive Enquiry in File menu for their user status
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True
                DocumentsReceivedToolStripMenuItem.Visible = True
                DocsReceivedImg.Visible = True
            Case Is = 4
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True
                DocumentsReceivedToolStripMenuItem.Visible = True
                DocsReceivedImg.Visible = True

            Case Is = 5
                'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True
                DocumentsReceivedToolStripMenuItem.Visible = True
                DocsReceivedImg.Visible = True
            Case Is > 5 'Systems Manager Level, Open.
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True
                DocumentsReceivedToolStripMenuItem.Visible = True
                DocsReceivedImg.Visible = True

            Case Else 'Readonly
                UpdateAllToolStripMenuItem.Visible = False
                'btnSaveAndExit.Enabled = False
                btnFinish.Enabled = False
                btnDocs.Enabled = False
                btnAddCustomer.Visible = False
                btnImport.Visible = False

        End Select
        '*************** end of Set permissions
        '************************* Universal Deny/negative settings
        If docsReceived = True Then
            DocumentsReceivedToolStripMenuItem.Text = "Reset Docs received"
            DocsReceivedImg.Visible = False
        End If

        'set StatusStrip text
        frmLoadInfo.Append("Setting customer panels - " & sw.Elapsed.ToString & ". ")
        ToolStripStatusLabel1.Text = "Setting customer panels."

        'set the panels
        If Not custCount > 0 Then
            pnlIndividual.Visible = False
            pnlCustomerType.Visible = False
            pnlCompany.Visible = False
            SetCustomerButtons(False)
        End If

        frmLoadInfo.Append("Finish PassVarible - " & sw.Elapsed.ToString & ". ")
        'MessageBox.Show(frmLoadInfo.ToString, "Form Load Information", MessageBoxButtons.OK)

        ResumeLayout()

    End Sub

    Private Sub WkShtWizForm3_HandleCreated(sender As Object, e As EventArgs) Handles Me.HandleCreated
        frmLoadInfo.Append("WkShtWizForm3_HandleCreated - " & sw.Elapsed.ToString & ". ")

    End Sub

    Private Sub WkShtWizForm3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmLoadInfo.Append("Start Form Load - " & sw.Elapsed.ToString & ". ")
        'Display step
        ToolStripStatusLabel1.Text = "Adding Handlers"
        Application.DoEvents()
        SuspendLayout()

        AnimatedCirclePanel.Visible = False
        asyncAnimation = New AnimatedCircle.AsyncAnimation(AnimatedCirclePanel, Color.RoyalBlue)
        'Add Handlers
        AddHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        AddHandler cmbxAcceptanceMethod.SelectedIndexChanged, AddressOf CmbxAcceptanceMethodSelectedIndexChanged
        AddHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf CmbxRiskAssessSelectedIndexChanged
        AddHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
        AddHandler cmbxTypeOfId.SelectedIndexChanged, AddressOf cmbxTypeOfId_SelectedIndexChanged
        AddHandler cmbxTypeDL.SelectedIndexChanged, AddressOf cmbxTypeDL_SelectedIndexChanged

        ResumeLayout()
        'Display step
        frmLoadInfo.Append("Finish Form Load - " & sw.Elapsed.ToString & ". ")
        log.Debug(frmLoadInfo.ToString)
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Page loaded."
       
    End Sub

    Private Sub WkShtWizForm3_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        log.Debug("WkShtWizForm3_Shown - " & sw.Elapsed.ToString)
        log.Debug("End Trace ****************")
        sw.Stop()

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'check if data has changed
            Dim rowView1 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not Me.CustomerBindingSource.Current Is Nothing Then
                rowView1 = CType(Me.CustomerBindingSource.Current, DataRowView)
                'test if any fields have changed
                'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                'If Not (rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed)) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Compare Current with Proposed
                    If Not rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed) Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                        'Get user confirmation
                        Dim msg As String
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        msg = "Do you want to Save Changed Data?"   ' Define message.
                        style = MsgBoxStyle.DefaultButton2 Or
                           MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                        title = "Save Record?"   ' Define title.
                        ' Display message.
                        response = MsgBox(msg, style, title)
                        If response = MsgBoxResult.Yes Then   ' User choose Yes.
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            SaveCustomer()
                        Else
                            ' User choose No. Do nothing
                        End If
                        creditQuestionStateChanged = False
                        customerDataStateChanged = False
                        customerTypeStateChanged = False
                        rbPepStateChanged = False
                    Else
                        'no changes to save
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "No changes to save."

                    End If
                ElseIf creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        SaveCustomer()
                    Else
                        ' User choose No. Do nothing
                    End If
                    creditQuestionStateChanged = False
                    customerDataStateChanged = False
                    customerTypeStateChanged = False
                    rbPepStateChanged = False
                Else
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try


        ''launch WkShtWizForm2 and pass Enquiry Id
        'Dim WkShtWizFrm2 As New WkShtWizForm2
        'Try
        '    WkShtWizFrm2.PassVariable(ThisEnquiryId)
        '    WkShtWizFrm2.Show()
        '    'Close this Form
        '    Me.Close()
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

        If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            Dim wkShtWizFrm1 As New WkShtWizForm1
            wkShtWizFrm1.PassVariable(thisEnquiryId)
            wkShtWizFrm1.Show()
            'Close this Form
            Me.Close()
        Else
            If thisTypeOfMainCustomer = MyEnums.mainCustomerType.Company Then
                'launch CompanyFinancialForm and pass Enquiry Id
                Dim companyFinancialFrm As New CompanyFinancialForm
                companyFinancialFrm.PassVariable(thisEnquiryId)
                companyFinancialFrm.Show()
                'Close this Form
                Me.Close()
            ElseIf thisTypeOfMainCustomer = MyEnums.mainCustomerType.Trust Then
                Dim companyFinancialFrm As New CompanyFinancialForm
                companyFinancialFrm.PassVariable(thisEnquiryId)
                companyFinancialFrm.Show()
                Me.Close()
            ElseIf thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership Then
                Dim companyFinancialFrm As New CompanyFinancialForm
                companyFinancialFrm.PassVariable(thisEnquiryId)
                companyFinancialFrm.Show()
                Me.Close()
            Else
                'launch WkShtWizForm4 and pass Enquiry Id
                Dim wkShtWizFrm4 As New WkShtWizForm4
                wkShtWizFrm4.PassVariable(thisEnquiryId)
                wkShtWizFrm4.Show()
                'Close this Form
                Me.Close()
            End If
        End If


        Cursor.Current = Cursors.Default

    End Sub

    Private Sub WkShtWizForm3_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SuspendLayout()
        Try
            'check if data has changed
            Dim rowView1 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not Me.CustomerBindingSource.Current Is Nothing Then
                rowView1 = CType(Me.CustomerBindingSource.Current, DataRowView)
                'test if any fields have changed
                If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        SaveCustomer()
                    Else
                        ' User choose No. Do nothing
                    End If
                    'reset changed states
                    creditQuestionStateChanged = False
                    customerDataStateChanged = False
                    customerTypeStateChanged = False
                    rbPepStateChanged = False
                Else
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            newLocX = 0
        Else
            newLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            newLocY = 0
        Else
            newLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.PrelimFormLocation = New Point(newLocX, newLocY)
        My.Settings.PrelimFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings

    End Sub


    Private Sub WkShtWizForm3_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        Dim minFormHeight As Integer = 610
        Dim dgvHeight As Integer
        Dim dgvDtsb23H As Integer 'height of dgvHDtsbType2 datagridview
        Dim dgvDtsb01H As Integer 'height of dgvDtsb01 datagridview
        Dim drHeight As Integer 'height of datarepeater drReferences
        Dim headerBtnLocY As Integer
        Dim spaceBelowGrid As Integer = 15
        Dim spaceBetweenGpbx As Integer = 10
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            dgvHeight = 23 + (22 * custCount)
            pnlHeader.Size = New Size(formWidth - 22, 106 + dgvHeight + 24 + spaceBelowGrid) 'panel space above grid + grid height + button space + space below grid
            dgvEnquiryCustomers.Size = New Size(formWidth - 58, dgvHeight)
            'button locations (inside pnlHeader)
            headerBtnLocY = 106 + dgvHeight + 8
            'AnimatedCirclePanel.Location = New Point(17, headerBtnLocY)
            'btnImport.Location = New Point(47, headerBtnLocY)
            'pbImportCustomer.Location = New Point(180, headerBtnLocY + 3)
            'btnAddCustomer.Location = New Point(224, headerBtnLocY)
            btnSaveCustomer.Location = New Point(formWidth - 286, headerBtnLocY)
            btnCancelCustomer.Location = New Point(formWidth - 205, headerBtnLocY)
            btnDeleteCustomer.Location = New Point(formWidth - 124, headerBtnLocY)
            'pnlCustomerType
            pnlCustomerType.Location = New Point(3, (pnlHeader.Size.Height + 3)) 'panel size + panel starting location
            pnlCustomerType.Size = New Size(formWidth - 22, 34)
            '**** Individual panel ****
            '**************************
            If thisTypeOfCustomer = MyEnums.CustomerType.Individual Or thisTypeOfCustomer = MyEnums.CustomerType.SoleTrader Then
                pnlIndividual.Location = New Point(3, pnlHeader.Size.Height + pnlCustomerType.Size.Height + 3)
                'pnlIndividual.Size = New Size(formWidth - 22, formHeight - pnlHeader.Size.Height - 137)
                pnlIndividual.Size = New Size(formWidth - 22, formHeight - pnlIndividual.Location.Y - 98)
                gpbxCustomerName.Size = New Size(formWidth - 55, 57)
                'identity appraisal
                gpbxIdentity.Location = New Point(12, gpbxCustomerName.Location.Y + gpbxCustomerName.Size.Height + spaceBetweenGpbx) '(12,111)
                gpbxIdentity.Size = New Size(formWidth - 55, 179)
                txtIdIsPerson.Size = New Size(formWidth - 395, 20)
                txtResidency.Size = New Size(formWidth - 395, 20)
                txtAgeOfId.Size = New Size(formWidth - 395, 30)
                'tenancy
                gpbxTenancy.Location = New Point(12, gpbxIdentity.Location.Y + gpbxIdentity.Size.Height + spaceBetweenGpbx)
                gpbxTenancy.Size = New Size(formWidth - 55, 154)
                txtbxResidenceSatis.Size = New Size(formWidth - 395, 35)
                txtbxTenancyEnviron.Size = New Size(formWidth - 395, 35)
                txtbxEstablishTenancy.Size = New Size(formWidth - 395, 35)
                'employment, trading activity
                gpbxEmployment.Location = New Point(12, gpbxTenancy.Location.Y + gpbxTenancy.Size.Height + spaceBetweenGpbx)
                txtbxQuestion1.Size = New Size(formWidth - 395, 20)
                txtbxQuestion2.Size = New Size(formWidth - 395, 20)
                txtbxQuestion9.Size = New Size(formWidth - 395, 20)
                txtbxQuestion3.Size = New Size(formWidth - 395, 20)
                txtbxQuestion4.Size = New Size(formWidth - 395, 20)
                txtbxQuestion6.Size = New Size(formWidth - 395, 20)
                txtbxQuestion7.Size = New Size(formWidth - 395, 20)
                txtbxQuestion8.Size = New Size(formWidth - 395, 20)
                If thisTypeOfCustomer = MyEnums.CustomerType.SoleTrader Then
                    gpbxEmployment.Size = New Size(formWidth - 55, 220)
                    lblWorkCompany.Location = New Point(316 - lblWorkCompany.Width, 25)
                    txtbxQuestion1.Location = New Point(331, 22)
                    lblFulltime.Location = New Point(316 - lblFulltime.Width, 50)
                    txtbxQuestion2.Location = New Point(331, 47)
                    lblEmploy9.Location = New Point(316 - lblEmploy9.Width, 75)
                    txtbxQuestion9.Location = New Point(331, 72)
                    lblWhatDo.Location = New Point(316 - lblWhatDo.Width, 100)
                    txtbxQuestion3.Location = New Point(331, 97)
                    lblHowLong.Location = New Point(316 - lblHowLong.Width, 125)
                    txtbxQuestion4.Location = New Point(331, 122)
                    lblPay.Location = New Point(316 - lblPay.Width, 150)
                    txtbxQuestion6.Location = New Point(331, 147)
                    lblJobSafe.Location = New Point(316 - lblJobSafe.Width, 175)
                    txtbxQuestion7.Location = New Point(331, 172)
                    'lblSelfEmployed.Location = New Point(12, 200)
                Else
                    gpbxEmployment.Size = New Size(formWidth - 55, 320)
                    lblWorkCompany.Location = New Point(316 - lblWorkCompany.Width, 48)
                    txtbxQuestion1.Location = New Point(331, 45)
                    lblFulltime.Location = New Point(316 - lblFulltime.Width, 73)
                    txtbxQuestion2.Location = New Point(331, 70)
                    lblEmploy9.Location = New Point(316 - lblEmploy9.Width, 98)
                    txtbxQuestion9.Location = New Point(331, 95)
                    lblWhatDo.Location = New Point(316 - lblWhatDo.Width, 123)
                    txtbxQuestion3.Location = New Point(331, 120)
                    lblHowLong.Location = New Point(316 - lblHowLong.Width, 148)
                    txtbxQuestion4.Location = New Point(331, 145)
                    lblPay.Location = New Point(225 - lblPay.Width, 173)
                    txtbxQuestion5.Location = New Point(234, 170)
                    txtbxQuestion6.Location = New Point(331, 170)
                    lblJobSafe.Location = New Point(316 - lblJobSafe.Width, 198)
                    txtbxQuestion7.Location = New Point(331, 195)
                    lblWorker.Location = New Point(316 - lblWorker.Width, 223)
                    txtbxQuestion8.Location = New Point(331, 220)
                    'lblSelfEmployed.Location = New Point(147, 256)
                End If
                'StabilityOther
                lblOther.Location = New Point(20, gpbxEmployment.Location.Y + gpbxEmployment.Size.Height + spaceBetweenGpbx)
                rtxtbxStabilityOther.Location = New Point(20, lblOther.Location.Y + lblOther.Size.Height)
                rtxtbxStabilityOther.Size = New Size(formWidth - 75, 55)
                'Credit
                gpbxCredit.Location = New Point(12, rtxtbxStabilityOther.Location.Y + rtxtbxStabilityOther.Size.Height + spaceBetweenGpbx)
                gpbxCredit.Size = New Size(formWidth - 55, 384)
                lblCreditCheck.Location = New Point((formWidth / 2) - 256, 18)
                txtbxCreditHistory.Size = New Size(formWidth - 75, 65)
                txtbxCreditDefaults.Size = New Size(formWidth - 75, 65)
                txtbxCreditPPSR.Size = New Size(formWidth - 75, 65)
                gpbxCreditQuestion1.Size = New Size((formWidth / 3) - 64, 66)
                gpbxCreditQuestion2.Size = New Size((formWidth / 3) - 1, 66)
                gpbxCreditQuestion2.Location = New Point(286 - ((960 - formWidth) / 3), 301) '((formWidth / 3) - 3, 301)
                gpbxCreditQuestion3.Size = New Size((formWidth / 3) - 36, 66)
                gpbxCreditQuestion3.Location = New Point(610 - (((960 - formWidth) / 3) * 2), 301)
            End If
            
            '**** Company panel ****
            '***********************
            If thisTypeOfCustomer = MyEnums.CustomerType.Company Or thisTypeOfCustomer = MyEnums.CustomerType.Trust Then
                pnlCompany.Location = New Point(3, pnlHeader.Size.Height + pnlCustomerType.Size.Height + 3) '(3, 234)
                pnlCompany.Size = New Size(formWidth - 22, formHeight - pnlCompany.Location.Y - 98)
                gpbxCompanyInfo.Size = New Size(formWidth - 59, 232)
                tlpInformation.Size = New Size(formWidth - 73, 212)
                'director/trustee
                gpbxDtsb01.Location = New Point(12, gpbxCompanyInfo.Location.Y + gpbxCompanyInfo.Size.Height + spaceBetweenGpbx)
                btnAddNewDtsb01.Location = New Point(formWidth - 145, 1)
                dgvDtsb01H = dgvDtsb01.ColumnHeadersHeight + (dgvDtsb01.RowTemplate.Height * custCountDtsbType01)
                dgvDtsb01.Size = New Size(formWidth - 73, dgvDtsb01H)
                tlpDirectors.Size = New Size(formWidth - 73, 67)
                tlpDirectors.Location = New Point(4, 30 + dgvDtsb01.Size.Height)
                gpbxDtsb01.Size = New Size(formWidth - 59, 118 + dgvDtsb01.Size.Height)
                'shareholder/beneficiary
                gpbxDtsb23.Location = New Point(12, gpbxDtsb01.Location.Y + gpbxDtsb01.Size.Height + spaceBetweenGpbx)
                btnAddNewST.Location = New Point(formWidth - 145, 1)
                dgvDtsb23H = dgvDTSBType2.ColumnHeadersHeight + (dgvDTSBType2.RowTemplate.Height * custCountDtsbType23)
                dgvDTSBType2.Size = New Size(formWidth - 73, dgvDtsb23H)
                tlpShareholders.Size = New Size(formWidth - 73, 67)
                tlpShareholders.Location = New Point(4, 30 + dgvDTSBType2.Size.Height)
                gpbxDtsb23.Size = New Size(formWidth - 59, 118 + dgvDTSBType2.Size.Height)
                'tenancy
                gpbxCoTenancy.Location = New Point(12, gpbxDtsb23.Location.Y + gpbxDtsb23.Size.Height + spaceBetweenGpbx)
                gpbxCoTenancy.Size = New Size(formWidth - 59, 115)
                txtbxCompanyTenancySatis.Size = New Size(formWidth - 380, 36)
                'credit
                gpbxBusinessCredit.Location = New Point(12, gpbxCoTenancy.Location.Y + gpbxCoTenancy.Size.Height + spaceBetweenGpbx)
                gpbxBusinessCredit.Size = New Size(formWidth - 59, 370)
                lblBusCreditCheck.Location = New Point((formWidth / 2) - 256, 18)
                txtBusCreditHistory.Size = New Size(formWidth - 83, 65)
                txtBusCreditDefaults.Size = New Size(formWidth - 83, 65)
                txtBusCreditPPSR.Size = New Size(formWidth - 83, 65)
                gpbxBusCreditQuestion1.Size = New Size((formWidth / 3) - 64, 66)
                gpbxBusCreditQuestion2.Size = New Size((formWidth / 3) - 1, 66)
                gpbxBusCreditQuestion2.Location = New Point(274 - ((960 - formWidth) / 3), 292) 'New Point((formWidth / 3) - 3, 292)
                gpbxBusCreditQuestion3.Size = New Size((formWidth / 3) - 36, 66)
                gpbxBusCreditQuestion3.Location = New Point(599 - (((960 - formWidth) / 3) * 2), 292)
                'Comments
                gpbxBusOther.Location = New Point(12, gpbxBusinessCredit.Location.Y + gpbxBusinessCredit.Size.Height + spaceBetweenGpbx)
                gpbxBusOther.Size = New Size(formWidth - 59, 89)
                txtBusOtherComments.Size = New Size(formWidth - 83, 65)
                'Trade References
                gpbxTradeRef.Location = New Point(12, gpbxBusOther.Location.Y + gpbxBusOther.Size.Height + spaceBetweenGpbx)
                If refCount > 2 Then
                    thisRefCount = 2
                Else
                    thisRefCount = refCount
                End If
                drHeight = 177 * thisRefCount
                gpbxTradeRef.Size = New Size(formWidth - 60, drHeight + 51)
                drReferences.Size = New Size(formWidth - 76, drHeight)
            End If
            
            ''fix issue of select indicator changing in dgvEnquiryCustomers
            'dgvEnquiryCustomers.Rows(dgvSelectedRow).Selected = True
        End If
    End Sub


    Private Sub btnRangEmpTimeStamp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRangEmpTimeStamp.Click
        'check txtbxEmployer.Text and txtbxSpokeTo.Text are both filled in.
        Dim checkFieldsMessage As String = ""
        If txtbxEmployer.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter Employer Name" + vbCrLf
        End If
        If txtbxSpokeTo.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter who you spoke to" + vbCrLf
        End If

        If Not checkFieldsMessage = "" Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: There are Employment errors please view message box."
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Else
            'get DateTime Now and save to database field EmployerRangDate
            Dim rangDateTime As Date = Date.Now
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Saving Timestamp data."
            Application.DoEvents()
            'update WizardStatus
            Try
                Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                enquiryRow.BeginEdit()
                Dim customerRow As EnquiryWorkSheetDataSet.CustomerRow
                customerRow = Me.EnquiryWorkSheetDataSet.Customer(0)
                customerRow.BeginEdit()
                customerRow.EmployerRangDate = rangDateTime

                'create log note
                Dim dateStr As String
                Dim newCommentStr As String
                Dim commentStr As String
                Dim loanComments As String = enquiryRow.LoanComments
                'Create Date string
                dateStr = "-- " & DateTime.Now.ToString("f")
                'Add Date string to comments
                commentStr = dateStr & " | " & LoggedinName & vbCrLf & "Customer " & custSalutation & ": Rang Employer: " & txtbxEmployer.Text & ", spoke to " & txtbxSpokeTo.Text & vbCrLf & vbCrLf
                'check loan comments from dataset exist
                If Not loanComments = "" Then
                    newCommentStr = commentStr & loanComments
                Else
                    newCommentStr = commentStr
                End If
                'update LoanComments 
                enquiryRow.LoanComments = newCommentStr

                'save changes
                Me.Validate()
                enquiryRow.EndEdit()
                EnquiryBindingSource.EndEdit()
                customerRow.EndEdit()
                CustomerBindingSource.EndEdit()
                Me.CustomerTableAdapter.Update(Me.EnquiryWorkSheetDataSet)
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet)
                'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                lblRangEmpTimeStamp.Text = Format(rangDateTime, "G")
                btnRangEmpTimeStamp.Enabled = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Changes saved successfully."
            Catch ex As Exception
                log.Error("ERROR: btnRangEmpTimeStamp_Click generated an error" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
                MsgBox("ERROR: btnRangEmpTimeStamp_Click generated an error" & vbCrLf & ex.Message)
            End Try
            'FormRefresh()
        End If

    End Sub

    Private Sub FormRefresh()

        Call PassVariable(thisEnquiryId)

    End Sub


#Region "Documents"

    '******************* Documents
    Private Sub btnDocs_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Drag and drop successful.    Status: Ready"
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error("Drag and Drop caused an error:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("Drag and Drop caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnDocs_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        Else
            Try
                'create directory
                IO.Directory.CreateDirectory(fname)
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
                'refresh form
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
            Catch ex As System.IO.DirectoryNotFoundException
                ' Let the user know that the directory did not exist.
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

        End If
    End Sub
    '*************** End of Documents

    Private Sub btnQRGList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQRGList.Click
        Try
            'MsgBox("ThisQRGListId = " & ThisQRGListId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Quick reference Guide List."
            Dim QRGFrm As New QRGForm
            QRGFrm.PassVariable(thisEnquiryId, thisEnquiryCode, thisContactDisplayName)
            'AddSecurityFrm.ShowDialog(Me)
            If (QRGFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                '*****************************  get QRGList status
                Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection2 As New SqlConnection(connectionString2)
                Dim selectStatement2 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand2)
                Dim QRGListTempDataSet As New DataSet
                Dim QRGListTempDataTable As New DataTable
                Try
                    'dumps results into datatable LoginDataTable
                    QRGListTempDataAdapter.Fill(QRGListTempDataTable)
                    'if no matching rows .....
                    If QRGListTempDataTable.Rows.Count = 0 Then
                        MessageBox.Show("ERROR: No QRG List, please try again.")
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                        'if there is a matching row
                    ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                        'get  QRGStatus value
                        Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                        qrgStatus = QRGListTempDataRow.Item(0)
                        'MsgBox("QRG Status = " & QRGStatus)
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                    End If
                    'close the connection
                    If connection2.State <> ConnectionState.Closed Then
                        connection2.Close()
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

                'set QRGStatus button colour
                Select Case qrgStatus
                    Case 0
                        btnQRGList.BackColor = Color.Transparent
                    Case 1
                        btnQRGList.BackColor = Color.Tomato
                    Case 2
                        btnQRGList.BackColor = Color.LightGreen
                    Case Else
                        btnQRGList.BackColor = Color.Transparent
                End Select
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Quick reference Guide List updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Quick reference Guide List cancelled. Status: Ready."
            End If
            QRGFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub btnViewDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Not Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Shared Folder does not exist!."
        Else
            Dim ViewDocsFrm As New ViewDocsForm
            ViewDocsFrm.PassVariable(thisEnquiryCode, thisContactDisplayName)
            ViewDocsFrm.ShowDialog(Me)
            If ViewDocsFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                ViewDocsFrm.Dispose()
            Else
                ViewDocsFrm.Dispose()
            End If
        End If
    End Sub

#End Region

    'Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Cursor.Current = Cursors.WaitCursor
    '    Try
    '        'save changes
    '        Me.Validate()
    '        Me.EnquiryBindingSource.EndEdit()
    '        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
    '        Cursor.Current = Cursors.Default
    '    Catch ex As Exception
    '        MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
    '    End Try
    '    Try
    '        'update Status Strip
    '        ToolStripStatusLabel1.ForeColor = Color.Blue
    '        ToolStripStatusLabel1.Text = "Editing Enquiry Record."
    '        Dim EditDetailsFrm As New EditDetailsForm
    '        EditDetailsFrm.PassVariable(ThisEnquiryId)
    '        'AddSecurityFrm.ShowDialog(Me)
    '        If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '            FormRefresh()
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Black
    '            ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
    '        Else
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Black
    '            ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
    '        End If
    '        EditDetailsFrm.Dispose()
    '        Cursor.Current = Cursors.Default

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub




    '******************************* MenuStrip code

#Region "MenuStrip"

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error("ERROR on Saving data: " & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub


    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim displayAllNamesFrm As New DisplayAllNamesForm
            displayAllNamesFrm.PassVariable()
            displayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim displayAllNamesFrm As New DisplayAllNamesForm
            displayAllNamesFrm.PassVariable()
            displayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim displayDealersFrm As New DisplayDealersForm
            displayDealersFrm.PassVariable(thisDealerId)
            displayDealersFrm.ShowDialog()
            displayDealersFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '************ end of Check Names

    Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click

        If cmbxCustomerType.SelectedIndex > -1 Then
            Cursor.Current = Cursors.WaitCursor
            SaveCustomer()
            FormRefresh()
            Cursor.Current = Cursors.Default
        Else
            MessageBox.Show("Nothing to save!", "Error saving", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim addCommentFrm As New AddCommentForm()
        addCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        addCommentFrm.Dispose()
    End Sub

    Private Sub EditCandDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCandDToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error("ERROR on Saving data: " & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub


    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim addCommentFrm As New AddCommentForm()
        addCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        addCommentFrm.Dispose()
    End Sub

    Private Sub EmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailFrm As New EmailForm
        EmailFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailFrm.Dispose()

    End Sub

    Private Sub ApprovalFormToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem1.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            log.Error("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub EmailApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailApprovalToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim emailAppFrm As New EmailApprovalForm
        emailAppFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If emailAppFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        emailAppFrm.Dispose()
    End Sub

    Private Sub WWWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WWWToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        'launch internet browser for web application
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching web browser."
        System.Diagnostics.Process.Start(My.Settings.YFL_website)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub DeclinedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeclinedToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        thisEnquiryResult = "Declined"
        progressStatus = False
        'check if data has changed
        Dim rowView1 As DataRowView
        'Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        'If Not EnquiryBindingSource.Current Is Nothing And Not ClientBindingSource.Current Is Nothing Then
        If Not CustomerBindingSource.Current Is Nothing Then
            rowView1 = CType(CustomerBindingSource.Current, DataRowView)
            'rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
            If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        CustomerBindingSource.EndEdit()
                        'EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim declinedFrm As New DeclinedForm
            declinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            declinedFrm.ShowDialog(Me)
            If declinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                declinedFrm.Dispose()

                progressStatus = True

            ElseIf declinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                declinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error("DeclinedForm caused an error:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error("AppForm caused an error:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub WithdrawnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawnToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        thisEnquiryResult = "Withdrawn"
        progressStatus = False
        'check if data has changed
        Dim rowView1 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            If Not CustomerBindingSource.Current Is Nothing Then
                rowView1 = CType(CustomerBindingSource.Current, DataRowView)
                'test if any fields have changed
                If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Customer Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   'Define title.
                    'Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        Try
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            Me.Validate()
                            CustomerBindingSource.EndEdit()
                            Me.CustomerTableAdapter.Update(Me.EnquiryWorkSheetDataSet)
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Customer changes saved successfully."
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            MsgBox(ex.Message)
                        End Try
                    Else
                        'User chose No. Do nothing
                    End If
                    'no changes to save. Do nothing
                End If
                'no Customer data - proceed
            End If

        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim declinedFrm As New DeclinedForm
            declinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            declinedFrm.ShowDialog(Me)
            If declinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                declinedFrm.Dispose()

                progressStatus = True

            ElseIf declinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                declinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Withdrawing of Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error("DeclinedForm caused an error:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error("AppForm caused an error:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub MCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MCToolStripMenuItem.Click
        Dim membersCentreUtilObject As New WebUtil
        membersCentreUtilObject.MCLink()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = membersCentreUtilObject.GetLabelForeColor
        ToolStripStatusLabel1.Text = membersCentreUtilObject.GetLabelText
    End Sub

#End Region


    '******************************* end of MenuStrip code

    Private Sub dgvEnquiryCustomers_Click(sender As Object, e As EventArgs) Handles dgvEnquiryCustomers.Click


        Dim thisCustType As Integer = 0
        Dim customerEnquiryType As Integer = -1
        Dim selectedRowView As Data.DataRowView
        Dim selectedRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
        'Get selected row
        selectedRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
        '###################################################
        'Save current customer before customer is lost
        '###################################################
        'check if data has changed
        Dim rowView1 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not Me.CustomerBindingSource.Current Is Nothing Then
            rowView1 = CType(Me.CustomerBindingSource.Current, DataRowView)
            If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                'Compare Current with Proposed
                If Not rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed) Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        SaveCustomer(False)
                    Else
                        ' User choose No. Do nothing
                    End If
                    creditQuestionStateChanged = False
                    customerDataStateChanged = False
                    customerTypeStateChanged = False
                    rbPepStateChanged = False
                Else
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."

                End If
            ElseIf creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or
                   MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Saving changed data."
                    'save changes
                    SaveCustomer(False)
                Else
                    ' User choose No. Do nothing
                End If
                creditQuestionStateChanged = False
                customerDataStateChanged = False
                customerTypeStateChanged = False
                rbPepStateChanged = False
            Else
                'no changes to save
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "No changes to save."
            End If
            'no data entered - proceed
        End If

        '###################################################
        'End of saving customer
        '###################################################
        Cursor.Current = Cursors.WaitCursor
        'Handlers
        RemoveHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        ToolStripStatusLabel1.Text = String.Empty

        'Clear Customer table
        EnquiryWorkSheetDataSet.Tables("Customer").Clear()
        'Load CustomerEnquiry link table
        Me.CustomerEnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.CustomerEnquiry, thisEnquiryId)


        If selectedRowView.Row IsNot Nothing Then
            selectedRow = selectedRowView.Row
            thisCustType = selectedRow.CustomerType 'Individual, Company, Trust
            customerEnquiryType = selectedRow.Type 'main, co-main, guarantor
            'dgvSelectedRow = dgvEnquiryCustomers.CurrentRow.Index
            'load customer table
            Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, selectedRow.CustomerId)
            loadedCustomerId = selectedRow.CustomerId 'Current customer being displayed
            cmbxCustomerType.SelectedIndex = thisCustType
            'lblCustomerType.Text = "Customer Type"
            'lblCustomerType.Location = New Point(60, 9)
            SetCustomerButtons(True)
            'set customer panels
            SetCustomerTypePanel(thisCustType, loadedCustomerId, customerEnquiryType)

            pnlCustomerType.Visible = True
            pnlCustomerType.Enabled = True


        End If
        'Set the selection
        For Each row As DataGridViewRow In dgvEnquiryCustomers.Rows
            If row.Cells(3).Value.ToString = loadedCustomerId Then
                EnquiryCustomersBindingSource.Position = row.Index
                Exit For
            End If
        Next

        'Handlers
        AddHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged

        Cursor.Current = Cursors.Default

    End Sub

    ''' <summary>
    ''' Set the CustomerType panel to be displayed and load data
    ''' </summary>
    ''' <param name="custType">Integer</param>
    ''' <param name="custId">Integer</param>
    ''' <param name="custEnquiryType">Integer</param>
    ''' <remarks>Uses the CustomerType Enum value</remarks>
    Private Sub SetCustomerTypePanel(ByVal custType As Integer, Optional ByVal custId As Integer = 0, Optional ByVal custEnquiryType As Integer = -1)
        SuspendLayout()
        thisTypeOfCustomer = custType
        '#######################
        'Individual, SoleTrader
        '#######################
        If custType = MyEnums.CustomerType.Individual Or custType = MyEnums.CustomerType.SoleTrader Then
            'Handlers
            'RemoveHandler cmbxCustomerEnquiryType.SelectedIndexChanged, AddressOf cmbxCustomerEnquiryType_SelectedIndexChanged
            RemoveHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
            RemoveHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf CmbxRiskAssessSelectedIndexChanged
            RemoveHandler cmbxAcceptanceMethod.SelectedIndexChanged, AddressOf CmbxAcceptanceMethodSelectedIndexChanged
            RemoveHandler rbtnYesCreditQuestion1.CheckedChanged, AddressOf rbtnYesCreditQuestion1_CheckedChanged
            RemoveHandler rbtnYesCreditQuestion2.CheckedChanged, AddressOf rbtnYesCreditQuestion2_CheckedChanged
            RemoveHandler rbtnYesCreditQuestion3.CheckedChanged, AddressOf rbtnYesCreditQuestion3_CheckedChanged
            RemoveHandler rbtnNoCreditQuestion1.CheckedChanged, AddressOf rbtnNoCreditQuestion1_CheckedChanged
            RemoveHandler rbtnNoCreditQuestion2.CheckedChanged, AddressOf rbtnNoCreditQuestion2_CheckedChanged
            RemoveHandler rbtnNoCreditQuestion3.CheckedChanged, AddressOf rbtnNoCreditQuestion3_CheckedChanged
            RemoveHandler rbPepNo.CheckedChanged, AddressOf rbPepNo_CheckedChanged
            RemoveHandler rbPepYes.CheckedChanged, AddressOf rbPepYes_CheckedChanged
            RemoveHandler cmbxTypeOfId.SelectedIndexChanged, AddressOf cmbxTypeOfId_SelectedIndexChanged
            RemoveHandler cmbxTypeDL.SelectedIndexChanged, AddressOf cmbxTypeDL_SelectedIndexChanged
            'panels
            pnlIndividual.Visible = True
            pnlCompany.Visible = False
            pnlCompany.Enabled = False
            'permissions
            If LoggedinPermissionLevel > MyEnums.RolePermissions.StdUser Then
                pnlIndividual.Enabled = True
            Else
                pnlIndividual.Enabled = False
            End If
            'load datasources for combo boxes
            cmbxRiskAssess.DisplayMember = "Value"
            cmbxRiskAssess.DataSource = MyEnums.ToList(GetType(MyEnums.AMLRisk))

            cmbxTypeOfId.DisplayMember = "Value"
            cmbxTypeOfId.DataSource = MyEnums.ToList(GetType(MyEnums.IdType))

            cmbxTypeDL.DisplayMember = "Value"
            cmbxTypeDL.DataSource = MyEnums.ToList(GetType(MyEnums.DriverLicenceType))
            
            'set customerType
            'cmbxCustomerEnquiryType.SelectedIndex = custEnquiryType
            cmbxCustEnquiryType.SelectedIndex = custEnquiryType
            '*** Employment
            If custType = MyEnums.CustomerType.SoleTrader Then
                gpbxEmployment.Text = "Trading activity"
                lblEmployer.Visible = False
                txtbxEmployer.Visible = False
                txtbxEmployer.Enabled = False
                lblSpokeTo.Visible = False
                txtbxSpokeTo.Visible = False
                txtbxSpokeTo.Enabled = False
                btnRangEmpTimeStamp.Visible = False
                btnRangEmpTimeStamp.Enabled = False
                lblDoes.Visible = False
                lblSalutation.Visible = False
                lblWorkCompany.Text = "How long has the business been trading?"
                'lblQuestion1.Location = New Point(316 - lblQuestion1.Width, 48)
                lblFulltime.Text = "Is this sufficient?"
                lblEmploy9.Text = "Do they have financials? Do we have copies?"
                lblWhatDo.Text = "Is this the main source of income?"
                lblHowLong.Text = "What sort of business is it?"
                txtbxQuestion5.Visible = False
                txtbxQuestion5.Enabled = False
                lblPay.Text = "Is it a satificatory business to lend to?"
                lblJobSafe.Text = "Is it registered with the IRD?"
                lblWorker.Visible = False
                txtbxQuestion8.Visible = False
                txtbxQuestion8.Enabled = False
                'ckbxSelfEmployed.Enabled = False
                'ckbxSelfEmployed.Visible = False
                lblBeneficiary.Visible = False
                ckbxBeneficiary.Enabled = False
                ckbxBeneficiary.Visible = False
                lblEmployerNotRung.Visible = False
                ckbxEmployerNotRung.Enabled = False
                ckbxEmployerNotRung.Visible = False
            Else
                gpbxEmployment.Text = "Employment"
                lblEmployer.Visible = True
                txtbxEmployer.Visible = True
                txtbxEmployer.Enabled = True
                lblSpokeTo.Visible = True
                txtbxSpokeTo.Visible = True
                txtbxSpokeTo.Enabled = True
                btnRangEmpTimeStamp.Visible = True
                btnRangEmpTimeStamp.Enabled = True
                lblDoes.Visible = True
                lblSalutation.Visible = True
                lblWorkCompany.Text = "work for your company?"
                lblFulltime.Text = "Are they fulltime permanent or other?"
                lblEmploy9.Text = "Are they PAYE employee, Contractor, Invoice or Other?"
                lblWhatDo.Text = "What does your employee do (job)?"
                lblHowLong.Text = "How long have they been there?"
                txtbxQuestion5.Visible = True
                txtbxQuestion5.Enabled = True
                lblPay.Text = "I need to confirm their pay of"
                lblJobSafe.Text = "Is their job safe?"
                lblWorker.Visible = True
                txtbxQuestion8.Visible = True
                txtbxQuestion8.Enabled = True
                'ckbxSelfEmployed.Enabled = True
                'ckbxSelfEmployed.Visible = True
                lblBeneficiary.Visible = True
                ckbxBeneficiary.Enabled = True
                ckbxBeneficiary.Visible = True
                lblEmployerNotRung.Visible = True
                ckbxEmployerNotRung.Enabled = True
                ckbxEmployerNotRung.Visible = True
                'set visiblity for EnquiryType
                If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                    ckbxEmployerNotRung.Visible = True
                    ckbxEmployerNotRung.Text = "Employer not rung"
                    lblEmployerNotRung.Visible = True
                ElseIf thisTypeOfMainCustomer = MyEnums.mainCustomerType.Company And custEnquiryType = MyEnums.CustomerEnquiryType.Guarantor Then
                    ckbxEmployerNotRung.Visible = True
                    ckbxEmployerNotRung.Text = "Guarantor is a director of the company (borrower) and this is their main employment. (Must ADD comments below)"
                    lblEmployerNotRung.Visible = False
                Else
                    ckbxEmployerNotRung.Visible = False
                    lblEmployerNotRung.Visible = False
                End If

            End If

            'new customer
            If custId > 0 Then
                'set name editability
                If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("XRefId") IsNot DBNull.Value And EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("XRefId").ToString.Length > 0 Then
                    If Not ApplicationAccessRights.CanUpdateCustomerName Then
                        Util.SetReadonlyControls(gpbxCustomerName.Controls)
                    End If
                End If
                'set comboBoxes
                cmbxRiskAssess.SelectedIndex = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AMLRisk")), -1, EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AMLRisk"))
                cmbxTypeOfId.SelectedIndex = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IdType")), -1, EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IdType"))
                cmbxTypeDL.SelectedIndex = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("DriverLicenceType")), -1, EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("DriverLicenceType"))
                If cmbxTypeOfId.SelectedIndex = MyEnums.IdType.NZDriversLicence Then
                    lblTypeDL.Visible = True
                    cmbxTypeDL.Visible = True
                    cmbxTypeDL.Enabled = True
                Else
                    lblTypeDL.Visible = False
                    cmbxTypeDL.Visible = False
                    cmbxTypeDL.Enabled = False
                End If
                cmbxAcceptanceMethod.SelectedIndex = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")), -1, EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod"))
                'Set radio buttons for CreditQuestion
                SetRadioButtonState(1)
                SetRadioButtonState(2)
                SetRadioButtonState(3)
                'Set radio buttons for IsaPep
                SetRbPep()
                'Set Customer Salutation
                custSalutation = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("Salutation")), "", EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("Salutation"))
                'set Rang employer timestamps
                If Not custType = MyEnums.CustomerType.SoleTrader Then
                    If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("EmployerRangdate") IsNot DBNull.Value Then
                        If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("EmployerRangdate") > testDate Then
                            btnRangEmpTimeStamp.Enabled = False
                        Else
                            btnRangEmpTimeStamp.Enabled = True
                        End If
                    Else
                        btnRangEmpTimeStamp.Enabled = True
                    End If
                End If
            Else
                cmbxRiskAssess.SelectedIndex = -1
                'cmbxIndividAcceptanceMethod.SelectedIndex = -1
                cmbxTypeOfId.SelectedIndex = -1
                cmbxTypeDL.SelectedIndex = -1
                cmbxAcceptanceMethod.SelectedIndex = -1
                cmbxTenancyType.SelectedIndex = -1
            End If

            'fix for trigger of comboBoxes.SelectedIndexChanged
            customerDataStateChanged = False
            

            '#######################
            'Company
            '#######################
        ElseIf custType = MyEnums.CustomerType.Company Then
            'Set variables

            'Handlers
            'RemoveHandler cmbxCustomerEnquiryType.SelectedIndexChanged, AddressOf cmbxCustomerEnquiryType_SelectedIndexChanged
            RemoveHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
            'RemoveHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf cmbxRiskAssess_SelectedIndexChanged
            RemoveHandler rbtnYesBusCreditQuestion1.CheckedChanged, AddressOf rbtnYesBusCreditQuestion1_CheckedChanged
            RemoveHandler rbtnYesBusCreditQuestion2.CheckedChanged, AddressOf rbtnYesBusCreditQuestion2_CheckedChanged
            RemoveHandler rbtnYesBusCreditQuestion3.CheckedChanged, AddressOf rbtnYesBusCreditQuestion3_CheckedChanged
            RemoveHandler rbtnNoBusCreditQuestion1.CheckedChanged, AddressOf rbtnNoBusCreditQuestion1_CheckedChanged
            RemoveHandler rbtnNoBusCreditQuestion2.CheckedChanged, AddressOf rbtnNoBusCreditQuestion2_CheckedChanged
            RemoveHandler rbtnNoBusCreditQuestion3.CheckedChanged, AddressOf rbtnNoBusCreditQuestion3_CheckedChanged
            'panels
            pnlCompany.Visible = True
            pnlIndividual.Visible = False
            pnlIndividual.Enabled = False
            'permissions
            If LoggedinPermissionLevel > 2 Then
                pnlCompany.Enabled = True
            Else
                pnlCompany.Enabled = False
            End If
            'load datasources for combo boxes
            ''dgvDtsb01.
            'Dim dgvDtsb01CmbxCol As DataGridViewComboBoxColumn
            'dgvDtsb01CmbxCol = dgvDtsb01.Columns.Item("AMLRisk")
            'With dgvDtsb01CmbxCol
            '    .DataSource = MyEnums.ToList(GetType(MyEnums.AMLRisk))
            '    .ValueMember = "Key"
            '    .DisplayMember = "Value"
            'End With

            'new customer
            If custId > 0 Then
                'If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                '    cmbxCompanyAcceptanceMethod.SelectedIndex = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                'Else
                '    cmbxCompanyAcceptanceMethod.SelectedIndex = -1
                'End If
                If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                    cmbxAcceptanceMethod.SelectedIndex = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                Else
                    cmbxAcceptanceMethod.SelectedIndex = -1
                End If
                lblShareholdersNotice.Visible = False
                lblDirectorsNotice.Visible = False
                'set customerType
                'cmbxCompanyEnquiryType.SelectedIndex = custEnquiryType
                cmbxCustEnquiryType.SelectedIndex = custEnquiryType
                'setup global variables
                dtsbType23 = MyEnums.DtsbType.ShareHolder
                dtsbType01 = MyEnums.DtsbType.Director

                Try
                    'Shareholders
                    Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType23))
                    custCountDtsbType23 = DTSBBindingSource.Count
                Catch ex As System.Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try

                Try
                    'Directors
                    Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType01))
                    custCountDtsbType01 = DTSB01BindingSource.Count
                Catch ex As System.Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
                'set columns to readOnly
                SetDgvDtsbType2ReadOnlyOn()
                SetDgvDtsbType01ReadOnlyOn()

                'Set Business Radio buttons for CreditQuestion
                SetBusRadioButtonState(1)
                SetBusRadioButtonState(2)
                SetBusRadioButtonState(3)
                'References
                Try
                    Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
                    refCount = TradeReferencesBindingSource.Count
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try

                If LoggedinPermissionLevel > MyEnums.RolePermissions.StdUser Then
                    gpbxDtsb23.Enabled = True
                    gpbxDtsb01.Enabled = True
                Else
                    gpbxDtsb23.Enabled = False
                    gpbxDtsb01.Enabled = False
                End If

            Else
                'cmbxCompanyEnquiryType.SelectedIndex = -1
                cmbxCustEnquiryType.SelectedIndex = -1
                'cmbxCompanyAcceptanceMethod.SelectedIndex = -1
                cmbxAcceptanceMethod.SelectedIndex = -1
                gpbxDtsb23.Enabled = False
                lblShareholdersNotice.Text = "A customer must be saved before you can add a shareholder"
                lblShareholdersNotice.Visible = True
                gpbxDtsb01.Enabled = False
                lblDirectorsNotice.Text = "A customer must be saved before you can add a director"
                lblDirectorsNotice.Visible = True
            End If
            'rename labels
            gpbxCompanyInfo.Text = "Company information"
            ckbxRegistered.Visible = True
            ckbxCoExtTrustDdSaved.Text = "Company extract saved"
            ckbxCapital.Text = "?Paid-up capital is sufficient"
            ckbxHistory.Text = "?Company history is satisfactory"
            lblHistoryComments.Text = "Comments on the Company's History"
            lblCompanyAddresses.Text = "Comment on the Company Addresses"
            gpbxDtsb23.Text = "Shareholders"
            ckbxShareIdentified.Text = "?All shareholders identified"
            ckbxShareStructure.Text = "?Shareholding structure identified"
            ckbxShareholders.Text = "?Shareholders are satisfactory"
            lblShareholdersNote.Text = "Comments on Shareholders and their related parties/interests"
            gpbxDtsb01.Text = "Directors"
            ckbxDirectorsTrustees.Text = "?Directors are satisfactory"
            lblDirectors.Text = "Comments on Directors and their related parties/interests"
            gpbxCoTenancy.Text = "Company tenancy"
            gpbxBusinessCredit.Text = "Company credit"

            '#######################
            'Trust
            '#######################
        ElseIf custType = MyEnums.CustomerType.Trust Then
            'Set variables

            'Handlers
            'RemoveHandler cmbxCustomerEnquiryType.SelectedIndexChanged, AddressOf cmbxCustomerEnquiryType_SelectedIndexChanged
            RemoveHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
            RemoveHandler rbtnYesBusCreditQuestion1.CheckedChanged, AddressOf rbtnYesBusCreditQuestion1_CheckedChanged
            RemoveHandler rbtnYesBusCreditQuestion2.CheckedChanged, AddressOf rbtnYesBusCreditQuestion2_CheckedChanged
            RemoveHandler rbtnYesBusCreditQuestion3.CheckedChanged, AddressOf rbtnYesBusCreditQuestion3_CheckedChanged
            RemoveHandler rbtnNoBusCreditQuestion1.CheckedChanged, AddressOf rbtnNoBusCreditQuestion1_CheckedChanged
            RemoveHandler rbtnNoBusCreditQuestion2.CheckedChanged, AddressOf rbtnNoBusCreditQuestion2_CheckedChanged
            RemoveHandler rbtnNoBusCreditQuestion3.CheckedChanged, AddressOf rbtnNoBusCreditQuestion3_CheckedChanged
            'panels
            pnlCompany.Visible = True
            pnlIndividual.Visible = False
            pnlIndividual.Enabled = False
            'load datasources for combo boxes

            'permissions
            If LoggedinPermissionLevel > 2 Then
                pnlCompany.Enabled = True
            Else
                pnlCompany.Enabled = False
            End If
            'new customer
            If custId > 0 Then
                'If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                '    cmbxCompanyAcceptanceMethod.SelectedIndex = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                'Else
                '    cmbxCompanyAcceptanceMethod.SelectedIndex = -1
                'End If
                If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                    cmbxAcceptanceMethod.SelectedIndex = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                Else
                    cmbxAcceptanceMethod.SelectedIndex = -1
                End If
                lblShareholdersNotice.Visible = False
                lblDirectorsNotice.Visible = False
                'set customerType
                'cmbxCompanyEnquiryType.SelectedIndex = custEnquiryType
                cmbxCustEnquiryType.SelectedIndex = custEnquiryType
                'setup global variables
                dtsbType23 = MyEnums.DtsbType.Beneficiary
                dtsbType01 = MyEnums.DtsbType.Trustee
                'Benificiaries
                Try
                    Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType23))
                    custCountDtsbType23 = DTSBBindingSource.Count
                Catch ex As System.Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
                'Trustees
                Try
                    Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType01))
                    custCountDtsbType01 = DTSB01BindingSource.Count
                Catch ex As System.Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
                'set columns to readOnly
                SetDgvDtsbType2ReadOnlyOn()
                SetDgvDtsbType01ReadOnlyOn()

                'Set Radio buttons for CreditQuestion
                SetBusRadioButtonState(1)
                SetBusRadioButtonState(2)
                SetBusRadioButtonState(3)

                Try
                    'References
                    Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
                    refCount = TradeReferencesBindingSource.Count
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try

                If LoggedinPermissionLevel > 2 Then
                    gpbxDtsb23.Enabled = True
                    gpbxDtsb01.Enabled = True
                Else
                    gpbxDtsb23.Enabled = False
                    gpbxDtsb01.Enabled = False
                End If

            Else
                'cmbxCompanyEnquiryType.SelectedIndex = -1
                cmbxCustEnquiryType.SelectedIndex = -1
                'cmbxCompanyAcceptanceMethod.SelectedIndex = -1
                cmbxAcceptanceMethod.SelectedIndex = -1
                gpbxDtsb23.Enabled = False
                lblShareholdersNotice.Text = "A customer must be saved before you can add a beneficiary"
                lblShareholdersNotice.Visible = True
                gpbxDtsb01.Enabled = False
                lblDirectorsNotice.Text = "A customer must be saved before you can add a trustee"
                lblDirectorsNotice.Visible = True
            End If
            'rename labels
            gpbxCompanyInfo.Text = "Trust information"
            ckbxRegistered.Visible = False
            ckbxCoExtTrustDdSaved.Text = "Trust Deed verified and saved"
            ckbxCapital.Text = "?Source of funds is satisfactory"
            ckbxHistory.Text = "?Trust history is satisfactory"
            lblHistoryComments.Text = "Comments on the Trust's History"
            lblCompanyAddresses.Text = "Comment on the Trust Addresses"
            gpbxDtsb23.Text = "Beneficiaries"
            ckbxShareIdentified.Text = "?All beneficiaries identified"
            ckbxShareStructure.Text = "?Beneficiary structure identified"
            ckbxShareholders.Text = "?Beneficiaries are satisfactory"
            lblShareholdersNote.Text = "Comments on beneficiaries and their related parties/interests"
            gpbxDtsb01.Text = "Trustees"
            ckbxDirectorsTrustees.Text = "?Trustees are satisfactory"
            lblDirectors.Text = "Comments on Trustees and their related parties/interests"
            gpbxCoTenancy.Text = "Trust tenancy"
            gpbxBusinessCredit.Text = "Trust credit"

        End If
        ResumeLayout(False)
        PerformLayout()
        'Add Handlers
        If custType = MyEnums.CustomerType.Individual Or custType = MyEnums.CustomerType.SoleTrader Then
            AddHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
            AddHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf CmbxRiskAssessSelectedIndexChanged
            AddHandler cmbxAcceptanceMethod.SelectedIndexChanged, AddressOf CmbxAcceptanceMethodSelectedIndexChanged
            AddHandler cmbxTypeOfId.SelectedIndexChanged, AddressOf cmbxTypeOfId_SelectedIndexChanged
            AddHandler cmbxTypeDL.SelectedIndexChanged, AddressOf cmbxTypeDL_SelectedIndexChanged
            'get user settings
            AddHandler rbtnYesCreditQuestion1.CheckedChanged, AddressOf rbtnYesCreditQuestion1_CheckedChanged
            AddHandler rbtnYesCreditQuestion2.CheckedChanged, AddressOf rbtnYesCreditQuestion2_CheckedChanged
            AddHandler rbtnYesCreditQuestion3.CheckedChanged, AddressOf rbtnYesCreditQuestion3_CheckedChanged
            AddHandler rbtnNoCreditQuestion1.CheckedChanged, AddressOf rbtnNoCreditQuestion1_CheckedChanged
            AddHandler rbtnNoCreditQuestion2.CheckedChanged, AddressOf rbtnNoCreditQuestion2_CheckedChanged
            AddHandler rbtnNoCreditQuestion3.CheckedChanged, AddressOf rbtnNoCreditQuestion3_CheckedChanged
            AddHandler rbPepNo.CheckedChanged, AddressOf rbPepNo_CheckedChanged
            AddHandler rbPepYes.CheckedChanged, AddressOf rbPepYes_CheckedChanged
        ElseIf custType = MyEnums.CustomerType.Company Then
            AddHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
            'AddHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf cmbxRiskAssess_SelectedIndexChanged
            AddHandler rbtnYesBusCreditQuestion1.CheckedChanged, AddressOf rbtnYesBusCreditQuestion1_CheckedChanged
            AddHandler rbtnYesBusCreditQuestion2.CheckedChanged, AddressOf rbtnYesBusCreditQuestion2_CheckedChanged
            AddHandler rbtnYesBusCreditQuestion3.CheckedChanged, AddressOf rbtnYesBusCreditQuestion3_CheckedChanged
            AddHandler rbtnNoBusCreditQuestion1.CheckedChanged, AddressOf rbtnNoBusCreditQuestion1_CheckedChanged
            AddHandler rbtnNoBusCreditQuestion2.CheckedChanged, AddressOf rbtnNoBusCreditQuestion2_CheckedChanged
            AddHandler rbtnNoBusCreditQuestion3.CheckedChanged, AddressOf rbtnNoBusCreditQuestion3_CheckedChanged
        ElseIf custType = MyEnums.CustomerType.Trust Then
            AddHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
            'AddHandler cmbxRiskAssess.SelectedIndexChanged, AddressOf cmbxRiskAssess_SelectedIndexChanged
            AddHandler rbtnYesBusCreditQuestion1.CheckedChanged, AddressOf rbtnYesBusCreditQuestion1_CheckedChanged
            AddHandler rbtnYesBusCreditQuestion2.CheckedChanged, AddressOf rbtnYesBusCreditQuestion2_CheckedChanged
            AddHandler rbtnYesBusCreditQuestion3.CheckedChanged, AddressOf rbtnYesBusCreditQuestion3_CheckedChanged
            AddHandler rbtnNoBusCreditQuestion1.CheckedChanged, AddressOf rbtnNoBusCreditQuestion1_CheckedChanged
            AddHandler rbtnNoBusCreditQuestion2.CheckedChanged, AddressOf rbtnNoBusCreditQuestion2_CheckedChanged
            AddHandler rbtnNoBusCreditQuestion3.CheckedChanged, AddressOf rbtnNoBusCreditQuestion3_CheckedChanged

        End If


    End Sub

    Private Sub cmbxCustomerType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxCustomerType.SelectedIndexChanged
        Dim custType As Integer = DirectCast(cmbxCustomerType.SelectedValue, KeyValuePair(Of Integer, String)).Key
        SetCustomerTypePanel(custType, loadedCustomerId)
        Dim selectedRowView As Data.DataRowView
        Dim selectedRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
        'Get selected row of EnquiryCustomers
        If EnquiryCustomersBindingSource.Current IsNot Nothing Then
            selectedRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
            If selectedRowView.Row IsNot Nothing Then
                selectedRow = selectedRowView.Row
                'has the CustomerType selection changed
                If Not cmbxCustomerType.SelectedIndex = selectedRow.CustomerType Then
                    customerTypeStateChanged = True
                Else
                    customerTypeStateChanged = False
                End If
            End If
        End If
        'customer controls in CustomerType panel
        If cmbxCustomerType.SelectedIndex > -1 Then
            lblCustEnquiryType.Visible = True
            cmbxCustEnquiryType.Visible = True
            cmbxCustEnquiryType.Enabled = True
            lblAcceptanceMethod.Visible = True
            cmbxAcceptanceMethod.Visible = True
            cmbxAcceptanceMethod.Enabled = True
            SetCustomerButtons(True)
        Else
            lblCustEnquiryType.Visible = False
            cmbxCustEnquiryType.Visible = False
            cmbxCustEnquiryType.Enabled = False
            lblAcceptanceMethod.Visible = False
            cmbxAcceptanceMethod.Visible = False
            cmbxAcceptanceMethod.Enabled = False
            SetCustomerButtons(False)
        End If


    End Sub

#Region "Name"

    Private Sub txtbxFirstName_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxFirstName.LostFocus
        'Check String has at least 1 Character
        If txtbxFirstName.Text.Length > 0 Then
            'first char upper case 
            txtbxFirstName.Text = Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtbxFirstName.Text.ToLower)
        End If
    End Sub

    Private Sub txtMiddleNames_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMiddleNames.LostFocus
        'Check String has at least 1 Character
        If txtMiddleNames.Text.Length > 0 Then
            txtMiddleNames.Text = Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtMiddleNames.Text.ToLower) 'first char upper case 
        End If
    End Sub

    Private Sub txtLastName_LostFocus(sender As Object, e As EventArgs) Handles txtLastName.LostFocus
        Dim char1FirstName As String = String.Empty
        'Check String has at least 1 Character
        Try
            If txtLastName.Text.Length > 0 Then
                txtLastName.Text = Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtLastName.Text.ToLower)
                If txtbxFirstName.Text.Length > 0 Then
                    'Get the 1st Character 
                    char1FirstName = txtbxFirstName.Text.Substring(0, 1).ToUpper & " "
                Else
                    char1FirstName = ""
                End If
                'set Salutation
                'txtDisplayName.Text = char1FirstName & txtLastName.Text
                custSalutation = char1FirstName & txtLastName.Text
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

#End Region



    Private Sub btnAddCustomer_Click(sender As Object, e As EventArgs) Handles btnAddCustomer.Click
        CustomerBindingSource.CancelEdit()
        'remove event handlers
        RemoveHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        'RemoveHandler cmbxCustomerEnquiryType.SelectedIndexChanged, AddressOf cmbxCustomerEnquiryType_SelectedIndexChanged
        'RemoveHandler cmbxCompanyEnquiryType.SelectedIndexChanged, AddressOf cmbxCompanyEnquiryType_SelectedIndexChanged
        RemoveHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
        'Clear Customer table
        EnquiryWorkSheetDataSet.Tables("Customer").Clear()
        loadedCustomerId = 0

        pnlCustomerType.Visible = True
        pnlCustomerType.Enabled = True
        'lblCustomerType.Text = "Select Customer Type"
        'lblCustomerType.Location = New Point(30, 9)
        pnlIndividual.Visible = False
        pnlIndividual.Enabled = False
        pnlCompany.Visible = False
        pnlCompany.Enabled = False
        'set comboboxes
        cmbxCustomerType.SelectedIndex = -1
        'cmbxCustomerEnquiryType.SelectedIndex = -1
        'cmbxCompanyEnquiryType.SelectedIndex = -1
        cmbxCustEnquiryType.SelectedIndex = -1
        'customer controls in CustomerType panel
        lblCustEnquiryType.Visible = False
        cmbxCustEnquiryType.Visible = False
        cmbxCustEnquiryType.Enabled = False
        lblAcceptanceMethod.Visible = False
        cmbxAcceptanceMethod.Visible = False
        cmbxAcceptanceMethod.Enabled = False
        SetCustomerButtons(False)

        'last thing
        AddHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        'AddHandler cmbxCustomerEnquiryType.SelectedIndexChanged, AddressOf cmbxCustomerEnquiryType_SelectedIndexChanged
        'AddHandler cmbxCompanyEnquiryType.SelectedIndexChanged, AddressOf cmbxCompanyEnquiryType_SelectedIndexChanged
        AddHandler cmbxCustEnquiryType.SelectedIndexChanged, AddressOf cmbxCustEnquiryType_SelectedIndexChanged
    End Sub


    Private Sub SaveCustomer(Optional ByVal reload As Boolean = True)
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        'check data entry | validate fields        
        Dim checkFieldsMessage As String = ""
        If cmbxCustomerType.SelectedIndex = -1 Then
            checkFieldsMessage = checkFieldsMessage + "Please select the Customer Type" + vbCrLf
        End If
        'If cmbxCustomerEnquiryType.SelectedIndex = -1 And cmbxCompanyEnquiryType.SelectedIndex = -1 Then
        '    checkFieldsMessage = checkFieldsMessage + "Please select the Customer Enquiry Type" + vbCrLf
        'End If
        If cmbxCustEnquiryType.SelectedIndex = -1 Then
            checkFieldsMessage = checkFieldsMessage + "Please select the Customer Enquiry Type" + vbCrLf
        End If
        '########################################
        'Individual, SoleTrader
        '########################################
        If cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Individual Or cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.SoleTrader Then
            If Not txtLastName.Text.Length > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please add customer name." + vbCrLf
            End If
            'If ckbxSelfEmployed.CheckState = CheckState.Checked Then
            '    If rtxtbxStabilityOther.Text.Length = 0 Then
            '        checkFieldsMessage = checkFieldsMessage + "Please add comments regarding references that you rang to establish work stability." + vbCrLf
            '    End If
            'End If
            If txtbxEmployer.Text.Length > 0 Then
                If txtbxSpokeTo.Text.Length = 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter who you spoke to at the employer." + vbCrLf
                End If
                If txtbxQuestion1.Text.Length = 0 Or txtbxQuestion2.Text.Length = 0 Or txtbxQuestion3.Text.Length = 0 Or txtbxQuestion4.Text.Length = 0 Or txtbxQuestion5.Text.Length = 0 Or txtbxQuestion6.Text.Length = 0 Or txtbxQuestion7.Text.Length = 0 Or txtbxQuestion8.Text.Length = 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please answer all questions." + vbCrLf
                End If
            End If
            If rbPepState = 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please select whether customer is a politically exposed person." + vbCrLf
            End If

            '########################################
            'Company
            '########################################
        ElseIf cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Company Then
            If Not txtCompanyName.Text.Length > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please add Company legal name." + vbCrLf
            End If
            '########################################
            'Trust
            '########################################
        ElseIf cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Trust Then
            If Not txtCompanyName.Text.Length > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please add Trust name." + vbCrLf
            End If

        End If

        If checkFieldsMessage.Length > 0 Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: There are errors please view message box."
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
            Cursor.Current = Cursors.Default
        Else
            'No validation errors
            If loadedCustomerId > 0 Then 'Customer already loaded
                Try
                    If Not CustomerBindingSource.Current Is Nothing Then
                        'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        Application.DoEvents()
                        ''CustomerEnquiry - Get row index
                        'EnquiryWorkSheetDataSet.Tables("CustomerEnquiry").Clear()
                        'Me.TableAdapterManager.CustomerEnquiryTableAdapter.GetDataByEnquiryId(Me.EnquiryWorkSheetDataSet.CustomerEnquiry, ThisEnquiryId)
                        'Dim rowIndex As Integer = 0
                        'Dim i As Integer
                        'For i = 0 To EnquiryWorkSheetDataSet.CustomerEnquiry.Rows.Count - 1
                        '    If EnquiryWorkSheetDataSet.CustomerEnquiry.Rows(i)("CustomerId") = LoadedCustomerId Then
                        '        rowIndex = i
                        '        Exit For
                        '    End If
                        'Next
                        'Dim customerEnquiryRow As EnquiryWorkSheetDataSet.CustomerEnquiryRow
                        'customerEnquiryRow = Me.EnquiryWorkSheetDataSet.CustomerEnquiry(rowIndex)
                        'customerEnquiryRow.BeginEdit()
                        'customerEnquiryRow.Type = cmbxCustomerEnquiryType.SelectedIndex

                        'update Customer fields not auto updated
                        Dim customerRow As EnquiryWorkSheetDataSet.CustomerRow
                        customerRow = Me.EnquiryWorkSheetDataSet.Customer(0)
                        customerRow.CustomerType = cmbxCustomerType.SelectedIndex
                        '########################################
                        'Individual
                        '########################################
                        If cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Individual Or cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.SoleTrader Then
                            'If cmbxIndividAcceptanceMethod.SelectedIndex > -1 Then
                            '    customerRow.AcceptanceMethod = cmbxIndividAcceptanceMethod.SelectedIndex
                            'End If
                            If cmbxAcceptanceMethod.SelectedIndex > -1 Then
                                customerRow.AcceptanceMethod = cmbxAcceptanceMethod.SelectedIndex
                            End If
                            customerRow.Salutation = custSalutation
                            customerRow.AMLRisk = cmbxRiskAssess.SelectedIndex
                            customerRow.IdType = cmbxTypeOfId.SelectedIndex
                            customerRow.DriverLicenceType = cmbxTypeDL.SelectedIndex
                            'update CreditQuestion States
                            If creditQuestionStateChanged Then
                                customerRow.CreditQuestion1 = creditQuestion1State
                                customerRow.CreditQuestion2 = creditQuestion2State
                                customerRow.CreditQuestion3 = creditQuestion3State
                            End If

                            Select Case rbPepState
                                Case 0
                                    'no state
                                Case 1
                                    'False
                                    customerRow.IsaPEP = False
                                Case 2
                                    'True
                                    customerRow.IsaPEP = True
                            End Select

                            '########################################
                            'Company
                            '########################################
                        ElseIf cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Company Then
                            'If cmbxCompanyAcceptanceMethod.SelectedIndex > -1 Then
                            '    customerRow.AcceptanceMethod = cmbxCompanyAcceptanceMethod.SelectedIndex
                            'End If
                            If cmbxAcceptanceMethod.SelectedIndex > -1 Then
                                customerRow.AcceptanceMethod = cmbxAcceptanceMethod.SelectedIndex
                            End If
                            'update CreditQuestion States
                            If creditQuestionStateChanged Then
                                customerRow.CreditQuestion1 = creditQuestion1State
                                customerRow.CreditQuestion2 = creditQuestion2State
                                customerRow.CreditQuestion3 = creditQuestion3State
                            End If


                            '########################################
                            'Trust
                            '########################################
                        ElseIf cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Trust Then
                            'If cmbxCompanyAcceptanceMethod.SelectedIndex > -1 Then
                            '    customerRow.AcceptanceMethod = cmbxCompanyAcceptanceMethod.SelectedIndex
                            'End If
                            If cmbxAcceptanceMethod.SelectedIndex > -1 Then
                                customerRow.AcceptanceMethod = cmbxAcceptanceMethod.SelectedIndex
                            End If
                            'update CreditQuestion States
                            If creditQuestionStateChanged Then
                                customerRow.CreditQuestion1 = creditQuestion1State
                                customerRow.CreditQuestion2 = creditQuestion2State
                                customerRow.CreditQuestion3 = creditQuestion3State
                            End If


                        End If


                        Me.Validate()
                        'CustomerEnquiryBindingSource.EndEdit()

                        customerRow.EndEdit()
                        CustomerBindingSource.EndEdit()
                        'customerRow.AcceptChanges()

                        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        Me.CustomerTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Customer)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                        'ReLoad in Customers for Enquiry
                        If reload = True Then
                            EnquiryWorkSheetDataSet.Tables("EnquiryCustomers").Clear()
                            Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
                            'for setting element sizing
                            custCount = EnquiryCustomersBindingSource.Count
                        End If

                    Else
                        'no current record in binding source - do not proceed
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                    End If
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
            Else 'no Customer Loaded so a New Customer
                Try
                    SuspendLayout()
                    'Dim custType As Integer = cmbxCustomerType.SelectedIndex
                    'Dim custEnquiryType As Integer = cmbxCustomerEnquiryType.SelectedIndex

                    'Set variables
                    Dim customerRow As EnquiryWorkSheetDataSet.CustomerRow
                    '########################################
                    'Individual
                    '########################################
                    If cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Individual Or cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.SoleTrader Then
                        'Dim custAcceptanceMethod As Integer = cmbxIndividAcceptanceMethod.SelectedIndex
                        Dim custAcceptanceMethod As Integer = cmbxAcceptanceMethod.SelectedIndex
                        Dim custTitle As String = cmbxCustomerTitle.SelectedItem
                        Dim custFirstname As String = Trim(txtbxFirstName.Text)
                        Dim custMiddleNames As String = Trim(txtMiddleNames.Text)
                        Dim custLastname As String = Trim(txtLastName.Text)
                        'Dim custSalutation As String = Trim(txtDisplayName.Text)
                        Dim custIdCurrent As Boolean = ckbxIdCurrent.Checked
                        Dim custIdIsPerson As String = txtIdIsPerson.Text
                        Dim custResidency As String = txtResidency.Text
                        Dim custAgeOfId As String = txtAgeOfId.Text
                        Dim custRiskAssess As Integer = cmbxRiskAssess.SelectedIndex
                        Dim custTypeOfId As Integer = cmbxTypeOfId.SelectedIndex
                        Dim custTypeDL As Integer = cmbxTypeDL.SelectedIndex
                        Dim custTenancy As String = txtbxTenancy.Text
                        Dim custTenancyType As String = cmbxTenancyType.SelectedValue
                        Dim custResidenceSatis As String = txtbxResidenceSatis.Text
                        Dim custTenancyEnviron As String = txtbxTenancyEnviron.Text
                        Dim custEstablishTenancy As String = txtbxEstablishTenancy.Text
                        Dim custEmployer As String = txtbxEmployer.Text
                        Dim custSpokeTo As String = txtbxSpokeTo.Text
                        Dim custQuestion1 As String = txtbxQuestion1.Text
                        Dim custQuestion2 As String = txtbxQuestion2.Text
                        Dim custQuestion3 As String = txtbxQuestion3.Text
                        Dim custQuestion4 As String = txtbxQuestion4.Text
                        Dim custQuestion5 As String = txtbxQuestion5.Text
                        Dim custQuestion6 As String = txtbxQuestion6.Text
                        Dim custQuestion7 As String = txtbxQuestion7.Text
                        Dim custQuestion8 As String = txtbxQuestion8.Text
                        Dim custQuestion9 As String = txtbxQuestion9.Text
                        'Dim custSelfEmployed As Boolean = ckbxSelfEmployed.Checked
                        Dim custBeneficiary As Boolean = ckbxBeneficiary.Checked
                        Dim custEmployerNotRung As Boolean = ckbxEmployerNotRung.Checked
                        Dim custStabilityOther As String = rtxtbxStabilityOther.Text
                        Dim custCreditHistory As String = txtbxCreditHistory.Text
                        Dim custCreditDefaults As String = txtbxCreditDefaults.Text
                        Dim custCreditPPSR As String = txtbxCreditPPSR.Text

                        'Me.CustomerTableAdapter.FillByNewCustomer(Me.EnquiryWorkSheetDataSet.Customer, thisEnquiryId, cmbxCustomerType.SelectedIndex, cmbxCustomerEnquiryType.SelectedIndex)
                        Me.CustomerTableAdapter.FillByNewCustomer(Me.EnquiryWorkSheetDataSet.Customer, thisEnquiryId, cmbxCustomerType.SelectedIndex, cmbxCustEnquiryType.SelectedIndex)
                        loadedCustomerId = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("CustomerId")
                        'update fields

                        customerRow = Me.EnquiryWorkSheetDataSet.Customer(0)
                        customerRow.BeginEdit()
                        'customerRow.CustomerType = custType
                        customerRow.AcceptanceMethod = custAcceptanceMethod
                        customerRow.Title = custTitle
                        customerRow.FirstName = custFirstname
                        customerRow.MiddleNames = custMiddleNames
                        customerRow.LastName = custLastname
                        customerRow.Salutation = custSalutation
                        customerRow.IDCurrent = custIdCurrent
                        customerRow.IDPerson = custIdIsPerson
                        customerRow.ResStatusAccept = custResidency
                        customerRow.IDLegitimate = custAgeOfId
                        customerRow.AMLRisk = custRiskAssess
                        customerRow.TenancyLength = custTenancy
                        customerRow.TenancyType = custTenancyType
                        customerRow.TenancySatisfactory = custResidenceSatis
                        customerRow.TenancyEnvironment = custTenancyEnviron
                        customerRow.TenancyEstablish = custEstablishTenancy
                        'update employment
                        customerRow.EmployerName = custEmployer
                        customerRow.EmployerSpoketoName = custSpokeTo
                        customerRow.StabilityQ1 = custQuestion1
                        customerRow.StabilityQ2 = custQuestion2
                        customerRow.StabilityQ3 = custQuestion3
                        customerRow.StabilityQ4 = custQuestion4
                        customerRow.StabilityQ5 = custQuestion5
                        customerRow.StabilityQ6 = custQuestion6
                        customerRow.StabilityQ7 = custQuestion7
                        customerRow.StabilityQ8 = custQuestion8
                        customerRow.StabilityQ9 = custQuestion9
                        'customerRow.SelfEmployed = custSelfEmployed
                        customerRow.Beneficiary = custBeneficiary
                        customerRow.EmployerNotRung = custEmployerNotRung
                        customerRow.StabilityComments = custStabilityOther
                        'update credit
                        customerRow.CreditComments = custCreditHistory
                        customerRow.CreditExplanation = custCreditDefaults
                        customerRow.CreditOther = custCreditPPSR
                        'update CreditQuestion States
                        customerRow.CreditQuestion1 = creditQuestion1State
                        customerRow.CreditQuestion2 = creditQuestion2State
                        customerRow.CreditQuestion3 = creditQuestion3State
                        Select Case rbPepState
                            Case 0 'no state
                            Case 1 'False
                                customerRow.IsaPEP = False
                            Case 2 'True
                                customerRow.IsaPEP = True
                        End Select

                        Me.Validate()
                        customerRow.EndEdit()

                        '########################################
                        'Company
                        '########################################
                    ElseIf cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Company Then
                        'Dim custAcceptanceMethod As Integer = cmbxIndividAcceptanceMethod.SelectedIndex
                        Dim custAcceptanceMethod As Integer = cmbxAcceptanceMethod.SelectedIndex
                        Dim companyName As String = Trim(txtCompanyName.Text)
                        Dim registered As Boolean = ckbxRegistered.Checked
                        Dim coExtTrustDdSaved As Boolean = ckbxCoExtTrustDdSaved.Checked
                        Dim returns As Boolean = ckbxReturns.Checked
                        Dim capital As Boolean = ckbxCapital.Checked
                        Dim history As Boolean = ckbxHistory.Checked
                        Dim historyComments As String = txtbxHistoryComments.Text
                        Dim addresses As Boolean = ckbxAddresses.Checked
                        Dim addressesComments As String = txtbxAddressesComments.Text
                        Dim shareholders As Boolean = ckbxShareholders.Checked
                        Dim shareholdersComments As String = txtShareholdersComments.Text
                        Dim directorsTrustees As Boolean = ckbxDirectorsTrustees.Checked
                        Dim directorsTrusteesComments As String = txtDirectorsTrustees.Text

                        'Me.CustomerTableAdapter.FillByNewCustomer(Me.EnquiryWorkSheetDataSet.Customer, thisEnquiryId, cmbxCustomerType.SelectedIndex, cmbxCompanyEnquiryType.SelectedIndex)
                        Me.CustomerTableAdapter.FillByNewCustomer(Me.EnquiryWorkSheetDataSet.Customer, thisEnquiryId, cmbxCustomerType.SelectedIndex, cmbxCustEnquiryType.SelectedIndex)
                        loadedCustomerId = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("CustomerId")
                        'update fields
                        customerRow = Me.EnquiryWorkSheetDataSet.Customer(0)
                        customerRow.BeginEdit()
                        'customerRow.CustomerType = custType
                        customerRow.AcceptanceMethod = custAcceptanceMethod
                        customerRow.LegalName = companyName
                        customerRow.BusinessRegistered = registered
                        customerRow.CoExtractTrustDeed = coExtTrustDdSaved
                        customerRow.BusinessReturnsFiled = returns
                        customerRow.BusinessPaidUpCapital = capital
                        customerRow.BusinessHistorySatisfactory = history
                        customerRow.BusinessHistoryNotes = historyComments
                        customerRow.BusinessAddressSatisfactory = addresses
                        customerRow.BusinessAddressNotes = addressesComments
                        customerRow.BusinessShareholders = shareholders
                        customerRow.BusinessShareholderNotes = shareholdersComments
                        customerRow.BusinessDirectors = directorsTrustees
                        customerRow.BusinessDirectorNotes = directorsTrusteesComments

                        Me.Validate()
                        customerRow.EndEdit()

                        '########################################
                        'Trust
                        '########################################
                    ElseIf cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Trust Then
                        'Dim custAcceptanceMethod As Integer = cmbxIndividAcceptanceMethod.SelectedIndex
                        Dim custAcceptanceMethod As Integer = cmbxAcceptanceMethod.SelectedIndex
                        Dim companyName As String = Trim(txtCompanyName.Text)
                        Dim registered As Boolean = ckbxRegistered.Checked
                        Dim coExtTrustDdSaved As Boolean = ckbxCoExtTrustDdSaved.Checked
                        Dim returns As Boolean = ckbxReturns.Checked
                        Dim capital As Boolean = ckbxCapital.Checked
                        Dim history As Boolean = ckbxHistory.Checked
                        Dim historyComments As String = txtbxHistoryComments.Text
                        Dim addresses As Boolean = ckbxAddresses.Checked
                        Dim addressesComments As String = txtbxAddressesComments.Text
                        Dim shareholders As Boolean = ckbxShareholders.Checked
                        Dim shareholdersComments As String = txtShareholdersComments.Text
                        Dim directorsTrustees As Boolean = ckbxDirectorsTrustees.Checked
                        Dim directorsTrusteesComments As String = txtDirectorsTrustees.Text

                        'Me.CustomerTableAdapter.FillByNewCustomer(Me.EnquiryWorkSheetDataSet.Customer, thisEnquiryId, cmbxCustomerType.SelectedIndex, cmbxCompanyEnquiryType.SelectedIndex)
                        Me.CustomerTableAdapter.FillByNewCustomer(Me.EnquiryWorkSheetDataSet.Customer, thisEnquiryId, cmbxCustomerType.SelectedIndex, cmbxCustEnquiryType.SelectedIndex)
                        loadedCustomerId = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("CustomerId")
                        'update fields
                        customerRow = Me.EnquiryWorkSheetDataSet.Customer(0)
                        customerRow.BeginEdit()
                        'customerRow.CustomerType = custType
                        customerRow.AcceptanceMethod = custAcceptanceMethod
                        customerRow.LegalName = companyName
                        customerRow.BusinessRegistered = registered
                        customerRow.CoExtractTrustDeed = coExtTrustDdSaved
                        customerRow.BusinessReturnsFiled = returns
                        customerRow.BusinessPaidUpCapital = capital
                        customerRow.BusinessHistorySatisfactory = history
                        customerRow.BusinessHistoryNotes = historyComments
                        customerRow.BusinessAddressSatisfactory = addresses
                        customerRow.BusinessAddressNotes = addressesComments
                        customerRow.BusinessShareholders = shareholders
                        customerRow.BusinessShareholderNotes = shareholdersComments
                        customerRow.BusinessDirectors = directorsTrustees
                        customerRow.BusinessDirectorNotes = directorsTrusteesComments

                        Me.Validate()
                        customerRow.EndEdit()

                    End If


                    Me.CustomerTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Customer)
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Customer saved successfully."
                    'ReLoad in Customers for Enquiry
                    If reload = True Then
                        Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
                        Me.CustomerEnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.CustomerEnquiry, thisEnquiryId)
                        'for setting element sizing
                        custCount = EnquiryCustomersBindingSource.Count
                    End If

                    'Call layout
                    ResumeLayout()
                    'Cursor.Current = Cursors.Default

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                'ReLoad in Customers for Enquiry
                If reload = True Then
                    Try
                        Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
                        Me.CustomerEnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.CustomerEnquiry, thisEnquiryId)
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                End If



            End If
            'reset changed states
            creditQuestionStateChanged = False
            customerDataStateChanged = False
            customerTypeStateChanged = False
            rbPepStateChanged = False

            If loadedCustomerId > 0 Then
                If cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Company Or cmbxCustomerType.SelectedIndex = MyEnums.CustomerType.Trust Then
                    dgvDTSBType2.Enabled = True
                    dgvDtsb01.Enabled = True
                End If
                'reselect saved customer in dgv
                If reload = True Then
                    For Each row As DataGridViewRow In dgvEnquiryCustomers.Rows
                        If row.Cells(3).Value.ToString = loadedCustomerId Then
                            EnquiryCustomersBindingSource.Position = row.Index
                            Exit For
                        End If
                    Next
                End If


            End If

            ''fix issue of select indicator changing in dgvEnquiryCustomers
            'dgvEnquiryCustomers.Rows(dgvSelectedRow).Selected = True
            Cursor.Current = Cursors.Default
        End If
    End Sub


    Private Sub btnSaveCustomer_Click(sender As Object, e As EventArgs) Handles btnSaveCustomer.Click
        Call SaveCustomer()

    End Sub

#Region "Individual Radio buttons"

    ''' <summary>
    ''' Set the Checked state of a radio button group
    ''' </summary>
    ''' <param name="num">Integer value for the radio button</param>
    ''' <remarks>QuestionYesSet[Num] and QuestionNoset[Num] radio buttons in group</remarks>
    Private Sub SetRadioButtonState(ByVal num As Integer)
        'set the radio button states for Credit Questions 
        Dim QuestionYesSet As String = "rbtnYesCreditQuestion" & num
        Dim QuestionNoset As String = "rbtnNoCreditQuestion" & num
        Dim CheckState As String = "CreditQuestion" & num
        'get State of Question from database
        Dim StateQuestion As Boolean = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item(CheckState)
        Try
            'Look through Controls Collection to find 'QuestionYesSet'
            Dim controls1() As Control = Me.Controls.Find(QuestionYesSet, True)
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton1 As RadioButton = DirectCast(controls1(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton1.Checked = False
                    Case 1
                        ThisRadioButton1.Checked = True
                    Case Else
                        ThisRadioButton1.Checked = False
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Try
            'Look through Controls Collection to find 'QuestionNoSet'
            Dim controls2() As Control = Me.Controls.Find(QuestionNoset, True)
            If controls2.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton2 As RadioButton = DirectCast(controls2(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton2.Checked = True
                    Case 1
                        ThisRadioButton2.Checked = False
                    Case Else
                        ThisRadioButton2.Checked = True
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub




    Private Sub rbtnYesCreditQuestion1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnYesCreditQuestion1.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion1State = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 1 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub rbtnNoCreditQuestion1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNoCreditQuestion1.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion1State = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 1 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnYesCreditQuestion2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnYesCreditQuestion2.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion2State = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 2 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnNoCreditQuestion2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNoCreditQuestion2.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion2State = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 2 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnYesCreditQuestion3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnYesCreditQuestion3.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion3State = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 3 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnNoCreditQuestion3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNoCreditQuestion3.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion3State = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 3 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region

#Region "Business Radio buttons"

    ''' <summary>
    ''' Set the Checked state of a Business radio button group
    ''' </summary>
    ''' <param name="num">Integer value for the radio button</param>
    ''' <remarks>QuestionYesSet[Num] and QuestionNoset[Num] radio buttons in group</remarks>
    Private Sub SetBusRadioButtonState(ByVal num As Integer)
        'set the radio button states for Credit Questions
        Dim QuestionYesSet As String = "rbtnYesBusCreditQuestion" & num
        Dim QuestionNoset As String = "rbtnNoBusCreditQuestion" & num
        Dim CheckState As String = "CreditQuestion" & num
        'get State of Question from database
        Dim StateQuestion As Boolean = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item(CheckState)
        Try
            'Look through Controls Collection to find 'QuestionYesSet'
            Dim controls1() As Control = Me.Controls.Find(QuestionYesSet, True)
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton1 As RadioButton = DirectCast(controls1(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton1.Checked = False
                    Case 1
                        ThisRadioButton1.Checked = True
                    Case Else
                        ThisRadioButton1.Checked = False
                End Select
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Try
            'Look through Controls Collection to find 'QuestionNoSet'
            Dim controls2() As Control = Me.Controls.Find(QuestionNoset, True)
            If controls2.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton2 As RadioButton = DirectCast(controls2(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton2.Checked = True
                    Case 1
                        ThisRadioButton2.Checked = False
                    Case Else
                        ThisRadioButton2.Checked = True
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub




    Private Sub rbtnYesBusCreditQuestion1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnYesBusCreditQuestion1.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion1State = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 1 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub rbtnNoBusCreditQuestion1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNoBusCreditQuestion1.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion1State = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 1 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnYesBusCreditQuestion2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnYesBusCreditQuestion2.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion2State = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 2 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnNoBusCreditQuestion2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNoBusCreditQuestion2.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion2State = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 2 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnYesBusCreditQuestion3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnYesBusCreditQuestion3.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion3State = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 3 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnNoBusCreditQuestion3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNoBusCreditQuestion3.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    creditQuestion3State = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Question 3 = " & rb.Text
                End If
                'set state change tracking
                creditQuestionStateChanged = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region


    Private Sub btnCancelCustomer_Click(sender As Object, e As EventArgs) Handles btnCancelCustomer.Click
        Try
            'check if data has changed
            Dim rowView1 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not Me.CustomerBindingSource.Current Is Nothing Then
                rowView1 = CType(Me.CustomerBindingSource.Current, DataRowView)
                'test if any fields have changed
                If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        SaveCustomer()
                    Else
                        ' User choose No. Do nothing
                    End If
                    'creditQuestionStateChanged = False
                    'customerDataStateChanged = False
                    'customerTypeStateChanged = False

                Else
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'lets do it
        CustomerBindingSource.CancelEdit()
        'Clear Customer table
        EnquiryWorkSheetDataSet.Tables("Customer").Clear()
        'panels     
        SetCustomerButtons(False)
        pnlIndividual.Visible = False
        pnlIndividual.Enabled = False
        pnlCustomerType.Visible = False
        pnlCustomerType.Enabled = False
        pnlCompany.Visible = False
        pnlCompany.Enabled = False
        'loaded customer
        loadedCustomerId = 0


    End Sub

    'Private Sub cmbxCustomerEnquiryType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxCustomerEnquiryType.SelectedIndexChanged
    '    'main, co-main, guarantor
    '    Dim rowIndex As Integer = -1
    '    Dim i As Integer
    '    For i = 0 To EnquiryWorkSheetDataSet.CustomerEnquiry.Rows.Count - 1
    '        If EnquiryWorkSheetDataSet.CustomerEnquiry.Rows(i)("CustomerId") = loadedCustomerId Then
    '            rowIndex = i
    '            Exit For
    '        End If
    '    Next
    '    'test if CustomerEnquiry exists
    '    If Not rowIndex = -1 Then
    '        Dim customerEnquiryRow As EnquiryWorkSheetDataSet.CustomerEnquiryRow
    '        customerEnquiryRow = Me.EnquiryWorkSheetDataSet.CustomerEnquiry(rowIndex)
    '        customerEnquiryRow.BeginEdit()
    '        customerEnquiryRow.Type = cmbxCustomerEnquiryType.SelectedIndex

    '        Me.Validate()
    '        customerEnquiryRow.EndEdit()
    '        Me.CustomerEnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.CustomerEnquiry)
    '        'ReLoad in Customers for Enquiry
    '        Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
    '    End If

    'End Sub

    Private Sub btnDeleteCustomer_Click(sender As Object, e As EventArgs) Handles btnDeleteCustomer.Click
        'Handlers
        RemoveHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        Cursor.Current = Cursors.WaitCursor
        CustomerBindingSource.CancelEdit()
        Try
            Me.CustomerTableAdapter.DeleteThisCustomer(loadedCustomerId)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try
        'panels        
        pnlIndividual.Visible = False
        pnlIndividual.Enabled = False
        pnlCustomerType.Visible = False
        pnlCustomerType.Enabled = False
        pnlCompany.Visible = False
        pnlCompany.Enabled = False
        'loaded customer
        loadedCustomerId = 0
        'Clear Customer tables
        EnquiryWorkSheetDataSet.Tables("Customer").Clear()
        EnquiryWorkSheetDataSet.Tables("CustomerEnquiry").Clear()

        'Refresh form
        AddHandler cmbxCustomerType.SelectedIndexChanged, AddressOf cmbxCustomerType_SelectedIndexChanged
        FormRefresh()

        Cursor.Current = Cursors.Default
    End Sub

    'Private Sub CmbxIndividAcceptanceMethodSelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxIndividAcceptanceMethod.SelectedIndexChanged
    '    customerDataStateChanged = True
    'End Sub
    Private Sub CmbxAcceptanceMethodSelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxAcceptanceMethod.SelectedIndexChanged
        customerDataStateChanged = True
    End Sub

    'Private Sub CmbxCompanyAcceptanceMethodSelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxCompanyAcceptanceMethod.SelectedIndexChanged
    '    customerDataStateChanged = True
    'End Sub

    Private Sub CmbxRiskAssessSelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxRiskAssess.SelectedIndexChanged
        customerDataStateChanged = True
    End Sub

    Private Sub cmbxTypeOfId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxTypeOfId.SelectedIndexChanged
        If cmbxTypeOfId.SelectedIndex = MyEnums.IdType.NZDriversLicence Then
            lblTypeDL.Visible = True
            cmbxTypeDL.Visible = True
            cmbxTypeDL.Enabled = True
            cmbxTypeDL.SelectedIndex = -1
        Else
            lblTypeDL.Visible = False
            cmbxTypeDL.Visible = False
            cmbxTypeDL.Enabled = False
            cmbxTypeDL.SelectedIndex = -1
        End If
        customerDataStateChanged = True
    End Sub

    Private Sub cmbxTypeDL_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxTypeDL.SelectedIndexChanged
        customerDataStateChanged = True
    End Sub


    'Private Sub cmbxCompanyEnquiryType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxCompanyEnquiryType.SelectedIndexChanged
    '    'main, co-main, guarantor
    '    Dim rowIndex As Integer = -1
    '    Dim i As Integer
    '    For i = 0 To EnquiryWorkSheetDataSet.CustomerEnquiry.Rows.Count - 1
    '        If EnquiryWorkSheetDataSet.CustomerEnquiry.Rows(i)("CustomerId") = loadedCustomerId Then
    '            rowIndex = i
    '            Exit For
    '        End If
    '    Next
    '    'test if CustomerEnquiry exists
    '    If Not rowIndex = -1 Then
    '        Dim customerEnquiryRow As EnquiryWorkSheetDataSet.CustomerEnquiryRow
    '        customerEnquiryRow = Me.EnquiryWorkSheetDataSet.CustomerEnquiry(rowIndex)
    '        customerEnquiryRow.BeginEdit()
    '        customerEnquiryRow.Type = cmbxCompanyEnquiryType.SelectedIndex

    '        Me.Validate()
    '        customerEnquiryRow.EndEdit()
    '        Me.CustomerEnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.CustomerEnquiry)
    '        'ReLoad in Customers for Enquiry
    '        Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
    '    End If
    'End Sub
    Private Sub cmbxCustEnquiryType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbxCustEnquiryType.SelectedIndexChanged
        'main, co-main, guarantor
        Dim rowIndex As Integer = -1
        Dim i As Integer

        For i = 0 To EnquiryWorkSheetDataSet.Tables("CustomerEnquiry").Rows.Count - 1
            If EnquiryWorkSheetDataSet.Tables("CustomerEnquiry").Rows(i)("CustomerId") = loadedCustomerId Then
                rowIndex = i
                Exit For
            End If
        Next
        'test if CustomerEnquiry exists
        If rowIndex > -1 Then
            Dim customerEnquiryRow As EnquiryWorkSheetDataSet.CustomerEnquiryRow
            customerEnquiryRow = Me.EnquiryWorkSheetDataSet.CustomerEnquiry(rowIndex)
            customerEnquiryRow.BeginEdit()
            customerEnquiryRow.Type = cmbxCustEnquiryType.SelectedIndex

            Me.Validate()
            customerEnquiryRow.EndEdit()
            Me.CustomerEnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.CustomerEnquiry)
            'ReLoad in Customers for Enquiry
            Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
        End If
    End Sub

#Region "DgvDtsbType23"

    Private Sub SetDgvDtsbType2ReadOnlyOn()
        'set columns to readOnly for dgvDTSBType2
        Try
            dgvDTSBType2.Columns(0).ReadOnly = True
            dgvDTSBType2.Columns(0).Visible = False
            dgvDTSBType2.Columns(1).ReadOnly = True
            dgvDTSBType2.Columns(1).Visible = False
            dgvDTSBType2.Columns(2).ReadOnly = True
            dgvDTSBType2.Columns(2).Visible = False
            dgvDTSBType2.Columns(3).ReadOnly = True
            dgvDTSBType2.Columns(4).ReadOnly = True
            dgvDTSBType2.Columns(5).ReadOnly = True
            dgvDTSBType2.Columns(6).ReadOnly = True
            dgvDTSBType2.Columns(7).ReadOnly = True
            dgvDTSBType2.Columns(8).ReadOnly = True
            dgvDTSBType2.Columns.Item("AMLRiskDesc").ReadOnly = True
            dgvDTSBType2.Columns.Item("IsaPEP").ReadOnly = True
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.ToString, "SetDgvDtsbType2ReadOnlyOn", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvDTSBType2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDTSBType2.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)
        Dim column = senderGrid.Columns(e.ColumnIndex)
        If TypeOf column Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim dgvRow As EnquiryWorkSheetDataSet.DTSBRow
            dgvRow = EnquiryWorkSheetDataSet.DTSB(e.RowIndex)
            Dim dtsbType As Integer = dgvRow.Type
            '******************** DELETE ******************
            If column.Index = senderGrid.Columns("Delete").Index Then
                'MessageBox.Show("Delete Click")
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Delete the record for " & Trim(dgvRow.FirstName) & " " & Trim(dgvRow.LastName) & "?"
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Record Deletion"
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    DTSBTableAdapter.Delete(dgvRow.Id)
                    Try
                        Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(loadedCustomerId), New System.Nullable(Of Integer)(dtsbType))
                        custCountDtsbType23 = DTSBBindingSource.Count
                        PerformLayout()
                    Catch ex As System.Exception
                        System.Windows.Forms.MessageBox.Show(ex.Message)
                    End Try
                Else
                    ' Perform some other action.
                End If
            End If
            '******************** EDIT ******************
            If column.Index = senderGrid.Columns("Edit").Index Then
                'MessageBox.Show("Edit Click")
                Dim recordId As Integer = dgvRow.Id
                'open form
                Try
                    Dim frmAddDTSB As New FormAddDTSB
                    frmAddDTSB.PassVariable(recordId, 0, loadedCustomerId, dtsbType)
                    frmAddDTSB.ShowDialog(Me)
                    If frmAddDTSB.DialogResult = System.Windows.Forms.DialogResult.OK Then
                        MessageBox.Show("Changes saved successfully.")

                        Try
                            Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(loadedCustomerId), New System.Nullable(Of Integer)(dtsbType))
                        Catch ex As System.Exception
                            System.Windows.Forms.MessageBox.Show(ex.Message)
                        End Try

                        SetDgvDtsbType2ReadOnlyOn()

                    End If
                    frmAddDTSB.Dispose()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
            End If
        End If
    End Sub

    Private Sub btnAddNewST_Click(sender As Object, e As EventArgs) Handles btnAddNewST.Click
        If loadedCustomerId > 0 Then
            Try
                Dim frmAddDTSB As New FormAddDTSB
                frmAddDTSB.PassVariable(0, 1, loadedCustomerId, dtsbType23)
                frmAddDTSB.ShowDialog(Me)
                If frmAddDTSB.DialogResult = System.Windows.Forms.DialogResult.OK Then

                    MessageBox.Show("Changes saved successfully.")

                    Try
                        Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(loadedCustomerId), New System.Nullable(Of Integer)(dtsbType23))
                        custCountDtsbType23 = DTSBBindingSource.Count
                        PerformLayout()
                    Catch ex As System.Exception
                        System.Windows.Forms.MessageBox.Show(ex.Message)
                    End Try

                    SetDgvDtsbType2ReadOnlyOn()
                End If
                frmAddDTSB.Dispose()

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Can not add a " & MyEnums.GetDescription(DirectCast(dtsbType23, MyEnums.DtsbType)) & " until a Customer has been created!")
        End If

    End Sub

#End Region

#Region "DgvDtsbType01"

    Private Sub SetDgvDtsbType01ReadOnlyOn()
        'set columns to readOnly for dgvDTSBType01
        Try
            dgvDtsb01.Columns(0).ReadOnly = True
            dgvDtsb01.Columns(0).Visible = False
            dgvDtsb01.Columns(1).ReadOnly = True
            dgvDtsb01.Columns(1).Visible = False
            dgvDtsb01.Columns(2).ReadOnly = True
            dgvDtsb01.Columns(2).Visible = False
            dgvDtsb01.Columns(3).ReadOnly = True
            dgvDtsb01.Columns(4).ReadOnly = True
            dgvDtsb01.Columns(5).ReadOnly = True
            dgvDtsb01.Columns(6).ReadOnly = True
            dgvDtsb01.Columns(7).ReadOnly = True
            dgvDtsb01.Columns(8).ReadOnly = True
            dgvDtsb01.Columns.Item("AMLRisk").ReadOnly = True
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.ToString, "SetDgvDtsbType01ReadOnlyOn", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
       

    End Sub

    Private Sub dgvDtsb01_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDtsb01.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)
        Dim column = senderGrid.Columns(e.ColumnIndex)
        If TypeOf column Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim dgvRow As EnquiryWorkSheetDataSet.DTSB01Row
            dgvRow = EnquiryWorkSheetDataSet.DTSB01(e.RowIndex)
            Dim dtsbType As Integer = dgvRow.Type
            '******************** DELETE ******************
            If column.Index = senderGrid.Columns("Dtsb01Delete").Index Then
                'MessageBox.Show("Delete Click")
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Delete the record for " & Trim(dgvRow.FirstName) & " " & Trim(dgvRow.LastName) & "?"
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Record Deletion"
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    DTSB01TableAdapter.Delete(dgvRow.Id)
                    Try
                        Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(loadedCustomerId), New System.Nullable(Of Integer)(dtsbType))
                        custCountDtsbType01 = DTSB01BindingSource.Count
                        PerformLayout()
                    Catch ex As System.Exception
                        System.Windows.Forms.MessageBox.Show(ex.Message)
                    End Try
                Else
                    ' Perform some other action.
                End If
            End If
            '******************** EDIT ******************
            If column.Index = senderGrid.Columns("Dtsb01Edit").Index Then
                'MessageBox.Show("Edit Click")
                Dim recordId As Integer = dgvRow.Id
                'open form
                Try
                    Dim frmAddDTSB As New FormAddDTSB
                    frmAddDTSB.PassVariable(recordId, 0, loadedCustomerId, dtsbType)
                    frmAddDTSB.ShowDialog(Me)
                    If frmAddDTSB.DialogResult = System.Windows.Forms.DialogResult.OK Then
                        'MessageBox.Show("Changes saved successfully.")

                        Try
                            Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(loadedCustomerId), New System.Nullable(Of Integer)(dtsbType))
                        Catch ex As System.Exception
                            System.Windows.Forms.MessageBox.Show(ex.Message)
                        End Try

                        SetDgvDtsbType01ReadOnlyOn()

                    End If
                    frmAddDTSB.Dispose()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
            End If
        End If
    End Sub

    Private Sub btnAddNewDtsb01_Click(sender As Object, e As EventArgs) Handles btnAddNewDtsb01.Click
        If loadedCustomerId > 0 Then
            Try
                Dim frmAddDTSB As New FormAddDTSB
                frmAddDTSB.PassVariable(0, 1, loadedCustomerId, dtsbType01)
                frmAddDTSB.ShowDialog(Me)
                If frmAddDTSB.DialogResult = System.Windows.Forms.DialogResult.OK Then

                    'MessageBox.Show("Changes saved successfully.")

                    Try
                        Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(loadedCustomerId), New System.Nullable(Of Integer)(dtsbType01))
                        custCountDtsbType01 = DTSB01BindingSource.Count
                        PerformLayout()
                    Catch ex As System.Exception
                        System.Windows.Forms.MessageBox.Show(ex.Message)
                    End Try

                    SetDgvDtsbType01ReadOnlyOn()
                End If
                frmAddDTSB.Dispose()

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Can not add a " & MyEnums.GetDescription(DirectCast(dtsbType01, MyEnums.DtsbType)) & " until a Customer has been created!")
        End If
    End Sub

#End Region


#Region "Trade References"

    Private Sub btnNewRef_Click(sender As Object, e As EventArgs) Handles btnNewRef.Click
        Dim frmAddReference As New FormAddReference
        frmAddReference.PassVariable(0, 1, loadedCustomerId)
        frmAddReference.ShowDialog(Me)
        If frmAddReference.DialogResult = System.Windows.Forms.DialogResult.OK Then
            'MessageBox.Show("Changes saved successfully.")
            Try
                Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(loadedCustomerId))
                refCount = TradeReferencesBindingSource.Count
                PerformLayout()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try

        End If
        frmAddReference.Dispose()
    End Sub

    Private Sub btnTradeRefEdit_Click(sender As Object, e As EventArgs) Handles btnTradeRefEdit.Click
        Dim row As EnquiryWorkSheetDataSet.TradeReferencesRow
        row = CType(CType(Me.TradeReferencesBindingSource.Current, DataRowView).Row, EnquiryWorkSheetDataSet.TradeReferencesRow)
        'MsgBox("Selected name: " + row.BusinessName)
        'open form
        Try
            Dim frmAddRef As New FormAddReference
            frmAddRef.PassVariable(row.Id, 0)
            frmAddRef.ShowDialog(Me)
            If frmAddRef.DialogResult = System.Windows.Forms.DialogResult.OK Then
                'MessageBox.Show("Changes saved successfully.")

                Try
                    Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(loadedCustomerId))
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try

            End If
            frmAddRef.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnTradeRefDelete_Click(sender As Object, e As EventArgs) Handles btnTradeRefDelete.Click
        Dim row As EnquiryWorkSheetDataSet.TradeReferencesRow
        row = CType(CType(Me.TradeReferencesBindingSource.Current, DataRowView).Row, EnquiryWorkSheetDataSet.TradeReferencesRow)
        MsgBox("Selected name: " + row.BusinessName)
        TradeReferencesTableAdapter.Delete(row.Id)
        Try
            Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(loadedCustomerId))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub drReferences_DrawItem(sender As Object, e As PowerPacks.DataRepeaterItemEventArgs) Handles drReferences.DrawItem
        If e.DataRepeaterItem.ItemIndex Mod 2 = 0 Then
            e.DataRepeaterItem.BackColor = SystemColors.Control
        Else
            e.DataRepeaterItem.BackColor = SystemColors.ControlLight
        End If
    End Sub

#End Region


    Private Sub BtnFinishClick(sender As Object, e As EventArgs) Handles btnFinish.Click

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        Dim valResult As Util.StringOut
        'Dim checkFieldsMessage As String = ""
        'Dim checkWarningMessage As String = ""
        'Get appSettings
        'Try
        '    Dim thisAppsettings As New AppSettings
        '    thisMinLoanValueSecurityInsured = thisAppsettings.MinLoanValueSecurityInsured
        'Catch ex As Exception
        '    MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        'End Try
        '#############################
        'Check Customer not being edited
        '#############################
        Try
            'check if data has changed
            Dim rowView1 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not Me.CustomerBindingSource.Current Is Nothing Then
                rowView1 = CType(Me.CustomerBindingSource.Current, DataRowView)
                'test if any fields have changed
                'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                'If Not (rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed)) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Compare Current with Proposed
                    If Not rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed) Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                        'Get user confirmation
                        Dim msg As String
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        msg = "Do you want to Save Changed Data?"   ' Define message.
                        style = MsgBoxStyle.DefaultButton2 Or
                           MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                        title = "Save Record?"   ' Define title.
                        ' Display message.
                        response = MsgBox(msg, style, title)
                        If response = MsgBoxResult.Yes Then   ' User choose Yes.
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            SaveCustomer()
                        Else
                            ' User choose No. Do nothing
                        End If
                        creditQuestionStateChanged = False
                        customerDataStateChanged = False
                        customerTypeStateChanged = False
                        rbPepStateChanged = False
                    Else
                        'no changes to save
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "No changes to save."

                    End If
                ElseIf creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        SaveCustomer()
                    Else
                        ' User choose No. Do nothing
                    End If
                    creditQuestionStateChanged = False
                    customerDataStateChanged = False
                    customerTypeStateChanged = False
                    rbPepStateChanged = False
                Else
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try

        '#############################
        'Start validation 
        '#############################
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data..."
        Application.DoEvents()
        'EnquiryTableAdapter is already loaded by PassVariable
        'leave if bindingsource.current is nothing (no data)
        'If Not Me.EnquiryBindingSource.Current Is Nothing Then

        '    Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        '    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
        '    thisTypeOfMainCustomer = enquiryRow.mainCustomerType

        '    'Check DocsReceived
        '    Dim docsReceived As Boolean = If(IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DocsReceived")), False, EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DocsReceived"))
        '    If docsReceived = True Then
        '        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
        '            'Check Security
        '            Try
        '                Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
        '            Catch ex As Exception
        '                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '            End Try
        '            'Dim securityCount As Integer = EnquiryWorkSheetDataSet.Security.Rows.Count
        '            'MsgBox("Security Count = " & SecurityCount)
        '            If EnquiryWorkSheetDataSet.Security.Rows.Count > 0 Then
        '                'Check that certain security types have a value and insurance
        '                For Each securityRow As DataRow In EnquiryWorkSheetDataSet.Security.Rows
        '                    'SecurityValued
        '                    Select Case Trim(securityRow.Item("Type"))
        '                        Case Constants.CONST_SEC_BOAT, Constants.CONST_SEC_COMMERCIALMACHINERY, Constants.CONST_SEC_HOUSEHOLD, Constants.CONST_SEC_MISCNONSERIAL, Constants.CONST_SEC_MISCSERIAL, Constants.CONST_SEC_LIVESTOCK, Constants.CONST_SEC_MOTORVEHICLE
        '                            If securityRow.Item("SecurityValued") = False Then
        '                                checkFieldsMessage = checkFieldsMessage & "Security item must be valued" & vbCrLf
        '                            End If
        '                        Case Else

        '                    End Select
        '                    'SecurityInsured
        '                    Select Case Trim(securityRow.Item("Type"))
        '                        Case Constants.CONST_SEC_BOAT, Constants.CONST_SEC_COMMERCIALMACHINERY, Constants.CONST_SEC_LIVESTOCK, Constants.CONST_SEC_MOTORVEHICLE
        '                            If enquiryRow.LoanValue > thisMinLoanValueSecurityInsured Then
        '                                If securityRow.Item("SecurityInsured") = False Then
        '                                    'checkFieldsMessage = checkFieldsMessage & "Security item must be insured" & vbCrLf
        '                                    checkWarningMessage = checkWarningMessage & "Security item " & securityRow.Item("Description") & " is not insured" & vbCrLf
        '                                End If
        '                            End If
        '                        Case Else

        '                    End Select
        '                Next

        '            Else
        '                checkFieldsMessage = checkFieldsMessage & "A security item must be added!" & vbCrLf
        '            End If
        '        End If

        '        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
        '            'Check Budget, Financial questions
        '            Select Case thisTypeOfMainCustomer
        '                Case MyEnums.mainCustomerType.Individual
        '                    If enquiryRow.BudgetQuestion1 = 0 Or enquiryRow.BudgetQuestion2 = 0 Or enquiryRow.BudgetQuestion3 = 0 Or enquiryRow.BudgetQuestion4 = 0 Or enquiryRow.BudgetQuestion5 = 0 Or enquiryRow.BudgetQuestion6 = 0 Or enquiryRow.BudgetQuestion7 = 0 Or enquiryRow.BudgetQuestion8 = 0 Or enquiryRow.BudgetQuestion9 = 0 Or enquiryRow.BudgetQuestion10 = 0 Or enquiryRow.BudgetQuestion11 = 0 Or enquiryRow.BudgetQuestion12 = 0 Then
        '                        checkFieldsMessage = checkFieldsMessage + "Some Budget Questions still need to be answered!" & vbCrLf
        '                    End If

        '                Case MyEnums.mainCustomerType.Company, MyEnums.mainCustomerType.Trust, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
        '                    'Count how establishing income
        '                    Dim countIncome As Integer = 0
        '                    If enquiryRow.CompanyIncomePrevious = True Then
        '                        countIncome = countIncome + 1
        '                    End If
        '                    If enquiryRow.CompanyIncomeYTD = True Then
        '                        countIncome = countIncome + 1
        '                    End If
        '                    If enquiryRow.CompanyIncomeGST = True Then
        '                        countIncome = countIncome + 1
        '                    End If
        '                    If enquiryRow.CompanyIncomeForcast = True Then
        '                        countIncome = countIncome + 1
        '                    End If
        '                    If countIncome < 1 Then
        '                        checkFieldsMessage = checkFieldsMessage + "Need to have established Business Income in at least one way" & vbCrLf
        '                    End If
        '                    If enquiryRow.CompanyIncomeNotes Is DBNull.Value Or enquiryRow.CompanyExpensesNotes Is DBNull.Value Then
        '                        checkFieldsMessage = checkFieldsMessage + "Some Business Income and Expenses Questions still need to be answered!" & vbCrLf
        '                    ElseIf Not enquiryRow.CompanyIncomeNotes.Length > 0 Or Not enquiryRow.CompanyExpensesNotes.Length > 0 Then
        '                        checkFieldsMessage = checkFieldsMessage + "Some Business Income and Expenses  Questions still need to be answered!" & vbCrLf
        '                    End If

        '                    If thisTypeOfMainCustomer = MyEnums.mainCustomerType.Company Or thisTypeOfMainCustomer = MyEnums.mainCustomerType.Trust Then
        '                        'Business Salaries
        '                        If enquiryRow.CompanySalariesNotes Is DBNull.Value Then
        '                            checkFieldsMessage = checkFieldsMessage + "Business Salaries need to be commented on!" & vbCrLf
        '                        ElseIf Not enquiryRow.CompanySalariesNotes.Length > 0 Then
        '                            checkFieldsMessage = checkFieldsMessage + "Business Salaries need to be commented on!" & vbCrLf
        '                        End If
        '                        If enquiryRow.CompanySalariesSatis = False Then
        '                            checkFieldsMessage = checkFieldsMessage + "Business Salaries still need to be confirmed satisfactory!" & vbCrLf
        '                        End If
        '                        'Business Financial position
        '                        If enquiryRow.CompanyDebtNotes Is DBNull.Value Then
        '                            checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position Questions still need to be answered!" & vbCrLf
        '                        ElseIf Not enquiryRow.CompanyDebtNotes.Length > 0 Then
        '                            checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position  Questions still need to be answered!" & vbCrLf
        '                        End If
        '                        If enquiryRow.CompanyAssetRatio = False Then
        '                            checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position questions still need to be confirmed!" & vbCrLf
        '                        End If
        '                    End If

        '                    'Business Earnings
        '                    If enquiryRow.CompanyEarningsNotes Is DBNull.Value Then
        '                        checkFieldsMessage = checkFieldsMessage + "Business Earnings need to be commented on!" & vbCrLf
        '                    ElseIf Not enquiryRow.CompanyEarningsNotes.Length > 0 Then
        '                        checkFieldsMessage = checkFieldsMessage + "Business Earnings need to be commented on!" & vbCrLf
        '                    End If
        '                    If enquiryRow.CompanyProfitSatis = False Then
        '                        checkFieldsMessage = checkFieldsMessage + "Business Profit still needs to be confirmed satisfactory!" & vbCrLf
        '                    End If
        '                    'Business Financial position
        '                    If enquiryRow.CompanyAcctsPayNotes Is DBNull.Value Or enquiryRow.CompanyTaxesNotes Is DBNull.Value Then
        '                        checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position Questions still need to be answered!" & vbCrLf
        '                    ElseIf Not enquiryRow.CompanyAcctsPayNotes.Length > 0 Or Not enquiryRow.CompanyTaxesNotes.Length > 0 Then
        '                        checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position  Questions still need to be answered!" & vbCrLf
        '                    End If
        '                    If enquiryRow.CompanyAcctsPaySatis = False Then
        '                        checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position questions still need to be confirmed!" & vbCrLf
        '                    End If

        '            End Select
        '        End If

        '        'Check customer
        '        Dim custType As Integer
        '        Dim custId As Integer
        '        Dim custName As String
        '        custCount = EnquiryCustomersBindingSource.Count
        '        If custCount > 0 Then
        '            'iterate through EnquiryCustomers to load a customer
        '            For Each row As DataRow In EnquiryWorkSheetDataSet.EnquiryCustomers.Rows
        '                custId = row.Item("CustomerId")
        '                custName = row.Item("Name")
        '                'load customer
        '                Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, custId)
        '                Dim custRow As EnquiryWorkSheetDataSet.CustomerRow
        '                custRow = Me.EnquiryWorkSheetDataSet.Customer(0)
        '                custType = custRow.CustomerType
        '                Dim checkCustomerMessage As String = ""
        '                Select Case custType
        '                    Case MyEnums.CustomerType.Individual, MyEnums.CustomerType.SoleTrader
        '                        Try
        '                            'Customer
        '                            If custRow.IsAcceptanceMethodNull Then
        '                                checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
        '                            ElseIf custRow.AcceptanceMethod < 0 Then
        '                                checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
        '                            End If
        '                            'Check Identity Assessment
        '                            If custRow.IsIDCurrentNull() Or custRow.IsIDPersonNull() Or custRow.IsResStatusAcceptNull() Or custRow.IsIDLegitimateNull() Or custRow.IsAMLRiskNull() Or custRow.IsIsaPEPNull() Then
        '                                checkCustomerMessage = checkCustomerMessage & "A Identity Assessment must be added!" & vbCrLf
        '                            ElseIf Not custRow.IDPerson.Length > 0 Or Not custRow.ResStatusAccept.Length > 0 Or Not custRow.IDLegitimate.Length > 0 Or custRow.AMLRisk = 0 Then
        '                                checkCustomerMessage = checkCustomerMessage & "A Identity Assessment must be added!" & vbCrLf
        '                            ElseIf custRow.IDCurrent = False Then
        '                                checkCustomerMessage = checkCustomerMessage & "Client's ID documents must be current!" & vbCrLf
        '                            End If
        '                            'check tenancy questions
        '                            If custRow.TenancyLength Is DBNull.Value Or custRow.TenancySatisfactory Is DBNull.Value Or custRow.TenancyType Is DBNull.Value Or custRow.TenancyEnvironment Is DBNull.Value Or custRow.TenancyEstablish Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage & "Some Tenancy Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.TenancyLength.Length > 0 Or Not custRow.TenancySatisfactory.Length > 0 Or Not custRow.TenancyType.Length > 0 Or Not custRow.TenancyEnvironment.Length > 0 Or Not custRow.TenancyEstablish.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage & "Some Tenancy Questions still need to be answered!" & vbCrLf
        '                            End If
        '                            If custType = MyEnums.CustomerType.Individual Then
        '                                'if self employed or if beneficiary or if EmployerNotRung
        '                                If custRow.SelfEmployed = True Or custRow.Beneficiary = True Or custRow.EmployerNotRung = True Then
        '                                    If custRow.StabilityComments Is DBNull.Value Then
        '                                        checkCustomerMessage = checkCustomerMessage & "Need to fill in Stability Comments" & vbCrLf
        '                                    ElseIf Not custRow.StabilityComments.Length > 0 Then
        '                                        checkCustomerMessage = checkCustomerMessage & "Need to fill in Stability Comments" & vbCrLf
        '                                    End If
        '                                Else
        '                                    Try
        '                                        If IsDBNull(custRow.EmployerRangDate) Then
        '                                            checkCustomerMessage = checkCustomerMessage & "The EmployerRangDate needs to be answered!" & vbCrLf
        '                                        ElseIf Not custRow.EmployerRangDate > DateTime.MinValue Then
        '                                            checkCustomerMessage = checkCustomerMessage & "The EmployerRangDate needs to be answered!" & vbCrLf
        '                                        End If
        '                                    Catch ex As Exception
        '                                        checkCustomerMessage = checkCustomerMessage & "The EmployerRangDate needs to be answered!" & vbCrLf
        '                                    End Try
        '                                    'check stability questions 'EmployerRangDate
        '                                    If custRow.EmployerName Is DBNull.Value Or custRow.EmployerSpoketoName Is DBNull.Value Or custRow.StabilityQ1 Is DBNull.Value Or custRow.StabilityQ2 Is DBNull.Value Or custRow.StabilityQ3 Is DBNull.Value Or custRow.StabilityQ4 Is DBNull.Value Or custRow.StabilityQ5 Is DBNull.Value Or custRow.StabilityQ6 Is DBNull.Value Or custRow.StabilityQ7 Is DBNull.Value Or custRow.StabilityQ8 Is DBNull.Value Or custRow.StabilityQ9 Is DBNull.Value Then
        '                                        checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
        '                                    ElseIf Not custRow.EmployerName.Length > 0 Or Not custRow.EmployerSpoketoName.Length > 0 Or Not custRow.StabilityQ1.Length > 0 Or Not custRow.StabilityQ2.Length > 0 Or Not custRow.StabilityQ3.Length > 0 Or Not custRow.StabilityQ4.Length > 0 Or Not custRow.StabilityQ5.Length > 0 Or Not custRow.StabilityQ6.Length > 0 Or Not custRow.StabilityQ7.Length > 0 Or Not custRow.StabilityQ8.Length > 0 Or Not custRow.StabilityQ9.Length > 0 Then
        '                                        checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
        '                                    End If
        '                                End If
        '                            ElseIf custType = MyEnums.CustomerType.SoleTrader Then
        '                                'check stability questions
        '                                If custRow.StabilityQ1 Is DBNull.Value Or custRow.StabilityQ2 Is DBNull.Value Or custRow.StabilityQ3 Is DBNull.Value Or custRow.StabilityQ4 Is DBNull.Value Or custRow.StabilityQ6 Is DBNull.Value Or custRow.StabilityQ7 Is DBNull.Value Or custRow.StabilityQ9 Is DBNull.Value Then
        '                                    checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
        '                                ElseIf Not custRow.StabilityQ1.Length > 0 Or Not custRow.StabilityQ2.Length > 0 Or Not custRow.StabilityQ3.Length > 0 Or Not custRow.StabilityQ4.Length > 0 Or Not custRow.StabilityQ6.Length > 0 Or Not custRow.StabilityQ7.Length > 0 Or Not custRow.StabilityQ9.Length > 0 Then
        '                                    checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
        '                                End If
        '                            End If
        '                            'Credit
        '                            If custRow.CreditComments Is DBNull.Value Or custRow.CreditExplanation Is DBNull.Value Or custRow.CreditOther Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Credit Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.CreditComments.Length > 0 Or Not custRow.CreditExplanation.Length > 0 Or Not custRow.CreditOther.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Credit Questions still need to be answered!" & vbCrLf
        '                            End If
        '                        Catch ex As Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try


        '                    Case MyEnums.CustomerType.Company
        '                        Try
        '                            'Customer
        '                            If custRow.IsAcceptanceMethodNull Then
        '                                checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
        '                            ElseIf custRow.AcceptanceMethod < 0 Then
        '                                checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
        '                            End If
        '                            'Business information
        '                            If custRow.BusinessRegistered = False Or custRow.BusinessReturnsFiled = False Or custRow.BusinessPaidUpCapital = False Or custRow.BusinessDirectors = False Or custRow.BusinessShareholders = False Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be confirmed!" & vbCrLf
        '                            End If
        '                            If custRow.BusinessDirectorNotes Is DBNull.Value Or custRow.BusinessShareholderNotes Is DBNull.Value Or custRow.BusinessHistoryNotes Is DBNull.Value Or custRow.BusinessAddressNotes Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.BusinessDirectorNotes.Length > 0 Or Not custRow.BusinessShareholderNotes.Length > 0 Or Not custRow.BusinessHistoryNotes.Length > 0 Or Not custRow.BusinessAddressNotes.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
        '                            End If
        '                            ''Guarantors
        '                            'If custRow.BusinessApplicationFilledOut = False Then
        '                            '    checkCustomerMessage = checkCustomerMessage + "Some Business Guarantor Questions still need to be confirmed!" & vbCrLf
        '                            'End If
        '                            'If custRow.BusinessGuarantorNotes Is DBNull.Value Or custRow.BusinessAssetNotes Is DBNull.Value Then
        '                            '    checkCustomerMessage = checkCustomerMessage + "Some Business Guarantor Questions still need to be answered!" & vbCrLf
        '                            'ElseIf Not custRow.BusinessGuarantorNotes.Length > 0 Or Not custRow.BusinessAssetNotes.Length > 0 Then
        '                            '    checkCustomerMessage = checkCustomerMessage + "Some Business Guarantor Questions still need to be answered!" & vbCrLf
        '                            'End If
        '                            'Tenancy
        '                            If custRow.IsTenancyTypeNull Or custRow.TenancyLength Is DBNull.Value Or custRow.TenancySatisfactory Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.TenancyType.Length > 0 Or Not custRow.TenancyLength.Length > 0 Or Not custRow.TenancySatisfactory.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
        '                            End If
        '                            'Credit
        '                            If custRow.CreditComments Is DBNull.Value Or custRow.CreditExplanation Is DBNull.Value Or custRow.CreditOther Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.CreditComments.Length > 0 Or Not custRow.CreditExplanation.Length > 0 Or Not custRow.CreditOther.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
        '                            End If
        '                        Catch ex As Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try

        '                        'Shareholders
        '                        Try
        '                            Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.ShareHolder))
        '                        Catch ex As System.Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try
        '                        For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB.Rows
        '                            If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "A shareholder is missing a name!" & vbCrLf
        '                            ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "A shareholder is missing a name!" & vbCrLf
        '                            End If
        '                            If dtsbRow.Item("IdVerified") = False Or dtsbRow.Item("AddressVerified") = False Then
        '                                checkCustomerMessage = checkCustomerMessage + "A shareholder's Id or Address has not been verified!" & vbCrLf
        '                            End If
        '                            Dim dob As Date = dtsbRow.Item("DateOfBirth")
        '                            If dob.AddYears(18) > Now.Date Then
        '                                checkCustomerMessage = checkCustomerMessage + "A shareholder's Date of Birth is not valid!" & vbCrLf
        '                            End If
        '                        Next dtsbRow
        '                        'Directors
        '                        Try
        '                            Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Director))
        '                        Catch ex As System.Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try
        '                        For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB01.Rows
        '                            If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "A director is missing a name!" & vbCrLf
        '                            ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "A director is missing a name!" & vbCrLf
        '                            End If
        '                            If dtsbRow.Item("IdVerified") = 0 Or dtsbRow.Item("AddressVerified") = 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "A director's Id or Address has not been verified!" & vbCrLf
        '                            End If
        '                            Dim dob As Date = dtsbRow.Item("DateOfBirth")
        '                            If dob.AddYears(18) > Now.Date Then
        '                                checkCustomerMessage = checkCustomerMessage + "A director's Date of Birth is not valid!" & vbCrLf
        '                            End If
        '                        Next dtsbRow
        '                        'Trade References
        '                        Try
        '                            Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
        '                        Catch ex As Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try
        '                        For Each trRow As DataRow In EnquiryWorkSheetDataSet.TradeReferences.Rows
        '                            If trRow.Item("BusinessName") Is DBNull.Value Or trRow.Item("Account") Is DBNull.Value Or trRow.Item("Spend") Is DBNull.Value Or trRow.Item("Pay") Is DBNull.Value Or trRow.Item("Comments") Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
        '                            ElseIf Not trRow.Item("BusinessName").length > 0 Or Not trRow.Item("Account").length > 0 Or Not trRow.Item("Spend").length > 0 Or Not trRow.Item("Pay").length > 0 Or Not trRow.Item("Comments").length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
        '                            End If
        '                        Next trRow



        '                    Case MyEnums.CustomerType.Trust
        '                        Try
        '                            'Customer
        '                            If custRow.IsAcceptanceMethodNull Then
        '                                checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
        '                            ElseIf custRow.AcceptanceMethod < 0 Then
        '                                checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
        '                            End If
        '                            'Business information
        '                            If custRow.BusinessReturnsFiled = False Or custRow.BusinessPaidUpCapital = False Or custRow.BusinessDirectors = False Or custRow.BusinessShareholders = False Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be confirmed!" & vbCrLf
        '                            End If
        '                            If custRow.BusinessDirectorNotes Is DBNull.Value Or custRow.BusinessShareholderNotes Is DBNull.Value Or custRow.BusinessHistoryNotes Is DBNull.Value Or custRow.BusinessAddressNotes Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.BusinessDirectorNotes.Length > 0 Or Not custRow.BusinessShareholderNotes.Length > 0 Or Not custRow.BusinessHistoryNotes.Length > 0 Or Not custRow.BusinessAddressNotes.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
        '                            End If
        '                            ''Guarantors
        '                            'If custRow.BusinessApplicationFilledOut = False Then
        '                            '    checkCustomerMessage = checkCustomerMessage + "Some Business Guarantor Questions still need to be confirmed!" & vbCrLf
        '                            'End If
        '                            'If custRow.BusinessGuarantorNotes Is DBNull.Value Or custRow.BusinessAssetNotes Is DBNull.Value Then
        '                            '    checkCustomerMessage = checkCustomerMessage + "Some Business Guarantor Questions still need to be answered!" & vbCrLf
        '                            'ElseIf Not custRow.BusinessGuarantorNotes.Length > 0 Or Not custRow.BusinessAssetNotes.Length > 0 Then
        '                            '    checkCustomerMessage = checkCustomerMessage + "Some Business Guarantor Questions still need to be answered!" & vbCrLf
        '                            'End If
        '                            'Tenancy
        '                            If custRow.IsTenancyTypeNull Or custRow.TenancyLength Is DBNull.Value Or custRow.TenancySatisfactory Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.TenancyType.Length > 0 Or Not custRow.TenancyLength.Length > 0 Or Not custRow.TenancySatisfactory.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
        '                            End If
        '                            'Credit
        '                            If custRow.CreditComments Is DBNull.Value Or custRow.CreditExplanation Is DBNull.Value Or custRow.CreditOther Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
        '                            ElseIf Not custRow.CreditComments.Length > 0 Or Not custRow.CreditExplanation.Length > 0 Or Not custRow.CreditOther.Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
        '                            End If
        '                        Catch ex As Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try

        '                        'Beneficaries
        '                        Try
        '                            Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Beneficiary))
        '                        Catch ex As System.Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try
        '                        For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB.Rows
        '                            If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "A beneficary is missing a name!" & vbCrLf
        '                            ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "A beneficary is missing a name!" & vbCrLf
        '                            End If
        '                            If dtsbRow.Item("IdVerified") = False Or dtsbRow.Item("AddressVerified") = False Then
        '                                checkCustomerMessage = checkCustomerMessage + "A beneficary's Id or Address has not been verified!" & vbCrLf
        '                            End If
        '                            Dim dob As Date = dtsbRow.Item("DateOfBirth")
        '                            If dob.AddYears(18) > Now.Date Then
        '                                checkCustomerMessage = checkCustomerMessage + "A beneficary's Date of Birth is not valid!" & vbCrLf
        '                            End If
        '                        Next dtsbRow
        '                        'Trustees
        '                        Try
        '                            Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Trustee))
        '                        Catch ex As System.Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try
        '                        For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB01.Rows
        '                            If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "A trustee is missing a name!" & vbCrLf
        '                            ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "A trustee is missing a name!" & vbCrLf
        '                            End If
        '                            If dtsbRow.Item("IdVerified") = False Or dtsbRow.Item("AddressVerified") = False Then
        '                                checkCustomerMessage = checkCustomerMessage + "A trustee's Id or Address has not been verified!" & vbCrLf
        '                            End If
        '                            Dim dob As Date = dtsbRow.Item("DateOfBirth")
        '                            If dob.AddYears(18) > Now.Date Then
        '                                checkCustomerMessage = checkCustomerMessage + "A trustee's Date of Birth is not valid!" & vbCrLf
        '                            End If
        '                        Next dtsbRow
        '                        'Trade References
        '                        Try
        '                            Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
        '                        Catch ex As Exception
        '                            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '                        End Try
        '                        For Each trRow As DataRow In EnquiryWorkSheetDataSet.TradeReferences.Rows
        '                            If trRow.Item("BusinessName") Is DBNull.Value Or trRow.Item("Account") Is DBNull.Value Or trRow.Item("Spend") Is DBNull.Value Or trRow.Item("Pay") Is DBNull.Value Or trRow.Item("Comments") Is DBNull.Value Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
        '                            ElseIf Not trRow.Item("BusinessName").length > 0 Or Not trRow.Item("Account").length > 0 Or Not trRow.Item("Spend").length > 0 Or Not trRow.Item("Pay").length > 0 Or Not trRow.Item("Comments").length > 0 Then
        '                                checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
        '                            End If
        '                        Next trRow

        '                    Case Else
        '                        checkCustomerMessage = checkCustomerMessage + "Could not validate customers!" & vbCrLf

        '                End Select

        '                If Not checkCustomerMessage = "" Then
        '                    checkFieldsMessage = checkFieldsMessage + "--Error validating " & custName & vbNewLine & checkCustomerMessage
        '                End If

        '            Next row
        '        Else
        '            checkFieldsMessage = checkFieldsMessage + "A customer needs to be created!" & vbCrLf
        '        End If



        '        '#############################
        '        'Launch DDApprovalForm
        '        '#############################
        '        'Display warnings
        '        If Not checkWarningMessage = "" Then
        '            Dim result As MsgBoxResult
        '            checkWarningMessage = checkWarningMessage & vbCrLf & "Do you wish to continue?" & vbCrLf
        '            result = MsgBox(checkWarningMessage, MsgBoxStyle.OkCancel, "Warning message")
        '            If result = MsgBoxResult.Cancel Then
        '                Exit Sub
        '            End If

        '        End If


        '        'Display Messages
        '        If Not checkFieldsMessage = "" Then
        '            MsgBox(checkFieldsMessage)
        '            'update Status Strip
        '            FormRefresh()
        '            ToolStripStatusLabel1.ForeColor = Color.Black
        '            ToolStripStatusLabel1.Text = "Status: Ready."
        '        Else
        '            'Get user approval
        '            Try
        '                Dim dDApprovalFrm As New DDApprovalForm
        '                dDApprovalFrm.PassVariable(thisEnquiryId, thisContactDisplayName)
        '                dDApprovalFrm.ShowDialog(Me)
        '                If dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
        '                    dDApprovalFrm.Dispose()
        '                    'update Status Strip
        '                    ToolStripStatusLabel1.ForeColor = Color.Black
        '                    ToolStripStatusLabel1.Text = "Changes saved successfully."
        '                    progressStatus = True
        '                ElseIf dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.No Then
        '                    dDApprovalFrm.Dispose()
        '                    'update Status Strip
        '                    ToolStripStatusLabel1.ForeColor = Color.DarkGreen
        '                    ToolStripStatusLabel1.Text = "Application Declined."
        '                    progressStatus = True
        '                ElseIf dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
        '                    dDApprovalFrm.Dispose()
        '                    'User choose Cancel, do nothing
        '                    progressStatus = False
        '                    'update Status Strip
        '                    ToolStripStatusLabel1.ForeColor = Color.Black
        '                    ToolStripStatusLabel1.Text = "Approving Due Diligence cancelled. Status: Ready."
        '                End If

        '            Catch ex As Exception
        '                MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        '            End Try
        '        End If ' end of If Not checkFieldsMessage = ""

        '    Else 'Documents not received
        '        MessageBox.Show("Documents have not been received.", "Loan documents", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '        progressStatus = False
        '    End If
        'Else
        '    'no current record in binding source - do not proceed
        '    'update Status Strip
        '    ToolStripStatusLabel1.ForeColor = Color.Red
        '    ToolStripStatusLabel1.Text = "No data entered, please enter data!"
        '    progressStatus = False
        'End If


        valResult = Util.ValidateDueDiligence(thisEnquiryId)
        If valResult.Result = False Then
            If valResult.StringOut.Length > 0 Then
                MessageBox.Show(valResult.StringOut, "Validation of Due-diligence", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            'Get user approval
            Try
                Dim dDApprovalFrm As New DDApprovalForm
                dDApprovalFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryType)
                dDApprovalFrm.ShowDialog(Me)
                If dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                    dDApprovalFrm.Dispose()
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Changes saved successfully."
                    progressStatus = True
                ElseIf dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.No Then
                    dDApprovalFrm.Dispose()
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.DarkGreen
                    ToolStripStatusLabel1.Text = "Application Declined."
                    progressStatus = True
                ElseIf dDApprovalFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                    dDApprovalFrm.Dispose()
                    'User choose Cancel, do nothing
                    progressStatus = False
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Approving Due Diligence cancelled. Status: Ready."
                End If
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
            End Try

            'launch  Standard Worksheet Form
            If progressStatus = True Then
                Try
                    Dim appFrm As New AppForm
                    appFrm.PassVariable(thisEnquiryId)
                    appFrm.Show()
                    Cursor.Current = Cursors.Default
                    'Close this Form
                    Me.Close()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try
            End If
        End If

        Cursor.Current = Cursors.Default

    End Sub



#Region "PEP"

    Private Sub pbPEP_Click(sender As Object, e As EventArgs) Handles pbPEP.Click, pbPEP2.Click
        'Dim msgText As String = "politically exposed person means—" & vbNewLine & "(a) an individual who holds, or has held at any time in the preceding 12 months, in any overseas country the prominent public function of—" & vbNewLine & vbTab & "(i) Head of State or head of a country or government; or" & vbNewLine & vbTab & "(ii) government minister or equivalent senior politician; or" & vbNewLine & vbTab & "(iii) Supreme Court Judge or equivalent senior Judge; or" & vbNewLine & vbTab & "(iv) governor of a central bank or any other position that has comparable influence to the Governor of the Reserve Bank of New Zealand; or" & vbNewLine & vbTab & "(v) senior foreign representative, ambassador, or high commissioner; or" & vbNewLine & vbTab & "(vi) high-ranking member of the armed forces; or" & vbNewLine & vbTab & "(vii) board chair, chief executive, or chief financial officer of, or any other position that has comparable influence in, any State enterprise; and" & vbNewLine & "(b) an immediate family member of a person referred to in paragraph (a), including—" & vbNewLine & vbTab & "(i) a spouse; or" & vbNewLine & vbTab & "(ii) a partner, being a person who is considered by the relevant national law as equivalent to a spouse; or" & vbNewLine & vbTab & "(iii) a child and a child’s spouse or partner; or " & vbNewLine & vbTab & "(iv) a parent; and" & vbNewLine & "(c) having regard to information that is public or readily available,—" & vbNewLine & vbTab & "(i) any individual who is known to have joint beneficial ownership of a legal entity or legal arrangement, or any other close relationship, with a person referred to in paragraph (a); or" & vbNewLine & vbTab & "(ii) any individual who has sole beneficial ownership of a legal entity or legal arrangement that is known to exist for the benefit of a person described in paragraph (a)" & vbNewLine

        'MessageBox.Show(msgText, "What is a politically exposed person", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\PoliticallyExposedPerson.rtf", "What is a politically exposed person?", MyEnums.MsgBoxMode.FilePath, 700)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()

    End Sub

    Private Sub pbPEP_MouseHover(sender As Object, e As EventArgs) Handles pbPEP.MouseHover
        pbPEP.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbPEP_MouseLeave(sender As Object, e As EventArgs) Handles pbPEP.MouseLeave
        pbPEP.Image = My.Resources.questionBsm
    End Sub

    Private Sub pbPEP2_MouseHover(sender As Object, e As EventArgs) Handles pbPEP2.MouseHover
        pbPEP2.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbPEP2_MouseLeave(sender As Object, e As EventArgs) Handles pbPEP2.MouseLeave
        pbPEP2.Image = My.Resources.questionBsm
    End Sub

    Private Sub rbPepYes_CheckedChanged(sender As Object, e As EventArgs) Handles rbPepYes.CheckedChanged
        'set IsaPep state
        rbPepState = 2
        rbPepStateChanged = True
    End Sub

    Private Sub rbPepNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbPepNo.CheckedChanged
        rbPepState = 1
        rbPepStateChanged = True
    End Sub

    Private Sub SetRbPep()
        Dim pepState As Boolean
        Dim state As Integer
        If IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IsaPEP")) = True Then
            state = 0
        Else
            pepState = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IsaPEP")
            If pepState = True Then
                state = 2
            Else
                state = 1
            End If

        End If

        Select Case state
            Case 0 'IsDBNull
                rbPepState = 0
                rbPepNo.Checked = False
                rbPepYes.Checked = False
            Case 1 'False
                rbPepState = 1
                rbPepNo.Checked = True
            Case 2 'true
                rbPepState = 2
                rbPepYes.Checked = True
        End Select
    End Sub

#End Region



    ''' <summary>
    ''' Import Customer details from the OAC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Async Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        'MsgBox("Sorry." & vbCrLf & "This button does not function at the moment.")
        'Change button text to 'Cancel Import'
        'Start busy animation
        'ImportCustomersProcess: Return Seccuss
        'If return true, import successful
        'Else alert

        Dim btnText As String = "Import customers"
        Dim retCheckApplicationResult As New TrackResult
        Dim retCustomerResult As New OacCustomersResponse 'Result returned from OAC Web Service to get Customer details
        Dim tTCustomerResult As TtCustomersResponse 'Result from TrueTrack database
        Dim ct As CancellationToken
        'This TaskScheduler captures SynchronizationContext.Current.
        Dim taskScheduler As TaskScheduler = taskScheduler.FromCurrentSynchronizationContext()

        Try
            If btnImport.Text.IndexOf("Cancel") > -1 Then
                cancellationTokenSource.Cancel()
                btnImport.Text = btnText
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Ready state"
                btnImport.Enabled = True
                btnImport.Visible = True
                Exit Sub
            Else
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Getting Customers details from OAC"
                'Instantiate cancellation token
                cancellationTokenSource = New CancellationTokenSource()
                ct = cancellationTokenSource.Token
                btnImport.Text = "Cancel"
            End If

            If Not ct.IsCancellationRequested Then
                Dim onlineApplication As OnlineApplication
                retCustomerResult.Exists = False
                If CheckEnquiryHasApplicationCode() Then
                    onlineApplication = New OnlineApplication
                    If Not ct.IsCancellationRequested Then
                        asyncAnimation.StartAnimation(btnImport.Left, AnimatedCirclePanelAlign.Left)
                        Application.DoEvents()
                        If WebUtil.WebServiceAvailable Then
                            asyncAnimation.EndAnimation()
                            If Not ct.IsCancellationRequested Then
                                asyncAnimation.StartAnimation(btnImport.Left, AnimatedCirclePanelAlign.Left)
                                Await task.Run(
                                    Sub()
                                        'Check application code exists in OAC
                                        retCheckApplicationResult = onlineApplication.CheckApplicationCode(thisApplicationCode)
                                    End Sub
                                              )
                                If retCheckApplicationResult.Status = False Then
                                    asyncAnimation.EndAnimation()
                                    ToolStripStatusLabel1.ForeColor = Color.Red
                                    ToolStripStatusLabel1.Text = retCheckApplicationResult.StatusMessage
                                    MessageBox.Show(retCheckApplicationResult.ErrorMessage, "OAC service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    btnImport.Visible = True
                                    btnImport.Enabled = True
                                    btnImport.Text = btnText
                                    Exit Sub
                                Else
                                    If Not ct.IsCancellationRequested Then
                                        asyncAnimation.StartAnimation(btnImport.Left, AnimatedCirclePanelAlign.Left)
                                        Await task.Run(
                                            Sub()
                                                'Get list of Customer details from OAC and filter out Partnerships
                                                retCustomerResult = onlineApplication.GetCustomerList(thisApplicationCode, True)
                                            End Sub
                                                      )
                                        asyncAnimation.EndAnimation()
                                        If retCustomerResult.Exists = False Then
                                            MessageBox.Show(retCustomerResult.ErrorMessage, retCustomerResult.StatusMessage)
                                        Else
                                            'success: do stuff with customers; check if have matching customer already, 
                                            'List of import customers
                                            Dim oacCustomersList As List(Of OacCustomer)
                                            oacCustomersList = retCustomerResult.CustomerList
                                            'Get List of TT Customers
                                            tTCustomerResult = onlineApplication.TTCustomerInfo(thisEnquiryId)
                                            If tTCustomerResult.Exists Then
                                                If tTCustomerResult.CustomerList.Count > 0 Then
                                                    'Display a form for matching OAC customers with trueTrack customers
                                                    'pass list of OAC customers and List of TT customers
                                                    'TT customers with XRefId display already matched
                                                    'TT customers info will be over written with OAC customer Info
                                                    'Load form to match OAC customers with TT customers
                                                    Dim frmImportCustomers As New FrmImportCustomers(thisEnquiryId, thisEnquiryCode, oacCustomersList, tTCustomerResult.CustomerList)
                                                    Dim diaResult As DialogResult = frmImportCustomers.ShowDialog(Me)
                                                    If diaResult = System.Windows.Forms.DialogResult.OK Then
                                                        MsgBox("Import of Customers completed successfully", MsgBoxStyle.Information, "Customer Import")
                                                    Else

                                                    End If

                                                Else
                                                    'no TT customers, import OAC customers directly, no user interaction needed
                                                    If onlineApplication.InsertOacCustomers(thisEnquiryId, oacCustomersList) Then
                                                        Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
                                                        ToolStripStatusLabel1.ForeColor = Color.Blue
                                                        ToolStripStatusLabel1.Text = "OAC Customers import into TrueTrack completed"
                                                    Else
                                                        ToolStripStatusLabel1.ForeColor = Color.Red
                                                        ToolStripStatusLabel1.Text = "Error importing OAC Customers"
                                                    End If
                                                End If
                                            Else
                                                'no result
                                                MsgBox(tTCustomerResult.ErrorMessage, tTCustomerResult.StatusMessage)
                                            End If 'tTCustomerResult.Exists
                                        End If 'retCustomerResult.Exists
                                    End If 'Not ct.IsCancellationRequested
                                End If 'retCheckApplicationResult
                            End If 'Not ct.IsCancellationRequested   
                        Else
                            asyncAnimation.EndAnimation()
                            MessageBox.Show("Web Service Server not available" & vbCrLf & "Try again later, if error persists contact your administrator", "Web Service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Web Service connection failed"
                            Exit Sub
                        End If 'WebServiceAvailable                       
                    End If 'Not ct.IsCancellationRequested
                End If 'CheckEnquiryHasApplicationCode()
            End If 'Not ct.IsCancellationRequested

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try

        btnImport.Visible = True
        btnImport.Enabled = True
        btnImport.Text = btnText

        FormRefresh()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            'check if data has changed
            Dim rowView1 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not Me.CustomerBindingSource.Current Is Nothing Then
                rowView1 = CType(Me.CustomerBindingSource.Current, DataRowView)
                'test if any fields have changed
                'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                'If Not (rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed)) Or creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Compare Current with Proposed
                    If Not rowView1.Row(0, DataRowVersion.Current) Is rowView1.Row(0, DataRowVersion.Proposed) Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                        'Get user confirmation
                        Dim msg As String
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        msg = "Do you want to Save Changed Data?"   ' Define message.
                        style = MsgBoxStyle.DefaultButton2 Or
                           MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                        title = "Save Record?"   ' Define title.
                        ' Display message.
                        response = MsgBox(msg, style, title)
                        If response = MsgBoxResult.Yes Then   ' User choose Yes.
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            SaveCustomer()
                        Else
                            ' User choose No. Do nothing
                        End If
                        creditQuestionStateChanged = False
                        customerDataStateChanged = False
                        customerTypeStateChanged = False
                        rbPepStateChanged = False
                    Else
                        'no changes to save
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "No changes to save."

                    End If
                ElseIf creditQuestionStateChanged = True Or customerDataStateChanged = True Or customerTypeStateChanged = True Or rbPepStateChanged = True Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        SaveCustomer()
                    Else
                        ' User choose No. Do nothing
                    End If
                    creditQuestionStateChanged = False
                    customerDataStateChanged = False
                    customerTypeStateChanged = False
                    rbPepStateChanged = False
                Else
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try


        'Clear Customer table
        EnquiryWorkSheetDataSet.Tables("Customer").Clear()

        Me.Close()

    End Sub


    ''' <summary>
    ''' Check application code exits  for the enquiry
    ''' if code does not exists then set the form toolbarStatus message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckEnquiryHasApplicationCode() As Boolean
        Dim retVal As Boolean = False
        If String.IsNullOrWhiteSpace(thisApplicationCode) Then
            MessageBox.Show("Please enter a valid Application code", "Application code", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "Application code not valid"
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

#Region "pbImportCustomer"

    Private Sub pbImportCustomer_Click(sender As Object, e As EventArgs) Handles pbImportCustomer.Click
        'Dim msgText As String = "When you import customers the system checks for previously imported customers and will only import customers that have not previously been imported." & vbNewLine & vbNewLine & "If the system finds a customer in TrueTrack that was not previously imported it will ask you to match the TrueTrack customer to an OAC customer." & vbNewLine & vbNewLine & "The only way to re-import a customer into TrueTrack is by deleted the existing customer in TrueTrack and then import customers." & vbNewLine

        'MessageBox.Show(msgText, "Importing customers", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\ImportCustomer.rtf", "Import Customer", MyEnums.MsgBoxMode.FilePath, 700)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()
    End Sub

    Private Sub pbImportCustomer_MouseHover(sender As Object, e As EventArgs) Handles pbImportCustomer.MouseHover
        pbImportCustomer.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbImportCustomer_MouseLeave(sender As Object, e As EventArgs) Handles pbImportCustomer.MouseLeave
        pbImportCustomer.Image = My.Resources.questionBsm
    End Sub

#End Region

#Region "Methods of acceptance Information"
    Private Sub pbHA_Click(sender As Object, e As EventArgs) Handles pbHA.Click
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\MethodsOfAcceptance.rtf", "Methods of acceptance", MyEnums.MsgBoxMode.FilePath, 700)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()

    End Sub

    Private Sub pbHA_MouseHover(sender As Object, e As EventArgs) Handles pbHA.MouseHover
        pbHA.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbHA_MouseLeave(sender As Object, e As EventArgs) Handles pbHA.MouseLeave
        pbHA.Image = My.Resources.questionBsm
    End Sub


#End Region

    ''' <summary>
    ''' Set the visibility of the Customer buttons: Save, Cancel, Delete
    ''' </summary>
    ''' <param name="vis"></param>
    ''' <remarks></remarks>
    Private Sub SetCustomerButtons(ByVal vis As Boolean)
        If vis = True Then
            btnSaveCustomer.Visible = True
            btnCancelCustomer.Visible = True
            btnDeleteCustomer.Visible = True
        Else
            btnSaveCustomer.Visible = False
            btnCancelCustomer.Visible = False
            btnDeleteCustomer.Visible = False
        End If
    End Sub

#Region "Handle MouseWheel"
    ''' <summary>
    ''' Capture mouse wheel events to ride them up to the host panel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ControlsInPanel_MouseWheel(sender As Object, e As MouseEventArgs) Handles cmbxRiskAssess.MouseWheel, cmbxTenancyType.MouseWheel, cmbxTenancy.MouseWheel, drReferences.MouseWheel, cmbxTypeOfId.MouseWheel, cmbxTypeDL.MouseWheel
        Dim parentPanel As Control
        parentPanel = Util.GetParentPanel(sender)
        parentPanel.Focus()
    End Sub

    ''' <summary>
    ''' Capture mouse wheel events to send them to panel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub CustTypePanel_MouseWheel(sender As Object, e As MouseEventArgs) Handles cmbxCustomerType.MouseWheel, cmbxCustEnquiryType.MouseWheel, cmbxAcceptanceMethod.MouseWheel

        Select Case cmbxCustomerType.SelectedIndex
            Case MyEnums.CustomerType.Company, MyEnums.CustomerType.Trust
                pnlCompany.Focus()
            Case MyEnums.CustomerType.Individual, MyEnums.CustomerType.SoleTrader
                pnlIndividual.Focus()
            Case Else
                sender.Focus()
        End Select


    End Sub

#End Region

#Region "Debugging"

    'Private Sub DataGridView_RowStateChanged(sender As Object, e As DataGridViewRowStateChangedEventArgs) Handles dgvEnquiryCustomers.RowStateChanged

    '    Dim messageBoxVB As New System.Text.StringBuilder()
    '    messageBoxVB.AppendFormat("{0} = {1}", "Row", e.Row)
    '    messageBoxVB.AppendLine()
    '    messageBoxVB.AppendFormat("{0} = {1}", "StateChanged", e.StateChanged)
    '    messageBoxVB.AppendLine()
    '    MessageBox.Show(messageBoxVB.ToString(), "RowStateChanged Event")

    'End Sub


#End Region




    Private Async Sub DocumentsReceivedToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocumentsReceivedToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        If Not EnquiryBindingSource.Current Is Nothing Then
            Dim commentId As Integer
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            If docsReceived = False Then
                enquiryRow.BeginEdit()
                enquiryRow.DocsReceived = True
                docsReceived = True
            Else
                Dim result As DialogResult = MessageBox.Show("This will unlock the OAC and set it to Status of ‘Waiting for Docs’." & vbCrLf & "You will need to receive the documents again before TrueTrack can pass CAD approval.", "Reset Documents Received", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                If result = DialogResult.Cancel Then
                    Exit Sub
                ElseIf result = DialogResult.OK Then
                    Cursor.Current = Cursors.WaitCursor
                    Application.DoEvents()
                    enquiryRow.BeginEdit()
                    enquiryRow.DocsReceived = False
                    docsReceived = False
                End If

            End If
            'save changes
            Try
                Me.Validate()
                enquiryRow.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                If docsReceived = True Then
                    DocumentsReceivedToolStripMenuItem.Text = "Documents received"
                    DocsReceivedImg.Visible = False
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Documents received")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Documents received saved successfully."
                Else
                    DocumentsReceivedToolStripMenuItem.Text = "Documents received"
                    DocsReceivedImg.Visible = True
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Documents received reset.")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Documents received reset."
                End If
                Application.DoEvents()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Update Status Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try 'Check is possible to Change Remote Status
            Dim systemCanChangeRemoteStatus As Boolean
            Dim thisAppsettings As New AppSettings
            Dim retTTResult As New TrackResult
            Dim onlineApplicationService As New OnlineApplication
            Dim actionType As Integer
            Try
                systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus

                If systemCanChangeRemoteStatus = True Then
                    If docsReceived = True Then
                        'Change Status in OAC
                        actionType = OACWebService.ApplicationActionType.DocumentsReceived
                        retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, String.Empty)
                        If retTTResult.Status = False Then 'error condition
                            commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                            MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Else
                        'Change Status to WaitingForDocs in OAC
                        Try
                            retTTResult = Await onlineApplicationService.SetStatusWaitingForDocs(thisApplicationCode)
                            If retTTResult.ErrorCode > 0 Then
                                MessageBox.Show(retTTResult.ErrorMessage, "Set Status WaitingForDocs", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                            Else
                                'action was successful, no need to do anything else
                            End If
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "Set Status WaitingForDocs", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Try
                        If retTTResult.Status = False Then 'error condition
                            commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                            MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Change Remote Status Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Cursor.Current = Cursors.WaitCursor
            Application.DoEvents()

            FormRefresh()
        End If
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub DocsReceivedImg_Click(sender As Object, e As EventArgs) Handles DocsReceivedImg.Click
        DocumentsReceivedToolStripMenuItem_Click(sender, e)
    End Sub

    Private Sub dgvDtsb01_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvDtsb01.DataError
        MessageBox.Show("Error happened " & e.Context.ToString() & vbCrLf & e.Exception.ToString())

        If (e.Context = DataGridViewDataErrorContexts.Commit) _
            Then
            MessageBox.Show("Commit error")
        End If
        If (e.Context = DataGridViewDataErrorContexts _
            .CurrentCellChange) Then
            MessageBox.Show("Cell change")
        End If
        If (e.Context = DataGridViewDataErrorContexts.Parsing) _
            Then
            MessageBox.Show("parsing error")
        End If
        If (e.Context = _
            DataGridViewDataErrorContexts.LeaveControl) Then
            MessageBox.Show("leave control error")
        End If

        If (TypeOf (e.Exception) Is ConstraintException) Then
            Dim view As DataGridView = CType(sender, DataGridView)
            view.Rows(e.RowIndex).ErrorText = "an error"
            view.Rows(e.RowIndex).Cells(e.ColumnIndex) _
                .ErrorText = "an error"

            e.ThrowException = False
        End If
    End Sub

   
End Class