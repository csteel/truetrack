﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportEnquiryForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource3 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReportEnquiryForm))
        Me.CurrentAndArchiveEnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.CurrentEnquiryReportBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CurrentEnquiryReportTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CurrentEnquiryReportTableAdapter()
        Me.CurrentAndArchiveEnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CurrentAndArchiveEnquiryTableAdapter()
        Me.CurrentEnquiryReportBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReportViewer2 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        CType(Me.CurrentAndArchiveEnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CurrentEnquiryReportBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CurrentEnquiryReportBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CurrentAndArchiveEnquiryBindingSource
        '
        Me.CurrentAndArchiveEnquiryBindingSource.DataMember = "CurrentAndArchiveEnquiry"
        Me.CurrentAndArchiveEnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CurrentEnquiryReportBindingSource
        '
        Me.CurrentEnquiryReportBindingSource.DataMember = "CurrentEnquiryReport"
        Me.CurrentEnquiryReportBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'CurrentEnquiryReportTableAdapter
        '
        Me.CurrentEnquiryReportTableAdapter.ClearBeforeFill = True
        '
        'CurrentAndArchiveEnquiryTableAdapter
        '
        Me.CurrentAndArchiveEnquiryTableAdapter.ClearBeforeFill = True
        '
        'CurrentEnquiryReportBindingSource1
        '
        Me.CurrentEnquiryReportBindingSource1.DataMember = "CurrentEnquiryReport"
        Me.CurrentEnquiryReportBindingSource1.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'ReportViewer2
        '
        Me.ReportViewer2.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "EnquiryWorkSheetDataSet_CurrentEnquiryReport"
        ReportDataSource1.Value = Me.CurrentEnquiryReportBindingSource
        Me.ReportViewer2.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer2.LocalReport.ReportEmbeddedResource = "AppWhShtB.CurrentEnquiryReport.rdlc"
        Me.ReportViewer2.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer2.Name = "ReportViewer2"
        Me.ReportViewer2.Size = New System.Drawing.Size(884, 1012)
        Me.ReportViewer2.TabIndex = 1
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource2.Name = "EnquiryWorkSheetDataSet_EnquiryReport"
        ReportDataSource2.Value = Me.CurrentAndArchiveEnquiryBindingSource
        ReportDataSource3.Name = "EnquiryWorkSheetDataSet_CurrentAndArchiveEnquiry"
        ReportDataSource3.Value = Me.CurrentAndArchiveEnquiryBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource3)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "AppWhShtB.EnquiryReport.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(884, 1012)
        Me.ReportViewer1.TabIndex = 0
        '
        'ReportEnquiryForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.ReportEnquiryFormSize
        Me.Controls.Add(Me.ReportViewer2)
        Me.Controls.Add(Me.ReportViewer1)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "ReportEnquiryFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "ReportEnquiryFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.ReportEnquiryFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "ReportEnquiryForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Enquiry Report"
        CType(Me.CurrentAndArchiveEnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CurrentEnquiryReportBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CurrentEnquiryReportBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents CurrentEnquiryReportBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CurrentEnquiryReportTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CurrentEnquiryReportTableAdapter
    Friend WithEvents CurrentAndArchiveEnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CurrentAndArchiveEnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CurrentAndArchiveEnquiryTableAdapter
    Friend WithEvents CurrentEnquiryReportBindingSource1 As System.Windows.Forms.BindingSource
    Private WithEvents ReportViewer2 As Microsoft.Reporting.WinForms.ReportViewer
    Private WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
End Class
