﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMatchCustomers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMatchCustomers))
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.dgvMatchCustomers = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.ttCustomerId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TTCustomer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OACCustomers = New System.Windows.Forms.DataGridViewComboBoxColumn()
        CType(Me.dgvMatchCustomers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(129, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(263, 13)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "Please match currently unmatched customers"
        '
        'dgvMatchCustomers
        '
        Me.dgvMatchCustomers.AllowUserToAddRows = False
        Me.dgvMatchCustomers.AllowUserToDeleteRows = False
        Me.dgvMatchCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMatchCustomers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ttCustomerId, Me.TTCustomer, Me.OACCustomers})
        Me.dgvMatchCustomers.Location = New System.Drawing.Point(18, 34)
        Me.dgvMatchCustomers.Name = "dgvMatchCustomers"
        Me.dgvMatchCustomers.Size = New System.Drawing.Size(543, 150)
        Me.dgvMatchCustomers.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "CustomerId"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "TrueTrack Customer"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.BackColor = System.Drawing.Color.LightGreen
        Me.btnNext.Location = New System.Drawing.Point(445, 201)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(116, 23)
        Me.btnNext.TabIndex = 5
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = False
        '
        'ttCustomerId
        '
        Me.ttCustomerId.HeaderText = "CustomerId"
        Me.ttCustomerId.Name = "ttCustomerId"
        Me.ttCustomerId.Visible = False
        '
        'TTCustomer
        '
        Me.TTCustomer.HeaderText = "Unmatched TrueTrack Customer"
        Me.TTCustomer.Name = "TTCustomer"
        Me.TTCustomer.Width = 200
        '
        'OACCustomers
        '
        Me.OACCustomers.HeaderText = "Select an OAC Customer"
        Me.OACCustomers.Name = "OACCustomers"
        Me.OACCustomers.Width = 300
        '
        'FrmMatchCustomers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(579, 236)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.dgvMatchCustomers)
        Me.Controls.Add(Me.lblTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmMatchCustomers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmMatchCustomers"
        CType(Me.dgvMatchCustomers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents dgvMatchCustomers As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents ttCustomerId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TTCustomer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OACCustomers As System.Windows.Forms.DataGridViewComboBoxColumn
End Class
