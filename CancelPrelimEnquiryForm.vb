﻿Public Class CancelPrelimEnquiryForm
    Dim otherEnquiryId As Integer
    Dim ThisEnquiryResult As String
    Dim OtherEnquiryCode As String



    Private Sub CancelPrelimEnquiryForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'loads data into the 'EnquiryWorkSheetDataSet.ActiveEnquiryClients' table.
        Me.ActiveEnquiryClientsTableAdapter1.Fill(Me.EnquiryWorkSheetDataSet.ActiveEnquiryClients)
    End Sub


    Private Sub btnCancelEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelEnquiry.Click
        'check data entered into txtboxes
        Dim checkFieldsMessage As String = ""
        If txtbxOtherEnquiryCode.Text.Length > 0 Then
            OtherEnquiryCode = txtbxOtherEnquiryCode.Text
            'check Enquiry Code is valid
            Dim columnString As String = "EnquiryCode = '" & Trim(txtbxOtherEnquiryCode.Text) & "'"
            Dim numberOfOccurences As Integer = CInt(EnquiryWorkSheetDataSet.ActiveEnquiryClients.Compute("Count(ContactLastName)", columnString))
            If Not numberOfOccurences > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please enter a valid enquiry code" + vbCrLf
            End If
        Else
            checkFieldsMessage = checkFieldsMessage + "Please enter an enquiry code" + vbCrLf
        End If

        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            DialogResult = System.Windows.Forms.DialogResult.None
        Else
            Try
                'load data into Enquiry table
                EnquiryTableAdapter1.FillByEnquiryCode(EnquiryWorkSheetDataSet.Enquiry, OtherEnquiryCode)
                'update fields
                Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                otherEnquiryId = enquiryRow("EnquiryId")
                'create log note
                Dim commentStr As String
                commentStr = "Preliminary enquiry information was entered and then cancelled by " & LoggedinName
                Dim commentId As Integer
                commentId = Util.SetComment(otherEnquiryId, "System", commentStr)
               
            Catch ex As Exception
                MsgBox("Cancel Enquiry caused an error: " & vbCrLf & ex.Message)
            End Try

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        End If

    End Sub

    Private Sub btnGoBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoBack.Click
        Me.EnquiryBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    
End Class