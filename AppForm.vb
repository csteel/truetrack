﻿Imports System.Data.SqlClient
Imports System.IO 'for file system operations
Imports System.Drawing.Printing 'for printing operations
Imports System.Globalization
Imports System.Collections.ObjectModel
Imports System.Text
Imports System.Security
Imports System.Threading
Imports System.Threading.Tasks
Imports AnimatedCircle

Public Class AppForm
    'Dim gpbxResizeQRGBoolean As Boolean = 0 ' set boolean count for toggle of resize of gpbxQRG
    Dim thisEnquiryId As Integer ' set New Enquiry Id 
    Dim thisEnquiryCode As String ' set Enquiry Code
    Dim thisApplicationCode As String
    Dim QRGStatus As Integer 'set QRG Status for colour
    Dim thisIsArchived As Boolean 'non-destructive archive
    Dim thisEnquiryManagerId As Integer ' set ThisEnquiryManagerId
    Dim thisEnquiryType As Integer
    Dim OriginalTime As DateTime 'Set New Enquiry Time
    Dim Status30 As String 'set Status30mins
    
    Dim dtsbType23 As Integer
    Dim dtsbType01 As Integer
    Dim ThisContactDisplayName As String
    Dim ContactName As String
    Dim ContactLastName As String
    Dim ContactAddress As String
    Dim thisTypeOfMainCustomer As Integer ' set Type of Main Client

    Dim ThisDealerId As String 'dealer name id
    Dim ThisDealerName As String 'dealer name
    Dim thisCurrentStatus As String
    Dim loadedCustomerId As Integer = 0
    Dim thisTypeOfCustomer As Integer 'set Type of Customer for loaded customer
    Dim mainCustType As Integer
    'Menu
    Dim ThisEnquiryResult As String = ""
    Dim ProgressStatus As Boolean = False
    'Security
    Dim TotalLending As Decimal
    Dim EquityValue As Decimal
    Dim TotalAssetValue As Decimal
    'Stability
    Dim TestDate As Date = #1/1/1900#
    'Budget
    Dim ThisSingleDepend As Decimal 'set DependSingle 
    Dim ThisCoupleDepend As Decimal 'set DependCouple
    Dim ThisDependChild As Decimal 'set DependChild
    Dim ClientDependency As Decimal 'set Client dependency
    Dim appDetect As Boolean 'Application values (ValuesId = 1) switch
    Dim BudgetDependTotal As Decimal 'set Budget Dependency
    Dim IncomeTotal As Decimal = 0 'set Total Income
    Dim ExpenseTotal As Decimal = 0 'set Total Expenses
    Dim BudgetMargin As Decimal = 0
    Dim OvertimeSelectedrb As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim OvertimeState As Boolean 'track state of Overtime Question
    'Credit
    Dim selectedrbQ1 As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim selectedrbQ2 As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim selectedrbQ3 As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim creditQuestion1State As Boolean 'track state of CreditQuestion1
    Dim creditQuestion2State As Boolean 'track state of CreditQuestion2
    Dim creditQuestion3State As Boolean 'track state of CreditQuestion3
    Dim creditQuestionStateChanged As Boolean = False 'track if a radio button state changed and not saved
    Dim customerTypeStateChanged As Boolean = False 'track if a CustomerType state changed from current
    Dim customerDataStateChanged As Boolean = False 'track if a data state changed and not been saved
    'Approval
    Dim selectedrbA1 As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim AApprovalState As Integer 'track state of ApproverApproval
    Dim selectedrbA2 As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim MApprovalState As Integer 'track state of ManagerApproval
    'Documents
    Dim WkshtFolder As Boolean = False 'set the Worksheet Folder present switch
    'Audit
    Dim TotalDrawdown As Decimal 'Total drawdown (Audit tab)
    Dim Refinance As Decimal ' refinanced amount (Audit tab)
    Dim AmountFinanced As Decimal 'Amount financed (Audit tab)
    Dim DealerCommission As Decimal 'Total Dealer Commission (Audit tab)
    Dim drawdownPaidOut As Decimal 'Drawdown paidout (Audit tab)
    Dim drawdownRetention As Decimal 'Drawdown retention (Audit tab)
    Dim CommPaid2Dealer As Decimal 'Commission paid to the dealer (Audit tab)
    Dim CommPaid2Retention As Decimal 'Commission paid to retention (Audit tab)
    'Payout
    Dim TotalPaidOutAmt As Decimal = 0 'set Paid-Out amount (Payout tab)
    Dim TotalFinAmt As Decimal = 0 'Total Drawdown + Refinance (Payout tab)
    Dim TotalDisburseAmt As Decimal 'set total disbursements amount (Audit tab)
    Dim ANZProcessDateSelected As Boolean = False
    Dim PayAdviceDateSelected As Boolean = False
    Dim EnquiryExported As Boolean = False
    'define print page info
    Private PrintPageSettings As New PageSettings
    Private StringToPrint As String
    Private PrintFont As New Font("Arial", 8)
    Private finishedString As String
    'localisation
    Dim culture As New CultureInfo("en-NZ")
    ' List to hold TabPages
    Dim AllTabPages As New List(Of TabPage)
    Dim FormTitle As String = String.Empty
    'Customer sizing
    Dim custCount As Integer
    Dim ffCustCount As Integer 'enqCustBS(bindingsource) customer count
    Dim custCountDtsbType23 As Integer 'number of records to be displayed in DtsbType2 grid. Used for sizing of grid 
    Dim custCountDtsbType01 As Integer
    Dim refCount As Integer 'number of records in reference bindingsource
    Dim thisRefCount As Integer 'number of records to be displayed in reference datarepeater
    'user settings
    Dim NewLocX As Integer
    Dim NewLocY As Integer
    'Loan
    Dim thisLoanCode As String
    '************
    'Define the cancellation token.
    Dim cancellationTokenSource As CancellationTokenSource ' = New CancellationTokenSource()
    'Dim ct As CancellationToken
    Dim asyncAnimation As AnimatedCircle.AsyncAnimation
    Dim auditpushright As Boolean = False
    'To keep track of handlers
    Dim cmbxPayoutYearMonthSelectedIndexChangedHandlerAdded As Boolean = True
    Dim prevLoaded As Boolean = False
    Dim specialNotesAlert As Boolean = False 'used in the Audit screen to display Alert in the Payout screen
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)


    'Load Form
    Friend Async Sub PassVariable(ByVal loadEnquiryVar As Integer, Optional ByVal checkOACStatus As Boolean = True)
        SuspendLayout()
        Dim compareResultTask As Threading.Tasks.Task(Of TrackResult)
        'MsgBox("Start Form Load")
        Try
            'Remove Handlers
            RemoveHandler dtpPSDate.ValueChanged, AddressOf dtpPSDate_ValueChanged
            RemoveHandler dtpPEDate.ValueChanged, AddressOf dtpPEDate_ValueChanged
            RemoveHandler ckbxSpecialNotesChecked.CheckedChanged, AddressOf ckbxSpecialNotesChecked_CheckedChanged

            'Payout
            auditpushright = False
            'get user settings
            Me.ClientSize = New Size(My.Settings.AppFormSize)
            Me.Location = New Point(My.Settings.AppFormLocation)

            SetToolbarStatusMessage(ToolStripStatusLabel1, "Status: Ready.", TrueTrackStatusBarMessageType.BlackMessage)
            Try
                'Load Lists
                'MsgBox("Loading Datasets")
                Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, loadEnquiryVar)
                ' setup the reference variables
                thisEnquiryId = loadEnquiryVar
                'set the form tag with EnquiryId
                Me.Tag = thisEnquiryId
                thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
                thisApplicationCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ApplicationCode")
                'ThisClientId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ClientId")
                thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
                'get type of Enquiry
                thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher 
                If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    lblLoanAmount.Visible = False
                Else
                    lblLoanAmount.Visible = True
                End If
                'set EnquiryType label
                lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
                thisIsArchived = If(IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("IsArchived")), False, EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("IsArchived"))
                'Load User table
                UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
                'get UserName
                lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
                thisCurrentStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentStatus")
                'Set Original Time variable
                OriginalTime = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DateTime")
                'Read Status30mins
                Status30 = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("Status30mins")
                'get Dealer ID
                ThisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
                'load the Enquiry Comments
                Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)

                '######################
                'Compare status with OAC status
                If Not thisCurrentStatus = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED Then
                    Dim onlineApplicationService As New OnlineApplication
                    'await later in the sub
                    Try
                        compareResultTask = onlineApplicationService.CompareStatus(thisApplicationCode, thisCurrentStatus, thisIsArchived)
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MessageBox.Show(ex.Message, "Status comparison", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If

                '######################
                'Customer
                '######################
                'Load in Customers for Enquiry
                Me.EnquiryCustomersTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryCustomers, thisEnquiryId)
                'number of customers for setting element sizing
                custCount = EnquiryCustomersBindingSource.Count
                'load first customer by default
                If custCount > 0 Then
                    Dim ecRowView As Data.DataRowView
                    Dim ecRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
                    ecRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
                    If ecRowView.Row IsNot Nothing Then
                        ecRow = ecRowView.Row
                        'load customer table
                        Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, ecRow.CustomerId)
                        loadedCustomerId = ecRow.CustomerId 'Current customer to be displayed
                        lblCustTypeValue.Text = MyEnums.GetDescription(DirectCast(ecRow.CustomerType, MyEnums.CustomerType))
                        'set customer panels
                        SetCustomerTypePanel(ecRow.CustomerType, loadedCustomerId, ecRow.Type)

                        pnlCustomerType.Visible = True
                        pnlCustomerType.Enabled = True
                    End If

                End If

                '************************* drFinPower (dataRepeater) datasource filter (in Audit tab)
                enqCustBS.DataSource = New DataView(EnquiryCustomersTableAdapter.GetDataByEnquiryId(thisEnquiryId))
                If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    'enqCustBS.Filter = "CustomerEnquiryType = 'main' OR CustomerEnquiryType = 'comain'"
                    enqCustBS.Filter = "Type = " & MyEnums.CustomerEnquiryType.Main & " OR Type = " & MyEnums.CustomerEnquiryType.CoMain
                End If
                ffCustCount = enqCustBS.Count

                '************************* Get Contact Display Name
                ThisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
                '************************* Set form title
                If thisIsArchived = True Then
                    FormTitle = " - Archived"
                Else
                    FormTitle = ""
                End If
                If My.Settings.IsTest = True Then
                    Me.Text = "Test Application | " & ThisContactDisplayName & FormTitle
                Else
                    Me.Text = ThisContactDisplayName & FormTitle
                End If
                '************************* Cannotate Contact Name           
                ContactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
                ContactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " &
                EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & ContactLastName
                'set lblContactName
                lblContactName.Text = ContactName
                ''************************* Cannotate Contact address
                ContactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
                lblContactAddress.Text = ContactAddress
                '************************* mainCustomerType
                If IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")) Then
                    lblMainCustTypeValue.Text = "Unknown"
                Else
                    mainCustType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")
                    lblMainCustTypeValue.Text = MyEnums.GetDescription(DirectCast(mainCustType, MyEnums.mainCustomerType))
                End If

                '*************** Set EnquiryType display options
                If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                    lblCurrentLoan.Visible = True
                    lblCurrentLoanAmt.Visible = True
                Else
                    lblCurrentLoan.Visible = False
                    lblCurrentLoanAmt.Visible = False
                End If
                '*************** end of Set EnquiryType display options
                Me.TableAdapterManager.SecurityTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Security, thisEnquiryId)
                'MsgBox("Loading Budget")
                Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                'Me.TableAdapterManager.ApplicationValuesTableAdapter.FillByValueId(EnquiryWorkSheetDataSet.ApplicationValues, 1)


            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("Load Form: Loading Lists generated an Exception Message: " + vbCrLf + ex.Message)
            End Try

            '*************** Get Dealer name
            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & ThisDealerId & "'"
            'MsgBox("Select Statement = " & selectStatement)
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim DealerTempDataAdapter As New SqlDataAdapter(selectCommand)
            Dim DealerTempDataSet As New DataSet
            Dim DealerTempDataTable As New DataTable
            Try
                'dumps results into datatable LoginDataTable
                DealerTempDataAdapter.Fill(DealerTempDataTable)
                'if no matching rows .....
                If DealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    DealerTempDataAdapter = Nothing
                    DealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf DealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim DealerTempDataRow As DataRow = DealerTempDataTable.Rows(0)
                    ThisDealerName = DealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    DealerTempDataAdapter = Nothing
                    DealerTempDataTable.Clear()
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
            lblDealerValue.Text = ThisDealerName
            '*************** End of Get Dealer name

            '*********************** Loan ****************************************
            '******************* End of Loan *************************************

            '*********************** Security ************************************
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                tbctrlAppDetails.TabPages.Remove(tabSecurity)
            Else
                'Calculate Total Lending
                CalculateTotalLending()
                CalculateTotalAssestValue()
            End If

            '********************* End of Security *******************************  
            '*********************** Budget **************************************
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                tbctrlAppDetails.TabPages.Remove(tabBudget)
                tbctrlAppDetails.TabPages.Remove(tabCompanyFinancials)
            Else
                'MyEnums.mainCustomerType.SoleTrader and MyEnums.mainCustomerType.Partnership require both TabPages
                'Set visibilities
                Select Case mainCustType
                    Case MyEnums.mainCustomerType.Company, MyEnums.mainCustomerType.Trust
                        tbctrlAppDetails.TabPages.Remove(tabBudget)
                    Case MyEnums.mainCustomerType.Individual
                        tbctrlAppDetails.TabPages.Remove(tabCompanyFinancials)
                    Case MyEnums.mainCustomerType.SoleTrader, mainCustType = MyEnums.mainCustomerType.Partnership
                        'tabCompanyFinancials
                        gpbxSalaries.Enabled = False
                        gpbxSalaries.Visible = False
                        tlpPosition.Enabled = False
                        tlpPosition.Visible = False
                        ckbxPAYEreturns.Enabled = False
                        ckbxPAYEreturns.Visible = False

                End Select

                If mainCustType = MyEnums.mainCustomerType.Individual Or mainCustType = MyEnums.mainCustomerType.SoleTrader Or mainCustType = MyEnums.mainCustomerType.Partnership Then
                    'Get Application values 
                    Try
                        Dim thisAppsettings As New AppSettings
                        ThisSingleDepend = thisAppsettings.DependSingle
                        ThisCoupleDepend = thisAppsettings.DependCouple
                        ThisDependChild = thisAppsettings.DependChild
                        appDetect = True
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Load Form: Get Application values generated an exception message:" & vbCrLf & ex.Message & vbNewLine & ex.TargetSite.ToString)
                    End Try
                    '************************************************
                    Try
                        'Calculate Budget YTD
                        Dim BudgetNetAmount As Decimal
                        Dim BudgetGross As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetGross"))
                        Dim BudgetPAYE As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPAYE"))
                        Dim BudgetDeductions As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDeductions"))
                        BudgetNetAmount = BudgetGross - BudgetPAYE - BudgetDeductions
                        'Get 31st March Year
                        Dim year As Integer = Date.Now.Year
                        Dim datTim1 As Date
                        Dim datTim2 As Date
                        Dim weekNum As Integer
                        'check Period Starting has value
                        If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodStarting") IsNot DBNull.Value Then
                            datTim1 = CDate(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodStarting"))
                        Else
                            datTim1 = New DateTime(year, 4, 1)
                        End If
                        'check Period Ending has value
                        If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodEnding") IsNot DBNull.Value Then
                            datTim2 = CDate(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodEnding"))
                        Else
                            datTim2 = Date.Now
                        End If
                        'Calculate week number from 31st march
                        If datTim1 < datTim2 Then
                            weekNum = DateDiff(DateInterval.WeekOfYear, datTim1, datTim2)
                        Else
                            datTim1 = New DateTime((year - 1), 4, 1)
                            weekNum = DateDiff(DateInterval.WeekOfYear, datTim1, datTim2)
                        End If
                        'set period starting DateTimePicker
                        dtpPSDate.Value = datTim1
                        'Calculate AverageYTD
                        If BudgetNetAmount > 0 And weekNum > 0 Then
                            Dim AverageYTD As Decimal = BudgetNetAmount / weekNum * 52 / 12
                            lblAvYTDValue.Text = AverageYTD.ToString("C")
                        End If
                        'End of Calculate Budget YTD
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Load Form: Calculate Budget YTD generated an exception message:" + vbCrLf + ex.Message)
                    End Try
                    '************************************************
                    Try
                        'Set Client Dependency from ThisClientType
                        If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyClient") IsNot DBNull.Value Then
                            ClientDependency = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyClient"))
                            If Not ClientDependency > 0 Then
                                ClientDependency = ThisSingleDepend
                                lblDependClient.Text = ThisSingleDepend.ToString("C")
                            Else
                                lblDependClient.Text = ClientDependency.ToString("C")
                            End If
                        Else
                            ClientDependency = ThisSingleDepend
                            lblDependClient.Text = ThisSingleDepend.ToString("C")
                        End If
                        'Budget dependency (get BudgetDependencyChildren)
                        If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyChildren") IsNot DBNull.Value Then
                            BudgetDependTotal = ClientDependency + (CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyChildren")) * ThisDependChild)
                        Else
                            BudgetDependTotal = ClientDependency
                        End If
                        lblTotalDependValue.Text = BudgetDependTotal.ToString("C")
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Load Form: Dependency generated an exception message:" + vbCrLf + ex.Message)
                    End Try
                    '************************************************
                    Try
                        'Total Incomes and Expenses
                        IncomeTotal = 0
                        ExpenseTotal = 0
                        lblMarginValue.ForeColor = Color.Black
                        Dim budgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
                        'add up the budget items
                        For Each budgetItemRow In EnquiryWorkSheetDataSet.Budget
                            IncomeTotal = IncomeTotal + CDec(budgetItemRow.Item("IncomeValue"))
                            ExpenseTotal = ExpenseTotal + CDec(budgetItemRow.Item("ExpenseValue"))
                        Next
                        'Add NetIncomeStated to Total Incomes
                        If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated") IsNot DBNull.Value Then
                            IncomeTotal = IncomeTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated"))
                        End If
                        lblTotalIncomeValue.Text = IncomeTotal.ToString("C")
                        'Add ExpensesStated to Total Expenses
                        If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated") IsNot DBNull.Value Then
                            ExpenseTotal = ExpenseTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated"))
                        End If
                        lblTotalExpensesValue.Text = ExpenseTotal.ToString("C")
                        '******************************************
                        'Calculate margin
                        BudgetMargin = 0
                        lblMarginValue.ForeColor = Color.Black
                        BudgetMargin = IncomeTotal - ExpenseTotal - BudgetDependTotal
                        If BudgetMargin < 0 Then
                            lblMarginValue.ForeColor = Color.Red
                        End If
                        lblMarginValue.Text = BudgetMargin.ToString("C")

                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Load Form: Incomes and Expenses generated an exception message:" + vbCrLf + ex.Message)
                    End Try
                    '************************************************
                    'Set Question check boxes and buttons
                    Try
                        ' set Overtime radioButtons
                        SetOvertimeRadioButtonState()
                        'Set Question 1 ckbx and btn
                        SetStateQuestion(1)
                        'Set Question 2 ckbx and btn
                        SetStateQuestion(2)
                        'Set Question 3 ckbx and btn
                        SetStateQuestion(3)
                        'Set Question 4 ckbx and btn
                        SetStateQuestion(4)
                        'Set Question 5 ckbx and btn
                        SetStateQuestion(5)
                        'Set Question 6 ckbx and btn
                        SetStateQuestion(6)
                        'Set Question 7 ckbx and btn
                        SetStateQuestion(7)
                        'Set Question 8 ckbx and btn
                        SetStateQuestion(8)
                        'Set Question 9 ckbx and btn
                        SetStateQuestion(9)
                        'Set Question 10 ckbx and btn
                        SetStateQuestion(10)
                        'Set Question 11 ckbx and btn
                        SetStateQuestion(11)
                        'Set Question 12 ckbx and btn
                        SetStateQuestion(12)
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("Load Form: SetStateQuestion generated an exception message:" + vbCrLf + ex.Message)
                    End Try
                End If
            End If



            '********************** End of Budget ****************************
            '****************** Documents ************************************
            'Load list of file info for ThisEnquiryId into Documents Tab
            ' ListView name = lvDocuments
            Try
                'set properties
                Me.lvDocuments.Items.Clear() 'to reset listbox when refresh
                If prevLoaded = False Then 'to stop multiple columns being created
                    Me.lvDocuments.BackColor = SystemColors.Window
                    lvDocuments.FullRowSelect = True
                    lvDocuments.View = View.Details
                    lvDocuments.GridLines = True
                    ' Display check boxes.
                    'lvDocuments.CheckBoxes = True
                    ' Create columns for the items and subitems.
                    ' Width of -2 indicates auto-size.
                    lvDocuments.Columns.Add("Name", 410, HorizontalAlignment.Left)
                    lvDocuments.Columns.Add("Creation Time", 150, HorizontalAlignment.Left)
                    lvDocuments.Columns.Add("File Size", 100, HorizontalAlignment.Right)
                End If
                'Get Files        
                'Get String to WorkSheet Drive
                Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
                Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
                'Check directory exists
                If Directory.Exists(folderPath) Then
                    'get new image
                    btnFolder.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                    'set visiblity of note
                    lblDocNote.Visible = True
                    'set the Worksheet Folder present switch
                    WkshtFolder = True
                    'activate drag and drop
                    Me.lvDocuments.AllowDrop = True
                    'Create place to store files
                    Dim aFiles() As String 'array type string
                    aFiles = Directory.GetFiles(folderPath)
                    'loop through aFiles
                    For Each sfile As String In aFiles
                        'create str array
                        Dim str(3) As String
                        Dim itm As ListViewItem
                        'assign str array values
                        Dim finfo As New FileInfo(sfile)
                        If finfo.Exists Then
                            str(0) = finfo.Name
                            str(1) = finfo.CreationTime
                            If finfo.Length < 1000 Then
                                str(2) = finfo.Length.ToString & " Bytes"
                            ElseIf finfo.Length >= 1000 And finfo.Length < 1000000 Then
                                str(2) = (finfo.Length / 1000).ToString & " KB"
                            Else
                                str(2) = (finfo.Length / 1000000).ToString & " MB"
                            End If
                            itm = New ListViewItem(str)
                            'itm.Checked = True
                            'add ListViewItem to ListView
                            lvDocuments.Items.Add(itm)
                        End If
                    Next sfile
                Else
                    'deactivate drag and drop
                    Me.lvDocuments.AllowDrop = False
                    Me.lvDocuments.BackColor = Color.LavenderBlush
                End If

                prevLoaded = True

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("Load Form: Loading Documents List generated an Exception Message: " & vbCrLf & ex.Message)
            End Try

            '*************** End of Documents ********************************
            'test appDetect switch
            If appDetect = True Then
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
            End If

        Catch ex As System.NullReferenceException
            MsgBox("System.NullReferenceException / " & ex.Message)

        Catch ex As SqlException
            MessageBox.Show("Sql Server Error # " & ex.Number & ": " & ex.Message, ex.GetType.ToString)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Loading Form generated an Exception Message: " & vbCrLf & ex.Message)
            'MsgBox(ex.Message)
        End Try

        '******************* QRG list **********************
        'get QRGList status
        Dim connectionString3 As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection3 As New SqlConnection(connectionString3)
        Dim selectStatement3 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
        'MsgBox("Select Statement = " & selectStatement2)
        Dim selectCommand3 As New SqlCommand(selectStatement3, connection3)
        Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand3)
        Dim QRGListTempDataSet As New DataSet
        Dim QRGListTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            QRGListTempDataAdapter.Fill(QRGListTempDataTable)
            'if no matching rows .....
            If QRGListTempDataTable.Rows.Count = 0 Then
                If thisCurrentStatus = "Enquiry Ended" Or thisCurrentStatus = "Follow-up" Then

                Else
                    MessageBox.Show("ERROR: No QRG List, please try again.")
                End If
                'clear the dataTable and the Connect information
                QRGListTempDataAdapter = Nothing
                QRGListTempDataTable.Clear()
                'if there is a matching row
            ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                QRGStatus = QRGListTempDataRow.Item(0)
                'MsgBox("QRG Status = " & QRGStatus)
                'clear the dataTable and the Connect information
                QRGListTempDataAdapter = Nothing
                QRGListTempDataTable.Clear()
            End If
            'close the connection
            If connection3.State <> ConnectionState.Closed Then
                connection3.Close()
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        '******************** set QRGStatus button colour
        Select Case QRGStatus
            Case 0
                btnQRGList.BackColor = Color.Transparent
            Case 1
                btnQRGList.BackColor = Color.Tomato
            Case 2
                btnQRGList.BackColor = Color.LightGreen
            Case Else
                btnQRGList.BackColor = Color.Transparent
        End Select
        '******************* end of QRG list **********************
        '************** change label.text lblCashPrice and adjust fields in Audit and Payout for checking
        'Select Case ThisTypeLoan
        Select Case thisEnquiryType
            Case MyEnums.EnquiryType.PersonalLoanNew, MyEnums.EnquiryType.PersonalLoanRefinance, MyEnums.EnquiryType.UnsecuredPersonalLoanNew, MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance
                'change label text
                lblCashPrice.Text = "Advance"
                'set visible for deposit
                lblDeposit.Visible = False
                txtbxDeposit.Visible = False
                'Audit
                ckbxFinPowerOpened.Enabled = True
                ckbxFinPowerOpened.Visible = True
                ckbxfinPowerWithdrawal.Enabled = False
                ckbxfinPowerWithdrawal.Visible = False
            Case MyEnums.EnquiryType.PersonalLoanVariation, MyEnums.EnquiryType.BusinessLoanVariation, MyEnums.EnquiryType.UnsecuredPersonalLoanVariation
                lblCashPrice.Text = "Advance"
                'set visible for deposit
                lblDeposit.Visible = False
                txtbxDeposit.Visible = False
                'Audit
                ckbxFinPowerOpened.Enabled = True
                ckbxFinPowerOpened.Visible = True
                ckbxfinPowerWithdrawal.Enabled = True
                ckbxfinPowerWithdrawal.Visible = True
            Case MyEnums.EnquiryType.BusinessLoanNew, MyEnums.EnquiryType.BusinessLoanRefinance
                lblCashPrice.Text = "Advance"
                'set visible for deposit
                lblDeposit.Visible = False
                txtbxDeposit.Visible = False
                'Audit
                ckbxFinPowerOpened.Enabled = True
                ckbxFinPowerOpened.Visible = True
                ckbxfinPowerWithdrawal.Enabled = False
                ckbxfinPowerWithdrawal.Visible = False
            Case MyEnums.EnquiryType.ConsumerRetail
                lblCashPrice.Text = "Cash Price"
                'set visible for deposit
                lblDeposit.Visible = True
                txtbxDeposit.Visible = True
                'Audit
                ckbxFinPowerOpened.Enabled = True
                ckbxFinPowerOpened.Visible = True
                ckbxfinPowerWithdrawal.Enabled = False
                ckbxfinPowerWithdrawal.Visible = False
            Case MyEnums.EnquiryType.ConsumerVehicle
                lblCashPrice.Text = "Cash Price"
                'set visible for deposit & tradein
                lblDeposit.Visible = True
                txtbxDeposit.Visible = True
                lblTradeIn.Visible = True
                txtTradeIn.Visible = True
                'Audit
                ckbxFinPowerOpened.Enabled = True
                ckbxFinPowerOpened.Visible = True
                ckbxfinPowerWithdrawal.Enabled = False
                ckbxfinPowerWithdrawal.Visible = False
            Case MyEnums.EnquiryType.PersonalSplitLoanDrawdown, MyEnums.EnquiryType.BusinessSplitLoanDrawdown, MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown
                lblCashPrice.Text = "Advance"
                'set visible for deposit
                lblDeposit.Visible = False
                txtbxDeposit.Visible = False
                'Audit
                drFinPower.Visible = False
                ckbxFinPowerOpened.Enabled = False
                ckbxFinPowerOpened.Visible = False
                ckbxfinPowerWithdrawal.Enabled = True
                ckbxfinPowerWithdrawal.Visible = True
                'Audit Processing
                lblApplication.Enabled = False
                ckbxApplication.Enabled = False
                lblDocs.Enabled = False
                ckbxDocs.Enabled = False
                lblDDShtNum.Enabled = False
                txtbxDDShtNum.Enabled = False
                ckbxPayBook.Enabled = False
                ckbxClientSetup.Enabled = False
                ckbxPayMethod.Enabled = False
                ckbxWageDeduction.Enabled = False
                'Audit processing documents
                ckbxID.Enabled = False
                ckbxDDAP.Enabled = False
                ckbxApp.Enabled = False
                ckbxProofAdd.Enabled = False
                ckbxProofInc.Enabled = False
                'Audit processing FinPower
                lblAllFields.Enabled = False
                ckbxClient.Enabled = False
                ckbxLoan.Enabled = False
                ckbxSecurity.Enabled = False
                ckbxGraded.Enabled = False
                'Audit processing PPSR
                ckbxPPSRNew.Enabled = False
                ckbxPPSRAmended.Enabled = False
                ckbxPPSRCurrent.Enabled = False
                ckbxPPSRFS.Enabled = False
                lblFS.Enabled = False
                ckbxDebtor.Enabled = False
                ckbxType.Enabled = False
                ckbxDesc.Enabled = False
                'Audit Protecta
                ckbxInsuranceIdentified.Enabled = False
                lblEntered.Enabled = False
                ckbxNoPolicies.Enabled = False
                ckbxCPI.Enabled = False
                ckbxPPIfull.Enabled = False
                ckbxPPIDO.Enabled = False
                ckbxGAP1.Enabled = False
                ckbxGAP2.Enabled = False
                ckbxMechWarr.Enabled = False
                ckbxInsuranceV.Enabled = False
                ckbxInsuranceC.Enabled = False
                txtSpecialNotes.IsReadOnly = True
                'Audit Final
                ckbxfinPowerWithdrawal.Enabled = True
                ckbxFinPowerOpened.Enabled = False
                ckbxWelcomeLetter.Enabled = False
                ckbxProtectaBooks.Enabled = False
                'Payout Refinance
                lblClosedInFP.Enabled = False
                ckbxClosedInFP.Enabled = False
                lblPPSRRefin.Enabled = False
                ckbxPPSRDischarged.Enabled = False
                ckbxRefPPSRAmended.Enabled = False
                lblProtectaCB.Enabled = False
                ckbxRefPayAdvice.Enabled = False
                lblPCBShtNum.Enabled = False
                txtbxPCBShtNum.Enabled = False
                lblCommRecall.Enabled = False
                ckbxCommRecall.Enabled = False
                lblCommRecallShtNum.Enabled = False
                txtbxCommRecallShtNum.Enabled = False
                'Payout Final
                lblDDSafe.Enabled = False
                ckbxDDSafe.Enabled = False
                lblArchives.Enabled = False
                ckbxArchived.Enabled = False
            Case MyEnums.EnquiryType.FinanceFacility
                'Loan breakdown
                lblLoanNum.Visible = False
                txtbxLoanNum.Visible = False
                txtbxLoanNum.Enabled = False
                gpbxLoanBreakdown.Visible = False
                gpbxLoanBreakdown.Enabled = False
                'Dealer commission
                gpbxDealerComm.Visible = False
                gpbxDealerComm.Enabled = False
                'Documents(scanned and saved)
                ckbxDDAP.Text = "Evaluation sheet"
                ckbxApp.Text = "Dealer Agreement"
                ckbxProofAdd.Text = "Deposit slip"
                gpbxProcessingForm.Visible = False
                gpbxProcessingForm.Enabled = False
                gpbxProcessingFinPower.Visible = False
                gpbxProcessingFinPower.Enabled = False
                gpbxProcessingPPSR.Visible = False
                gpbxProcessingPPSR.Enabled = False
                gpbxProcessingProtecta.Visible = False
                gpbxProcessingProtecta.Enabled = False
                'Insurances
                ckbxSecurityInsured.Text = "Indemnity Insurance and YFL interest noted"
                gpbxDisbursements.Visible = False
                gpbxDisbursements.Enabled = False
                'Final
                ckbxFinPowerOpened.Text = "Dealer setup completed"
                ckbxfinPowerWithdrawal.Visible = False
                ckbxfinPowerWithdrawal.Enabled = False
                lblMail.Text = "Banking"
                ckbxWelcomeLetter.Text = "Details passed to accountant"
                ckbxProtectaBooks.Visible = False
                ckbxProtectaBooks.Enabled = False

            Case Else
                lblCashPrice.Text = "Advance"
                'set visible for deposit
                lblDeposit.Visible = True
                txtbxDeposit.Visible = True
        End Select

        '******** disable controls if Current Status is declined or Waiting Sign-up etc *******
        '******** and enable buttons
        Select Case thisCurrentStatus
            Case Constants.CONST_CURRENT_STATUS_DECLINED, Constants.CONST_CURRENT_STATUS_WITHDRAWN, Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED
                'disable some controls on form
                btnQRGList.Enabled = False
                DisableSomeControls()
                ' Take a copy of TabPages(tabDueDiligence, tabPayOut) at the start
                AllTabPages.Add(tabDueDiligence) 'Audit
                tbctrlAppDetails.TabPages.Remove(tabDueDiligence)
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    AllTabPages.Add(tabPayout)
                End If
                tbctrlAppDetails.TabPages.Remove(tabPayout)
            Case Constants.CONST_CURRENT_STATUS_CAD_APPROVED
                ''disable some controls on form
                'DisableSomeControls()
                'Dim ApplicationDataFileMissing As Boolean = False
                'Dim AppMissingText As String = "Application file missing in the Enquiry folder"
                ''**##**
                'If Not CheckMembersCentreAppFileExists() Then
                '    ToolStripStatusLabel1.Text = AppMissingText
                '    ToolStripStatusLabel1.ForeColor = Color.Red
                '    ApplicationDataFileMissing = True
                'End If
                'If Not ApplicationDataFileMissing Or ApplicationAccessRights.canbyPassApplicationDocumentChecks Then

                '    'In case of TopUP (Variation) no XML or LoanCode will be generated
                '    If ThisTypeLoan = Constants.CONST_PRELIMREASON_PERSONAL_LOAN_TOPUP Or ThisTypeLoan = Constants.CONST_PRELIMREASON_PERSONAL_LOAN_VARIATION Then
                '        'set visible of DueDiligence button
                '        btnAudit.Visible = True
                '        btnAudit.Enabled = True
                '    Else
                '        'Import Loan XML to finPower only if LoanNum does not exist
                '        If getLoanNum() = "" Then

                '            btnAudit.Visible = False
                '            'Check XML file exists in Enquiry Folder
                '            Dim EnquiryFolder As String
                '            EnquiryFolder = ""
                '            If CheckXMLFileExists() Then
                '                btnCreatefinPowerLoan.Visible = True
                '                btnCreatefinPowerLoan.Enabled = True
                '                btnCreatefinPowerLoan.BackColor = Color.YellowGreen
                '            Else
                '                btnCreatefinPowerLoan.Visible = True
                '                btnCreatefinPowerLoan.Enabled = False
                '                btnCreatefinPowerLoan.BackColor = Me.BackColor
                '                If ToolStripStatusLabel1.Text = AppMissingText Then
                '                    ToolStripStatusLabel1.Text = ToolStripStatusLabel1.Text & ", XML file is missing in the Enquiry folder"
                '                Else
                '                    ToolStripStatusLabel1.Text = "XML file is missing in the Enquiry folder"
                '                End If
                '                ToolStripStatusLabel1.ForeColor = Color.Red
                '            End If
                '            btnLoanCode.Visible = True

                '        Else
                '            'If LoanNum already exists show Audit
                '            btnCreatefinPowerLoan.Visible = False
                '            btnLoanCode.Visible = False
                '            btnAudit.Visible = True
                '            btnAudit.Enabled = True
                '        End If
                '    End If
                'End If
                '' Take a copy of TabPages(tabDueDiligence, tabPayOut) at the start
                'AllTabPages.Add(tabDueDiligence)
                'AllTabPages.Add(tabPayout)
                'tbctrlAppDetails.TabPages.Remove(tabDueDiligence)
                'tbctrlAppDetails.TabPages.Remove(tabPayout)
                'disable some controls on form
                ''*****************************************************************************
                ''New code
                ''*****************************************************************************
                DisableSomeControls()
                Dim applicationDataFileMissing As Boolean = False
                Dim appMissingText As String = "Application file missing in the Enquiry folder"
                '**##**
                'If Not CheckMembersCentreAppFileExists() Then
                '    ToolStripStatusLabel1.Text = AppMissingText
                '    ToolStripStatusLabel1.ForeColor = Color.Red
                '    ApplicationDataFileMissing = True
                'End If
                'If Not ApplicationDataFileMissing Or ApplicationAccessRights.canbyPassApplicationDocumentChecks Then

                'In case of TopUP (Variation) no XML or LoanCode will be generated
                'Check the loanType is topup or variation
                '25/01/2018 add SplitoanDrawDown
                If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                    'if refinance then check application file exists in enquiry folder
                    If Not CheckApplicationFileExists() Then
                        'ToolStripStatusLabel1.Text = AppMissingText
                        'ToolStripStatusLabel1.ForeColor = Color.Red
                        SetToolbarStatusMessage(ToolStripStatusLabel1, appMissingText, TrueTrackStatusBarMessageType.RedMessage)
                        applicationDataFileMissing = True
                        'Display button to get Application html file from OAC
                        btnGetOACApplicationFile.Visible = True
                    Else
                        'Hide button to get application file
                        btnGetOACApplicationFile.Visible = False
                        'set visible of DueDiligence button
                        btnAudit.Visible = True
                        btnAudit.Enabled = True
                    End If

                Else 'if LoanType is NOT Topup or Variation

                    'Check LoanNum exists in DueDiligence table
                    If GetLoanNumFromDueDiligenceTable() = "" And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        'If Loan Code does not exist in DueDiligence Table
                        btnCreatefinPowerLoan.Visible = True 'if Application exists OAC
                        If ApplicationAccessRights.canViewManualLoanCodeBtn Then
                            btnLoanCode.Visible = True 'get Loan Code manually from OAC
                        End If
                        'Disable buttons Audit and GetOACApplicationfile
                        btnAudit.Visible = False
                        btnGetOACApplicationFile.Visible = False
                        btnPassedAudit.Visible = False


                    Else 'If LoanNum already exists OR EnquiryType = FinanceFacility show Audit button 

                        'Call OAC web service to check Application exists in Online Application Centre
                        Dim retTTResult As TrackResult
                        retTTResult = CheckApplicationExistsinOAC()
                        If retTTResult IsNot Nothing Then
                            If String.IsNullOrWhiteSpace(retTTResult.ErrorMessage) Then
                                If retTTResult.Status = True Then
                                    'Application exists in OAC then show button to continue with creating Loan in finPower
                                    btnCreatefinPowerLoan.Visible = True
                                Else
                                    'Display button for Audit if application does not exist in OAC and LoanCode in duediligence table
                                    btnAudit.Visible = True
                                    btnAudit.Enabled = True
                                End If
                            Else
                                'Display both button for Audit and Create Loan in finPower if web service failed
                                btnAudit.Visible = True
                                btnAudit.Enabled = True

                                btnCreatefinPowerLoan.Visible = True
                                auditpushright = True
                            End If
                            If retTTResult.Status = False Then
                                SetToolbarStatusMessage(ToolStripStatusLabel1, retTTResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                                'MessageBox.Show(retTTResult.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        End If
                    End If
                End If
                ' Take a copy of TabPages(tabDueDiligence, tabPayOut) at the start
                AllTabPages.Add(tabDueDiligence)
                tbctrlAppDetails.TabPages.Remove(tabDueDiligence)
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    AllTabPages.Add(tabPayout)
                End If
                tbctrlAppDetails.TabPages.Remove(tabPayout)
            Case Constants.CONST_CURRENT_STATUS_AUDIT
                'disable some controls on form
                DisableSomeControls()


                btnLoanCode.Visible = False
                ' Take a copy of TabPages(tabDueDiligence, tabPayOut) at the start
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    AllTabPages.Add(tabPayout)
                End If
                tbctrlAppDetails.TabPages.Remove(tabPayout)

                Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
                If EnquiryWorkSheetDataSet.DueDiligence.Rows.Count > 0 Then
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        SetAuditAmounts()
                    End If
                Else
                    AllTabPages.Add(tabDueDiligence)
                    tbctrlAppDetails.TabPages.Remove(tabDueDiligence)
                    MessageBox.Show("Error Loading Due Diligence Table: No record found!", "Due Diligence", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'set StatusStrip text
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Error Loading Due Diligence Table!"
                End If
                'If previously Status was payout then we need to reset Audit controls
                'enable Due Diligence Controls
                enableAuditControls(True)
                'set visible of Export button, set visible of Payout button
                If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    btnPassedAudit.Visible = False
                    btnPassedAudit.Enabled = False
                    btnExportFiles.Visible = True
                    btnExportFiles.Enabled = True
                Else
                    btnPassedAudit.Visible = True
                    btnPassedAudit.Enabled = True
                    btnExportFiles.Visible = False
                    btnExportFiles.Enabled = False
                End If



            Case Constants.CONST_CURRENT_STATUS_AUDIT_FAILED
                'disable some controls on form
                DisableSomeControls()
                'set visible of Payout button
                btnPassedAudit.Visible = False
                btnPassedAudit.Enabled = False
                ' Take a copy of TabPages(tabDueDiligence, tabPayOut) at the start
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    AllTabPages.Add(tabPayout)
                End If
                tbctrlAppDetails.TabPages.Remove(tabPayout)
                Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
                If EnquiryWorkSheetDataSet.DueDiligence.Rows.Count > 0 Then
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        SetAuditAmounts()
                    End If
                    'disable Due Diligence Controls
                    enableAuditControls(False)

                Else
                    AllTabPages.Add(tabDueDiligence)
                    tbctrlAppDetails.TabPages.Remove(tabDueDiligence)
                    MessageBox.Show("Error Loading Due Diligence Table: No record found!", "Due Diligence", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'set StatusStrip text
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Error Loading Due Diligence Table!"
                End If

            Case Constants.CONST_CURRENT_STATUS_PAYOUT
                Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
                Me.TableAdapterManager.PayoutTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Payout, thisEnquiryId)
                If EnquiryWorkSheetDataSet.Payout.Rows.Count > 0 Then
                    'If IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("LoanInMonthOf")) Or EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("LoanInMonthOf") = String.Empty Then
                    '    cmbxPayoutMonth.SelectedText = CStr(Date.Now.Month)
                    'End If
                    'If IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("LoanInYearOf")) Or EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("LoanInYearOf") = String.Empty Then
                    '    cmbxPayoutYear.SelectedText = CStr(Date.Now.Year)
                    'End If
                    'disable some controls on form
                    DisableSomeControls()
                    'set visible of Save & Export button
                    '10/2017
                    'btnSaveFile.Visible = True
                    'btnSaveFile.Enabled = True
                    btnExportFiles.Visible = True
                    btnExportFiles.Enabled = True

                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        SetAuditAmounts()
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("SpecialNotes")) Then
                            If Not String.IsNullOrEmpty(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("SpecialNotes")) Then
                                specialNotesAlert = True
                            End If
                        End If
                        If specialNotesAlert = True Then
                            lblSpecialNotesAlert.Visible = True
                            ckbxSpecialNotesChecked.Visible = True
                        Else
                            lblSpecialNotesAlert.Visible = False
                            ckbxSpecialNotesChecked.Visible = False
                        End If
                        ckbxSpecialNotesChecked.Checked = CheckState.Unchecked
                    End If
                    'disable Due Diligence Controls
                    enableAuditControls(False)

                    'incase has been reset
                    'disable Payout Controls
                    gpbxProcessPayout.Enabled = True
                    gpbxPaid.Enabled = True
                    gpbxRefinAccounts.Enabled = True
                    gpbxFinal.Enabled = True
                    btnPayoutUpdate.Enabled = True

                    SetPayoutAmounts()
                    If IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("ProcessDate")) Then
                        lblANZDate.ForeColor = Color.Red
                        'set tooltip
                        ToolTip1.SetToolTip(dtpANZProcess, "a date for the ANZ process is required")
                    Else
                        lblANZDate.ForeColor = SystemColors.ControlText
                        'set tooltip to nothing
                        ToolTip1.SetToolTip(dtpANZProcess, "")
                    End If
                    If IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("AdviceDate")) Then
                        lblPayAdviceDate.ForeColor = Color.Red
                    Else
                        lblPayAdvice.ForeColor = SystemColors.ControlText
                    End If



                Else
                    AllTabPages.Add(tabPayout)
                    tbctrlAppDetails.TabPages.Remove(tabPayout)
                    MessageBox.Show("Error Loading Payout Table:" & vbCrLf & "No record found!", "Payout", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'set StatusStrip text
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Error Loading Payout Table!"
                End If


                'Case Constants.CONST_CURRENT_STATUS_COMPLETED
                '    Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
                '    'disable some controls on form
                '    DisableSomeControls()
                '    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                '        Me.TableAdapterManager.PayoutTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Payout, thisEnquiryId)
                '        If EnquiryWorkSheetDataSet.Payout.Rows.Count > 0 Then
                '            SetAuditAmounts()
                '            'disable Due Diligence Controls
                '            enableAuditControls(False)

                '            'disable Payout Controls
                '            gpbxProcessPayout.Enabled = False
                '            gpbxPaid.Enabled = False
                '            gpbxRefinAccounts.Enabled = False
                '            gpbxFinal.Enabled = False
                '            btnPayoutUpdate.Enabled = False
                '            SetPayoutAmounts()
                '            'set visible of Export button
                '            btnExportFiles.Visible = True
                '            btnExportFiles.Enabled = True
                '        Else
                '            AllTabPages.Add(tabPayout)
                '            tbctrlAppDetails.TabPages.Remove(tabPayout)
                '            MessageBox.Show("Error Loading Payout Table:" & vbCrLf & "No record found!", "Payout", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '            'set StatusStrip text
                '            ToolStripStatusLabel1.ForeColor = Color.Red
                '            ToolStripStatusLabel1.Text = "Error Loading Payout Table!"
                '        End If
                '    Else
                '        'disable Due Diligence Controls
                '        enableAuditControls(False)

                '        tbctrlAppDetails.TabPages.Remove(tabPayout)
                '        'set visible of Export button
                '        btnExportFiles.Visible = True
                '        btnExportFiles.Enabled = True
                '    End If


                'Case Constants.CONST_CURRENT_STATUS_EXPORTED
            Case Constants.CONST_CURRENT_STATUS_COMPLETED
                Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
                'disable some controls on form
                DisableSomeControls()
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    Me.TableAdapterManager.PayoutTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Payout, thisEnquiryId)
                    If EnquiryWorkSheetDataSet.Payout.Rows.Count > 0 Then
                        SetAuditAmounts()
                        'disable Due Diligence Controls
                        enableAuditControls(False)

                        'disable Payout Controls
                        gpbxProcessPayout.Enabled = False
                        gpbxPaid.Enabled = False
                        gpbxRefinAccounts.Enabled = False
                        gpbxFinal.Enabled = False
                        btnPayoutUpdate.Enabled = False
                        SetPayoutAmounts()
                        'Disable Export button
                        btnExportFiles.Visible = True
                        btnExportFiles.Enabled = False
                        btnAddLoanProgress.Enabled = False
                    Else
                        AllTabPages.Add(tabPayout)
                        tbctrlAppDetails.TabPages.Remove(tabPayout)
                        MessageBox.Show("Error Loading Payout Table:" & vbCrLf & "No record found!", "Payout", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'set StatusStrip text
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Error Loading Payout Table!"
                    End If
                Else
                    'disable Due Diligence Controls
                    enableAuditControls(False)

                    tbctrlAppDetails.TabPages.Remove(tabPayout)
                    'Disable Export button
                    btnExportFiles.Visible = True
                    btnExportFiles.Enabled = False
                    btnAddLoanProgress.Enabled = False
                End If


        End Select



        '******** End of disable controls ***********
        '*************** Set permissions
        'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
        If LoggedinPermissionLevel = MyEnums.RolePermissions.Readonly Then
            btnAddLoanProgress.Enabled = False
            'disable Due Diligence Controls
            enableAuditControls(False)
            'disable Payout Controls
            gpbxProcessPayout.Enabled = False
            gpbxPaid.Enabled = False
            gpbxRefinAccounts.Enabled = False
            gpbxFinal.Enabled = False
            btnPayoutUpdate.Enabled = False
            'Disable Export button
            btnExportFiles.Visible = True
            btnExportFiles.Enabled = False
            btnAddLoanProgress.Enabled = False
        End If
        'StdUser
        If LoggedinPermissionLevel = MyEnums.RolePermissions.StdUser Then
            'disable Due Diligence Controls
            enableAuditControls(False)
            'disable Payout Controls
            gpbxProcessPayout.Enabled = False
            gpbxPaid.Enabled = False
            gpbxRefinAccounts.Enabled = False
            gpbxFinal.Enabled = False
            btnPayoutUpdate.Enabled = False
            'Disable Export button
            btnExportFiles.Visible = True
            btnExportFiles.Enabled = False
            btnAddLoanProgress.Enabled = False
        End If
        'StdUser+
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.StdUser Then
            AddCommentToolStripMenuItem1.Visible = True
            AddCommentToolStripMenuItem.Visible = True
            btnAddLoanProgress.Visible = True
        End If
        'Approver+
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Approver Then
            'Can see /use Archive Enquiry in File menu for their user status
            SendEmailToolStripMenuItem.Visible = True
            ApprovalFormToolStripMenuItem.Visible = True
            ClientDetailsToolStripMenuItem.Visible = True
            EditDetailsToolStripMenuItem.Visible = True
            EndEnquiryToolStripMenuItem.Visible = True
            'verify user is Enquiry Manager for the Enquiry
            If LoggedinId = thisEnquiryManagerId Then
                ArchiveEnquiryToolStripMenuItem.Visible = True
            End If
            If thisIsArchived = True Then
                UnArchiveTSMenuItem.Visible = True
                ReactivateEnquiryToolStripMenuItem.Visible = False
            Else
                UnArchiveTSMenuItem.Visible = False
                ReactivateEnquiryToolStripMenuItem.Visible = True
            End If
        End If
        'Manager+
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Manager Then
            ResetToDueDiligenceMenuItem.Visible = True
            If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then
                ChangeToAuditToolStripMenuItem.Visible = True 'Reset to Audit from Payout
            Else
                ChangeToAuditToolStripMenuItem.Visible = False
            End If
            ArchiveEnquiryToolStripMenuItem.Visible = True
        End If
        'Administrator+
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Administrator Then
            If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_CAD_APPROVED Then
                JumpToAuditToolStripMenuItem.Visible = True
            Else
                JumpToAuditToolStripMenuItem.Visible = False 'Calls AuditProcess
            End If
            If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_AUDIT Or
                thisCurrentStatus = Constants.CONST_CURRENT_STATUS_AUDIT_FAILED Or
                thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then
                ReverseXMLExportToolStripMenuItem.Visible = True
            End If
        End If

        '*************** end of Set permissions
        '*************** set Deny permissions (after Allow permissions)
        'Can only archive if CurrentStatus is Enquiry Ended, Declined, Withdrawn or Completed
        If Not thisCurrentStatus = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED And Not thisCurrentStatus = Constants.CONST_CURRENT_STATUS_DECLINED And Not thisCurrentStatus = Constants.CONST_CURRENT_STATUS_WITHDRAWN And Not thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Then 'CONST_CURRENT_STATUS_EXPORTED Then
            ArchiveEnquiryToolStripMenuItem.Visible = False
            ReactivateEnquiryToolStripMenuItem.Visible = False
        Else
            EndEnquiryToolStripMenuItem.Visible = False
        End If
        'If CurrentStatus is Payout
        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then
            EmailRemittanceToolStripMenuItem.Visible = True
        Else
            EmailRemittanceToolStripMenuItem.Visible = False
        End If

        'If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Then
        '    EndEnquiryToolStripMenuItem.Visible = False
        'End If
        '*************** end of set Deny permissions
        '*************** thisIsArchived settings for display and functionality
        If thisIsArchived = True Then
            Me.BackColor = Color.LightGray
            Me.MenuStrip1.BackColor = Color.LightGray
            Me.Panel1.BackColor = Color.LightGray
            Me.StatusStripMessage.BackColor = Color.LightGray
            'Menu
            EndEnquiryToolStripMenuItem.Visible = False
            ArchiveEnquiryToolStripMenuItem.Visible = False
            ReactivateEnquiryToolStripMenuItem.Visible = False
            'UnArchiveTSMenuItem.Visible = True 'See LoggedinPermissionLevel
            EditToolStripMenuItem.Visible = False
            DocumentsToolStripMenuItem.Visible = False
            EditDetailsToolStripMenuItem.Visible = False
            AddCommentToolStripMenuItem.Visible = False
            SendEmailToolStripMenuItem.Visible = False
            'iterate through Controls
            For Each ctl As Control In Me.Controls
                If ctl.Name = "tbctrlAppDetails" Then
                    For Each ctrl As Control In tbctrlAppDetails.Controls
                        If ctrl.Name = "tabLoan" Then
                            For Each c1 As Control In ctrl.Controls
                                If c1.Name = "pnlLoan" Then
                                    For Each c2 As Control In c1.Controls
                                        If c2.Name = "btnAddLoanProgress" Then
                                            c2.Visible = False
                                        End If
                                    Next
                                End If
                            Next
                        ElseIf ctrl.Name = "tabDueDiligence" And thisCurrentStatus = Constants.CONST_CURRENT_STATUS_AUDIT Then
                            'disable Due Diligence Controls
                            enableAuditControls(False)
                        ElseIf ctrl.Name = "tabPayout" And thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then
                            'disable Payout Controls
                            gpbxProcessPayout.Enabled = False
                            gpbxPaid.Enabled = False
                            gpbxRefinAccounts.Enabled = False
                            gpbxFinal.Enabled = False
                            btnPayoutUpdate.Enabled = False
                            SetPayoutAmounts()
                        End If
                    Next
                ElseIf ctl.Name = "Panel1" Then
                    Util.SetReadonlyControls(ctl.Controls)
                End If

            Next
        Else
            Me.BackColor = Color.Empty
            Me.MenuStrip1.BackColor = Color.Empty
            Me.Panel1.BackColor = Color.Empty
            Me.StatusStripMessage.BackColor = Color.Empty

        End If
        '*************** End of thisIsArchived settings for display and functionality

        ' Add handlers to combo and text boxes
        AddHandler dtpPSDate.ValueChanged, AddressOf dtpPSDate_ValueChanged
        AddHandler dtpPEDate.ValueChanged, AddressOf dtpPEDate_ValueChanged
        AddHandler ckbxSpecialNotesChecked.CheckedChanged, AddressOf ckbxSpecialNotesChecked_CheckedChanged

        ResumeLayout()

        '************************* Compare status with OAC status
        If Not thisCurrentStatus = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED Then
            If checkOACStatus = True Then
                Dim compareResult As New TrackResult
                Try
                    compareResult = Await compareResultTask
                    If compareResult.ErrorCode > 0 Then
                        MessageBox.Show(compareResult.ErrorMessage, "Status comparison", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    End If
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MessageBox.Show(ex.Message, "Status comparison", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        End If



    End Sub

    Private Sub AppForm_Leave(sender As Object, e As EventArgs) Handles Me.Leave

    End Sub    'End of PassVariable 

    Private Sub AppForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Payout
        'If ThisLoanReason = "Split Loan Drawdown" Then
        '    MsgBox("This is a Split Loan Drawdown")
        'End If
        AnimatedCirclePanel.Visible = False
        asyncAnimation = New AnimatedCircle.AsyncAnimation(AnimatedCirclePanel, Color.RoyalBlue)
        'Audit information
        If ApplicationAccessRights.isAdministrator Or ApplicationAccessRights.isSystemsManager Then
            pbAuditInformation.Visible = True
        Else
            pbAuditInformation.Visible = False
        End If

        'TODO: Call to check status

    End Sub

    '*********** End of Loading Form ***************

    '*********** TabControl ***************

    '*********** End of TabControl ***************
    ''' <summary>
    ''' Set Security, Budget, Business Financials Controls to disabled or readonly
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DisableSomeControls() 'disable some controls on form
        'btnCheckEnquiries.Enabled = False
        'btnCheckNames.Enabled = False
        'gpbxQuickRefGuide.Enabled = False
        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            '********** Security
            txtbxSecurityComments.ReadOnly = True
            '*********** Budget
            'gpbxIncome.Enabled = False
            Util.SetReadonlyControls(gpbxIncome.Controls)
            'gpbxExpenses.Enabled = False
            Util.SetReadonlyControls(gpbxExpenses.Controls)
            gpbxDependency.Enabled = False
            Util.SetReadonlyControls(gpbxDependency.Controls)
            'btnDeleteSelectedRow.Enabled = False
            txtbxOtherExplain.ReadOnly = True

        End If
        '********** Company Financials
        'gpbxIncomeExpenses.Enabled = False
        Util.SetReadonlyControls(gpbxIncomeExpenses.Controls)
        'gpbxSalaries.Enabled = False
        Util.SetReadonlyControls(gpbxSalaries.Controls)
        'gpbxEarnings.Enabled = False
        Util.SetReadonlyControls(gpbxEarnings.Controls)
        'gpbxPosition.Enabled = False
        Util.SetReadonlyControls(gpbxPosition.Controls)
        '********** Customers

        '*********** Documents
        btnFolder.Enabled = False

    End Sub

    Private Sub FormRefresh(Optional ByVal checkOACStatus As Boolean = True)
        Cursor.Current = Cursors.WaitCursor
        Try
            'Set BackColors back to default color in case values written by code
            Util.ReSetControlsBackColor(gpbxPaid.Controls, False)
            Call PassVariable(thisEnquiryId, checkOACStatus)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try
        Cursor.Current = Cursors.Default

    End Sub
    'End of Refresh

    '************************ AppForm **********************









    'Private Sub cmbxCurrentStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxCurrentStatus.SelectedIndexChanged
    '    'calculate time difference for 30min status
    '    Try
    '        'MsgBox("ThisEnquiryId = " & ThisEnquiryId & vbCrLf & "Status30 = " & Status30 & vbCrLf & "cmbxCurrentStatus.Text = " & _
    '        '       cmbxCurrentStatus.Text & vbCrLf & "ckbxActive.CheckState = " & ckbxActive.Checked)

    '        'update Enquiry table
    '        Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
    '        EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '        EnquiryRow.BeginEdit()
    '        'update Current Status
    '        EnquiryRow.CurrentStatus = cmbxCurrentStatus.Text
    '        'calculate time difference for 30min status
    '        Dim StatusTimeDiff As Integer
    '        StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
    '        'MsgBox("DateDiff = " + CStr(StatusTimeDiff))
    '        If StatusTimeDiff < 30 Then
    '            EnquiryRow.Status30mins = cmbxCurrentStatus.Text
    '        End If

    '        Me.Validate()
    '        Me.ClientBindingSource.EndEdit()
    '        Me.EnquiryBindingSource.EndEdit()
    '        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
    '        'update Status Strip
    '        ToolStripStatusLabel1.ForeColor = Color.Black    '        ToolStripStatusLabel1.Text = "Current Status Updated.    Status: Ready."
    '        'MsgBox("update Current Status")
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub


    'Private Sub gpbxQuickRefGuideResize(Optional ByVal ResizeSwitch = 2)
    '    'change size of groupboxes: gpbxQuickRefGuide and tbctrlAppDetails when overlaid label lblQRGOverlay is clicked
    '    'check value of form variable
    '    ' Suspend the form layout
    '    Me.SuspendLayout()

    '    If ResizeSwitch = 1 Or 0 Then
    '        gpbxResizeQRGBoolean = ResizeSwitch
    '    End If

    '    'If gpbxResizeQRGBoolean = 0 Then
    '    '    gpbxQuickRefGuide.Size = New Size(680, 15)
    '    '    gpbxResizeQRGBoolean = 1
    '    '    tbctrlAppDetails.Location = New Point(12, 106)
    '    '    tbctrlAppDetails.Size = New Size(680, 673)
    '    'Else
    '    '    gpbxQuickRefGuide.Size = New Size(680, 295)
    '    '    gpbxResizeQRGBoolean = 0
    '    '    tbctrlAppDetails.Location = New Point(12, 391)
    '    '    tbctrlAppDetails.Size = New Size(680, 388)
    '    'End If

    '    If gpbxResizeQRGBoolean = 0 Then
    '        gpbxQuickRefGuide.Size = New Size(680, 15)
    '        gpbxResizeQRGBoolean = 1
    '        tbctrlAppDetails.Location = New Point(12, 106)
    '        tbctrlAppDetails.Size = New Size(680, 645)
    '    Else
    '        gpbxQuickRefGuide.Size = New Size(680, 295)
    '        gpbxResizeQRGBoolean = 0
    '        tbctrlAppDetails.Location = New Point(12, 391)
    '        tbctrlAppDetails.Size = New Size(680, 360)
    '    End If
    '    changelblQRGBacLayer()

    '    'MsgBox("gpbxResizeQRGBoolean value = " & gpbxResizeQRGBoolean & vbCrLf & _
    '    '       "formWidth = " & Me.Width & vbCrLf & _
    '    '       "formHeight = " & Me.Height & vbCrLf & _
    '    '       "Current width = " & tbctrlAppDetails.Width.ToString & vbCrLf & _
    '    '       "Current height = " & tbctrlAppDetails.Height.ToString)

    '    'Resume the form layout
    '    Me.ResumeLayout()



    'End Sub

    '********************* End of  AppForm **********************
    '************************ Menu ******************************

    '******************* Menu items

#Region "MenuStrip"

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    'Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click
    '    Cursor.Current = Cursors.WaitCursor
    '    Try
    '        Me.Validate()
    '        Me.ClientBindingSource.EndEdit()
    '        Me.EnquiryBindingSource.EndEdit()
    '        Me.SecurityBindingSource.EndEdit()
    '        'Me.QRGListBindingSource.EndEdit()
    '        Me.BudgetBindingSource.EndEdit()
    '        Me.DueDiligenceBindingSource.EndEdit()
    '        Me.PayoutBindingSource.EndEdit()
    '        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    '    FormRefresh()
    '    Cursor.Current = Cursors.Default
    'End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        'Me.ClientBindingSource.CancelEdit()
        'Me.EnquiryBindingSource.CancelEdit()
        'Me.SecurityBindingSource.CancelEdit()
        'Me.QRGListBindingSource.CancelEdit()
        'Me.BudgetBindingSource.CancelEdit()
        Me.DueDiligenceBindingSource.CancelEdit()
        Me.PayoutBindingSource.CancelEdit()
        Me.Close()
    End Sub

    ''' <summary>
    ''' Reset Status back to Due Diligence
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ResetToDueDiligenceMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetToDueDiligenceMenuItem.Click
        'Check with user
        Dim msg As String
        Dim response As MsgBoxResult
        Dim CommentId As Integer
        'Display message
        msg = "Do you want to Reset Status back to Due-diligence?"
        response = MessageBox.Show(msg, "Reset Status", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
        If response = MsgBoxResult.Yes Then   ' User choose Yes.
            Cursor.Current = Cursors.WaitCursor
            Application.DoEvents()
            'update WizardStatus and RadioButtons
            Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
            'reset approval status 0 = nothing, 1 = declined, 2 = approved
            EnquiryRow.ApprovalStatus = MyEnums.ApprovalStatus.Nothing
            'reset State of QuestionApprover
            EnquiryRow.ApproverName = ""
            'Reset Current Status
            EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
            'update WizardStatus
            EnquiryRow.WizardStatus = MyEnums.WizardStatus.WizForm1
            'reset DDAprovalDate
            EnquiryRow.DDAprovalDate = #1/1/2000#
            'Set Original Enquiry Time
            Dim OriginalTime As DateTime
            OriginalTime = EnquiryRow.DateTime
            'calculate time difference for 30min status
            Dim StatusTimeDiff As Integer
            StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
            If StatusTimeDiff < 30 Then
                EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
            End If

            'create log note
            Dim CommentStr As String
            CommentStr = "Reset Status back to Due-diligence"
            CommentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)
            'save to database
            Try
                Me.Validate()
                EnquiryBindingSource.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

            'launch WkShtWizForm1 and pass Reference Id
            Dim WkShtWizFrm1 As New WkShtWizForm1
            WkShtWizFrm1.PassVariable(thisEnquiryId)
            WkShtWizFrm1.Show()
            Cursor.Current = Cursors.Default
            'Close this Form
            Me.Close()
        End If

    End Sub

    ''' <summary>
    ''' Jump to Audit
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Call the AuditProcess</remarks>
    Private Sub JumpToAuditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JumpToAuditToolStripMenuItem.Click
        AuditProcess()
    End Sub

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    ''' <summary>
    ''' Reverse XML Export
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Reset the currentStatus to WaitingForDocs. Remove Loan Num from DueDiligence table. If no LoanNum then btnCreatefinPowerLoan is displayed</remarks>
    Private Sub ReverseXMLExportToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ReverseXMLExportToolStripMenuItem.Click
        'Check with user
        Dim msg As String
        Dim title As String
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        Dim commentId As Integer
        msg = "Do you want to Reverse XMLExport?"   ' Define message.
        style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
        title = "Reverse XML Export?"   ' Define title.
        ' Display message.
        response = MsgBox(msg, style, title)
        If response = MsgBoxResult.Yes Then   ' User choose Yes.
            Cursor.Current = Cursors.WaitCursor
            'update WizardStatus and RadioButtons
            Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
            'Reset Current Status
            EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_CAD_APPROVED
            'update WizardStatus
            EnquiryRow.WizardStatus = 7

            'Set Original Enquiry Time
            Dim OriginalTime As DateTime
            OriginalTime = EnquiryRow.DateTime
            'calculate time difference for 30min status
            Dim StatusTimeDiff As Integer
            StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
            If StatusTimeDiff < 30 Then
                EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_CAD_APPROVED
            End If

            'Update DueDiligence
            Dim rowView3 As DataRowView
            rowView3 = CType(DueDiligenceBindingSource.Current, DataRowView)
            Dim DueDilRow As EnquiryWorkSheetDataSet.DueDiligenceRow
            DueDilRow = Me.EnquiryWorkSheetDataSet.DueDiligence(0)
            DueDilRow.LoanNum = ""

            'create log note            
            Dim CommentStr As String
            CommentStr = "XML Export reversed. Status changed to Waiting Sign-up"
            commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

            'save to database
            Try
                Me.Validate()
                EnquiryBindingSource.EndEdit()
                DueDiligenceBindingSource.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                Me.DueDiligenceTableAdapter.Update(Me.EnquiryWorkSheetDataSet.DueDiligence)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

            'refresh form
            FormRefresh()
        End If
    End Sub
    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub

    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim DisplayDealersFrm As New DisplayDealersForm
            DisplayDealersFrm.PassVariable(ThisDealerId)
            DisplayDealersFrm.ShowDialog()
            DisplayDealersFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '************ end of Check Names

    Private Sub EditDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditDetailsToolStripMenuItem.Click
        'Save all incase
        Try
            Me.Validate()
            'Me.ClientBindingSource.EndEdit()
            'Me.EnquiryBindingSource.EndEdit()
            'Me.SecurityBindingSource.EndEdit()
            'Me.QRGListBindingSource.EndEdit()
            'Me.BudgetBindingSource.EndEdit()
            Me.DueDiligenceBindingSource.EndEdit()
            Me.PayoutBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'AddSecurityFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save all incase
        Try
            Me.Validate()
            'Me.ClientBindingSource.EndEdit()
            'Me.EnquiryBindingSource.EndEdit()
            'Me.SecurityBindingSource.EndEdit()
            'Me.QRGListBindingSource.EndEdit()
            'Me.BudgetBindingSource.EndEdit()
            Me.DueDiligenceBindingSource.EndEdit()
            Me.PayoutBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        Try
            'launch Add Comment Form
            Dim AddCommentFrm As New AddCommentForm()
            AddCommentFrm.PassVariable(thisEnquiryId)
            ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
            If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'Call the function you used to populate the data grid..
                Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment added."
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment cancelled."
            End If
            'When Add Comment Form closed
            AddCommentFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Adding a comment caused an error:" & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub SendEmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SendEmailToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Dim ActiveTab As String = ""
        Try
            Me.Validate()
            'Me.ClientBindingSource.EndEdit()
            'Me.EnquiryBindingSource.EndEdit()
            'Me.SecurityBindingSource.EndEdit()
            'Me.BudgetBindingSource.EndEdit()
            Me.DueDiligenceBindingSource.EndEdit()
            Me.PayoutBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default
        Try
            Dim EmailFrm As New EmailForm
            EmailFrm.PassVariable(thisEnquiryId, ThisDealerId)
            'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
            If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'refresh comment box
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
            End If
            EmailFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ApprovalFormToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub EmailApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailApprovalToolStripMenuItem.Click
        'save changes
        'Cursor.Current = Cursors.WaitCursor
        'Me.Validate()
        'Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        'Cursor.Current = Cursors.Default

        Dim EmailAppFrm As New EmailApprovalForm
        EmailAppFrm.PassVariable(thisEnquiryId, ThisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailAppFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailAppFrm.Dispose()
    End Sub

    Private Sub EmailRemittanceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EmailRemittanceToolStripMenuItem.Click
        Dim EmailDealerRemittanceFrm As New EmailDealerRemittanceForm
        EmailDealerRemittanceFrm.PassVariable(thisEnquiryId, ThisDealerId)
        'Show EmailDealerRemittanceFrm as a modal dialog and determine if DialogResult = OK.
        If EmailDealerRemittanceFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh(False)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailDealerRemittanceFrm.Dispose()
    End Sub

    Private Sub WebsiteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WebsiteToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        'launch internet browser for web application
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching web browser."
        System.Diagnostics.Process.Start(My.Settings.YFL_website)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub DeclinedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeclinedToolStripMenuItem.Click
        ThisEnquiryResult = "Declined"
        ProgressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        EnquiryBindingSource.EndEdit()
                        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, ThisContactDisplayName, ThisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                DeclinedFrm.Dispose()
                ProgressStatus = True
            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                ProgressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining enquiry cancelled. Status: Ready."
            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Abort Then
                DeclinedFrm.Dispose()
                ProgressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining enquiry aborted. Status: Ready."
            Else
                DeclinedFrm.Dispose()
                ProgressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining enquiry failed. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If ProgressStatus = True Then
            'Try
            '    Cursor.Current = Cursors.WaitCursor
            '    'launch  Standard Worksheet Form
            '    Dim AppFrm As New AppForm
            '    AppFrm.PassVariable(thisEnquiryId)
            '    AppFrm.Show()
            '    Cursor.Current = Cursors.Default
            '    'Close this Form
            '    Me.Close()
            'Catch ex As Exception
            '    MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            'End Try
            FormRefresh(False)
        End If
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub WithdrawnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawnToolStripMenuItem.Click
        ThisEnquiryResult = "Withdrawn"
        ProgressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, ThisContactDisplayName, ThisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                DeclinedFrm.Dispose()
                ProgressStatus = True
            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                ProgressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Withdrawing of Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If ProgressStatus = True Then
            'Try
            '    Cursor.Current = Cursors.WaitCursor
            '    'launch  Standard Worksheet Form
            '    Dim AppFrm As New AppForm
            '    AppFrm.PassVariable(thisEnquiryId)
            '    AppFrm.Show()
            '    Cursor.Current = Cursors.Default
            '    'Close this Form
            '    Me.Close()
            'Catch ex As Exception
            '    MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            'End Try
            FormRefresh(False)
        End If
    End Sub

    Private Async Sub ArchiveEnquiryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArchiveEnquiryToolStripMenuItem.Click
        Dim result As New Util.StringOut
        result = Await Util.NewArchiveEnquiry(thisEnquiryId)
        If result.Result = True Then
            If result.StringOut.Length > 0 Then
                MessageBox.Show(result.StringOut, "Archiving", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Archiving completed. Status: Ready."
            Else
                MessageBox.Show("Archiving was successful", "Archiving", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            MessageBox.Show(result.StringOut, "Error Archiving", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        FormRefresh()

        'Dim archiveDays As Integer
        'Dim thisAppsettings As New AppSettings
        'archiveDays = CInt(thisAppsettings.DaysBeforeArchive)
        'Dim thisCurrentStatusSetDate As Date
        'Dim thisCurrentStatus As String
        ''check enquiry exists in Enquiry table
        'Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        'enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        'Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
        'enquiryDt = enquiryTa.GetDataByEnquiryId(thisEnquiryId)
        'Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        'If enquiryDt.Rows.Count = 0 Then
        '    'no rows
        '    MessageBox.Show("Enquiry does not exist", "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        'ElseIf enquiryDt.Rows.Count > 0 Then
        '    enquiryRow = enquiryDt.Rows(0)
        '    thisCurrentStatusSetDate = enquiryRow.CurrentStatusSetDate
        '    thisCurrentStatus = enquiryRow.CurrentStatus
        '    'test CurrentStatusSetDate is 10 days old Or Exported
        '    Dim dD As Long = DateDiff(DateInterval.Day, thisCurrentStatusSetDate, Date.Now)
        '    If dD >= archiveDays Then 'If dD >= 10 Then 'Use when not auto archiving exported enquiries
        '        'OK to carryon
        '        'Get user confirmation
        '        Dim msg As String
        '        Dim title As String
        '        Dim response As MsgBoxResult
        '        Dim result As Boolean

        '        msg = "Do you want to Archive this Enquiry?"   ' Define message.
        '        title = "Archive Enquiry?"   ' Define title.
        '        response = MessageBox.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
        '        If response = MsgBoxResult.Yes Then   ' User choose Yes.
        '            Cursor.Current = Cursors.WaitCursor
        '            Try
        '                'Perform operations
        '                'construct print string
        '                Dim enquiryPrint As String
        '                'Dim enquiryPrintOut As Util.StringOut = ConstructPrintStringAll()
        '                Dim enquiryPrintOut As Util.StringOut = Util.GetPrintStringAll(thisEnquiryId)

        '                If enquiryPrintOut.Result Then
        '                    enquiryPrint = enquiryPrintOut.StringOut
        '                Else
        '                    MessageBox.Show("Error creating Print String" & vbCrLf & "Aborting Archive process!", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                    Exit Sub
        '                End If

        '                'Check is possible to Change Remote Status
        '                Dim systemCanChangeRemoteStatus As Boolean
        '                systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
        '                If systemCanChangeRemoteStatus = True Then
        '                    'update OAC
        '                    Dim onlineApplicationService As New OnlineApplication
        '                    Dim retTTResult As New TrackResult
        '                    Dim actionType As Integer = OACWebService.ApplicationActionType.Archive
        '                    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, String.Empty)
        '                    If retTTResult.Status = False Then 'error condition
        '                        Dim commentId As Integer
        '                        'Add comment
        '                        commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
        '                        'set dialogbox
        '                        msg = retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage & vbCrLf & vbCrLf & "Remote (OAC) Archiving has failed, do you wish to continue with TrueTrack archiving?" & vbCrLf & vbCrLf & "If you do not know what to do, select No."
        '                        Dim diagResult As Integer = MessageBox.Show(msg, "Remote (OAC) Archiving Error", MessageBoxButtons.YesNo)
        '                        If diagResult = DialogResult.No Then
        '                            ToolStripStatusLabel1.Text = "Remote (OAC) Archiving error. Archiving aborted!"
        '                            ToolStripStatusLabel1.ForeColor = Color.Red
        '                            Exit Sub
        '                        ElseIf diagResult = DialogResult.Yes Then
        '                            'call archive function and get return value
        '                            result = Archive_Enquiry(thisEnquiryId, enquiryPrint)
        '                            If result = True Then
        '                                'Add comment
        '                                commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Remote (OAC) Archiving error. TrueTrack archiving completed.")
        '                                ToolStripStatusLabel1.Text = "Remote (OAC) Archiving error. TrueTrack archiving completed."
        '                                ToolStripStatusLabel1.ForeColor = Color.Blue

        '                            Else
        '                                ToolStripStatusLabel1.Text = "Remote (OAC) Archiving error. TrueTrack archiving error!"
        '                                ToolStripStatusLabel1.ForeColor = Color.Red
        '                                MsgBox("TrueTrack Archiving failed")
        '                            End If
        '                        End If
        '                    Else 'retTTResult.Status = True
        '                        'call archive function and get return value
        '                        result = Archive_Enquiry(thisEnquiryId, enquiryPrint)
        '                        If result = True Then
        '                            Dim commentId As Integer
        '                            'Add comment
        '                            commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Remote (OAC) Archiving completed. TrueTrack archiving completed.")
        '                            ToolStripStatusLabel1.Text = "Remote (OAC) Archiving Completed. TrueTrack archiving completed."
        '                            ToolStripStatusLabel1.ForeColor = Color.Blue

        '                        Else
        '                            ToolStripStatusLabel1.Text = "Remote (OAC) Archiving completed. TrueTrack archiving error!"
        '                            ToolStripStatusLabel1.ForeColor = Color.Red
        '                            MsgBox("TrueTrack Archiving failed")
        '                        End If

        '                    End If
        '                Else
        '                    'no remote archiving allowed, call archive function and get return value
        '                    result = Archive_Enquiry(thisEnquiryId, enquiryPrint)
        '                    If result = True Then
        '                        Dim commentId As Integer
        '                        'Add comment
        '                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, "TrueTrack archiving completed.")
        '                        ToolStripStatusLabel1.Text = "TrueTrack archiving completed."
        '                        ToolStripStatusLabel1.ForeColor = Color.Blue

        '                    Else
        '                        ToolStripStatusLabel1.Text = "TrueTrack archiving failed!"
        '                        ToolStripStatusLabel1.ForeColor = Color.Red
        '                        MsgBox("TrueTrack Archiving failed")
        '                    End If
        '                End If


        '            Catch ex As Exception
        '                MsgBox(ex.Message)
        '            End Try

        '            FormRefresh()
        '            Cursor.Current = Cursors.Default

        '        Else
        '            ' User choose No.
        '        End If
        '    Else
        '        'Not enough time since last Status set to Archive
        '        MessageBox.Show("Enquiry Status is not " & archiveDays.ToString & " days old yet." & vbCrLf & "Try again later", "Archiving has been aborted", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    End If

        'End If


    End Sub

    Private Async Sub ReactivateEnquiryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReactivateEnquiryToolStripMenuItem.Click
        'PURPOSE: To change the status from Enquiry Ended, Withdrawn or Declined back to Due Diligence or Follow-up
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        'check current status
        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_WITHDRAWN Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_DECLINED Then
            'Get user confirmation
            Dim msg As String
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult

            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            'Check Enquiry is archived, can reactivate only archived enquiry
            Dim isArchive As Boolean = If(IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("IsArchived")), False, EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("IsArchived"))
            If isArchive = True Then
                msg = "Can not reactivate an archived Enquiry!"   ' Define message.
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            msg = "Do you want to reactive this Enquiry?"   ' Define message.
            style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
            title = "Reactivate Enquiry?"   ' Define title.
            ' Display message.
            response = MsgBox(msg, style, title)
            If response = MsgBoxResult.Yes Then   ' User choose Yes.
                Cursor.Current = Cursors.WaitCursor
                Application.DoEvents()
                If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED Then
                    Try
                        'no applicationCode, update WizardStatus & CurrentStatus
                        enquiryRow.BeginEdit()
                        enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                        'update WizardStatus
                        enquiryRow.WizardStatus = MyEnums.WizardStatus.FollowUp '1
                        'set CurrentStatusSetDate
                        enquiryRow.CurrentStatusSetDate = DateTime.Now
                        'Set Original Enquiry Time
                        Dim OriginalTime As DateTime
                        OriginalTime = enquiryRow.DateTime
                        'calculate time difference for 30min status
                        Dim StatusTimeDiff As Integer
                        StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
                        If StatusTimeDiff < 30 Then
                            enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                        End If
                        'save to database
                        Me.Validate()
                        enquiryRow.EndEdit()
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                    'create comment
                    Dim CommentStr As String
                    CommentStr = "Enquiry Reactivated. Status changed to " & Constants.CONST_CURRENT_STATUS_FOLLOW_UP
                    Dim commentId As Integer
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)
                    'launch FollowUpForm and pass Enquiry Id
                    Dim FollowUpFrm1 As New FollowUpForm
                    Try
                        FollowUpFrm1.PassVariable(thisEnquiryId)
                        FollowUpFrm1.Show()
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                    Me.Close()
                Else
                    'update WizardStatus & CurrentStatus has applicationCode so set OAC
                    'Check is possible to Change Remote Status
                    Dim commentId As Integer
                    Dim systemCanChangeRemoteStatus As Boolean
                    Dim thisAppsettings As New AppSettings
                    systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                    If systemCanChangeRemoteStatus = True Then
                        Dim onlineApplicationService As New OnlineApplication
                        Dim retTTResult As New TrackResult
                        Dim thisApplicationCode As String = enquiryRow.ApplicationCode
                        Dim note As String = String.Empty
                        'Change Status in OAC
                        Dim actionType As Integer = OACWebService.ApplicationActionType.Reactivate
                        retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                        If retTTResult.Status = False Then 'error condition
                            commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                            MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                    End If

                    Try
                        enquiryRow.BeginEdit()
                        'Reset Current Status
                        enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
                        'update WizardStatus
                        enquiryRow.WizardStatus = MyEnums.WizardStatus.WizForm1 '2
                        'set CurrentStatusSetDate
                        enquiryRow.CurrentStatusSetDate = DateTime.Now
                        'Set Original Enquiry Time
                        Dim OriginalTime As DateTime
                        OriginalTime = enquiryRow.DateTime
                        'calculate time difference for 30min status
                        Dim StatusTimeDiff As Integer
                        StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
                        If StatusTimeDiff < 30 Then
                            enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
                        End If
                        'save to database
                        Me.Validate()
                        enquiryRow.EndEdit()
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                    'create log note
                    Dim CommentStr As String
                    CommentStr = "Enquiry Reactivated. Status changed to " & Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)
                    'launch WkShtWizForm1 and pass Enquiry Id
                    Dim WkShtWizFrm1 As New WkShtWizForm1
                    Try
                        WkShtWizFrm1.PassVariable(thisEnquiryId)
                        WkShtWizFrm1.Show()
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                    Me.Close()
                End If
            Else
                ' User choose No, do nothing
            End If
        Else
            MessageBox.Show("This Enquiry can not be reactivated", "Reactivation Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub MCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MCToolStripMenuItem.Click
        Dim MembersCentreUtilObject As New WebUtil
        MembersCentreUtilObject.MCLink()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = MembersCentreUtilObject.GetLabelForeColor
        ToolStripStatusLabel1.Text = MembersCentreUtilObject.GetLabelText
    End Sub

    Private Async Sub UnArchiveTSMenuItem_Click(sender As Object, e As EventArgs) Handles UnArchiveTSMenuItem.Click
        'Get user confirmation
        Dim msg As String
        Dim title As String
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        Dim result As Boolean
        Dim commentId As Integer

        msg = "Do you want to Un-Archive this Enquiry?"
        style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
        title = "Un-Archive Enquiry?"
        ' Display message.
        response = MsgBox(msg, style, title)
        If response = MsgBoxResult.Yes Then
            Cursor.Current = Cursors.WaitCursor
            Try
                Dim systemCanChangeRemoteStatus As Boolean
                Dim thisAppsettings As New AppSettings
                systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                If systemCanChangeRemoteStatus = True Then
                    'update OAC
                    Dim onlineApplicationService As New OnlineApplication
                    Dim retTTResult As New TrackResult
                    Dim actionType As Integer = OACWebService.ApplicationActionType.Unarchive
                    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, String.Empty)
                    If retTTResult.Status = False Then 'error condition
                        'Add comment
                        commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                        'set dialogbox
                        msg = retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage & vbCrLf & vbCrLf & "Remote Un-archiving has failed, do you wish to continue with local Un-archiving?" & vbCrLf & vbCrLf & "If you do not know what to do, select No."
                        Dim diagResult As Integer = MessageBox.Show(msg, "Remote Un-Archiving Error", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2)
                        If diagResult = DialogResult.No Then
                            ToolStripStatusLabel1.Text = "Remote Un-Archiving error. Un-Archiving aborted!"
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            Exit Sub
                        ElseIf diagResult = DialogResult.Yes Then
                            'call Un-archive function and get return value
                            result = UnArchive_Enquiry(thisEnquiryId)
                            If result = True Then
                                'Add comment
                                commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Remote un-archiving error. Local un-archiving completed.")
                                ToolStripStatusLabel1.Text = "Remote un-archiving error. Local un-archiving completed."
                                ToolStripStatusLabel1.ForeColor = Color.Blue

                            Else
                                ToolStripStatusLabel1.Text = "Remote Un-archiving error. Local un-archiving error!"
                                ToolStripStatusLabel1.ForeColor = Color.Red
                                MsgBox("Local un-archiving failed")
                            End If
                        End If
                    Else
                        'SetStatus completed Ok
                        'call local Un-archive function and get return value
                        result = UnArchive_Enquiry(thisEnquiryId)
                        If result = True Then
                            'Add comment
                            commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Remote un-archiving completed. Local un-archiving completed.")
                            ToolStripStatusLabel1.Text = "Remote un-archiving completed. Local un-archiving completed."
                            ToolStripStatusLabel1.ForeColor = Color.Blue

                        Else
                            ToolStripStatusLabel1.Text = "Remote Un-archiving completed. Local un-archiving error!"
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            MsgBox("Local un-archiving failed")
                        End If
                    End If
                Else
                    'System cannot do remote change Status
                    'call local Un-archive function and get return value
                    result = UnArchive_Enquiry(thisEnquiryId)
                    If result = True Then
                        'Add comment
                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, "Local un-archiving completed.")
                        ToolStripStatusLabel1.Text = "Local un-archiving completed."
                        ToolStripStatusLabel1.ForeColor = Color.Blue

                    Else
                        ToolStripStatusLabel1.Text = "Local un-archiving error!"
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        MsgBox("Local un-archiving failed")
                    End If
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

            FormRefresh()
            Cursor.Current = Cursors.Default

        End If

    End Sub

#End Region



    '*********************End of Menu ******************************
    '********************* Loan ********************

    '******************* End of Menu items

    Private Sub btnAddLoanProgress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddLoanProgress.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()

    End Sub



    Private Sub SecurityDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles SecurityDataGridView.DoubleClick
        Try
            'test there is data rows
            If Not SecurityBindingSource.Current Is Nothing Then
                'Get Selected Row
                Dim SelectedRowView As Data.DataRowView
                Dim SelectedRow As EnquiryWorkSheetDataSet.SecurityRow
                SelectedRowView = CType(SecurityBindingSource.Current, System.Data.DataRowView)
                SelectedRow = CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.SecurityRow)
                'Launch new AddSecurity Form
                'pass Security Id so will bring up selected security for modification
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Modifying Security Item."
                Dim AddSecurityFrm As New AddSecurityForm
                AddSecurityFrm.PassVariable(thisEnquiryId, thisEnquiryManagerId, SelectedRow.SecurityId)
                'AddSecurityFrm.ShowDialog(Me)
                If (AddSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Security Record successfully updated!"
                    '    'Call the function you used to populate the data grid..
                    '    MsgBox("AddSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
                    '    Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, ThisEnquiryId)
                Else
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Security Record modification cancelled. Status: Ready."
                End If
                AddSecurityFrm.Dispose()
                Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
                'Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
                CalculateTotalLending()
                CalculateTotalAssestValue()
            Else
                'No Data Row selected, add NEW Security
                Select Case thisCurrentStatus
                    Case Constants.CONST_CURRENT_STATUS_DECLINED

                    Case Constants.CONST_CURRENT_STATUS_CAD_APPROVED

                    Case Constants.CONST_CURRENT_STATUS_AUDIT

                    Case Constants.CONST_CURRENT_STATUS_AUDIT_FAILED

                    Case Constants.CONST_CURRENT_STATUS_PAYOUT

                    Case Constants.CONST_CURRENT_STATUS_COMPLETED

                        'Case Constants.CONST_CURRENT_STATUS_EXPORTED

                    Case Else
                        Try
                            'Launch new AddSecurity Form
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Blue
                            ToolStripStatusLabel1.Text = "Adding New Security Record."
                            Dim AddSecurityFrm As New AddSecurityForm
                            AddSecurityFrm.PassVariable(thisEnquiryId, thisEnquiryManagerId)
                            'AddSecurityFrm.ShowDialog(Me)
                            If (AddSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Black
                                ToolStripStatusLabel1.Text = "Security Record added successfully!"
                            Else
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Black
                                ToolStripStatusLabel1.Text = "Adding of Security Record cancelled. Status: Ready."
                            End If
                            AddSecurityFrm.Dispose()
                            Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
                            'Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
                            CalculateTotalLending()
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            MsgBox(ex.Message)
                        End Try
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CalculateTotalLending()
        Try
            'Calculate Total Lending
            TotalLending = 0
            For Each dr As DataRow In EnquiryWorkSheetDataSet.Tables("Security").Rows
                TotalLending = TotalLending + CDec(dr.Item("LendingValue"))
            Next
            'MsgBox("Total Lending = " + CStr(TotalLending))
            lblTotalLendValue.Text = TotalLending.ToString("C")
            'Equity Value
            Dim thisLoanAmount As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanValue"))
            Dim thisCurrentLoanAmt As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentLoanAmt"))
            lblSecurityLoanAmountValue.Text = thisLoanAmount.ToString("C")
            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurntLoan.Visible = True
                lblCurrentLoanAmtCalc.Visible = True
                EquityValue = TotalLending - thisLoanAmount - thisCurrentLoanAmt
            Else
                lblCurntLoan.Visible = False
                lblCurrentLoanAmtCalc.Visible = False
                EquityValue = TotalLending - thisLoanAmount
            End If
            If EquityValue < 0 Then
                lblEquityValue.ForeColor = Color.Red
            Else
                lblEquityValue.ForeColor = Color.Black
            End If
            'MsgBox("Equity Value = " + CStr(EquityValue))
            lblEquityValue.Text = EquityValue.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("CalculateTotalLending generated an exception message: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub CalculateTotalAssestValue()
        Try
            'Calculate Total Assest Value
            TotalAssetValue = 0
            For Each avdr As DataRow In EnquiryWorkSheetDataSet.Tables("Security").Rows
                TotalAssetValue = TotalAssetValue + CDec(avdr.Item("TodayValue"))
            Next
            lblTotalAssetValue.Text = TotalAssetValue.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '********************** End of Security *********************
    '*********************** Stability *************************



    Function GetRiskDesc(ByVal riskValue As Int16) As String
        'returns the Risk description equal to the Risk value passed
        Dim riskTable As EnquiryWorkSheetDataSet.List_AMLRiskDataTable
        riskTable = List_AMLRiskTableAdapter.GetData()
        Dim riskDataRow() As DataRow
        riskDataRow = riskTable.Select("RiskValue = " & riskValue)
        Return riskDataRow(0)("RiskDesc")
    End Function

    '*********************** End of Stability *************************
    '*********************** Budget **********************

    Private Sub SetStateQuestion(ByVal Num As String)
        Dim BudgetQuestion As String = "BudgetQuestion" & Num
        Dim ckbx As String = "ckbxQuestion" & Num & "No"
        Dim btn As String = "btnQuestion" & Num & "Add"
        'get State of Question from database
        Dim StateQuestion As Integer = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item(BudgetQuestion)
        'Look through Controls Collection to find 'ckbx'
        Dim controls1() As Control = Me.Controls.Find(ckbx, True)
        If controls1.Count > 0 Then
            ' It exists, let us cast it to the right control type (CheckBox)
            Dim ThisCheckBox1 As CheckBox = DirectCast(controls1(0), CheckBox)
            ' We're working with a checkbox now...
            Select Case StateQuestion
                Case 1
                    ThisCheckBox1.Checked = True
                    ThisCheckBox1.Enabled = True
                Case 2
                    ThisCheckBox1.Checked = False
                    ThisCheckBox1.Enabled = False
                Case Else
                    ThisCheckBox1.Checked = False
                    ThisCheckBox1.Enabled = True
            End Select
        End If
        'Look through Controls Collection to find 'btn'
        Dim controls2() As Control = Me.Controls.Find(btn, True)
        If controls2.Count > 0 Then
            ' It exists, let us cast it to the right control type (CheckBox)
            Dim ThisBtn1 As Button = DirectCast(controls2(0), Button)
            ' We're working with a Button now...
            Select Case StateQuestion
                Case 1
                    ThisBtn1.Enabled = False
                Case 2
                    ThisBtn1.Enabled = True
                Case Else
                    ThisBtn1.Enabled = True
            End Select
        End If
    End Sub

    'Private Sub btnDependSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDependSingle.Click
    '    'Get Value for Single dependency
    '    Try
    '        lblDependClient.Text = ThisSingleDepend.ToString("C")
    '        CalcTotalDependValue()
    '        CalcMargin()
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    'Private Sub btnDependCouple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDependCouple.Click
    '    'Get Value for Couple dependency
    '    Try
    '        lblDependClient.Text = ThisCoupleDepend.ToString("C")
    '        CalcTotalDependValue()
    '        CalcMargin()
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    'Private Sub txtbxNumChildren_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxNumChildren.LostFocus
    '    Try
    '        CalcTotalDependValue()
    '        CalcMargin()
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub CalcTotalDependValue()
        Try
            'Budget dependency
            ' BudgetDependTotal already set in top of document
            If CDec(txtbxNumChildren.Text) > 0 Then
                BudgetDependTotal = CDec(lblDependClient.Text) + (CDec(txtbxNumChildren.Text) * ThisDependChild)
            Else
                BudgetDependTotal = CDec(lblDependClient.Text)
            End If
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "CalcTotalDependValue(): BudgetDependTotal = " & BudgetDependTotal & ". Status: Ready."
            'MsgBox("CalcTotalDependValue(): BudgetDependTotal = " & BudgetDependTotal)
            lblTotalDependValue.Text = BudgetDependTotal.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CalcMargin()
        Try
            Dim MarginValue As Decimal = CDec(lblTotalIncomeValue.Text) - CDec(lblTotalExpensesValue.Text) - CDec(lblTotalDependValue.Text)
            If MarginValue < 0 Then
                lblMarginValue.ForeColor = Color.Red
            Else
                lblMarginValue.ForeColor = Color.Black
            End If
            lblMarginValue.Text = MarginValue.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

#Region "Earnings calculator"

    Private Sub txtbxGross_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxGross.LostFocus
        CalcAvYtdValue()
    End Sub

    Private Sub txtbxPAYE_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPAYE.LostFocus
        CalcAvYtdValue()
    End Sub

    Private Sub txtbxDeductions_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDeductions.LostFocus
        CalcAvYtdValue()
    End Sub

    Private Sub dtpPSDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPSDate.ValueChanged
        CalcAvYtdValue()
    End Sub

    Private Sub dtpPEDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPEDate.ValueChanged
        CalcAvYtdValue()
    End Sub

#End Region


    Private Sub CalcAvYtdValue()
        Try
            'Calculate Budget
            Dim BudgetNetAmount As Decimal
            Dim BudgetGross As Decimal = CDec(If(String.IsNullOrEmpty(txtbxGross.Text), "0", txtbxGross.Text))
            Dim BudgetPAYE As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPAYE.Text), "0", txtbxPAYE.Text))
            Dim BudgetDeductions As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDeductions.Text), "0", txtbxDeductions.Text))
            Dim PeriodEnding As Date = CDate(dtpPEDate.Value)
            Dim PeriodStarting As Date = CDate(dtpPSDate.Value)
            BudgetNetAmount = BudgetGross - BudgetPAYE - BudgetDeductions
            Dim weekNum As Integer
            'check Period Ending has value
            If PeriodEnding = Nothing Then
                PeriodEnding = Date.Now
            End If
            'Calculate week number from PeriodStarting
            If PeriodStarting < PeriodEnding Then
                weekNum = DateDiff(DateInterval.WeekOfYear, PeriodStarting, PeriodEnding)
            Else
                MessageBox.Show("Starting Period is OLDER than Ending Period", "Calculating YYD Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            'Calculate AverageYTD
            If BudgetNetAmount > 0 And weekNum > 0 Then
                Dim AverageYTD As Decimal = BudgetNetAmount / weekNum * 52 / 12
                lblAvYTDValue.Text = AverageYTD.ToString("C")
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TotalIncomeExpenses()
        Try
            'Total Incomes and Expenses
            IncomeTotal = 0
            ExpenseTotal = 0
            Dim BudgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
            For Each BudgetItemRow In EnquiryWorkSheetDataSet.Budget
                IncomeTotal = IncomeTotal + CDec(BudgetItemRow.Item("IncomeValue"))
                ExpenseTotal = ExpenseTotal + CDec(BudgetItemRow.Item("ExpenseValue"))
            Next
            'Total Incomes
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated") IsNot DBNull.Value Then
                IncomeTotal = IncomeTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated"))
            End If
            lblTotalIncomeValue.Text = IncomeTotal.ToString("C")
            'Total Expenses
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated") IsNot DBNull.Value Then
                ExpenseTotal = ExpenseTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated"))
            End If
            lblTotalExpensesValue.Text = ExpenseTotal.ToString("C")
            '******************************************
            'Budget dependency
            Dim DependClient As Decimal
            'Dim BudgetDependTotal As Decimal, already set at top of page
            'Client Dependency
            If Not lblDependClient.Text = Nothing And Not lblDependClient.Text = "" Then
                DependClient = CDec(lblDependClient.Text)
            Else
                DependClient = ThisSingleDepend
            End If
            'Children Dependency
            'MsgBox("txtbxNumChildren.Text = " & txtbxNumChildren.Text)
            If Not txtbxNumChildren.Text = Nothing Or Not txtbxNumChildren.Text = "" Or CDec(txtbxNumChildren.Text) > 0 Then
                BudgetDependTotal = DependClient + (CDec(txtbxNumChildren.Text) * ThisDependChild)
            Else
                BudgetDependTotal = DependClient
            End If
            lblTotalDependValue.Text = BudgetDependTotal.ToString("C")
            '******************************************
            'Calculate margin
            Dim BudgetMargin As Decimal = IncomeTotal - ExpenseTotal - BudgetDependTotal
            If BudgetMargin < 0 Then
                lblMarginValue.ForeColor = Color.Red
            Else
                lblMarginValue.ForeColor = Color.Black
            End If
            lblMarginValue.Text = BudgetMargin.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub btnExpensesStated_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            'Save current values
            Me.Validate()
            Me.BudgetBindingSource.EndEdit()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        Try
            Dim AddBudgetExpenseItemFrm As New AddBudgetExpenseItemForm
            AddBudgetExpenseItemFrm.PassVariable(thisEnquiryId)
            AddBudgetExpenseItemFrm.ShowDialog()
            'If (AddBudgetItemFrmShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            '    'Call the function you used to populate the data grid..
            '    MsgBox("AddBudgetItemFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
            '    Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, ThisEnquiryId)
            'End If
            AddBudgetExpenseItemFrm.Dispose()
            Me.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
            'Total incomes and expenses
            TotalIncomeExpenses()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BudgetDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles BudgetDataGridView.DoubleClick
        Select Case thisCurrentStatus
            Case Constants.CONST_CURRENT_STATUS_DECLINED

            Case Constants.CONST_CURRENT_STATUS_CAD_APPROVED

            Case Constants.CONST_CURRENT_STATUS_AUDIT

            Case Constants.CONST_CURRENT_STATUS_AUDIT_FAILED

            Case Constants.CONST_CURRENT_STATUS_PAYOUT

            Case Constants.CONST_CURRENT_STATUS_COMPLETED

                'Case Constants.CONST_CURRENT_STATUS_EXPORTED

            Case Else
                Cursor.Current = Cursors.WaitCursor
                Try
                    'test there is data rows
                    If Not BudgetBindingSource.Current Is Nothing Then
                        'Get Selected Row
                        Dim SelectedRowView As Data.DataRowView
                        Dim SelectedRow As EnquiryWorkSheetDataSet.BudgetRow
                        SelectedRowView = CType(BudgetBindingSource.Current, System.Data.DataRowView)
                        SelectedRow = CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.BudgetRow)
                        'Get Income Value
                        Dim IncomeFieldVal As Decimal = CDec(SelectedRow.IncomeValue)
                        'Get Expense value
                        Dim ExpenseFieldVal As Decimal = CDec(SelectedRow.ExpenseValue)
                        'Test Values
                        If IncomeFieldVal > ExpenseFieldVal Then
                            'Launch Add Income Form
                            Dim ModBudgetFrm As New AddBudgetIncomeItemForm
                            ModBudgetFrm.PassVariable(thisEnquiryId, SelectedRow.BudgetId)
                            ' Show ModBudgetFrm as a modal dialog and determine if DialogResult = OK.
                            If ModBudgetFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                                'Call the function you used to populate the data grid..
                                Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                            End If
                            ModBudgetFrm.Dispose()
                        Else
                            'Launch Add Expense Form
                            Dim ModBudgetFrm As New AddBudgetExpenseItemForm
                            ModBudgetFrm.PassVariable(thisEnquiryId, SelectedRow.BudgetId)
                            ' Show ModBudgetFrm as a modal dialog and determine if DialogResult = OK.
                            If ModBudgetFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                                'Call the function you used to populate the data grid..
                                Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                            End If
                            ModBudgetFrm.Dispose()
                        End If
                        'Total incomes and expenses
                        TotalIncomeExpenses()
                    Else
                        'MsgBox("No Data Row selected." + vbCrLf + "Please select a Data Row")
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "No Data Row selected, Please select a Data Row!"
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
                Cursor.Current = Cursors.Default
        End Select

    End Sub



    Private Sub SetOvertimeRadioButtonState()
        'set the radio button states for Joint Name Credit Questions
        Dim QuestionYesSet As String = "rbOvertimeYes"
        Dim QuestionNoset As String = "rbOvertimeNo"
        Dim CheckState As String = "OvertimeState"
        'get State of Question from database
        OvertimeState = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item(CheckState)
        Try
            'Look through Controls Collection to find 'QuestionYesSet'
            Dim controls1() As Control = Me.Controls.Find(QuestionYesSet, True)
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton1 As RadioButton = DirectCast(controls1(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case OvertimeState
                    Case 0
                        ThisRadioButton1.Checked = False
                    Case 1
                        ThisRadioButton1.Checked = True
                    Case Else
                        ThisRadioButton1.Checked = False
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Try
            'Look through Controls Collection to find 'QuestionNoSet'
            Dim controls2() As Control = Me.Controls.Find(QuestionNoset, True)
            If controls2.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton2 As RadioButton = DirectCast(controls2(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case OvertimeState
                    Case 0
                        ThisRadioButton2.Checked = True
                    Case 1
                        ThisRadioButton2.Checked = False
                    Case Else
                        ThisRadioButton2.Checked = True
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '******************** End of Budget ************************
    '******************** Documents *********************************

    Private Sub lvDocuments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDocuments.DoubleClick
        Try
            Cursor.Current = Cursors.WaitCursor
            'Get String to WorkSheet Drive
            Dim worksheetDrive = Switch.GetWorksheetSharedFolder
            Dim filePath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
            Dim fileList As ListView.SelectedListViewItemCollection = lvDocuments.SelectedItems
            Dim item As ListViewItem
            Dim fileName As String
            For Each item In fileList
                fileName = item.SubItems(0).Text
                'MsgBox("Selected file: = " & filePath & fileName)
                'start program to open file
                System.Diagnostics.Process.Start(filePath & fileName)
            Next
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("The opening of Folder Files caused an exception:" & vbCrLf & ex.Message)
        End Try
    End Sub

    'Drag and drop events
    Private Sub lvDocuments_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lvDocuments.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'refresh
        Try
            'Clear the ListView
            lvDocuments.Items.Clear()
            'Create place to store files
            Dim aFiles() As String 'array type string
            aFiles = Directory.GetFiles(folderPath)
            'loop through aFiles
            For Each sfile As String In aFiles
                'create str array
                Dim str(3) As String
                Dim itm As ListViewItem
                'assign str array values
                Dim finfo As New FileInfo(sfile)
                If finfo.Exists Then
                    str(0) = finfo.Name
                    str(1) = finfo.CreationTime
                    If finfo.Length < 1000 Then
                        str(2) = finfo.Length.ToString & " bytes"
                    Else
                        str(2) = (finfo.Length / 1000).ToString & " kb"
                    End If
                    itm = New ListViewItem(str)
                    'itm.Checked = True
                    'add ListViewItem to ListView
                    lvDocuments.Items.Add(itm)
                End If
            Next sfile

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Loading Documents List generated an Exception Message: " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub lvDocuments_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lvDocuments.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFolder.Click
        'test the Worksheet Folder present switch
        If WkshtFolder = False Then
            'get WorksheetSharedFolder string
            Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
            Dim fname As String = WkshtStr & "\" & thisEnquiryCode
            'Determine whether the directory exists.
            If Directory.Exists(fname) Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
            Else
                Try
                    'create directory
                    IO.Directory.CreateDirectory(fname)
                    'activate drag and drop
                    Me.lvDocuments.AllowDrop = True
                    'refresh form
                    FormRefresh()
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
                Catch ex As System.IO.DirectoryNotFoundException
                    ' Let the user know that the directory did not exist.
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

            End If
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        End If
    End Sub

    Private Sub lvDocuments_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvDocuments.MouseDown
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            If lvDocuments.SelectedIndices.Count > 0 Then
                'launch contextMenu
                Dim contextMenu1 As New ContextMenu()
                Dim addedMenu As System.Windows.Forms.MenuItem
                addedMenu = contextMenu1.MenuItems.Add("rename")
                AddHandler addedMenu.Click, AddressOf renameEventHandler
                lvDocuments.ContextMenu = contextMenu1
            Else
                'lvDocuments.ContextMenu = Nothing
            End If
        End If
    End Sub

    Private Sub renameEventHandler(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Cursor.Current = Cursors.WaitCursor
            'Get String to WorkSheet Drive
            Dim worksheetDrive = Switch.GetWorksheetSharedFolder
            Dim filePath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
            Dim fileList As ListView.SelectedListViewItemCollection = lvDocuments.SelectedItems
            Dim item As ListViewItem
            Dim fileName As String = String.Empty
            Dim nFileName As String = String.Empty
            For Each item In fileList
                fileName = item.SubItems(0).Text
                'MsgBox("Selected file: = " & filePath & fileName)
                'load a modal form to rename the file
                Dim renameFileFrm As New frmRenameFile
                renameFileFrm.PassVariable(filePath & fileName)
                renameFileFrm.ShowDialog(Me)
                If renameFileFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                    nFileName = renameFileFrm.newFileName
                    My.Computer.FileSystem.RenameFile(filePath & fileName, nFileName)
                    renameFileFrm.Dispose()
                Else
                    'do nothing
                    renameFileFrm.Dispose()
                End If
            Next

            Cursor.Current = Cursors.WaitCursor
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("The renaming of file caused an exception:" & vbCrLf & ex.Message)
        End Try

        FormRefresh()
    End Sub

    ''' <summary>
    ''' Check record exists in Due Diligence table
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsExistsDueDiligenceRecordForEnquiry(enquiryId As Integer) As TrackResult
        Dim retResult As New TrackResult
        retResult.Status = False
        Dim dt As EnquiryWorkSheetDataSet.DueDiligenceDataTable
        dt = Me.TableAdapterManager.DueDiligenceTableAdapter.GetDataByEnquiryId(enquiryId)

        If dt IsNot Nothing Then
            If dt.Rows.Count > 0 Then
                retResult.Status = True
            End If
        End If

        Return retResult

    End Function

    ''' <summary>
    ''' check id DueDilgence table entry exists, if not then Insert with ThisEnquiryId to create record
    ''' if a record exists, edit record with latest loancode and clientNum
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateDueDiligenceRecordSaveLoanCode() As TrackResult
        'check id DueDilgence table entry exists, if not then Insert with ThisEnquiryId to create record
        Dim retResult As New TrackResult
        retResult.Status = False
        Dim insertResult As Integer
        Dim dueDiligenceDataTable As EnquiryWorkSheetDataSet.DueDiligenceDataTable
        Dim dueDiligenceDataRow As DataRow
        Try

            dueDiligenceDataTable = Me.TableAdapterManager.DueDiligenceTableAdapter.GetDataByEnquiryId(thisEnquiryId)
            If dueDiligenceDataTable IsNot Nothing OrElse dueDiligenceDataTable.Rows.Count = 0 Then
                insertResult = Me.TableAdapterManager.DueDiligenceTableAdapter.InsertWithEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId, txtbxLoanNum.Text) ', txtbxClientNum.Text, txtbxClientNum2.Text, txtClient1TypeId.Text, txtClient2TypeId.Text)
                If insertResult = 1 Then
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Successfully created New due Diligence record.   Status: Ready."
                    retResult.Status = True
                ElseIf insertResult = 0 Then
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Due Diligence record already exists.  Status: Ready."
                    retResult.Status = True
                Else
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Error creating Due Diligence record."
                    retResult.Status = False
                End If

            Else
                'Update if already exists
                dueDiligenceDataRow = dueDiligenceDataTable.Rows(0)
                If dueDiligenceDataRow IsNot Nothing Then
                    dueDiligenceDataRow.BeginEdit()
                    dueDiligenceDataRow("LoanCode") = txtbxLoanNum.Text
                    dueDiligenceDataRow.EndEdit()
                    Me.TableAdapterManager.DueDiligenceTableAdapter.Update(dueDiligenceDataTable)
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Due Diligence record saved successfully.  Status: Ready."
                    retResult.Status = True
                End If
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR: DueDiligenceTableAdapter caused an error" & vbCrLf & ex.Message)
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "Error saving Due Diligence record."
            retResult.Status = False
        End Try
        retResult.ErrorMessage = ToolStripStatusLabel1.Text

        Return retResult

    End Function

    '***************** End of Documents ************************
    '*************** Due Diligence / Audit **********************

    Private Sub btnAudit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAudit.Click
        'Change Enquiry status to Audit
        AuditProcess()

    End Sub

    ''' <summary>
    ''' Sets the Enquiry to Audit status and initializes
    ''' </summary>
    ''' <remarks></remarks>
    Private Async Sub AuditProcess()
        Cursor.Current = Cursors.WaitCursor
        'check id DueDilgence table entry exists, if not then Insert with ThisEnquiryId to create record
        Dim insertResult As Integer = -1

        Try
            Dim retResult As New TrackResult
            retResult.Status = False
            Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
            If EnquiryWorkSheetDataSet.DueDiligence.Rows.Count = 0 Then
                'no duediligence record
                insertResult = Me.TableAdapterManager.DueDiligenceTableAdapter.InsertWithEnquiryId(Me.EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId, IIf(txtbxLoanNum.Text Is Nothing, "", txtbxLoanNum.Text))
            Else
                'Due Diligence record already exists
                insertResult = 0
                'Status may have been reset, if payout table exists then set payout.payoutComplete date to Null
                'to avoid concurrency error in btnSaveFile
                Dim payoutDataTable As EnquiryWorkSheetDataSet.PayoutDataTable
                payoutDataTable = Me.TableAdapterManager.PayoutTableAdapter.GetDataByEnquiryId(thisEnquiryId)
                If payoutDataTable.Rows.Count > 0 Then
                    Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                    payoutRow = payoutDataTable(0)
                    payoutRow.BeginEdit()
                    Dim index As Integer = payoutDataTable.Columns.IndexOf("PayoutCompleted")
                    payoutRow.SetField(index, DBNull.Value)

                    Try
                        Me.Validate()
                        Me.PayoutBindingSource.EndEdit()
                        Me.PayoutTableAdapter.Update(payoutDataTable)
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("ERROR: Updating of Payout fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
                    End Try
                End If
            End If

            If insertResult = 1 Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Successfully created New due Diligence record.   Status: Ready."
            ElseIf insertResult = 0 Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Due Diligence record already exists.  Status: Ready."
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error creating Due Diligence record."
            End If

            'Update the Loan Summary in Audit: added 21/08/2017 by Christopher
            If insertResult = 0 Or insertResult = 1 Then
                'Check is possible to GetLoanSummary in AppSettings
                Dim systemCanGetLoanSummary As Boolean
                Dim thisAppsettings As New AppSettings
                systemCanGetLoanSummary = thisAppsettings.SystemCanGetLoanSummary
                If systemCanGetLoanSummary = True Then
                    'Get values form the OAC
                    Dim retLoanSumResult As New OacLoanSummaryResponse
                    Dim onlineApplicationService As New OnlineApplication
                    retLoanSumResult = onlineApplicationService.GetLoanSummary(thisApplicationCode)
                    If retLoanSumResult.Exists = False Then
                        MessageBox.Show(retLoanSumResult.ErrorMessage, retLoanSumResult.StatusMessage)
                    Else
                        If retLoanSumResult.ErrorCode > 0 Then
                            'error returned
                            MessageBox.Show(retLoanSumResult.ErrorMessage, retLoanSumResult.StatusMessage)
                        Else
                            'no error returned, update DueDiligence table
                            If Not DueDiligenceBindingSource.Current Is Nothing Then
                                Try
                                    Dim dueDilRow As EnquiryWorkSheetDataSet.DueDiligenceRow
                                    dueDilRow = Me.EnquiryWorkSheetDataSet.DueDiligence(0)
                                    dueDilRow.BeginEdit()
                                    dueDilRow.CashPrice_Advance = If(IsDBNull(retLoanSumResult.CashPrice), 0, retLoanSumResult.CashPrice)
                                    dueDilRow.OtherAdvance = If(IsDBNull(retLoanSumResult.OtherFundsAdvanced), 0, retLoanSumResult.OtherFundsAdvanced)
                                    dueDilRow.Deposit = If(IsDBNull(retLoanSumResult.CashDeposit), 0, retLoanSumResult.CashDeposit)
                                    dueDilRow.Brokerage = If(IsDBNull(retLoanSumResult.Brokerage), 0, retLoanSumResult.Brokerage)
                                    dueDilRow.TradeIn = If(IsDBNull(retLoanSumResult.TradeIn), 0, retLoanSumResult.TradeIn)
                                    dueDilRow.LoanRetention = If(IsDBNull(retLoanSumResult.LoanRetentionRate), 0, retLoanSumResult.LoanRetentionRate)
                                    dueDilRow.YFLFees = If(IsDBNull(retLoanSumResult.YFLFees), 0, retLoanSumResult.YFLFees)
                                    dueDilRow.YFLInsurances = If(IsDBNull(retLoanSumResult.YFLInsurances), 0, retLoanSumResult.YFLInsurances)
                                    dueDilRow.ProductCommission = If(IsDBNull(retLoanSumResult.ProductMargin), 0, retLoanSumResult.ProductMargin)
                                    dueDilRow.InterestCommission = If(IsDBNull(retLoanSumResult.InterestCommission), 0, retLoanSumResult.InterestCommission)
                                    dueDilRow.CommRetention = If(IsDBNull(retLoanSumResult.CommissionRetentionRate), 0, retLoanSumResult.CommissionRetentionRate)
                                    Me.Validate()
                                    Me.DueDiligenceBindingSource.EndEdit()
                                    Me.DueDiligenceTableAdapter.Update(Me.EnquiryWorkSheetDataSet.DueDiligence)

                                Catch ex As Exception
                                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                    MsgBox("ERROR: Updating of DueDiligence fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
                                End Try

                            End If
                        End If
                    End If

                Else
                    'Do not have permission to GetLoanSummary
                End If

            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR: DueDiligenceTableAdapter caused an error" & vbCrLf & ex.Message)
        End Try

        'add tabPage
        If Not tbctrlAppDetails.Contains(tabDueDiligence) Then tbctrlAppDetails.TabPages.Add(tabDueDiligence)
        'change Current Status and create comment
        'leave if bindingsource.current is nothing
        Try
            If Not EnquiryBindingSource.Current Is Nothing Then
                Cursor.Current = Cursors.WaitCursor
                'update fields
                Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                EnquiryRow.BeginEdit()
                'update Current Status
                EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_AUDIT '"Audit"
                EnquiryRow.CurrentStatusSetDate = DateTime.Now
                'Set Original Enquiry Time
                Dim OriginalTime As DateTime
                OriginalTime = EnquiryRow.DateTime
                'calculate time difference for 30min status
                Dim StatusTimeDiff As Integer
                StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
                If StatusTimeDiff < 30 Then
                    EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_AUDIT '"Audit"
                End If

                'create log note
                Dim CommentStr As String
                CommentStr = "Status change to Audit"
                Dim commentId As Integer
                commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

                'save changes
                Me.Validate()
                EnquiryBindingSource.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                'change buttom visibility
                btnAudit.Visible = False
                btnAudit.Enabled = False
                'incase form has been reset
                btnExportFiles.Visible = False
                btnExportFiles.Enabled = False
                'change buttom enabled
                btnPassedAudit.Visible = True
                btnPassedAudit.Enabled = True
                'set OAC Status
                'Check is possible to Change Remote Status
                Dim systemCanChangeRemoteStatus As Boolean
                Dim retTTResult As New TrackResult
                Dim onlineApplicationService As New OnlineApplication
                Dim thisAppsettings As New AppSettings
                Dim note As String = String.Empty
                systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                If systemCanChangeRemoteStatus = True Then
                    'Change Status in OAC
                    Dim actionType As Integer = OACWebService.ApplicationActionType.Audit
                    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                    If retTTResult.Status = False Then 'error condition
                        commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                        MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                    'set focus
                    tbctrlAppDetails.SelectedTab = tabDueDiligence
                    'form refresh
                    FormRefresh()
                    Cursor.Current = Cursors.Default
                End If
            Else
                'no current record in binding source - do not proceed
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No data entered, please enter data!"
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR: btnAudit caused an error" & vbCrLf & ex.Message)
        End Try

        Cursor.Current = Cursors.Default
    End Sub





#Region "Audit Checkboxes"

    Private Sub ckbxFinPowerOpened_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxFinPowerOpened.CheckedChanged
        If ckbxFinPowerOpened.CheckState = CheckState.Checked Then
            ckbxfinPowerWithdrawal.Enabled = False
        Else
            ckbxfinPowerWithdrawal.Enabled = True
        End If
    End Sub

    Private Sub ckbxfinPowerWithdrawal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxfinPowerWithdrawal.CheckedChanged
        If ckbxfinPowerWithdrawal.CheckState = CheckState.Checked Then
            ckbxFinPowerOpened.Enabled = False
        Else
            ckbxFinPowerOpened.Enabled = True
        End If
    End Sub

    Private Sub ckbxDDinPlace_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxDDinPlace.CheckedChanged
        If ckbxDDinPlace.CheckState = CheckState.Checked Then
            ckbxWageDeduction.Enabled = False
            ckbxPayBook.Enabled = False
            ckbxClientSetup.Enabled = False
            txtbxDDShtNum.Enabled = False
        Else
            ckbxWageDeduction.Enabled = True
            ckbxPayBook.Enabled = True
            ckbxClientSetup.Enabled = True
            txtbxDDShtNum.Enabled = True
        End If
    End Sub

    Private Sub ckbxPayBook_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxPayBook.CheckedChanged
        If ckbxPayBook.CheckState = CheckState.Checked Then
            ckbxDDinPlace.Enabled = False
            ckbxWageDeduction.Enabled = False
            ckbxClientSetup.Enabled = False
            txtbxDDShtNum.Enabled = False
        Else
            ckbxDDinPlace.Enabled = True
            ckbxWageDeduction.Enabled = True
            ckbxClientSetup.Enabled = True
            txtbxDDShtNum.Enabled = True
        End If
    End Sub

    Private Sub ckbxClientSetup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxClientSetup.CheckedChanged
        If ckbxClientSetup.CheckState = CheckState.Checked Then
            ckbxDDinPlace.Enabled = False
            ckbxPayBook.Enabled = False
            ckbxWageDeduction.Enabled = False
            txtbxDDShtNum.Enabled = False
        Else
            ckbxDDinPlace.Enabled = True
            ckbxPayBook.Enabled = True
            ckbxWageDeduction.Enabled = True
            txtbxDDShtNum.Enabled = True
        End If
    End Sub

    Private Sub ckbxWageDeduction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxWageDeduction.CheckedChanged
        If ckbxWageDeduction.CheckState = CheckState.Checked Then
            ckbxDDinPlace.Enabled = False
            ckbxPayBook.Enabled = False
            ckbxClientSetup.Enabled = False
            txtbxDDShtNum.Enabled = False
        Else
            ckbxDDinPlace.Enabled = True
            ckbxPayBook.Enabled = True
            ckbxClientSetup.Enabled = True
            txtbxDDShtNum.Enabled = True
        End If
    End Sub

    Private Sub txtbxDDShtNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxDDShtNum.TextChanged
        If txtbxDDShtNum.Text.Length > 0 Then
            ckbxDDinPlace.Enabled = False
            ckbxWageDeduction.Enabled = False
            ckbxPayBook.Enabled = False
            ckbxClientSetup.Enabled = False
        Else
            ckbxDDinPlace.Enabled = True
            ckbxWageDeduction.Enabled = True
            ckbxPayBook.Enabled = True
            ckbxClientSetup.Enabled = True
        End If
    End Sub

#End Region

    Private Async Sub btnPassedAudit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPassedAudit.Click
        'Due Diligence Audit Passed
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
        Dim insertResult As Integer = 2

        '######################################################
        'Validate Audit fields
        '######################################################
        If Not String.IsNullOrEmpty(CheckAuditFields()) Then
            MessageBox.Show(CheckAuditFields())
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "Status: Failed Audit Validation. Ready."
            Exit Sub
        ElseIf Not Me.DueDiligenceBindingSource.Current Is Nothing Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Status: passed Payout validation"
            Application.DoEvents()
            'update Due Diligence
            Me.Validate()
            Me.DueDiligenceBindingSource.EndEdit()
            Me.DueDiligenceTableAdapter.Update(Me.EnquiryWorkSheetDataSet.DueDiligence)
            '######################################################
            'Validate database due-diligence values
            '######################################################
            If Not String.IsNullOrEmpty(CheckDueDiligenceData()) Then
                MsgBox(CheckDueDiligenceData())
            Else
                '######################################################
                'Check finPower Client Numbers in customer db table
                'check id Payout table entry exists, if not then Insert with ThisEnquiryId to create record
                '######################################################
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    Try
                        insertResult = Me.TableAdapterManager.PayoutTableAdapter.InsertWithEnquiryId(Me.EnquiryWorkSheetDataSet.Payout, thisEnquiryId)
                        If insertResult = 1 Then
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Successfully created New Payout record.   Status: Ready."
                        ElseIf insertResult = 0 Then
                            'Payout record already exists
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Payout record already exists.    Status: Ready."
                        Else
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Error creating Payout record."
                        End If
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox("ERROR: PayoutTableAdapter caused an error" & vbCrLf & ex.Message)
                    End Try
                    If insertResult = 1 Or insertResult = 0 Then
                        'add tabPage
                        If Not tbctrlAppDetails.Contains(tabPayout) Then tbctrlAppDetails.TabPages.Add(tabPayout)
                    End If
                    '######################################################
                    'set OAC Status, check is possible to Change Remote Status
                    '######################################################
                    Dim systemCanChangeRemoteStatus As Boolean
                    Dim thisAppsettings As New AppSettings
                    systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                    If systemCanChangeRemoteStatus = True Then
                        Dim retTTResult As New TrackResult
                        Dim onlineApplicationService As New OnlineApplication
                        Dim note As String = String.Empty
                        Dim commentId As Integer
                        'Change Status in OAC
                        Dim actionType As Integer = OACWebService.ApplicationActionType.Payout
                        retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                        If retTTResult.Status = False Then 'error condition
                            commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                            MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If

                Else
                    '######################################################
                    'Finance facilty not handled here, handled by btnExportFiles
                    '######################################################
                    Call btnExportFiles_Click(Nothing, Nothing)
                End If

                If insertResult = 1 Or insertResult = 0 Then
                    '######################################################
                    'change Current Status and create comment
                    '######################################################
                    Dim rowView2 As DataRowView
                    'leave if bindingsource.current is nothing
                    If Not EnquiryBindingSource.Current Is Nothing Then
                        Dim OriginalTime As DateTime
                        Dim StatusTimeDiff As Integer
                        Dim newCurrentStatus As String = String.Empty
                        rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'update fields
                        Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                        EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                        EnquiryRow.BeginEdit()
                        EnquiryRow.CurrentStatusSetDate = DateTime.Now
                        'Set Original Enquiry Time
                        OriginalTime = EnquiryRow.DateTime
                        'calculate time difference for 30min status
                        StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
                        'update Current Status
                        EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT
                        newCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT
                        If StatusTimeDiff < 30 Then
                            EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_PAYOUT
                        End If
                        'save changes
                        Me.Validate()
                        EnquiryBindingSource.EndEdit()
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                        'create log note
                        Dim CommentStr As String
                        CommentStr = "VERIFICATION: I verify that the Application has passed the Audit" & vbCrLf & "Status change to " & newCurrentStatus
                        Dim commentId As Integer
                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)
                        'change buttom visibility
                        btnPassedAudit.Visible = False
                        btnPassedAudit.Enabled = False
                        'disable Due Diligence Controls
                        enableAuditControls(False)
                        'set focus
                        tbctrlAppDetails.SelectedTab = tabPayout
                        'change buttom enabled
                        btnExportFiles.Visible = True
                        btnExportFiles.Enabled = True

                        'form refresh
                        FormRefresh()

                    Else
                        'no current record in binding source - do not proceed
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                    End If

                End If

            End If


        End If
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub btnAuditUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuditUpdate.Click
        'code for Audit Update button
        Try
            Me.Validate()
            Me.DueDiligenceBindingSource.EndEdit()
            Me.DueDiligenceTableAdapter.Update(Me.EnquiryWorkSheetDataSet.DueDiligence)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Due Diligence Update Successful.   Status: Ready."
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR: btnAuditUpdate caused an error." & vbCrLf & ex.Message)
        End Try
    End Sub

#Region "Loan Breakdown txtbx Lost Focus"
    ''' <summary>
    ''' Assign values to the LoanBreakdown totals (TotalDrawdown and AmountFinanced)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoanBreakdownTotals()
        'Page scope variables
        'TotalDrawdown As Decimal (Audit tab)
        'Refinance As Decimal (Audit tab)
        'AmountFinanced As Decimal (Audit tab)
        'drawdownPaidOut As Decimal (Audit tab)
        'drawdownRetention As Decimal (Audit tab)
        'TotalDisburseAmt As Decimal (Audit tab)


        Dim LoanCashPrice As Decimal = CDec(If(String.IsNullOrEmpty(txtbxCashPrice.Text), "0", txtbxCashPrice.Text))
        Dim LoanOther As Decimal = CDec(If(String.IsNullOrEmpty(txtbxOther.Text), "0", txtbxOther.Text))
        Dim LoanTradeIn As Decimal = CDec(If(String.IsNullOrEmpty(txtTradeIn.Text), "0", txtTradeIn.Text))
        Refinance = CDec(If(String.IsNullOrEmpty(txtbxRefinance.Text), "0", txtbxRefinance.Text))
        Dim LoanFees As Decimal = CDec(If(String.IsNullOrEmpty(txtbxFees.Text), "0", txtbxFees.Text))
        Dim LoanInsurance As Decimal = CDec(If(String.IsNullOrEmpty(txtbxInsurance.Text), "0,", txtbxInsurance.Text))
        Dim Brokerage As Decimal = CDec(If(String.IsNullOrEmpty(txtBrokerage.Text), "0", txtBrokerage.Text))
        Dim LoanRetention As Decimal = CDec(If(String.IsNullOrEmpty(txtLoanRetention.Text), "0", txtLoanRetention.Text))

        Try
            If txtbxDeposit.Visible = True Then
                Dim LoanDeposit As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDeposit.Text), "0", txtbxDeposit.Text))
                TotalDrawdown = LoanCashPrice + LoanOther + Brokerage - LoanDeposit - LoanTradeIn
            Else
                TotalDrawdown = LoanCashPrice + LoanOther + Brokerage - LoanTradeIn
            End If
            lblDrawdownValue.Text = TotalDrawdown.ToString("C")
            drawdownRetention = Decimal.Round(TotalDrawdown * LoanRetention / 100, 2)
            lblDrawdownRetentionValue.Text = drawdownRetention.ToString("C")
            drawdownPaidOut = TotalDrawdown - drawdownRetention
            lblDrawdownPaidOutValue.Text = drawdownPaidOut.ToString("C")
            AmountFinanced = TotalDrawdown + Refinance + LoanFees + LoanInsurance
            lblFinancedValue.Text = Format(AmountFinanced, "C")

            If Not drawdownPaidOut = TotalDisburseAmt Then
                lblDisburseWarning.Visible = True
            Else
                lblDisburseWarning.Visible = False
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Loan Breakdown caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub


    Private Sub txtbxCashPrice_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxCashPrice.LostFocus

        If txtbxCashPrice.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxCashPrice.Text
            txtbxCashPrice.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If

    End Sub

    Private Sub txtbxOther_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxOther.LostFocus

        If txtbxOther.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxOther.Text
            txtbxOther.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If
    End Sub

    Private Sub txtTradeIn_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTradeIn.LostFocus

        If txtTradeIn.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtTradeIn.Text
            txtTradeIn.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If
    End Sub

    Private Sub txtbxDeposit_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDeposit.LostFocus

        If txtbxDeposit.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDeposit.Text
            txtbxDeposit.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If

    End Sub


    Private Sub txtbxRefinance_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxRefinance.LostFocus

        If txtbxRefinance.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxRefinance.Text
            txtbxRefinance.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If
    End Sub

    Private Sub txtbxFees_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxFees.LostFocus

        If txtbxFees.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxFees.Text
            txtbxFees.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If
    End Sub

    Private Sub txtbxInsurance_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxInsurance.LostFocus

        If txtbxInsurance.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxInsurance.Text
            txtbxInsurance.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If
    End Sub

    Private Sub txtBrokerage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBrokerage.LostFocus

        If txtBrokerage.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtBrokerage.Text
            txtBrokerage.Text = [String].Format("{0:n2}", tmpDecimal)
            LoanBreakdownTotals()
        End If
    End Sub

    'txtLoanRetention
    Private Sub txtLoanRetention_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanRetention.LostFocus

        If txtLoanRetention.Text.Length > 0 Then
            Dim tmpInt As Decimal
            tmpInt = txtLoanRetention.Text
            txtLoanRetention.Text = [String].Format("{0:n2}", tmpInt)
            LoanBreakdownTotals()
        End If
    End Sub
#End Region

#Region "Dealer commissions txtbx lost focus"
    Private Sub DealerCommissionTotals()
        'Page scope variables
        'DealerCommission As Decimal - Total Dealer Commission (Audit tab)
        'CommPaid2Dealer As Decimal
        'CommPaid2Retention As Decimal

        Dim ProductComm As Decimal = CDec(If(String.IsNullOrEmpty(txtProductComm.Text), "0", txtProductComm.Text))
        Dim InterestComm As Decimal = CDec(If(String.IsNullOrEmpty(txtInterestComm.Text), "0", txtInterestComm.Text))
        Dim CommRetention As Decimal = CDec(If(String.IsNullOrEmpty(txtCommRetention.Text), "0", txtCommRetention.Text))
        CommPaid2Retention = Decimal.Round((ProductComm + InterestComm) * CommRetention / 100, 2)
        CommPaid2Dealer = ProductComm + InterestComm - CommPaid2Retention

        Try
            If txtProductComm.Text.Length > 0 Or txtInterestComm.Text.Length > 0 Then
                DealerCommission = ProductComm + InterestComm
                lblTotalCommValue.Text = DealerCommission.ToString("C")
                lblCommPaid2DealerValue.Text = CommPaid2Dealer.ToString("C")
                lblCommPaid2RetentionValue.Text = CommPaid2Retention.ToString("C")
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show("Dealer commissions caused an error:" & vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub txtProductComm_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductComm.LostFocus

        If txtProductComm.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtProductComm.Text
            txtProductComm.Text = [String].Format("{0:n2}", tmpDecimal)
            DealerCommissionTotals()
        End If
    End Sub

    Private Sub txtInterestComm_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInterestComm.LostFocus

        If txtInterestComm.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtInterestComm.Text
            txtInterestComm.Text = [String].Format("{0:n2}", tmpDecimal)
            DealerCommissionTotals()
        End If
    End Sub

    Private Sub txtCommRetention_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCommRetention.LostFocus

        If txtCommRetention.Text.Length > 0 Then
            Dim tmpInt As Decimal
            tmpInt = txtCommRetention.Text
            txtCommRetention.Text = [String].Format("{0:n2}", tmpInt)
            DealerCommissionTotals()
        End If
    End Sub

#End Region

#Region "Disbursements txtbx LostFocus"

    Private Sub DisbursementsTotals()
        Dim DisburseAmt1 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDisburseAmt1.Text), "0", txtbxDisburseAmt1.Text))
        Dim DisburseAmt2 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDisburseAmt2.Text), "0", txtbxDisburseAmt2.Text))
        Dim DisburseAmt3 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDisburseAmt3.Text), "0", txtbxDisburseAmt3.Text))
        Dim DisburseAmt4 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDisburseAmt4.Text), "0", txtbxDisburseAmt4.Text))
        Dim DisburseAmt5 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDisburseAmt5.Text), "0", txtbxDisburseAmt5.Text))
        Dim DisburseAmt6 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDisburseAmt6.Text), "0", txtbxDisburseAmt6.Text))
        TotalDisburseAmt = DisburseAmt1 + DisburseAmt2 + DisburseAmt3 + DisburseAmt4 + DisburseAmt5 + DisburseAmt6
        lblDisburseAmtTotal.Text = TotalDisburseAmt.ToString("C")
        If Not drawdownPaidOut = TotalDisburseAmt Then
            lblDisburseWarning.Visible = True
        Else
            lblDisburseWarning.Visible = False
        End If
    End Sub

    Private Sub txtbxDisburseAmt1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDisburseAmt1.LostFocus
        If txtbxDisburseAmt1.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDisburseAmt1.Text
            txtbxDisburseAmt1.Text = [String].Format("{0:n2}", tmpDecimal)
            DisbursementsTotals()
        End If
    End Sub

    Private Sub txtbxDisburseAmt2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDisburseAmt2.LostFocus
        If txtbxDisburseAmt2.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDisburseAmt2.Text
            txtbxDisburseAmt2.Text = [String].Format("{0:n2}", tmpDecimal)
            DisbursementsTotals()
        End If
    End Sub

    Private Sub txtbxDisburseAmt3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDisburseAmt3.LostFocus
        If txtbxDisburseAmt3.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDisburseAmt3.Text
            txtbxDisburseAmt3.Text = [String].Format("{0:n2}", tmpDecimal)
            DisbursementsTotals()
        End If
    End Sub

    Private Sub txtbxDisburseAmt4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDisburseAmt4.LostFocus
        If txtbxDisburseAmt4.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDisburseAmt4.Text
            txtbxDisburseAmt4.Text = [String].Format("{0:n2}", tmpDecimal)
            DisbursementsTotals()
        End If
    End Sub

    Private Sub txtbxDisburseAmt5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDisburseAmt5.LostFocus
        If txtbxDisburseAmt5.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDisburseAmt5.Text
            txtbxDisburseAmt5.Text = [String].Format("{0:n2}", tmpDecimal)
            DisbursementsTotals()
        End If
    End Sub

    Private Sub txtbxDisburseAmt6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDisburseAmt6.LostFocus
        If txtbxDisburseAmt6.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxDisburseAmt6.Text
            txtbxDisburseAmt6.Text = [String].Format("{0:n2}", tmpDecimal)
            DisbursementsTotals()
        End If
    End Sub

#End Region

    '***********End of Due Diligence / Audit **********************
    '*************** Payout *****************************


    Private Sub btnPayoutUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayoutUpdate.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            Me.PayoutBindingSource.EndEdit()
            Me.PayoutTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Payout)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Payout Update Successful.   Status: Ready."
            'Set BackColors back to default color in case values written by code
            Util.ReSetControlsBackColor(gpbxPaid.Controls, False)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR: btnPayoutUpdate caused an error." & vbCrLf & ex.Message)
        End Try
    End Sub

#Region "PayOut txtbx LostFocus"

    Private Sub PayoutTotals()
        Dim LoanPayAmt1 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt1.Text), "0", txtbxPayAmt1.Text))
        Dim LoanPayAmt2 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt2.Text), "0", txtbxPayAmt2.Text))
        Dim LoanPayAmt3 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt3.Text), "0", txtbxPayAmt3.Text))
        Dim LoanPayAmt4 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt4.Text), "0", txtbxPayAmt4.Text))
        Dim LoanPayAmt5 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt5.Text), "0", txtbxPayAmt5.Text))
        Dim LoanPayAmt6 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt6.Text), "0", txtbxPayAmt6.Text))
        Dim DealerCommissionPaid As Decimal = CDec(If(String.IsNullOrEmpty(txtCommissionAmt.Text), "0", txtCommissionAmt.Text))
        Dim LoanHoldBack As Decimal = CDec(If(String.IsNullOrEmpty(txtbxHoldbackAmt.Text), "0", txtbxHoldbackAmt.Text))
        Dim LoanCommRetentionAmt As Decimal = CDec(If(String.IsNullOrEmpty(txtbxRetentionAmt.Text), "0", txtbxRetentionAmt.Text))
        Dim LoanRefinanceAmt1 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxRefinanceAmt1.Text), "0", txtbxRefinanceAmt1.Text))
        Dim LoanRefinanceAmt2 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxRefinanceAmt2.Text), "0", txtbxRefinanceAmt2.Text))

        TotalPaidOutAmt = LoanPayAmt1 + LoanPayAmt2 + LoanPayAmt3 + LoanPayAmt4 + LoanPayAmt5 + LoanPayAmt6 + DealerCommissionPaid + LoanHoldBack + LoanCommRetentionAmt
        lblTotalPaidOutAmt.Text = TotalPaidOutAmt.ToString("C")
        TotalFinAmt = LoanRefinanceAmt1 + LoanRefinanceAmt2 + TotalDrawdown
        lblTotalFinAmt.Text = TotalFinAmt.ToString("C")

        If Not TotalPaidOutAmt = TotalDisburseAmt + DealerCommission + drawdownRetention Then
            lblPaidoutWarning.Visible = True
        Else
            lblPaidoutWarning.Visible = False
        End If
    End Sub


    Private Sub txtbxPayAmt1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPayAmt1.LostFocus
        If txtbxPayAmt1.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxPayAmt1.Text
            txtbxPayAmt1.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()

    End Sub

    Private Sub txtbxPayAmt2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPayAmt2.LostFocus
        If txtbxPayAmt2.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxPayAmt2.Text
            txtbxPayAmt2.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub

    Private Sub txtbxPayAmt3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPayAmt3.LostFocus
        If txtbxPayAmt3.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxPayAmt3.Text
            txtbxPayAmt3.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub

    Private Sub txtbxPayAmt4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPayAmt4.LostFocus
        If txtbxPayAmt4.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxPayAmt4.Text
            txtbxPayAmt4.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub

    Private Sub txtbxPayAmt5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPayAmt5.LostFocus
        If txtbxPayAmt5.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxPayAmt5.Text
            txtbxPayAmt5.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub

    Private Sub txtbxPayAmt6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPayAmt6.LostFocus
        If txtbxPayAmt6.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxPayAmt6.Text
            txtbxPayAmt6.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub

    Private Sub txtCommissionAmt_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCommissionAmt.LostFocus
        If txtCommissionAmt.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtCommissionAmt.Text
            txtCommissionAmt.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub


    Private Sub txtbxHoldbackAmt_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxHoldbackAmt.LostFocus
        If txtbxHoldbackAmt.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxHoldbackAmt.Text
            txtbxHoldbackAmt.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub


    Private Sub txtbxRetentionAmt_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxRetentionAmt.LostFocus
        If txtbxRetentionAmt.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxRetentionAmt.Text
            txtbxRetentionAmt.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        PayoutTotals()
    End Sub

#End Region

#Region "Refinance txtbx LostFocus"

    Private Sub RefinanceTotals()
        Dim LoanPayAmt1 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt1.Text), "0", txtbxPayAmt1.Text))
        Dim LoanPayAmt2 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt2.Text), "0", txtbxPayAmt2.Text))
        Dim LoanPayAmt3 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt3.Text), "0", txtbxPayAmt3.Text))
        Dim LoanPayAmt4 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt4.Text), "0", txtbxPayAmt4.Text))
        Dim LoanPayAmt5 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt5.Text), "0", txtbxPayAmt5.Text))
        Dim LoanPayAmt6 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPayAmt6.Text), "0", txtbxPayAmt6.Text))
        Dim DealerCommissionPaid As Decimal = CDec(If(String.IsNullOrEmpty(txtCommissionAmt.Text), "0", txtCommissionAmt.Text))
        Dim LoanHoldBack As Decimal = CDec(If(String.IsNullOrEmpty(txtbxHoldbackAmt.Text), "0", txtbxHoldbackAmt.Text))
        Dim LoanCommRetentionAmt As Decimal = CDec(If(String.IsNullOrEmpty(txtbxRetentionAmt.Text), "0", txtbxRetentionAmt.Text))
        Dim LoanRefinanceAmt1 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxRefinanceAmt1.Text), "0", txtbxRefinanceAmt1.Text))
        Dim LoanRefinanceAmt2 As Decimal = CDec(If(String.IsNullOrEmpty(txtbxRefinanceAmt2.Text), "0", txtbxRefinanceAmt2.Text))

        TotalPaidOutAmt = LoanPayAmt1 + LoanPayAmt2 + LoanPayAmt3 + LoanPayAmt4 + LoanPayAmt5 + LoanPayAmt6 + DealerCommissionPaid + LoanHoldBack + LoanCommRetentionAmt
        lblTotalPaidOutAmt.Text = TotalPaidOutAmt.ToString("C")
        TotalFinAmt = TotalDrawdown + LoanRefinanceAmt1 + LoanRefinanceAmt2
        lblTotalFinAmt.Text = TotalFinAmt.ToString("C")

        'Audit refinance = Payout refinance?
        If Not (LoanRefinanceAmt1 + LoanRefinanceAmt2) = Refinance Then
            lblRefinanceWarning.Visible = True
        Else
            lblRefinanceWarning.Visible = False
        End If
    End Sub


    Private Sub txtbxRefinanceAmt1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxRefinanceAmt1.LostFocus
        If txtbxRefinanceAmt1.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxRefinanceAmt1.Text
            txtbxRefinanceAmt1.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        RefinanceTotals()
    End Sub

    Private Sub txtbxRefinanceAmt2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxRefinanceAmt2.LostFocus

        If txtbxRefinanceAmt2.Text.Length > 0 Then
            Dim tmpDecimal As Decimal
            tmpDecimal = txtbxRefinanceAmt2.Text
            txtbxRefinanceAmt2.Text = [String].Format("{0:n2}", tmpDecimal)
        End If
        RefinanceTotals()
    End Sub

#End Region



    ''' <summary>
    ''' Used to programmatically reset the enquiry status
    ''' </summary>
    ''' <param name="newStatus"></param>
    ''' <remarks>Currently only resets to Payout</remarks>
    Private Sub ResetStatus(ByVal newStatus As String)
        Cursor.Current = Cursors.WaitCursor
        If newStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
            enquiryRow.BeginEdit()
            'update Current Status
            enquiryRow.CurrentStatus = newStatus
            enquiryRow.CurrentStatusSetDate = DateTime.Now
            'Set Original Enquiry Time
            Dim originalTime As DateTime
            originalTime = enquiryRow.DateTime
            'calculate time difference for 30min status
            Dim statusTimeDiff As Integer
            statusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
            If statusTimeDiff < 30 Then
                enquiryRow.Status30mins = newStatus
            End If
            'create log note
            Dim commentStr As String
            commentStr = "Error condition: Status change to " & newStatus
            Dim commentId As Integer
            commentId = Util.SetComment(thisEnquiryId, "System", commentStr)

            'update Enquiry
            Try
                Me.Validate()
                Me.EnquiryBindingSource.EndEdit()
                Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("ERROR: Updating of Enquiry fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
            End Try
            'reset the payoutCompleted date
            If newStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then
                Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                payoutRow = Me.EnquiryWorkSheetDataSet.Payout(0)
                payoutRow.BeginEdit()
                Dim index As Integer = EnquiryWorkSheetDataSet.Payout.Columns.IndexOf("PayoutCompleted")
                payoutRow.SetField(index, DBNull.Value)
                Try
                    Me.Validate()
                    Me.PayoutBindingSource.EndEdit()
                    Me.PayoutTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Payout)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("ERROR: Updating of Payout fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
                End Try
            End If
        Else
            MessageBox.Show("ERROR: ResetStatus is not coded for " & newStatus, "newStatus Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


    End Sub

    Private Sub cmbxPayoutMonth_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbxPayoutMonth.SelectionChangeCommitted
        Dim senderComboBox As ComboBox = CType(sender, ComboBox)
        Dim selectedIndex As Integer = CInt(senderComboBox.SelectedIndex)
        Cursor.Current = Cursors.WaitCursor
        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then

            If Not cmbxPayoutMonth.SelectedItem Is Nothing And Not cmbxPayoutYear.SelectedItem Is Nothing Then
                Dim thisPayoutMonth As Integer = cmbxPayoutMonth.SelectedIndex + 1
                Dim thisPayoutYear As Integer = cmbxPayoutYear.SelectedItem
                Dim thisPayoutDate As New Date(CInt(thisPayoutYear), CInt(thisPayoutMonth), 1, 0, 0, 0)
                'MsgBox("Payout Month = " & ThisPayoutDate.ToString)
                Try
                    Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                    payoutRow = Me.EnquiryWorkSheetDataSet.Payout(0)
                    payoutRow.BeginEdit()
                    payoutRow.PayoutMonth = thisPayoutDate
                    PayoutBindingSource.EndEdit()
                    Me.PayoutTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Payout)
                    'MsgBox("Payout Month = " & thisPayoutDate.ToString)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("ERROR updating Payout" & vbCrLf & ex.Message.ToString)
                End Try

            End If

        End If

        Cursor.Current = Cursors.Default

        cmbxPayoutMonth.SelectedIndex = selectedIndex

    End Sub


    Private Sub cmbxPayoutYear_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbxPayoutYear.SelectionChangeCommitted
        Dim senderComboBox As ComboBox = CType(sender, ComboBox)
        Dim selectedIndex As Integer = CInt(senderComboBox.SelectedIndex)
        Cursor.Current = Cursors.WaitCursor
        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then

            If Not cmbxPayoutMonth.SelectedItem Is Nothing And Not cmbxPayoutYear.SelectedItem Is Nothing Then
                Dim thisPayoutMonth As Integer = cmbxPayoutMonth.SelectedIndex + 1
                Dim thisPayoutYear As Integer = cmbxPayoutYear.SelectedItem
                Dim thisPayoutDate As New Date(CInt(thisPayoutYear), CInt(thisPayoutMonth), 1, 0, 0, 0)
                'MsgBox("Payout Month = " & ThisPayoutDate.ToString)
                Try
                    Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                    payoutRow = Me.EnquiryWorkSheetDataSet.Payout(0)
                    payoutRow.BeginEdit()
                    payoutRow.PayoutMonth = thisPayoutDate
                    PayoutBindingSource.EndEdit()
                    Me.PayoutTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Payout)
                    'MsgBox("Payout Month = " & thisPayoutDate.ToString)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("ERROR updating Payout" & vbCrLf & ex.Message.ToString)
                End Try

            End If

        End If

        Cursor.Current = Cursors.Default

        cmbxPayoutYear.SelectedIndex = selectedIndex

    End Sub

    Private Sub dtpANZProcess_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpANZProcess.ValueChanged
        lblANZDate.ForeColor = SystemColors.ControlText
        ANZProcessDateSelected = True
        ToolTip1.SetToolTip(dtpANZProcess, "")
    End Sub

    Private Sub dtpPayAdvice_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPayAdvice.ValueChanged
        lblPayAdviceDate.ForeColor = SystemColors.ControlText
        PayAdviceDateSelected = True
    End Sub

    '*********** End of Payout *****************************

#Region "Printing"


    Private Sub PageSetupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageSetupToolStripMenuItem.Click
        Try
            'load page settings and display page setup dialog box
            PageSetupDialog1.PageSettings = PrintPageSettings
            PageSetupDialog1.ShowDialog()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            'display error message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CommentsOnlyToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CommentsOnlyToolStripMenuItem1.Click
        Try
            'Specify current page settings
            PrintDocument1.DefaultPageSettings = PrintPageSettings
            'Specify document for print preview dialog box and show
            finishedString = "Comments Report for Enquiry " & thisEnquiryCode & vbCrLf & vbCrLf & Util.GetEnquiryComments(thisEnquiryId) & vbCrLf & vbCrLf & "Printed by: " & LoggedinName & vbCrLf & DateTime.Now.ToString
            StringToPrint = finishedString
            PrintPreviewDialog1.Document = PrintDocument1
            PrintPreviewDialog1.ShowDialog()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            'Display error message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim numChars As Integer
        Dim numLines As Integer
        Dim stringForPage As String
        Dim strFormat As New StringFormat
        'Based on page setup, define drawable rectangle on page
        Dim rectDraw As New RectangleF(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, e.MarginBounds.Height)
        'Define area to determine how much text can fit on a page
        'Make height one line shorter to ensure text doesn't clip
        Dim sizeMeasure As New SizeF(e.MarginBounds.Width, e.MarginBounds.Height - PrintFont.GetHeight(e.Graphics))

        'When drawing long strings, break between words
        strFormat.Trimming = StringTrimming.Word
        'Compute how many chars and lines can fit based on sizeMeasure
        e.Graphics.MeasureString(StringToPrint, PrintFont, sizeMeasure, strFormat, numChars, numLines)
        'Compute string that will fit on page
        stringForPage = StringToPrint.Substring(0, numChars)
        'Print string on current page
        e.Graphics.DrawString(stringForPage, PrintFont, Brushes.Black, rectDraw, strFormat)
        'If there is more text, indicate there are more pages
        If numChars < StringToPrint.Length Then
            'Subtract text from string that has been printed
            StringToPrint = StringToPrint.Substring(numChars)
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            'All text has been printed, so restore string
            StringToPrint = finishedString
        End If
    End Sub

    Private Sub AllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AllToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'Construct print string
            'Dim enquiryPrintOut As Util.StringOut = ConstructPrintStringAll()
            Dim enquiryPrintOut As Util.StringOut = Util.GetPrintStringAll(thisEnquiryId)
            If enquiryPrintOut.Result Then
                finishedString = enquiryPrintOut.StringOut
            Else
                MessageBox.Show("Error creating Print String", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            StringToPrint = finishedString

            'MsgBox("String to print:" & vbCrLf & vbCrLf & StringToPrint)
            'Specify current page settings
            PrintDocument2.DefaultPageSettings = PrintPageSettings
            'print dialog
            PrintDialog1.UseEXDialog = True
            PrintDialog1.Document = PrintDocument2
            If PrintDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                'Specify document for print preview dialog box and show
                Try
                    PrintPreviewDialog1.Document = PrintDocument2
                    PrintPreviewDialog1.Icon = My.Resources.TT
                    PrintPreviewDialog1.ShowDialog()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MessageBox.Show("PrintPreviewDialog1 ERROR:" & vbCrLf & ex.Message)

                End Try

            End If

            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            'Display error message
            MessageBox.Show("Print Preview All ERROR:" & vbCrLf & ex.Message)

        End Try
    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim numChars As Integer
        Dim numLines As Integer
        Dim stringForPage As String
        Dim strFormat As New StringFormat

        'Based on page setup, define drawable rectangle on page
        Dim rectDraw As New RectangleF(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, e.MarginBounds.Height)
        'Define area to determine how much text can fit on a page
        'Make height one line shorter to ensure text doesn't clip
        Dim sizeMeasure As New SizeF(e.MarginBounds.Width, e.MarginBounds.Height - PrintFont.GetHeight(e.Graphics))
        'When drawing long strings, break between words
        strFormat.Trimming = StringTrimming.Word
        'Compute how many chars (numChars) and lines (numLines) can fit based on sizeMeasure
        e.Graphics.MeasureString(StringToPrint, PrintFont, sizeMeasure, strFormat, numChars, numLines)
        'Compute string that will fit on page
        stringForPage = StringToPrint.Substring(0, numChars)
        'Print string on current page
        e.Graphics.DrawString(stringForPage, PrintFont, Brushes.Black, rectDraw, strFormat)
        'If there is more text, indicate there are more pages
        If numChars < StringToPrint.Length Then
            'Subtract text from string that has been printed
            StringToPrint = StringToPrint.Substring(numChars)
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            'All text has been printed, so restore string
            StringToPrint = finishedString
        End If
    End Sub

    'Function ConstructPrintStringAll(Optional ByVal printPrintedBy As Boolean = True) As Util.StringOut
    '    Dim ourPrintString As Util.StringOut
    '    ourPrintString = New Util.StringOut
    '    ourPrintString.Result = False

    '    Try
    '        Dim constructedString As String = ""
    '        Dim auditPayBookSentSwitch As Boolean = False
    '        'Get Row in Enquiry dataset
    '        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
    '        enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '        'ThisEnquiryType = enquiryRow.Item("EnquiryType")
    '        thisEnquiryType = enquiryRow.EnquiryType
    '        If Not IsDBNull(enquiryRow.Item("mainCustomerType")) Then
    '            thisTypeOfMainCustomer = enquiryRow.Item("mainCustomerType")
    '        End If

    '        'Construct header using main string
    '        constructedString = vbCrLf & "Enquiry number: " & enquiryRow.Item("EnquiryCode") & _
    '        "                            Application number: " & enquiryRow.Item("ApplicationCode") & vbCrLf & enquiryRow.Item("ContactDisplayName") & vbCrLf & vbCrLf & _
    '        "Date: " & CStr(enquiryRow.Item("DateTime")) & "                    Current Status: " & enquiryRow.Item("CurrentStatus") & vbCrLf
    '        constructedString = constructedString & "Main client Type: " & MyEnums.GetDescription(DirectCast(thisTypeOfMainCustomer, MyEnums.mainCustomerType)) & vbCrLf
    '        constructedString = constructedString & "Reason for Loan: " & MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
    '        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '            constructedString = constructedString & "                       Amount of Loan: " & CStr(Format(enquiryRow.Item("LoanValue"), "C")) & vbCrLf
    '            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
    '                constructedString = constructedString & "Current Loan value: " & CStr(Format(enquiryRow.Item("CurrentLoanAmt"), "C")) & vbCrLf
    '            End If
    '            If Not IsDBNull(enquiryRow.Item("LoanPurpose")) Then
    '                constructedString = constructedString & "Loan purpose: " & enquiryRow.Item("LoanPurpose") & vbCrLf
    '            End If
    '        Else
    '            constructedString = constructedString & vbCrLf
    '        End If
    '        constructedString = constructedString & "Contact name: " & enquiryRow.Item("ContactTitle") & " " & enquiryRow.Item("ContactFirstName") & " " & enquiryRow.Item("ContactLastName") & vbCrLf
    '        constructedString = constructedString & "Suburb: " & enquiryRow.Item("ContactSuburb") & "          City: " & enquiryRow.Item("ContactCity") & vbCrLf
    '        constructedString = constructedString & "Contact number: " & enquiryRow.Item("ContactPhone") & vbCrLf
    '        constructedString = constructedString & "Contact email: " & enquiryRow.Item("ContactEmail") & vbCrLf

    '        '********** Loan *************************
    '        'If Not IsDBNull(enquiryRow.Item("LoanComments")) Then
    '        '    If enquiryRow.Item("LoanComments").length > 0 Then
    '        '        constructedString = constructedString & vbCrLf & "*****  LOAN COMMENTS: Progress and Status  *************************" & vbCrLf & enquiryRow.Item("LoanComments") & vbCrLf
    '        '    End If
    '        'End If

    '        'amended 13/07/2017
    '        constructedString = constructedString & vbCrLf & "*****  LOAN COMMENTS: Progress and Status  *************************" & vbCrLf & Util.GetEnquiryComments(thisEnquiryId) & vbCrLf

    '        '********** End of Loan ******************
    '        '********** Security *********************
    '        'If ThisLoanReason = Split Loan Drawdown or FinanceFacility, then NO Security info exits
    '        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '            Dim securityString As String
    '            securityString = vbCrLf & "*****  SECURITY  ************************************************************" & _
    '            vbCrLf & "Insurance and Security Comments: " & vbCrLf & enquiryRow.Item("SecurityComments") & "Security Items: " & vbCrLf
    '            'get Security Items      
    '            Dim securitySubString As String
    '            Dim securityItemRow As EnquiryWorkSheetDataSet.SecurityRow
    '            For Each securityItemRow In EnquiryWorkSheetDataSet.Security
    '                'contruct string
    '                securitySubString = "Description: " & securityItemRow.Item("Description") & vbCrLf & "Type: " & securityItemRow.Item("Type") & _
    '                "     Valuer: " & securityItemRow.Item("TodayValuer") & "    Value: " & CStr(Format(securityItemRow.Item("TodayValue"), "C")) & vbCrLf & _
    '                "Lending: " & securityItemRow.Item("LendingPercentage") & "%                  Lending Value: " & CStr(Format(securityItemRow.Item("LendingValue"), "C")) & _
    '                vbCrLf & "Valuation Approved? "
    '                If securityItemRow.Item("SecurityValued") = False Then
    '                    securitySubString = securitySubString & "No" & vbCrLf
    '                Else
    '                    securitySubString = securitySubString & "Yes" & vbCrLf
    '                End If
    '                If Not IsDBNull(securityItemRow.Item("Comments")) Then
    '                    If securityItemRow.Item("Comments").length > 0 Then
    '                        'If Not String.IsNullOrEmpty(SecurityItemRow.Item("Comments")) Then
    '                        'If SecurityItemRow.Item("Comments") <> "" Then
    '                        securitySubString = securitySubString & securityItemRow.Item("Comments") & vbCrLf
    '                    End If
    '                End If
    '                securitySubString = securitySubString & "Security Insured? "
    '                If securityItemRow.Item("SecurityInsured") = False Then
    '                    securitySubString = securitySubString & "No" & vbCrLf
    '                Else
    '                    securitySubString = securitySubString & "Yes" & vbCrLf
    '                End If
    '                If Not IsDBNull(securityItemRow.Item("InsuranceComments")) Then
    '                    If securityItemRow.Item("InsuranceComments").length > 0 Then
    '                        'If Not String.IsNullOrEmpty(SecurityItemRow.Item("InsuranceComments")) Then
    '                        'If SecurityItemRow.Item("InsuranceComments") <> "" Then
    '                        securitySubString = securitySubString & securityItemRow.Item("InsuranceComments") & vbCrLf
    '                    End If
    '                End If
    '                'Add to SecurityString
    '                securityString = securityString & securitySubString & vbCrLf
    '            Next
    '            securityString = securityString & "Total Assest Value:      " & CStr(Format(TotalAssetValue, "C")) & vbCrLf
    '            securityString = securityString & "Total Lending Available: " & CStr(Format(TotalLending, "C")) & "                    Lending Equity: " & CStr(Format(EquityValue, "C")) & vbCrLf
    '            'add security string to main string
    '            If securityString.Length > 0 Then
    '                constructedString = constructedString & securityString
    '            End If
    '        End If

    '        '********** End of Security **************
    '        '**********Budget ********************************
    '        'If ThisLoanReason = Split Loan Drawdown or FinanceFacility, then NO Budget info exits
    '        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '            Dim budgetString As String = String.Empty
    '            Select Case thisTypeOfMainCustomer
    '                Case MyEnums.mainCustomerType.Company, MyEnums.mainCustomerType.Trust, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
    '                    budgetString = vbCrLf & "*****  FINANCIALS **************************************************" & vbCrLf
    '                    'Financials
    '                    'Income and Expenses
    '                    Dim incomeExpensesString As String = "--------------------  Income and Expenses:" & vbCrLf
    '                    incomeExpensesString = "Income was established by previous years financials? "
    '                    If enquiryRow.Item("CompanyIncomePrevious") = True Then
    '                        incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
    '                    Else
    '                        incomeExpensesString = incomeExpensesString & "No" & vbCrLf
    '                    End If
    '                    incomeExpensesString = incomeExpensesString & "Income was established by year to date financials? "
    '                    If enquiryRow.Item("CompanyIncomeYTD") = True Then
    '                        incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
    '                    Else
    '                        incomeExpensesString = incomeExpensesString & "No" & vbCrLf
    '                    End If
    '                    incomeExpensesString = incomeExpensesString & "Income was established by GST returns? "
    '                    If enquiryRow.Item("CompanyIncomeGST") = True Then
    '                        incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
    '                    Else
    '                        incomeExpensesString = incomeExpensesString & "No" & vbCrLf
    '                    End If
    '                    incomeExpensesString = incomeExpensesString & "Income was established by income forcast? "
    '                    If enquiryRow.Item("CompanyIncomeForcast") = True Then
    '                        incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
    '                    Else
    '                        incomeExpensesString = incomeExpensesString & "No" & vbCrLf
    '                    End If
    '                    If Not IsDBNull(enquiryRow.Item("CompanyIncomeNotes")) Then
    '                        If enquiryRow.Item("CompanyIncomeNotes").length > 0 Then
    '                            incomeExpensesString = incomeExpensesString & "Past, present and future income is satisfactory because " & enquiryRow.Item("CompanyIncomeNotes") & vbCrLf
    '                        End If
    '                    End If
    '                    If Not IsDBNull(enquiryRow.Item("CompanyExpensesNotes")) Then
    '                        If enquiryRow.Item("CompanyExpensesNotes").length > 0 Then
    '                            incomeExpensesString = incomeExpensesString & "Past, present and future expenses are satisfactory because " & enquiryRow.Item("CompanyExpensesNotes") & vbCrLf
    '                        End If
    '                    End If
    '                    incomeExpensesString = incomeExpensesString & "Income and expenses are satisfactory? "
    '                    If enquiryRow.Item("CompanyIncomeExpensesSatis") = True Then
    '                        incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
    '                    Else
    '                        incomeExpensesString = incomeExpensesString & "No" & vbCrLf
    '                    End If
    '                    budgetString = budgetString & incomeExpensesString
    '                    'Salaries (Partnership and SoleTrader donot have salaries section)
    '                    If Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership And Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
    '                        Dim salariesString As String = "--------------------  Salaries:" & vbCrLf
    '                        If Not IsDBNull(enquiryRow.Item("CompanySalariesNotes")) Then
    '                            If enquiryRow.Item("CompanySalariesNotes").length > 0 Then
    '                                Select Case thisTypeOfMainCustomer
    '                                    Case MyEnums.mainCustomerType.Company
    '                                        salariesString = salariesString & "Salaries to directors, shareholders and owners are satisfactory because " & enquiryRow.Item("CompanySalariesNotes") & vbCrLf
    '                                    Case MyEnums.mainCustomerType.Trust
    '                                        salariesString = salariesString & "Salaries to trustees, beneficiaries are satisfactory because " & enquiryRow.Item("CompanySalariesNotes") & vbCrLf
    '                                        'Case MyEnums.mainCustomerType.Partnership
    '                                        '    salariesString = salariesString & "Salaries to partners are satisfactory because " & enquiryRow.Item("CompanySalariesNotes") & vbCrLf
    '                                End Select
    '                            End If
    '                        End If
    '                        salariesString = salariesString & "Salaries are satisfactory? "
    '                        If enquiryRow.Item("CompanySalariesSatis") = True Then
    '                            salariesString = salariesString & "Yes" & vbCrLf
    '                        Else
    '                            salariesString = salariesString & "No" & vbCrLf
    '                        End If
    '                        budgetString = budgetString & salariesString
    '                    End If
    '                    'Earnings before interest and taxes
    '                    Dim earningsString As String = "--------------------  Earnings before interest and taxes:" & vbCrLf
    '                    If Not IsDBNull(enquiryRow.Item("CompanyEarningsNotes")) Then
    '                        If enquiryRow.Item("CompanyEarningsNotes").length > 0 Then
    '                            Select Case thisTypeOfMainCustomer
    '                                Case MyEnums.mainCustomerType.Company
    '                                    earningsString = earningsString & "The company's performamce and ability to service YFL loan is satisfactory because " & enquiryRow.Item("CompanyEarningsNotes") & vbCrLf
    '                                Case MyEnums.mainCustomerType.Trust
    '                                    earningsString = earningsString & "The trust's performamce and ability to service YFL loan is satisfactory because " & enquiryRow.Item("CompanyEarningsNotes") & vbCrLf
    '                                Case MyEnums.mainCustomerType.Partnership
    '                                    earningsString = earningsString & "The partnerships's performamce and ability to service YFL loan is satisfactory because " & enquiryRow.Item("CompanyEarningsNotes") & vbCrLf
    '                            End Select

    '                        End If
    '                    End If
    '                    earningsString = earningsString & "Profit is satisfactory? "
    '                    If enquiryRow.Item("CompanyProfitSatis") = True Then
    '                        earningsString = earningsString & "Yes" & vbCrLf
    '                    Else
    '                        earningsString = earningsString & "No" & vbCrLf
    '                    End If
    '                    budgetString = budgetString & earningsString
    '                    'Financial Position
    '                    Dim positionString As String = "--------------------  Financial position:" & vbCrLf
    '                    If Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership And Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
    '                        positionString = positionString & "Asset to liabilities ratio is satisfactory? "
    '                        If enquiryRow.Item("CompanyAssetRatio") = True Then
    '                            positionString = positionString & "Yes" & vbCrLf
    '                        Else
    '                            positionString = positionString & "No" & vbCrLf
    '                        End If
    '                        If Not IsDBNull(enquiryRow.Item("CompanyDebtNotes")) Then
    '                            If enquiryRow.Item("CompanyDebtNotes").length > 0 Then
    '                                positionString = positionString & "The debt and asset to liabilities ratio is satisfactory because " & enquiryRow.Item("CompanyDebtNotes") & vbCrLf
    '                            End If
    '                        End If
    '                    End If
    '                    positionString = positionString & "Accounts payable are satisfactory and identified? "
    '                    If enquiryRow.Item("CompanyAcctsPaySatis") = True Then
    '                        positionString = positionString & "Yes" & vbCrLf
    '                    Else
    '                        positionString = positionString & "No" & vbCrLf
    '                    End If
    '                    If Not IsDBNull(enquiryRow.Item("CompanyAcctsPayNotes")) Then
    '                        If enquiryRow.Item("CompanyAcctsPayNotes").length > 0 Then
    '                            positionString = positionString & "The accounts payable are satisfactory because " & enquiryRow.Item("CompanyAcctsPayNotes") & vbCrLf
    '                        End If
    '                    End If
    '                    positionString = positionString & "GST returns and payments are up to date? "
    '                    If enquiryRow.Item("CompanyGSTUTD") = True Then
    '                        positionString = positionString & "Yes" & vbCrLf
    '                    Else
    '                        positionString = positionString & "No" & vbCrLf
    '                    End If
    '                    If Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership And Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
    '                        positionString = positionString & "PAYE returns and payments are up to date? "
    '                        If enquiryRow.Item("CompanyPayeUTD") = True Then
    '                            positionString = positionString & "Yes" & vbCrLf
    '                        Else
    '                            positionString = positionString & "No" & vbCrLf
    '                        End If
    '                    End If
    '                    positionString = positionString & "Income Tax returns and payments are up to date? "
    '                    If enquiryRow.Item("CompanyITUTD") = True Then
    '                        positionString = positionString & "Yes" & vbCrLf
    '                    Else
    '                        positionString = positionString & "No" & vbCrLf
    '                    End If
    '                    If Not IsDBNull(enquiryRow.Item("CompanyTaxesNotes")) Then
    '                        If enquiryRow.Item("CompanyTaxesNotes").length > 0 Then
    '                            positionString = positionString & "The taxes are satisfactory because " & enquiryRow.Item("CompanyTaxesNotes") & vbCrLf
    '                        End If
    '                    End If
    '                    budgetString = budgetString & positionString
    '                    'end of Company Financials

    '                Case MyEnums.mainCustomerType.Individual, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
    '                    budgetString = vbCrLf & "*****  BUDGET (per month)  *************************************************" & vbCrLf
    '                    If thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership Then
    '                        budgetString = "*****  Personal Incomes are to be combined to see if the Budget is sufficent for the partners to live on." & vbCrLf
    '                    End If
    '                    If thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
    '                        budgetString = "*****  Personal Income is derived from the income of the Sole Trader's business activities." & vbCrLf
    '                    End If
    '                    budgetString = budgetString & "Stated Net Income: " & CStr(Format(enquiryRow.Item("NetIncomeStated"), "C")) & _
    '                   "                         Stated Expenses: " & CStr(Format(enquiryRow.Item("ExpensesStated"), "C")) & vbCrLf & vbCrLf
    '                    'check questions
    '                    'contruct BudgetQuestionString
    '                    Dim budgetQuestionString As String
    '                    budgetQuestionString = "Is Partner's pay relevant? "
    '                    If enquiryRow.Item("BudgetQuestion1") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No                       "
    '                    ElseIf enquiryRow.Item("BudgetQuestion1") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes                      "
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & "                         "
    '                    End If
    '                    budgetQuestionString = budgetQuestionString & "Any other income? "
    '                    If enquiryRow.Item("BudgetQuestion2") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No" & vbCrLf
    '                    ElseIf enquiryRow.Item("BudgetQuestion2") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & vbCrLf
    '                    End If

    '                    budgetQuestionString = budgetQuestionString & "Are you paying child support? "
    '                    If enquiryRow.Item("BudgetQuestion3") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No               "
    '                    ElseIf enquiryRow.Item("BudgetQuestion3") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes              "
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & "                 "
    '                    End If
    '                    budgetQuestionString = budgetQuestionString & "Are you paying any fines? "
    '                    If enquiryRow.Item("BudgetQuestion4") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No" & vbCrLf
    '                    ElseIf enquiryRow.Item("BudgetQuestion4") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & vbCrLf
    '                    End If

    '                    budgetQuestionString = budgetQuestionString & "Are you paying any other loans? "
    '                    If enquiryRow.Item("BudgetQuestion5") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No              "
    '                    ElseIf enquiryRow.Item("BudgetQuestion5") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes             "
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & "                "
    '                    End If
    '                    budgetQuestionString = budgetQuestionString & "Is you partner paying any loans? "
    '                    If enquiryRow.Item("BudgetQuestion6") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No" & vbCrLf
    '                    ElseIf enquiryRow.Item("BudgetQuestion6") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & vbCrLf
    '                    End If

    '                    budgetQuestionString = budgetQuestionString & "Any hire purchase payments? "
    '                    If enquiryRow.Item("BudgetQuestion7") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No                 "
    '                    ElseIf enquiryRow.Item("BudgetQuestion7") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes                "
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & "                   "
    '                    End If
    '                    budgetQuestionString = budgetQuestionString & "Any Credit Card payments? "
    '                    If enquiryRow.Item("BudgetQuestion8") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No" & vbCrLf
    '                    ElseIf enquiryRow.Item("BudgetQuestion8") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & vbCrLf
    '                    End If

    '                    budgetQuestionString = budgetQuestionString & "Any insurance payments? "
    '                    If enquiryRow.Item("BudgetQuestion9") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No                        "
    '                    ElseIf enquiryRow.Item("BudgetQuestion9") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes                       "
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & "                          "
    '                    End If
    '                    budgetQuestionString = budgetQuestionString & "Any other expenses? "
    '                    If enquiryRow.Item("BudgetQuestion10") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No" & vbCrLf
    '                    ElseIf enquiryRow.Item("BudgetQuestion10") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & vbCrLf
    '                    End If

    '                    budgetQuestionString = budgetQuestionString & "Board/rent/mortgage? "
    '                    If enquiryRow.Item("BudgetQuestion11") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No                               "
    '                    ElseIf enquiryRow.Item("BudgetQuestion11") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes                              "
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & "                                 "
    '                    End If
    '                    budgetQuestionString = budgetQuestionString & "This YFL Loan? "
    '                    If enquiryRow.Item("BudgetQuestion12") = 1 Then
    '                        budgetQuestionString = budgetQuestionString & "No" & vbCrLf
    '                    ElseIf enquiryRow.Item("BudgetQuestion12") = 2 Then
    '                        budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
    '                    Else
    '                        budgetQuestionString = budgetQuestionString & vbCrLf
    '                    End If
    '                    'Add to BudgetString
    '                    budgetString = budgetString & budgetQuestionString & vbCrLf
    '                    'Budget items
    '                    Dim budgetSubString As String = ""
    '                    Dim budgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
    '                    For Each budgetItemRow In EnquiryWorkSheetDataSet.Budget
    '                        'contruct SubString
    '                        If Not IsDBNull(budgetItemRow.Item("Description")) Then
    '                            If budgetItemRow.Item("Description").length > 0 Then
    '                                budgetSubString = budgetSubString & "Description: " & budgetItemRow.Item("Description") & vbCrLf
    '                            End If
    '                        End If
    '                        If budgetItemRow.Item("ExpenseValue") > 0 Then
    '                            budgetSubString = budgetSubString & "Expense: " & CStr(Format(budgetItemRow.Item("ExpenseValue"), "C")) & vbCrLf
    '                        ElseIf budgetItemRow.Item("IncomeValue") > 0 Then
    '                            budgetSubString = budgetSubString & "Income: " & CStr(Format(budgetItemRow.Item("IncomeValue"), "C")) & vbCrLf
    '                        End If
    '                        If Not IsDBNull(budgetItemRow.Item("Comments")) Then
    '                            If budgetItemRow.Item("Comments").length > 0 Then
    '                                budgetSubString = budgetSubString & "Comments: " & budgetItemRow.Item("Comments") & vbCrLf
    '                            End If
    '                        End If
    '                        budgetSubString = budgetSubString & vbCrLf
    '                    Next
    '                    'Add to BudgetString
    '                    budgetString = budgetString & budgetSubString & vbCrLf
    '                    budgetString = budgetString & "Total Income: " & CStr(Format(IncomeTotal, "C")) & "                                   TotalExpenses: " & CStr(Format(ExpenseTotal, "C")) & vbCrLf
    '                    'Dependency
    '                    budgetString = budgetString & "Dependency: " & CStr(Format(enquiryRow.Item("BudgetDependencyClient"), "C")) & _
    '                    "          No: of Dependants: " & enquiryRow.Item("BudgetDependencyChildren") & "          Total Dependency: " & _
    '                    CStr(Format(enquiryRow.Item("BudgetDependencyTotal"), "C")) & vbCrLf & vbCrLf & "MARGIN = " & CStr(Format(BudgetMargin, "C")) & vbCrLf
    '                    'Added BudgetExplanation 12/11/2012
    '                    If Not IsDBNull(enquiryRow.Item("BudgetExplanation")) Then
    '                        If enquiryRow.Item("BudgetExplanation").length > 0 Then
    '                            budgetString = budgetString & "Budget Comments and Explanations: " & enquiryRow.Item("BudgetExplanation") & vbCrLf
    '                        End If
    '                    End If
    '            End Select
    '            If budgetString.Length > 0 Then
    '                constructedString = constructedString & budgetString
    '            End If

    '        Else
    '            'ThisEnquiryType = Split Loan Drawdown   
    '        End If
    '        '******* End of Budget ***********
    '        '********** Customer Info ******************
    '        Dim clientString As String = String.Empty
    '        'Check customer
    '        Dim custType As Integer
    '        Dim custId As Integer
    '        Dim custEnquiryType As Integer
    '        'Dim custName As String
    '        Dim thisCustString As String
    '        custCount = EnquiryCustomersBindingSource.Count
    '        If custCount > 0 Then
    '            'iterate through EnquiryCustomers to load a customer
    '            For Each row As DataRow In EnquiryWorkSheetDataSet.EnquiryCustomers.Rows
    '                thisCustString = vbCrLf & "*****  CUSTOMER INFORMATION  *****************************************" & vbCrLf
    '                custId = row.Item("CustomerId")
    '                custEnquiryType = row.Item("Type")
    '                'custName = row.Item("Name")
    '                'load customer
    '                Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, custId)
    '                Dim custRow As EnquiryWorkSheetDataSet.CustomerRow
    '                custRow = Me.EnquiryWorkSheetDataSet.Customer(0)
    '                custType = custRow.Item("CustomerType")
    '                Select Case custType
    '                    '==============================
    '                    'Individual
    '                    '==============================
    '                    Case MyEnums.CustomerType.Individual, MyEnums.CustomerType.SoleTrader
    '                        thisCustString = thisCustString & "Type: Individual  -  " & MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType)) & vbCrLf
    '                        thisCustString = thisCustString & "Customer name: " & If(IsDBNull(custRow.Item("FirstName")), "", custRow.Item("FirstName")) & " " & If(IsDBNull(custRow.Item("MiddleNames")), "", custRow.Item("MiddleNames")) & " " & If(IsDBNull(custRow.Item("LastName")), "", custRow.Item("LastName")) & vbCrLf
    '                        If Not IsDBNull(custRow.Item("ClientNum")) Then
    '                            If custRow.ClientNum.Length > 0 Then
    '                                thisCustString = thisCustString & "FinPower: Client No - Client_" & custRow.Item("ClientNum") & vbCrLf 'custRow.Item("ClientTypeId") & custRow.Item("ClientNum") & vbCrLf
    '                            Else
    '                                If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '                                        Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
    '                                    ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility And custEnquiryType = MyEnums.CustomerEnquiryType.Main Then
    '                                        Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
    '                                    Else
    '                                        thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                                    End If
    '                                Else
    '                                    thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                                End If
    '                            End If
    '                        Else
    '                            If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '                                    Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
    '                                ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility And custEnquiryType = MyEnums.CustomerEnquiryType.Main Then
    '                                    Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
    '                                Else
    '                                    thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                            End If
    '                        End If
    '                        'If EnquiryType = Split Loan Drawdown, then NO Stability info exits
    '                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                            '********** Identity Risk Assessment
    '                            Dim identityString As String = "--------------------   Identity Risk Assessment" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("IdType")) Then
    '                                If custRow.Item("IdType") > -1 Then
    '                                    'MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType))
    '                                    identityString = identityString & "The ID type is " & MyEnums.GetDescription(DirectCast(CInt(custRow.Item("IdType")), MyEnums.IdType)) & vbCrLf
    '                                    If custRow.Item("IdType") = MyEnums.IdType.NZDriversLicence Then
    '                                        If Not IsDBNull(custRow.Item("DriverLicenceType")) Then
    '                                            If custRow.Item("DriverLicenceType") > -1 Then
    '                                                identityString = identityString & "The Driver's Licence type is " & MyEnums.GetDescription(DirectCast(CInt(custRow.Item("DriverLicenceType")), MyEnums.DriverLicenceType)) & vbCrLf
    '                                            Else
    '                                                identityString = identityString & "The Driver's Licence type is unknown!" & vbCrLf
    '                                            End If
    '                                        Else
    '                                            identityString = identityString & "The Driver's Licence type is unknown!" & vbCrLf
    '                                        End If
    '                                    End If
    '                                Else
    '                                    identityString = identityString & "The ID type is unknown!" & vbCrLf
    '                                End If
    '                            Else
    '                                identityString = identityString & "The ID type is unknown!" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("IDCurrent")) Then
    '                                If custRow.Item("IDCurrent") = True Then
    '                                    identityString = identityString & "The ID documents are current" & vbCrLf
    '                                Else
    '                                    identityString = identityString & "The ID documents are NOT current!" & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("IDPerson")) Then
    '                                If custRow.Item("IDPerson").length > 0 Then
    '                                    identityString = identityString & "I am satisfied the the ID is this person because " & custRow.Item("IDPerson") & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("ResStatusAccept")) Then
    '                                If custRow.Item("ResStatusAccept").length > 0 Then
    '                                    identityString = identityString & "Their residency status is acceptable because " & custRow.Item("ResStatusAccept") & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("IDLegitimate")) Then
    '                                If custRow.Item("IDLegitimate").length > 0 Then
    '                                    identityString = identityString & "Taking into account the age of the ID and Client I am satisfied it is legitimate because " & custRow.Item("IDLegitimate") & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("AMLRisk")) Then
    '                                If custRow.Item("AMLRisk") > 0 Then
    '                                    identityString = identityString & "Based on the above my AML/CFT risk assessment of this client is " & GetRiskDesc(custRow.Item("AMLRisk")) & vbCrLf
    '                                End If
    '                            End If

    '                            thisCustString = thisCustString & identityString

    '                            '********** Stability
    '                            Dim stabilityString As String
    '                            stabilityString = "*****  STABILITY  *****" & vbCrLf
    '                            'tenancy
    '                            stabilityString = stabilityString & "--------------------  Tenancy:" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("TenancyLength")) Then
    '                                If custRow.Item("TenancyLength").Length > 0 Then
    '                                    stabilityString = stabilityString & "Length of current tenancy is " & custRow.Item("TenancyLength")
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("TenancyType")) Then
    '                                If custRow.Item("TenancyType").Length > 0 Then
    '                                    stabilityString = stabilityString & "                    The tenancy type is " & custRow.Item("TenancyType") & vbCrLf
    '                                End If
    '                            Else
    '                                stabilityString = stabilityString & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("TenancySatisfactory")) Then
    '                                If custRow.Item("TenancySatisfactory").Length > 0 Then
    '                                    stabilityString = stabilityString & "This is satisfactory because " & custRow.Item("TenancySatisfactory") & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("TenancyEnvironment")) Then
    '                                If custRow.Item("TenancyEnvironment").Length > 0 Then
    '                                    stabilityString = stabilityString & "Is the tenancy environment likely to change over the term of the loan? " & custRow.Item("TenancyEnvironment") & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("TenancyEstablish")) Then
    '                                If custRow.Item("TenancyEstablish").Length > 0 Then
    '                                    stabilityString = stabilityString & "Established tenancy by " & custRow.Item("TenancyEstablish") & vbCrLf
    '                                End If
    '                            End If
    '                            'employment
    '                            If custType = MyEnums.CustomerType.SoleTrader Then
    '                                stabilityString = stabilityString & "--------------------   Trading activity:" & vbCrLf
    '                                stabilityString = stabilityString & "How long has the business been trading? " & custRow.StabilityQ1 & vbCrLf & _
    '                                            "Is this sufficient? " & custRow.StabilityQ2 & vbCrLf & _
    '                                            "Do they have financials? Do we have copies? " & custRow.StabilityQ3 & vbCrLf & _
    '                                            "Is this the main source of income? " & custRow.StabilityQ9 & vbCrLf & _
    '                                            "What sort of business is it? " & custRow.StabilityQ4 & vbCrLf & _
    '                                            "Is it a satificatory business to lend to? " & custRow.StabilityQ6 & vbCrLf & _
    '                                            "Is it registered with the IRD? " & custRow.StabilityQ7 & vbCrLf
    '                            ElseIf custType = MyEnums.CustomerType.Individual Then
    '                                stabilityString = stabilityString & "--------------------   Employment:" & vbCrLf
    '                                'check if self employed
    '                                If custRow.SelfEmployed = False And custRow.Beneficiary = False And custRow.EmployerNotRung = False Then
    '                                    If Not IsDBNull(custRow.Item("EmployerName")) Then
    '                                        If custRow.EmployerName.Length > 0 Then
    '                                            stabilityString = stabilityString & "Rang Employer " & custRow.EmployerName & " and spoke with " & _
    '                                            custRow.EmployerSpoketoName & " on " & If(custRow.EmployerRangDate = Nothing, "unknown", custRow.EmployerRangDate) & vbCrLf
    '                                            stabilityString = stabilityString & "Does Client work for your company? " & custRow.StabilityQ1 & vbCrLf & _
    '                                            "Are they fulltime permanent or other? " & custRow.StabilityQ2 & vbCrLf & _
    '                                            "What does your employee do? " & custRow.StabilityQ3 & vbCrLf & _
    '                                            "Are they PAYE employee, Contractor, Invoice or other? " & custRow.StabilityQ9 & vbCrLf & _
    '                                            "How long have they been there? " & custRow.StabilityQ4 & vbCrLf & _
    '                                            "Confirm pay of $" & custRow.StabilityQ5 & "? " & custRow.StabilityQ6 & vbCrLf & _
    '                                            "Is their job safe? " & custRow.StabilityQ7 & vbCrLf & _
    '                                            "Are they a good worker? " & custRow.StabilityQ8 & vbCrLf
    '                                        End If
    '                                    End If
    '                                Else
    '                                    If custRow.SelfEmployed = True Then
    '                                        stabilityString = stabilityString & "Client is self-employed" & vbCrLf
    '                                    End If
    '                                    If custRow.Beneficiary = True Then
    '                                        stabilityString = stabilityString & "Client is a beneficiary" & vbCrLf
    '                                    End If
    '                                    If custRow.EmployerNotRung = True Then
    '                                        stabilityString = stabilityString & "Employer not rung, must be a Top-up Enquiry" & vbCrLf
    '                                    End If
    '                                End If
    '                            End If

    '                            'work and other comments
    '                            If Not IsDBNull(custRow.Item("StabilityComments")) Then
    '                                If custRow.Item("StabilityComments").length > 0 Then
    '                                    stabilityString = stabilityString & "Comments: " & custRow.Item("StabilityComments") & vbCrLf
    '                                End If
    '                            End If

    '                            thisCustString = thisCustString & stabilityString

    '                            '********** Credit
    '                            Dim creditString As String
    '                            creditString = "*****  CREDIT  *****" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("CreditComments")) Then
    '                                If custRow.CreditComments.Length > 0 Then
    '                                    creditString = creditString & "Comments on Credit History - work notes: " & custRow.CreditComments & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CreditExplanation")) Then
    '                                If custRow.CreditExplanation.Length > 0 Then
    '                                    creditString = creditString & "Defaults / Collection explanations: " & custRow.CreditExplanation & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CreditOther")) Then
    '                                If custRow.CreditOther.Length > 0 Then
    '                                    creditString = creditString & "PPSR and other checks: " & custRow.CreditOther & vbCrLf
    '                                End If
    '                            End If
    '                            creditString = creditString & "Is Credit History sufficient? "
    '                            If custRow.CreditQuestion1 = False Then
    '                                creditString = creditString & "                                       No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion1 = True Then
    '                                creditString = creditString & "                                       Yes" & vbCrLf
    '                            End If
    '                            creditString = creditString & "Are all enquiries expected and accounted for? "
    '                            If custRow.CreditQuestion2 = False Then
    '                                creditString = creditString & "     No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion2 = True Then
    '                                creditString = creditString & "     Yes" & vbCrLf
    '                            End If
    '                            creditString = creditString & "Are there Gaps in the Credit History? "
    '                            If custRow.CreditQuestion3 = False Then
    '                                creditString = creditString & "                     No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion3 = True Then
    '                                creditString = creditString & "                     Yes" & vbCrLf
    '                            End If

    '                            thisCustString = thisCustString & creditString & vbCrLf

    '                        End If

    '                        '==============================
    '                        'Company
    '                        '==============================
    '                    Case MyEnums.CustomerType.Company
    '                        thisCustString = thisCustString & "Type: Company  -  " & MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType)) & vbCrLf
    '                        thisCustString = thisCustString & "Company name: " & If(IsDBNull(custRow.Item("LegalName")), "", custRow.Item("LegalName")) & "      Company number: " & If(IsDBNull(custRow.Item("CompanyNumber")), "", custRow.Item("CompanyNumber")) & vbCrLf
    '                        If Not IsDBNull(custRow.Item("TradingName")) Then
    '                            thisCustString = thisCustString & "Trading name: " & custRow.TradingName & vbCrLf
    '                        End If
    '                        If Not IsDBNull(custRow.Item("ClientNum")) Then
    '                            If custRow.ClientNum.Length > 0 Then
    '                                thisCustString = thisCustString & "FinPower: Client No - Client_" & custRow.ClientNum & vbCrLf 'custRow.ClientTypeId & custRow.ClientNum & vbCrLf
    '                            Else
    '                                If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                                    Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
    '                                Else
    '                                    thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                                End If
    '                            End If
    '                        Else
    '                            If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                                Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
    '                            Else
    '                                thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                            End If
    '                        End If
    '                        'If EnquiryType = Split Loan Drawdown, then NO Stability info exits
    '                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                            '********** Shareholders
    '                            Try
    '                                Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.ShareHolder))
    '                            Catch ex As System.Exception
    '                                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '                            End Try
    '                            If EnquiryWorkSheetDataSet.DTSB.Rows.Count > 0 Then
    '                                Dim thisDtsbString As String = String.Empty
    '                                thisDtsbString = thisDtsbString & "*****  Shareholders:" & vbCrLf
    '                                For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB.Rows
    '                                    Dim idVerified As String = String.Empty
    '                                    Dim addressVerified As String = String.Empty
    '                                    If Not IsDBNull(dtsbRow.Item("IdVerified")) Then
    '                                        If dtsbRow.Item("IdVerified") = True Then
    '                                            idVerified = "Yes"
    '                                        Else
    '                                            idVerified = "No"
    '                                        End If
    '                                    End If
    '                                    If Not IsDBNull(dtsbRow.Item("AddressVerified")) Then
    '                                        If dtsbRow.Item("AddressVerified") = True Then
    '                                            addressVerified = "Yes"
    '                                        Else
    '                                            addressVerified = "No"
    '                                        End If
    '                                    End If
    '                                    thisDtsbString = thisDtsbString & "   " & dtsbRow.Item("FirstName") & "" & dtsbRow.Item("LastName") & "    Born: " & dtsbRow.Item("DateOfBirth") & vbCrLf
    '                                    thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
    '                                Next
    '                                thisCustString = thisCustString & thisDtsbString
    '                            End If

    '                            thisCustString = thisCustString & vbCrLf
    '                            If custRow.BusinessShareIdentified = True Then
    '                                thisCustString = thisCustString & "Company's shareholders have been fully identified" & vbCrLf
    '                            Else
    '                                thisCustString = thisCustString & "Company's shareholders have NOT been fully identified" & vbCrLf
    '                            End If
    '                            If custRow.BusinessShareStructure = True Then
    '                                thisCustString = thisCustString & "Company's shareholding structure has been fully identified" & vbCrLf
    '                            Else
    '                                thisCustString = thisCustString & "Company's shareholding structure has NOT been fully identified" & vbCrLf
    '                            End If
    '                            If custRow.BusinessShareholders = True Then
    '                                thisCustString = thisCustString & "Company's shareholders are satisfactory" & vbCrLf
    '                            Else
    '                                thisCustString = thisCustString & "Company's shareholders are NOT satisfactory, or not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessShareholderNotes")) Then
    '                                If custRow.BusinessShareholderNotes.Length > 0 Then
    '                                    thisCustString = thisCustString & custRow.Item("BusinessShareholderNotes") & vbCrLf
    '                                End If
    '                            End If

    '                            '********** Directors
    '                            Try
    '                                Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Director))
    '                            Catch ex As System.Exception
    '                                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '                            End Try
    '                            If EnquiryWorkSheetDataSet.DTSB01.Rows.Count > 0 Then
    '                                Dim thisDtsbString As String = String.Empty
    '                                thisDtsbString = thisDtsbString & "*****  Directors:" & vbCrLf
    '                                For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB01.Rows
    '                                    Dim idVerified As String = String.Empty
    '                                    Dim addressVerified As String = String.Empty
    '                                    If Not IsDBNull(dtsbRow.Item("IdVerified")) Then
    '                                        If dtsbRow.Item("IdVerified") = True Then
    '                                            idVerified = "Yes"
    '                                        Else
    '                                            idVerified = "No"
    '                                        End If
    '                                    End If
    '                                    If Not IsDBNull(dtsbRow.Item("AddressVerified")) Then
    '                                        If dtsbRow.Item("AddressVerified") = True Then
    '                                            addressVerified = "Yes"
    '                                        Else
    '                                            addressVerified = "No"
    '                                        End If
    '                                    End If
    '                                    thisDtsbString = thisDtsbString & "   " & dtsbRow.Item("FirstName") & "" & dtsbRow.Item("LastName") & "    Born: " & dtsbRow.Item("DateOfBirth") & vbCrLf
    '                                    thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
    '                                Next
    '                                thisCustString = thisCustString & thisDtsbString
    '                            End If

    '                            thisCustString = thisCustString & vbCrLf
    '                            If Not IsDBNull(custRow.Item("BusinessDirectors")) Then
    '                                If custRow.BusinessDirectors = True Then
    '                                    thisCustString = thisCustString & "Company's directors are satisfactory" & vbCrLf
    '                                Else
    '                                    thisCustString = thisCustString & "Company's directors are NOT satisfactory, or not yet checked" & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessDirectorNotes")) Then
    '                                If custRow.BusinessDirectorNotes.Length > 0 Then
    '                                    thisCustString = thisCustString & custRow.Item("BusinessDirectorNotes") & vbCrLf
    '                                End If
    '                            End If
    '                            '********** Trade References
    '                            Try
    '                                Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
    '                            Catch ex As Exception
    '                                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '                            End Try
    '                            If EnquiryWorkSheetDataSet.TradeReferences.Rows.Count > 0 Then
    '                                Dim thisTrString As String = String.Empty
    '                                thisTrString = thisTrString & "*****  TradeReferences:" & vbCrLf
    '                                For Each trRow As DataRow In EnquiryWorkSheetDataSet.TradeReferences.Rows
    '                                    thisTrString = thisTrString & "   " & trRow.Item("BusinessName") & vbCrLf
    '                                    thisTrString = thisTrString & "   Account: " & trRow.Item("Account") & vbCrLf
    '                                    thisTrString = thisTrString & "   Spend: " & trRow.Item("Spend") & vbCrLf
    '                                    thisTrString = thisTrString & "   Pay: " & trRow.Item("Pay") & vbCrLf
    '                                    thisTrString = thisTrString & "   Comments: " & trRow.Item("Comments") & vbCrLf
    '                                Next
    '                                thisCustString = thisCustString & thisTrString
    '                            End If
    '                            '********** Stability
    '                            Dim stabilityString As String
    '                            stabilityString = vbCrLf & "*****  STABILITY  *****" & vbCrLf
    '                            Dim thisCompanyInfo As String = "--------------------  Company Information:" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("BusinessRegistered")) Then
    '                                If custRow.BusinessRegistered = True Then
    '                                    thisCompanyInfo = thisCompanyInfo & "Company is registered" & vbCrLf
    '                                Else
    '                                    thisCompanyInfo = thisCompanyInfo & "Company is NOT registered" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCompanyInfo = thisCompanyInfo & "Company registration not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CoExtractTrustDeed")) Then
    '                                If custRow.CoExtractTrustDeed = True Then
    '                                    thisCompanyInfo = thisCompanyInfo & "Company Extract is saved" & vbCrLf
    '                                Else
    '                                    thisCompanyInfo = thisCompanyInfo & "Company Extract is NOT saved" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCompanyInfo = thisCompanyInfo & "Company Extract is NOT saved" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessReturnsFiled")) Then
    '                                If custRow.BusinessReturnsFiled = True Then
    '                                    thisCompanyInfo = thisCompanyInfo & "Company has filed returns" & vbCrLf
    '                                Else
    '                                    thisCompanyInfo = thisCompanyInfo & "Company has NOT filed returns" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCompanyInfo = thisCompanyInfo & "Company filed returns not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessPaidUpCapital")) Then
    '                                If custRow.BusinessPaidUpCapital = True Then
    '                                    thisCompanyInfo = thisCompanyInfo & "Company's paid-up capital is sufficient" & vbCrLf
    '                                Else
    '                                    thisCompanyInfo = thisCompanyInfo & "Company's paid-up capital is NOT sufficient" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCompanyInfo = thisCompanyInfo & "Company's paid-up capital is not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessHistorySatisfactory")) Then
    '                                If custRow.BusinessHistorySatisfactory = True Then
    '                                    thisCompanyInfo = thisCompanyInfo & "The Company's previous history is satisfactory" & vbCrLf
    '                                Else
    '                                    thisCompanyInfo = thisCompanyInfo & "The Company's previous history is NOT satisfactory" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCompanyInfo = thisCompanyInfo & "The Company's previous history is not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessHistoryNotes")) Then
    '                                If custRow.BusinessHistoryNotes.Length > 0 Then
    '                                    thisCompanyInfo = thisCompanyInfo & custRow.Item("BusinessHistoryNotes") & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessAddressSatisfactory")) Then
    '                                If custRow.BusinessAddressSatisfactory = True Then
    '                                    thisCompanyInfo = thisCompanyInfo & "The Company's addresses are satisfactory" & vbCrLf
    '                                Else
    '                                    thisCompanyInfo = thisCompanyInfo & "The Company's addresses are NOT satisfactory" & vbCrLf
    '                                End If
    '                            Else
    '                                thisCompanyInfo = thisCompanyInfo & "The Company's addresses are not yet checked" & vbCrLf
    '                            End If

    '                            If Not IsDBNull(custRow.Item("BusinessAddressNotes")) Then
    '                                If custRow.BusinessAddressNotes.Length > 0 Then
    '                                    thisCompanyInfo = thisCompanyInfo & custRow.BusinessAddressNotes & vbCrLf
    '                                End If
    '                            End If
    '                            stabilityString = stabilityString & thisCompanyInfo
    '                            'Company Tenancy
    '                            Dim thisCompanyTenancy As String = "--------------------  Company Tenancy:" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("TenancyType")) Then
    '                                If custRow.TenancyType.Length > 0 Then
    '                                    thisCompanyTenancy = thisCompanyTenancy & "The company tenancy is " & custRow.TenancyType & " for " & custRow.TenancyLength & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("TenancySatisfactory")) Then
    '                                If custRow.TenancySatisfactory.Length > 0 Then
    '                                    thisCompanyTenancy = thisCompanyTenancy & "This is satisfactory for the term of the loan because " & custRow.TenancySatisfactory & vbCrLf
    '                                End If
    '                            End If
    '                            stabilityString = stabilityString & thisCompanyTenancy
    '                            thisCustString = thisCustString & stabilityString & vbCrLf
    '                            'Company Credit
    '                            Dim creditString As String
    '                            creditString = vbCrLf & "*****  CREDIT  *****" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("CreditComments")) Then
    '                                If custRow.CreditComments.Length > 0 Then
    '                                    creditString = creditString & "Comments on Credit History - work notes: " & custRow.CreditComments & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CreditExplanation")) Then
    '                                If custRow.CreditExplanation.Length > 0 Then
    '                                    creditString = creditString & "Defaults / Collection explanations: " & custRow.CreditExplanation & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CreditOther")) Then
    '                                If custRow.CreditOther.Length > 0 Then
    '                                    creditString = creditString & "PPSR and other checks: " & custRow.CreditOther & vbCrLf
    '                                End If
    '                            End If
    '                            creditString = creditString & "Is Credit History sufficient? "
    '                            If custRow.CreditQuestion1 = False Then
    '                                creditString = creditString & "                                       No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion1 = True Then
    '                                creditString = creditString & "                                       Yes" & vbCrLf
    '                            End If
    '                            creditString = creditString & "Are all enquiries expected and accounted for? "
    '                            If custRow.CreditQuestion2 = False Then
    '                                creditString = creditString & "     No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion2 = True Then
    '                                creditString = creditString & "     Yes" & vbCrLf
    '                            End If
    '                            creditString = creditString & "Are there Gaps in the Credit History? "
    '                            If custRow.CreditQuestion3 = False Then
    '                                creditString = creditString & "                     No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion3 = True Then
    '                                creditString = creditString & "                     Yes" & vbCrLf
    '                            End If

    '                            thisCustString = thisCustString & creditString & vbCrLf

    '                            'Company Other Comments
    '                            If Not IsDBNull(custRow.Item("BusinessOtherNotes")) Then
    '                                If custRow.Item("BusinessOtherNotes").length > 0 Then
    '                                    thisCustString = thisCustString & "Other notes: " & custRow.Item("BusinessOtherNotes") & vbCrLf
    '                                End If
    '                            End If

    '                        End If 'End:  If Not ThisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not ThisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not ThisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown 

    '                        '==============================
    '                        'Trust
    '                        '==============================
    '                    Case MyEnums.CustomerType.Trust
    '                        thisCustString = thisCustString & "Type: Trust  -  " & MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType)) & vbCrLf
    '                        If Not IsDBNull(custRow.Item("TradingName")) Then
    '                            thisCustString = thisCustString & "Trust name: " & custRow.TradingName & vbCrLf
    '                        End If
    '                        If Not IsDBNull(custRow.Item("ClientNum")) Then
    '                            If custRow.ClientNum.Length > 0 Then
    '                                thisCustString = thisCustString & "FinPower: Client No - Client_" & custRow.ClientNum & vbCrLf 'custRow.ClientTypeId & custRow.ClientNum & vbCrLf
    '                            Else
    '                                If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                                    Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
    '                                Else
    '                                    thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                                End If
    '                            End If
    '                        Else
    '                            If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                                Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
    '                            Else
    '                                thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
    '                            End If
    '                        End If
    '                        'If EnquiryType = Split Loan Drawdown, then NO Stability info exits
    '                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                            '********** Beneficaries
    '                            Try
    '                                Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Beneficiary))
    '                            Catch ex As System.Exception
    '                                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '                            End Try
    '                            If EnquiryWorkSheetDataSet.DTSB.Rows.Count > 0 Then
    '                                Dim thisDtsbString As String = String.Empty
    '                                thisDtsbString = thisDtsbString & "*****  Beneficaries:" & vbCrLf
    '                                For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB.Rows
    '                                    Dim idVerified As String = String.Empty
    '                                    Dim addressVerified As String = String.Empty
    '                                    If Not IsDBNull(dtsbRow.Item("IdVerified")) Then
    '                                        If dtsbRow.Item("IdVerified") = True Then
    '                                            idVerified = "Yes"
    '                                        Else
    '                                            idVerified = "No"
    '                                        End If
    '                                    End If
    '                                    If Not IsDBNull(dtsbRow.Item("AddressVerified")) Then
    '                                        If dtsbRow.Item("AddressVerified") = True Then
    '                                            addressVerified = "Yes"
    '                                        Else
    '                                            addressVerified = "No"
    '                                        End If
    '                                    End If
    '                                    thisDtsbString = thisDtsbString & "   " & dtsbRow.Item("FirstName") & "" & dtsbRow.Item("LastName") & "    Born: " & dtsbRow.Item("DateOfBirth") & vbCrLf
    '                                    thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
    '                                Next
    '                                thisCustString = thisCustString & thisDtsbString
    '                            End If
    '                            '********** Trustees
    '                            Try
    '                                Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Trustee))
    '                            Catch ex As System.Exception
    '                                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '                            End Try
    '                            If EnquiryWorkSheetDataSet.DTSB01.Rows.Count > 0 Then
    '                                Dim thisDtsbString As String = String.Empty
    '                                thisDtsbString = thisDtsbString & "*****  Trustees:" & vbCrLf
    '                                For Each dtsbRow As DataRow In EnquiryWorkSheetDataSet.DTSB01.Rows
    '                                    Dim idVerified As String = String.Empty
    '                                    Dim addressVerified As String = String.Empty
    '                                    If Not IsDBNull(dtsbRow.Item("IdVerified")) Then
    '                                        If dtsbRow.Item("IdVerified") = True Then
    '                                            idVerified = "Yes"
    '                                        Else
    '                                            idVerified = "No"
    '                                        End If
    '                                    End If
    '                                    If Not IsDBNull(dtsbRow.Item("AddressVerified")) Then
    '                                        If dtsbRow.Item("AddressVerified") = True Then
    '                                            addressVerified = "Yes"
    '                                        Else
    '                                            addressVerified = "No"
    '                                        End If
    '                                    End If
    '                                    thisDtsbString = thisDtsbString & "   " & dtsbRow.Item("FirstName") & "" & dtsbRow.Item("LastName") & "    Born: " & dtsbRow.Item("DateOfBirth") & vbCrLf
    '                                    thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
    '                                Next
    '                                thisCustString = thisCustString & thisDtsbString
    '                            End If
    '                            '********** Trade References
    '                            Try
    '                                Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
    '                            Catch ex As Exception
    '                                System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '                            End Try
    '                            If EnquiryWorkSheetDataSet.TradeReferences.Rows.Count > 0 Then
    '                                Dim thisTrString As String = String.Empty
    '                                thisTrString = thisTrString & "*****  TradeReferences:" & vbCrLf
    '                                For Each trRow As DataRow In EnquiryWorkSheetDataSet.TradeReferences.Rows
    '                                    thisTrString = thisTrString & "   " & trRow.Item("BusinessName") & vbCrLf
    '                                    thisTrString = thisTrString & "   Account: " & trRow.Item("Account") & vbCrLf
    '                                    thisTrString = thisTrString & "   Spend: " & trRow.Item("Spend") & vbCrLf
    '                                    thisTrString = thisTrString & "   Pay: " & trRow.Item("Pay") & vbCrLf
    '                                    thisTrString = thisTrString & "   Comments: " & trRow.Item("Comments") & vbCrLf
    '                                Next
    '                                thisCustString = thisCustString & thisTrString
    '                            End If
    '                            '********** Stability
    '                            Dim stabilityString As String
    '                            stabilityString = vbCrLf & "*****  STABILITY  *****" & vbCrLf
    '                            Dim thisTrustInfo As String = "--------------------  Trust Information:" & vbCrLf
    '                            'If custRow.BusinessRegistered = True Then
    '                            '    thisCompanyInfo = thisCompanyInfo & "Company is registered" & vbCrLf
    '                            'Else
    '                            '    thisCompanyInfo = thisCompanyInfo & "Company is NOT registered or not yet checked" & vbCrLf
    '                            'End If
    '                            If custRow.CoExtractTrustDeed = True Then
    '                                thisTrustInfo = thisTrustInfo & "Trust Deed is saved" & vbCrLf
    '                            Else
    '                                thisTrustInfo = thisTrustInfo & "Trust Deed is NOT saved" & vbCrLf
    '                            End If
    '                            If custRow.BusinessReturnsFiled = True Then
    '                                thisTrustInfo = thisTrustInfo & "Trust has files returns" & vbCrLf
    '                            Else
    '                                thisTrustInfo = thisTrustInfo & "Trust has NOT filed returns, or not yet checked" & vbCrLf
    '                            End If
    '                            If custRow.BusinessPaidUpCapital = True Then
    '                                thisTrustInfo = thisTrustInfo & "Trust's paid-up capital is sufficient" & vbCrLf
    '                            Else
    '                                thisTrustInfo = thisTrustInfo & "Trust's paid-up capital is NOT sufficient, or not yet checked" & vbCrLf
    '                            End If
    '                            If custRow.BusinessHistorySatisfactory = True Then
    '                                thisTrustInfo = thisTrustInfo & "The Trust's previous history is satisfactory" & vbCrLf
    '                            Else
    '                                thisTrustInfo = thisTrustInfo & "The Trust's previous history is NOT satisfactory, or not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessHistoryNotes")) Then
    '                                If custRow.BusinessHistoryNotes.Length > 0 Then
    '                                    thisTrustInfo = thisTrustInfo & custRow.Item("BusinessHistoryNotes") & vbCrLf
    '                                End If
    '                            End If
    '                            If custRow.BusinessAddressSatisfactory = True Then
    '                                thisTrustInfo = thisTrustInfo & "The Trust's addresses are satisfactory" & vbCrLf
    '                            Else
    '                                thisTrustInfo = thisTrustInfo & "The Trust's addresses are NOT satisfactory, or not yet checked" & vbCrLf
    '                            End If
    '                            If Not IsDBNull(custRow.Item("BusinessAddressNotes")) Then
    '                                If custRow.BusinessAddressNotes.Length > 0 Then
    '                                    thisTrustInfo = thisTrustInfo & custRow.BusinessAddressNotes & vbCrLf
    '                                End If
    '                            End If
    '                            stabilityString = stabilityString & thisTrustInfo
    '                            'Company Tenancy
    '                            Dim thisTrustTenancy As String = "--------------------  Trust Tenancy:" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("TenancyType")) Then
    '                                If custRow.TenancyType.Length > 0 Then
    '                                    thisTrustTenancy = thisTrustTenancy & "The trust tenancy is " & custRow.TenancyType & " for " & custRow.TenancyLength & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("TenancySatisfactory")) Then
    '                                If custRow.TenancySatisfactory.Length > 0 Then
    '                                    thisTrustTenancy = thisTrustTenancy & "This is satisfactory for the term of the loan because " & custRow.TenancySatisfactory & vbCrLf
    '                                End If
    '                            End If
    '                            stabilityString = stabilityString & thisTrustTenancy
    '                            thisCustString = thisCustString & stabilityString & vbCrLf

    '                            'Company Credit
    '                            Dim creditString As String
    '                            creditString = vbCrLf & "*****  CREDIT  *****" & vbCrLf
    '                            If Not IsDBNull(custRow.Item("CreditComments")) Then
    '                                If custRow.CreditComments.Length > 0 Then
    '                                    creditString = creditString & "Comments on Credit History - work notes: " & custRow.CreditComments & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CreditExplanation")) Then
    '                                If custRow.CreditExplanation.Length > 0 Then
    '                                    creditString = creditString & "Defaults / Collection explanations: " & custRow.CreditExplanation & vbCrLf
    '                                End If
    '                            End If
    '                            If Not IsDBNull(custRow.Item("CreditOther")) Then
    '                                If custRow.CreditOther.Length > 0 Then
    '                                    creditString = creditString & "PPSR and other checks: " & custRow.CreditOther & vbCrLf
    '                                End If
    '                            End If
    '                            creditString = creditString & "Is Credit History sufficient? "
    '                            If custRow.CreditQuestion1 = False Then
    '                                creditString = creditString & "                                       No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion1 = True Then
    '                                creditString = creditString & "                                       Yes" & vbCrLf
    '                            End If
    '                            creditString = creditString & "Are all enquiries expected and accounted for? "
    '                            If custRow.CreditQuestion2 = False Then
    '                                creditString = creditString & "     No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion2 = True Then
    '                                creditString = creditString & "     Yes" & vbCrLf
    '                            End If
    '                            creditString = creditString & "Are there Gaps in the Credit History? "
    '                            If custRow.CreditQuestion3 = False Then
    '                                creditString = creditString & "                     No" & vbCrLf
    '                            ElseIf custRow.CreditQuestion3 = True Then
    '                                creditString = creditString & "                     Yes" & vbCrLf
    '                            End If

    '                            thisCustString = thisCustString & creditString & vbCrLf

    '                            'Company Other Comments
    '                            If Not IsDBNull(custRow.Item("BusinessOtherNotes")) Then
    '                                If custRow.Item("BusinessOtherNotes").length > 0 Then
    '                                    thisCustString = thisCustString & "Other notes: " & custRow.Item("BusinessOtherNotes") & vbCrLf
    '                                End If
    '                            End If

    '                        End If


    '                    Case Else

    '                End Select

    '                clientString = clientString & thisCustString & vbCrLf
    '            Next

    '        End If

    '        If clientString.Length > 0 Then
    '            constructedString = constructedString & clientString
    '        End If
    '        '********** End of Client Info ************ 

    '        '********** Audit ********************
    '        Dim auditString As String = ""
    '        auditString = vbCrLf & "*****  AUDIT INFORMATION  **********************************************" & vbCrLf
    '        'test Bindingsource.Current Is Nothing
    '        If Not DueDiligenceBindingSource.Current Is Nothing Then
    '            'Create Audit row
    '            Dim auditRow As EnquiryWorkSheetDataSet.DueDiligenceRow
    '            'Get Row in DueDiligence dataset
    '            auditRow = Me.EnquiryWorkSheetDataSet.DueDiligence(0)
    '            'Loan Breakdown & Dealer Commission
    '            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '                'Loan Breakdown
    '                auditString = auditString & "--------------------  Loan Breakdown:" & vbCrLf
    '                If Not IsDBNull(auditRow.Item("LoanNum")) Then
    '                    auditString = auditString & "Loan number: " & auditRow.Item("LoanNum") & vbCrLf
    '                Else
    '                    If ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or ThisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
    '                        Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Audit: Loan number is null.")
    '                    Else
    '                        auditString = auditString & "Loan number: Unknown" & vbCrLf
    '                    End If
    '                End If
    '                Dim totalDrawdown As Decimal = If(IsDBNull(auditRow.Item("CashPrice_Advance")), 0, auditRow.Item("CashPrice_Advance")) + If(IsDBNull(auditRow.Item("OtherAdvance")), 0, auditRow.Item("OtherAdvance")) - If(IsDBNull(auditRow.Item("Deposit")), 0, auditRow.Item("Deposit")) + If(IsDBNull(auditRow.Item("Brokerage")), 0, auditRow.Item("Brokerage"))
    '                auditString = auditString & "Cash price/Advance: " & Format(If(IsDBNull(auditRow.Item("CashPrice_Advance")), 0, auditRow.Item("CashPrice_Advance")), "C") & "  |  Other advanced: " & Format(If(IsDBNull(auditRow.Item("OtherAdvance")), 0, auditRow.Item("OtherAdvance")), "C") & vbCrLf & "Deposit: " & Format(If(IsDBNull(auditRow.Item("Deposit")), 0, auditRow.Item("Deposit")), "C") & "  |  Brokerage: " & Format(If(IsDBNull(auditRow.Item("Brokerage")), 0, auditRow.Item("Brokerage")), "C") & vbCrLf & "Loan retention: " & If(IsDBNull(auditRow.Item("LoanRetention")), 0, auditRow.Item("LoanRetention")) & "%" & vbCrLf & "Total drawdown: " & Format(totalDrawdown, "C") & vbCrLf
    '                auditString = auditString & "Refinance: " & Format(If(IsDBNull(auditRow.Item("Refinance")), 0, auditRow.Item("Refinance")), "C") & "  |  YFL fees: " & Format(If(IsDBNull(auditRow.Item("YFLFees")), 0, auditRow.Item("YFLFees")), "C") & "  |  YFL insurances: " & Format(If(IsDBNull(auditRow.Item("YFLInsurances")), 0, auditRow.Item("YFLInsurances")), "C") & vbCrLf
    '                'AmountFinanced = CashPriceAdvance + OtherAdvance - Deposit + Refinance + YFLFees + YFLInsurances  
    '                Dim amountFinanced As Decimal = If(IsDBNull(auditRow.Item("CashPrice_Advance")), 0, auditRow.Item("CashPrice_Advance")) + If(IsDBNull(auditRow.Item("OtherAdvance")), 0, auditRow.Item("OtherAdvance")) - If(IsDBNull(auditRow.Item("Deposit")), 0, auditRow.Item("Deposit")) + If(IsDBNull(auditRow.Item("Brokerage")), 0, auditRow.Item("Brokerage")) + If(IsDBNull(auditRow.Item("Refinance")), 0, auditRow.Item("Refinance")) + If(IsDBNull(auditRow.Item("YFLFees")), 0, auditRow.Item("YFLFees")) + If(IsDBNull(auditRow.Item("YFLInsurances")), 0, auditRow.Item("YFLInsurances"))
    '                auditString = auditString & "Amount financed: " & Format(amountFinanced, "C") & vbCrLf
    '                If If(IsDBNull(auditRow.Item("LoanFiguresChecked")), False, auditRow.Item("LoanFiguresChecked")) = True Then
    '                    auditString = auditString & "Loan breakdown figures have been checked." & vbCrLf
    '                Else
    '                    auditString = auditString & "Loan breakdown figures have NOT been checked!" & vbCrLf
    '                End If
    '                'Dealer Commission
    '                auditString = auditString & "--------------------  Dealer Commission:" & vbCrLf
    '                Dim totalCommission As Decimal = If(IsDBNull(auditRow.Item("ProductCommission")), 0, auditRow.Item("ProductCommission")) + If(IsDBNull(auditRow.Item("InterestCommission")), 0, auditRow.Item("InterestCommission"))
    '                auditString = auditString & "Product commission: " & Format(If(IsDBNull(auditRow.Item("ProductCommission")), 0, auditRow.Item("ProductCommission")), "C") & "  |  Interest commission: " & Format(If(IsDBNull(auditRow.Item("InterestCommission")), 0, auditRow.Item("InterestCommission")), "C") & vbCrLf & "Commission retention: " & If(IsDBNull(auditRow.Item("CommRetention")), 0, auditRow.Item("CommRetention")) & "%" & vbCrLf & "Total commission: " & Format(totalCommission, "C") & vbCrLf
    '                If If(IsDBNull(auditRow.Item("CommFiguresChecked")), False, auditRow.Item("CommFiguresChecked")) = True Then
    '                    auditString = auditString & "Dealer commission figures have been checked." & vbCrLf
    '                Else
    '                    auditString = auditString & "Dealer commission figures have NOT been checked!" & vbCrLf
    '                End If
    '            End If
    '            'documents
    '            auditString = auditString & "--------------------  Documents:" & vbCrLf
    '            If If(IsDBNull(auditRow.Item("Originals")), False, auditRow.Item("Originals")) = True Then
    '                auditString = auditString & "All pages of original contract are clear and legible - Yes" & vbCrLf
    '            Else
    '                auditString = auditString & "All pages of original contract are clear and legible - No" & vbCrLf
    '            End If
    '            If If(IsDBNull(auditRow.Item("Signed")), False, auditRow.Item("Signed")) = True Then
    '                auditString = auditString & "All pages are signed or initialled correctly where appropiate - Yes" & vbCrLf
    '            Else
    '                auditString = auditString & "All pages are signed or initialled correctly where appropiate - No" & vbCrLf
    '            End If

    '            If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                If If(IsDBNull(auditRow.Item("Application")), False, auditRow.Item("Application")) = True Then
    '                    auditString = auditString & "Received original application; completed and signed correctly - Yes" & vbCrLf
    '                Else
    '                    auditString = auditString & "Received original application; completed and signed correctly - No" & vbCrLf
    '                End If
    '            End If
    '            If If(IsDBNull(auditRow.Item("SpecialConditions")), False, auditRow.Item("SpecialConditions")) = True Then
    '                auditString = auditString & "All special conditions are met - Yes" & vbCrLf
    '            Else
    '                auditString = auditString & "All special conditions are met - No" & vbCrLf
    '            End If

    '            If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                If If(IsDBNull(auditRow.Item("Documents")), False, auditRow.Item("Documents")) = True Then
    '                    auditString = auditString & "All supporting documents are clear and legible - Yes" & vbCrLf
    '                Else
    '                    auditString = auditString & "All supporting documents are clear and legible - No" & vbCrLf
    '                End If
    '            End If
    '            'processing
    '            auditString = auditString & "--------------------  Processing:" & vbCrLf
    '            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '                If If(IsDBNull(auditRow.Item("DDinPlace")), False, auditRow.Item("DDinPlace")) = True Then
    '                    auditString = auditString & "DD is already inplace (Variation)" & vbCrLf
    '                End If
    '                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                    auditString = auditString & "DD / AP Form Sheet number - " & If(IsDBNull(auditRow.Item("APDDShtNum")), String.Empty, auditRow.Item("APDDShtNum")) & vbCrLf
    '                    If If(IsDBNull(auditRow.Item("BanckAcc")), False, auditRow.Item("BanckAcc")) = True Then
    '                        auditString = auditString & "Bank Acc No in finPower checked - Yes"
    '                    Else
    '                        auditString = auditString & "Bank Acc No in finPower checked - No "
    '                    End If
    '                    If If(IsDBNull(auditRow.Item("PayBookSent")), False, auditRow.Item("PayBookSent")) = True Then
    '                        auditString = auditString & "                           Payment book given to client - Yes" & vbCrLf
    '                        'for use in payout - final
    '                        auditPayBookSentSwitch = True
    '                    Else
    '                        auditString = auditString & "                          Payment book given to client - No" & vbCrLf
    '                    End If
    '                    If auditRow.APDDClientFiled = True Then
    '                        auditString = auditString & "Client has setup themselves and the Agreement Form has been filed." & vbCrLf
    '                    End If
    '                    auditString = auditString & "Payment method has been checked in finPower? "
    '                    If If(IsDBNull(auditRow.Item("PayMethod")), False, auditRow.Item("PayMethod")) = True Then
    '                        auditString = auditString & "Yes" & vbCrLf
    '                    Else
    '                        auditString = auditString & "No" & vbCrLf
    '                    End If
    '                    auditString = auditString & "Wage deduction has been confirmed? "
    '                    If If(IsDBNull(auditRow.Item("WageDeduction")), False, auditRow.Item("WageDeduction")) = True Then
    '                        auditString = auditString & "Yes" & vbCrLf
    '                    Else
    '                        auditString = auditString & "No" & vbCrLf
    '                    End If
    '                    'Documents
    '                    auditString = auditString & "Documents scanned and/or saved: "
    '                    auditString = auditString & "IDs - "
    '                    If If(IsDBNull(auditRow.Item("IDsaved")), False, auditRow.Item("IDsaved")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    auditString = auditString & "                   DD / AP - "
    '                    If If(IsDBNull(auditRow.Item("DDAPsaved")), False, auditRow.Item("DDAPsaved")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    auditString = auditString & "                   Application - "
    '                    If If(IsDBNull(auditRow.Item("AppSaved")), False, auditRow.Item("AppSaved")) = True Then
    '                        auditString = auditString & "Yes" & vbCrLf
    '                    Else
    '                        auditString = auditString & "No " & vbCrLf
    '                    End If
    '                    auditString = auditString & "                                    Proof of address - "
    '                    If If(IsDBNull(auditRow.Item("ProofAddress")), False, auditRow.Item("ProofAddress")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    auditString = auditString & "                   Proof of income - "
    '                    If If(IsDBNull(auditRow.Item("ProofIncome")), False, auditRow.Item("ProofIncome")) = True Then
    '                        auditString = auditString & "Yes" & vbCrLf
    '                    Else
    '                        auditString = auditString & "No " & vbCrLf
    '                    End If
    '                End If

    '                'finPower  
    '                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                    auditString = auditString & "All fields are filled in and correct: Client - "
    '                    If If(IsDBNull(auditRow.Item("finClient")), False, auditRow.Item("finClient")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    auditString = auditString & "     Loan - "
    '                    If If(IsDBNull(auditRow.Item("finLoan")), False, auditRow.Item("finLoan")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    auditString = auditString & "     Security - "
    '                    If If(IsDBNull(auditRow.Item("finSecurity")), False, auditRow.Item("finSecurity")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    auditString = auditString & "     Graded - "
    '                    If If(IsDBNull(auditRow.Item("finGrading")), False, auditRow.Item("finGrading")) = True Then
    '                        auditString = auditString & "Yes" & vbCrLf
    '                    Else
    '                        auditString = auditString & "No " & vbCrLf
    '                    End If
    '                    'PPSR registered
    '                    auditString = auditString & "PPSR registered: "
    '                    If If(IsDBNull(auditRow.Item("PPSRNew")), False, auditRow.Item("PPSRNew")) = True Then
    '                        auditString = auditString & "New"
    '                    ElseIf If(IsDBNull(auditRow.Item("PPSRAmended")), False, auditRow.Item("PPSRAmended")) = True Then
    '                        auditString = auditString & "Amended"
    '                    Else
    '                        auditString = auditString & "Not registered"
    '                    End If
    '                    If If(IsDBNull(auditRow.Item("PPSRFiled")), False, auditRow.Item("PPSRFiled")) = True Then
    '                        auditString = auditString & "               F/S PIN - printed and saved" & vbCrLf
    '                    Else
    '                        auditString = auditString & "               F/S PIN - not processed" & vbCrLf
    '                    End If
    '                    auditString = auditString & "Financial Statement has been checked and is correct:" & vbCrLf & "Debtor - "
    '                    If If(IsDBNull(auditRow.Item("FSDebtor")), False, auditRow.Item("FSDebtor")) = True Then
    '                        auditString = auditString & "Yes"
    '                    Else
    '                        auditString = auditString & "No "
    '                    End If
    '                    If If(IsDBNull(auditRow.Item("FSType")), False, auditRow.Item("FSType")) = True Then
    '                        auditString = auditString & "               Collateral Type - Yes"
    '                    Else
    '                        auditString = auditString & "               Collateral Type - No "
    '                    End If
    '                    If If(IsDBNull(auditRow.Item("FSDesc")), False, auditRow.Item("FSDesc")) = True Then
    '                        auditString = auditString & "               Description - Yes" & vbCrLf
    '                    Else
    '                        auditString = auditString & "               Description - No " & vbCrLf
    '                    End If
    '                    'Protecta
    '                    auditString = auditString & "Protecta: "
    '                    If If(IsDBNull(auditRow.Item("NoProtectaPolicies")), False, auditRow.Item("NoProtectaPolicies")) = True Then
    '                        auditString = auditString & "There are no Protecta policies" & vbCrLf
    '                    Else
    '                        Dim lineBreakVar As Boolean = False
    '                        If If(IsDBNull(auditRow.Item("InsuranceIdentified")), False, auditRow.Item("InsuranceIdentified")) = True Then
    '                            auditString = auditString & "Insurance has been identified in OAC Insurances Report"
    '                        Else
    '                            auditString = auditString & "Insurance has NOT been identified in OAC Insurances Report" & vbCrLf
    '                        End If
    '                        auditString = auditString & "  entered for - "
    '                        If If(IsDBNull(auditRow.Item("CPI")), False, auditRow.Item("CPI")) = True Then
    '                            auditString = auditString & "CPI,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("PPIfull")), False, auditRow.Item("PPIfull")) = True Then
    '                            auditString = auditString & "PPI full,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("PPIDO")), False, auditRow.Item("PPIDO")) = True Then
    '                            auditString = auditString & "PPI Death only,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("GAP1")), False, auditRow.Item("GAP1")) = True Then
    '                            auditString = auditString & "GAP option1,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("GAP2")), False, auditRow.Item("GAP2")) = True Then
    '                            auditString = auditString & "GAP option2,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("MechWarr")), False, auditRow.Item("MechWarr")) = True Then
    '                            auditString = auditString & "Mech Warranty,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("InsuranceV")), False, auditRow.Item("InsuranceV")) = True Then
    '                            auditString = auditString & "Vehicle insurance,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("InsuranceC")), False, auditRow.Item("InsuranceC")) = True Then
    '                            auditString = auditString & "Chattels insurance,  "
    '                            lineBreakVar = True
    '                        End If
    '                        If lineBreakVar = True Then
    '                            auditString = auditString & vbCrLf
    '                        End If
    '                        If Not IsDBNull(auditRow.Item("SpecialNotes")) Then
    '                            If auditRow.Item("SpecialNotes").Length > 0 Then
    '                                auditString = auditString & " Special notes: " & auditRow.Item("SpecialNotes") & vbCrLf
    '                            End If
    '                        End If

    '                    End If
    '                    'insurances
    '                    auditString = auditString & "Insurances: "
    '                    If If(IsDBNull(auditRow.Item("SecurityInsured")), False, auditRow.Item("SecurityInsured")) = True Then
    '                        auditString = auditString & "Security insured and YFL interest noted" & vbCrLf
    '                    ElseIf If(IsDBNull(auditRow.Item("NoInsuranceRequired")), False, auditRow.Item("NoInsuranceRequired")) = True Then
    '                        auditString = auditString & "No insurance is required" & vbCrLf
    '                    End If
    '                End If
    '                'disbursements
    '                auditString = auditString & "--------------------  Disbursements:" & vbCrLf
    '                If If(IsDBNull(auditRow.Item("DisburseAmt1")), 0, auditRow.Item("DisburseAmt1")) > 0 Then
    '                    auditString = auditString & auditRow.Item("DisburseName1") & ",  " & auditRow.Item("DisburseAcc1") & ",  " & auditRow.Item("DisburseRef1") & ",  " & Format(auditRow.Item("DisburseAmt1"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(auditRow.Item("DisburseAmt2")), 0, auditRow.Item("DisburseAmt2")) > 0 Then
    '                    auditString = auditString & auditRow.Item("DisburseName2") & ",  " & auditRow.Item("DisburseAcc2") & ",  " & auditRow.Item("DisburseRef2") & ",  " & Format(auditRow.Item("DisburseAmt2"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(auditRow.Item("DisburseAmt3")), 0, auditRow.Item("DisburseAmt3")) > 0 Then
    '                    auditString = auditString & auditRow.Item("DisburseName3") & ",  " & auditRow.Item("DisburseAcc3") & ",  " & auditRow.Item("DisburseRef3") & ",  " & Format(auditRow.Item("DisburseAmt3"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(auditRow.Item("DisburseAmt4")), 0, auditRow.Item("DisburseAmt4")) > 0 Then
    '                    auditString = auditString & auditRow.Item("DisburseName4") & ",  " & auditRow.Item("DisburseAcc4") & ",  " & auditRow.Item("DisburseRef4") & ",  " & Format(auditRow.Item("DisburseAmt4"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(auditRow.Item("DisburseAmt5")), 0, auditRow.Item("DisburseAmt5")) > 0 Then
    '                    auditString = auditString & auditRow.Item("DisburseName5") & ",  " & auditRow.Item("DisburseAcc5") & ",  " & auditRow.Item("DisburseRef5") & ",  " & Format(auditRow.Item("DisburseAmt5"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(auditRow.Item("DisburseAmt6")), 0, auditRow.Item("DisburseAmt6")) > 0 Then
    '                    auditString = auditString & auditRow.Item("DisburseName6") & ",  " & auditRow.Item("DisburseAcc6") & ",  " & auditRow.Item("DisburseRef6") & ",  " & Format(auditRow.Item("DisburseAmt6"), "C") & vbCrLf
    '                End If
    '                auditString = auditString & "Total amount financed =  " & Format(If(IsDBNull(auditRow.Item("DisburseAmtTotal")), 0, auditRow.Item("DisburseAmtTotal")), "C") & vbCrLf
    '                auditString = auditString & "--------------------  Final:" & vbCrLf
    '                'Final
    '                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                    If If(IsDBNull(auditRow.Item("finPowerOpen")), False, auditRow.Item("finPowerOpen")) = True Then
    '                        auditString = auditString & "FinPower Loan has been opened." & vbCrLf
    '                        If If(IsDBNull(auditRow.Item("WelcomeLetter")), False, auditRow.Item("WelcomeLetter")) = True Then
    '                            auditString = auditString & "Welcome letter has been sent."
    '                        Else
    '                            auditString = auditString & "Welcome letter has NOT been sent."
    '                        End If
    '                        If If(IsDBNull(auditRow.Item("ProtectaBook")), False, auditRow.Item("ProtectaBook")) = True Then
    '                            auditString = auditString & "                    Protecta books have been sent." & vbCrLf
    '                        Else
    '                            auditString = auditString & "                    Protecta books have NOT been sent." & vbCrLf
    '                        End If
    '                    Else
    '                        auditString = auditString & "FinPower Loan has NOT been opened." & vbCrLf
    '                    End If
    '                Else
    '                    If If(IsDBNull(auditRow.Item("finPowerWithdrawal")), False, auditRow.Item("finPowerWithdrawal")) = True Then
    '                        auditString = auditString & "FinPower withdrawal has been completed." & vbCrLf
    '                    Else
    '                        auditString = auditString & "FinPower withdrawal has NOT been completed." & vbCrLf
    '                    End If
    '                End If
    '            Else 'thisEnquiryType = MyEnums.EnquiryType.FinanceFacility
    '                'Documents
    '                auditString = auditString & "Documents scanned and/or saved: "
    '                auditString = auditString & "IDs - "
    '                If If(IsDBNull(auditRow.Item("IDsaved")), False, auditRow.Item("IDsaved")) = True Then
    '                    auditString = auditString & "Yes"
    '                Else
    '                    auditString = auditString & "No "
    '                End If
    '                auditString = auditString & "                   Evaluation sheet - "
    '                If If(IsDBNull(auditRow.Item("DDAPsaved")), False, auditRow.Item("DDAPsaved")) = True Then
    '                    auditString = auditString & "Yes"
    '                Else
    '                    auditString = auditString & "No "
    '                End If
    '                auditString = auditString & "                   Dealer Agreement - "
    '                If If(IsDBNull(auditRow.Item("AppSaved")), False, auditRow.Item("AppSaved")) = True Then
    '                    auditString = auditString & "Yes" & vbCrLf
    '                Else
    '                    auditString = auditString & "No " & vbCrLf
    '                End If
    '                auditString = auditString & "                                    Deposit slip - "
    '                If If(IsDBNull(auditRow.Item("ProofAddress")), False, auditRow.Item("ProofAddress")) = True Then
    '                    auditString = auditString & "Yes"
    '                Else
    '                    auditString = auditString & "No "
    '                End If
    '                auditString = auditString & "                   Proof of income - "
    '                If If(IsDBNull(auditRow.Item("ProofIncome")), False, auditRow.Item("ProofIncome")) = True Then
    '                    auditString = auditString & "Yes" & vbCrLf
    '                Else
    '                    auditString = auditString & "No " & vbCrLf
    '                End If
    '                'insurances
    '                auditString = auditString & "Insurances: "
    '                If If(IsDBNull(auditRow.Item("SecurityInsured")), False, auditRow.Item("SecurityInsured")) = True Then
    '                    auditString = auditString & "Indemnity insurance and YFL interest noted" & vbCrLf
    '                ElseIf If(IsDBNull(auditRow.Item("NoInsuranceRequired")), False, auditRow.Item("NoInsuranceRequired")) = True Then
    '                    auditString = auditString & "No insurance is required" & vbCrLf
    '                End If
    '                'Final
    '                If If(IsDBNull(auditRow.Item("finPowerOpen")), False, auditRow.Item("finPowerOpen")) = True Then
    '                    auditString = auditString & "Dealer has been setup in FinPower." & vbCrLf
    '                Else
    '                    auditString = auditString & "Dealer has NOT been setup in FinPower." & vbCrLf
    '                End If
    '                If If(IsDBNull(auditRow.Item("WelcomeLetter")), False, auditRow.Item("WelcomeLetter")) = True Then
    '                    auditString = auditString & "Details have been sent to the Accountant."
    '                Else
    '                    auditString = auditString & "Details have NOT been sent to the Accountant."
    '                End If

    '            End If
    '        Else
    '            'DueDiligenceBindingSource.Current Is Nothing 
    '            auditString = auditString & "There is no Audit information." & vbCrLf
    '        End If
    '        'add to constructedString
    '        If auditString.Length > 0 Then
    '            constructedString = constructedString & auditString
    '        End If
    '        '******* End of Audit ****************
    '        '********** Payout *******************
    '        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '            Dim payoutString As String = ""
    '            payoutString = vbCrLf & "*****  PAYOUT INFORMATION  *******************************************" & vbCrLf
    '            'test Bindingsource.Current Is Nothing
    '            If Not PayoutBindingSource.Current Is Nothing Then
    '                'Create Payout row
    '                Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
    '                'Get Row in Payout dataset
    '                payoutRow = Me.EnquiryWorkSheetDataSet.Payout(0)
    '                'processing
    '                payoutString = payoutString & "--------------------  Processing:" & vbCrLf
    '                If If(IsDBNull(payoutRow.Item("TraderLoansLoaded")), False, payoutRow.Item("TraderLoansLoaded")) = True Then
    '                    payoutString = payoutString & "Loan figures in Audit are correct." & vbCrLf
    '                Else
    '                    payoutString = payoutString & "Loan figures in Audit have not been checked." & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("VerificationStatement")), False, payoutRow.Item("VerificationStatement")) = True Then
    '                    payoutString = payoutString & "The PPSR Verification Statement has been viewed and is correct." & vbCrLf
    '                Else
    '                    payoutString = payoutString & "The PPSR Verification Statement has NOT been viewed." & vbCrLf
    '                End If
    '                payoutString = payoutString & "Payout month is " & If(IsDBNull(payoutRow.Item("LoanInMonthOf")), String.Empty, payoutRow.Item("LoanInMonthOf")) & "  " & If(IsDBNull(payoutRow.Item("LoanInYearOf")), String.Empty, payoutRow.Item("LoanInYearOf")) & vbCrLf
    '                'payout
    '                payoutString = payoutString & "--------------------  Payout:" & vbCrLf
    '                If If(IsDBNull(payoutRow.Item("PayAmt1")), 0, payoutRow.Item("PayAmt1")) > 0 Then
    '                    payoutString = payoutString & payoutRow.Item("PayeeName1") & ",  " & payoutRow.Item("PayeeRef1") & ",  " & Format(payoutRow.Item("PayAmt1"), "C") & ",  " & payoutRow.Item("Cheque1") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("PayAmt2")), 0, payoutRow.Item("PayAmt2")) > 0 Then
    '                    payoutString = payoutString & payoutRow.Item("PayeeName2") & ",  " & payoutRow.Item("PayeeRef2") & ",  " & Format(payoutRow.Item("PayAmt2"), "C") & ",  " & payoutRow.Item("Cheque2") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("PayAmt3")), 0, payoutRow.Item("PayAmt3")) > 0 Then
    '                    payoutString = payoutString & payoutRow.Item("PayeeName3") & ",  " & payoutRow.Item("PayeeRef3") & ",  " & Format(payoutRow.Item("PayAmt3"), "C") & ",  " & payoutRow.Item("Cheque3") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("PayAmt4")), 0, payoutRow.Item("PayAmt4")) > 0 Then
    '                    payoutString = payoutString & payoutRow.Item("PayeeName4") & ",  " & payoutRow.Item("PayeeRef4") & ",  " & Format(payoutRow.Item("PayAmt4"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("PayAmt5")), 0, payoutRow.Item("PayAmt5")) > 0 Then
    '                    payoutString = payoutString & payoutRow.Item("PayeeName5") & ",  " & payoutRow.Item("PayeeRef5") & ",  " & Format(payoutRow.Item("PayAmt5"), "C") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("PayAmt6")), 0, payoutRow.Item("PayAmt6")) > 0 Then
    '                    payoutString = payoutString & payoutRow.Item("PayeeName6") & ",  " & payoutRow.Item("PayeeRef6") & ",  " & Format(payoutRow.Item("PayAmt6"), "C") & vbCrLf
    '                End If
    '                'Dealer commission
    '                If If(IsDBNull(payoutRow.Item("CommissionAmt")), 0, payoutRow.Item("CommissionAmt")) > 0 Then
    '                    payoutString = payoutString & "Dealer commission,  " & ",  " & payoutRow.Item("CommissionRef") & ",  " & Format(payoutRow.Item("CommissionAmt"), "C") & vbCrLf
    '                End If
    '                'Interest holdback (for Interest free deal)
    '                If If(IsDBNull(payoutRow.Item("HoldbackAmt")), 0, payoutRow.Item("HoldbackAmt")) > 0 Then
    '                    payoutString = payoutString & "Holdback Interest,  " & payoutRow.Item("HoldbackRef") & ",  " & payoutRow.Item("HoldbackAmt") & vbCrLf
    '                End If
    '                'Loan/Commission retention
    '                If If(IsDBNull(payoutRow.Item("RetentionAmt")), 0, payoutRow.Item("RetentionAmt")) > 0 Then
    '                    payoutString = payoutString & "Loan and Commission Retention,  " & payoutRow.Item("RetentionRef") & ",  " & payoutRow.Item("RetentionAmt") & vbCrLf
    '                End If
    '                Dim totalPaidOut As Decimal = _
    '                    If(IsDBNull(payoutRow.Item("PayAmt1")), 0, payoutRow.Item("PayAmt1")) + _
    '                    If(IsDBNull(payoutRow.Item("PayAmt2")), 0, payoutRow.Item("PayAmt2")) + _
    '                    If(IsDBNull(payoutRow.Item("PayAmt3")), 0, payoutRow.Item("PayAmt3")) + _
    '                    If(IsDBNull(payoutRow.Item("PayAmt4")), 0, payoutRow.Item("PayAmt4")) + _
    '                    If(IsDBNull(payoutRow.Item("PayAmt5")), 0, payoutRow.Item("PayAmt5")) + _
    '                    If(IsDBNull(payoutRow.Item("PayAmt6")), 0, payoutRow.Item("PayAmt6")) + _
    '                    If(IsDBNull(payoutRow.Item("CommissionAmt")), 0, payoutRow.Item("CommissionAmt")) + _
    '                    If(IsDBNull(payoutRow.Item("HoldbackAmt")), 0, payoutRow.Item("HoldbackAmt")) + _
    '                    If(IsDBNull(payoutRow.Item("RetentionAmt")), 0, payoutRow.Item("RetentionAmt"))
    '                payoutString = payoutString & "Total paid out: " & Format(totalPaidOut, "C")
    '                If If(IsDBNull(payoutRow.Item("TotalCheque")), False, payoutRow.Item("TotalCheque")) = True Then
    '                    payoutString = payoutString & "                    Total paid by cheque" & vbCrLf
    '                Else
    '                    payoutString = payoutString & "                    ANZ Batch Number:  " & payoutRow.Item("BatchNum") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("RefinanceAmt1")), 0, payoutRow.Item("RefinanceAmt1")) > 0 Then
    '                    payoutString = payoutString & "Refinanced Ref:  " & payoutRow.Item("RefinanceRef1") & ",  " & payoutRow.Item("RefinanceAmt1") & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("RefinanceAmt2")), 0, payoutRow.Item("RefinanceAmt2")) > 0 Then
    '                    payoutString = payoutString & "Refinanced Ref:  " & payoutRow.Item("RefinanceRef2") & ",  " & payoutRow.Item("RefinanceAmt2") & vbCrLf
    '                End If
    '                Dim totalFinAmt As Decimal = If(IsDBNull(payoutRow.Item("RefinanceAmt1")), 0, payoutRow.Item("RefinanceAmt1")) + If(IsDBNull(payoutRow.Item("RefinanceAmt2")), 0, payoutRow.Item("RefinanceAmt2")) + totalPaidOut
    '                payoutString = payoutString & "Total financed amount:  " & Format(totalFinAmt, "C") & vbCrLf
    '                If payoutRow.Item("BankProcessed") = True Then
    '                    payoutString = payoutString & "ANZ processed and/or all cheques issued: Yes                    Date:  " & payoutRow.Item("ProcessDate") & vbCrLf
    '                Else
    '                    payoutString = payoutString & "ANZ processed and/or all cheques issued: No " & vbCrLf
    '                End If
    '                If payoutRow.Item("CashBook") = True Then
    '                    payoutString = payoutString & "Cheques and/or Batch entered into Cash Book: Yes" & vbCrLf
    '                Else
    '                    payoutString = payoutString & "Cheques and/or Batch entered into Cash Book: No " & vbCrLf
    '                End If
    '                If payoutRow.Item("PayAdvice") = True Then
    '                    payoutString = payoutString & "Pay advice faxed / emailed to Dealer: Yes                    Date:  " & payoutRow.Item("AdviceDate") & vbCrLf
    '                Else
    '                    payoutString = payoutString & "Pay advice faxed / emailed to Dealer: No " & vbCrLf
    '                End If
    '                If If(IsDBNull(payoutRow.Item("RetentionAmt")), 0, payoutRow.Item("RetentionAmt")) > 0 Then
    '                    If payoutRow.Item("RetentionEntered") = True Then
    '                        payoutString = payoutString & "Retention entered into Dealer trust account: Yes" & vbCrLf
    '                    Else
    '                        payoutString = payoutString & "Retention entered into Dealer trust account: No " & vbCrLf
    '                    End If
    '                End If

    '                'If Not ThisLoanReason = Constants.CONST_PRELIMREASON_SPLIT_LOAN_DRAWDOWN Then
    '                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
    '                    'refinanced accounts
    '                    payoutString = payoutString & "--------------------  Refinanced accounts:" & vbCrLf
    '                    If If(IsDBNull(payoutRow.Item("RefinanceAmt1")), 0, payoutRow.Item("RefinanceAmt1")) > 0 Or If(IsDBNull(payoutRow.Item("RefinanceAmt2")), 0, payoutRow.Item("RefinanceAmt2")) > 0 Then
    '                        If payoutRow.Item("ClosedFinPower") = True Then
    '                            payoutString = payoutString & "Account/s have been closed in finPower" & vbCrLf
    '                        Else
    '                            payoutString = payoutString & "Account/s have been NOT closed in finPower" & vbCrLf
    '                        End If
    '                        If payoutRow.Item("PPSRDischarged") = True Or payoutRow.Item("PPSRAmended") = True Then
    '                            If payoutRow.Item("PPSRDischarged") = True Then
    '                                payoutString = payoutString & "PPSR has been discharged." & vbCrLf
    '                            End If
    '                            If payoutRow.Item("PPSRAmended") = True Then
    '                                payoutString = payoutString & "PPSR has been amended." & vbCrLf
    '                            End If
    '                        Else
    '                            payoutString = payoutString & "PPSR has not been processed." & vbCrLf
    '                        End If
    '                        If payoutRow.Item("ProtectaClaimBack") = True Then
    '                            payoutString = payoutString & "Protecta Claim Back has been processed.          Sheet number - " & payoutRow.Item("ProtectaShtNum") & vbCrLf
    '                        Else
    '                            payoutString = payoutString & "Protecta Claim Back has NOT been processed." & vbCrLf
    '                        End If
    '                        If payoutRow.Item("CommRecall") = True Then
    '                            payoutString = payoutString & "Commission Recall has been processed.          Sheet number - " & payoutRow.Item("CommShtNum") & vbCrLf
    '                        Else
    '                            payoutString = payoutString & "Commission Recall has NOT been processed." & vbCrLf
    '                        End If
    '                    Else
    '                        payoutString = payoutString & "No Refinance information" & vbCrLf
    '                    End If
    '                    'Final
    '                    payoutString = payoutString & "--------------------  Final:" & vbCrLf
    '                    If auditPayBookSentSwitch = False Then
    '                        If payoutRow.Item("DDSafe") = True Then
    '                            payoutString = payoutString & "DD filed in safe."
    '                        Else
    '                            payoutString = payoutString & "DD NOT filed in safe."
    '                        End If
    '                    End If
    '                    If payoutRow.Item("ClosedFinPower") = True Then
    '                        If payoutRow.Item("Archived") = True Then
    '                            payoutString = payoutString & "Accounts closed have been filed in the Archives" & vbCrLf
    '                        Else
    '                            payoutString = payoutString & "Accounts closed have NOT been filed in the Archives" & vbCrLf
    '                        End If
    '                    End If
    '                End If

    '            Else 'PayoutBindingSource.Current Is Nothing 
    '                payoutString = payoutString & "There is no Payout information." & vbCrLf
    '            End If
    '            'add to constructedString
    '            If payoutString.Length > 0 Then
    '                constructedString = constructedString & payoutString
    '            End If
    '        End If

    '        '******* End of Payout ***************
    '        '******** Document Info **************
    '        'get list of documents in workfolder
    '        'Get String to WorkSheet Drive
    '        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
    '        Dim fileString As String = String.Empty
    '        fileString = Util.GetDocumentsList(worksheetDrive, thisEnquiryCode)
    '        'add to constructedString
    '        If fileString.Length > 0 Then
    '            constructedString = constructedString & vbCrLf & fileString
    '        End If

    '        '******End of Document Info ************
    '        '*********** Printing Info *************
    '        If printPrintedBy = True Then
    '            Dim printInfoString As String
    '            printInfoString = vbCrLf & vbCrLf & vbCrLf & "*****  Printed by: " & LoggedinName & vbCrLf & DateTime.Now.ToString
    '            'add to constructedString
    '            If printInfoString.Length > 0 Then
    '                constructedString = constructedString & printInfoString
    '            End If
    '        End If

    '        '******** End of Printing Info *********
    '        'MsgBox(constructedString)
    '        '**************** Return value **************
    '        ourPrintString.StringOut = constructedString
    '        ourPrintString.Result = True
    '    Catch ex As Exception
    '        'Display error message
    '        MessageBox.Show("Function ConstructPrintStringAll caused an error:" & vbCrLf & ex.Message)
    '        ourPrintString.Result = False
    '    End Try

    '    Return ourPrintString

    'End Function




#End Region


    '************** Export *********************************
    ' ''' <summary>
    ' ''' Move Customer and Loan documents to finPower folders 
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Async Sub btnExportFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportFiles.Click
    '    Me.tbctrlAppDetails.SelectedTab = tabPayout
    '    Cursor.Current = Cursors.WaitCursor
    '    Dim custId As Integer
    '    Dim custIdNumber As String
    '    Dim custName As String
    '    Dim enqCustType As Integer
    '    'get file path
    '    Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
    '    Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
    '    Dim finPowerDrive As String = Switch.GetFinPowerSharedFolder & "\"
    '    'Printout
    '    Dim stringToSave As String
    '    Dim enquiryPrintOut As Util.StringOut
    '    Dim commentId As Integer
    '    Dim progressMsg As String = String.Empty
    '    'Enquiry
    '    Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
    '    Dim originalTime As DateTime
    '    Dim StatusTimeDiff As Integer
    '    Dim CommentStr As String

    '    '******** Financefacility bypasses payout so must create Enquiry text file here **************
    '    If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '        'run GetPrintStringAll() to get notes
    '        enquiryPrintOut = Util.GetPrintStringAll(thisEnquiryId, True)
    '        If enquiryPrintOut.Result Then
    '            stringToSave = enquiryPrintOut.StringOut
    '        Else
    '            'Error creating Print String, exit sub
    '            MessageBox.Show("Error creating Print String" & vbCrLf & "Export aborted! Reverted to Payout", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            FormRefresh()
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error creating Print String"
    '            Exit Sub
    '        End If
    '        'Created print string successful, now write string to folder
    '        Dim fileName As String
    '        fileName = "TrueTrackWorkSheet-" & thisEnquiryCode & ".txt"
    '        'write to file
    '        Try
    '            Dim sb As New StringBuilder()
    '            sb.AppendLine(stringToSave)
    '            Using outfile As New StreamWriter(folderPath & fileName, False) 'overwrite file if exits
    '                outfile.Write(sb.ToString())
    '                outfile.Flush()
    '                outfile.Close()
    '            End Using
    '            'set file permission to read only
    '            File.SetAttributes(folderPath & fileName, FileAttributes.ReadOnly)
    '            progressMsg = progressMsg & vbCrLf & "Successfully saved TrueTrackWorkSheet-" & thisEnquiryCode & ".txt"
    '        Catch ex As UnauthorizedAccessException
    '            MsgBox("ERROR: Saving file caused an error. Access is denied." & vbCrLf & ex.Message)
    '            FormRefresh()
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. Access is denied."
    '            Exit Sub
    '        Catch ex As DirectoryNotFoundException
    '            MsgBox("ERROR: Saving file caused an error. The specified path is invalid." & vbCrLf & ex.Message)
    '            FormRefresh()
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. The specified path is invalid."
    '            Exit Sub
    '        Catch ex As SecurityException
    '            MsgBox("ERROR: Saving file caused an error. The caller does not have the required permission." & vbCrLf & ex.Message)
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. The caller does not have the required permission."
    '            Exit Sub
    '        Catch ex As Exception
    '            MsgBox("ERROR: Saving file caused an error." & vbCrLf & ex.Message)
    '            FormRefresh()
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error."
    '            Exit Sub
    '        End Try
    '    Else
    '        'Not Financefacility
    '        'Validate payout data
    '        If Not String.IsNullOrEmpty(CheckPayoutData()) Then
    '            MsgBox(CheckPayoutData())
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Black
    '            ToolStripStatusLabel1.Text = "Status: Ready."
    '            Exit Sub
    '        Else
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Blue
    '            ToolStripStatusLabel1.Text = "Status: passed Payout validation"
    '            progressMsg = progressMsg & vbCrLf & "Successfully passed Payout validation"
    '            Application.DoEvents()
    '            'Test folderpath exists first
    '            If Directory.Exists(folderPath) Then
    '                'Update all fields, contruct string and save to text file on Shared drive folder
    '                'Test construction of print string first, exit on error
    '                enquiryPrintOut = Util.GetPrintStringAll(thisEnquiryId)
    '                If enquiryPrintOut.Result Then
    '                    stringToSave = enquiryPrintOut.StringOut
    '                    progressMsg = progressMsg & vbCrLf & "Successfully tested printout creation"
    '                Else
    '                    MessageBox.Show("Error creating Print String", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                    Exit Sub
    '                End If
    '                'Update fields
    '                'Payout
    '                Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
    '                payoutRow = Me.EnquiryWorkSheetDataSet.Payout(0)
    '                payoutRow.BeginEdit()
    '                payoutRow.PayoutCompleted = Date.Now
    '                Try
    '                    Me.Validate()
    '                    Me.PayoutBindingSource.EndEdit()
    '                    Me.PayoutTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Payout)
    '                    progressMsg = progressMsg & vbCrLf & "Successfully updated Payout in database"
    '                Catch ex As Exception
    '                    MsgBox("ERROR: Updating of Payout fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "ERROR: Updating of Payout fields caused an error."
    '                    Exit Sub
    '                End Try
    '                ''Enquiry
    '                'enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '                'enquiryRow.BeginEdit()
    '                ''update Current Status
    '                'enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED
    '                'enquiryRow.CurrentStatusSetDate = DateTime.Now
    '                ''Set Original Enquiry Time
    '                'originalTime = enquiryRow.DateTime
    '                ''calculate time difference for 30min status
    '                'StatusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
    '                'If StatusTimeDiff < 30 Then
    '                '    enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_COMPLETED
    '                'End If
    '                ''update Enquiry
    '                'Try
    '                '    Me.Validate()
    '                '    Me.EnquiryBindingSource.EndEdit()
    '                '    Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
    '                'Catch ex As Exception
    '                '    MsgBox("ERROR: Updating of Enquiry fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
    '                '    FormRefresh()
    '                '    'update Status Strip
    '                '    ToolStripStatusLabel1.ForeColor = Color.Red
    '                '    ToolStripStatusLabel1.Text = "ERROR: Updating of Enquiry fields caused an error."
    '                '    Exit Sub
    '                'End Try

    '                'run GetPrintStringAll() again to get latest comments
    '                enquiryPrintOut = Util.GetPrintStringAll(thisEnquiryId)
    '                If enquiryPrintOut.Result Then
    '                    stringToSave = enquiryPrintOut.StringOut
    '                Else
    '                    'set Status back to Payout
    '                    MessageBox.Show("Error creating Print String" & vbCrLf & "Reverted to Payout", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                    ResetStatus(Constants.CONST_CURRENT_STATUS_PAYOUT)
    '                    FormRefresh()
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "Error creating Print String"
    '                    Exit Sub
    '                End If


    '                Dim fileName As String
    '                'construct file name
    '                fileName = "TrueTrackWorkSheet-" & thisEnquiryCode & ".txt"
    '                'write to file
    '                Try
    '                    Dim sb As New StringBuilder()
    '                    sb.AppendLine(stringToSave)
    '                    Using outfile As New StreamWriter(folderPath & fileName, False) 'overwrite file if exits
    '                        outfile.Write(sb.ToString())
    '                        outfile.Flush()
    '                        outfile.Close()
    '                    End Using
    '                    'set file permission to read only
    '                    File.SetAttributes(folderPath & fileName, FileAttributes.ReadOnly)
    '                    ToolStripStatusLabel1.ForeColor = Color.Blue
    '                    ToolStripStatusLabel1.Text = "Saving Enquiry Summary file was successful."
    '                    progressMsg = progressMsg & vbCrLf & "Saving Enquiry Summary file was successful."
    '                    Application.DoEvents()
    '                Catch ex As UnauthorizedAccessException
    '                    MsgBox("ERROR: Saving file caused an error. Access is denied." & vbCrLf & ex.Message)
    '                    ResetStatus(Constants.CONST_CURRENT_STATUS_PAYOUT)
    '                    FormRefresh()
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. Access is denied."
    '                    Exit Sub
    '                Catch ex As DirectoryNotFoundException
    '                    MsgBox("ERROR: Saving file caused an error. The specified path is invalid." & vbCrLf & ex.Message)
    '                    ResetStatus(Constants.CONST_CURRENT_STATUS_PAYOUT)
    '                    FormRefresh()
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. The specified path is invalid."
    '                    Exit Sub
    '                Catch ex As SecurityException
    '                    MsgBox("ERROR: Saving file caused an error. The caller does not have the required permission." & vbCrLf & ex.Message)
    '                    ResetStatus(Constants.CONST_CURRENT_STATUS_PAYOUT)
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. The caller does not have the required permission."
    '                    Exit Sub
    '                Catch ex As Exception
    '                    MsgBox("ERROR: Saving file caused an error." & vbCrLf & ex.Message)
    '                    ResetStatus(Constants.CONST_CURRENT_STATUS_PAYOUT)
    '                    FormRefresh()
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error."
    '                    Exit Sub
    '                End Try
    '                'Saving Enquiry Summary file was successful
    '                'set OAC Status
    '                'Check is possible to Change Remote Status
    '                Dim systemCanChangeRemoteStatus As Boolean
    '                Dim thisAppsettings As New AppSettings
    '                systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
    '                If systemCanChangeRemoteStatus = True Then
    '                    Dim retTTResult As New TrackResult
    '                    Dim onlineApplicationService As New OnlineApplication
    '                    Dim note As String = String.Empty
    '                    'Change Status in OAC
    '                    Dim actionType As Integer = OACWebService.ApplicationActionType.PayoutCompleted
    '                    retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
    '                    If retTTResult.Status = False Then 'error condition
    '                        commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
    '                        MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                        ToolStripStatusLabel1.ForeColor = Color.Red
    '                        ToolStripStatusLabel1.Text = "set OAC Status was not successful."
    '                        progressMsg = progressMsg & vbCrLf & "Set OAC Status was not successful."
    '                    Else
    '                        ToolStripStatusLabel1.ForeColor = Color.Blue
    '                        ToolStripStatusLabel1.Text = "set OAC Status was successful."
    '                        progressMsg = progressMsg & vbCrLf & "Set OAC Status was successful."
    '                    End If
    '                    Application.DoEvents()
    '                End If

    '                'disable payout controls
    '                gpbxProcessPayout.Enabled = False
    '                gpbxPaid.Enabled = False
    '                gpbxRefinAccounts.Enabled = False
    '                gpbxFinal.Enabled = False
    '                btnPayoutUpdate.Enabled = False

    '            Else
    '                MsgBox("ERROR: The specified path is invalid." & vbCrLf & folderPath)
    '                'update Status Strip
    '                ToolStripStatusLabel1.ForeColor = Color.Red
    '                ToolStripStatusLabel1.Text = "ERROR: The specified path is invalid: " & folderPath
    '                Exit Sub
    '            End If 'Directory.Exists(Enquiry folderPath)

    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Black
    '            ToolStripStatusLabel1.Text = "Status: Ready."
    '        End If 'Validate payout data

    '    End If 'Financefacility
    '    'Validatation of Payout fields was successful
    '    'Saving Enquiry Summary file was successful
    '    'Set OAC Status may be successful, failure does not stop this procedure
    '    '******** Document Info **************
    '    'Call this before the file list is empty, enquiryPrint is needed to create the EnquirySummary field in ArchiveEnquiry table
    '    'get list of documents in workfolder
    '    Dim fileString As String = String.Empty
    '    fileString = Util.GetDocumentsList(worksheetDrive, thisEnquiryCode, False)

    '    '******************** Client documents
    '    'Search worksheet folder and Any file starting with ID move to the finPower Client Folder
    '    If Not Me.EnquiryCustomersBindingSource.Current Is Nothing Then
    '        custCount = EnquiryCustomersBindingSource.Count
    '        Dim mainClientNum As String = String.Empty
    '        If custCount > 0 Then
    '            For Each row As DataRow In EnquiryWorkSheetDataSet.EnquiryCustomers.Rows
    '                custId = row.Item("CustomerId")
    '                custIdNumber = If(IsDBNull(row.Item("Id_number")), "", row.Item("Id_number"))
    '                custName = If(IsDBNull(row.Item("Name")), "", row.Item("Name"))
    '                enqCustType = If(IsDBNull(row.Item("Type")), "", row.Item("Type"))
    '                'load customer
    '                If custIdNumber.Length > 0 Then
    '                    Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, custId)
    '                    Dim custRow As EnquiryWorkSheetDataSet.CustomerRow
    '                    Dim thisClientNum As String = String.Empty
    '                    custRow = Me.EnquiryWorkSheetDataSet.Customer(0)
    '                    'In a FinanceFacility enquiry the guarantor will not have a ClientNum, utilise the EnquiryCustomerType
    '                    If enqCustType = MyEnums.CustomerEnquiryType.Main Then
    '                        mainClientNum = custRow.ClientNum
    '                    End If
    '                    If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '                        If Not String.IsNullOrEmpty(mainClientNum) Then
    '                            thisClientNum = mainClientNum
    '                        Else
    '                            Throw New ArgumentException("btnExportFiles: mainClientNum is NullOrEmpty")
    '                            Exit Sub
    '                        End If
    '                    Else
    '                        thisClientNum = custRow.ClientNum
    '                    End If

    '                    Dim thisFinPowerClientPath As String = finPowerDrive & "Client_" & thisClientNum 'thisClientTypeId & thisClientNum
    '                    Try
    '                        If Directory.Exists(thisFinPowerClientPath) Then
    '                            If Directory.Exists(folderPath) Then
    '                                '################################
    '                                'Revise file processing 24/02/2017
    '                                '################################
    '                                ProcessDirectory(folderPath, thisFinPowerClientPath, folderPath, MyEnums.TransferFileType.Client, thisEnquiryType, custIdNumber)
    '                                progressMsg = progressMsg & vbCrLf & "Moving Customer Documents to finPower Folder completed"
    '                            Else
    '                                progressMsg = progressMsg & vbCrLf & "Path to TrueTrack Folder does not exist. Export aborted!"
    '                                MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
    '                                'update Status Strip
    '                                ToolStripStatusLabel1.ForeColor = Color.Red
    '                                ToolStripStatusLabel1.Text = "Path to TrueTrack Folder does not exist. Export aborted!"
    '                                Exit Sub
    '                            End If
    '                        Else
    '                            progressMsg = progressMsg & vbCrLf & "Path to finPower Client Folder does not exist. Export aborted!"
    '                            MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
    '                            'update Status Strip
    '                            ToolStripStatusLabel1.ForeColor = Color.Red
    '                            ToolStripStatusLabel1.Text = "Path to finPower Client Folder does not exist. Export aborted!"
    '                            Exit Sub
    '                        End If

    '                    Catch ex As System.IO.IOException
    '                        progressMsg = progressMsg & vbCrLf & "Moving Customer Documents to finPower Folder generated an IOException Message: " & vbCrLf & ex.Message
    '                        MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
    '                        'update Status Strip
    '                        ToolStripStatusLabel1.ForeColor = Color.Red
    '                        ToolStripStatusLabel1.Text = "Customer document processing error. Export aborted!"
    '                        Exit Sub
    '                    Catch ex As Exception
    '                        progressMsg = progressMsg & vbCrLf & "Moving Customer Documents to finPower Folder generated an Exception Message: " & vbCrLf & ex.Message
    '                        MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
    '                        'update Status Strip
    '                        ToolStripStatusLabel1.ForeColor = Color.Red
    '                        ToolStripStatusLabel1.Text = "Customer document processing error. Export aborted!"
    '                        Exit Sub
    '                    End Try
    '                Else
    '                    progressMsg = progressMsg & vbCrLf & "Customer " & custName & " has no Id number" & vbCrLf & "Export aborted!"
    '                    MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "Customer has no Id number. Export aborted!"
    '                    Exit Sub
    '                End If

    '            Next
    '        Else
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "No customers!"
    '        End If
    '    End If

    '    '******************** finPower Loan
    '    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
    '        'get finPower_LoanNumber
    '        Dim thisFinPowerLoanNum As String = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum")
    '        'finPower Loan Folder path
    '        Dim thisFinPowerLoanNumPath = finPowerDrive & "Loan_" & thisFinPowerLoanNum
    '        'MsgBox("Path to Loan = " & ThisFinPowerLoanNumPath)  
    '        'Loan documents move to finPower Loan folder
    '        Try
    '            If Directory.Exists(thisFinPowerLoanNumPath) Then
    '                If Directory.Exists(folderPath) Then
    '                    '################################
    '                    'Revise file processing 24/02/2017
    '                    '################################
    '                    ProcessDirectory(folderPath, thisFinPowerLoanNumPath, folderPath, MyEnums.TransferFileType.Loan, thisEnquiryType)
    '                    progressMsg = progressMsg & vbCrLf & "Moving Loan Documents to finPower Folder completed"
    '                Else
    '                    progressMsg = progressMsg & vbCrLf & "Path to TrueTrack Folder does not exist. Export aborted!"
    '                    MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
    '                    'update Status Strip
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    ToolStripStatusLabel1.Text = "Path to TrueTrack Folder does not exist. Export aborted!"
    '                    Exit Sub
    '                End If
    '            Else
    '                progressMsg = progressMsg & vbCrLf & "Path to finPower Loan Folder does not exist. Export aborted!"
    '                MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
    '                'update Status Strip
    '                ToolStripStatusLabel1.ForeColor = Color.Red
    '                ToolStripStatusLabel1.Text = "Path to finPower Loan Folder does not exist. Export aborted!"
    '                Exit Sub
    '            End If
    '        Catch ex As System.IO.IOException
    '            progressMsg = progressMsg & vbCrLf & "Moving Loan Documents to finPower Folder generated an IOException Message: " & vbCrLf & ex.Message
    '            MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Moving Loan Documents to finPower Folder generated an IOException Message!"
    '            Exit Sub
    '        Catch ex As Exception
    '            progressMsg = progressMsg & vbCrLf & "Moving Loan Documents to finPower Folder generated an Exception Message: " & vbCrLf & ex.Message
    '            MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Moving Loan Documents to finPower Folder generated an Exception Message!"
    '            Exit Sub
    '        End Try
    '    End If
    '    'MsgBox("Path to Client = " & ThisFinPowerClientIdPath)      
    '    'after moving files mark this enquiry as NOT active
    '    ''Dim bindSrc As BindingSource = Me.EnquiryBindingSource
    '    ' ''Dim rowView As DataRowView
    '    ' ''rowView = CType(bindSrc.Current, DataRowView)
    '    ' ''update fields

    '    ''enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '    ''enquiryRow.BeginEdit()
    '    ' ''update Current Status
    '    ''enquiryRow.CurrentStatus = "Exported"
    '    ''enquiryRow.CurrentStatusSetDate = DateTime.Now
    '    ' ''Set Original Enquiry Time
    '    ''originalTime = enquiryRow.DateTime
    '    ' ''calculate time difference for 30min status
    '    ''StatusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
    '    ''If statusTimeDiff < 30 Then
    '    ''    enquiryRow.Status30mins = "Exported"
    '    ''End If
    '    ' ''create log note
    '    ''CommentStr = "Status changed to Exported"
    '    ''commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

    '    ' ''change Active status
    '    ''enquiryRow.ActiveStatus = False

    '    ' ''save changes
    '    ''Me.Validate()
    '    ''bindSrc.EndEdit()
    '    ''Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

    '    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '    enquiryRow.BeginEdit()
    '    'update Current Status
    '    enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED
    '    enquiryRow.CurrentStatusSetDate = DateTime.Now
    '    'Set Original Enquiry Time
    '    originalTime = enquiryRow.DateTime
    '    'calculate time difference for 30min status
    '    StatusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
    '    If StatusTimeDiff < 30 Then
    '        enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_COMPLETED
    '    End If
    '    'change Active status
    '    enquiryRow.ActiveStatus = False
    '    'update Enquiry
    '    Try
    '        Me.Validate()
    '        Me.EnquiryBindingSource.EndEdit()
    '        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
    '    Catch ex As Exception
    '        MsgBox("ERROR: Updating of Enquiry fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
    '        FormRefresh()
    '        'update Status Strip
    '        ToolStripStatusLabel1.ForeColor = Color.Red
    '        ToolStripStatusLabel1.Text = "ERROR: Updating of Enquiry fields caused an error."
    '        Exit Sub
    '    End Try
    '    'create log note
    '    CommentStr = "Status change to " & Constants.CONST_CURRENT_STATUS_COMPLETED
    '    commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

    '    progressMsg = progressMsg & vbCrLf & "Export Completed Successfully"
    '    'update Status Strip
    '    ToolStripStatusLabel1.ForeColor = Color.Blue
    '    ToolStripStatusLabel1.Text = "Export Completed Successfully"
    '    Application.DoEvents()
    '    'Disable Export button
    '    btnExportFiles.Visible = True
    '    btnExportFiles.Enabled = False
    '    btnAddLoanProgress.Enabled = False
    '    EnquiryExported = True
    '    'Now archive
    '    'the file list will be empty, but enquiryPrint is needed to create the EnquirySummary field in ArchiveEnquiry table
    '    Dim enquiryPrint As String
    '    'enquiryPrintOut = ConstructPrintStringAll(False)
    '    enquiryPrintOut = Util.GetPrintStringAll(thisEnquiryId, False)
    '    If enquiryPrintOut.Result Then
    '        enquiryPrint = enquiryPrintOut.StringOut
    '        progressMsg = progressMsg & vbCrLf & "Print string created successfully."
    '    Else
    '        progressMsg = progressMsg & vbCrLf & "Error creating Print String" & vbCrLf & "Archive process aborted!"
    '        MessageBox.Show(progressMsg, "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        'update Status Strip
    '        ToolStripStatusLabel1.ForeColor = Color.Red
    '        ToolStripStatusLabel1.Text = "Export completed successfully. Archive process aborted!"
    '        Exit Sub
    '    End If
    '    'Add filelist to string,  list was generated before moving files (without header)
    '    If fileString.Length > 0 Then
    '        enquiryPrint = enquiryPrint & vbCrLf & fileString & vbCrLf
    '    End If
    '    'Printed by info
    '    Dim printInfoString As String
    '    printInfoString = vbCrLf & vbCrLf & vbCrLf & "*****  Printed by: " & LoggedinName & vbCrLf & DateTime.Now.ToString
    '    'add to constructedString
    '    If printInfoString.Length > 0 Then
    '        enquiryPrint = enquiryPrint & printInfoString
    '        progressMsg = progressMsg & vbCrLf & "Printing info added to Print String successfully."
    '    End If

    '    'call archive function
    '    Try
    '        'Check is possible to Change Remote Status
    '        'Dim result As Boolean
    '        Dim result As New Util.StringOut
    '        Dim systemCanChangeRemoteStatus As Boolean
    '        Dim thisAppsettings As New AppSettings
    '        systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
    '        If systemCanChangeRemoteStatus = True Then
    '            'update OAC
    '            Dim onlineApplicationService As New OnlineApplication
    '            Dim retTTResult As New TrackResult
    '            Dim actionType As Integer = OACWebService.ApplicationActionType.Archive
    '            retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, String.Empty)
    '            If retTTResult.Status = False Then 'error condition
    '                'Add comment
    '                commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
    '                'set dialogbox
    '                Dim msg As String
    '                msg = progressMsg & vbCrLf & retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage & vbCrLf & vbCrLf & "Remote Archiving has failed, do you wish to continue with local archiving?" & vbCrLf & vbCrLf & "If you do not know what to do, select No."
    '                Dim diagResult As Integer = MessageBox.Show(msg, "Remote Archiving Error", MessageBoxButtons.YesNo)
    '                If diagResult = DialogResult.No Then
    '                    ToolStripStatusLabel1.Text = "Remote Archiving error. Archiving aborted!"
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    Exit Sub
    '                ElseIf diagResult = DialogResult.Yes Then
    '                    progressMsg = progressMsg & vbCrLf & "Remote Archiving failed!"
    '                    'call archive function and get return value
    '                    'result = Archive_Enquiry(thisEnquiryId, enquiryPrint)
    '                    result = Await Util.NewArchiveEnquiry(thisEnquiryId)
    '                    If result.Result = True Then
    '                        progressMsg = progressMsg & vbCrLf & "Local archiving completed successfully."
    '                        ToolStripStatusLabel1.Text = "Remote Archiving error. Local archiving completed."
    '                        ToolStripStatusLabel1.ForeColor = Color.Blue
    '                    Else
    '                        progressMsg = progressMsg & vbCrLf & "Local archiving failed."
    '                        MessageBox.Show(progressMsg, "Error in Archiving process", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                        ToolStripStatusLabel1.Text = "Remote Archiving error. Local archiving error!"
    '                        ToolStripStatusLabel1.ForeColor = Color.Red
    '                        Exit Sub
    '                    End If
    '                End If
    '            Else
    '                progressMsg = progressMsg & vbCrLf & "Remote Archiving successful."
    '                'call archive function and get return value
    '                'result = Archive_Enquiry(thisEnquiryId, enquiryPrint)
    '                result = Await Util.NewArchiveEnquiry(thisEnquiryId)
    '                If result.Result = True Then
    '                    progressMsg = progressMsg & vbCrLf & "Local archiving completed successfully."
    '                    ToolStripStatusLabel1.Text = "Remote Archiving successful. Local archiving completed."
    '                    ToolStripStatusLabel1.ForeColor = Color.Blue
    '                Else
    '                    progressMsg = progressMsg & vbCrLf & "Local archiving failed."
    '                    MessageBox.Show(progressMsg, "Error in Archiving process", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                    ToolStripStatusLabel1.Text = "Remote Archiving successful. Local archiving error!"
    '                    ToolStripStatusLabel1.ForeColor = Color.Red
    '                    Exit Sub
    '                End If
    '            End If
    '        Else
    '            'no remote archiving allowed, call archive function and get return value
    '            'result = Archive_Enquiry(thisEnquiryId, enquiryPrint)
    '            result = Await Util.NewArchiveEnquiry(thisEnquiryId)
    '            If result.Result = True Then
    '                progressMsg = progressMsg & vbCrLf & "Local archiving completed successfully."
    '                ToolStripStatusLabel1.Text = "Local archiving completed."
    '                ToolStripStatusLabel1.ForeColor = Color.Blue
    '            Else
    '                progressMsg = progressMsg & vbCrLf & "Local archiving failed!"
    '                MessageBox.Show(progressMsg, "Error in Archiving process", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                ToolStripStatusLabel1.Text = "Local archiving failed!"
    '                ToolStripStatusLabel1.ForeColor = Color.Red
    '                Exit Sub
    '            End If
    '        End If

    '    Catch ex As Exception
    '        progressMsg = progressMsg & vbCrLf & "Archiving process generated an Exception Message: " & vbCrLf & ex.Message
    '        MessageBox.Show(progressMsg, "Error in Archiving process", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Exit Sub
    '    End Try

    '    'delete directories
    '    Try
    '        DeleteDirectories(folderPath)
    '    Catch ex As Exception
    '        progressMsg = progressMsg & vbCrLf & "Error deleting directories." & vbCrLf & ex.Message
    '        MessageBox.Show(progressMsg, "Error deleting directories", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    '    ''launch  Archive Form
    '    'Dim archiveFrm As New ArchiveForm
    '    'Try
    '    '    'launch ArchiveForm passing EnquiryCode
    '    '    archiveFrm.PassVariable(thisEnquiryCode)
    '    '    archiveFrm.Show()
    '    'Catch ex As Exception
    '    '    progressMsg = progressMsg & vbCrLf & "Error launching Archive Form: " & vbCrLf & ex.Message
    '    '    MessageBox.Show(progressMsg, "Error launching Archive Form", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    'End Try
    '    ''Close form
    '    'Me.Close()

    '    FormRefresh()

    '    Cursor.Current = Cursors.Default

    'End Sub

    '******** End of Export *******************************

    Private Sub AppForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If EnquiryExported = False Then
            Try
                'Remove Handlers
                RemoveHandler dtpPSDate.ValueChanged, AddressOf dtpPSDate_ValueChanged
                RemoveHandler dtpPEDate.ValueChanged, AddressOf dtpPEDate_ValueChanged

                'check if data has changed
                'Dim bindSrc1 As BindingSource = Me.ClientBindingSource
                Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
                Dim bindSrc4 As BindingSource = Me.DueDiligenceBindingSource
                Dim bindSrc5 As BindingSource = Me.PayoutBindingSource
                'Dim rowView1 As DataRowView
                Dim rowView2 As DataRowView
                Dim rowView4 As DataRowView
                Dim rowView5 As DataRowView

                If LoggedinPermissionLevel > 2 Then
                    If Not bindSrc2.Current Is Nothing And Not bindSrc4.Current Is Nothing And Not bindSrc5.Current Is Nothing Then
                        'rowView1 = CType(bindSrc1.Current, DataRowView)
                        rowView2 = CType(bindSrc2.Current, DataRowView)
                        rowView4 = CType(bindSrc4.Current, DataRowView)
                        rowView5 = CType(bindSrc5.Current, DataRowView)
                        'test if any fields have changed
                        If rowView2.Row.HasVersion(DataRowVersion.Proposed) Or rowView4.Row.HasVersion(DataRowVersion.Proposed) Or rowView5.Row.HasVersion(DataRowVersion.Proposed) Then
                            'Get user confirmation
                            Dim msg As String
                            Dim title As String
                            Dim style As MsgBoxStyle
                            Dim response As MsgBoxResult
                            msg = "Do you want to Save Changed Data?"   ' Define message.
                            style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                            title = "Save Record?"   ' Define title.
                            ' Display message.
                            response = MsgBox(msg, style, title)
                            If response = MsgBoxResult.Yes Then   ' User choose Yes.
                                Try
                                    'update Status Strip
                                    ToolStripStatusLabel1.ForeColor = Color.Black
                                    ToolStripStatusLabel1.Text = "Saving changed data."
                                    'save changes
                                    Me.Validate()
                                    'bindSrc1.EndEdit()
                                    bindSrc2.EndEdit()
                                    bindSrc4.EndEdit()
                                    bindSrc5.EndEdit()
                                    Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                                    'update Status Strip
                                    ToolStripStatusLabel1.ForeColor = Color.Black
                                    ToolStripStatusLabel1.Text = "Changes saved successfully."
                                Catch ex As Exception
                                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                    MsgBox(ex.Message)
                                End Try
                            Else
                                ' User choose No. Do nothing
                            End If
                            'no changes to save. Do nothing
                        End If
                        'no data entered - proceed
                    End If
                End If
                'leave if bindingsource.current is nothing (no data)

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

            '******************** check user settings
            'check user location settings
            If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
                NewLocX = 0
            Else
                NewLocX = Me.Location.X
            End If
            If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
                NewLocY = 0
            Else
                NewLocY = Me.Location.Y
            End If
            'save user settings
            My.Settings.AppFormLocation = New Point(NewLocX, NewLocY)
            My.Settings.AppFormSize = Me.ClientSize
            My.Settings.Save()
            '******************** end of check user settings
        Else
            '******************** check user settings
            'check user location settings
            If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
                NewLocX = 0
            Else
                NewLocX = Me.Location.X
            End If
            If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
                NewLocY = 0
            Else
                NewLocY = Me.Location.Y
            End If
            'save user settings
            My.Settings.AppFormLocation = New Point(NewLocX, NewLocY)
            My.Settings.AppFormSize = Me.ClientSize
            My.Settings.Save()
            '******************** end of check user settings
        End If


    End Sub

    Private Sub AppForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        'formwide = 960, formheight = 1050
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & Me.Height & vbCrLf & "formWidth = " & Me.Width)
        Dim dgvCustHeight As Integer
        Dim dgvCustLocY As Integer
        'Dim dgvHeight As Integer
        Dim dgvDtsb23H As Integer 'height of dgvHDtsbType2 datagridview
        Dim dgvDtsb01H As Integer 'height of dgvDtsb01 datagridview
        Dim drHeight As Integer 'height of datarepeater drReferences
        Dim drFinPowerHeight As Integer 'height of datarepeater drFinPower
        Dim drMinFinPowerHeight As Integer = 30
        Dim spaceBelowGrid As Integer = 15
        Dim spaceBetweenGpbx As Integer = 10
        'resize
        If formHeight > minFormHeight Then
            'Contact details
            lblAddress.Location = New Point(lblContactName.Location.X + lblContactName.Size.Width + spaceBetweenGpbx, lblContactName.Location.Y)
            lblContactAddress.Location = New Point(lblAddress.Location.X + lblAddress.Size.Width, lblAddress.Location.Y)
            lblContact.Location = New Point(lblContactAddress.Location.X + lblContactAddress.Size.Width + spaceBetweenGpbx, lblContactAddress.Location.Y)
            lblContactNumber.Location = New Point(lblContact.Location.X + lblContact.Size.Width, lblContact.Location.Y)
            'tbctrlAppDetails.Height set to 645
            tbctrlAppDetails.Size = New Size(formWidth - 27, formHeight - 203)
            '********************** loan
            'rtxtbxLoanProgress.Size = New Size(formWidth - 60, formHeight - 309)
            dgvEnquiryComments.Size = New Size(formWidth - 48, formHeight - 301)
            '********************** security
            SecurityDataGridView.Size = New Size(formWidth - 61, 179)
            txtbxSecurityComments.Size = New Size(formWidth - 59, formHeight - 513)
            '********************** Customer
            dgvCustLocY = dgvEnquiryCustomers.Location.Y
            dgvCustHeight = 23 + (22 * custCount)
            dgvEnquiryCustomers.Size = New Size(formWidth - 60, dgvCustHeight)
            pnlCustomerType.Location = New Point(2, dgvCustLocY + dgvCustHeight + spaceBelowGrid)
            pnlCustomerType.Size = New Size(formWidth - 53, 20)
            pnlIndividual.Location = New Point(2, pnlCustomerType.Location.Y + pnlCustomerType.Size.Height)
            pnlIndividual.Size = New Size(formWidth - 36, tabCustomers.Height - pnlCustomerType.Location.Y - pnlCustomerType.Size.Height)
            pnlCompany.Location = New Point(2, pnlCustomerType.Location.Y + pnlCustomerType.Size.Height)
            pnlCompany.Size = New Size(formWidth - 36, tabCustomers.Height - pnlCustomerType.Location.Y - pnlCustomerType.Size.Height)

            ''*********Individual
            gpbxCustomerName.Size = New Size(formWidth - 60, 58)
            'identity appraisal
            gpbxIdentity.Location = New Point(4, gpbxCustomerName.Location.Y + gpbxCustomerName.Size.Height + spaceBetweenGpbx)
            gpbxIdentity.Size = New Size(formWidth - 60, 175)
            txtIdIsPerson.Size = New Size(formWidth - 397, 20)
            txtResidency.Size = New Size(formWidth - 397, 20)
            txtAgeOfId.Size = New Size(formWidth - 397, 30)
            'tenancy
            gpbxTenancy.Location = New Point(4, gpbxIdentity.Location.Y + gpbxIdentity.Size.Height + spaceBetweenGpbx)
            'gpbxTenancy.Size = New Size(formWidth - 60, 154)
            gpbxTenancy.Size = New Size(tabCustomers.Width - 25, 154)
            txtbxResidenceSatis.Size = New Size(formWidth - 400, 35)
            txtbxTenancyEnviron.Size = New Size(formWidth - 400, 35)
            txtbxEstablishTenancy.Size = New Size(formWidth - 400, 35)
            'employment
            gpbxEmployment.Location = New Point(4, gpbxTenancy.Location.Y + gpbxTenancy.Size.Height + spaceBetweenGpbx)
            'gpbxEmployment.Size = New Size(formWidth - 60, 328)
            gpbxEmployment.Size = New Size(tabCustomers.Width - 25, 328)
            txtbxQuestion1.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion2.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion9.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion3.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion4.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion6.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion7.Size = New Size(gpbxEmployment.Width - 340, 20)
            txtbxQuestion8.Size = New Size(gpbxEmployment.Width - 340, 20)
            If thisTypeOfCustomer = MyEnums.CustomerType.SoleTrader Then
                gpbxEmployment.Size = New Size(tabCustomers.Width - 25, 228)
                lblWorkCompany.Location = New Point(316 - lblWorkCompany.Width, 25)
                txtbxQuestion1.Location = New Point(331, 22)
                lblFulltime.Location = New Point(316 - lblFulltime.Width, 50)
                txtbxQuestion2.Location = New Point(331, 47)
                lblEmploy9.Location = New Point(316 - lblEmploy9.Width, 75)
                txtbxQuestion9.Location = New Point(331, 72)
                lblWhatDo.Location = New Point(316 - lblWhatDo.Width, 100)
                txtbxQuestion3.Location = New Point(331, 97)
                lblHowLong.Location = New Point(316 - lblHowLong.Width, 125)
                txtbxQuestion4.Location = New Point(331, 122)
                lblPay.Location = New Point(316 - lblPay.Width, 150)
                txtbxQuestion6.Location = New Point(331, 147)
                lblJobSafe.Location = New Point(316 - lblJobSafe.Width, 175)
                txtbxQuestion7.Location = New Point(331, 172)
                lblSelfEmployed.Location = New Point(12, 200)
            Else
                gpbxEmployment.Size = New Size(tabCustomers.Width - 25, 328)
                lblWorkCompany.Location = New Point(316 - lblWorkCompany.Width, 48)
                txtbxQuestion1.Location = New Point(331, 45)
                lblFulltime.Location = New Point(316 - lblFulltime.Width, 73)
                txtbxQuestion2.Location = New Point(331, 70)
                lblEmploy9.Location = New Point(316 - lblEmploy9.Width, 98)
                txtbxQuestion9.Location = New Point(331, 95)
                lblWhatDo.Location = New Point(316 - lblWhatDo.Width, 123)
                txtbxQuestion3.Location = New Point(331, 120)
                lblHowLong.Location = New Point(316 - lblHowLong.Width, 148)
                txtbxQuestion4.Location = New Point(331, 145)
                lblPay.Location = New Point(225 - lblPay.Width, 173)
                txtbxQuestion5.Location = New Point(234, 170)
                txtbxQuestion6.Location = New Point(331, 170)
                lblJobSafe.Location = New Point(316 - lblJobSafe.Width, 198)
                txtbxQuestion7.Location = New Point(331, 195)
                lblWorker.Location = New Point(316 - lblWorker.Width, 223)
                txtbxQuestion8.Location = New Point(331, 220)
                lblSelfEmployed.Location = New Point(147, 256)
            End If
            'lblStabilityOther
            lblStabilityOther.Location = New Point(9, gpbxEmployment.Location.Y + gpbxEmployment.Size.Height + 3)
            rtxtbxStabilityOther.Location = New Point(9, lblStabilityOther.Location.Y + lblStabilityOther.Size.Height)
            rtxtbxStabilityOther.Size = New Size(formWidth - 80, 55)
            'Credit
            gpbxCredit.Location = New Point(4, rtxtbxStabilityOther.Location.Y + rtxtbxStabilityOther.Size.Height + spaceBetweenGpbx)
            gpbxCredit.Size = New Size(formWidth - 60, 384)
            lblCreditCheck.Location = New Point((formWidth / 2) - 256, 18)
            txtbxCreditHistory.Size = New Size(formWidth - 80, 65)
            txtbxCreditDefaults.Size = New Size(formWidth - 80, 65)
            txtbxCreditPPSR.Size = New Size(formWidth - 80, 65)
            gpbxCreditQuestion1.Size = New Size((formWidth / 3) - 64, 66)
            gpbxCreditQuestion2.Size = New Size((formWidth / 3) - 1, 66)
            gpbxCreditQuestion2.Location = New Point(286 - ((960 - formWidth) / 3), 301) '((formWidth / 3) - 3, 301)
            gpbxCreditQuestion3.Size = New Size((formWidth / 3) - 36, 66)
            gpbxCreditQuestion3.Location = New Point(610 - (((960 - formWidth) / 3) * 2), 301)

            '*********Company
            gpbxCompanyInfo.Size = New Size(formWidth - 60, 232)
            'gpbxCompanyInfo.Location = New Point(3, 22)
            tlpCompanyInfo.Size = New Size(formWidth - 73, 212)
            'director/trustee
            gpbxDtsb01.Location = New Point(3, gpbxCompanyInfo.Location.Y + gpbxCompanyInfo.Size.Height + spaceBetweenGpbx)
            dgvDtsb01H = dgvDtsb01.ColumnHeadersHeight + (dgvDtsb01.RowTemplate.Height * custCountDtsbType01)
            dgvDtsb01.Size = New Size(formWidth - 73, dgvDtsb01H)
            tlpDirectors.Size = New Size(formWidth - 73, 67)
            tlpDirectors.Location = New Point(4, 10 + dgvDtsb01.Size.Height)
            gpbxDtsb01.Size = New Size(formWidth - 59, 95 + dgvDtsb01.Size.Height)
            'shareholder/beneficiary 
            gpbxDtsb23.Location = New Point(3, gpbxDtsb01.Location.Y + gpbxDtsb01.Size.Height + spaceBetweenGpbx)
            dgvDtsb23H = dgvDTSBType2.ColumnHeadersHeight + (dgvDTSBType2.RowTemplate.Height * custCountDtsbType23)
            dgvDTSBType2.Size = New Size(formWidth - 73, dgvDtsb23H)
            tlpShareholders.Size = New Size(formWidth - 73, 67)
            tlpShareholders.Location = New Point(4, 10 + dgvDTSBType2.Size.Height)
            gpbxDtsb23.Size = New Size(formWidth - 59, 95 + dgvDTSBType2.Size.Height)
            'tenancy
            gpbxCoTenancy.Location = New Point(3, gpbxDtsb23.Location.Y + gpbxDtsb23.Size.Height + spaceBetweenGpbx)
            gpbxCoTenancy.Size = New Size(formWidth - 59, 115)
            txtbxCompanyTenancySatis.Size = New Size(formWidth - 380, 36)
            'credit
            gpbxBusinessCredit.Location = New Point(3, gpbxCoTenancy.Location.Y + gpbxCoTenancy.Size.Height + spaceBetweenGpbx)
            gpbxBusinessCredit.Size = New Size(formWidth - 59, 370)
            lblBusCreditCheck.Location = New Point((formWidth / 2) - 256, 18)
            txtBusCreditHistory.Size = New Size(formWidth - 83, 65)
            txtBusCreditDefaults.Size = New Size(formWidth - 83, 65)
            txtBusCreditPPSR.Size = New Size(formWidth - 83, 65)
            gpbxBusCreditQuestion1.Size = New Size((formWidth / 3) - 64, 66)
            gpbxBusCreditQuestion2.Size = New Size((formWidth / 3) - 1, 66)
            gpbxBusCreditQuestion2.Location = New Point(274 - ((960 - formWidth) / 3), 292) 'New Point((formWidth / 3) - 3, 292)
            gpbxBusCreditQuestion3.Size = New Size((formWidth / 3) - 36, 66)
            gpbxBusCreditQuestion3.Location = New Point(599 - (((960 - formWidth) / 3) * 2), 292)
            'Comments
            gpbxBusOther.Location = New Point(3, gpbxBusinessCredit.Location.Y + gpbxBusinessCredit.Size.Height + spaceBetweenGpbx)
            gpbxBusOther.Size = New Size(formWidth - 59, 90)
            txtBusOtherComments.Size = New Size(formWidth - 83, 65)
            'Trade References
            gpbxTradeRef.Location = New Point(3, gpbxBusOther.Location.Y + gpbxBusOther.Size.Height + spaceBetweenGpbx)
            If refCount > 2 Then
                thisRefCount = 2
            Else
                thisRefCount = refCount
            End If
            drHeight = 178 * thisRefCount
            gpbxTradeRef.Size = New Size(formWidth - 60, drHeight + 22)
            drReferences.Size = New Size(formWidth - 76, drHeight)

            '********************** budget
            gpbxCalculator.Size = New Size(formWidth - 75, 102)
            tlpCalculator.Size = New Size(formWidth - 87, 77)
            gpbxIncome.Size = New Size(formWidth - 75, 165)
            tlpIncomeStated.Size = New Size(formWidth - 87, 29)
            tlpIncome.Size = New Size(formWidth - 87, 29)
            tlpOvertime.Size = New Size(formWidth - 87, 77)
            txtbxOvertime.Size = New Size(formWidth - 436, 35)
            gpbxExpenses.Size = New Size(formWidth - 75, 198)
            tlpExpensesStated.Size = New Size(formWidth - 87, 29)
            tlpExpenses.Size = New Size(formWidth - 87, 138)
            gpbxDependency.Size = New Size(formWidth - 75, 57)
            tlpDependency.Size = New Size(formWidth - 87, 29)
            gpbxBudgetSummary.Size = New Size(formWidth - 75, 169)
            txtbxOtherExplain.Size = New Size(formWidth - 87, 101)


            '********************** Company Financials
            'Income and expenses
            gpbxIncomeExpenses.Size = New Size(formWidth - 68, 301)
            tlpIncomeExpenses.Size = New Size(formWidth - 80, 276)
            Select Case mainCustType
                Case MyEnums.mainCustomerType.SoleTrader, MyEnums.mainCustomerType.Partnership
                    gpbxEarnings.Location = New Point(0, 315)
                    'Position
                    gpbxPosition.Location = New Point(12, 417)
                    gpbxPosition.Size = New Size(formWidth - 50, 225)
                    tlpPosition2.Location = New Point(6, 19)
                    tlpPosition2.Size = New Size(formWidth - 80, 200)
                Case Else
                    'Salaries
                    gpbxSalaries.Size = New Size(formWidth - 68, 96)
                    tlpSalaries.Size = New Size(formWidth - 80, 72)
                    'Position
                    gpbxPosition.Size = New Size(formWidth - 50, 298)
                    tlpPosition.Size = New Size(formWidth - 80, 71)
                    tlpPosition2.Location = New Point(6, 89)
                    tlpPosition2.Size = New Size(formWidth - 80, 200)

            End Select
            'Earnings
            gpbxEarnings.Size = New Size(formWidth - 68, 96)
            tlpEarnings.Size = New Size(formWidth - 80, 71)

            '********************** documents
            lvDocuments.Size = New Size(formWidth - 54, formHeight - 276)
            '********************** waiting for sign-up

            '********************** Audit
            'finPower mapping
            ' gpbxfinPowerNum.Location = New Point(6, 33)
            If (ffCustCount * 24) + 8 < drMinFinPowerHeight Then
                drFinPowerHeight = drMinFinPowerHeight
            Else
                drFinPowerHeight = (ffCustCount * 24) + 8
            End If
            drFinPower.Size = New Size(341, drFinPowerHeight)
            gpbxfinPowerNum.Size = New Size(formWidth - 78, drFinPowerHeight + 32)
            'LoanBreakdown
            gpbxLoanBreakdown.Location = New Point(6, gpbxfinPowerNum.Location.Y + gpbxfinPowerNum.Size.Height + 8)
            gpbxLoanBreakdown.Size = New Size(formWidth - 78, 150)
            TableLayoutPanel8.Size = New Size(formWidth - 88, 124)
            'Dealer Commission
            gpbxDealerComm.Location = New Point(6, gpbxLoanBreakdown.Location.Y + gpbxLoanBreakdown.Size.Height)
            gpbxDealerComm.Size = New Size(formWidth - 78, 96)
            'Documents
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                gpbxDocuments.Location = New Point(6, gpbxfinPowerNum.Location.Y + gpbxfinPowerNum.Size.Height + 8)
                gpbxDocuments.Size = New Size(formWidth - 78, 149)
                tlpDocuments.Size = New Size(formWidth - 88, 126)
            Else
                gpbxDocuments.Location = New Point(6, gpbxDealerComm.Location.Y + gpbxDealerComm.Size.Height)
                gpbxDocuments.Size = New Size(formWidth - 78, 149)
                tlpDocuments.Size = New Size(formWidth - 88, 126)
            End If
            'processing gpbxs
            gpbxProcessingForm.Location = New Point(6, gpbxDocuments.Location.Y + gpbxDocuments.Size.Height + 8)
            gpbxProcessingForm.Size = New Size(formWidth - 78, 73)
            tlpProcessingForm.Size = New Size(formWidth - 88, 48)
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                gpbxProcessingDocs.Location = New Point(6, gpbxDocuments.Location.Y + gpbxDocuments.Size.Height + 8)
                gpbxProcessingDocs.Size = New Size(formWidth - 78, 48)
                tlpProcessingDocs.Size = New Size(formWidth - 88, 24)
            Else
                gpbxProcessingDocs.Location = New Point(6, gpbxProcessingForm.Location.Y + gpbxProcessingForm.Size.Height)
                gpbxProcessingDocs.Size = New Size(formWidth - 78, 48)
                tlpProcessingDocs.Size = New Size(formWidth - 88, 24)
            End If

            gpbxProcessingFinPower.Location = New Point(6, gpbxProcessingDocs.Location.Y + gpbxProcessingDocs.Size.Height)
            gpbxProcessingFinPower.Size = New Size(formWidth - 78, 48)
            tlpProcessingFinPower.Size = New Size(formWidth - 88, 24)
            gpbxProcessingPPSR.Location = New Size(6, gpbxProcessingFinPower.Location.Y + gpbxProcessingFinPower.Size.Height)
            gpbxProcessingPPSR.Size = New Size(formWidth - 78, 74)
            tlpProcessingPPSR.Size = New Size(formWidth - 88, 48)
            gpbxProcessingProtecta.Location = New Point(6, gpbxProcessingPPSR.Location.Y + gpbxProcessingPPSR.Size.Height)
            gpbxProcessingProtecta.Size = New Size(formWidth - 78, 133)
            tlpProcessingProtecta.Size = New Size(formWidth - 88, 114)
            txtSpecialNotes.Size = New Size(tlpProcessingInsurance.Size.Width - 143, 35)
            'Insurance
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                gpbxProcessingInsurance.Location = New Point(6, gpbxProcessingDocs.Location.Y + gpbxProcessingDocs.Size.Height + 8)
                gpbxProcessingInsurance.Size = New Size(formWidth - 78, 48)
                tlpProcessingInsurance.Size = New Size(formWidth - 88, 24)
            Else
                gpbxProcessingInsurance.Location = New Point(6, gpbxProcessingProtecta.Location.Y + gpbxProcessingProtecta.Size.Height)
                gpbxProcessingInsurance.Size = New Size(formWidth - 78, 48)
                tlpProcessingInsurance.Size = New Size(formWidth - 88, 24)
            End If
            'Disbursments
            gpbxDisbursements.Location = New Point(6, gpbxProcessingInsurance.Location.Y + gpbxProcessingInsurance.Size.Height + 8)
            gpbxDisbursements.Size = New Size(formWidth - 78, 223)
            tlpDisbursements.Size = New Size(formWidth - 88, 203)
            'Final
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                gpbxAuditFinal.Location = New Point(6, gpbxProcessingInsurance.Location.Y + gpbxProcessingInsurance.Size.Height + 8)
                gpbxAuditFinal.Size = New Size(formWidth - 78, 72)
                tlpAuditFinal.Size = New Size(formWidth - 88, 51)
            Else
                gpbxAuditFinal.Location = New Point(6, gpbxDisbursements.Location.Y + gpbxDisbursements.Size.Height + 8)
                gpbxAuditFinal.Size = New Size(formWidth - 78, 72)
                tlpAuditFinal.Size = New Size(formWidth - 88, 51)
            End If
            'button
            btnAudit.Location = New Point(402 - (960 - formWidth) / 2, 3)
            If auditpushright = True Then btnAudit.Location = New Point(formWidth - btnAudit.Width - 40, 3)
            btnPassedAudit.Location = New Point(332 - (960 - formWidth) / 2, 3)
            '********************** payout
            gpbxProcessPayout.Size = New Size(formWidth - 78, 70)
            'tlpPayoutProcessing.Size = New Size(formWidth - 88, 52)
            gpbxPaid.Size = New Size(formWidth - 78, 495)
            'tlpPayoutPaid.Size = New Size(formWidth - 88, 453)
            gpbxRefinAccounts.Size = New Size(formWidth - 78, 120)
            'tlpPayoutRefin.Size = New Size(formWidth - 88, 102)
            gpbxFinal.Size = New Size(formWidth - 78, 70)
            'tlpPayoutFinal.Size = New Size(formWidth - 88, 50)
            btnExportFiles.Location = New Point(332 - (960 - formWidth) / 2, 3)

        End If

    End Sub


    Private Sub btnQRGList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQRGList.Click
        Try
            'MsgBox("ThisQRGListId = " & ThisQRGListId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Quick reference Guide List."
            Dim QRGFrm As New QRGForm
            QRGFrm.PassVariable(thisEnquiryId, thisEnquiryCode, ThisContactDisplayName)
            'AddSecurityFrm.ShowDialog(Me)
            If (QRGFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                '*****************************  get QRGList status
                Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection2 As New SqlConnection(connectionString2)
                Dim selectStatement2 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand2)
                Dim QRGListTempDataSet As New DataSet
                Dim QRGListTempDataTable As New DataTable
                Try
                    'dumps results into datatable LoginDataTable
                    QRGListTempDataAdapter.Fill(QRGListTempDataTable)
                    'if no matching rows .....
                    If QRGListTempDataTable.Rows.Count = 0 Then
                        MessageBox.Show("ERROR: No QRG List, please try again.")
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                        'if there is a matching row
                    ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                        'get  QRGStatus value
                        Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                        QRGStatus = QRGListTempDataRow.Item(0)
                        'MsgBox("QRG Status = " & QRGStatus)
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                    End If
                    'close the connection
                    If connection2.State <> ConnectionState.Closed Then
                        connection2.Close()
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

                'set QRGStatus button colour
                Select Case QRGStatus
                    Case 0
                        btnQRGList.BackColor = Color.Transparent
                    Case 1
                        btnQRGList.BackColor = Color.Tomato
                    Case 2
                        btnQRGList.BackColor = Color.LightGreen
                    Case Else
                        btnQRGList.BackColor = Color.Transparent
                End Select
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Quick reference Guide List updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Quick reference Guide List cancelled. Status: Ready."
            End If
            QRGFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub


    ''' <summary>
    ''' Checks application html file exists in enquiry folder
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckApplicationFileExists() As Boolean
        Dim ApplicationFileExists As Boolean = False

        'Get Files        
        'Get String to WorkSheet Drive
        'Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String '= worksheetDrive & "\" & ThisEnquiryCode
        folderPath = ApplicationInterchange.GetWorkSheetFolder(thisEnquiryCode)
        'Check directory exists
        If Directory.Exists(folderPath) Then

            Dim aFiles() As String 'array type string

            aFiles = Directory.GetFiles(folderPath)

            'loop through aFiles
            For Each sfile As String In aFiles

                If sfile.IndexOf(thisApplicationCode) > -1 Then
                    If (sfile.IndexOf(".mht") > -1 Or sfile.IndexOf(".htm") > -1 Or sfile.IndexOf(".pdf") > -1) Then
                        ApplicationFileExists = True
                        Exit For
                    End If
                End If
            Next sfile

        End If

        Return ApplicationFileExists

    End Function


    Private Function CheckXMLFileExists() As Boolean

        Return ApplicationInterchange.CheckXMLFileExists(thisEnquiryCode, thisApplicationCode)

    End Function


    ''' <summary>
    ''' Create Loan in FinPOWER
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks> 
    ''' Gets Xml and Html from OAC
    ''' Opens the latency point screen
    '''</remarks>
    Private Async Sub btnCreatefinPowerLoan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreatefinPowerLoan.Click
        Dim btnText As String = "Documents Executed"
        Dim ret As Boolean
        Dim retCreate As Task(Of Boolean)
        'Cancellation Token for Cancel ops
        Dim ct As CancellationToken
        'This TaskScheduler captures SynchronizationContext.Current.
        Dim taskScheduler As TaskScheduler = taskScheduler.FromCurrentSynchronizationContext()
        Try
            If btnCreatefinPowerLoan.Text.IndexOf("Cancel") > -1 Then
                asyncAnimation.EndAnimation()
                cancellationTokenSource.Cancel()
                btnCreatefinPowerLoan.Text = btnText
                ''make the button invisible
                'btnCreatefinPowerLoan.Enabled = False
                'SetToolbarStatusMessage(ToolStripStatusLabel1, "Cancelling create loan in finPower", TrueTrackStatusBarMessageType.RedMessage)
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Ready state", TrueTrackStatusBarMessageType.BlackMessage)
                'Display button btnCreatefinPowerLoan if cancelled
                btnCreatefinPowerLoan.Enabled = True
                Exit Sub
            Else

                'Instantiate cancellation token
                cancellationTokenSource = New CancellationTokenSource()
                ct = cancellationTokenSource.Token
                btnCreatefinPowerLoan.Text = "Cancel"
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Set OAC Status in progress", TrueTrackStatusBarMessageType.BlackMessage)
            End If

            'var for response from OAC service calls
            Dim retHtmlResult As New TrackResult
            Dim retXmlResult As New TrackResult
            Dim retStatusResult As New TrackResult

            asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
            'Application.DoEvents()
            If WebUtil.WebServiceAvailable Then
                'asyncAnimation.EndAnimation()
                'Check if user has cancelled the operation
                If Not ct.IsCancellationRequested Then
                    'Version 2.5.0 
                    'Set OAC Status to 'Documents Executed'
                    SetToolbarStatusMessage(ToolStripStatusLabel1, "Setting OAC status", TrueTrackStatusBarMessageType.BlackMessage)
                    asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                    'makes an asynchronous call to the function that sets the status
                    Await Task.Run(Sub()
                                       retStatusResult = SetStatusDocsExecutedOAC()
                                   End Sub)
                    asyncAnimation.EndAnimation()
                    If retStatusResult.Status = False Then 'error condition
                        SetToolbarStatusMessage(ToolStripStatusLabel1, retStatusResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                        btnCreatefinPowerLoan.Text = btnText
                        MessageBox.Show(retStatusResult.ErrorMessage, "OAC Service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'End of Version 2.5.0
                    Else
                        'Setting OAC status was successful
                        asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                        'UpdateLoanComments("Setting the OAC status completed succesfully")
                        Util.SetComment(thisEnquiryId, "System", "Setting the OAC status completed succesfully")
                        asyncAnimation.EndAnimation()
                        'Check if user has cancelled the operation
                        If Not ct.IsCancellationRequested Then
                            SetToolbarStatusMessage(ToolStripStatusLabel1, "Accessing Xml file from OAC service", TrueTrackStatusBarMessageType.BlackMessage)
                            Application.DoEvents()
                            asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                            'makes an asynchronous call to the function that fetches and copies Xml file to enquiry folder
                            Await Task.Run(Sub()
                                               'get Xml data file from OAC. This does not save into enquiry folder
                                               retXmlResult = GetOACXmlFile()
                                           End Sub)
                            asyncAnimation.EndAnimation()
                            If retXmlResult.Status = False Then
                                SetToolbarStatusMessage(ToolStripStatusLabel1, retXmlResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                                btnCreatefinPowerLoan.Text = btnText
                                MessageBox.Show(retXmlResult.ErrorMessage, "OAC service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Else
                                If String.IsNullOrWhiteSpace(retXmlResult.ResultStringValue) Then
                                    'No Application Xml returned from OAC
                                    SetToolbarStatusMessage(ToolStripStatusLabel1, "Failed to retrieve application Xml file", TrueTrackStatusBarMessageType.RedMessage)
                                    btnCreatefinPowerLoan.Text = btnText
                                    MessageBox.Show("OAC web service did not return application xml file", "OAC service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Else
                                    'Check if user has cancelled the operation
                                    If Not ct.IsCancellationRequested Then

                                        If retXmlResult IsNot Nothing AndAlso retXmlResult.Status = True Then
                                            asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                                            'run the following in the UI thread for updates
                                            Await Task.Factory.StartNew(
                                                Sub()

                                                    'Post process save Xml file into enquiry folder 
                                                    ret = GetXmlFilePostProcess(retXmlResult, ct)
                                                    If ret Then
                                                        SetToolbarStatusMessage(ToolStripStatusLabel1, "Accessing Application file from OAC service", TrueTrackStatusBarMessageType.BlackMessage)
                                                        Application.DoEvents()
                                                    End If

                                                End Sub,
                                                          CancellationToken.None, TaskCreationOptions.None, taskScheduler)
                                            asyncAnimation.EndAnimation()
                                            If Not ct.IsCancellationRequested Then
                                                'if Xml file saved successfully, continue retrieving Application html file
                                                If ret Then
                                                    asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                                                    'makes an Asynchronous call to the function that fetches and copies Application file to Enquiry folder
                                                    Await Task.Run(Sub()
                                                                       'get Application file from OAC and save into enquiry folder
                                                                       retHtmlResult = GetOACApplicationFile()

                                                                   End Sub)
                                                    asyncAnimation.EndAnimation()
                                                    If retHtmlResult.Status = False Then
                                                        SetToolbarStatusMessage(ToolStripStatusLabel1, retHtmlResult.StatusMessage, TrueTrackStatusBarMessageType.BlackMessage)
                                                        btnCreatefinPowerLoan.Text = btnText
                                                        MessageBox.Show(retHtmlResult.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                                    ElseIf retXmlResult.Status = True AndAlso retHtmlResult.Status = True Then
                                                        If String.IsNullOrWhiteSpace(retHtmlResult.ResultStringValue) Then
                                                            'No Application Xml returned from OAC
                                                            SetToolbarStatusMessage(ToolStripStatusLabel1, "Failed to retrieve application data file", TrueTrackStatusBarMessageType.RedMessage)
                                                            btnCreatefinPowerLoan.Text = btnText
                                                            MessageBox.Show("OAC web service did not return application data file", "OAC service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                                        Else
                                                            'Check if user has cancelled the operation
                                                            If Not ct.IsCancellationRequested Then
                                                                asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                                                                'run the following in the UI thread for updates
                                                                Await Task.Factory.StartNew(
                                                                    Sub()

                                                                        'Post process save html file into enquiry folder 
                                                                        ret = GetApplicationFilePostProcess(retHtmlResult, ct)
                                                                        If ret Then
                                                                            SetToolbarStatusMessage(ToolStripStatusLabel1, "Application file retrieved from OAC service successfully", TrueTrackStatusBarMessageType.BlackMessage)
                                                                        End If
                                                                    End Sub,
                                                                              CancellationToken.None, TaskCreationOptions.None, taskScheduler)
                                                                asyncAnimation.EndAnimation()

                                                            End If
                                                            'Can cancel after completing downloading file from OAC, but before starting Post file retrieval operation
                                                            If Not ct.IsCancellationRequested Then
                                                                If retXmlResult.Status = True AndAlso retHtmlResult.Status = True Then
                                                                    asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                                                                    'run the following in the UI thread for updates
                                                                    ret = Await Task.Factory.StartNew(
                                                                        Function()
                                                                            'Cannot Cancel beyond this point 
                                                                            'Post process
                                                                            retCreate = CreatefinPowerLoanPostProcess()
                                                                            Return ret
                                                                        End Function,
                                                                                  CancellationToken.None, TaskCreationOptions.None, taskScheduler)
                                                                    asyncAnimation.EndAnimation()
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                asyncAnimation.EndAnimation()
                MessageBox.Show("Web Service Server not available" & vbCrLf & "Try again later, if error persists contact your administrator", "Web Service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnCreatefinPowerLoan.Enabled = True
                btnCreatefinPowerLoan.Text = btnText
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Web Service connection failed", TrueTrackStatusBarMessageType.RedMessage)
                Exit Sub
            End If


            If ct.IsCancellationRequested Then
                'run the following in the UI thread for updates
                Await Task.Factory.StartNew(
                    Sub()
                        SetToolbarStatusMessage(ToolStripStatusLabel1, "ready state", TrueTrackStatusBarMessageType.BlackMessage)
                        'Display button btnCreatefinPowerLoan if cancelled
                        btnCreatefinPowerLoan.Enabled = True
                        btnCreatefinPowerLoan.Text = btnText
                    End Sub,
                        CancellationToken.None, TaskCreationOptions.None, taskScheduler)

            End If



        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            asyncAnimation.EndAnimation()
            btnCreatefinPowerLoan.Text = btnText
            btnCreatefinPowerLoan.Visible = True
            btnCreatefinPowerLoan.Enabled = True
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error in creating loan in finPOWER", TrueTrackStatusBarMessageType.RedMessage)
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally


        End Try

    End Sub

    ''' <summary>
    ''' Process after saving application XML and HTML in TrueTrack
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>A series of processes are run: Match customers; Add Latency Points; Check the XML client Names and DOB matches records in FinPOWER, If any match found then open ValidateXML form to allow selecting Client data in FinPOWER; Set Enquiry status to Audit.</remarks>
    Private Async Function CreatefinPowerLoanPostProcess() As Task(Of Boolean)
        'CreatefinPowerLoanPostProcess(): uncomment this block
        Dim btnText As String = "Create Loan in finPower"
        Dim ret As Boolean = False
        Dim status As Boolean = False
        Try
            'creating loan in finPOWER process
            btnCreatefinPowerLoan.Visible = False
            btnCreatefinPowerLoan.Text = btnText

            Dim completion As New TaskCompletionSource(Of DialogResult)()
            Dim completionXml As New TaskCompletionSource(Of DialogResult)()
            Dim completionXRefId As New TaskCompletionSource(Of DialogResult)()

            Dim _tempClientType As String = ""
            'initialize object by setting enqiry code and applicationcode
            Dim _finPowerOpObj As New finPowerOps(thisEnquiryCode, thisApplicationCode, thisEnquiryId, thisEnquiryType)

            'Load form to match XML customers with TT customers and save the XRefId to TT Customers
            Dim _frmMatchCustomers As New FrmMatchCustomers(thisEnquiryId, thisEnquiryCode, thisApplicationCode, thisEnquiryType)
            '=============================================
            'Match Customers
            '=============================================
            Dim asyncresultMatch As IAsyncResult
            asyncresultMatch = btnCreatefinPowerLoan.BeginInvoke(Sub() completionXRefId.SetResult(_frmMatchCustomers.ShowDialog()))
            Dim resMatch As DialogResult 'Task(Of DialogResult)
            resMatch = Await completionXRefId.Task
            'If _frmMatchCustomers.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            'XML: If resMatch = System.Windows.Forms.DialogResult.OK Then etc
            If resMatch = System.Windows.Forms.DialogResult.OK Then
                Dim _frmLatencyPointForm As New frmLatencyPoints(thisEnquiryId, thisEnquiryCode, thisApplicationCode, thisEnquiryType)
                _frmLatencyPointForm.SetFormCaption(ThisContactDisplayName)
                '=============================================
                'Add Latency Points
                '=============================================
                Dim asyncresult As IAsyncResult
                asyncresult = btnCreatefinPowerLoan.BeginInvoke(Sub() completion.SetResult(_frmLatencyPointForm.ShowDialog()))
                Dim resLatencyPoint As DialogResult 'Task(Of DialogResult)
                resLatencyPoint = Await completion.Task

                If resLatencyPoint = System.Windows.Forms.DialogResult.OK Then
                    '=============================================
                    'Add the Customer Monitor field to XML customer
                    '=============================================
                    If _finPowerOpObj.UpdateXmlMonitor() Then
                        ToolStripStatusLabel1.Text = "Monitor info updated successfully!"
                        ToolStripStatusLabel1.ForeColor = Color.Blue
                        Application.DoEvents()
                    Else
                        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                            ToolStripStatusLabel1.Text = "Error updating monitor value!"
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            Application.DoEvents()
                        End If
                    End If
                    '=============================================
                    'Load form ValidateXML and update the changes in XML file then move the Xml to finPOWER  
                    '=============================================
                    Dim _frmValidateXml As New ValidateXML
                    _frmValidateXml.SetFormCaption(ThisContactDisplayName)
                    _frmValidateXml.ApplicationCode = thisApplicationCode
                    _frmValidateXml.EnquiryCode = thisEnquiryCode
                    _frmValidateXml.EnquiryId = thisEnquiryId
                    _frmValidateXml.EnquiryType = thisEnquiryType

                    '=================================================
                    'Check the XML client Names and DOB matchs records in FinPOWER
                    '=================================================
                    If _finPowerOpObj.CheckClientNames() Then
                        'If any match found then open ValidateXML form to allow selecting Client data in FinPOWER
                        'If no clientfinPow match in finPower then if EnquiryType is a Finance Facility then as the ClientId is created by the OAC then add ClientId to ttCustomer (main) where XRefId = ... This is done inside CheckClientNames function as beyond this point the XML document is moved to the finPower Import folder
                        _frmValidateXml.finPowerOpObj = _finPowerOpObj
                        Dim asyncresultXml As IAsyncResult
                        asyncresultXml = btnCreatefinPowerLoan.BeginInvoke(Sub() completionXml.SetResult(_frmValidateXml.ShowDialog()))
                        Dim resValidateXml As DialogResult = Await completionXml.Task

                        'If _frmValidateXml.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                        If resValidateXml = System.Windows.Forms.DialogResult.OK Then
                            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                                txtbxLoanNum.Visible = False
                            Else
                                txtbxLoanNum.Visible = True
                                txtbxLoanNum.Text = _frmValidateXml.Loancode
                            End If

                            btnCreatefinPowerLoan.Visible = False
                            btnLoanCode.Visible = False

                            'Set to Audit mode
                            AuditProcess()

                            ToolStripStatusLabel1.Text = "XML file successfully transferred to finPOWER import folder."
                            'UpdateLoanComments("XML file successfully transferred to finPOWER import folder.")
                            Util.SetComment(thisEnquiryId, "System", "XML file successfully transferred to finPOWER import folder.")
                        Else
                            FormRefresh()
                        End If
                    Else
                        '=================================================
                        'If Client Names not match, NOT possible to select finPOWER client
                        'However before moving directly must add Monitor in XML
                        '=================================================
                        'Create duediligence table
                        'Insert Loan number
                        'Set to audit mode
                        If _finPowerOpObj.MoveXMLFileToImportFolder() Then
                            btnCreatefinPowerLoan.Visible = False
                            btnLoanCode.Visible = False
                            AuditProcess()
                            ToolStripStatusLabel1.Text = "XML file successfully transferred to finPOWER import folder."
                            'UpdateLoanComments("XML file successfully transferred to finPOWER import folder.")
                            Util.SetComment(thisEnquiryId, "System", "XML file successfully transferred to finPOWER import folder.")
                            ToolStripStatusLabel1.Text = "XML file successfully transferred to finPOWER import folder."
                        Else
                            MessageBox.Show(_finPowerOpObj.ErrorMessage)
                        End If

                        FormRefresh()
                    End If

                Else
                    FormRefresh()
                End If 'frmLatencyPoint.ShowDialog(Me)
            Else
                FormRefresh()
            End If 'frmMatchCustomers.ShowDialog(Me)            
            btnCreatefinPowerLoan.Text = btnText
            status = True

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            btnCreatefinPowerLoan.Visible = True
            btnCreatefinPowerLoan.Text = btnText
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error in creating loan in finPOWER", TrueTrackStatusBarMessageType.RedMessage)
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

        Return status

    End Function

    ''' <summary>
    ''' Get Loan number from Due Diligence table
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Private Function GetLoanNumFromDueDiligenceTable() As String
        '**##**
        Dim LoanNum As String = ""
        Dim DueDiliTable As AppWhShtB.EnquiryWorkSheetDataSet.DueDiligenceDataTable
        Try
            DueDiliTable = Me.TableAdapterManager.DueDiligenceTableAdapter.GetDataByEnquiryId(thisEnquiryId)
            If DueDiliTable.Rows.Count > 0 Then
                LoanNum = DueDiliTable(0).LoanNum.ToString
            End If
        Catch ex As Exception
            If Not ex.Message.IndexOf("DBNull") > -1 Then
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                Throw
            End If
        End Try

        Return LoanNum

    End Function

    'Private Function CheckWorksheetSharedFolderExists() As Boolean
    '    Dim status As Boolean = False
    '    'Check WorksheetFolder Exists
    '    Dim TTResult As TrackResult = ApplicationInterchange.CheckWorksheetSharedFolderExists()

    '    If Not TTResult Is Nothing Then
    '        status = TTResult.Status
    '        If Not String.IsNullOrWhiteSpace(TTResult.ErrorMessage) Then
    '            SetToolbarStatusMessage(ToolStripStatusLabel1, "Worksheet shared folder does not exists", TrueTrackStatusBarMessageType.RedMessage)
    '        End If
    '    End If


    '    Return status

    'End Function





    ''' <summary>
    ''' Get the Loancode manually from OAC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Async Sub btnLoanCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoanCode.Click
        Dim btnText As String = "Manual Loan (no XML)"
        Dim ret As Boolean = False
        Dim retCheckApplicationResult As New TrackResult
        Dim retLoanCodeResult As New TrackResult 'Result returned from OAC Web Service to get LoanCode

        Dim ct As CancellationToken
        'This TaskScheduler captures SynchronizationContext.Current.
        Dim taskScheduler As TaskScheduler = taskScheduler.FromCurrentSynchronizationContext()
        Try
            'Cursor.Current = Cursors.WaitCursor

            If btnLoanCode.Text.IndexOf("Cancel") > -1 Then
                cancellationTokenSource.Cancel()
                btnLoanCode.Text = btnText
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Ready state", TrueTrackStatusBarMessageType.BlackMessage)
                btnLoanCode.Enabled = True
                btnLoanCode.Visible = True
                Exit Sub
            Else
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Generating loan code from OAC", TrueTrackStatusBarMessageType.BlackMessage)
                'Instantiate cancellation token
                cancellationTokenSource = New CancellationTokenSource()
                ct = cancellationTokenSource.Token
                btnLoanCode.Text = "Cancel"
                'btnCreatefinPowerLoan.Enabled = False
            End If


            If Not ct.IsCancellationRequested Then

                'Call web service here
                'Await task.Run(Sub()
                '                   'Process to get loan code manually
                '                   retLoanCodeResult = GetLoanCodeManually(ct)
                '               End Sub
                '              )

                Dim onlineApplication As New OnlineApplication
                retLoanCodeResult.Status = False
                If CheckEnquiryHasApplicationCode() Then

                    'Cursor.Current = Cursors.WaitCursor
                    onlineApplication = New OnlineApplication
                    If Not ct.IsCancellationRequested Then
                        asyncAnimation.StartAnimation(btnLoanCode.Left, AnimatedCirclePanelAlign.Left)
                        Await task.Run(
                            Sub()
                                'Ping Online Application Centre web service to check the service is available
                                retLoanCodeResult = onlineApplication.PingOnlineApplicationService()
                            End Sub
                                      )
                        asyncAnimation.EndAnimation()
                        If retLoanCodeResult Is Nothing Then retLoanCodeResult = New TrackResult()
                        If retLoanCodeResult.Status = False Then
                            SetToolbarStatusMessage(ToolStripStatusLabel1, retLoanCodeResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                            btnLoanCode.Text = btnText
                            MessageBox.Show("ERROR on Manual Loan: " + vbCrLf + retLoanCodeResult.ErrorMessage)
                        ElseIf retLoanCodeResult.Status = True Then
                            'if ping to OAC service successful
                            retLoanCodeResult.Status = False
                            If Not ct.IsCancellationRequested Then
                                asyncAnimation.StartAnimation(btnLoanCode.Left, AnimatedCirclePanelAlign.Left)
                                Await task.Run(
                                    Sub()
                                        'Check application code exists in OAC
                                        retCheckApplicationResult = onlineApplication.CheckApplicationCode(thisApplicationCode)
                                    End Sub
                                              )
                                asyncAnimation.EndAnimation()
                                If Not ct.IsCancellationRequested Then
                                    asyncAnimation.StartAnimation(btnLoanCode.Left, AnimatedCirclePanelAlign.Left)
                                    Await task.Run(
                                        Sub()
                                            'get Loan Code from Online Application Centre returns LoanCode and Is Application Exists in OAC status
                                            retLoanCodeResult = onlineApplication.GetLoanCode(thisApplicationCode)
                                        End Sub
                                                  )
                                    asyncAnimation.EndAnimation()
                                End If
                            End If

                            If retLoanCodeResult Is Nothing Then retLoanCodeResult = New TrackResult

                            If retLoanCodeResult.Status = False Then
                                SetToolbarStatusMessage(ToolStripStatusLabel1, retLoanCodeResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                                btnLoanCode.Text = btnText
                                MessageBox.Show("ERROR on Manual Loan: " + vbCrLf + retLoanCodeResult.ErrorMessage)
                            Else
                                'Check Loan code returned status 
                                If retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.Status = True Then
                                    ''set Status message
                                    SetToolbarStatusMessage(ToolStripStatusLabel1, String.Format("Loan code {0} returned by OAC service", retLoanCodeResult.ResultStringValue), TrueTrackStatusBarMessageType.BlackMessage)
                                    'show any messages returned from web service
                                    If Not String.IsNullOrWhiteSpace(retLoanCodeResult.ErrorMessage) Then MessageBox.Show(retLoanCodeResult.ErrorMessage)

                                    asyncAnimation.StartAnimation(btnLoanCode.Left, AnimatedCirclePanelAlign.Left)
                                    Application.DoEvents()
                                    'Boolean Value assign status Application Exits in OAC from CheckApplicationCode exits web method
                                    retLoanCodeResult.ResultBooleanValue = If(retCheckApplicationResult IsNot Nothing, retCheckApplicationResult.ResultBooleanValue, False)

                                    'Cannot Cancel beyond this if LoanCode is generated
                                    Await Task.Factory.StartNew(
                                            Sub()
                                                btnLoanCode.Text = btnText
                                                btnCreatefinPowerLoan.Enabled = False
                                                'Do post process operations 
                                                ret = LoanCodeManuallyPostProcess(retLoanCodeResult)
                                            End Sub,
                                                CancellationToken.None, TaskCreationOptions.None, taskScheduler)
                                    asyncAnimation.EndAnimation()
                                End If

                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error on manual loan", TrueTrackStatusBarMessageType.RedMessage)
            MessageBox.Show("ERROR on Manual Loan: " + vbCrLf + ex.Message)
            btnLoanCode.Visible = True
            btnLoanCode.Enabled = True
            btnLoanCode.Text = btnText
        Finally
            Cursor.Current = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Post process LoanCode  received from OAC service
    ''' Updates LoanCode in LoanComments and in DueDiligence
    ''' </summary>
    ''' <param name="retLoanCodeResult"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function LoanCodeManuallyPostProcess(retLoanCodeResult As TrackResult) As Boolean
        Dim btnText As String = "Manual Loan (no XML)"
        Try
            Dim retUpdateResult As TrackResult
            If retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.Status = False Then
                'Show toolbar status message
                MessageBox.Show(retLoanCodeResult.ErrorMessage)
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Loan code not returned by web service", TrueTrackStatusBarMessageType.RedMessage)
            ElseIf retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.Status = True Then
                btnLoanCode.Enabled = False
                ' Save loan code in DueDiligence table and update in LoanComments in Enquiry Table
                thisLoanCode = retLoanCodeResult.ResultStringValue
                'loan code will also be updated into DueDiligence table when btnAudit_Click is called
                txtbxLoanNum.Text = thisLoanCode

                If Not String.IsNullOrWhiteSpace(thisLoanCode) Then
                    'Update LoanComments field in Enquiry Table
                    'UpdateLoanComments("LoanCode " & thisLoanCode & " received successfully.")
                    Util.SetComment(thisEnquiryId, "System", "LoanCode " & thisLoanCode & " received successfully.")
                End If
                'Save the loan code into due diligence table
                retUpdateResult = CreateDueDiligenceRecordSaveLoanCode()
            End If

            'Result contains LoandCode number and IsApplicationExistInOAC status
            If retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.Status = True Then
                'Changes Buttons to invisible
                btnLoanCode.Visible = False
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Loan code received successfully", TrueTrackStatusBarMessageType.BlackMessage)
                'Show Audit button
                If retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.ResultBooleanValue = True Then
                    'if Application Exists (ResultBooleanValue = true) in OAC then show create loan in finPower
                    btnCreatefinPowerLoan.Visible = True
                    btnCreatefinPowerLoan.Enabled = True
                    btnAudit.Visible = False
                ElseIf retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.ResultBooleanValue = False Then
                    'if Application does NOT Exist (ResultBooleanValue = false) in OAC then do not show create loan in finPower
                    btnCreatefinPowerLoan.Visible = False
                    btnAudit.Enabled = True
                    btnAudit.Visible = True
                End If
                'Set to Audit mode
                'btnAudit_Click(sender, e)
            ElseIf retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.Status = False Then
                btnLoanCode.Visible = True
                btnAudit.Visible = False
                btnCreatefinPowerLoan.Visible = False
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Manual Loan: " + vbCrLf + ex.Message)
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error on manual loan", TrueTrackStatusBarMessageType.RedMessage)

        End Try

        Return True
    End Function


    ''' <summary>
    ''' Process to get loan code manually
    ''' Save loancode into Duediligence table, show error message if failed to get LoanCode
    ''' Update LoanComments with Loan code
    ''' </summary>
    ''' <returns>status of generation of loancode</returns>
    ''' <remarks></remarks>
    Private Function GetLoanCodeManually(ct As CancellationToken) As TrackResult
        Dim retLoanCodeResult As New TrackResult
        Dim retCheckApplicationResult As New TrackResult
        Dim onlineApplication As New OnlineApplication
        Try
            retLoanCodeResult.Status = False
            If CheckEnquiryHasApplicationCode() Then

                'Cursor.Current = Cursors.WaitCursor
                onlineApplication = New OnlineApplication
                If Not ct.IsCancellationRequested Then
                    'Ping Online Application Centre web service to check the service is available
                    retLoanCodeResult = onlineApplication.PingOnlineApplicationService()
                    If retLoanCodeResult Is Nothing Then retLoanCodeResult = New TrackResult()
                    If retLoanCodeResult.Status = False Then
                        retLoanCodeResult.StatusMessage = "Web service not available"
                    Else
                        retLoanCodeResult.Status = False
                        If Not ct.IsCancellationRequested Then
                            'Check application exists in OAC
                            retCheckApplicationResult = onlineApplication.CheckApplicationCode(thisApplicationCode)

                            If Not ct.IsCancellationRequested Then
                                'get Loan Code from Online Application Centre returns LoanCode and Is Application Exists in OAC status
                                retLoanCodeResult = onlineApplication.GetLoanCode(thisApplicationCode)
                            End If
                        End If
                    End If
                End If
            End If

            If retLoanCodeResult Is Nothing Then retLoanCodeResult = New TrackResult
            If retLoanCodeResult IsNot Nothing AndAlso retLoanCodeResult.Status = True Then
                'Boolean Value assign status Application Exits in OAC
                retLoanCodeResult.ResultBooleanValue = If(retCheckApplicationResult IsNot Nothing, retCheckApplicationResult.Status, False)
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            If retLoanCodeResult Is Nothing Then retLoanCodeResult = New TrackResult()
            retLoanCodeResult.ErrorMessage = ex.Message
        End Try

        Return retLoanCodeResult

    End Function


    ' ''' <summary>
    ' ''' Updates LoanComments field in Enquiry Table
    ' ''' </summary>
    ' ''' <param name="StrComment"></param>
    ' ''' <remarks></remarks>
    'Private Sub UpdateLoanComments(ByVal StrComment As String)
    '    Try
    '        'add a Loan Comment
    '        Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
    '        EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
    '        EnquiryRow.BeginEdit()

    '        'create log note
    '        Dim DateStr As String
    '        Dim NewCommentStr As String
    '        Dim CommentStr As String
    '        Dim LoanComments As String = EnquiryRow.LoanComments
    '        'Create Date string
    '        DateStr = "-- " & DateTime.Now.ToString("f")
    '        'Add Date string to comments
    '        CommentStr = DateStr & " | " & LoggedinName & vbCrLf & StrComment & vbCrLf & vbCrLf
    '        'check loan comments from dataset exist
    '        If Not LoanComments = "" Then
    '            NewCommentStr = CommentStr & LoanComments
    '        Else
    '            NewCommentStr = CommentStr
    '        End If
    '        'update LoanComments 
    '        EnquiryRow.LoanComments = NewCommentStr

    '        'save changes
    '        'Me.Validate()
    '        EnquiryBindingSource.EndEdit()
    '        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

    '    Catch ex As Exception

    '    End Try

    'End Sub


    Private Sub MCApplicationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MCApplicationsToolStripMenuItem.Click
        MessageBox.Show("Sorry, currently this function is not available", "Not Functioning", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'Dim frmMembersCentreArchive As New MembersCentreArchive
        'frmMembersCentreArchive.StartPosition = FormStartPosition.CenterParent
        'frmMembersCentreArchive.txtApplicationCode.Text = lblAppCodeValue.Text
        'frmMembersCentreArchive.ShowDialog()


    End Sub

    ''' <summary>
    ''' Changes the status to Audit - Admin Only feature
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Reset only. Reset to Audit from Payout. Menu item only available in payout.</remarks>
    Private Async Sub ChangeToAuditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangeToAuditToolStripMenuItem.Click
        'Cursor.Current = Cursors.WaitCursor
        Me.UseWaitCursor = True
        Dim commentId As Integer
        Dim CommentStr As String = String.Empty
        Dim continueChange As Boolean = False
        Dim msgResult As DialogResult
        'Check if enquiry is archived
        If thisIsArchived = True Then
            MessageBox.Show("Enquiry is Archived!" & vbCrLf & "Can not proceed.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            'rewrite to use Reset functionality, done 24/10/2017           
            'leave if bindingsource.current is nothing
            Try
                If Not EnquiryBindingSource.Current Is Nothing Then
                    'set OAC Status
                    'Check is possible to Change Remote Status
                    Dim systemCanChangeRemoteStatus As Boolean
                    Dim oacStatusResult As New TrackResult
                    Dim retTTResult As New TrackResult
                    Dim onlineApplicationService As New OnlineApplication
                    Dim thisAppsettings As New AppSettings
                    Dim note As String = String.Empty
                    systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                    If systemCanChangeRemoteStatus = True Then
                        'Change Status in OAC, Check OAC is in correct status (Payout)
                        'Get status from the OAC
                        oacStatusResult = Await onlineApplicationService.GetOacStatus(thisApplicationCode)
                        If oacStatusResult.ErrorCode = MyEnums.ResponseErrorCode.None Then
                            If oacStatusResult.ResultIntegerValue = OACWebService.ApplicationStatusType.Payout Then
                                Dim actionType As Integer = OACWebService.ApplicationActionType.Reset
                                retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
                                If retTTResult.Status = False Then 'error condition
                                    commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                                    MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    continueChange = False
                                Else
                                    continueChange = True
                                End If
                            Else 'error condition
                                SetToolbarStatusMessage(ToolStripStatusLabel1, "OAC in incorrect status", TrueTrackStatusBarMessageType.RedMessage)
                                MessageBox.Show("OAC not in status of Payout", "OAC in incorrect status", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                continueChange = False
                            End If
                        Else 'error condition
                            SetToolbarStatusMessage(ToolStripStatusLabel1, oacStatusResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                            msgResult = MessageBox.Show(oacStatusResult.ErrorMessage & vbCrLf & "Do you wish to continue?", oacStatusResult.StatusMessage, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                            If msgResult = System.Windows.Forms.DialogResult.Yes Then
                                continueChange = True
                            Else
                                continueChange = False
                            End If
                        End If

                    End If

                    If continueChange = True Then
                        btnLoanCode.Visible = False
                        btnCreatefinPowerLoan.Visible = False
                        Me.TableAdapterManager.DueDiligenceTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.DueDiligence, thisEnquiryId)
                        'if payout table exists then set payout.payoutComplete date to Null
                        'to avoid concurrency error in btnSaveFile
                        Dim payoutDataTable As EnquiryWorkSheetDataSet.PayoutDataTable
                        payoutDataTable = Me.TableAdapterManager.PayoutTableAdapter.GetDataByEnquiryId(thisEnquiryId)
                        If payoutDataTable.Rows.Count > 0 Then
                            Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                            payoutRow = payoutDataTable(0)
                            payoutRow.BeginEdit()
                            Dim index As Integer = payoutDataTable.Columns.IndexOf("PayoutCompleted")
                            payoutRow.SetField(index, DBNull.Value)

                            Try
                                Me.Validate()
                                Me.PayoutBindingSource.EndEdit()
                                Me.PayoutTableAdapter.Update(payoutDataTable)
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox("ERROR: Updating of Payout fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
                            End Try
                        End If

                        'add tabPage
                        If Not tbctrlAppDetails.Contains(tabDueDiligence) Then
                            tbctrlAppDetails.TabPages.Add(tabDueDiligence)
                        End If
                        'Set to Audit mode
                        'change Current Status and create comment
                        'update fields
                        Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                        EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                        EnquiryRow.BeginEdit()
                        'update Current Status
                        EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_AUDIT '"Audit"
                        EnquiryRow.CurrentStatusSetDate = DateTime.Now
                        'Set Original Enquiry Time
                        Dim OriginalTime As DateTime
                        OriginalTime = EnquiryRow.DateTime
                        'calculate time difference for 30min status
                        Dim StatusTimeDiff As Integer
                        StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
                        If StatusTimeDiff < 30 Then
                            EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_AUDIT '"Audit"
                        End If
                        'save changes
                        Me.Validate()
                        EnquiryBindingSource.EndEdit()
                        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
                        'create log note
                        CommentStr = "Status reset to Audit"
                        commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)
                        'change buttom visibility
                        btnAudit.Visible = False
                        btnAudit.Enabled = False
                        'incase form has been reset
                        btnExportFiles.Visible = False
                        btnExportFiles.Enabled = False
                        'change buttom enabled
                        btnPassedAudit.Visible = True
                        btnPassedAudit.Enabled = True

                        'set focus
                        tbctrlAppDetails.SelectedTab = tabDueDiligence
                        'form refresh
                        FormRefresh()

                    End If

                Else
                    'no current record in binding source - do not proceed
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No data entered, please enter data!"

                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("ERROR: Reset to Audit caused an error" & vbCrLf & ex.Message)
            End Try
        End If

        Me.UseWaitCursor = False
        Cursor.Current = Cursors.Default

    End Sub

    ''' <summary>
    ''' get application html file from OAC using web service
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Async Sub btnGetOACApplicationFile_Click(sender As Object, e As System.EventArgs) Handles btnGetOACApplicationFile.Click
        Dim btnText As String = "Documents Executed"
        Dim ret As Boolean = False
        Dim retHtmlResult As New TrackResult
        retHtmlResult.Status = False
        Dim retStatusResult As New TrackResult
        Try
            'Cancellation Token
            Dim ct As CancellationToken
            'This TaskScheduler captures SynchronizationContext.Current.
            Dim taskScheduler As TaskScheduler = taskScheduler.FromCurrentSynchronizationContext()

            'Check the button Text is Cancel or not
            If btnGetOACApplicationFile.Text.IndexOf("Cancel") > -1 Then
                cancellationTokenSource.Cancel()
                btnGetOACApplicationFile.Text = btnText
                btnGetOACApplicationFile.Visible = True
                Exit Sub
            Else
                SetToolbarStatusMessage(ToolStripStatusLabel1, "", TrueTrackStatusBarMessageType.BlackMessage)
                'Instantiate cancellation token
                cancellationTokenSource = New CancellationTokenSource()
                ct = cancellationTokenSource.Token
                btnGetOACApplicationFile.Text = "Cancel"
            End If

            asyncAnimation.StartAnimation(btnGetOACApplicationFile.Left, AnimatedCirclePanelAlign.Left)
            If WebUtil.WebServiceAvailable Then
                asyncAnimation.EndAnimation()
                'Check if user has cancelled the operation
                If Not ct.IsCancellationRequested Then
                    'Version 2.5.0 
                    'Set OAC Status to 'Documents Executed'
                    SetToolbarStatusMessage(ToolStripStatusLabel1, "Setting OAC status", TrueTrackStatusBarMessageType.BlackMessage)
                    asyncAnimation.StartAnimation(btnCreatefinPowerLoan.Left, AnimatedCirclePanelAlign.Left)
                    Application.DoEvents()
                    'makes an asynchronous call to the function that sets the status
                    Await Task.Run(Sub()
                                       retStatusResult = SetStatusDocsExecutedOAC()
                                   End Sub)
                    asyncAnimation.EndAnimation()
                    If retStatusResult.Status = False Then
                        SetToolbarStatusMessage(ToolStripStatusLabel1, retStatusResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                        btnCreatefinPowerLoan.Text = btnText
                        MessageBox.Show(retStatusResult.ErrorMessage, "OAC service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'End of Version 2.5.0
                    Else
                        'Setting OAC status was successful
                        Util.SetComment(thisEnquiryId, "System", "Setting the OAC status completed succesfully")
                        'Check the process is cancelled. 
                        If Not ct.IsCancellationRequested Then
                            asyncAnimation.StartAnimation(btnGetOACApplicationFile.Left, AnimatedCirclePanelAlign.Left)
                            SetToolbarStatusMessage(ToolStripStatusLabel1, "Retrieving Application file from OAC", TrueTrackStatusBarMessageType.BlackMessage)
                            Application.DoEvents()
                            Await Task.Run(Sub()
                                               retHtmlResult = GetOACApplicationFile()
                                           End Sub)
                            asyncAnimation.EndAnimation()
                            'Check the process is cancelled. Cancel is possible even after file has been retrieved
                            If Not ct.IsCancellationRequested Then
                                If retHtmlResult.Status = False Then
                                    SetToolbarStatusMessage(ToolStripStatusLabel1, retHtmlResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
                                    btnGetOACApplicationFile.Text = btnText
                                    MessageBox.Show(retHtmlResult.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Else
                                    asyncAnimation.StartAnimation(btnGetOACApplicationFile.Left, AnimatedCirclePanelAlign.Left)
                                    Application.DoEvents()
                                    'run the following in the UI thread for updates
                                    Await Task.Factory.StartNew(
                                        Sub()
                                            'Post process save html file into enquiry folder 
                                            ret = GetApplicationFilePostProcess(retHtmlResult, ct)
                                            If ret Then
                                                SetToolbarStatusMessage(ToolStripStatusLabel1, "Ready state", TrueTrackStatusBarMessageType.BlackMessage)
                                                'if LoanType is Topup or variation
                                                '25/01/2018 add SplitLoanDrawDown
                                                If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                                                    'if application file saved successfully, then show Audit button
                                                    btnAudit.Visible = True
                                                    btnAudit.Enabled = True
                                                End If
                                                'hide the button to get Application file from the OAC web service 
                                                btnGetOACApplicationFile.Visible = False
                                            End If

                                        End Sub,
                                                  CancellationToken.None, TaskCreationOptions.None, taskScheduler)
                                    asyncAnimation.EndAnimation()
                                    btnGetOACApplicationFile.Text = btnText
                                End If
                            End If
                        End If
                    End If
                End If

            Else
                asyncAnimation.EndAnimation()
                MessageBox.Show("Web Service Server not available" & vbCrLf & "Try again later, if error persists contact your administrator", "Web Service Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnCreatefinPowerLoan.Enabled = True
                btnCreatefinPowerLoan.Text = btnText
                SetToolbarStatusMessage(ToolStripStatusLabel1, "Web Service connection failed", TrueTrackStatusBarMessageType.RedMessage)
                Exit Sub
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            btnGetOACApplicationFile.Visible = True
            btnGetOACApplicationFile.Enabled = True
            btnGetOACApplicationFile.Text = btnText
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error while retrieving application file from OAC", TrueTrackStatusBarMessageType.RedMessage)
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Finally

        End Try

    End Sub

    ''' <summary>
    ''' Check application code exits  for the enquiry
    ''' if code does not exists then set the form toolbarStatus message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckEnquiryHasApplicationCode() As Boolean
        Dim retVal As Boolean = False
        If String.IsNullOrWhiteSpace(thisApplicationCode) Then
            MessageBox.Show("Please select a valid Application code", "Application code", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'set StatusStrip text
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Application code not selected", TrueTrackStatusBarMessageType.RedMessage)
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

    ' ''' <summary>
    ' ''' Calls the check enquiry folder exists method and shows message
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function CheckAndAlertEnquiryFolderExists() As Boolean
    '    Dim TTResult As TrackResult
    '    TTResult = CheckEnquiryFolderExists()
    '    If TTResult.Status = False Then
    '        MessageBox.Show(TTResult.ErrorMessage, "Folder error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        'set StatusStrip text
    '        SetToolbarStatusMessage(ToolStripStatusLabel1, TTResult.StatusMessage, TrueTrackStatusBarMessageType.RedMessage)
    '    End If
    '    Return TTResult.Status
    'End Function


    ''' <summary>
    ''' Checks the enquiry folder exists in True Track
    ''' if does not exists then set the form Toolbar status message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckEnquiryFolderExists() As TrackResult
        Dim status As Boolean = False
        Dim enquiryFolder As String
        Dim TTResult As New TrackResult
        enquiryFolder = ApplicationInterchange.GetWorkSheetFolder(thisEnquiryCode)
        Try
            'If Not Directory.Exists(enquiryFolder) Then
            '    Directory.CreateDirectory(enquiryFolder)
            'End If
            TTResult = ApplicationInterchange.CheckFolderExists(enquiryFolder)

            If TTResult Is Nothing Then
                TTResult = New TrackResult
            End If
            status = TTResult.Status

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            TTResult.ErrorMessage = ex.Message
        End Try
        If status = False Then
            TTResult.StatusMessage = "Enquiry folder does not exist"
            If String.IsNullOrWhiteSpace(TTResult.ErrorMessage) Then
                TTResult.ErrorMessage = TTResult.StatusMessage
            End If
        End If
        Return TTResult

    End Function

    ''' <summary>
    ''' Gets Applications file from OAC web service
    ''' </summary>
    ''' <param name="ct"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetOACApplicationFile(ct As CancellationToken) As TrackResult
        Dim applicationsOps As New OnlineApplication
        Dim retTTResult As New TrackResult
        retTTResult.Status = False
        Dim checkFolderResult As TrackResult
        checkFolderResult = CheckEnquiryFolderExists()
        If checkFolderResult.Status Then
            Try
                If Not ct.IsCancellationRequested Then
                    'application file from OAC and copy to enquiry folder
                    retTTResult = applicationsOps.getApplicationFile(thisApplicationCode)
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show("ERROR on Manual Loan: " + vbCrLf + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            retTTResult = checkFolderResult
        End If

        Return retTTResult

    End Function

    ' ''' <summary>
    ' ''' Checks Applicationcode exists for the Enquiry and Enquiry folder exists
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Public Function PreProcessChecksForFileRetrievalFromOACService() As TrackResult
    '    Dim retTTResult As New TrackResult
    '    Dim enquiryFolder As String

    '    enquiryFolder = ApplicationInterchange.GetWorkSheetFolder(ThisEnquiryCode)
    '    'Check an Application Code is assigned to Enquiry
    '    If CheckEnquiryHasApplicationCode() Then
    '        'Checks enquiry folder exists else create a folder for the Enquiry
    '        If CheckAndAlertEnquiryFolderExists() Then
    '            'if successful
    '            retTTResult.Status = True
    '        End If
    '    End If

    '    Return retTTResult

    'End Function

    ''' <summary>
    ''' Gets application HTML file from OAC
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOACApplicationFile() As TrackResult
        Dim onlineApplicationService As New OnlineApplication
        Dim retTTResult As New TrackResult

        'application file from OAC and copy to enquiry folder
        retTTResult = onlineApplicationService.getApplicationFile(thisApplicationCode)

        Return retTTResult

    End Function



    ''' <summary>
    ''' Save Application file to TrueTrack folder
    ''' sets the form Toolbar status with appropriate messages if failed
    ''' </summary>
    ''' <returns>true when file is retrieved from OAC</returns>
    ''' <remarks></remarks>
    Private Function GetApplicationFilePostProcess(retTTResult As TrackResult, ct As CancellationToken) As Boolean
        Dim onlineApplicationService As New OnlineApplication
        Dim enquiryFolder As String
        Dim retTTResult2 As TrackResult
        Dim ret As Boolean = False
        Try
            enquiryFolder = ApplicationInterchange.GetWorkSheetFolder(thisEnquiryCode)

            If retTTResult IsNot Nothing Then
                'if Error while fetching file from web service
                If retTTResult.Status = False Then
                    MessageBox.Show(retTTResult.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'set StatusStrip text
                    SetToolbarStatusMessage(ToolStripStatusLabel1, "Application file retrieval failed from OAC!", TrueTrackStatusBarMessageType.RedMessage)
                Else
                    'not cancelled
                    If Not ct.IsCancellationRequested Then
                        'if successfully fetched Application file
                        'set StatusStrip text
                        SetToolbarStatusMessage(ToolStripStatusLabel1, "Application file retrieved successfully from OAC", TrueTrackStatusBarMessageType.BlackMessage)
                        retTTResult2 = onlineApplicationService.SaveFile(retTTResult.ResultStringValue, "APP_" & thisApplicationCode & ".htm", enquiryFolder)
                        ret = retTTResult2.Status
                        If retTTResult2.Status = False Then
                            MessageBox.Show(retTTResult2.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'set StatusStrip text
                            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error saving application file to TrueTrack!", TrueTrackStatusBarMessageType.RedMessage)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on retrieving application file: " + vbCrLf + ex.Message)
        End Try

        Return ret

    End Function


    ''' <summary>
    ''' Sets the Status to Documents Executed in the OAC
    ''' </summary>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Function SetStatusDocsExecutedOAC() As TrackResult
        Dim onlineApplicationService As New OnlineApplication
        Dim retTTResult As New TrackResult
        retTTResult = onlineApplicationService.SetStatusDocsExecuted(thisApplicationCode)
        Return retTTResult
    End Function


    ''' <summary>
    ''' Gets Xml data file from OAC service
    ''' </summary>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Function GetOACXmlFile() As TrackResult
        Dim onlineApplicationService As New OnlineApplication
        Dim retTTResult As New TrackResult

        Dim checkFolderResult As TrackResult
        checkFolderResult = CheckEnquiryFolderExists()
        If checkFolderResult.Status Then
            'retrieve Xml file from OAC
            retTTResult = onlineApplicationService.GetXmlFile(thisApplicationCode)
            If retTTResult.Status = "False" Then
                retTTResult.StatusMessage = "Error retrieving Xml file."
            End If
        Else
            retTTResult = checkFolderResult
        End If

        Return retTTResult

    End Function


    ''' <summary>
    ''' Save the Xml of Application into enquiry folder
    ''' </summary>
    ''' <param name="retTTResult"></param>
    ''' <param name="ct"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function GetXmlFilePostProcess(retTTResult As TrackResult, ct As CancellationToken) As Boolean
        Dim _onlineApplication As New OnlineApplication
        Dim _retXmlSaveResult2 As New TrackResult
        Dim _enquiryFolder As String
        Dim _retVal As Boolean = False


        'if Error while fetching file from web service
        If retTTResult.Status = False Then
            MessageBox.Show(retTTResult.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'set StatusStrip text
            SetToolbarStatusMessage(ToolStripStatusLabel1, "Xml file retrieval failed!", TrueTrackStatusBarMessageType.RedMessage)
        Else
            'Save the Xml into enquiry folder
            If Not String.IsNullOrWhiteSpace(retTTResult.ResultStringValue) Then
                SetToolbarStatusMessage(ToolStripStatusLabel1, "XML data file retrieved from OAC service", TrueTrackStatusBarMessageType.BlackMessage)
                'get the enquiry folder path
                _enquiryFolder = ApplicationInterchange.GetWorkSheetFolder(thisEnquiryCode)
                'replace encoding label to utf-8
                retTTResult.ResultStringValue = retTTResult.ResultStringValue.Replace("utf-16", "utf-8")
                If Not ct.IsCancellationRequested Then
                    'save the Xml file in enquiry folder
                    _retXmlSaveResult2 = _onlineApplication.SaveFile(retTTResult.ResultStringValue, "XML_" & thisApplicationCode & ".xml", _enquiryFolder)
                    If _retXmlSaveResult2 IsNot Nothing Then
                        'set the status after saving Xml file in the enquiry folder
                        _retVal = _retXmlSaveResult2.Status
                        'If there is any error while saving
                        If _retXmlSaveResult2.Status = False Then
                            MessageBox.Show(_retXmlSaveResult2.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'set StatusStrip text
                            SetToolbarStatusMessage(ToolStripStatusLabel1, "Error saving Xml file to TrueTrack!", TrueTrackStatusBarMessageType.RedMessage)
                        Else
                            SetToolbarStatusMessage(ToolStripStatusLabel1, "XML data file saved successfully to TrueTrack", TrueTrackStatusBarMessageType.BlackMessage)
                        End If
                    Else
                        'if no response handle here
                    End If
                End If
            End If
        End If

        Return _retVal

    End Function




    ''' <summary>
    ''' Checks Application exists for the Application code provided
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckApplicationExistsinOAC() As TrackResult
        Dim _onlineApplication As New OnlineApplication
        Dim _retTTResult As New TrackResult
        Dim _retVal As Boolean = False

        If CheckEnquiryHasApplicationCode() Then
            'Calls web service method check Application Exists for the Application code provided
            _retTTResult = _onlineApplication.CheckApplicationCode(thisApplicationCode)

        End If
        If _retTTResult.Status = False Then
            _retTTResult.StatusMessage = "Error occurred while checking application exists in OAC"
        End If
        Return _retTTResult

    End Function

    ''' <summary>
    ''' Sets text in Form Toolbar Status Label  ToolStripStatusLabel1
    ''' </summary>
    ''' <param name="toolStripStatusLabel1"></param>
    ''' <param name="message"></param>
    ''' <param name="msgType">Type sets the color of the text to show severity</param>
    ''' <remarks></remarks>
    Private Sub SetToolbarStatusMessage(toolStripStatusLabel1 As ToolStripStatusLabel, message As String, msgType As TrueTrackStatusBarMessageType)
        Dim _msgColor As Color
        Select Case msgType
            Case TrueTrackStatusBarMessageType.RedMessage
                _msgColor = Color.Red
            Case TrueTrackStatusBarMessageType.BlueMessage
                _msgColor = Color.Blue
            Case TrueTrackStatusBarMessageType.BlackMessage
                _msgColor = Color.Black
        End Select
        'set StatusStrip text
        toolStripStatusLabel1.ForeColor = _msgColor
        toolStripStatusLabel1.Text = message
    End Sub



    ''' <summary>
    ''' Set the type for message Statusbar
    ''' </summary>
    ''' <remarks></remarks>
    Enum TrueTrackStatusBarMessageType
        RedMessage
        BlueMessage
        BlackMessage

    End Enum

    ' ''' <summary>
    ' ''' Set Enabled property to False/True for Audit Processing Groupboxes
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Private Sub ProcessingGpbxsDisabled(Optional ByVal disable As Boolean = True)
    '    If disable = True Then
    '        gpbxProcessingForm.Enabled = False
    '        gpbxProcessingDocs.Enabled = False
    '        gpbxProcessingFinPower.Enabled = False
    '        gpbxProcessingPPSR.Enabled = False
    '        gpbxProcessingProtecta.Enabled = False
    '        gpbxProcessingInsurance.Enabled = False
    '    Else
    '        gpbxProcessingForm.Enabled = True
    '        gpbxProcessingDocs.Enabled = True
    '        gpbxProcessingFinPower.Enabled = True
    '        gpbxProcessingPPSR.Enabled = True
    '        gpbxProcessingProtecta.Enabled = True
    '        gpbxProcessingInsurance.Enabled = True
    '    End If

    'End Sub

    ''' <summary>
    ''' Set Enabled property to True/False for Audit Processing controls
    ''' </summary>
    ''' <param name="enable"></param>
    ''' <remarks></remarks>
    Private Sub enableAuditControls(ByVal enable As Boolean)
        If enable = True Then
            gpbxfinPowerNum.Enabled = True
            gpbxLoanBreakdown.Enabled = True
            gpbxDealerComm.Enabled = True
            gpbxDocuments.Enabled = True
            gpbxProcessingForm.Enabled = True
            gpbxProcessingDocs.Enabled = True
            gpbxProcessingFinPower.Enabled = True
            gpbxProcessingPPSR.Enabled = True
            gpbxProcessingProtecta.Enabled = True
            gpbxProcessingInsurance.Enabled = True
            'make disbursments readonly (so can be copied)
            For Each tb As Control In tlpDisbursements.Controls
                If tb.GetType() Is GetType(TextBox) Then
                    CType(tb, TextBox).ReadOnly = False
                End If
            Next
            gpbxAuditFinal.Enabled = True
            btnAuditUpdate.Enabled = True

        Else
            gpbxfinPowerNum.Enabled = False
            gpbxLoanBreakdown.Enabled = False
            gpbxDealerComm.Enabled = False
            gpbxDocuments.Enabled = False
            gpbxProcessingForm.Enabled = False
            gpbxProcessingDocs.Enabled = False
            gpbxProcessingFinPower.Enabled = False
            gpbxProcessingPPSR.Enabled = False
            gpbxProcessingProtecta.Enabled = False
            gpbxProcessingInsurance.Enabled = False
            'make disbursments readonly (so can be copied)
            For Each tb As Control In tlpDisbursements.Controls
                If tb.GetType() Is GetType(TextBox) Then
                    CType(tb, TextBox).ReadOnly = True
                End If
            Next
            gpbxAuditFinal.Enabled = False
            btnAuditUpdate.Enabled = False
        End If
    End Sub

#Region "Enquiry number label"
    Private Sub lblEnquiryNumberValue_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lblEnquiryNumberValue.MouseDoubleClick
        lblEnquiryNumberValue.BackColor = Color.LightPink
        Clipboard.SetDataObject(lblEnquiryNumberValue.Text, False)

        'lblEnquiryNumberValue.BackColor = Color.LightBlue
    End Sub


    Private Sub lblEnquiryNumberValue_MouseHover(sender As Object, e As EventArgs) Handles lblEnquiryNumberValue.MouseHover
        Cursor.Current = Cursors.Hand
        lblEnquiryNumberValue.BackColor = Color.LightBlue
    End Sub

    Private Sub lblEnquiryNumberValue_MouseLeave(sender As Object, e As EventArgs) Handles lblEnquiryNumberValue.MouseLeave
        Cursor.Current = Cursors.Default
        lblEnquiryNumberValue.BackColor = SystemColors.Control
    End Sub
#End Region

#Region "Customers"
    Private Sub dgvEnquiryCustomers_Click(sender As Object, e As EventArgs) Handles dgvEnquiryCustomers.Click
        Cursor.Current = Cursors.WaitCursor
        'Handlers

        SetToolbarStatusMessage(ToolStripStatusLabel1, "", TrueTrackStatusBarMessageType.BlackMessage)
        'Clear Customer table
        EnquiryWorkSheetDataSet.Tables("Customer").Clear()
        Dim thisCustType As Integer = 0
        Dim customerEnquiryType As Integer = -1
        Dim selectedRowView As Data.DataRowView
        Dim selectedRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
        'Get selected row
        selectedRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
        If selectedRowView.Row IsNot Nothing Then
            selectedRow = selectedRowView.Row
            thisCustType = selectedRow.CustomerType 'Individual, Company, Trust
            customerEnquiryType = selectedRow.Type 'main, co-main, guarantor
            'load customer table
            Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, selectedRow.CustomerId)
            loadedCustomerId = selectedRow.CustomerId 'Current customer to be displayed
            lblCustTypeValue.Text = MyEnums.GetDescription(DirectCast(thisCustType, MyEnums.CustomerType))
            'set customer panels
            SetCustomerTypePanel(thisCustType, loadedCustomerId, customerEnquiryType)

            pnlCustomerType.Visible = True
            pnlCustomerType.Enabled = True

        End If
        'Handlers        

    End Sub

    Private Sub SetCustomerTypePanel(ByVal custType As Integer, Optional ByVal custId As Integer = 0, Optional ByVal custEnquiryType As Integer = -1)
        Dim acceptanceMethod As Integer
        Dim amlRisk As Integer
        Dim typeOfId As Integer
        Dim typeDL As Integer
        thisTypeOfCustomer = custType
        '#######################
        'Individual
        '#######################
        If custType = MyEnums.CustomerType.Individual Or custType = MyEnums.CustomerType.SoleTrader Then
            'panels
            pnlIndividual.Visible = True
            pnlCompany.Visible = False
            pnlCompany.Enabled = False
            'panel display settings
            pnlIndividual.Enabled = True
            Util.SetReadonlyControls(pnlIndividual.Controls)
            '*** Employment
            'Set gpbxEmployment section for SoleTrader
            If custType = MyEnums.CustomerType.SoleTrader Then
                gpbxEmployment.Text = "Trading activity"
                lblEmployer.Visible = False
                txtbxEmployer.Visible = False
                'txtbxEmployer.Enabled = False
                lblSpokeTo.Visible = False
                txtbxSpokeTo.Visible = False
                'txtbxSpokeTo.Enabled = False
                btnRangEmpTimeStamp.Visible = False
                'btnRangEmpTimeStamp.Enabled = False
                lblDoes.Visible = False
                lblSalutation.Visible = False
                lblWorkCompany.Text = "How long has the business been trading?"
                'lblQuestion1.Location = New Point(316 - lblQuestion1.Width, 48)
                lblFulltime.Text = "Is this sufficient?"
                lblEmploy9.Text = "Do they have financials? Do we have copies?"
                lblWhatDo.Text = "Is this the main source of income?"
                lblHowLong.Text = "What sort of business is it?"
                txtbxQuestion5.Visible = False
                'txtbxQuestion5.Enabled = False
                lblPay.Text = "Is it a satificatory business to lend to?"
                lblJobSafe.Text = "Is it registered with the IRD?"
                lblWorker.Visible = False
                txtbxQuestion8.Visible = False
                'txtbxQuestion8.Enabled = False
                'ckbxSelfEmployed.Enabled = False
                ckbxSelfEmployed.Visible = False
                lblBeneficiary.Visible = False
                ckbxBeneficiary.Enabled = False
                'ckbxBeneficiary.Visible = False
                lblEmployerNotRung.Visible = False
                'ckbxEmployerNotRung.Enabled = False
                ckbxEmployerNotRung.Visible = False
            Else
                gpbxEmployment.Text = "Employment"
                lblEmployer.Visible = True
                txtbxEmployer.Visible = True
                'txtbxEmployer.Enabled = True
                lblSpokeTo.Visible = True
                txtbxSpokeTo.Visible = True
                'txtbxSpokeTo.Enabled = True
                btnRangEmpTimeStamp.Visible = True
                'btnRangEmpTimeStamp.Enabled = True
                lblDoes.Visible = True
                lblSalutation.Visible = True
                lblWorkCompany.Text = "work for your company?"
                lblFulltime.Text = "Are they fulltime permanent or other?"
                lblEmploy9.Text = "Are they PAYE employee, Contractor, Invoice or Other?"
                lblWhatDo.Text = "What does your employee do (job)?"
                lblHowLong.Text = "How long have they been there?"
                txtbxQuestion5.Visible = True
                'txtbxQuestion5.Enabled = True
                lblPay.Text = "I need to confirm their pay of"
                lblJobSafe.Text = "Is their job safe?"
                lblWorker.Visible = True
                txtbxQuestion8.Visible = True
                'txtbxQuestion8.Enabled = True
                'ckbxSelfEmployed.Enabled = True
                ckbxSelfEmployed.Visible = True
                lblBeneficiary.Visible = True
                'ckbxBeneficiary.Enabled = True
                ckbxBeneficiary.Visible = True
                lblEmployerNotRung.Visible = True
                'ckbxEmployerNotRung.Enabled = True
                ckbxEmployerNotRung.Visible = True

                'set visiblity for EnquiryType
                If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                    ckbxEmployerNotRung.Visible = True
                    lblEmployerNotRung.Visible = True
                Else
                    ckbxEmployerNotRung.Visible = False
                    lblEmployerNotRung.Visible = False
                End If
            End If

            'existing customer
            If custId > 0 Then
                If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                    acceptanceMethod = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                    lblIndividAcceptanceMethodValue.Text = MyEnums.GetDescription(DirectCast(acceptanceMethod, MyEnums.CustAcceptanceMethod))
                Else
                    lblIndividAcceptanceMethodValue.Text = "Unknown"
                End If
                amlRisk = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AMLRisk")
                lblRiskAssessValue.Text = MyEnums.GetDescription(DirectCast(amlRisk, MyEnums.AMLRisk))
                typeOfId = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IdType")), -1, EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IdType"))
                typeDL = If(IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("DriverLicenceType")), -1, EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("DriverLicenceType"))
                If typeOfId > -1 Then
                    lblTypeOfIdValue.Text = MyEnums.GetDescription(DirectCast(typeOfId, MyEnums.IdType))
                    If typeOfId = MyEnums.IdType.NZDriversLicence Then
                        If typeDL > -1 Then
                            lblTypeDLValue.Text = MyEnums.GetDescription(DirectCast(typeDL, MyEnums.DriverLicenceType))
                        Else
                            lblTypeDLValue.Text = "Unknown"
                        End If
                        lblTypeDL.Visible = True
                        lblTypeDLValue.Visible = True
                    Else
                        lblTypeDL.Visible = False
                        lblTypeDLValue.Visible = False
                    End If
                Else
                    lblTypeOfIdValue.Text = "Unknown"
                    lblTypeDL.Visible = False
                    lblTypeDLValue.Visible = False
                End If



                'set CustomerEnquiryType: main, co-main, guarantor
                If custEnquiryType > -1 Then
                    lblIndividCustEnquiryTypeValue.Text = MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType))
                Else
                    lblIndividCustEnquiryTypeValue.Text = "Unknown"
                End If

                'Set radio buttons for CreditQuestions 1,2 and 3
                Try
                    SetRadioButtonState(1)
                    SetRadioButtonState(2)
                    SetRadioButtonState(3)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("Load Form: SetRadioButtonState generated an exception message:" + vbCrLf + ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try
                'Set radio buttons for IsaPep
                SetRbPep()
                'Set Customer Salutation
                lblSalutation.Text = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("Salutation")

                '    'set Rang employer timestamps
                '    If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("EmployerRangdate") IsNot DBNull.Value Then
                '        If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("EmployerRangdate") > TestDate Then
                '            btnRangEmpTimeStamp.Enabled = False
                '        Else
                '            btnRangEmpTimeStamp.Enabled = True
                '        End If
                '    Else
                '        btnRangEmpTimeStamp.Enabled = True
                '    End If
            Else
                lblRiskAssessValue.Text = "Unknown"
                lblIndividAcceptanceMethodValue.Text = "Unknown"
                lblIndividCustEnquiryTypeValue.Text = "Unknown"
                lblTypeOfIdValue.Text = "Unknown"
                lblTypeDLValue.Text = "Unknown"
                '    cmbxTenancyType.SelectedIndex = -1
            End If

            '#######################
            'Company
            '#######################
        ElseIf custType = MyEnums.CustomerType.Company Then
            'Set variables               
            'Handlers
            'panels
            pnlCompany.Visible = True
            pnlIndividual.Visible = False
            pnlIndividual.Enabled = False
            'panel display settings            
            pnlCompany.Enabled = True
            Util.SetReadonlyControls(pnlCompany.Controls)
            'existing customer
            If custId > 0 Then
                If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                    acceptanceMethod = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                    lblCompanyAcceptanceMethodValue.Text = MyEnums.GetDescription(DirectCast(acceptanceMethod, MyEnums.CustAcceptanceMethod))
                Else
                    lblCompanyAcceptanceMethodValue.Text = "Unknown"
                End If
                'set CustomerEnquiryType: main, co-main, guarantor
                If custEnquiryType > -1 Then
                    lblBusCustEnquiryTypeValue.Text = MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType))
                Else
                    lblBusCustEnquiryTypeValue.Text = "Unknown"
                End If
                'Set radio buttons for CreditQuestions 1,2 and 3
                Try
                    SetBusRadioButtonState(1)
                    SetBusRadioButtonState(2)
                    SetBusRadioButtonState(3)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("Load Form: SetBusRadioButtonState generated an exception message:" + vbCrLf + ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try

                'setup global variables
                dtsbType23 = MyEnums.DtsbType.ShareHolder
                dtsbType01 = MyEnums.DtsbType.Director
                'Shareholders
                Try
                    Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType23))
                    custCountDtsbType23 = DTSBBindingSource.Count
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try
                'Directors
                Try
                    Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType01))
                    custCountDtsbType01 = DTSB01BindingSource.Count
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try

                gpbxDtsb23.Enabled = False
                gpbxDtsb01.Enabled = False
                'set columns for dgvDtsb01
                dgvDtsb01.Columns(dgvDtsb01.Columns.Item("Dtsb01Id").Index).Visible = False
                dgvDtsb01.Columns(dgvDtsb01.Columns.Item("Dtsb01CustomerId").Index).Visible = False
                dgvDtsb01.Columns(dgvDtsb01.Columns.Item("Dtsb01Type").Index).Visible = False
                dgvDtsb01.Columns(dgvDtsb01.Columns.Item("Dtsb01Description").Index).Visible = False
                'set columns for dgvDtsb23
                dgvDTSBType2.Columns(dgvDTSBType2.Columns.Item("DTSBType2Id").Index).Visible = False
                dgvDTSBType2.Columns(dgvDTSBType2.Columns.Item("DTSBType2CustomerId").Index).Visible = False
                dgvDTSBType2.Columns(dgvDTSBType2.Columns.Item("DTSBType2Type").Index).Visible = False
                dgvDTSBType2.Columns(dgvDTSBType2.Columns.Item("DTSBType2Description").Index).Visible = False

                'References
                Try
                    Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
                    refCount = TradeReferencesBindingSource.Count
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try

            Else
                lblIndividAcceptanceMethodValue.Text = "Unknown"
                lblIndividCustEnquiryTypeValue.Text = "Unknown"
            End If
            'rename labels
            gpbxCompanyInfo.Text = "Company information"
            ckbxRegistered.Visible = True
            ckbxCoExtTrustDdSaved.Text = "Company extract saved"
            ckbxCapital.Text = "?Paid-up capital is sufficient"
            ckbxHistory.Text = "?Company history is satisfactory"
            lblHistoryComments.Text = "Comments on the Company's History"
            lblCompanyAddresses.Text = "Comment on the Company Addresses"
            gpbxDtsb23.Text = "Shareholders"
            ckbxShareIdentified.Text = "?All shareholders identified"
            ckbxShareStructure.Text = "?Shareholding structure identified"
            ckbxShareholders.Text = "?Shareholders are satisfactory"
            lblShareholdersNote.Text = "Comments on Shareholders and their related parties/interests"
            gpbxDtsb01.Text = "Directors"
            ckbxDirectorsTrustees.Text = "?Directors are satisfactory"
            lblDirectors.Text = "Comments on Directors and their related parties/interests"
            gpbxCoTenancy.Text = "Company tenancy"
            gpbxBusinessCredit.Text = "Company credit"
            'Handlers

            '#######################
            'Trust
            '#######################
        ElseIf custType = MyEnums.CustomerType.Trust Then
            'Set variables
            'Handlers
            'panels
            pnlCompany.Visible = True
            pnlIndividual.Visible = False
            pnlIndividual.Enabled = False
            'panel display settings            
            pnlCompany.Enabled = True
            Util.SetReadonlyControls(pnlCompany.Controls)
            'existing customer
            If custId > 0 Then
                If EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod") IsNot DBNull.Value Then
                    acceptanceMethod = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("AcceptanceMethod")
                    lblCompanyAcceptanceMethodValue.Text = MyEnums.GetDescription(DirectCast(acceptanceMethod, MyEnums.CustAcceptanceMethod))
                Else
                    lblCompanyAcceptanceMethodValue.Text = ""
                End If
                'set CustomerEnquiryType: main, co-main, guarantor
                If custEnquiryType > -1 Then
                    lblBusCustEnquiryTypeValue.Text = MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType))
                Else
                    lblBusCustEnquiryTypeValue.Text = "Unknown"
                End If
                'Set radio buttons for CreditQuestions 1,2 and 3
                Try
                    SetBusRadioButtonState(1)
                    SetBusRadioButtonState(2)
                    SetBusRadioButtonState(3)
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("Load Form: SetBusRadioButtonState generated an exception message:" + vbCrLf + ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try

                'setup global variables
                dtsbType23 = MyEnums.DtsbType.Beneficiary
                dtsbType01 = MyEnums.DtsbType.Trustee
                'Benficiaries
                Try
                    Me.DTSBTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType23))
                    custCountDtsbType23 = DTSBBindingSource.Count
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try
                'Trustees
                Try
                    Me.DTSB01TableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DTSB01, New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(dtsbType01))
                    custCountDtsbType01 = DTSB01BindingSource.Count
                Catch ex As System.Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try

                gpbxDtsb23.Enabled = False
                gpbxDtsb01.Enabled = False
                'set columns for dgvDtsb01
                dgvDtsb01.Columns(0).Visible = False
                dgvDtsb01.Columns(1).Visible = False
                dgvDtsb01.Columns(2).Visible = False
                'set columns for dgvDtsb23
                dgvDTSBType2.Columns(0).Visible = False
                dgvDTSBType2.Columns(1).Visible = False
                dgvDTSBType2.Columns(2).Visible = False

                'References
                Try
                    Me.TradeReferencesTableAdapter.FillByCustomerId(Me.EnquiryWorkSheetDataSet.TradeReferences, New System.Nullable(Of Integer)(custId))
                    refCount = TradeReferencesBindingSource.Count
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                End Try

            Else
                lblIndividAcceptanceMethodValue.Text = "Unknown"
                lblIndividCustEnquiryTypeValue.Text = "Unknown"
            End If
            'rename labels
            gpbxCompanyInfo.Text = "Trust information"
            ckbxRegistered.Visible = False
            ckbxCoExtTrustDdSaved.Text = "Trust Deed verified and saved"
            ckbxCapital.Text = "?Source of funds is satisfactory"
            ckbxHistory.Text = "?Trust history is satisfactory"
            lblHistoryComments.Text = "Comments on the Trust's History"
            lblCompanyAddresses.Text = "Comment on the Trust Addresses"
            gpbxDtsb23.Text = "Beneficiaries"
            ckbxShareIdentified.Text = "?All beneficiaries identified"
            ckbxShareStructure.Text = "?Beneficiary structure identified"
            ckbxShareholders.Text = "?Beneficiaries are satisfactory"
            lblShareholdersNote.Text = "Comments on beneficiaries and their related parties/interests"
            gpbxDtsb01.Text = "Trustees"
            ckbxDirectorsTrustees.Text = "?Trustees are satisfactory"
            lblDirectors.Text = "Comments on Trustees and their related parties/interests"
            gpbxCoTenancy.Text = "Trust tenancy"
            gpbxBusinessCredit.Text = "Trust credit"
            'Handlers

        End If


    End Sub

    ''' <summary>
    ''' Set the Checked state of a radio button group
    ''' </summary>
    ''' <param name="num">Integer value for the radio button</param>
    ''' <remarks>QuestionYesSet[Num] and QuestionNoset[Num] radio buttons in group</remarks>
    Private Sub SetRadioButtonState(ByVal num As Integer)
        'set the radio button states for Credit Questions
        Dim QuestionYesSet As String = "rbtnYesCreditQuestion" & num
        Dim QuestionNoset As String = "rbtnNoCreditQuestion" & num
        Dim CheckState As String = "CreditQuestion" & num
        'get State of Question from database
        Dim StateQuestion As Boolean = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item(CheckState)
        Try
            'Look through Controls Collection to find 'QuestionYesSet'
            Dim controls1() As Control = Me.Controls.Find(QuestionYesSet, True)
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton1 As RadioButton = DirectCast(controls1(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton1.Checked = False
                    Case 1
                        ThisRadioButton1.Checked = True
                    Case Else
                        ThisRadioButton1.Checked = False
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try
        Try
            'Look through Controls Collection to find 'QuestionNoSet'
            Dim controls2() As Control = Me.Controls.Find(QuestionNoset, True)
            If controls2.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton2 As RadioButton = DirectCast(controls2(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton2.Checked = True
                    Case 1
                        ThisRadioButton2.Checked = False
                    Case Else
                        ThisRadioButton2.Checked = True
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' Set the Checked state of a Business radio button group
    ''' </summary>
    ''' <param name="num">Integer value for the radio button</param>
    ''' <remarks>QuestionYesSet[Num] and QuestionNoset[Num] radio buttons in group</remarks>
    Private Sub SetBusRadioButtonState(ByVal num As Integer)
        'set the radio button states for Credit Questions
        Dim QuestionYesSet As String = "rbtnYesBusCreditQuestion" & num
        Dim QuestionNoset As String = "rbtnNoBusCreditQuestion" & num
        Dim CheckState As String = "CreditQuestion" & num
        'get State of Question from database
        Dim StateQuestion As Boolean = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item(CheckState)
        Try
            'Look through Controls Collection to find 'QuestionYesSet'
            Dim controls1() As Control = Me.Controls.Find(QuestionYesSet, True)
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton1 As RadioButton = DirectCast(controls1(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton1.Checked = False
                    Case 1
                        ThisRadioButton1.Checked = True
                    Case Else
                        ThisRadioButton1.Checked = False
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Try
            'Look through Controls Collection to find 'QuestionNoSet'
            Dim controls2() As Control = Me.Controls.Find(QuestionNoset, True)
            If controls2.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton2 As RadioButton = DirectCast(controls2(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case StateQuestion
                    Case 0
                        ThisRadioButton2.Checked = True
                    Case 1
                        ThisRadioButton2.Checked = False
                    Case Else
                        ThisRadioButton2.Checked = True
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region



#Region "PEP button"

    Private Sub pbPEP_Click(sender As Object, e As EventArgs) Handles pbPEP.Click
        'Dim msgText As String = "politically exposed person means—" & vbNewLine & "(a) an individual who holds, or has held at any time in the preceding 12 months, in any overseas country the prominent public function of—" & vbNewLine & vbTab & "(i) Head of State or head of a country or government; or" & vbNewLine & vbTab & "(ii) government minister or equivalent senior politician; or" & vbNewLine & vbTab & "(iii) Supreme Court Judge or equivalent senior Judge; or" & vbNewLine & vbTab & "(iv) governor of a central bank or any other position that has comparable influence to the Governor of the Reserve Bank of New Zealand; or" & vbNewLine & vbTab & "(v) senior foreign representative, ambassador, or high commissioner; or" & vbNewLine & vbTab & "(vi) high-ranking member of the armed forces; or" & vbNewLine & vbTab & "(vii) board chair, chief executive, or chief financial officer of, or any other position that has comparable influence in, any State enterprise; and" & vbNewLine & "(b) an immediate family member of a person referred to in paragraph (a), including—" & vbNewLine & vbTab & "(i) a spouse; or" & vbNewLine & vbTab & "(ii) a partner, being a person who is considered by the relevant national law as equivalent to a spouse; or" & vbNewLine & vbTab & "(iii) a child and a child’s spouse or partner; or " & vbNewLine & vbTab & "(iv) a parent; and" & vbNewLine & "(c) having regard to information that is public or readily available,—" & vbNewLine & vbTab & "(i) any individual who is known to have joint beneficial ownership of a legal entity or legal arrangement, or any other close relationship, with a person referred to in paragraph (a); or" & vbNewLine & vbTab & "(ii) any individual who has sole beneficial ownership of a legal entity or legal arrangement that is known to exist for the benefit of a person described in paragraph (a)" & vbNewLine

        'MessageBox.Show(msgText, "What is a politically exposed person", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\PoliticallyExposedPerson.rtf", "What is a politically exposed person?", MyEnums.MsgBoxMode.FilePath, 700)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()

    End Sub

    Private Sub pbPEP_MouseHover(sender As Object, e As EventArgs) Handles pbPEP.MouseHover
        pbPEP.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbPEP_MouseLeave(sender As Object, e As EventArgs) Handles pbPEP.MouseLeave
        pbPEP.Image = My.Resources.questionBsm
    End Sub

    Private Sub SetRbPep()
        Dim pepState As Boolean
        Dim state As Integer
        If IsDBNull(EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IsaPEP")) = True Then
            state = 0
        Else
            pepState = EnquiryWorkSheetDataSet.Customer.Rows(CustomerBindingSource.Position()).Item("IsaPEP")
            If pepState = True Then
                state = 2
            Else
                state = 1
            End If

        End If

        Select Case state
            Case 0 'IsDBNull
                'rbPepState = 0
                rbPepNo.Checked = False
                rbPepYes.Checked = False
            Case 1 'False
                'rbPepState = 1
                rbPepNo.Checked = True
            Case 2 'true
                'rbPepState = 2
                rbPepYes.Checked = True
        End Select
    End Sub

#End Region

#Region "Information buttons"
    'Naming Conventions
    Private Sub pbNC_Click(sender As Object, e As EventArgs) Handles pbNC.Click
        'Dim msgText As String = "All files that need to go to the finPower Client folder need to be prefixed with Client[TrueTrack Id number]_ " & vbNewLine & "The TrueTrack Id number can be found in the Enquiry Customers grid in the Id column." & vbNewLine & vbNewLine & "Client Personal Guarantee documents to be prefixed with" & vbNewLine & "Client[TrueTrack Id number]_PG[date]" & vbNewLine & "Client GSA documents to be prefixed with" & vbNewLine & "Client[TrueTrack Id number]_GSA[date]" & vbNewLine & "Special case for Clients is their Identity documents." & vbNewLine & "Prefix documents with ID[ TrueTrack Id number] _[customer name]" & vbNewLine & vbNewLine & "ALL other files will be exported to the finPower Loan folder" & vbNewLine & "Naming of Loan documents:" & vbNewLine & "LoanDocs to be prefixed with LoanDocs_[date]" & vbNewLine & "Variation documents to be prefixed with LoanVariation_[date]" & vbNewLine & vbNewLine

        'MessageBox.Show(msgText, "Document naming conventions", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\DocNameConvention.rtf", "Document Naming Conventions", MyEnums.MsgBoxMode.FilePath, 600)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()

    End Sub

    Private Sub pbNC_MouseHover(sender As Object, e As EventArgs) Handles pbNC.MouseHover
        pbNC.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbNC_MouseLeave(sender As Object, e As EventArgs) Handles pbNC.MouseLeave
        pbNC.Image = My.Resources.questionBsm
    End Sub

#End Region



#Region "Audit: finPower mapping"

    Private Sub ClientNumTextBox_KeyDown(sender As Object, e As EventArgs) Handles ClientNumTextBox.KeyDown
        If sender.[GetType]().ToString().EndsWith("TextBox") Then
            Dim txt As TextBox = DirectCast(sender, TextBox)
            If Trim(txt.Text).Length > 0 Then
                txt.BackColor = Color.LavenderBlush
            End If
        End If

    End Sub


    Private Sub ClientNumTextBox_TextChanged(sender As Object, e As EventArgs) Handles ClientNumTextBox.TextChanged
        If sender.[GetType]().ToString().EndsWith("TextBox") Then
            Dim txt As TextBox = DirectCast(sender, TextBox)
            'set tag to isDirty
            txt.Tag = 1
        End If
    End Sub


    Private Sub ClientNumTextBox_LostFocus(sender As Object, e As EventArgs) Handles ClientNumTextBox.LostFocus
        Cursor.Current = Cursors.WaitCursor
        SetToolbarStatusMessage(ToolStripStatusLabel1, "Saving Client Number", TrueTrackStatusBarMessageType.BlueMessage)
        Application.DoEvents()
        Try
            If sender.[GetType]().ToString().EndsWith("TextBox") Then
                Dim txt As TextBox = DirectCast(sender, TextBox)
                'get Id_number
                Dim enqCustId As String = txt.Parent.Controls("lblIdValue").Text
                'Test if text isdirty   
                If txt.Tag = 1 Then
                    Dim row As EnquiryWorkSheetDataSet.EnquiryCustomersRow
                    'drFinpower bindingsource is enqCustBS
                    Dim rowIndex As Integer = Me.enqCustBS.Find("Id_number", enqCustId)
                    Me.enqCustBS.Position = rowIndex
                    row = CType(CType(Me.enqCustBS.Current, DataRowView).Row, EnquiryWorkSheetDataSet.EnquiryCustomersRow)
                    'CustomerId = row.CustomerId
                    'Update the Customer, fill customer dataset
                    Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, row.CustomerId)
                    Dim customerRow As EnquiryWorkSheetDataSet.CustomerRow
                    customerRow = EnquiryWorkSheetDataSet.Customer.Rows(0)
                    customerRow.BeginEdit()
                    'customerRow.ClientNum = Trim(txt.Text)
                    'Convert text to TitleCase
                    customerRow.ClientNum = StrConv(Trim(txt.Text), VbStrConv.ProperCase)
                    customerRow.EndEdit()
                    Me.CustomerTableAdapter.Update(EnquiryWorkSheetDataSet.Customer)
                    EnquiryWorkSheetDataSet.Customer.AcceptChanges()
                    'reset tag
                    txt.Tag = 0
                    If Trim(txt.Text) = String.Empty Then
                        txt.BackColor = Color.LavenderBlush
                    Else
                        txt.BackColor = SystemColors.Window
                    End If
                End If
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message + vbNewLine + ex.TargetSite.ToString)
        End Try
        SetToolbarStatusMessage(ToolStripStatusLabel1, "Status: Ready.", TrueTrackStatusBarMessageType.BlackMessage)
        Cursor.Current = Cursors.Default

    End Sub


#End Region

    ''' <summary>
    ''' Calculate TotalDrawdown and AmountFinanced and set Total values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetAuditAmounts()
        'TotalDrawdown and AmountFinanced
        'Page scope variables
        'TotalDrawdown As Decimal (Audit tab)
        'Refinance As Decimal (Audit tab)
        'AmountFinanced As Decimal (Audit tab)
        'drawdownPaidOut As Decimal (Audit tab)
        'drawdownRetention As Decimal (Audit tab)
        Dim cashPriceAdvance As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("CashPrice_Advance")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("CashPrice_Advance"))
        Dim otherAdvance As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("OtherAdvance")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("OtherAdvance"))
        'add txtTradeIn (SetAuditAmounts()), add to database [TradeIn] (money,null)
        Dim tradeIn As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("TradeIn")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("TradeIn"))
        Dim deposit As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("Deposit")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("Deposit"))
        Dim brokerage As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("Brokerage")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("Brokerage"))
        Refinance = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("Refinance")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("Refinance"))
        Dim yflFees As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("YFLFees")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("YFLFees"))
        Dim yflInsurances As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("YFLInsurances")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("YFLInsurances"))
        TotalDrawdown = cashPriceAdvance + otherAdvance + brokerage - deposit - tradeIn
        lblDrawdownValue.Text = Format(TotalDrawdown, "C")
        drawdownRetention = Decimal.Round(TotalDrawdown * If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanRetention")) / 100, 2)
        lblDrawdownRetentionValue.Text = Format(drawdownRetention, "C")
        drawdownPaidOut = TotalDrawdown - drawdownRetention
        lblDrawdownPaidOutValue.Text = Format(drawdownPaidOut, "C")
        AmountFinanced = TotalDrawdown + Refinance + yflFees + yflInsurances
        lblFinancedValue.Text = Format(AmountFinanced, "C")

        'Commission
        'Page scope variables
        'DealerCommission As Decimal - Total Dealer Commission (Audit tab)
        'CommPaid2Dealer As Decimal
        'CommPaid2Retention As Decimal
        Dim productComm As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("ProductCommission")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("ProductCommission"))
        Dim interestComm As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("InterestCommission")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("InterestCommission"))
        Dim commRetention As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("CommRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("CommRetention"))
        DealerCommission = Decimal.Round(productComm + interestComm, 2)
        lblTotalCommValue.Text = DealerCommission.ToString("C")
        CommPaid2Retention = Decimal.Round((productComm + interestComm) * commRetention / 100, 2)
        lblCommPaid2RetentionValue.Text = CommPaid2Retention.ToString("C")
        CommPaid2Dealer = productComm + interestComm - CommPaid2Retention
        lblCommPaid2DealerValue.Text = CommPaid2Dealer.ToString("C")

        'Disbursements
        Dim disburseAmt1 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt1")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt1"))
        Dim disburseAmt2 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt2")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt2"))
        Dim disburseAmt3 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt3")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt3"))
        Dim disburseAmt4 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt4")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt4"))
        Dim disburseAmt5 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt5")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt5"))
        Dim disburseAmt6 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt6")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt6"))
        TotalDisburseAmt = disburseAmt1 + disburseAmt2 + disburseAmt3 + disburseAmt4 + disburseAmt5 + disburseAmt6
        lblDisburseAmtTotal.Text = TotalDisburseAmt.ToString("C")
        'warning message
        If Not TotalDisburseAmt = drawdownPaidOut Then
            lblDisburseWarning.Visible = True
        Else
            lblDisburseWarning.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Calculate PaidOut and Refinance Amount and set Total values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetPayoutAmounts()
        'Payout - total PaidOut Amount
        Dim thisPaidOutAmt1 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt1")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt1"))
        Dim thisPaidOutAmt2 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt2")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt2"))
        Dim thisPaidOutAmt3 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt3")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt3"))
        Dim thisPaidOutAmt4 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt4")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt4"))
        Dim thisPaidOutAmt5 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt5")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt5"))
        Dim thisPaidOutAmt6 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt6")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt6"))
        Dim thisPaidOutAmt7 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("HoldbackAmt")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("HoldbackAmt"))
        Dim thisPaidOutAmt8 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RetentionAmt")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RetentionAmt"))
        Dim thisPaidOutAmt9 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("CommissionAmt")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("CommissionAmt"))
        TotalPaidOutAmt = thisPaidOutAmt1 + thisPaidOutAmt2 + thisPaidOutAmt3 + thisPaidOutAmt4 + thisPaidOutAmt5 + thisPaidOutAmt6 + thisPaidOutAmt7 + thisPaidOutAmt8 + thisPaidOutAmt9
        lblTotalPaidOutAmt.Text = TotalPaidOutAmt.ToString("C")
        'total Refinance amount
        Dim thisRefinanceAmt1 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceAmt1")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceAmt1"))
        Dim thisRefinanceAmt2 As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceAmt2")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceAmt2"))
        'Financed amount = TotalDrawdown + Refinance amount + Fees + Insurances
        TotalFinAmt = thisRefinanceAmt1 + thisRefinanceAmt2 + TotalDrawdown '(Totaldrawdown + refinance)
        lblTotalFinAmt.Text = TotalFinAmt.ToString("C")
        'Paid out = disbursements + Dealer commission
        If Not TotalPaidOutAmt = TotalDisburseAmt + DealerCommission + drawdownRetention Then
            lblPaidoutWarning.Visible = True
        Else
            lblPaidoutWarning.Visible = False
        End If
        'Audit refinance = Payout refinance?
        If Not (thisRefinanceAmt1 + thisRefinanceAmt2) = Refinance Then
            lblRefinanceWarning.Visible = True
        Else
            lblRefinanceWarning.Visible = False
        End If
    End Sub

#Region "File processing"



    '################################
    'Revise file processing 24/02/2017
    '################################
    ''' <summary>
    ''' Process all files in the directory passed in, recurse on any directories that are found, and process the files they contain.
    ''' </summary>
    ''' <param name="targetDirectory"></param>
    ''' <param name="finPowerNumPath">path to finPower Loan folder</param>
    ''' <param name="enquiryNumPath">path to TrueTrack Enquiry folder</param>
    ''' <param name="transferFileType">Client or Loan</param>
    ''' <param name="enquiryType"></param>
    ''' <param name="custIdNumber"></param>
    ''' <remarks></remarks>
    Public Shared Sub ProcessDirectory(ByVal targetDirectory As String, ByVal finPowerNumPath As String, ByVal enquiryNumPath As String, ByVal transferFileType As Integer, ByVal enquiryType As Integer, Optional ByVal custIdNumber As String = "")

        Dim fileEntries As String() = Directory.GetFiles(targetDirectory)
        ' Process the list of files found in the directory.
        Dim fileName As String
        For Each fileName In fileEntries
            ProcessFile(fileName, finPowerNumPath, enquiryNumPath, transferFileType, enquiryType, custIdNumber)
        Next fileName
        Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)
        ' Recurse into subdirectories of this directory.
        Dim subdirectory As String
        For Each subdirectory In subdirectoryEntries
            ProcessDirectory(subdirectory, finPowerNumPath, enquiryNumPath, transferFileType, enquiryType, custIdNumber)
        Next subdirectory

    End Sub 'ProcessDirectory

    ''' <summary>
    ''' Logic for processing found files and moving them to finPower folder
    ''' </summary>
    ''' <param name="path">path to TrueTrack Enquiry file</param>
    ''' <param name="finPowerNumPath">path to finPower Loan folder</param>
    ''' <param name="enquiryNumPath">path to TrueTrack Enquiry folder</param>
    ''' <param name="transferFileType">Client or Loan</param>
    ''' <param name="enquiryType"></param>
    ''' <param name="custIdNumber"></param>
    ''' <remarks></remarks>
    Public Shared Sub ProcessFile(ByVal path As String, ByVal finPowerNumPath As String, ByVal enquiryNumPath As String, ByVal transferFileType As Integer, ByVal enquiryType As Integer, Optional ByVal custIdNumber As String = "")
        'create str array
        Dim str As String = ""
        Dim strExt As String = ""
        Dim validFileName As String = ""
        Dim finfo As New FileInfo(path)
        If finfo.Exists Then
            'Compare file path with enquirypath
            'add diff to str
            Dim txt1(path.Split("\").Length) As String
            Dim txt2(enquiryNumPath.Split("\").Length) As String
            txt1 = path.Split("\")
            txt2 = enquiryNumPath.Split("\")
            Dim diff1 As String = ""
            For Each diff As String In txt1
                If Array.IndexOf(txt2, diff.ToString) = -1 Then
                    diff1 += "\" & diff.ToString
                End If
            Next

            If String.IsNullOrEmpty(diff1) Then
                str = finfo.Name
            Else
                str = diff1.Substring(1) 'remove leading"\"
            End If

            'str = finfo.Name
            strExt = finfo.Extension
            'move files
            Try
                If transferFileType = MyEnums.TransferFileType.Client Then
                    If enquiryType = MyEnums.EnquiryType.FinanceFacility Then 'no loan for FinanceFacility and only one customer
                        validFileName = ValidateFileName(finPowerNumPath, str, strExt)
                        My.Computer.FileSystem.MoveFile(path, finPowerNumPath & "\" & validFileName, True)
                    Else
                        If Not String.IsNullOrEmpty(custIdNumber) Then
                            If finfo.Name.StartsWith("ID_" & custIdNumber) Or finfo.Name.StartsWith("ID" & custIdNumber) Or finfo.Name.StartsWith("Client" & custIdNumber) Then
                                validFileName = ValidateFileName(finPowerNumPath, str, strExt)
                                My.Computer.FileSystem.MoveFile(path, finPowerNumPath & "\" & validFileName, True)
                            End If
                        Else
                            MessageBox.Show("No Customer Id number.", "Error in Client file transfer", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If


                ElseIf transferFileType = MyEnums.TransferFileType.Loan Then
                    'Client files should have been processed first so now move whatever is left to the finPower loan folder
                    validFileName = ValidateFileName(finPowerNumPath, str, strExt)
                    My.Computer.FileSystem.MoveFile(path, finPowerNumPath & "\" & validFileName, True)
                End If
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("ERROR on processing file: " + vbCrLf + ex.Message)
            End Try

        End If

    End Sub 'ProcessFile

    ''' <summary>
    ''' Recurse into subdirectories of this directory and delete them
    ''' </summary>
    ''' <param name="targetDirectory"></param>
    ''' <remarks></remarks>
    Private Sub DeleteDirectories(ByVal targetDirectory As String)
        'If Directory.GetFiles(targetDirectory).Count = 0 And Directory.GetDirectories(targetDirectory).Count = 0 Then
        '    Thread.Sleep(10)
        '    Directory.Delete(targetDirectory)
        'End If
        ''Recurse into subdirectories of this directory.
        'Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)
        'Dim subdirectory As String
        'For Each subdirectory In subdirectoryEntries
        '    DeleteDirectories(subdirectory)
        'Next subdirectory

        Directory.Delete(targetDirectory, True)



    End Sub

    '################################
#End Region

    ''' <summary>
    ''' Check database due-diligence values
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckDueDiligenceData() As String
        Dim checkDbMessage As String = String.Empty
        Dim ddRow As EnquiryWorkSheetDataSet.DueDiligenceRow
        ddRow = Me.EnquiryWorkSheetDataSet.DueDiligence(0)
        '################################
        'NOT Variation and NOT SplitLoanDrawdown and NOT FinanceFacility
        '################################
        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            'check final
            If Not ddRow.finPowerOpen = True Then
                checkDbMessage = checkDbMessage & "Please check FinPower has been Opened" & vbCrLf
            End If
            If Not ddRow.WelcomeLetter = True Then
                checkDbMessage = checkDbMessage & "Please check the Welcome letter has been sent" & vbCrLf
            End If
        ElseIf thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Then
            'check final
            If Not ddRow.finPowerOpen = True And Not ddRow.WelcomeLetter = True Then
                checkDbMessage = checkDbMessage & "Please check FinPower has been Opened or Withdrawal Completed" & vbCrLf
            End If
        ElseIf thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
            If Not ddRow.DDinPlace = True Then
                checkDbMessage = checkDbMessage & "Please check DD / AP  is entered on sheet or DD is in place" & vbCrLf
            End If
            'check final
            If Not ddRow.finPowerWithdrawal = True Then
                checkDbMessage = checkDbMessage & "Please check FinPower withdrawal has been completed" & vbCrLf
            End If
        ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            'check final
            If Not ddRow.finPowerOpen = True Then
                checkDbMessage = checkDbMessage & "Please check FinPower Dealer Setup has been completed" & vbCrLf
            End If
            If Not ddRow.WelcomeLetter = True Then
                checkDbMessage = checkDbMessage & "Please check the new Dealer details have been passed to the Accountant" & vbCrLf
            End If
        End If

        '################################
        'NOT SplitLoanDrawdown
        '################################
        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
            'check documents
            If Not ddRow.Application = True Then
                checkDbMessage = checkDbMessage & "Please check that you have received the original application, completed and signed correctly" & vbCrLf
            End If
            If Not ddRow.Documents = True Then
                checkDbMessage = checkDbMessage & "Please check that all supporting documents are clear and legible" & vbCrLf
            End If
            'check processing DD/AP Form
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If (ddRow.APDDShtNum Is DBNull.Value Or Not ddRow.APDDShtNum.Length > 0) And Not ddRow.DDinPlace = True And Not ddRow.PayBookSent = True And Not ddRow.APDDClientFiled = True And Not ddRow.WageDeduction = True Then
                    checkDbMessage = checkDbMessage & "Please check DD / AP  is entered on sheet or DD is in place" & vbCrLf & _
                    "or Payment Book given to Client or Wage deduction confirmed" & vbCrLf & _
                    "or Client setup themselves and the Agreement Form has been filed" & vbCrLf
                End If
                If Not ddRow.APDDClientFiled = True Then
                    If Not ddRow.BanckAcc = True And Not ddRow.PayBookSent = True And Not ddRow.WageDeduction = True Then
                        checkDbMessage = checkDbMessage & "Please check Bank Acc No in finPower and / or Payment book given to client" + vbCrLf + "or Wage deduction have been checked" + vbCrLf
                    End If
                End If
                If Not ddRow.PayMethod = True Then
                    checkDbMessage = checkDbMessage & "Please check the Pay Method in finPower is correct." + vbCrLf
                End If
            End If
            'check processing documents
            If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If Not ddRow.DDAPsaved = True Then
                    checkDbMessage = checkDbMessage & "Please check Evaluation sheet is scanned or saved" + vbCrLf
                End If
            End If
            If Not ddRow.IDsaved = True Then
                checkDbMessage = checkDbMessage & "Please check IDs are scanned or saved" + vbCrLf
            End If
            If Not ddRow.AppSaved = True Then
                If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    checkDbMessage = checkDbMessage & "Please check the Dealer Agreement is scanned or saved" + vbCrLf
                Else
                    checkDbMessage = checkDbMessage & "Please check the application is scanned or saved" + vbCrLf
                End If
            End If
            If Not ddRow.ProofAddress = True Then
                If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    checkDbMessage = checkDbMessage & "Please check the Deposit slip is scanned or saved" + vbCrLf
                Else
                    checkDbMessage = checkDbMessage & "Please check the proof of address documents are scanned or saved" + vbCrLf
                End If
            End If
            If Not ddRow.ProofIncome = True Then
                checkDbMessage = checkDbMessage & "Please check the proof of income documents are scanned or saved" + vbCrLf
            End If
            'check processing FinPower
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If Not ddRow.finClient = True Then
                    checkDbMessage = checkDbMessage & "Please check FinPower Client fields are filled in and correct" + vbCrLf
                End If
                If Not ddRow.finLoan = True Then
                    checkDbMessage = checkDbMessage & "Please check FinPower Loan fields are filled in and correct" + vbCrLf
                End If
                If Not ddRow.finSecurity = True Then
                    checkDbMessage = checkDbMessage & "Please check FinPower Security fields are filled in and correct" + vbCrLf
                End If
                If Not ddRow.finGrading = True Then
                    checkDbMessage = checkDbMessage & "Please check Client has been graded in FinPower" + vbCrLf
                End If
            End If
            'check processing PPSR
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If Not ddRow.PPSRNew = True And Not ddRow.PPSRAmended = True And Not ddRow.PPSRCurrent = True Then
                    checkDbMessage = checkDbMessage & "Please check PPSR registration" + vbCrLf
                End If
                If Not ddRow.PPSRFiled = True Or Not ddRow.FSDebtor = True Or Not ddRow.FSType = True Or Not ddRow.FSDesc = True Then
                    checkDbMessage = checkDbMessage & "Please check PPSR Financial Statement" + vbCrLf
                End If
            End If
            'check Protecta
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If ddRow.InsuranceIdentified = False And ddRow.NoProtectaPolicies = False Then
                    If ddRow.SpecialNotes Is DBNull.Value Then
                        checkDbMessage = checkDbMessage & "Please check your Protecta processing" + vbCrLf
                    ElseIf String.IsNullOrEmpty(ddRow.SpecialNotes) Then
                        checkDbMessage = checkDbMessage & "Please check your Protecta processing" + vbCrLf
                    End If
                End If
            End If
            'check Insurances
            If Not ddRow.SecurityInsured = True And Not ddRow.NoInsuranceRequired = True Then
                checkDbMessage = checkDbMessage & "Please check your Insurances processing" + vbCrLf
            ElseIf ddRow.SecurityInsured = True And ddRow.NoInsuranceRequired = True Then
                checkDbMessage = checkDbMessage & "Please check your Insurances processing" + vbCrLf
            End If
            'check final
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If ddRow.InsuranceIdentified = True Then
                    If Not ddRow.ProtectaBook = True Then
                        checkDbMessage = checkDbMessage & "Please check that the Protecta books have been sent" + vbCrLf
                    End If
                End If
            End If

        End If
        'All scenarios
        'check documents
        If Not ddRow.Originals = True Then
            checkDbMessage = checkDbMessage & "Please check that all pages of original contract are clear and legible" + vbCrLf
        End If
        If Not ddRow.Signed = True Then
            checkDbMessage = checkDbMessage & "Please check that all pages are signed or initialled correctly where appropiate" + vbCrLf
        End If
        If Not ddRow.SpecialConditions = True Then
            checkDbMessage = checkDbMessage & "Please check all Special Conditions have been met" + vbCrLf
        End If
        'check disbursements
        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            Dim disbursTotal As Decimal = 0.0
            disbursTotal = CDec(ddRow.DisburseAmt1) + CDec(ddRow.DisburseAmt2) + CDec(ddRow.DisburseAmt3) + CDec(ddRow.DisburseAmt4) + CDec(ddRow.DisburseAmt5) + CDec(ddRow.DisburseAmt6)
            If Not disbursTotal = drawdownPaidOut Then
                checkDbMessage = checkDbMessage & "Please check your disbursements" + vbCrLf
            End If
        End If
        If Not String.IsNullOrEmpty(checkDbMessage) Then
            checkDbMessage = checkDbMessage & "Have you saved your Audit entries?" & vbCrLf
        End If

        Return checkDbMessage
    End Function

    ''' <summary>
    ''' Check Payout fields values
    ''' </summary>
    ''' <returns>Error Message</returns>
    ''' <remarks>Returns empty string if successful</remarks>
    Private Function CheckPayoutData() As String
        'check fields
        Dim checkFieldsMessage As String = String.Empty
        Try
            'check documents
            If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If PayAdviceDateSelected = False Then
                    If ckbxPayAdvice.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please select Payment advice faxed / emailed to Dealer Date" + vbCrLf
                    End If
                End If
            End If

            If Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If ckbxClosedInFP.CheckState = CheckState.Checked Then
                    If Not ckbxArchived.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please check that refinanced loans are closed and filed in the archives" + vbCrLf
                    End If
                End If
                Dim thisDdap As Boolean = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DDAPSaved")
                If thisDdap = True Then
                    If Not ckbxDDSafe.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please check that the DD / AP form is in the safe" + vbCrLf
                    End If
                End If
            Else 'ThisLoanReason = Split Loan Drawdown or FinanceFacility only

            End If

            'All scenarios
            If Not ckbxTraderLoansLoaded.CheckState = CheckState.Checked Then
                checkFieldsMessage = checkFieldsMessage + "Please check Loan figures in Audit are correct" + vbCrLf
            End If
            If Not ckbxVerification.CheckState = CheckState.Checked Then
                checkFieldsMessage = checkFieldsMessage + "Please check that the PPSR Verification Statement has been viewed and is correct" + vbCrLf
            End If
            'check payout month
            If Not cmbxPayoutMonth.Text.Length > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please check that the Payout Month is correct" + vbCrLf
            End If
            If Not cmbxPayoutYear.Text.Length > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please check that the Payout Year is correct" + vbCrLf
            End If
            'check paid
            If Not txtbxBatchNum.Text.Length > 0 And Not ckbxTotalCheque.CheckState = CheckState.Checked Then
                checkFieldsMessage = checkFieldsMessage + "Please check Batch number or that the Total was paid by cheque" + vbCrLf
            End If
            If ckbxTotalCheque.CheckState = CheckState.Checked Then
                If Not txtbxCheque1.TextLength > 0 And Not txtbxCheque2.TextLength > 0 And Not txtbxCheque3.TextLength > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter a Cheque number" + vbCrLf
                End If
            End If
            If Not CDec(lblTotalFinAmt.Text) > 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please check your payments" + vbCrLf
            ElseIf Not (TotalDrawdown + Refinance) = CDec(lblTotalFinAmt.Text) Then
                checkFieldsMessage = checkFieldsMessage + "Please check your payments" + vbCrLf
            End If
            If Not ckbxANZProcess.CheckState = CheckState.Checked Then
                checkFieldsMessage = checkFieldsMessage + "Please check the ANZ process or that all cheques have been issued" + vbCrLf
            End If
            If Not ckbxBatch.CheckState = CheckState.Checked Then
                checkFieldsMessage = checkFieldsMessage + "Please check that all cheques or Batch have been entered into the Cash Book" + vbCrLf
            End If
            If ANZProcessDateSelected = False Then
                checkFieldsMessage = checkFieldsMessage + "Please select ANZ processed / all cheques issued Date" + vbCrLf
            End If
            'Check References entered
            If CDec(txtbxRefinanceAmt1.Text) > 0 Then
                If Not txtbxRefinanceRef1.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Refinance amount" + vbCrLf
                End If
            End If
            If CDec(txtbxRefinanceAmt2.Text) > 0 Then
                If Not txtbxRefinanceRef2.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Refinance amount" + vbCrLf
                End If
            End If
            'txtbxPayAmt, txtbxPayeeRef, txtbxPayeeName
            If CDec(txtbxPayAmt1.Text) > 0 Then
                If Not txtbxPayeeRef1.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Payout amount" + vbCrLf
                End If
                If Not txtbxPayeeName1.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Payee Name for Payout amount" + vbCrLf
                End If
            End If
            If CDec(txtbxPayAmt2.Text) > 0 Then
                If Not txtbxPayeeRef2.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Payout amount" + vbCrLf
                End If
                If Not txtbxPayeeName2.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Payee Name for Payout amount" + vbCrLf
                End If
            End If
            If CDec(txtbxPayAmt3.Text) > 0 Then
                If Not txtbxPayeeRef3.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Payout amount" + vbCrLf
                End If
                If Not txtbxPayeeName3.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Payee Name for Payout amount" + vbCrLf
                End If
            End If
            If CDec(txtbxPayAmt4.Text) > 0 Then
                If Not txtbxPayeeRef4.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Payout amount" + vbCrLf
                End If
                If Not txtbxPayeeName4.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Payee Name for Payout amount" + vbCrLf
                End If
            End If
            If CDec(txtbxPayAmt5.Text) > 0 Then
                If Not txtbxPayeeRef5.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Payout amount" + vbCrLf
                End If
                If Not txtbxPayeeName5.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Payee Name for Payout amount" + vbCrLf
                End If
            End If
            If CDec(txtbxPayAmt6.Text) > 0 Then
                If Not txtbxPayeeRef6.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Payout amount" + vbCrLf
                End If
                If Not txtbxPayeeName6.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Payee Name for Payout amount" + vbCrLf
                End If
            End If
            'txtCommissionAmt
            If CDec(txtCommissionAmt.Text) > 0 Then
                If Not txtCommissionRef.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Dealer Commission" + vbCrLf
                End If
            End If
            If CDec(txtbxHoldbackAmt.Text) > 0 Then
                If Not txtbxHoldbackRef.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for HoldBack Interest" + vbCrLf
                End If
            End If
            'txtbxRetentionAmt
            If CDec(txtbxRetentionAmt.Text) > 0 Then
                If Not txtbxRetentionRef.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please add Reference for Retention" + vbCrLf
                End If
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        Return checkFieldsMessage

    End Function


    ''' <summary>
    ''' Check Audit fields values
    ''' </summary>
    ''' <returns>Error Message</returns>
    ''' <remarks>Returns empty string if successful</remarks>
    Private Function CheckAuditFields() As String
        'check fields
        Dim checkFieldsMessage As String = String.Empty
        Try
            If Not thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                'check final
                If Not ckbxFinPowerOpened.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check FinPower has been Opened" + vbCrLf
                End If
                If Not ckbxWelcomeLetter.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check the Welcome letter has been sent" + vbCrLf
                End If
            ElseIf thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Then
                'check final
                If Not ckbxFinPowerOpened.CheckState = CheckState.Checked And Not ckbxfinPowerWithdrawal.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check FinPower has been Opened or Withdrawal Completed" + vbCrLf
                End If
            ElseIf thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                If ckbxDDinPlace.CheckState = CheckState.Indeterminate Or ckbxDDinPlace.CheckState = CheckState.Unchecked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check DD / AP  is entered on sheet or DD is in place" + vbCrLf
                End If
                'check final
                If Not ckbxfinPowerWithdrawal.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check FinPower withdrawal has been completed" + vbCrLf
                End If
            ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                'check final
                If Not ckbxFinPowerOpened.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check FinPower Dealer Setup has been completed" + vbCrLf
                End If
                If Not ckbxWelcomeLetter.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check the new Dealer details have been passed to the Accountant" + vbCrLf
                End If
            End If

            If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                'check documents
                If ckbxApplication.CheckState = CheckState.Unchecked Or ckbxApplication.CheckState = CheckState.Indeterminate Then
                    checkFieldsMessage = checkFieldsMessage + "Please check that you have received the original application, completed and signed correctly" + vbCrLf
                End If
                If ckbxDocs.CheckState = CheckState.Unchecked Or ckbxDocs.CheckState = CheckState.Indeterminate Then
                    checkFieldsMessage = checkFieldsMessage + "Please check that all supporting documents are clear and legible" + vbCrLf
                End If

                'check processing DD/AP Form
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    If Not txtbxDDShtNum.Text.Length > 0 And Not ckbxDDinPlace.CheckState = CheckState.Checked And Not ckbxPayBook.CheckState =
                CheckState.Checked And Not ckbxClientSetup.CheckState = CheckState.Checked And Not ckbxWageDeduction.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage & "Please check DD / AP  is entered on sheet or DD is in place" & vbCrLf &
                        "or Payment Book given to Client or Wage deduction confirmed" & vbCrLf &
                        "or Client setup themselves and the Agreement Form has been filed" & vbCrLf
                    End If
                    If Not ckbxClientSetup.CheckState = CheckState.Checked Then
                        If Not ckbxBankAcc.CheckState = CheckState.Checked And Not ckbxPayBook.CheckState = CheckState.Checked And Not ckbxWageDeduction.CheckState = CheckState.Checked Then
                            checkFieldsMessage = checkFieldsMessage + "Please check Bank Acc No in finPower and / or Payment book given to client" + vbCrLf + "or Wage deduction have been checked" + vbCrLf
                        End If
                    End If
                    If Not ckbxPayMethod.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please check the Pay Method in finPower is correct." + vbCrLf
                    End If
                End If


                'check processing documents
                If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    If ckbxDDAP.CheckState = CheckState.Unchecked Or ckbxDDAP.CheckState = CheckState.Indeterminate Then
                        checkFieldsMessage = checkFieldsMessage + "Please check Evaluation sheet is scanned or saved" + vbCrLf
                    End If
                End If
                If ckbxID.CheckState = CheckState.Unchecked Or ckbxID.CheckState = CheckState.Indeterminate Then
                    checkFieldsMessage = checkFieldsMessage + "Please check IDs are scanned or saved" + vbCrLf
                End If
                If ckbxApp.CheckState = CheckState.Unchecked Or ckbxApp.CheckState = CheckState.Indeterminate Then
                    If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        checkFieldsMessage = checkFieldsMessage + "Please check the Dealer Agreement is scanned or saved" + vbCrLf
                    Else
                        checkFieldsMessage = checkFieldsMessage + "Please check the application is scanned or saved" + vbCrLf
                    End If
                End If
                If ckbxProofAdd.CheckState = CheckState.Unchecked Or ckbxProofAdd.CheckState = CheckState.Indeterminate Then
                    If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        checkFieldsMessage = checkFieldsMessage + "Please check the Deposit slip is scanned or saved" + vbCrLf
                    Else
                        checkFieldsMessage = checkFieldsMessage + "Please check the proof of address documents are scanned or saved" + vbCrLf
                    End If

                End If
                If ckbxProofInc.CheckState = CheckState.Unchecked Or ckbxProofInc.CheckState = CheckState.Indeterminate Then
                    checkFieldsMessage = checkFieldsMessage + "Please check the proof of income documents are scanned or saved" + vbCrLf
                End If

                'check processing FinPower
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    If ckbxClient.CheckState = CheckState.Unchecked Or ckbxClient.CheckState = CheckState.Indeterminate Then
                        checkFieldsMessage = checkFieldsMessage + "Please check FinPower Client fields are filled in and correct" + vbCrLf
                    End If
                    If ckbxLoan.CheckState = CheckState.Unchecked Or ckbxLoan.CheckState = CheckState.Indeterminate Then
                        checkFieldsMessage = checkFieldsMessage + "Please check FinPower Loan fields are filled in and correct" + vbCrLf
                    End If
                    If ckbxSecurity.CheckState = CheckState.Unchecked Or ckbxSecurity.CheckState = CheckState.Indeterminate Then
                        checkFieldsMessage = checkFieldsMessage + "Please check FinPower Security fields are filled in and correct" + vbCrLf
                    End If
                    If ckbxGraded.CheckState = CheckState.Unchecked Or ckbxGraded.CheckState = CheckState.Indeterminate Then
                        checkFieldsMessage = checkFieldsMessage + "Please check Client has been graded in FinPower" + vbCrLf
                    End If
                End If

                'check processing PPSR
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    If Not ckbxPPSRNew.CheckState = CheckState.Checked And Not ckbxPPSRAmended.CheckState = CheckState.Checked And Not ckbxPPSRCurrent.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please check PPSR registration" + vbCrLf
                    End If
                    If Not ckbxPPSRFS.CheckState = CheckState.Checked Or Not ckbxDebtor.CheckState = CheckState.Checked Or Not ckbxType.CheckState = CheckState.Checked Or Not ckbxDesc.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please check PPSR Financial Statement" + vbCrLf
                    End If
                End If

                'check Protecta
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    If Not ckbxInsuranceIdentified.CheckState = CheckState.Checked And Not ckbxNoPolicies.CheckState = CheckState.Checked And Not txtSpecialNotes.Text.Length > 0 Then
                        checkFieldsMessage = checkFieldsMessage + "Please check your Protecta processing" + vbCrLf
                    End If
                    If txtSpecialNotes.Text.Length > 0 Then
                        specialNotesAlert = True
                    Else
                        specialNotesAlert = False
                    End If
                End If

                'check Insurances
                If Not ckbxSecurityInsured.CheckState = CheckState.Checked And Not ckbxNoInsuranceRequired.CheckState = CheckState.Checked Then
                    checkFieldsMessage = checkFieldsMessage + "Please check your Insurances processing" + vbCrLf
                Else
                    If ckbxSecurityInsured.CheckState = CheckState.Checked And ckbxNoInsuranceRequired.CheckState = CheckState.Checked Then
                        checkFieldsMessage = checkFieldsMessage + "Please check your Insurances processing" + vbCrLf
                    End If
                End If

                'check final
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    If ckbxInsuranceIdentified.CheckState = CheckState.Checked Then
                        If Not ckbxProtectaBooks.CheckState = CheckState.Checked Then
                            checkFieldsMessage = checkFieldsMessage + "Please check that the Protecta books have been sent" + vbCrLf
                        End If
                    End If
                End If

            End If

            'All scenarios
            'check documents
            If ckbxOriginals.CheckState = CheckState.Unchecked Or ckbxOriginals.CheckState = CheckState.Indeterminate Then
                checkFieldsMessage = checkFieldsMessage + "Please check that all pages of original contract are clear and legible" + vbCrLf
            End If
            If ckbxSigned.CheckState = CheckState.Unchecked Or ckbxSigned.CheckState = CheckState.Indeterminate Then
                checkFieldsMessage = checkFieldsMessage + "Please check that all pages are signed or initialled correctly where appropiate" + vbCrLf
            End If

            If Not ckbxSpecialConditions.CheckState = CheckState.Checked Then
                checkFieldsMessage = checkFieldsMessage + "Please check all Special Conditions have been met" + vbCrLf
            End If

            'check FinPower mapping
            Dim clientNumTextBox As TextBox
            For Each cn As Control In drFinPower.Controls
                clientNumTextBox = TryCast(cn.Controls("ClientNumTextBox"), TextBox)
                If clientNumTextBox Is Nothing Or Not clientNumTextBox.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter finPower Client Number" + vbCrLf
                    clientNumTextBox.BackColor = Color.LavenderBlush
                Else
                    clientNumTextBox.BackColor = SystemColors.Window
                End If
            Next

            'check disbursements
            'drawdownPaidOut As Decimal 'Drawdown paidout (Audit tab)
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If Not CDec(lblDisburseAmtTotal.Text) = drawdownPaidOut Then
                    checkFieldsMessage = checkFieldsMessage + "Please check your disbursements" + vbCrLf
                End If
                'check figures checked
                If ckbxLoanFigures.CheckState = CheckState.Unchecked Or ckbxLoanFigures.CheckState = CheckState.Indeterminate Then
                    checkFieldsMessage = checkFieldsMessage + "Please check the Loan Breakdown figures" + vbCrLf
                End If
                If ckbxCommsFigures.CheckState = CheckState.Unchecked Or ckbxCommsFigures.CheckState = CheckState.Indeterminate Then
                    checkFieldsMessage = checkFieldsMessage + "Please check the Dealer Commission figures" + vbCrLf
                End If
            End If

            'check finPower Loan and Client folders exist
            Dim finPowerDrive As String = Switch.GetFinPowerSharedFolder & "\"
            'TOD0:get finPower Client and Loan numbers to test their folders exist
            If ffCustCount > 0 Then
                For Each ecRowView As DataRowView In enqCustBS
                    Dim ecRow = ecRowView.Row
                    If Not ecRow.ClientNum Is DBNull.Value And Not ecRow.ClientNum = String.Empty Then
                        'Dim thisClientTypeId As String = ecRow.ClientTypeId
                        Dim thisClientNum As String = ecRow.ClientNum
                        'finPower Client Folder path
                        Dim thisFinPowerClientPath As String = finPowerDrive & "Client_" & thisClientNum 'thisClientTypeId & thisClientNum
                        If Not Directory.Exists(thisFinPowerClientPath) Then
                            checkFieldsMessage = checkFieldsMessage + "FinPower Client Folder " + thisClientNum + " does not exist" + vbCrLf 'thisClientTypeId + thisClientNum + " does not exist" + vbCrLf
                        End If
                    Else
                        checkFieldsMessage = checkFieldsMessage + "A finPower Client Code is missing" + vbCrLf
                    End If
                Next

            End If
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                'get finPower_LoanNumber
                Dim thisFinPowerLoanNum As String = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum")
                'finPower Loan Folder path
                Dim thisFinPowerLoanNumPath = finPowerDrive & "Loan_" & thisFinPowerLoanNum
                'MsgBox("Path to Client = " & ThisFinPowerClientIdPath)
                'MsgBox("Path to Loan = " & ThisFinPowerLoanNumPath)
                'Determine whether the directory exists.           
                If Not Directory.Exists(thisFinPowerLoanNumPath) Then
                    checkFieldsMessage = checkFieldsMessage + "FinPower Loan Folder " + thisFinPowerLoanNum + " does not exist" + vbCrLf
                End If
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.Message, "Error in CheckAuditData", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Return checkFieldsMessage

    End Function

    ' ''' <summary>
    ' ''' Do stuff when certain tab selected
    ' ''' </summary>
    ' ''' <param name="sender">TabControl</param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Private Sub tbctrlAppDetails_Selected(sender As Object, e As TabControlEventArgs) Handles tbctrlAppDetails.Selected

    '    Dim tab As String = DirectCast(sender, TabControl).SelectedTab.Name
    '    If tab = "tabCustomers" Then
    '        If custCount > 0 Then
    '            Dim ecRowView As Data.DataRowView
    '            Dim ecRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
    '            ecRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
    '            If ecRowView.Row IsNot Nothing Then
    '                ecRow = ecRowView.Row
    '                'load customer table
    '                Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, ecRow.CustomerId)
    '                loadedCustomerId = ecRow.CustomerId 'Current customer to be displayed
    '                lblCustTypeValue.Text = MyEnums.GetDescription(DirectCast(ecRow.CustomerType, MyEnums.CustomerType))
    '                'set customer panels
    '                SetCustomerTypePanel(ecRow.CustomerType, loadedCustomerId, ecRow.Type)

    '                pnlCustomerType.Visible = True
    '                pnlCustomerType.Enabled = True
    '            End If

    '        End If

    '        'Set the selection
    '        For Each row As DataGridViewRow In dgvEnquiryCustomers.Rows
    '            If row.Cells(3).Value.ToString = loadedCustomerId Then
    '                EnquiryCustomersBindingSource.Position = row.Index
    '                Exit For
    '            End If
    '        Next
    '    End If

    'End Sub

    ' ''' <summary>
    ' ''' get list of documents in workfolder
    ' ''' </summary>
    ' ''' <param name="thisWorksheetDrive"></param>
    ' ''' <param name="ourEnquiryCode"></param>
    ' ''' <returns></returns>
    ' ''' <remarks>Call this before the file list is empty</remarks>
    'Function GetDocumentsList(ByVal thisWorksheetDrive As String, ByVal ourEnquiryCode As String, Optional withHeader As Boolean = True) As String
    '    Dim filePath As String = thisWorksheetDrive & "\" & ourEnquiryCode
    '    Dim folderExists As Boolean
    '    folderExists = My.Computer.FileSystem.DirectoryExists(filePath)
    '    Dim fileString As String = String.Empty
    '    If folderExists = True Then
    '        If withHeader = True Then
    '            fileString = vbCrLf & "*****  DOCUMENTS  ********************************************************" & vbCrLf
    '        End If
    '        Dim files As ReadOnlyCollection(Of String)
    '        files = My.Computer.FileSystem.GetFiles(filePath, FileIO.SearchOption.SearchAllSubDirectories, "*.*")

    '        For Each fileItem As String In files
    '            'Compare file path with fileItem and add diff to str
    '            Dim str As String = ""
    '            'Dim strExt As String = ""
    '            Dim finfo As New FileInfo(fileItem)
    '            Dim txt1(filePath.Split("\").Length) As String
    '            Dim txt2(fileItem.Split("\").Length) As String
    '            txt1 = filePath.Split("\")
    '            txt2 = fileItem.Split("\")
    '            Dim diff1 As String = ""
    '            For Each diff As String In txt2
    '                If Array.IndexOf(txt1, diff.ToString) = -1 Then 'Searches for the specified object and returns the index of its first occurrence in a one-dimensional array.
    '                    diff1 += "\" & diff.ToString
    '                End If
    '            Next
    '            If String.IsNullOrEmpty(diff1) Then
    '                str = finfo.Name & finfo.Extension
    '            Else
    '                str = diff1.Substring(1) 'remove leading"\"
    '            End If
    '            fileString = fileString & str & vbCrLf
    '        Next

    '        Return fileString

    '    Else
    '        Return String.Empty
    '    End If
    'End Function



    Private Sub ckbxSpecialNotesChecked_CheckedChanged(sender As Object, e As EventArgs) Handles ckbxSpecialNotesChecked.CheckedChanged
        specialNotesAlert = False
        lblSpecialNotesAlert.Visible = False
        ckbxSpecialNotesChecked.Visible = False
    End Sub

    Private Sub dgvEnquiryComments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEnquiryComments.DoubleClick

        Dim selectedRowCommentId As Integer = 0
        Dim selectedRowView As Data.DataRowView
        Dim selectedRow As EnquiryWorkSheetDataSet.EnquiryCommentRow
        selectedRowView = CType(EnquiryCommentBS.Current, System.Data.DataRowView)
        If selectedRowView.Row IsNot Nothing Then
            selectedRow = selectedRowView.Row
            selectedRowCommentId = selectedRow.Id

            'launch Add Comment Form
            Dim addCommentFrm As New AddCommentForm()
            If thisIsArchived = True Then
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.ReadOnly)
            ElseIf ApplicationAccessRights.CanEditComments Then
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.Edit)
            Else
                addCommentFrm.PassVariable(thisEnquiryId, selectedRowCommentId, MyEnums.Mode.ReadOnly)
            End If


            ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
            If addCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'Call the function you used to populate the data grid..
                Me.TableAdapterManager.EnquiryCommentTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisEnquiryId)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment added."
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Comment cancelled."
            End If
            'When Add Comment Form closed
            addCommentFrm.Dispose()

        End If

    End Sub


#Region "Handle MouseWheel"

    ''' <summary>
    ''' Capture mouse wheel events to ride them up to the host panel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ControlsInPanel_MouseWheel(sender As Object, e As MouseEventArgs) Handles txtbxHistoryComments.MouseWheel, txtbxAddressesComments.MouseWheel, txtDirectorsTrustees.MouseWheel, txtShareholdersComments.MouseWheel, txtbxCompanyTenancySatis.MouseWheel, txtBusCreditHistory.MouseWheel, txtBusCreditDefaults.MouseWheel, txtBusCreditPPSR.MouseWheel, txtBusOtherComments.MouseWheel, txtTradeRefComments.MouseWheel, txtAgeOfId.MouseWheel, txtbxResidenceSatis.MouseWheel, txtbxTenancyEnviron.MouseWheel, txtbxEstablishTenancy.MouseWheel, rtxtbxStabilityOther.MouseWheel, txtbxCreditHistory.MouseWheel, txtbxCreditDefaults.MouseWheel, txtbxCreditPPSR.MouseWheel
        Dim parentPanel As Control
        parentPanel = Util.GetParentPanel(sender)
        parentPanel.Focus()
    End Sub


#End Region


#Region "Information"

    Private Sub PbAuditInformationClick(sender As Object, e As EventArgs) Handles pbAuditInformation.Click
        'Dim msgText As String = "Auditor: " & vbNewLine & "Protecta section:" & vbNewLine & "Identify the insurance in the Insurance report in the OAC and select the checkbox  in the TrueTrack Audit screen." & vbNewLine & "If there is any issues with the insurance then add notes to the Special Notes box." & vbNewLine & "If the insurance is not identified in the Insurance report in the OAC then you must find out why and add appropriate notes to the Special Notes." & vbNewLine & "If the issue is related to the payment of insurance and the Insurance report in the OAC then you must add a note in the 20th Account Notes in the YFL (company) calendar. (See Patrick)" & vbNewLine & vbNewLine & "Accounts:" & vbNewLine & "If there are any Insurance Special Notes by the Auditor an Alert will be displayed in the TrueTrack Payout screen." & vbNewLine & "Check the notes and also, if appropriate, check that a note has been added to the 20th Account Notes in the YFL (company) calendar." & vbNewLine & vbNewLine & "Logic:" & vbNewLine & "If the insurance is not identified then Special Notes must be added or No insurance selected in-order to pass validation." & vbNewLine & "Special Notes can be added even if the insurance is identified." & vbNewLine & vbNewLine

        'MessageBox.Show(msgText, "Audit Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\AuditInformation.rtf", "Audit information", MyEnums.MsgBoxMode.FilePath)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()


    End Sub

    Private Sub PbAuditInformationMouseHover(sender As Object, e As EventArgs) Handles pbAuditInformation.MouseHover
        pbAuditInformation.Image = My.Resources.questionGsm
    End Sub

    Private Sub PbAuditInformationMouseLeave(sender As Object, e As EventArgs) Handles pbAuditInformation.MouseLeave
        pbAuditInformation.Image = My.Resources.questionBsm
    End Sub

    Private Sub pbBank_Click(sender As Object, e As EventArgs) Handles pbBank.Click
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\BankProcessing.rtf", "Bank Processing", MyEnums.MsgBoxMode.FilePath, 600)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()
    End Sub

    Private Sub pbBank_MouseHover(sender As Object, e As EventArgs) Handles pbBank.MouseHover
        pbBank.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbBank_MouseLeave(sender As Object, e As EventArgs) Handles pbBank.MouseLeave
        pbBank.Image = My.Resources.questionBsm
    End Sub

#End Region


    Private Sub TbctrlAppDetails_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbctrlAppDetails.SelectedIndexChanged
        'Dim indexOfSelectedTab As Integer = tbctrlAppDetails.SelectedIndex
        Dim selectedTab As System.Windows.Forms.TabPage = tbctrlAppDetails.SelectedTab
        If Not selectedTab Is Nothing Then
            'Customers ==========================================
            If selectedTab.Name = "tabCustomers" Then
                If custCount > 0 Then
                    Dim ecRowView As Data.DataRowView
                    Dim ecRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
                    ecRowView = CType(EnquiryCustomersBindingSource.Current, System.Data.DataRowView)
                    If ecRowView.Row IsNot Nothing Then
                        ecRow = ecRowView.Row
                        'load customer table
                        Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, ecRow.CustomerId)
                        loadedCustomerId = ecRow.CustomerId 'Current customer to be displayed
                        lblCustTypeValue.Text = MyEnums.GetDescription(DirectCast(ecRow.CustomerType, MyEnums.CustomerType))
                        'set customer panels
                        SetCustomerTypePanel(ecRow.CustomerType, loadedCustomerId, ecRow.Type)

                        pnlCustomerType.Visible = True
                        pnlCustomerType.Enabled = True
                    End If

                End If

                'Set the selection
                For Each row As DataGridViewRow In dgvEnquiryCustomers.Rows
                    If row.Cells(3).Value.ToString = loadedCustomerId Then
                        EnquiryCustomersBindingSource.Position = row.Index
                        Exit For
                    End If
                Next
            End If

            'Payout ==========================================
            If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Then

                If selectedTab.Name = "tabPayout" Then
                    If If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayeeName1")), String.Empty, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayeeName1")) = String.Empty And If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt1")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("PayAmt1")) = 0 Then
                        'Populate from disbursments
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName1")) Then
                            If EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName1").ToString.Length > 0 Then
                                txtbxPayeeName1.Text = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName1")
                                txtbxPayAmt1.Text = [String].Format("{0:n2}", CDec(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt1")))
                                txtbxPayeeRef1.Text = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum")), String.Empty, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum"))
                                txtbxPayeeName1.BackColor = Color.MistyRose
                                txtbxPayAmt1.BackColor = Color.MistyRose
                                txtbxPayeeRef1.BackColor = Color.MistyRose
                            End If
                        End If
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName2")) Then
                            If EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName2").ToString.Length > 0 Then
                                txtbxPayeeName2.Text = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName2")
                                txtbxPayAmt2.Text = [String].Format("{0:n2}", CDec(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt2")))
                                txtbxPayeeName2.BackColor = Color.MistyRose
                                txtbxPayAmt2.BackColor = Color.MistyRose
                            End If
                        End If
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName3")) Then
                            If EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName3").ToString.Length > 0 Then
                                txtbxPayeeName3.Text = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName3")
                                txtbxPayAmt3.Text = [String].Format("{0:n2}", CDec(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt3")))
                                txtbxPayeeName3.BackColor = Color.MistyRose
                                txtbxPayeeName3.BackColor = Color.MistyRose
                            End If
                        End If
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName4")) Then
                            If EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName4").ToString.Length > 0 Then
                                txtbxPayeeName4.Text = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName4")
                                txtbxPayAmt4.Text = [String].Format("{0:n2}", CDec(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt4")))
                                txtbxPayeeName4.BackColor = Color.MistyRose
                                txtbxPayeeName4.BackColor = Color.MistyRose
                            End If
                        End If
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName5")) Then
                            If EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName5").ToString.Length > 0 Then
                                txtbxPayeeName5.Text = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName5")
                                txtbxPayAmt5.Text = [String].Format("{0:n2}", CDec(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt5")))
                                txtbxPayeeName5.BackColor = Color.MistyRose
                                txtbxPayeeName5.BackColor = Color.MistyRose
                            End If
                        End If
                        If Not IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName6")) Then
                            If EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName6").ToString.Length > 0 Then
                                txtbxPayeeName6.Text = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseName6")
                                txtbxPayAmt6.Text = [String].Format("{0:n2}", CDec(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("DisburseAmt6")))
                                txtbxPayeeName6.BackColor = Color.MistyRose
                                txtbxPayeeName6.BackColor = Color.MistyRose
                            End If
                        End If
                        'Populate from Dealer Commission
                        'TotalDrawdown As Decimal 'Total drawdown (Audit tab)
                        'Refinance As Decimal ' refinanced amount (Audit tab)
                        'AmountFinanced As Decimal 'Amount financed (Audit tab)
                        'DealerCommission As Decimal 'Total Dealer Commission (Audit tab)
                        'drawdownPaidOut As Decimal 'Drawdown paidout (Audit tab)
                        'drawdownRetention As Decimal 'Drawdown retention (Audit tab)
                        'CommPaid2Dealer As Decimal 'Commission paid to the dealer (Audit tab)
                        'CommPaid2Retention As Decimal 'Commission paid to retention (Audit tab)
                        Dim productComm As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("ProductCommission")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("ProductCommission"))
                        Dim interestComm As Decimal = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("InterestCommission")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("InterestCommission"))
                        Dim commRetention As Integer = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("CommRetention")), 0, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("CommRetention"))
                        CommPaid2Retention = (productComm + interestComm) * commRetention / 100
                        CommPaid2Dealer = productComm + interestComm - CommPaid2Retention
                        Dim totalPaid2Retention As Decimal = drawdownRetention + CommPaid2Retention
                        If CommPaid2Dealer > 0 Then
                            txtCommissionAmt.Text = String.Format("{0:n2}", CommPaid2Dealer)
                            txtCommissionRef.Text = If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum")), String.Empty, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum"))
                            txtCommissionAmt.BackColor = Color.MistyRose
                            txtCommissionRef.BackColor = Color.MistyRose
                            txtbxRetentionRef.Text = "YFL Trust: " & If(IsDBNull(EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum")), String.Empty, EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum"))
                            txtbxRetentionAmt.Text = String.Format("{0:n2}", totalPaid2Retention)
                            txtbxRetentionRef.BackColor = Color.MistyRose
                            txtbxRetentionAmt.BackColor = Color.MistyRose
                        End If

                        'Refinance
                        If If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceRef1")), String.Empty, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceRef1")) = String.Empty And If(IsDBNull(EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceAmt1")), 0, EnquiryWorkSheetDataSet.Payout.Rows(PayoutBindingSource.Position()).Item("RefinanceAmt1")) = 0 Then
                            If Refinance > 0 Then
                                txtbxRefinanceRef1.BackColor = Color.MistyRose
                                txtbxRefinanceAmt1.Text = String.Format("{0:n2}", Refinance)
                                txtbxRefinanceAmt1.BackColor = Color.MistyRose
                            End If
                        End If

                        PayoutTotals()
                        RefinanceTotals()

                    End If

                End If
            End If
        End If
    End Sub


    ''' <summary>
    ''' Export files to finPower and Archives
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>For not FinanceFacility validates payout data then updates TrueTrack else validates Audit fields and updates TrueTrack Due-diligence. Tests construction of print string, exits if this fails. If allowed sets the OAC Status to Completed. If not FinanceFacility disables Payout fields. Creates Print String and saves file to TrueTrack folder. Export Client documents to finPower folder. Export Loan documents to finPower folder. Set Status and update Enquiry. If allowed Archive OAC application then archive TrueTrack enquiry. Delete TrueTrack directories</remarks>
    Private Async Sub btnExportFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportFiles.Click
        'Me.tbctrlAppDetails.SelectedTab = tabPayout
        Cursor.Current = Cursors.WaitCursor
        Dim custId As Integer
        Dim custIdNumber As String
        Dim custName As String
        Dim enqCustType As Integer
        'get file path
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim finPowerDrive As String = Switch.GetFinPowerSharedFolder & "\"
        'Printout
        Dim stringToSave As String
        Dim enquiryPrintOut As Util.StringOut
        Dim commentId As Integer
        Dim progressMsg As String = String.Empty
        'Enquiry
        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        Dim originalTime As DateTime
        Dim StatusTimeDiff As Integer
        Dim CommentStr As String


        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            '######################################################
            'Validate payout data
            '######################################################
            If Not String.IsNullOrEmpty(CheckPayoutData()) Then
                MsgBox(CheckPayoutData())
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
                Exit Sub
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Status: passed Payout validation"
                progressMsg = progressMsg & vbCrLf & "Successfully passed Payout validation"
                Application.DoEvents()
            End If
                '######################################################
                'Update  Payout fields, exit on error
                '######################################################
                Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                payoutRow = Me.EnquiryWorkSheetDataSet.Payout(0)
                payoutRow.BeginEdit()
                payoutRow.PayoutCompleted = Date.Now
            Try
                Me.Validate()
                Me.PayoutBindingSource.EndEdit()
                Me.PayoutTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Payout)
                progressMsg = progressMsg & vbCrLf & "Successfully updated Payout in database"
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("ERROR: Updating of Payout fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "ERROR: Updating of Payout fields caused an error."
                    Exit Sub
                End Try
           
        Else 'Is FinanceFacility (called from Audit tabpage)
            '######################################################
            'Validate Audit fields
            '######################################################
            If Not String.IsNullOrEmpty(CheckAuditFields()) Then
                MessageBox.Show(CheckAuditFields(), "Error checking Audit fields", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Status: Failed Audit field Validation. Ready."
                Exit Sub
            ElseIf Not Me.DueDiligenceBindingSource.Current Is Nothing Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Status: passed Audit fields validation"
                progressMsg = progressMsg & vbCrLf & "Successfully passed Audit fields validation"
                Application.DoEvents()
                'update Due Diligence
                Me.Validate()
                Me.DueDiligenceBindingSource.EndEdit()
                Me.DueDiligenceTableAdapter.Update(Me.EnquiryWorkSheetDataSet.DueDiligence)
                '######################################################
                'Validate database due-diligence values
                '######################################################
                If Not String.IsNullOrEmpty(CheckDueDiligenceData()) Then
                    MsgBox(CheckDueDiligenceData())
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Status: Failed Audit data Validation. Ready."
                    Exit Sub
                Else
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Status: passed Audit data validation"
                    progressMsg = progressMsg & vbCrLf & "Successfully passed Audit data validation"
                End If

            End If

        End If
        '######################################################
        'Test construction of print string first, exit on error
        '######################################################
        'Test folderpath exists first
        If Directory.Exists(folderPath) Then
            enquiryPrintOut = Util.GetPrintStringAll(thisEnquiryId)
            If enquiryPrintOut.Result Then
                stringToSave = enquiryPrintOut.StringOut
                progressMsg = progressMsg & vbCrLf & "Successfully tested printout creation"
            Else
                MessageBox.Show("Error creating Print String", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Else
            MsgBox("ERROR: The specified path is invalid." & vbCrLf & folderPath)
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: The specified path is invalid: " & folderPath
            Exit Sub
        End If
        '######################################################
        'set OAC Status
        '######################################################
        'Check is possible to Change Remote Status
        Dim systemCanChangeRemoteStatus As Boolean
        Dim thisAppsettings As New AppSettings
        systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
        If systemCanChangeRemoteStatus = True Then
            Dim retTTResult As New TrackResult
            Dim onlineApplicationService As New OnlineApplication
            Dim note As String = String.Empty
            'Change Status in OAC
            Dim actionType As Integer
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                actionType = OACWebService.ApplicationActionType.PayoutCompleted
            Else
                actionType = OACWebService.ApplicationActionType.AuditCompleted
            End If
            retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, note)
            If retTTResult.Status = False Then 'error condition
                commentId = Util.SetComment(thisEnquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                MessageBox.Show(retTTResult.ErrorMessage, retTTResult.StatusMessage, MessageBoxButtons.OK, MessageBoxIcon.Error)
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "set OAC Status was not successful."
                progressMsg = progressMsg & vbCrLf & "Set OAC Status was not successful."
            Else
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "set OAC Status was successful."
                progressMsg = progressMsg & vbCrLf & "Set OAC Status was successful."
            End If
            Application.DoEvents()
        End If

        'disable payout controls
        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            gpbxProcessPayout.Enabled = False
            gpbxPaid.Enabled = False
            gpbxRefinAccounts.Enabled = False
            gpbxFinal.Enabled = False
            btnPayoutUpdate.Enabled = False
        End If

        '######################################################
        'Created print string successful, now write string to folder
        '######################################################
        Dim fileName As String
        fileName = "TrueTrackWorkSheet-" & thisEnquiryCode & ".txt"
        'write to file
        Try
            Dim sb As New StringBuilder()
            sb.AppendLine(stringToSave)
            Using outfile As New StreamWriter(folderPath & fileName, False) 'overwrite file if exits
                outfile.Write(sb.ToString())
                outfile.Flush()
                outfile.Close()
            End Using
            'set file permission to read only
            File.SetAttributes(folderPath & fileName, FileAttributes.ReadOnly)
            progressMsg = progressMsg & vbCrLf & "Successfully saved TrueTrackWorkSheet-" & thisEnquiryCode & ".txt"
        Catch ex As UnauthorizedAccessException
            MsgBox("ERROR: Saving file caused an error. Access is denied." & vbCrLf & ex.Message)
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. Access is denied."
            Exit Sub
        Catch ex As DirectoryNotFoundException
            MsgBox("ERROR: Saving file caused an error. The specified path is invalid." & vbCrLf & ex.Message)
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. The specified path is invalid."
            Exit Sub
        Catch ex As SecurityException
            MsgBox("ERROR: Saving file caused an error. The caller does not have the required permission." & vbCrLf & ex.Message)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error. The caller does not have the required permission."
            Exit Sub
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR: Saving file caused an error." & vbCrLf & ex.Message)
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: Saving file caused an error."
            Exit Sub
        End Try
        'Validatation of Payout fields was successful
        'Set OAC Status may be successful, failure does not stop this procedure
        'Saving Enquiry Summary file was successful
        '######################################################
        'Export Client documents
        '######################################################
        'Search worksheet folder and Any file starting with ID move to the finPower Client Folder
        If Not Me.EnquiryCustomersBindingSource.Current Is Nothing Then
            custCount = EnquiryCustomersBindingSource.Count
            Dim mainClientNum As String = String.Empty
            If custCount > 0 Then
                For Each row As DataRow In EnquiryWorkSheetDataSet.EnquiryCustomers.Rows
                    custId = row.Item("CustomerId")
                    custIdNumber = If(IsDBNull(row.Item("Id_number")), "", row.Item("Id_number"))
                    custName = If(IsDBNull(row.Item("Name")), "", row.Item("Name"))
                    enqCustType = If(IsDBNull(row.Item("Type")), "", row.Item("Type"))
                    'load customer
                    If custIdNumber.Length > 0 Then
                        Me.CustomerTableAdapter.FillByCustomerId(EnquiryWorkSheetDataSet.Customer, custId)
                        Dim custRow As EnquiryWorkSheetDataSet.CustomerRow
                        Dim thisClientNum As String = String.Empty
                        custRow = Me.EnquiryWorkSheetDataSet.Customer(0)
                        'In a FinanceFacility enquiry the guarantor will not have a ClientNum, utilise the EnquiryCustomerType
                        If enqCustType = MyEnums.CustomerEnquiryType.Main Then
                            mainClientNum = custRow.ClientNum
                        End If
                        If thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                            If Not String.IsNullOrEmpty(mainClientNum) Then
                                thisClientNum = mainClientNum
                            Else
                                Throw New ArgumentException("btnExportFiles: mainClientNum is NullOrEmpty")
                                Exit Sub
                            End If
                        Else
                            thisClientNum = custRow.ClientNum
                        End If

                        Dim thisFinPowerClientPath As String = finPowerDrive & "Client_" & thisClientNum 'thisClientTypeId & thisClientNum
                        Try
                            If Directory.Exists(thisFinPowerClientPath) Then
                                If Directory.Exists(folderPath) Then
                                    '################################
                                    'Revise file processing 24/02/2017
                                    '################################
                                    ProcessDirectory(folderPath, thisFinPowerClientPath, folderPath, MyEnums.TransferFileType.Client, thisEnquiryType, custIdNumber)
                                    progressMsg = progressMsg & vbCrLf & "Moving Customer Documents to finPower Folder completed"
                                Else
                                    progressMsg = progressMsg & vbCrLf & "Path to TrueTrack Folder does not exist. Export aborted!"
                                    MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
                                    'update Status Strip
                                    ToolStripStatusLabel1.ForeColor = Color.Red
                                    ToolStripStatusLabel1.Text = "Path to TrueTrack Folder does not exist. Export aborted!"
                                    Exit Sub
                                End If
                            Else
                                progressMsg = progressMsg & vbCrLf & "Path to finPower Client Folder does not exist. Export aborted!"
                                MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
                                'update Status Strip
                                ToolStripStatusLabel1.ForeColor = Color.Red
                                ToolStripStatusLabel1.Text = "Path to finPower Client Folder does not exist. Export aborted!"
                                Exit Sub
                            End If

                        Catch ex As System.IO.IOException
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            progressMsg = progressMsg & vbCrLf & "Moving Customer Documents to finPower Folder generated an IOException Message: " & vbCrLf & ex.Message
                            MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Customer document processing error. Export aborted!"
                            Exit Sub
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            progressMsg = progressMsg & vbCrLf & "Moving Customer Documents to finPower Folder generated an Exception Message: " & vbCrLf & ex.Message
                            MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Red
                            ToolStripStatusLabel1.Text = "Customer document processing error. Export aborted!"
                            Exit Sub
                        End Try
                    Else
                        progressMsg = progressMsg & vbCrLf & "Customer " & custName & " has no Id number" & vbCrLf & "Export aborted!"
                        MessageBox.Show(progressMsg, "Customer document processing error", MessageBoxButtons.OK)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Customer has no Id number. Export aborted!"
                        Exit Sub
                    End If

                Next
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No customers!"
            End If
        End If
        '######################################################
        'Export Loan documents
        '######################################################
        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            'get finPower_LoanNumber
            Dim thisFinPowerLoanNum As String = EnquiryWorkSheetDataSet.DueDiligence.Rows(DueDiligenceBindingSource.Position()).Item("LoanNum")
            'finPower Loan Folder path
            Dim thisFinPowerLoanNumPath = finPowerDrive & "Loan_" & thisFinPowerLoanNum
            'MsgBox("Path to Loan = " & ThisFinPowerLoanNumPath)  
            'Loan documents move to finPower Loan folder
            Try
                If Directory.Exists(thisFinPowerLoanNumPath) Then
                    If Directory.Exists(folderPath) Then
                        '################################
                        'Revise file processing 24/02/2017
                        '################################
                        ProcessDirectory(folderPath, thisFinPowerLoanNumPath, folderPath, MyEnums.TransferFileType.Loan, thisEnquiryType)
                        progressMsg = progressMsg & vbCrLf & "Moving Loan Documents to finPower Folder completed"
                    Else
                        progressMsg = progressMsg & vbCrLf & "Path to TrueTrack Folder does not exist. Export aborted!"
                        MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Path to TrueTrack Folder does not exist. Export aborted!"
                        Exit Sub
                    End If
                Else
                    progressMsg = progressMsg & vbCrLf & "Path to finPower Loan Folder does not exist. Export aborted!"
                    MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "Path to finPower Loan Folder does not exist. Export aborted!"
                    Exit Sub
                End If
            Catch ex As System.IO.IOException
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                progressMsg = progressMsg & vbCrLf & "Moving Loan Documents to finPower Folder generated an IOException Message: " & vbCrLf & ex.Message
                MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Moving Loan Documents to finPower Folder generated an IOException Message!"
                Exit Sub
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                progressMsg = progressMsg & vbCrLf & "Moving Loan Documents to finPower Folder generated an Exception Message: " & vbCrLf & ex.Message
                MessageBox.Show(progressMsg, "Loan document processing error", MessageBoxButtons.OK)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Moving Loan Documents to finPower Folder generated an Exception Message!"
                Exit Sub
            End Try
        End If
        '######################################################
        'Update TrueTrack Enquiry and set status
        '######################################################
        enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
        enquiryRow.BeginEdit()
        'update Current Status
        enquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED
        enquiryRow.CurrentStatusSetDate = DateTime.Now
        'Set Original Enquiry Time
        originalTime = enquiryRow.DateTime
        'calculate time difference for 30min status
        StatusTimeDiff = DateDiff(DateInterval.Minute, originalTime, Now)
        If StatusTimeDiff < 30 Then
            enquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_COMPLETED
        End If
        'change Active status
        enquiryRow.ActiveStatus = False
        'update Enquiry
        Try
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            progressMsg = progressMsg & vbCrLf & "Update and and set status of Enquiry was successful."
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            'MsgBox("ERROR: Updating of Enquiry fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString)
            progressMsg = progressMsg & vbCrLf & "ERROR: Updating of Enquiry fields caused an error." & vbCrLf & ex.Message & vbCrLf & ex.TargetSite.ToString
            MessageBox.Show(progressMsg, "Update of Enquiry", MessageBoxButtons.OK)
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: Updating of Enquiry fields caused an error."
            Exit Sub
        End Try
        'create log note
        CommentStr = "Status change to " & Constants.CONST_CURRENT_STATUS_COMPLETED
        commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

        progressMsg = progressMsg & vbCrLf & "Export Completed Successfully"
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Export Completed Successfully"
        Application.DoEvents()
        'Disable Export button
        btnExportFiles.Visible = True
        btnExportFiles.Enabled = False
        btnAddLoanProgress.Enabled = False
        EnquiryExported = True
        '######################################################
        'Now archive - call archive function
        '######################################################
        Dim result As New Util.StringOut
        result = Await Util.NewArchiveEnquiry(thisEnquiryId)
        If result.Result = True Then
            If result.StringOut.Length > 0 Then
                progressMsg = progressMsg & vbCrLf & result.StringOut
            Else
                progressMsg = progressMsg & vbCrLf & "Archiving was successful"
            End If
        Else
            progressMsg = progressMsg & vbCrLf & result.StringOut
            MessageBox.Show(progressMsg, "Error Archiving", MessageBoxButtons.OK)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: Archiving caused an error."
            Exit Sub
        End If
        '######################################################
        'delete directories
        '######################################################
        Try
            If Directory.Exists(folderPath) Then
                DeleteDirectories(folderPath)
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            progressMsg = progressMsg & vbCrLf & "Error deleting directories." & vbCrLf & ex.Message
            MessageBox.Show(progressMsg, "Error deleting directories", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        FormRefresh()

        Cursor.Current = Cursors.Default

    End Sub




   
End Class





