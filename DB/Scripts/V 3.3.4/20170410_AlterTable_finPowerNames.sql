--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
--Alter the finPowerNames table, add a field to record the LegalName values.
--written by Christopher 10/04/2017
PRINT 'Starting database script'
GO
PRINT'Alter finPowerNames table, add a field to record the LegalName values'
GO
PRINT '**************************'
GO

--add LegalName column to Enquiry table
if NOT exists(select * from sys.columns 
            where Name = N'LegalName' and Object_ID = Object_ID(N'dbo.finPowerNames'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add LegalName field to finPowerNames table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE finPowerNames
		ADD LegalName varchar(60) NULL;
	END	
END
ELSE
BEGIN
	PRINT 'LegalName field already exists in finPowerNames table'
END
GO
PRINT '**************************'
GO
PRINT 'DB script completed'
GO
