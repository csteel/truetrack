/****** Script for SelectTopNRows command from SSMS  ******/
use TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO

SELECT Count(StatusHistory.Id)
  FROM [dbo].[StatusHistory]
  where status = 'Saved'


UPDATE [dbo].[StatusHistory] SET
Status = 'Completed'
WHERE Status = 'Saved' 


SELECT Count(StatusHistory.Id)
  FROM [dbo].[StatusHistory]
  WHERE status = 'Completed'
