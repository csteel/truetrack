-- 01/09/2017 by Christopher
--Add new field to Enquiry table, Add DocsReceived (bit, null)
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--Enquiry table
--Add the IsArchived column to Enquiry table
IF Not exists(select * from sys.columns 
            where Name = N'DocsReceived' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD DocsReceived bit Null;
	END
	BEGIN
		PRINT 'Added DocsReceived field to Enquiry table'
	END	
END
ELSE
BEGIN
	PRINT 'DocsReceived field already exists in Enquiry table'
END
GO

