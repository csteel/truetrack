--01/09/2017: Christopher: Drop SP UpdateLoanComments
USE TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO

IF OBJECT_ID('UpdateLoanComments', 'P') IS NOT NULL
BEGIN
BEGIN
	DROP PROCEDURE UpdateLoanComments; 
	END
	BEGIN
		PRINT'SP UpdateLoanComments dropped'
	END
END
ELSE
BEGIN
	Print 'SP UpdateLoanComments does not exist'
END
GO

