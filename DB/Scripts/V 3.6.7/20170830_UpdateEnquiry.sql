use TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO
--UPDATE CurrentStatus
SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  where CurrentStatus = 'Saved'
GO

UPDATE [dbo].[Enquiry] SET
CurrentStatus = 'Completed'
WHERE CurrentStatus = 'Saved' 
GO

SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  WHERE CurrentStatus = 'Completed'
  GO

--UPDATE Status30mins
SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  where Status30mins = 'Saved'
GO

UPDATE [dbo].[Enquiry] SET
Status30mins = 'Completed'
WHERE Status30mins = 'Saved' 
GO

SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  WHERE Status30mins = 'Completed'
  GO
