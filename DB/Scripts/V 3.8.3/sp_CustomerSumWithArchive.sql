-- =============================================
-- Return Total Customers
--20/11/2017: Christopher: Modified to use union to get Archived Enquiries
-- =============================================
--Use TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO

-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'CustomerSum' 
)
   DROP PROCEDURE dbo.CustomerSum
GO

CREATE PROCEDURE dbo.CustomerSum
	@startDate DATE,
	@endDate DATE		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	
	SELECT Count(*) As NumberOfCust
	FROM
	((SELECT C.CustomerId
	FROM  Customer C INNER JOIN
    CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
    Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
		Payout P ON E.EnquiryId = P.EnquiryId 
  	WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
	)
	UNION
	(SELECT C.CustomerId
	FROM  Customer C INNER JOIN
    CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
    ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId 
  	WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
	)) AS SubData

			
END

--EXECUTE dbo.CustomerSum '2016-04-01','2017-10-01'
GO