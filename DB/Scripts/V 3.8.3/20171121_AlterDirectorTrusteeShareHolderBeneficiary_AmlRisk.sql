--Add AMLRisk smallint field in DirectorTrusteeShareHolderBeneficiary table
--21/11/2017: Christopher
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--DirectorTrusteeShareHolderBeneficiary table
--Add the AMLRisk column to DirectorTrusteeShareHolderBeneficiary table
IF Not exists(select * from sys.columns 
            where Name = N'AMLRisk' and Object_ID = Object_ID(N'DirectorTrusteeShareHolderBeneficiary'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE DirectorTrusteeShareHolderBeneficiary
		ADD AMLRisk smallint NULL DEFAULT 0;
	END
	BEGIN
		UPDATE DirectorTrusteeShareHolderBeneficiary SET
		AMLRisk = 0
		WHERE AMLRisk IS NULL;
	END
	BEGIN
		PRINT 'Added AMLRisk field to DirectorTrusteeShareHolderBeneficiary table'
	END	
END
ELSE
BEGIN
	BEGIN
		UPDATE DirectorTrusteeShareHolderBeneficiary SET
		AMLRisk = 0
		WHERE AMLRisk IS NULL;
	END
	BEGIN
		PRINT 'AMLRisk field already exists in DirectorTrusteeShareHolderBeneficiary table'
	END
END
GO

