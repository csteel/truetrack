--USE [TestEnquiryWorkSheet]
USE EnquiryWorkSheet
GO

/****** Object:  Table [dbo].[Enum_CustAcceptanceMethod]    Script Date: 12/10/2017 5:29:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Enum_CustAcceptanceMethod](
	[Enum] [INT] NOT NULL,
	[Name] [VARCHAR](50) NULL,
	[Description] [VARCHAR](50) NULL,
 CONSTRAINT [PK_Enum_CustAcceptanceMethod] PRIMARY KEY CLUSTERED 
(
	[Enum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO Enum_CustAcceptanceMethod (Enum,Name,Description) VALUES (0,'FaceToFace','Face to face');
GO
INSERT INTO Enum_CustAcceptanceMethod (Enum,Name,Description) VALUES (1,'NonFaceToFace','Non face to face');
GO
INSERT INTO Enum_CustAcceptanceMethod (Enum,Name,Description) VALUES (2,'DomesticIntermeds','Domestic intermediaries');
GO
INSERT INTO Enum_CustAcceptanceMethod (Enum,Name,Description) VALUES (3,'OverseasIntermeds','Overseas intermediaries');
GO

ALTER TABLE [dbo].[Enum_CustomerType]  WITH NOCHECK ADD  CONSTRAINT [chk_read_only_Enum_CustAcceptanceMethod] CHECK  (((1)=(0)))
GO

ALTER TABLE [dbo].[Enum_CustomerType] CHECK CONSTRAINT [chk_read_only_Enum_CustAcceptanceMethod]
GO


--Public Enum CustAcceptanceMethod
--        <Description("Face to face")> FaceToFace = 0
--        <Description("Non face to face")> NonFaceToFace
--        <Description("Domestic intermediaries")> DomesticIntermeds
--        <Description("Overseas intermediaries")> OverseasIntermeds
-- 	End Enum
