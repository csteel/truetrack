-- =============================================
-- Return Customers by Acceptance Method
-- 21/11/2017: Christopher: Modified to include Archived
-- 29/11/2017: Christopher: Added Archived field into query
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'AcceptanceMethodCustomer' 
)
   DROP PROCEDURE dbo.AcceptanceMethodCustomer
GO

CREATE PROCEDURE dbo.AcceptanceMethodCustomer
	@startDate DATE,
	@endDate DATE,
	@acceptanceMethodEnum int = null, 
	@ClassificationEnum int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @ClassificationEnum IS NULL 
	BEGIN
		--SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId
		--FROM  Customer C INNER JOIN
  --      CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
  --      Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
		--	Payout P ON E.EnquiryId = P.EnquiryId INNER JOIN
  --      Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
		--	Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
		--	Enum_Classification Cl ON ET.Classification = Cl.Enum 
		--WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		--AND CAM.Enum = @acceptanceMethodEnum
		----AND Cl.Enum = @ClassificationEnum
		--ORDER BY c.CustomerId

		SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
		FROM
		((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
		FROM  Customer C INNER JOIN
        CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
        Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = P.EnquiryId INNER JOIN
        Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
			Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
			Enum_Classification Cl ON ET.Classification = Cl.Enum 
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		AND CAM.Enum = @acceptanceMethodEnum)
		UNION
		(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
			Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum INNER JOIN
			Enum_Classification Cl ON ET.Classification = Cl.Enum 
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND CAM.Enum = @acceptanceMethodEnum)) AS SubData
		ORDER BY SubData.CustomerId
	END
	ELSE
	BEGIN
		SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
		FROM
		((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
		FROM  Customer C INNER JOIN
        CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
        Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = P.EnquiryId INNER JOIN
        Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
			Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
			Enum_Classification Cl ON ET.Classification = Cl.Enum 
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		AND CAM.Enum = @acceptanceMethodEnum
		AND Cl.Enum = @ClassificationEnum)
		UNION
		(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
			Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum INNER JOIN
			Enum_Classification Cl ON ET.Classification = Cl.Enum 
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND CAM.Enum = @acceptanceMethodEnum
		AND Cl.Enum = @ClassificationEnum)) AS SubData
		ORDER BY SubData.CustomerId

	END
	
END

-- =============================================
-- to execute the stored procedure
-- =============================================
--Enum|Name|Description
--0|FaceToFace|Face to face
--1|NonFaceToFace|Non face to face
--2|DomesticIntermeds|Domestic intermediaries
--3|OverseasIntermeds|Overseas intermediaries

--Enum|Name|Description
--0|BranchPersonalLoansSecured|Branch Personal Loans Secured
--1|BranchPersonalLoansUnSecured|Branch Personal Loans Un-secured
--2|BranchBusinessLoans|Branch Business Loans
--3|ConsumerGoodsLoans|Consumer Goods Loans
--4|MotorVehicleLoans|Motor Vehicle Loans
--5|FinancialFacility|Financial Facility

--EXECUTE dbo.AcceptanceMethodCustomer '2017-04-01','2017-10-01', 0 , NULL
GO