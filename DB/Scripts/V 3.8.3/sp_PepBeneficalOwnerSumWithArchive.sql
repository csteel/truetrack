-- =============================================
-- Return Polically Exposed Benefical Owners
--20/11/2017: Christopher: Modified to use union to get Archived Enquiries
-- =============================================
--Use TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO

-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'PepBeneficalOwnerSum' 
)
   DROP PROCEDURE dbo.PepBeneficalOwnerSum
GO

CREATE PROCEDURE dbo.PepBeneficalOwnerSum
	@startDate DATE,
	@endDate DATE		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	SELECT Count(*) As NumberOfPEP
	FROM
		((SELECT DTSB.Id
		FROM  Customer C INNER JOIN
			CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
			Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = P.EnquiryId INNER JOIN
			DirectorTrusteeShareHolderBeneficiary DTSB ON c.CustomerId = DTSB.CustomerId
  			WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
			AND DTSB.IsaPEP = 1)
		UNION
		(SELECT DTSB.Id
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			DirectorTrusteeShareHolderBeneficiary DTSB ON c.CustomerId = DTSB.CustomerId
  			WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
			AND DTSB.IsaPEP = 1)) AS SubData
		
END

--EXECUTE dbo.PepBeneficalOwnerSum '2017-04-01','2017-10-01'
GO