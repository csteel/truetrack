-- =============================================
-- return Customer Acceptance Method Summary by CAM type
-- 20/11/2017: Christopher: Modify for Union with Archive tables
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'AcceptanceMethodSummary' 
)
   DROP PROCEDURE dbo.AcceptanceMethodSummary
GO

CREATE PROCEDURE dbo.AcceptanceMethodSummary
	@startDate date,
	@endDate date 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--SELECT CAM.Description, COUNT(C.CustomerId) AS Total, CAM.Enum
	--	FROM  Customer C INNER JOIN
	--		CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
	--		Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
	--		Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
	--		Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum
	--	WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
	--	GROUP BY CAM.[Description], CAM.Enum
	
	SELECT SubData.[Description], COUNT(SubData.CustomerId) AS Total, SubData.Enum
	FROM
	((SELECT CAM.[Description], C.CustomerId, CAM.Enum
		FROM  Customer C INNER JOIN
			CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
			Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
			Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		)
		UNION
		(SELECT CAM.[Description], C.CustomerId, CAM.Enum
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		)) AS SubData
		GROUP BY SubData.[Description], SubData.Enum

END

-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.AcceptanceMethodSummary '2017-04-01','2017-10-01'
GO