-- =============================================
-- Director/Shareholder AMK Risk Customer 23/11/2017
-- 29/11/2017: Christopher: Added Archived field into query
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'DirectorShareholderAMLRiskCustomer' 
)
   DROP PROCEDURE dbo.DirectorShareholderAMLRiskCustomer
GO

CREATE PROCEDURE dbo.DirectorShareholderAMLRiskCustomer
	@startDate date,
	@endDate date,
	@amlRiskEnum int,
	@dtsbTypeEnum int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
	FROM
	((SELECT DISTINCT  E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
		FROM  Customer C INNER JOIN
			CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
			Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
			Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
			DirectorTrusteeShareHolderBeneficiary DTSB ON C.CustomerId = DTSB.CustomerId INNER JOIN
			Enum_AMLRisk AML ON DTSB.AMLRisk = AML.Enum INNER JOIN
			Enum_DTSB_Type EDT ON DTSB.[Type] = EDT.Enum
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate 
		AND DTSB.AMLRisk = @amlRiskEnum 
		AND DTSB.[Type] = @dtsbTypeEnum
		)
		UNION
		(SELECT DISTINCT  AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum INNER JOIN
			DirectorTrusteeShareHolderBeneficiary DTSB ON C.CustomerId = DTSB.CustomerId INNER JOIN
			Enum_AMLRisk AML ON C.AMLRisk = AML.Enum INNER JOIN
			Enum_DTSB_Type EDT ON DTSB.[Type] = EDT.Enum
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND DTSB.AMLRisk = @amlRiskEnum 
		AND DTSB.[Type] = @dtsbTypeEnum
		)) AS SubData

		ORDER BY SubData.CustomerId
END

--Enum_AMLRisk|Name|Description
--0|SelectOne|Select one
--1|Low|Low
--2|Medium|Medium
--3|High|High

--Enum_DTSB_Type|Name|Description
--0|Director|Director
--1|Trustee|Trustee
--2|Shareholder|Shareholder
--3|Beneficiary|Beneficiary

-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.DirectorShareholderAMLRiskCustomer '2016-07-01','2017-11-30', 0, 0
GO