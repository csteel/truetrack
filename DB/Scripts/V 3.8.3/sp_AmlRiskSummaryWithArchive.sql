--USE [TestEnquiryWorkSheet]
USE EnquiryWorkSheet
GO
--=============================================================
--StoredProcedure [dbo].[AmlRiskSummary]
--=============================================================
--20/11/2017: Christopher: Modified to use union to get Archived Enquiries
--=============================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AmlRiskSummary]
	@startDate date,
	@endDate date 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SubData.Description, COUNT(SubData.CustomerId) AS Total, SubData.Enum
	FROM
	((SELECT AML.Description, C.CustomerId, AML.Enum
		FROM  Customer C INNER JOIN
			CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
			Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
			Enum_AMLRisk AML ON C.AMLRisk = AML.Enum
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate)
	UNION
	(SELECT AML.Description, C.CustomerId, AML.Enum
	FROM  Customer C INNER JOIN
		CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
		ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			Enum_AMLRisk AML ON C.AMLRisk = AML.Enum
			WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate)) AS SubData
			
		GROUP BY SubData.[Description], SubData.Enum
	
END

-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.AmlRiskSummary '2017-04-01','2017-10-01'
GO