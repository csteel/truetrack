-- =============================================
-- Return Customers by IsaPEP
--20/11/2017: Christopher: Modified to use union to get Archived Enquiries
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'CustTypeCustomer' 
)
   DROP PROCEDURE dbo.CustTypeCustomer
GO

CREATE PROCEDURE dbo.CustTypeCustomer
	@startDate DATE,
	@endDate DATE,
	@custTypeEnum INT
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
	FROM
	((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
	FROM  Customer C INNER JOIN
    CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
    Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
	Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
	Payout P ON E.EnquiryId = P.EnquiryId 
   	WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
	AND C.CustomerType = @custTypeEnum)
	UNION
	(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
	FROM  Customer C INNER JOIN
    CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
    ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
	Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum 
   	WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
	AND C.CustomerType = @custTypeEnum)) AS SubData
	ORDER BY SubData.CustomerId
		
END

--EXECUTE dbo.CustTypeCustomer '2017-04-01','2017-10-01',0
GO