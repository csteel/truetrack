-- =============================================
-- Return Customers by Loan Classification
--20/11/2017: Christopher: Modified to use union to get Archived Enquiries
-- 29/11/2017: Christopher: Added Archived field into query
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'ClassificationCustomer' 
)
   DROP PROCEDURE dbo.ClassificationCustomer  
GO

CREATE PROCEDURE dbo.ClassificationCustomer
	@startDate DATE,
	@endDate DATE,
	@classificationEnum INT
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
	FROM
	((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, E.Archived
	FROM  Customer C INNER JOIN
    CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
    Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
	Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
	Payout P ON E.EnquiryId = P.EnquiryId INNER JOIN
	Enum_Classification EC ON ET.Classification = EC.Enum
   	WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
	AND EC.Enum = @classificationEnum)
	--ORDER BY c.CustomerId
	UNION
	(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, AE.Archived
	FROM  Customer C INNER JOIN
    CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
    ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
	Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum INNER JOIN
	Enum_Classification EC ON ET.Classification = EC.Enum
   	WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
	AND EC.Enum = @classificationEnum)) AS SubData
		
END

--EXECUTE dbo.ClassificationCustomer '2017-04-01','2017-10-01',0
GO