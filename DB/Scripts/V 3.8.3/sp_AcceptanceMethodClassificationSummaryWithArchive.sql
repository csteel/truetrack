-- =============================================
-- AcceptanceMethodClassificationSummary 01/11/2017
-- 20/11/2017: Christopher: Modify for Union with Archive tables
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'AcceptanceMethodClassificationSummary' 
)
   DROP PROCEDURE dbo.AcceptanceMethodClassificationSummary
GO

CREATE PROCEDURE dbo.AcceptanceMethodClassificationSummary
	@startDate date,
	@endDate date 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SubData.CustAcceptanceMethod , SubData.Classification,  Count(SubData.CustomerId) AS Total
	, Sum(SubData.Drawdown) AS Drawdown, SubData.CAM_Enum, SubData.Cl_Enum
	FROM
	((SELECT CAM.[Description] As CustAcceptanceMethod , Cl.[Description] As Classification, C.CustomerId
	, (ISNULL(D.CashPrice_Advance,0) + ISNULL(D.OtherAdvance,0) - ISNULL(D.Deposit,0) + ISNULL(D.Brokerage,0)) As Drawdown, CAM.Enum As CAM_Enum, Cl.Enum As Cl_Enum
		FROM  Customer C INNER JOIN
			CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
			Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			DueDiligence D ON E.EnquiryId = D.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
			Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
			Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
			Enum_Classification Cl ON ET.Classification = Cl.Enum 
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		)
		UNION
		(SELECT CAM.Description As CustAcceptanceMethod , Cl.Description As Classification, C.CustomerId
	, (ISNULL(AE.CashPrice_Advance,0) + ISNULL(AE.OtherAdvance,0) - ISNULL(AE.Deposit,0) + ISNULL(AE.Brokerage,0)) As Drawdown, CAM.Enum As CAM_Enum, Cl.Enum As Cl_Enum
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			Enum_CustAcceptanceMethod CAM ON C.AcceptanceMethod = CAM.Enum INNER JOIN
			Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum INNER JOIN
			Enum_Classification Cl ON ET.Classification = Cl.Enum 
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		)) AS SubData

		GROUP BY SubData.Classification, SubData.CustAcceptanceMethod, SubData.CAM_Enum, SubData.Cl_Enum
		Order By SubData.CustAcceptanceMethod, SubData.Classification

END

-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.AcceptanceMethodClassificationSummary '2016-07-01','2017-10-30'
GO