-- =============================================
-- return number of Enquiries by Classification
-- 20/11/2017: Christopher: Modify for Union with Archive tables
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'LoanClassificationSummary' 
)
   DROP PROCEDURE dbo.LoanClassificationSummary
GO

CREATE PROCEDURE dbo.LoanClassificationSummary
	@startDate date,
	@endDate date 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--SELECT EC.Description, COUNT(E.EnquiryId) AS Total, Sum(D.CashPrice_Advance) + Sum(D.OtherAdvance) - Sum(D.Deposit) + Sum(D.Brokerage) As Drawdown, EC.Enum
	--	FROM  Enquiry E INNER JOIN
	--	DueDiligence D ON E.EnquiryId = D.EnquiryId INNER JOIN
	--		Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
	--		Enum_EnquiryType EET ON E.EnquiryType = EET.Enum INNER JOIN
	--		Enum_Classification EC ON EET.Classification = EC.Enum
	--	WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
	--	GROUP BY EC.[Description], EC.Enum

	SELECT SubData.[Description], COUNT(SubData.EnquiryId) AS Total, Sum(SubData.Drawdown) AS Drawdown, SubData.Enum
	FROM
		((SELECT EC.[Description], E.EnquiryId,(ISNULL(D.CashPrice_Advance,0) + ISNULL(D.OtherAdvance,0) - ISNULL(D.Deposit,0) + ISNULL(D.Brokerage,0)) As Drawdown, EC.Enum
		FROM  Enquiry E INNER JOIN
		DueDiligence D ON E.EnquiryId = D.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
			Enum_EnquiryType EET ON E.EnquiryType = EET.Enum INNER JOIN
			Enum_Classification EC ON EET.Classification = EC.Enum
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		)
		UNION
		(SELECT EC.[Description], AE.EnquiryId, (ISNULL(AE.CashPrice_Advance,0) + ISNULL(AE.OtherAdvance,0) - ISNULL(AE.Deposit,0) + ISNULL(AE.Brokerage,0)) As Drawdown, EC.Enum
		FROM  ArchiveEnquiry AE INNER JOIN
			Enum_EnquiryType EET ON AE.EnquiryType = EET.Enum INNER JOIN
			Enum_Classification EC ON EET.Classification = EC.Enum
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		)) AS SubData		
		GROUP BY SubData.[Description], SubData.Enum

END

-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.LoanClassificationSummary '2016-07-01','2017-06-30'
GO