-- =============================================
-- Director/Shareholder AML Risk Summary 22/11/2017
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'DirectorShareholderAMLRiskSummary' 
)
   DROP PROCEDURE dbo.DirectorShareholderAMLRiskSummary
GO

CREATE PROCEDURE dbo.DirectorShareholderAMLRiskSummary
	@startDate date,
	@endDate date 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SubData.[Description] , SubData.DTSBType,  Count(SubData.DTSBId) AS Total,  SubData.AML_Enum, SubData.EDT_Enum
	FROM
	((SELECT AML.[Description], EDT.[Description] As DTSBType, DTSB.Id AS DTSBId, AML.Enum AS AML_Enum, EDT.Enum AS EDT_Enum
		FROM  Customer C INNER JOIN
			CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
			Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
			Payout P ON E.EnquiryId = p.EnquiryId INNER JOIN
			DirectorTrusteeShareHolderBeneficiary DTSB ON C.CustomerId = DTSB.CustomerId INNER JOIN
			Enum_AMLRisk AML ON DTSB.AMLRisk = AML.Enum INNER JOIN
			Enum_DTSB_Type EDT ON DTSB.[Type] = EDT.Enum
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		AND (DTSB.[Type] = 0 OR DTSB.[Type] = 2)
		)
		UNION
		(SELECT AML.[Description], EDT.[Description] As DTSBType, DTSB.Id AS DTSBId, DTSB.AMLRisk AS AML_Enum, DTSB.[Type] AS EDT_Enum
		FROM  Customer C INNER JOIN
			CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
			ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
			DirectorTrusteeShareHolderBeneficiary DTSB ON C.CustomerId = DTSB.CustomerId INNER JOIN
			Enum_AMLRisk AML ON DTSB.AMLRisk = AML.Enum INNER JOIN
			Enum_DTSB_Type EDT ON DTSB.[Type] = EDT.Enum
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND (DTSB.[Type] = 0 OR DTSB.[Type] = 2)
		)) AS SubData

		GROUP BY SubData.[Description], SubData.DTSBType, SubData.AML_Enum, SubData.EDT_Enum
		Order By SubData.AML_Enum, SubData.EDT_Enum
END

-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.DirectorShareholderAMLRiskSummary '2016-07-01','2017-11-30'
GO