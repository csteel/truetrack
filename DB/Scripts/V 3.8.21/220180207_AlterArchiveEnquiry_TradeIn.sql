--Add TradeIn money field in ArchiveEnquiry table
--07/02/2018: Christopher
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--ArchiveEnquiry table
--Add the TradeIn column to ArchiveEnquiry table
IF Not exists(select * from sys.columns 
            where Name = N'TradeIn' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD TradeIn money NULL;
	END
	BEGIN
		ALTER TABLE [dbo].ArchiveEnquiry ADD  CONSTRAINT [DF_ArchiveEnquiry_TradeIn]  DEFAULT ((0.00)) FOR [TradeIn]
	END
	BEGIN
		PRINT 'Added TradeIn field to ArchiveEnquiry table'
	END	
END
ELSE
BEGIN
	PRINT 'TradeIn field already exists in ArchiveEnquiry table'
END
GO

