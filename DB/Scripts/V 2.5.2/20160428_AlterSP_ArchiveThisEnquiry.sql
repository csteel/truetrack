--TrueTrack V2.5.1
USE Dev_TrueTrack
--USE TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[ArchiveThisEnquiry]    Script Date: 29/04/2016 12:01:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author: Christopher Steel
-- Create date: 07/06/2011
-- Description:	Archive Enquiry Data
-- =============================================
-- Updated 05/11/2013: Christopher: Introduced condition:  if @CurrentStatus = 'Declined' OR @CurrentStatus = 'Withdrawn' OR @CurrentStatus = ''
-- Update 24/03/2016: Christopher: Added LoanPurpose to fields archived
-- Update 28/04/2016: Christopher Added EnquiryType
-- Later need to remove LoanReason
-- =============================================
ALTER PROCEDURE [dbo].[ArchiveThisEnquiry] 
	-- Add the parameters for the stored procedure here
	@EnquiryId int, 
	@EnquirySummary varchar(MAX) = '',
	@ApplicationDoc varchar(MAX) = ''
AS
    -- Declare variables used in error checking.
    DECLARE @error_var int, @rowcount_var int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @CurrentStatus varchar(20) 
	SET @CurrentStatus = ''
    -- Insert statements for procedure here
    -- Test if record already exits
    SELECT EnquiryId 
    FROM dbo.ArchiveEnquiry
    WHERE EnquiryId = @EnquiryId
    -- Save the @@ERROR and @@ROWCOUNT values in local 
	-- variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 0 
        BEGIN
         --    BEGIN TRY
         --   BEGIN TRAN
	        --INSERT INTO dbo.ArchiveEnquiry (EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, DealerId, DealerName, CurrentStatus, LoanReason,
	        --PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId)
	        --SELECT EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, dbo.Enquiry.DealerId, dbo.ActiveDealers.Name, CurrentStatus, LoanReason, PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId
	        --FROM dbo.ActiveDealers INNER JOIN dbo.Enquiry ON dbo.ActiveDealers.DealerId = dbo.Enquiry.DealerId
	        --WHERE EnquiryId = @EnquiryId
	        --COMMIT TRAN
	        --END TRY
	        
	         BEGIN TRY
            BEGIN TRAN
            -- Added 05/11/2013
            Select @CurrentStatus = ISNULL(CurrentStatus,'') from Enquiry WHERE dbo.Enquiry.EnquiryId = @EnquiryId
            
            if @CurrentStatus = 'Declined' OR @CurrentStatus = 'Withdrawn' OR @CurrentStatus = ''
            Begin
            INSERT INTO dbo.ArchiveEnquiry (EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, DealerId, DealerName, CurrentStatus, LoanReason, LoanPurpose,
	        PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId, DDAprovalDate, ApprovalStatus, EnquiryType)
	        SELECT dbo.Enquiry.EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, dbo.Enquiry.DealerId, dbo.ActiveDealers.Name, CurrentStatus, LoanReason, LoanPurpose,
	        PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId, dbo.Enquiry.DDAprovalDate, dbo.Enquiry.ApprovalStatus, dbo.Enquiry.EnquiryType
	        FROM dbo.Enquiry INNER JOIN 
	        dbo.ActiveDealers ON dbo.ActiveDealers.DealerId = dbo.Enquiry.DealerId LEFT OUTER JOIN
	        dbo.DueDiligence ON dbo.DueDiligence.EnquiryId = dbo.Enquiry.EnquiryId LEFT OUTER JOIN
	        dbo.Payout ON dbo.Payout.EnquiryId = dbo.Enquiry.EnquiryId 
	        
	        WHERE dbo.Enquiry.EnquiryId = @EnquiryId
	        End
            ELSE
            Begin
            INSERT INTO dbo.ArchiveEnquiry (EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, DealerId, DealerName, CurrentStatus, LoanReason, LoanPurpose,
	        PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId, DDAprovalDate, ApprovalStatus, ProcessDate, PayoutCompleted, PayoutMonth, CashPrice_Advance, OtherAdvance, Deposit, 
	        YFLFees, YFLInsurances, EnquiryType)
	        SELECT dbo.Enquiry.EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, dbo.Enquiry.DealerId, dbo.ActiveDealers.Name, CurrentStatus, LoanReason, LoanPurpose,
	        PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId, dbo.Enquiry.DDAprovalDate, dbo.Enquiry.ApprovalStatus, dbo.Payout.ProcessDate,dbo.Payout.PayoutCompleted, dbo.Payout.PayoutMonth, dbo.DueDiligence.CashPrice_Advance,
	        dbo.DueDiligence.OtherAdvance, dbo.DueDiligence.Deposit, dbo.DueDiligence.YFLFees, dbo.DueDiligence.YFLInsurances, dbo.Enquiry.EnquiryType
	        FROM dbo.Enquiry INNER JOIN 
	        dbo.ActiveDealers ON dbo.ActiveDealers.DealerId = dbo.Enquiry.DealerId LEFT OUTER JOIN
	        dbo.DueDiligence ON dbo.DueDiligence.EnquiryId = dbo.Enquiry.EnquiryId LEFT OUTER JOIN
	        dbo.Payout ON dbo.Payout.EnquiryId = dbo.Enquiry.EnquiryId 
	        
	        WHERE dbo.Enquiry.EnquiryId = @EnquiryId
	        End
            -- End of Added 05/11/2013
            
	       
	        COMMIT TRAN
	        END TRY
	
	        BEGIN CATCH
	            ROLLBACK TRAN
	            PRINT 'Creating Archive failed'
	            RETURN (1)
	        END CATCH
        END
    ELSE
        BEGIN
            PRINT 'Archive already exists'
            RETURN (2)
        END

	-- Verify data is in table
	SELECT * FROM dbo.ArchiveEnquiry
	WHERE EnquiryId = @EnquiryId

	-- Save the @@ERROR and @@ROWCOUNT values in local 
	-- variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 0 
        BEGIN
        -- no rows returned from select statement
            PRINT 'Archive record not found'
            RETURN (3)
        END
    ELSE
        BEGIN
        -- successful
        -- add the documents
        UPDATE dbo.ArchiveEnquiry 
        SET EnquirySummary = @EnquirySummary,
        ApplicationDoc = @ApplicationDoc
        WHERE EnquiryId = @EnquiryId
        -- archiving complete
        
        -- get QRGListId if Column exists
        IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'dbo.Enquiry' AND COLUMN_NAME = 'QRGListId')
            -- exists
            BEGIN            
            PRINT 'QRGListID exits in Enquiry table'
            RETURN(4)   
            END
         ELSE
            BEGIN
            -- does not exist
            -- Delete from Delete BudgetItems
            DELETE FROM dbo.Budget
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete SecurityItems
            DELETE FROM dbo.Security
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete DueDiligence
            DELETE FROM dbo.DueDiligence
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete Payout
            DELETE FROM dbo.Payout
            WHERE EnquiryId = @EnquiryId
            -- get QRGList if exist
            DELETE FROM dbo.QRGList
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete Enquiry
            DELETE FROM dbo.Enquiry
            WHERE EnquiryId = @EnquiryId        
            -- Delete from Delete QRGList
            
            PRINT 'Archiving completed'
            RETURN(0)
            END
       
        END



