USE [Dev_TrueTrack]
GO
PRINT 'Starting database script'
GO
PRINT '****************************'
GO
--***************************************************************************
--Create Enum_EnquiryType table and populate it
--Script by Christopher 13/05/2016
--***************************************************************************
--Test if table exists
if(object_id(N'[dbo].[Enum_EnquiryType]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Enum_EnquiryType table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Enum_EnquiryType](
			[Enum] [int] NOT NULL,
			[Name] [varchar](50) NULL,
			[Description] [varchar](50) NULL,
		 CONSTRAINT [PK_Enum_EnquiryType] PRIMARY KEY CLUSTERED 
		(
			[Enum] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Enum_EnquiryType table created'
	END
END
ELSE
BEGIN 
	BEGIN
		PRINT 'Enum_EnquiryType table exists'
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_EnquiryType', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_EnquiryType table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_EnquiryType CONSTRAINT from Enum_EnquiryType table
			ALTER TABLE [dbo].[Enum_EnquiryType] DROP CONSTRAINT [chk_read_only_Enum_EnquiryType]
			END
		END
	END
END
GO
--***************************************************************************
--Delete data from table
BEGIN
        BEGIN
            PRINT 'Delete records from Enum_EnquiryType table'
        END
        BEGIN
			DELETE FROM Enum_EnquiryType
        END
END
GO
--***************************************************************************
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate Enum_EnquiryType table'
        END
        BEGIN
        INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (0,'PersonalLoanNew','Personal Loan (New)');
        INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (1,'PersonalLoanRefinance','Personal Loan (Refinance)');
		INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (2,'PersonalLoanVariation','Personal Loan (Variation)');
        INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (3,'PersonalSplitLoanDrawdown','Personal Split Loan Drawdown');
		INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (4,'ConsumerRetail','Consumer Retail');
        INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (5,'ConsumerVehicle','Consumer Vehicle');
		INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (6,'BusinessLoanNew','Business Loan (New)');
        INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (7,'BusinessLoanRefinance','Business Loan (Refinance)');
		INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (8,'BusinessLoanVariation','Business Loan (Variation)');
        INSERT INTO [Enum_EnquiryType] ([Enum],[Name],[Description]) VALUES (9,'BusinessSplitLoanDrawdown','Business Split Loan Drawdown');
        END
END
GO
--***************************************************************************
--Protect table
BEGIN
	BEGIN
		PRINT 'Protect Enum_EnquiryType table'
    END
    BEGIN		
		ALTER TABLE [dbo].Enum_EnquiryType WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_EnquiryType CHECK( 1 = 0 )				
	END
END
GO
--***************************************************************************
-- finished script
PRINT '****************************'
GO
PRINT 'Database script finished'
GO










