--Script to add fields to the Enquiry & ArchiveEnquiry table'
--EnquiryType
--Written by Christopher 22/04/2016

--USE TestEnquiryWorkSheet
--USE Dev_TrueTrack
Use EnquiryWorkSheet

PRINT 'Starting database script'
GO
PRINT'Alter Enquiry table, add field for EnquiryType'
GO
PRINT '**************************'
GO
--add EnquiryType column
if NOT exists(select * from sys.columns 
            where Name = N'EnquiryType' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add EnquiryType field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD EnquiryType smallint NULL;
	END		
END
ELSE
BEGIN
	PRINT 'EnquiryType field already exists in Enquiry table'
END
GO
BEGIN
	PRINT 'Update EnquiryType fields'
END
BEGIN
	--Process the EnquiryType fields
	--Transpose LoanReason values into EnquiryType values
	declare @EnquiryType smallint
	declare @LoanReason varchar(max)
	declare @EnquiryId int
	declare @applicationCount int	

	SET @applicationCount = 0	

	declare cur CURSOR LOCAL for
		select LoanReason, EnquiryId from [dbo].[Enquiry] 
		--Where EnquiryType IS NULL
	open cur

	fetch next from cur into @LoanReason, @EnquiryId

	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row		  
		SET @EnquiryType = CASE @LoanReason
				WHEN 'Personal Loan (New)' THEN 0
				WHEN 'Personal Loan (Refinance)' THEN 1
				WHEN 'Personal Loan (Variation)' THEN 2
				WHEN 'Personal Split Loan Drawdown' THEN 3
				WHEN 'Consumer Retail' THEN 4
				WHEN 'Consumer Vehicle' THEN 5
				WHEN 'Business Loan (New)' THEN 6
				WHEN 'Business Loan' THEN 6
				WHEN 'Business Loan (Refinance)' THEN 7
				WHEN 'Business Loan (Variation)' THEN 8
				WHEN 'Business Split Loan Drawdown' THEN 9
				END  
	
		Update [Enquiry] SET EnquiryType = @EnquiryType 
		WHERE EnquiryId = @EnquiryId
	
		SELECT @applicationCount = @applicationCount + 1;		
			
		fetch next from cur into @LoanReason, @EnquiryId

	END

	close cur
	deallocate cur

	PRINT 'Data migrated for ' + CAST(@applicationCount AS VARCHAR)  + ' applications'	
	-- finished migrate existing data!!!!! ***************************************************
	PRINT 'Finished migrating data.'
END
PRINT '**************************'
GO
--***********************************************

PRINT'Alter ArchiveEnquiry table, add field for EnquiryType'
GO

--add EnquiryType column
if NOT exists(select * from sys.columns 
            where Name = N'EnquiryType' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add EnquiryType field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD EnquiryType smallint NULL;
	END		
END
ELSE
BEGIN
	PRINT 'EnquiryType field already exists in ArchiveEnquiry table'
END
GO
BEGIN
	PRINT 'Update EnquiryType fields'
END
BEGIN
	--Process the EnquiryType fields
	--Transpose LoanReason values into EnquiryType values
	declare @EnquiryType smallint
	declare @LoanReason varchar(max)
	declare @EnquiryId int
	declare @applicationCount int

	SET @applicationCount = 0

	declare cur CURSOR LOCAL for
		select LoanReason, EnquiryId from [dbo].[ArchiveEnquiry] 
		Where EnquiryType IS NULL
	open cur

	fetch next from cur into @LoanReason, @EnquiryId

	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row
		PRINT CAST(@EnquiryId AS VARCHAR)  
		SET @EnquiryType = CASE @LoanReason
				WHEN 'Personal Loan (New)' THEN 0
				WHEN 'Personal Loan (Refinance)' THEN 1
				WHEN 'Personal Loan (Variation)' THEN 2
				WHEN 'Personal Split Loan Drawdown' THEN 3
				WHEN 'Split Loan Drawdown' THEN 3
				WHEN 'Consumer Retail' THEN 4
				WHEN 'Consumer Vehicle' THEN 5
				WHEN 'Business Loan (New)' THEN 6
				WHEN 'Business Loan' THEN 6
				WHEN 'Business Loan (Refinance)' THEN 7
				WHEN 'Business Loan (Variation)' THEN 8
				WHEN 'Business Split Loan Drawdown' THEN 9
				END  
	
		Update [ArchiveEnquiry] SET EnquiryType = @EnquiryType 
		WHERE EnquiryId = @EnquiryId
	
		SELECT @applicationCount = @applicationCount + 1;
	
		fetch next from cur into @LoanReason, @EnquiryId

	END

	close cur
	deallocate cur

	PRINT 'Data migrated for ' + CAST(@applicationCount AS VARCHAR)  + ' applications'
	-- finished migrate existing data!!!!! ***************************************************
	PRINT 'Finished migrating data.'
END
--***********************************************





-- Finished
PRINT '**************************'
GO
PRINT 'DB script completed'
GO
