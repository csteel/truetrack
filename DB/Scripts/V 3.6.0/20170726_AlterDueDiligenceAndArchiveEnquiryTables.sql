--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
-- =============================================
-- Author: Christopher Steel
-- Create date: 26/07/2017
-- Description:	Alter the DueDiligence and ArchiveEnquiry table, add a fields to extend the LoanBreakdown and
--to record the Dealer Commission values.
-- =============================================
--Update 03/08/2017: Christopher: Add CommissionAmt, RetentionAmt, Refinance to ArchiveEnquiry table
-- =============================================
PRINT 'Starting database script'
GO
PRINT'Alter DueDiligence table, add a fields to extend the LoanBreakdown'
GO
PRINT 'and to record the Dealer Commission values'
GO
PRINT '**************************'
GO
--##############################################
--DueDiligence table
--add Brokerage column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'Brokerage' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add Brokerage field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD Brokerage smallmoney NULL  CONSTRAINT [DF_DueDiligence_Brokerage]  DEFAULT (0.00);
	END	
END
ELSE
BEGIN
	PRINT 'Brokerage field already exists in DueDiligence table'
END
GO

--add LoanRetention column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'LoanRetention' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add LoanRetention field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD LoanRetention smallint NULL CONSTRAINT [DF_DueDiligence_LoanRetention]  DEFAULT (0);
	END	
END
ELSE
BEGIN
	PRINT 'LoanRetention field already exists in DueDiligence table'
END
GO

--add LoanFiguresChecked column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'LoanFiguresChecked' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add LoanFiguresChecked field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD LoanFiguresChecked bit NULL CONSTRAINT [DF_DueDiligence_LoanFiguresChecked]  DEFAULT (0);
	END	
END
ELSE
BEGIN
	PRINT 'LoanFiguresChecked field already exists in DueDiligence table'
END
GO

--add ProductCommission column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'ProductCommission' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ProductCommission field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD ProductCommission smallmoney NULL CONSTRAINT [DF_DueDiligence_ProductCommission]  DEFAULT (0.00);
	END	
END
ELSE
BEGIN
	PRINT 'ProductCommission field already exists in DueDiligence table'
END
GO

--add InterestCommission column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'InterestCommission' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add InterestCommission field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD InterestCommission smallmoney NULL CONSTRAINT [DF_DueDiligence_InterestCommission]  DEFAULT (0.00);
	END	
END
ELSE
BEGIN
	PRINT 'InterestCommission field already exists in DueDiligence table'
END
GO

--add CommissionRetention column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'CommRetention' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add CommRetention field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD CommRetention smallint NULL  CONSTRAINT [DF_DueDiligence_CommRetention]  DEFAULT (0);
	END	
END
ELSE
BEGIN
	PRINT 'CommRetention field already exists in DueDiligence table'
END
GO

--add CommissionFiguresChecked column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'CommFiguresChecked' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add CommFiguresChecked field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD CommFiguresChecked bit NULL CONSTRAINT [DF_DueDiligence_CommFiguresChecked]  DEFAULT (0);
	END	
END
ELSE
BEGIN
	PRINT 'CommFiguresChecked field already exists in DueDiligence table'
END
GO

PRINT '**************************'
GO
--End of DueDiligence
PRINT 'End of DueDiligence'
GO
--##############################################
--ArchiveEnquiry
PRINT 'Start add columns to ArchiveEnquiry table'
GO
PRINT '**************************'
GO
--add columns to ArchiveEnquiry table
--add Brokerage column to ArchiveEnquiry table
if NOT exists(select * from sys.columns 
            where Name = N'Brokerage' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add Brokerage field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD Brokerage smallmoney NULL;
	END	
END
ELSE
BEGIN
	PRINT 'Brokerage field already exists in ArchiveEnquiry table'
END
GO

--add ProductCommission column to ArchiveEnquiry table
if NOT exists(select * from sys.columns 
            where Name = N'ProductCommission' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ProductCommission field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD ProductCommission smallmoney NULL;
	END	
END
ELSE
BEGIN
	PRINT 'ProductCommission field already exists in ArchiveEnquiry table'
END
GO

--add InterestCommission column to ArchiveEnquiry table
if NOT exists(select * from sys.columns 
            where Name = N'InterestCommission' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add InterestCommission field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD InterestCommission smallmoney NULL;
	END	
END
ELSE
BEGIN
	PRINT 'InterestCommission field already exists in ArchiveEnquiry table'
END
GO

--Add CommissionAmt to ArchiveEnquiry table
if NOT exists(select * from sys.columns 
            where Name = N'CommissionAmt' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add CommissionAmt field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD CommissionAmt smallmoney NULL;
	END	
END
ELSE
BEGIN
	PRINT 'CommissionAmt field already exists in ArchiveEnquiry table'
END
GO

--Add RetentionAmt to ArchiveEnquiry table
if NOT exists(select * from sys.columns 
            where Name = N'RetentionAmt' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add RetentionAmt field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD RetentionAmt smallmoney NULL;
	END	
END
ELSE
BEGIN
	PRINT 'RetentionAmt field already exists in ArchiveEnquiry table'
END
GO

--Add Refinance
if NOT exists(select * from sys.columns 
            where Name = N'Refinance' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add Refinance field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD Refinance smallmoney NULL;
	END	
END
ELSE
BEGIN
	PRINT 'Refinance field already exists in ArchiveEnquiry table'
END
GO

--End of ArchiveEnquiry
PRINT '**************************'
GO
PRINT 'End of ArchiveEnquiry'
GO
--##############################################
--Payout table
PRINT 'Start add columns to Payout table'
GO
PRINT '**************************'
GO
--add CommissionRef column to Payout table
if NOT exists(select * from sys.columns 
            where Name = N'CommissionRef' and Object_ID = Object_ID(N'Payout'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add CommissionRef field to Payout table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Payout
		ADD CommissionRef varchar(20) NULL;
	END	
END
ELSE
BEGIN
	PRINT 'CommissionRef field already exists in Payout table'
END
GO
--add CommissionAmt column to Payout table
if NOT exists(select * from sys.columns 
            where Name = N'CommissionAmt' and Object_ID = Object_ID(N'Payout'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add CommissionAmt field to Payout table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Payout
		ADD CommissionAmt smallmoney NULL CONSTRAINT [DF_Payout_CommissionAmt]  DEFAULT (0.00);
	END	
END
ELSE
BEGIN
	PRINT 'CommissionAmt field already exists in Payout table'
END
GO

--End of Payout
PRINT '**************************'
GO
PRINT 'End of Payout'
GO
--##############################################
--End of script
PRINT 'DB script completed'
GO

