USE [TestEnquiryWorkSheet]
GO
/****** Object:  StoredProcedure [dbo].[ArchiveThisEnquiry]    Script Date: 3/08/2017 3:46:09 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author: Christopher Steel
-- Create date: 07/06/2011
-- Description:	Archive Enquiry Data
-- =============================================
-- Updated 05/11/2013: Christopher: Introduced condition:  if @CurrentStatus = 'Declined' OR @CurrentStatus = 'Withdrawn' OR @CurrentStatus = ''
-- Update 24/03/2016: Christopher: Added LoanPurpose to fields archived
-- Update 28/04/2016: Christopher Added EnquiryType
-- Update 10/06/2016: Christopher: Modify code to suit the changes to database schema (multiple customers)
-- Later need to remove LoanReason
-- Update 12/12/2016: Christopher: Add ContactDisplayName, ContactSuburb, ContactCity to the INSERT INTO statement; added DELETE FROM dbo.CustomerEnquiry
-- Update 21/07/2017: Christopher: added DELETE FROM dbo.EnquiryComment
-- Update 03/08/2017: Christopher: Add Brokerage, ProductCommission, InterestCommission, CommissionAmt
-- =============================================
ALTER PROCEDURE [dbo].[ArchiveThisEnquiry] 
	-- Add the parameters for the stored procedure here
	@EnquiryId int, 
	@EnquirySummary varchar(MAX) = '',
	@ApplicationDoc varchar(MAX) = ''
AS
    -- Declare variables used in error checking.
    DECLARE @error_var int, @rowcount_var int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @CurrentStatus varchar(20) 
	SET @CurrentStatus = ''
    -- Insert statements for procedure here
    -- Test if record already exits
    SELECT EnquiryId 
    FROM dbo.ArchiveEnquiry
    WHERE EnquiryId = @EnquiryId
    -- Save the @@ERROR and @@ROWCOUNT values in local 
	-- variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 0 
        BEGIN
         --    BEGIN TRY
         --   BEGIN TRAN
	        --INSERT INTO dbo.ArchiveEnquiry (EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, DealerId, DealerName, CurrentStatus, LoanReason,
	        --PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId)
	        --SELECT EnquiryId, ClientId, EnquiryCode, ApplicationCode, DateTime, dbo.Enquiry.DealerId, dbo.ActiveDealers.Name, CurrentStatus, LoanReason, PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId
	        --FROM dbo.ActiveDealers INNER JOIN dbo.Enquiry ON dbo.ActiveDealers.DealerId = dbo.Enquiry.DealerId
	        --WHERE EnquiryId = @EnquiryId
	        --COMMIT TRAN
	        --END TRY
	        
	         BEGIN TRY
            BEGIN TRAN
            -- Added 05/11/2013
            Select @CurrentStatus = ISNULL(CurrentStatus,'') from Enquiry WHERE dbo.Enquiry.EnquiryId = @EnquiryId
            
            if @CurrentStatus = 'Declined' OR @CurrentStatus = 'Withdrawn' OR @CurrentStatus = ''
            BEGIN
				INSERT INTO dbo.ArchiveEnquiry (EnquiryId, EnquiryCode, ApplicationCode, [DateTime], DealerId, DealerName, CurrentStatus, LoanPurpose
				,PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId, DDAprovalDate, ApprovalStatus, EnquiryType, BranchId
				,ContactDisplayName, ContactSuburb, ContactCity)
				SELECT E.EnquiryId, E.EnquiryCode, E.ApplicationCode, E.[DateTime], E.DealerId, AD.Name, E.CurrentStatus, E.LoanPurpose
				,E.PrelimMethod, E.PrelimSource, E.LoanValue, E.EnquiryManagerId, E.DDAprovalDate, E.ApprovalStatus, E.EnquiryType, E.BranchId 
				,E.ContactDisplayName, E.ContactSuburb, E.ContactCity
				FROM dbo.Enquiry E INNER JOIN 
				dbo.ActiveDealers AD ON AD.DealerId = E.DealerId LEFT OUTER JOIN
				dbo.DueDiligence ON dbo.DueDiligence.EnquiryId = E.EnquiryId LEFT OUTER JOIN
				dbo.Payout ON dbo.Payout.EnquiryId = E.EnquiryId 	        
				WHERE E.EnquiryId = @EnquiryId
	        END
            ELSE
            BEGIN
				INSERT INTO dbo.ArchiveEnquiry (EnquiryId, EnquiryCode, ApplicationCode, [DateTime], DealerId, DealerName, CurrentStatus, LoanPurpose,
				PrelimMethod, PrelimSource, LoanValue, EnquiryManagerId, DDAprovalDate, ApprovalStatus, ProcessDate, PayoutCompleted, PayoutMonth, CashPrice_Advance, OtherAdvance, Deposit, 
				YFLFees, YFLInsurances, EnquiryType, BranchId, ContactDisplayName, ContactSuburb, ContactCity, Brokerage, ProductCommission, InterestCommission, CommissionAmt, RetentionAmt, Refinance)
				SELECT E.EnquiryId, E.EnquiryCode, E.ApplicationCode, E.[DateTime], E.DealerId, AD.Name, E.CurrentStatus, E.LoanPurpose,
				E.PrelimMethod, E.PrelimSource, E.LoanValue, E.EnquiryManagerId, E.DDAprovalDate, E.ApprovalStatus, P.ProcessDate,P.PayoutCompleted, P.PayoutMonth, DD.CashPrice_Advance,
				DD.OtherAdvance, DD.Deposit, DD.YFLFees, DD.YFLInsurances, E.EnquiryType, E.BranchId, E.ContactDisplayName, E.ContactSuburb, E.ContactCity, DD.Brokerage, 
				DD.ProductCommission, DD.InterestCommission, P.CommissionAmt, P.RetentionAmt, DD.Refinance
				FROM dbo.Enquiry E INNER JOIN 
				dbo.ActiveDealers AD ON AD.DealerId = E.DealerId LEFT OUTER JOIN
				dbo.DueDiligence DD ON DD.EnquiryId = E.EnquiryId LEFT OUTER JOIN
				dbo.Payout P ON P.EnquiryId = E.EnquiryId 	        
				WHERE E.EnquiryId = @EnquiryId
	        END
            -- End of Added 05/11/2013
			-- Added 10/06/2016 --Iterate through Customers
			-- create in-memory customer table to hold distinct customerId
			DECLARE @i int
			DECLARE @numrows int
			DECLARE @customerId int
			DECLARE @type varchar(20)
			DECLARE @id_number int
			DECLARE @customer_table TABLE (idx smallint Primary Key IDENTITY(1,1), customerId int, [type] varchar(20), id_number int)
			-- populate customer table
			INSERT INTO @customer_table (customerId, [type], id_number)
			SELECT distinct CE.CustomerId,CE.[Type],CE.Id_number
	        FROM dbo.Enquiry E INNER JOIN
			CustomerEnquiry CE ON CE.EnquiryId = E.EnquiryId 
	        WHERE E.EnquiryId = @EnquiryId
			-- enumerate the table
			SET @i = 1
			SET @numrows = (SELECT COUNT(*) FROM @customer_table)
			IF @numrows > 0
				WHILE (@i <= (SELECT MAX(idx) FROM @customer_table))
				BEGIN
					-- get the next customer primary key
					SET @customerId = (SELECT customerId FROM @customer_table WHERE idx = @i)
					INSERT INTO dbo.CustomerArchiveEnquiry (CustomerId,EnquiryId,[Type],Id_number)
					SELECT CT.customerId,@EnquiryId,CT.[type],CT.id_number
					FROM @customer_table CT	        
					WHERE CT.customerId = @customerId
					-- increment counter
					SET @i = @i + 1
				END
            -- End of Added 10/06/2016
	       
	        COMMIT TRAN
	        END TRY
	
	        BEGIN CATCH
	            ROLLBACK TRAN
	            PRINT 'Creating Archive failed'
	            RETURN (1)
	        END CATCH
        END
    ELSE
        BEGIN
            PRINT 'Archive already exists'
            RETURN (2)
        END

	-- Verify data is in table
	SELECT * FROM dbo.ArchiveEnquiry
	WHERE EnquiryId = @EnquiryId

	-- Save the @@ERROR and @@ROWCOUNT values in local 
	-- variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 0 
        BEGIN
        -- no rows returned from select statement
            PRINT 'Archive record not found'
            RETURN (3)
        END
    ELSE
        BEGIN
        -- successful
        -- add the documents
        UPDATE dbo.ArchiveEnquiry 
        SET EnquirySummary = @EnquirySummary,
        ApplicationDoc = @ApplicationDoc
        WHERE EnquiryId = @EnquiryId
        -- archiving complete
        
        -- get QRGListId if Column exists
        IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'dbo.Enquiry' AND COLUMN_NAME = 'QRGListId')
            -- exists
            BEGIN            
            PRINT 'QRGListID exits in Enquiry table'
            RETURN(4)   
            END
         ELSE
            BEGIN
            -- does not exist
            -- Delete from Delete BudgetItems
            DELETE FROM dbo.Budget
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete SecurityItems
            DELETE FROM dbo.Security
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete DueDiligence
            DELETE FROM dbo.DueDiligence
            WHERE EnquiryId = @EnquiryId
            -- Delete from Delete Payout
            DELETE FROM dbo.Payout
            WHERE EnquiryId = @EnquiryId
            -- get QRGList if exist
            DELETE FROM dbo.QRGList
            WHERE EnquiryId = @EnquiryId
			--CustomerEnquiry Added 12/12/2016
			DELETE FROM dbo.CustomerEnquiry
            WHERE EnquiryId = @EnquiryId
			--EnquiryComment Added 21/07/2017
			DELETE FROM dbo.EnquiryComment
			WHERE EnquiryId = @EnquiryId
            -- Delete from Delete Enquiry
            DELETE FROM dbo.Enquiry
            WHERE EnquiryId = @EnquiryId  
            
            PRINT 'Archiving completed'
            RETURN(0)
            END
       
        END



