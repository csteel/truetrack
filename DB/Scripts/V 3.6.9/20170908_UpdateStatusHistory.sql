/****** Script for SelectTopNRows command from SSMS  ******/
use TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO

SELECT Count(StatusHistory.Id)
  FROM [dbo].[StatusHistory]
  where status = 'Waiting for Docs'


UPDATE [dbo].[StatusHistory] SET
Status = 'CAD Approved'
WHERE Status = 'Waiting for Docs' 


SELECT Count(StatusHistory.Id)
  FROM [dbo].[StatusHistory]
  WHERE status = 'CAD Approved'
