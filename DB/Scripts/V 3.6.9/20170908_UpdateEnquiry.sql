use TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO
--UPDATE CurrentStatus
SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  where CurrentStatus = 'Waiting for Docs'
GO

UPDATE [dbo].[Enquiry] SET
CurrentStatus = 'CAD Approved'
WHERE CurrentStatus = 'Waiting for Docs' 
GO

SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  WHERE CurrentStatus = 'CAD Approved'
  GO

--UPDATE Status30mins
SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  where Status30mins = 'Waiting for Docs'
GO

UPDATE [dbo].[Enquiry] SET
Status30mins = 'CAD Approved'
WHERE Status30mins = 'Waiting for Docs' 
GO

SELECT Count(Enquiry.EnquiryId)
  FROM [dbo].[Enquiry]
  WHERE Status30mins = 'CAD Approved'
  GO
