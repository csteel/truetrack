-- =============================================
-- 04/12/2017: Christopher: Update Stored procedures for DirectorTrusteeShareHolderBeneficiary; [InsertDTSB]
-- 04/12/2017: Christopher Add AMLRisk
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'InsertDTSB' 
)
   DROP PROCEDURE dbo.InsertDTSB
GO

-- =============================================
CREATE PROCEDURE [dbo].[InsertDTSB]	
	@Type int = 0,
	@CustomerId int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0,
	@IsaPEP bit = 0,
	@AMLRisk smallint = 0,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO DirectorTrusteeShareHolderBeneficiary ([Type], CustomerId, FirstName, LastName, DateOfBirth, IdVerified, AddressVerified, IsaPEP, AMLRisk)
	VALUES(@Type, @CustomerId, @FirstName, @LastName, @DateOfBirth, @IdVerified, @AddressVerified, @IsaPEP, @AMLRisk)
	--Get back the Id
	SELECT @Id = Id
	FROM DirectorTrusteeShareHolderBeneficiary
	WHERE @@ROWCOUNT > 0 AND Id = SCOPE_IDENTITY()	
END