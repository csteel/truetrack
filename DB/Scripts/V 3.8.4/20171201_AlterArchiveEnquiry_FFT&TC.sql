--Add ForeignFundsTransfer (bit) & TotalCheque (bit) fields in ArchiveEnquiry table
--01/12/2017: Christopher
--Use TestEnquiryWorkSheet
Use EnquiryWorkSheet
GO

--ArchiveEnquiry table
--Add the ForeignFundsTransfer column to ArchiveEnquiry table
IF Not exists(select * from sys.columns 
            where Name = N'ForeignFundsTransfer' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD ForeignFundsTransfer bit NULL DEFAULT 0;
	END	
	BEGIN
		PRINT 'Added ForeignFundsTransfer field to ArchiveEnquiry table'
	END	
END
ELSE
BEGIN
	PRINT 'ForeignFundsTransfer field already exists in ArchiveEnquiry table'
END
GO

UPDATE ArchiveEnquiry SET
ForeignFundsTransfer = 0
WHERE ForeignFundsTransfer IS NULL;
GO

--Add the TotalCheque column to ArchiveEnquiry table
IF Not exists(select * from sys.columns 
            where Name = N'TotalCheque' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD TotalCheque bit NULL DEFAULT 0;
	END
	BEGIN
		PRINT 'Added TotalCheque field to ArchiveEnquiry table'
	END	
END
ELSE
BEGIN
	PRINT 'TotalCheque field already exists in ArchiveEnquiry table'
END
GO

UPDATE ArchiveEnquiry SET
TotalCheque = 0
WHERE TotalCheque IS NULL;
GO

