-- =============================================
-- 04/12/2017: Christopher: Update Stored procedures for DirectorTrusteeShareHolderBeneficiary; [UpdateDTSB]
-- 04/12/2017: Christopher Add AMLRisk
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'UpdateDTSB' 
)
   DROP PROCEDURE dbo.UpdateDTSB
GO
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDTSB]
	@Id int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0,
	@AMLRisk smallint = 0,
	@IsaPEP bit = 0
AS
BEGIN
	UPDATE  DirectorTrusteeShareHolderBeneficiary
	SET     FirstName = @FirstName, 
			LastName = @LastName, 			
			DateOfBirth = @DateOfBirth, 
			IdVerified = @IdVerified, 
			AddressVerified = @AddressVerified,
			IsaPEP = @IsaPEP,
			AMLRisk = @AMLRisk
	WHERE   (Id = @Id)
END