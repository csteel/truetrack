--01/12/2017: Christopher
--*************************
--The DB payout (and ArchiveEnquiry) table has the fields > 
--ForeignFundsTransfer (bit), payout.TotalCheque (bit). 
--If none of these fields is active then it is assumed that payment method is Domestic Transfer.
--Want datatable returned as the following > 
--Payment Method         | Number(Sum)
-----------------------------------------
--Domestic Transfer      | x
--Cheque                 | y
--ForeignFundsTransfer   | z
--*************************
--SELECT  Client,
--        [Business Unit],
--        SUM(CASE WHEN [Year] = 2010 THEN [USD Amt] ELSE 0 END) [2010],
--        SUM(CASE WHEN [Year] = 2011 THEN [USD Amt] ELSE 0 END) [2011],
--        SUM(CASE WHEN [Year] = 2012 THEN [USD Amt] ELSE 0 END) [2012]
--FROM YourTable
--GROUP BY Client, [Business Unit] 

USE TestEnquiryWorkSheet
GO

SELECT COUNT(CASE WHEN TotalCheque = 0 AND ForeignFundsTransfer = 0 THEN [EnquiryId] END) [Domestic Transfer]
		, COUNT(CASE WHEN TotalCheque = 1  THEN [EnquiryId] END) [Cheque]
		, COUNT(CASE WHEN ForeignFundsTransfer = 1  THEN [EnquiryId] END) [Foreign Funds Transfer]
FROM Payout
WHERE PayoutCompleted BETWEEN '2017-07-01' AND '2017-11-30'
GO


SELECT COUNT(CASE WHEN TotalCheque = 0 AND ForeignFundsTransfer = 0 THEN [EnquiryId] END) [Domestic Transfer]
		, COUNT(CASE WHEN TotalCheque = 1  THEN [EnquiryId] END) [Cheque]
		, COUNT(CASE WHEN ForeignFundsTransfer = 1  THEN [EnquiryId] END) [Foreign Funds Transfer]
		FROM Payout P
		WHERE P.PayoutCompleted BETWEEN '2017-07-01' AND '2017-11-30'
			UNION
		SELECT COUNT(CASE WHEN TotalCheque = 0 AND ForeignFundsTransfer = 0 THEN [EnquiryId] END) [Domestic Transfer]
		, COUNT(CASE WHEN TotalCheque = 1  THEN [EnquiryId] END) [Cheque]
		, COUNT(CASE WHEN ForeignFundsTransfer = 1  THEN [EnquiryId] END) [Foreign Funds Transfer]
		FROM ArchiveEnquiry AE
		WHERE AE.PayoutCompleted BETWEEN '2017-07-01' AND '2017-11-30'




--SELECT EnquiryId from Payout
--WHERE PayoutCompleted BETWEEN '2017-07-01' AND '2017-11-30'
--GO
