-- =============================================
-- PaymentMethodSummary: Christopher: 01/12/2017
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'spPaymentMethodSummary' 
)
   DROP PROCEDURE dbo.spPaymentMethodSummary
GO

CREATE PROCEDURE dbo.spPaymentMethodSummary
	@startDate date,
	@endDate date 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SUM(SubData.DT) [Domestic Transfer] , SUM(SubData.C) [Cheque],  SUM(SubData.FFT) [Foreign Funds Transfer]
	FROM
	((SELECT COUNT(CASE WHEN TotalCheque = 0 AND ForeignFundsTransfer = 0 THEN [EnquiryId] END) AS DT --[Domestic Transfer]
		, COUNT(CASE WHEN TotalCheque = 1  THEN [EnquiryId] END) AS C --[Cheque]
		, COUNT(CASE WHEN ForeignFundsTransfer = 1  THEN [EnquiryId] END) AS FFT --[Foreign Funds Transfer]
		FROM Payout P
		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		)
		UNION
		(SELECT COUNT(CASE WHEN TotalCheque = 0 AND ForeignFundsTransfer = 0 THEN [EnquiryId] END) AS DT --[Domestic Transfer]
		, COUNT(CASE WHEN TotalCheque = 1  THEN [EnquiryId] END) AS C --[Cheque]
		, COUNT(CASE WHEN ForeignFundsTransfer = 1  THEN [EnquiryId] END) AS FFT --[Foreign Funds Transfer]
		FROM ArchiveEnquiry AE
		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		)) AS SubData	

END
-- =============================================
-- to execute the stored procedure
-- =============================================
--EXECUTE dbo.spPaymentMethodSummary '2017-07-01', '2017-11-30'
GO