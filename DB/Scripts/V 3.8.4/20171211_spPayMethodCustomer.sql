USE [TestEnquiryWorkSheet]
--USE EnquiryWorkSheet
GO

CREATE PROCEDURE [dbo].[PayMethodCustomer]
-- =============================================
-- 11/12/2017: Christopher: Create sp to return PayMethod Loans
-- =============================================
	@startDate DATE,
	@endDate DATE,
	@payMethod varchar(24)
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF(@payMethod = 'Cheque')
     BEGIN
		SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
		FROM
		((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
		FROM  Customer C INNER JOIN
		CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
		Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
		Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
		Payout P ON E.EnquiryId = P.EnquiryId 
   		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		AND P.TotalCheque = 1 
		AND CE.Type = 0)
		UNION
		(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
		FROM  Customer C INNER JOIN
		CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
		ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
		Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum 
   		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND AE.TotalCheque = 1
		AND CAE.Type = 0)) AS SubData
	
		ORDER BY SubData.CustomerId
     END

ELSE IF(@payMethod = 'Foreign Funds Transfer')
      BEGIN
        SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
		FROM
		((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
		FROM  Customer C INNER JOIN
		CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
		Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
		Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
		Payout P ON E.EnquiryId = P.EnquiryId 
   		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		AND P.ForeignFundsTransfer = 1
		AND CE.Type = 0)
		UNION
		(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
		FROM  Customer C INNER JOIN
		CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
		ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
		Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum 
   		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND AE.ForeignFundsTransfer = 1
		AND CAE.Type = 0)) AS SubData
	
		ORDER BY SubData.CustomerId 
      END 

    ELSE      --<--- Default Task if none of the above is true
     BEGIN
       SELECT SubData.EnquiryCode, SubData.ApplicationCode, SubData.Name, SubData.[Description], SubData.PayoutCompleted,  SubData.EnquiryId, SubData.Archived
		FROM
		((SELECT E.EnquiryCode, E.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], P.PayoutCompleted,  E.EnquiryId, C.CustomerId, E.Archived
		FROM  Customer C INNER JOIN
		CustomerEnquiry CE ON C.CustomerId = CE.CustomerId INNER JOIN
		Enquiry E ON CE.EnquiryId = E.EnquiryId INNER JOIN
		Enum_EnquiryType ET ON E.EnquiryType = ET.Enum INNER JOIN
		Payout P ON E.EnquiryId = P.EnquiryId 
   		WHERE P.PayoutCompleted BETWEEN @startDate AND @endDate
		AND (P.TotalCheque = 0 AND P.ForeignFundsTransfer = 0)
		AND CE.Type = 0)
		UNION
		(SELECT AE.EnquiryCode, AE.ApplicationCode, ISNULL(C.Salutation, C.LegalName) As Name, ET.[Description], AE.PayoutCompleted,  AE.EnquiryId, C.CustomerId, AE.Archived
		FROM  Customer C INNER JOIN
		CustomerArchiveEnquiry CAE ON C.CustomerId = CAE.CustomerId INNER JOIN
		ArchiveEnquiry AE ON CAE.EnquiryId = AE.EnquiryId INNER JOIN
		Enum_EnquiryType ET ON AE.EnquiryType = ET.Enum 
   		WHERE AE.PayoutCompleted BETWEEN @startDate AND @endDate
		AND (AE.TotalCheque = 1 AND AE.ForeignFundsTransfer = 0)
		AND CAE.Type = 0)) AS SubData
	
		ORDER BY SubData.CustomerId
     END
		
END

--EXECUTE dbo.[PayMethodCustomer] '2017-04-01','2017-10-01', 'Cheque'
GO
