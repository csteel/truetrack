-- =============================================
-- 04/12/2017: Christopher: Update Stored procedures for DirectorTrusteeShareHolderBeneficiary; [GetSingleDTSB]
-- 04/12/2017: Christopher Add AMLRisk
-- 06/12/2017: Christopher: Add Join to Enum_AMLRisk to provide Description to query
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetSingleDTSB' 
)
   DROP PROCEDURE dbo.GetSingleDTSB
GO

-- =============================================
CREATE PROCEDURE [dbo].[GetSingleDTSB]
		@recordId int = 0	
AS
BEGIN
-- =============================================
-- 04/12/2017: Christopher: Update Stored procedures for DirectorTrusteeShareHolderBeneficiary; [GetSingleDTSB]
-- 04/12/2017: Christopher Add AMLRisk
-- 06/12/2017: Christopher: Add Join to Enum_AMLRisk to provide Description to query
-- =============================================
	SET NOCOUNT ON;
	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.[Type], 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, DirectorTrusteeShareHolderBeneficiary.IsaPEP
			, Enum_DTSB_Type.[Description], DirectorTrusteeShareHolderBeneficiary.AMLRisk, Enum_AMLRisk.[Description] As AMLRiskDesc
	FROM       DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.[Type] = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId INNER JOIN
		   Enum_AMLRisk ON DirectorTrusteeShareHolderBeneficiary.AMLRisk = Enum_AMLRisk.Enum
	WHERE      (DirectorTrusteeShareHolderBeneficiary.Id = @recordId)
END