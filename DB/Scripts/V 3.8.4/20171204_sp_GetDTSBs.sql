-- =============================================
-- 04/12/2017: Christopher: Update Stored procedures for DirectorTrusteeShareHolderBeneficiary; [GetDTSBs]
-- 04/12/2017: Christopher Add AMLRisk
-- =============================================
--USE TestEnquiryWorkSheet
USE EnquiryWorkSheet
GO
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetDTSBs' 
)
   DROP PROCEDURE dbo.GetDTSBs
GO

-- =============================================
CREATE PROCEDURE [dbo].[GetDTSBs]
	@CustomerId int = 0,
	@DTSBtype int = 0
AS
BEGIN
-- =============================================
-- 04/12/2017: Christopher: Update Stored procedures for DirectorTrusteeShareHolderBeneficiary; [GetDTSBs]
-- 04/12/2017: Christopher Add AMLRisk
-- 06/12/2017: Christopher: Add Join to Enum_AMLRisk to provide Description to query
-- =============================================
	SET NOCOUNT ON;

	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.[Type], 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, DirectorTrusteeShareHolderBeneficiary.IsaPEP
			, Enum_DTSB_Type.[Description], DirectorTrusteeShareHolderBeneficiary.AMLRisk, Enum_AMLRisk.[Description] As AMLRiskDesc
	FROM   DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.[Type] = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId INNER JOIN
		   Enum_AMLRisk ON DirectorTrusteeShareHolderBeneficiary.AMLRisk = Enum_AMLRisk.Enum
	WHERE      (DirectorTrusteeShareHolderBeneficiary.CustomerId = @CustomerId) AND (DirectorTrusteeShareHolderBeneficiary.[Type] = @DTSBtype)

END