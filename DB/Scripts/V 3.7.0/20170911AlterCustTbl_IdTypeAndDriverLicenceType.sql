--Add IdType & DriverLicenceType enum fields in Customer table
--11/09/2017: Christopher
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--Customer table
--Add the IdType column to Customer table
IF Not exists(select * from sys.columns 
            where Name = N'IdType' and Object_ID = Object_ID(N'Customer'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE Customer
		ADD IdType smallint Null;
	END
	BEGIN
		PRINT 'Added IdType field to Customer table'
	END	
END
ELSE
BEGIN
	PRINT 'IdType field already exists in Customer table'
END
GO

--Customer table
--Add the IdType column to Customer table
IF Not exists(select * from sys.columns 
            where Name = N'DriverLicenceType' and Object_ID = Object_ID(N'Customer'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE Customer
		ADD DriverLicenceType smallint Null;
	END
	BEGIN
		PRINT 'Added DriverLicenceType field to Customer table'
	END	
END
ELSE
BEGIN
	PRINT 'DriverLicenceType field already exists in Customer table'
END
GO
