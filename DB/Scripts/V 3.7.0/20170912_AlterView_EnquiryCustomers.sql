USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO

/****** Object:  View [dbo].[EnquiryCustomers]    Script Date: 12/09/2017 9:38:22 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[EnquiryCustomers]
--15/06/2016: Create View by Christopher
--12/09/2017: Christopher: Add Driver's Licence Type
AS
SELECT  dbo.CustomerEnquiry.Id_number
		, CASE WHEN dbo.Customer.CustomerType = 0 THEN ISNULL(dbo.Customer.FirstName,'') + ' ' + ISNULL(LastName,'') 
		WHEN dbo.Customer.CustomerType = 1 THEN ISNULL(dbo.Customer.LegalName,ISNULL(dbo.Customer.TradingName,''))
		WHEN dbo.Customer.CustomerType = 2 THEN ISNULL(dbo.Customer.TradingName,'') 
		WHEN dbo.Customer.CustomerType = 3 THEN ISNULL(dbo.Customer.FirstName,'') + ' ' + ISNULL(LastName,'')  END as [Name]
		, dbo.CustomerEnquiry.[Type]
		, dbo.Customer.CustomerType
		, dbo.Customer.CustomerId
		, dbo.CustomerEnquiry.EnquiryId
		, dbo.Customer.ClientTypeId
		, dbo.Customer.ClientNum
		, dbo.Customer.XRefId
		, dbo.Customer.DriverLicenceType
	FROM  dbo.Customer INNER JOIN
		dbo.CustomerEnquiry ON dbo.Customer.CustomerId = dbo.CustomerEnquiry.CustomerId
GO


