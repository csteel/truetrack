--Add IsaPEP bit field in DirectorTrusteeShareHolderBeneficiary table
--03/11/2017: Christopher
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--DirectorTrusteeShareHolderBeneficiary table
--Add the IsaPEP column to DirectorTrusteeShareHolderBeneficiary table
IF Not exists(select * from sys.columns 
            where Name = N'IsaPEP' and Object_ID = Object_ID(N'DirectorTrusteeShareHolderBeneficiary'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE DirectorTrusteeShareHolderBeneficiary
		ADD IsaPEP BIT NOT NULL DEFAULT 0;
	END
	BEGIN
		PRINT 'Added IsaPEP field to DirectorTrusteeShareHolderBeneficiary table'
	END	
END
ELSE
BEGIN
	PRINT 'IsaPEP field already exists in DirectorTrusteeShareHolderBeneficiary table'
END
GO

