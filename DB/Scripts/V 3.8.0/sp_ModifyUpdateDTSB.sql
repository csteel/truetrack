USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[UpdateDTSB]    Script Date: 3/11/2017 12:11:19 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
ALTER PROCEDURE [dbo].[UpdateDTSB]
	@Id int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0,
	@IsaPEP bit = 0
AS
BEGIN
	UPDATE  DirectorTrusteeShareHolderBeneficiary
	SET     FirstName = @FirstName, 
			LastName = @LastName, 			
			DateOfBirth = @DateOfBirth, 
			IdVerified = @IdVerified, 
			AddressVerified = @AddressVerified,
			IsaPEP = @IsaPEP
	WHERE   (Id = @Id)
END