USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[GetDTSBs]    Script Date: 3/11/2017 12:04:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- 03/11/2017 Christopher: Add IsaPEP
-- =============================================
ALTER PROCEDURE [dbo].[GetDTSBs]
	@CustomerId int = 0,
	@DTSBtype int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.Type, 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, DirectorTrusteeShareHolderBeneficiary.IsaPEP, Enum_DTSB_Type.Description
	FROM   DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.Type = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId
	WHERE      (DirectorTrusteeShareHolderBeneficiary.CustomerId = @CustomerId) AND (DirectorTrusteeShareHolderBeneficiary.Type = @DTSBtype)

END