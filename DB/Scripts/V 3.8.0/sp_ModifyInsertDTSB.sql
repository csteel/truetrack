USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[InsertDTSB]    Script Date: 3/11/2017 12:07:59 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- 03/11/2017 Christopher: added IsaPEP
-- =============================================
ALTER PROCEDURE [dbo].[InsertDTSB]	
	@Type int = 0,
	@CustomerId int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0,
	@IsaPEP bit = 0,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO DirectorTrusteeShareHolderBeneficiary ([Type], CustomerId, FirstName, LastName, DateOfBirth, IdVerified, AddressVerified, IsaPEP)
	VALUES(@Type, @CustomerId, @FirstName, @LastName, @DateOfBirth, @IdVerified, @AddressVerified, @IsaPEP)
	--Get back the Id
	SELECT @Id = Id
	FROM DirectorTrusteeShareHolderBeneficiary
	WHERE @@ROWCOUNT > 0 AND Id = SCOPE_IDENTITY()	
END