--Add ForeignFundsTransfer bit field in Payout table
--03/11/2017: Christopher
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--Payout table
--Add the ForeignFundsTransfer column to Payout table
IF Not exists(select * from sys.columns 
            where Name = N'ForeignFundsTransfer' and Object_ID = Object_ID(N'Payout'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE Payout
		ADD ForeignFundsTransfer BIT NOT NULL DEFAULT 0;
	END
	BEGIN
		PRINT 'Added ForeignFundsTransfer field to Payout table'
	END	
END
ELSE
BEGIN
	PRINT 'ForeignFundsTransfer field already exists in Payout table'
END
GO

