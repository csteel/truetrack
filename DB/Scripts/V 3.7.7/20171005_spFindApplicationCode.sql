-- ================================================
-- 05/10/2017: Christopher
-- Find if a Application Code already exists
-- Return 0 (False) or 1 (True)
-- ================================================
USE TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE FindApplicationCode 
	-- Add the parameters for the stored procedure here
	@appCode varchar(14) = '', 
	@enquiryId int = 0,
	@number int = 0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @number = count(*) 
	FROM Enquiry
	WHERE Enquiry.ApplicationCode = @appCode
	AND Enquiry.EnquiryId <> @enquiryId

	RETURN @number
END
GO
