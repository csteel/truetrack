--06/10/2017: Christopher
--=======================
USE TestEnquiryWorkSheet
--USE EnquiryWorkSheet
GO

PRINT'Database script started'
GO

--##########################################
--amend [StatusHistory] table records
--Remove records with [Status] = Completed
--After above then rename [Status] = Exported to Completed

If exists(Select * From StatusHistory
WHERE StatusHistory.Status = 'Exported')
BEGIN
	BEGIN
		Delete StatusHistory 
		WHERE StatusHistory.Status = 'Completed'
	END
	BEGIN
		UPDATE StatusHistory SET
		Status = 'Completed'
		WHERE StatusHistory.Status = 'Exported'
	END
	BEGIN
		PRINT'renamed [Status] = Exported to Completed in StatusHistory'
	END
END
ELSE
BEGIN
	PRINT'No Status = Exported found in StatusHistory'
END
GO

--##########################################
--amend [CurrentStatus] in current Enquiries
--Check if any enquiries currently in 'Completed',if so amend to 'Payout'
--Change 'Exported' to 'Completed'

If exists(Select * From Enquiry
WHERE Enquiry.CurrentStatus = 'Exported')
BEGIN
	BEGIN
		UPDATE Enquiry SET
		CurrentStatus = 'Payout'
		WHERE Enquiry.CurrentStatus = 'Completed'
	END
	BEGIN
		ALTER TABLE Enquiry DISABLE TRIGGER tr_Enquiry_update
	END
	BEGIN
		UPDATE Enquiry SET
		CurrentStatus = 'Completed'
		WHERE Enquiry.CurrentStatus = 'Exported'
	END
	BEGIN
		ALTER TABLE Enquiry ENABLE TRIGGER tr_Enquiry_update
	END
	BEGIN
		PRINT'renamed [CurrentStatus] = Exported to Completed in Enquiry'
	END
END
ELSE
BEGIN
	PRINT'No Status = Exported found in Enquiry'
END
GO

--##########################################
--amend [CurrentStatus] in ArchivedEnquiries
If exists(Select * From ArchiveEnquiry
WHERE ArchiveEnquiry.CurrentStatus = 'Exported')
BEGIN
	BEGIN
		UPDATE ArchiveEnquiry SET
		CurrentStatus = 'Payout'
		WHERE ArchiveEnquiry.CurrentStatus = 'Completed'
	END
	BEGIN
		UPDATE ArchiveEnquiry SET
		CurrentStatus = 'Completed'
		WHERE ArchiveEnquiry.CurrentStatus = 'Exported'
	END
	BEGIN
		PRINT'renamed [CurrentStatus] = Exported to Completed in ArchiveEnquiry'
	END
END
ELSE
BEGIN
	PRINT'No Status = Exported found in ArchiveEnquiry'
END
GO

--##########################################

PRINT'Database script ended'
GO