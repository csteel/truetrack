--Script to change the status from 'Waiting for sign-up' to Waiting for Docs'
--So that TrueTrack and OAC have the same statuses
--Written by Christopher 13/04/2016

USE TestEnquiryWorkSheet
--USE Dev_TrueTrack
--Use EnquiryWorkSheet

Select * from Enquiry
where CurrentStatus like 'Waiting%'
OR Status30mins like 'Waiting%'
GO

Update Enquiry SET
CurrentStatus = 'Waiting for Docs'
WHERE CurrentStatus = 'Waiting Sign-up'
GO

Update Enquiry SET
Status30mins = 'Waiting for Docs'
WHERE Status30mins = 'Waiting Sign-up'

Select * from Enquiry
where CurrentStatus like 'Waiting%'
OR Status30mins like 'Waiting%'
GO