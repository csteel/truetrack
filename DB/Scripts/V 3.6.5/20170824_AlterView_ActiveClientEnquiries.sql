USE [TestEnquiryWorkSheet]
--USE EnquiryWorkSheet
GO

/****** Object:  View [dbo].[ActiveClientEnquiries]    Script Date: 25/08/2017 4:47:38 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[ActiveClientEnquiries]
--11/06/2016 removed dbo.Enquiry.ClientId, dbo.Client.Salutation, dbo.Client.BranchId
--11/06/2016 added dbo.Enquiry.BranchId, dbo.Enquiry.ContactDisplayName
--25/08/2017 added filter by IsArchived field
AS
SELECT TOP (100) PERCENT dbo.Enquiry.EnquiryCode, dbo.Enquiry.ApplicationCode, dbo.Enquiry.ContactDisplayName, dbo.Enquiry.[DateTime]
    ,dbo.Enquiry.ActiveStatus,dbo.Enquiry.BranchId,dbo.Enquiry.EnquiryManagerId,dbo.Users.UserName,dbo.Enquiry.CurrentStatus, dbo.Enquiry.EnquiryId
	FROM         dbo.Enquiry INNER JOIN dbo.Users ON dbo.Enquiry.EnquiryManagerId = dbo.Users.UserId
	WHERE dbo.Enquiry.IsArchived IS NULL OR  dbo.Enquiry.IsArchived = 0
	ORDER BY dbo.Enquiry.[DateTime]



GO


