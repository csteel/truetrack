USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('SystemCanChangeRemoteStatus'
           ,Null
           ,Null
           ,1
           ,'Whether current application can change the Status on 3rd party (OAC)')
GO


