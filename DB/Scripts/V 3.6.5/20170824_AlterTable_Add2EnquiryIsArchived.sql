-- 25/08/2017 by Christopher
--Add new field to tables, Add IsArchived (bit, null)
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

--Enquiry table
--Add the IsArchived column to Enquiry table
IF Not exists(select * from sys.columns 
            where Name = N'IsArchived' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD IsArchived bit Null;
	END
	BEGIN
		PRINT 'Added IsArchived field to Enquiry table'
	END	
END
ELSE
BEGIN
	PRINT 'IsArchived field already exists in Enquiry table'
END
GO

--ArchiveEnquiry table
--Add the IsArchived column to ArchiveEnquiry table
IF Not exists(select * from sys.columns 
            where Name = N'IsArchived' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD IsArchived bit Null;
	END
	BEGIN
		PRINT 'Added IsArchived field to ArchiveEnquiry table'
	END	
END
ELSE
BEGIN
	PRINT 'IsArchived field already exists in ArchiveEnquiry table'
END
GO