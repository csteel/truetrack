--29/08/2017: Christopher: alter retention fields in DueDiligence
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO


--Alter the LoanRetention column in DueDiligence table
if exists(select * from sys.columns 
            where Name = N'LoanRetention' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does Exist
	ALTER TABLE DueDiligence
	ALTER COLUMN LoanRetention Decimal(5,2) Null;		
END
ELSE
BEGIN
	PRINT 'LoanRetention field does not exist in DueDiligence table'
END
GO
--##################################
--Alter the CommRetention column in DueDiligence table
if exists(select * from sys.columns 
            where Name = N'CommRetention' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does Exist	
	ALTER TABLE DueDiligence
	ALTER COLUMN CommRetention Decimal(5,2) Null;	
END
ELSE
BEGIN
	PRINT 'CommRetention field does not exist in DueDiligence table'
END
GO

--BEGIN
--	Select * from DueDiligence where (LoanRetention > 0 OR CommRetention > 0)
--END