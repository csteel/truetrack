USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('SystemCanGetLoanSummary'
           ,Null
           ,Null
           ,1
           ,'Whether current application can import Loan Summary information from 3rd party (OAC)')
GO


