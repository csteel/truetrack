USE [TestEnquiryWorkSheet]
--USE EnquiryWorkSheet
GO

/****** Object:  View [dbo].[DisplayAllNames]    Script Date: 25/08/2017 4:58:41 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[DisplayAllNames]
--30/06/2016 written by Christopher
--altered the view to take intop account the new database schema
--20/12/2016: Christopher: Changed Customer.Suburb & Customer.City to Enquiry.ContactSuburb and Enquiry.ContactCity. Removed WHERE statement so returns ALL customers
--25/08/2017: Christopher: Added Enquiry.IsArchived
AS
--SELECT TOP (100) PERCENT SubData.ContactDisplayName, dbo.Customer.LastName, dbo.Customer.FirstName, dbo.Customer.Suburb, dbo.Customer.City, SubData.[DateTime], SubData.CurrentStatus
--		, SubData.EnquiryCode, SubData.ApplicationCode, SubData.Archived, SubData.EnquiryId
--FROM   (SELECT Enquiry.EnquiryId, Enquiry.EnquiryCode, Enquiry.[DateTime], Enquiry.CurrentStatus, Enquiry.Archived, Enquiry.ApplicationCode
--		, Enquiry.ContactDisplayName, CustomerEnquiry.Type, CustomerEnquiry.CustomerId
--        FROM dbo.Enquiry 
--			INNER JOIN dbo.CustomerEnquiry ON dbo.Enquiry.EnquiryId = dbo.CustomerEnquiry.EnquiryId
--        UNION
--        SELECT ArchiveEnquiry.EnquiryId, ArchiveEnquiry.EnquiryCode, ArchiveEnquiry.[DateTime], ArchiveEnquiry.CurrentStatus, ArchiveEnquiry.Archived, ArchiveEnquiry.ApplicationCode
--		,ArchiveEnquiry.ContactDisplayName, CustomerArchiveEnquiry.Type, CustomerArchiveEnquiry.CustomerId
--        FROM dbo.ArchiveEnquiry 
--			INNER JOIN dbo.CustomerArchiveEnquiry ON dbo.ArchiveEnquiry.EnquiryId = dbo.CustomerArchiveEnquiry.EnquiryId) AS SubData 
--			INNER JOIN dbo.Customer ON SubData.CustomerId = dbo.Customer.CustomerId
--WHERE   (SubData.Type = 0) -- 0 = main
SELECT TOP (100) PERCENT SubData.ContactDisplayName, dbo.Customer.LastName, dbo.Customer.FirstName, SubData.ContactSuburb, SubData.ContactCity, SubData.[DateTime], SubData.CurrentStatus
		, SubData.EnquiryCode, SubData.ApplicationCode, SubData.Archived, SubData.EnquiryId, SubData.IsArchived
FROM   (SELECT Enquiry.EnquiryId, Enquiry.EnquiryCode, Enquiry.[DateTime], Enquiry.CurrentStatus, Enquiry.Archived, Enquiry.ApplicationCode
		, Enquiry.ContactDisplayName, Enquiry.ContactSuburb, Enquiry.ContactCity, CustomerEnquiry.Type, CustomerEnquiry.CustomerId, Enquiry.IsArchived
        FROM dbo.Enquiry 
			LEFT OUTER JOIN dbo.CustomerEnquiry ON dbo.Enquiry.EnquiryId = dbo.CustomerEnquiry.EnquiryId
        UNION
        SELECT ArchiveEnquiry.EnquiryId, ArchiveEnquiry.EnquiryCode, ArchiveEnquiry.[DateTime], ArchiveEnquiry.CurrentStatus, ArchiveEnquiry.Archived, ArchiveEnquiry.ApplicationCode
		,ArchiveEnquiry.ContactDisplayName, ArchiveEnquiry.ContactSuburb, ArchiveEnquiry.ContactCity, CustomerArchiveEnquiry.Type, CustomerArchiveEnquiry.CustomerId, ArchiveEnquiry.IsArchived
        FROM dbo.ArchiveEnquiry 
			LEFT OUTER JOIN dbo.CustomerArchiveEnquiry ON dbo.ArchiveEnquiry.EnquiryId = dbo.CustomerArchiveEnquiry.EnquiryId) AS SubData 
			LEFT OUTER JOIN dbo.Customer ON SubData.CustomerId = dbo.Customer.CustomerId

ORDER BY dbo.Customer.LastName, dbo.Customer.FirstName




GO


