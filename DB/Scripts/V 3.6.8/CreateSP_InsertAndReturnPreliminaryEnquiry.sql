USE [TestEnquiryWorkSheet]
--USE EnquiryWorkSheet
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Christopher
-- Create date: 08/09/2017
-- Description:	Insert new Preliminary Enquiry
-- =============================================

CREATE PROCEDURE [dbo].[InsertAndReturnPreliminaryEnquiry] 
	-- Add the parameters for the stored procedure here
	@EnquiryType smallint = 0,
	@PrelimAmount money = 0,
	@LoanValue money = 0,
	@LoanPurpose varchar(255) = '',
	@DealerId varchar(10) = '',
	@ContactPhone varchar(20) = '',
	@ContactEmail varchar(125) = '',
	@ContactTitle varchar(40) = '',
	@ContactFirstName varchar(40) = '',
	@ContactLastName varchar(40) = '',
	@ContactDisplayName varchar(50) = '',
	@ContactSuburb varchar(50) = '',
	@ContactCity varchar(50) = '',
	@PrelimSource varchar(50) = '',
	@PrelimMethod varchar(30) = '',
	@PrelimResult varchar(100) = '',
	@CurrentStatus varchar(20) = 'Preliminary',
	@Status30mins varchar(20) = 'Preliminary',
	@EnquiryManagerId int = 0,
	@LoggedinBranchId varchar(5) = 'YFMAN',	    
	@StartTime datetime = GETDATE
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Setup Enquiry table
	DECLARE @ActiveStatus bit, @WizardStatus smallint
	Set @ActiveStatus = 1
    Set @WizardStatus = 1
	
    -- Insert statements for Enquiry table
	INSERT INTO Enquiry ([DateTime], Status30mins, CurrentStatus, ActiveStatus, WizardStatus, EnquiryManagerId, CurrentStatusSetDate, BranchId, EnquiryType, PrelimAmount, LoanValue, LoanPurpose, DealerId
	, ContactPhone, ContactEmail, ContactTitle, ContactFirstName, ContactLastName, ContactDisplayName, ContactSuburb, ContactCity, PrelimSource, PrelimMethod, PrelimResult) 
	VALUES (@StartTime, @Status30mins, @CurrentStatus, @ActiveStatus, @WizardStatus, @EnquiryManagerId, @StartTime, @LoggedinBranchId, @EnquiryType, @PrelimAmount, @LoanValue, @LoanPurpose, @DealerId
	,@ContactPhone, @ContactEmail, @ContactTitle, @ContactFirstName, @ContactLastName, @ContactDisplayName, @ContactSuburb, @ContactCity, @PrelimSource, @PrelimMethod, @PrelimResult)
	
	DECLARE @Identity int
	SET @Identity = SCOPE_IDENTITY()
	
	-- Retreive data form table		
	SELECT * FROM Enquiry WHERE EnquiryId = @Identity
	
END