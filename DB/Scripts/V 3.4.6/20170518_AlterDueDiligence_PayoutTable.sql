--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
--18/05/2017
--Add bit field in DueDiligence database table called InsuranceIdentified.
--Add a varchar(500) field Due Diligence database table called SpecialNotes
--Add bit field in Payout database table called AlertDone.
--written by Christopher 18/05/2017

PRINT 'Starting database script'
GO
PRINT '**************************'
GO
PRINT'Alter DueDiligence table, add a field to record InsuranceIdentified'
GO
--add InsuranceIdentified column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'InsuranceIdentified' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add InsuranceIdentified field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD InsuranceIdentified Bit DEFAULT 0 NOT NULL;
	END	
END
ELSE
BEGIN
	PRINT 'InsuranceIdentified field already exists in DueDiligence table'
END
GO
PRINT '**************************'
GO
--##########################################
PRINT'Alter DueDiligence table, add a field to record SpecialNotes'
GO
--add InsuranceIdentified column to DueDiligence table
if NOT exists(select * from sys.columns 
            where Name = N'SpecialNotes' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add SpecialNotes field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD SpecialNotes varchar(500) NULL;
	END	
END
ELSE
BEGIN
	PRINT 'SpecialNotes field already exists in DueDiligence table'
END
GO
PRINT '**************************'
GO
--##########################################
PRINT'Alter Payout table, add a field to record AlertDone'
GO
--add AlertDone column to Payout table
if NOT exists(select * from sys.columns 
            where Name = N'AlertDone' and Object_ID = Object_ID(N'Payout'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add AlertDone field to Payout table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Payout
		ADD AlertDone Bit DEFAULT 0 NOT NULL;
	END	
END
ELSE
BEGIN
	PRINT 'AlertDone field already exists in Payout table'
END
GO
PRINT '**************************'
GO
--End
PRINT 'DB script completed'
GO
