--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
--19/05/2017
--Transform data for new field InsuranceIdentified
--To be run immediately after creating the new field
--If status of Enquiry is Payout or Exported then set InsuranceIdentified to True
--written by Christopher 19/05/2017

PRINT 'Starting database script'
GO
PRINT '**************************'
GO
PRINT'Transform data for new field InsuranceIdentified'
GO
PRINT 'Where CurrentStatus = Payout OR Saved OR Exported'
--Transform data for new InsuranceIdentified column in DueDiligence table
if exists(select * from sys.columns 
            where Name = N'InsuranceIdentified' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column exists
	DECLARE @enquiryId int
	DECLARE @currentStatus varchar(20)
	DECLARE @noProtectaPolicies bit
	DECLARE @enquiryCount int

	SET @enquiryCount = 0	

	DECLARE cur CURSOR LOCAL for
		select e.EnquiryId, e.CurrentStatus, d.NoProtectaPolicies
			From Enquiry e	LEFT JOIN
			DueDiligence d ON d.EnquiryId = e.EnquiryId					
	OPEN cur

	fetch next from cur into @enquiryId, @currentStatus, @noProtectaPolicies
	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row
		
		If @noProtectaPolicies = 0 AND (@currentStatus = 'Payout' OR @currentStatus = 'Saved' OR @currentStatus = 'Exported')
		BEGIN
			PRINT 'EnquiryId ' + CAST(@enquiryId AS VARCHAR)  
			Update [DueDiligence] SET 
			InsuranceIdentified = 1
			WHERE EnquiryId = @EnquiryId				
		END
		--ELSE
		--BEGIN
		--	Update [DueDiligence] SET 
		--	InsuranceIdentified = 0
		--	WHERE EnquiryId = @EnquiryId
		--END
		
		SELECT @enquiryCount = @enquiryCount + 1;	
		fetch next from cur into  @enquiryId, @currentStatus, @noProtectaPolicies

	END

	close cur
	deallocate cur

	PRINT 'Data transformed for ' + CAST(@enquiryCount AS VARCHAR)    + ' enquiries'
	-- finished transforming existing data!!!!! ***************************************************
	
END
ELSE
BEGIN
	PRINT 'InsuranceIdentified field does not exist in DueDiligence table'
END
GO
PRINT '**************************'
GO
--End
PRINT 'DB script completed'
GO
