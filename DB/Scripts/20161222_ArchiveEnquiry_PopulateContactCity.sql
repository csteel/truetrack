Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO
--start ArchiveEnquiry ContactSuburb and ContactCity Information migration
--test if ContactSuburbe field exists
if EXISTS(select * from sys.columns where Name = N'ContactSuburb' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN	
	BEGIN TRY
		--perform migration
		--Contact's fields: ContactSuburb 
		DECLARE @enquiryId int
		DECLARE @ContactSuburb varchar(50)	
		DECLARE @ContactCity varchar(50)
			
		DECLARE @enquiryCount int
	
		SET @enquiryCount = 0	

		DECLARE cur CURSOR LOCAL for
			select ae.EnquiryId, ae.ContactSuburb, ae.ContactCity
				From ArchiveEnquiry ae 		
		OPEN cur

		fetch next from cur into @enquiryId, @ContactSuburb, @ContactCity
		while @@FETCH_STATUS = 0 BEGIN
			--execute  on each row		
			If @ContactSuburb IS NULL
			BEGIN
				BEGIN
					SET @ContactSuburb = (Select c.Suburb from Customer c INNER JOIN
					CustomerArchiveEnquiry cae ON c.CustomerId = cae.CustomerId
					where cae.EnquiryId = 12312 AND cae.Type = 0) --main 
				END
				BEGIN
					SET @ContactCity = (Select c.City from Customer c INNER JOIN
					CustomerArchiveEnquiry cae ON c.CustomerId = cae.CustomerId
					where cae.EnquiryId = @EnquiryId AND cae.Type = 0) --main 
				END
				BEGIN
					PRINT CAST(@enquiryId AS VARCHAR)  
					Update [ArchiveEnquiry] SET 			
					ContactSuburb = @ContactSuburb,			
					ContactCity = @ContactCity			
					WHERE EnquiryId = @EnquiryId		
				END
							
			END
			ELSE
			BEGIN
				PRINT CAST(@enquiryId AS VARCHAR) + ' ArchiveEnquiry.ContactCity already set'
			END
			SELECT @enquiryCount = @enquiryCount + 1;
	
			fetch next from cur into  @enquiryId, @ContactSuburb, @ContactCity

		END

		close cur
		deallocate cur

		PRINT 'Data migrated for ' + CAST(@enquiryId AS VARCHAR)  + ' applications'
		-- finished migrate existing data!!!!! ***************************************************
		PRINT 'Finished migrating Contact Information data for ArchiveEnquiry.'	
	END TRY
	BEGIN CATCH
		--returns the complete original error message as a result set
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage

		--will return the complete original error message as an error message
		DECLARE @ErrorMessage nvarchar(400), @ErrorNumber int, @ErrorSeverity int, @ErrorState int, @ErrorLine int
		SELECT @ErrorMessage = N'Error %d, Line %d, Message: '+ERROR_MESSAGE() + ' ArchiveEnquiry ContactDisplayName and BranchId Information migration for Enquiry ' + CAST(@enquiryId AS VARCHAR),@ErrorNumber = ERROR_NUMBER(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE(),@ErrorLine = ERROR_LINE()
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber,@ErrorLine)
	END CATCH
END
ELSE
BEGIN
	PRINT 'Contact details fields not present in ArchiveEnquiry table'
	PRINT 'Contact Information migration for ArchiveEnquiry aborted' 
END
GO

PRINT '****'
GO