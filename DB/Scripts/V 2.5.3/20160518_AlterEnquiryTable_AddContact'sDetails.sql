--Script to add fields to the Enquiry table'
--Contact's title, firstName, lastName, displayName, suburb, city, phoneNumber, emailAddress, BranchId
--Script to add BranchId field to the ArchiveEnquiry table
--Migrate data to Enquiry and ArchiveEnquiry tables
--Written by Christopher 18/05/2016

--USE TestEnquiryWorkSheet
USE Dev_TrueTrack
--Use EnquiryWorkSheet

PRINT 'Starting database script'
GO
PRINT'Alter Enquiry table,'
GO
PRINT'add fields for Contact''s name, displayName, suburb, city, phoneNumber, emailAddress'
GO
PRINT '**************************'
GO
--add ContactTitle column
if NOT exists(select * from sys.columns 
            where Name = N'ContactTitle' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactTitle field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactTitle varchar(40) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactTitle field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactFirstName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactFirstName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactFirstName field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactFirstName varchar(40) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactFirstName field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactLastName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactLastName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactLastName field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactLastName varchar(40) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactLastName field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactDisplayName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactDisplayName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactDisplayName field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactDisplayName varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactDisplayName field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactSuburb column
if NOT exists(select * from sys.columns 
            where Name = N'ContactSuburb' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactSuburb field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactSuburb varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactSuburb field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactCity column
if NOT exists(select * from sys.columns 
            where Name = N'ContactCity' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactCity field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactCity varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactCity field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactPhone column
if NOT exists(select * from sys.columns 
            where Name = N'ContactPhone' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactPhone field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactPhone varchar(20) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactPhone field already exists in Enquiry table'
END
GO
--***********************************************
--add ContactEmail column
if NOT exists(select * from sys.columns 
            where Name = N'ContactEmail' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactEmail field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactEmail varchar(125) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactEmail field already exists in Enquiry table'
END
GO
--***********************************************
--add BranchId column
if NOT exists(select * from sys.columns 
            where Name = N'BranchId' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add BranchId field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD BranchId varchar(5) NULL
	END		
END
ELSE
BEGIN
	PRINT 'BranchId field already exists in Enquiry table'
END
GO
--***********************************************
PRINT 'Alter Enquiry table finished'
GO
PRINT '**************************'
GO
--***********************************************
PRINT'Start Alter ArchiveEnquiry table,'
GO
PRINT'add fields for BranchId'
GO
PRINT '**************************'
GO
--add ContactTitle column
if NOT exists(select * from sys.columns 
            where Name = N'BranchId' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add BranchId field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD BranchId varchar(5) NULL
	END		
END
ELSE
BEGIN
	PRINT 'BranchId field already exists in ArchiveEnquiry table'
END
GO
--***********************************************
PRINT '**************************'
GO
PRINT 'Alter ArchiveEnquiry table finished'
GO
PRINT '**************************'
GO
PRINT'Start data migration of Contact Information to Enquiry table'
GO
--***********************************************
--start Contact Information migration
--test if Contact details fields exist
if EXISTS(select * from sys.columns where Name = N'ContactLastName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN	
	--perform migration
	--Contact's fields: ContactTitle, ContactFirstName, ContactLastName, ContactDisplayName, ContactSuburb, ContactCity, ContactPhone, ContactEmail
	DECLARE @enquiryId int
	DECLARE @Title varchar(40)
	DECLARE @FirstName varchar(40)
	DECLARE @LastName varchar(40)
	DECLARE @Salutation varchar(50)
	DECLARE @Suburb varchar(50)
	DECLARE @City varchar(50)
	DECLARE @PrelimContact varchar(20)
	DECLARE @ContactLastName varchar(40)
	DECLARE @BranchId varchar(5)		
	DECLARE @enquiryCount int

	SET @enquiryCount = 0	

	DECLARE cur CURSOR LOCAL for
		select c.Title, c.FirstName, c.LastName, c.Salutation, c.Suburb, c.City, c.BranchId, e.PrelimContact, e.EnquiryId, e.ContactLastName
			From Enquiry e LEFT JOIN
			Client c ON c.ClientId = e.ClientId				
	OPEN cur

	fetch next from cur into @Title, @FirstName, @LastName, @Salutation, @Suburb, @City, @BranchId, @PrelimContact, @enquiryId, @ContactLastName
	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row
		If @ContactLastName IS NULL
		BEGIN
			PRINT CAST(@enquiryId AS VARCHAR)  
			Update [Enquiry] SET 
			ContactTitle = @Title,
			ContactFirstName = @FirstName,
			ContactLastName = @LastName,
			ContactDisplayName = @Salutation,
			ContactSuburb = @Suburb,
			ContactCity = @City,
			ContactPhone = @PrelimContact,
			BranchId = @BranchId			
			WHERE EnquiryId = @EnquiryId				
		END
		ELSE
		BEGIN
			PRINT CAST(@enquiryId AS VARCHAR) + ' Enquiry already migrated'
		END
		SELECT @enquiryCount = @enquiryCount + 1;
	
		fetch next from cur into  @Title, @FirstName, @LastName, @Salutation, @Suburb, @City, @BranchId, @PrelimContact, @enquiryId, @ContactLastName

	END

	close cur
	deallocate cur

	PRINT 'Data migrated for ' + CAST(@enquiryId AS VARCHAR)  + ' applications'
	-- finished migrate existing data!!!!! ***************************************************
	PRINT 'Finished migrating data.'	
END
ELSE
BEGIN
	PRINT 'Contact details fields not present in Enquiry table'
	PRINT 'Contact Information migration aborted' 
END
GO
--******************************************************************************
--***********************************************
--start ArchiveEnquiry.BranchId migration
--test if BranchId field exist
if EXISTS(select * from sys.columns where Name = N'BranchId' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN	
	--perform migration
	--fields: BranchId
	DECLARE @enquiryId int	
	DECLARE @BranchId varchar(5)		
	DECLARE @enquiryCount int

	SET @enquiryCount = 0	

	DECLARE cur CURSOR LOCAL for
		select c.BranchId, e.EnquiryId
			From ArchiveEnquiry e LEFT JOIN
			Client c ON c.ClientId = e.ClientId				
	OPEN cur

	fetch next from cur into @BranchId, @enquiryId
	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row
		If @BranchId IS NOT NULL
		BEGIN
			PRINT CAST(@enquiryId AS VARCHAR)  
			Update [ArchiveEnquiry] SET 			
			BranchId = @BranchId			
			WHERE EnquiryId = @EnquiryId				
		END
		ELSE
		BEGIN
			PRINT CAST(@enquiryId AS VARCHAR) + ' Enquiry already migrated'
		END
		SELECT @enquiryCount = @enquiryCount + 1;
	
		fetch next from cur into  @BranchId, @enquiryId

	END

	close cur
	deallocate cur

	PRINT 'Data migrated for ' + CAST(@enquiryId AS VARCHAR)  + ' applications'
	-- finished migrate existing data!!!!! ***************************************************
	PRINT 'Finished migrating data.'	
END
ELSE
BEGIN
	PRINT 'BranchId field not present in ArchiveEnquiry table'
	PRINT 'BranchId migration aborted' 
END
GO
-- Finished
PRINT '**************************'
GO
PRINT 'Migration by DB script completed'
GO
PRINT 'DB script completed'
GO
