use TestEnquiryWorkSheet
GO
--Change LoanRetention and CommRetention fields to Decimal type

If(OBJECT_ID('tempdb..#MyTempTable') Is Not Null)
Begin
    Drop Table #MyTempTable
End

Select * from DueDiligence where (LoanRetention > 0 OR CommRetention > 0)

Select EnquiryId, LoanRetention, CommRetention
into #MyTempTable
from DueDiligence where LoanRetention > 0

Select * from #MyTempTable

--##############################################
--DueDiligence table
--Alter the LoanRetention column in DueDiligence table
if exists(select * from sys.columns 
            where Name = N'LoanRetention' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does Exist
	-- remove LoanRetention Constraints from table
	IF OBJECT_ID('DF_DueDiligence_LoanRetention', 'D') IS NOT NULL 
		BEGIN
			ALTER TABLE [dbo].DueDiligence DROP CONSTRAINT [DF_DueDiligence_LoanRetention]
		END
	BEGIN
		PRINT 'Alter LoanRetention field in DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ALTER COLUMN LoanRetention Decimal(4,2) Null;
	END	
END
ELSE
BEGIN
	PRINT 'LoanRetention field does not exist in DueDiligence table'
END
GO

--Alter the CommRetention column in DueDiligence table
if exists(select * from sys.columns 
            where Name = N'CommRetention' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does Exist
	-- remove CommRetention Constraints from table
	IF OBJECT_ID('DF_DueDiligence_CommRetention', 'D') IS NOT NULL 
		BEGIN
			ALTER TABLE [dbo].DueDiligence DROP CONSTRAINT [DF_DueDiligence_CommRetention]
		END
	BEGIN
		PRINT 'Alter CommRetention field in DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ALTER COLUMN CommRetention Decimal(4,2) Null;
	END	
END
ELSE
BEGIN
	PRINT 'CommRetention field does not exist in DueDiligence table'
END
GO

BEGIN
	Select * from DueDiligence where (LoanRetention > 0 OR CommRetention > 0)
END
GO

If(OBJECT_ID('tempdb..#MyTempTable') Is Not Null)
Begin
    Drop Table #MyTempTable
End