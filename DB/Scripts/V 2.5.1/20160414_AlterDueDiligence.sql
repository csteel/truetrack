--Script to add fields to the Due Diligence table'
--SecurityInsured & NoInsuranceRequired
--Written by Christopher 14/04/2016

USE TestEnquiryWorkSheet
--USE Dev_TrueTrack
--Use EnquiryWorkSheet

PRINT 'Starting database script'
GO
PRINT'Alter DueDiligence table, add fields for Insurances'
GO
PRINT '**************************'
GO
--add SecurityInsured column
if NOT exists(select * from sys.columns 
            where Name = N'SecurityInsured' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add SecurityInsured field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD SecurityInsured bit DEFAULT 0;
	END		
END
ELSE
BEGIN
	PRINT 'SecurityInsured field already exists in DueDiligence table'
END
GO
BEGIN
	PRINT 'Set NULL SecurityInsured fields to 0'
END
BEGIN
	Update DueDiligence SET
	SecurityInsured = 0
	WHERE SecurityInsured IS NULL
END
PRINT '**************************'
GO

--***********************************************
--add NoInsuranceRequired column
if NOT exists(select * from sys.columns 
            where Name = N'NoInsuranceRequired' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add NoInsuranceRequired field to DueDiligence table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD NoInsuranceRequired bit DEFAULT 0;
	END	
END
ELSE
BEGIN
	PRINT 'NoInsuranceRequired field already exists in DueDiligence table'
END
GO
BEGIN
	PRINT 'Set NULL NoInsuranceRequired fields to 0'
END
BEGIN
	Update DueDiligence SET
	NoInsuranceRequired = 0
	WHERE NoInsuranceRequired IS NULL
END
-- Finished
PRINT '**************************'
GO
PRINT 'DB script completed'
GO
