USE [TestEnquiryWorkSheet]
--USE EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[DeleteThisEnquiry]    Script Date: 17/10/2017 2:39:29 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Christopher Steel
-- Create date: 07/06/2011
-- Description:	Delete Enquiry Data
-- =============================================
ALTER PROCEDURE [dbo].[DeleteThisEnquiry] 
--20160818 Christopher: Add parameter @deleteCustomers
--20160818 Christopher: Add code to delete multiple customers
--20171017 Christopher: Modify BEGIN END blocks and add delete Comments & StatusHistory
-- =============================================
	-- Add the parameters for the stored procedure here
	@EnquiryId int,
	@deleteCustomers bit = 1
AS
    -- Declare variables used in error checking.
    DECLARE @error_var int, @rowcount_var int, @custerror_var int, @custrowcount_var int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
    -- Test if record already exits
    SELECT EnquiryId 
    FROM dbo.Enquiry
    WHERE EnquiryId = @EnquiryId
    -- Save the @@ERROR and @@ROWCOUNT values in local variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 1 
    BEGIN
		IF @deleteCustomers = 1
		--Delete Customers
		BEGIN
			BEGIN
				-- Test if customers exits				
				SELECT CustomerId FROM dbo.CustomerEnquiry
				WHERE EnquiryId = @EnquiryId;
				SELECT @custerror_var = @@ERROR, @custrowcount_var = @@ROWCOUNT
				IF @custrowcount_var > 0 
				BEGIN
					BEGIN TRY
						DECLARE @Temp AS TABLE (CustomerId integer NOT NULL); 
						INSERT INTO @Temp (CustomerId)
						SELECT CustomerId FROM dbo.CustomerEnquiry
						WHERE EnquiryId = @EnquiryId;
				
						DECLARE @cust int							
											
						declare cur CURSOR LOCAL for
							SELECT CustomerId FROM @Temp
						open cur
						fetch next from cur into @cust
						WHILE @@FETCH_STATUS = 0 BEGIN
							--execute on each row
							DELETE FROM TradeReferences WHERE CustomerId = @cust
							DELETE FROM DirectorTrusteeShareHolderBeneficiary  WHERE CustomerId = @cust
							DELETE FROM CustomerEnquiry WHERE CustomerId = @cust AND EnquiryId = @EnquiryId
							DELETE FROM Customer WHERE CustomerId = @cust
							
							fetch next from cur into @cust
						END
						close cur
						deallocate cur						
					END TRY			
					BEGIN CATCH
						ROLLBACK TRAN
						PRINT 'Deleting Customer failed'
						RETURN 3
					END CATCH	
				END				
			END
			BEGIN		  
				BEGIN TRY
					BEGIN TRAN				            
						-- Delete from BudgetItems
						DELETE FROM dbo.Budget
						WHERE EnquiryId = @EnquiryId
						-- Delete from SecurityItems
						DELETE FROM dbo.Security
						WHERE EnquiryId = @EnquiryId
						-- Delete from DueDiligence
						DELETE FROM dbo.DueDiligence
						WHERE EnquiryId = @EnquiryId
						-- Delete from Payout
						DELETE FROM dbo.Payout
						WHERE EnquiryId = @EnquiryId
						-- Delete from QRGList
						DELETE FROM dbo.QRGList
						WHERE EnquiryId = @EnquiryId
						-- Delete from Comments
						DELETE FROM dbo.EnquiryComment
						WHERE EnquiryId = @EnquiryId
						-- Delete from StatusHistory
						DELETE FROM dbo.StatusHistory
						WHERE EnquiryId = @EnquiryId
						-- Delete from Enquiry
						DELETE FROM dbo.Enquiry
						WHERE EnquiryId = @EnquiryId                 
					COMMIT TRAN
					RETURN 0
				END TRY	
				BEGIN CATCH
					ROLLBACK TRAN
					PRINT 'Deleting Enquiry failed'
					RETURN 1
				END CATCH
			END
		END
		ELSE --@deleteCustomers != 1
		BEGIN
			BEGIN TRY
				BEGIN TRAN				            
					-- Delete from BudgetItems
					DELETE FROM dbo.Budget
					WHERE EnquiryId = @EnquiryId
					-- Delete from SecurityItems
					DELETE FROM dbo.Security
					WHERE EnquiryId = @EnquiryId
					-- Delete from DueDiligence
					DELETE FROM dbo.DueDiligence
					WHERE EnquiryId = @EnquiryId
					-- Delete from Payout
					DELETE FROM dbo.Payout
					WHERE EnquiryId = @EnquiryId
					-- Delete from QRGList
					DELETE FROM dbo.QRGList
					WHERE EnquiryId = @EnquiryId
					-- Delete from Comments
					DELETE FROM dbo.EnquiryComment
					WHERE EnquiryId = @EnquiryId
					-- Delete from StatusHistory
					DELETE FROM dbo.StatusHistory
					WHERE EnquiryId = @EnquiryId
					-- Delete from Enquiry
					DELETE FROM dbo.Enquiry
					WHERE EnquiryId = @EnquiryId                 
				COMMIT TRAN
				RETURN 0
			END TRY	
			BEGIN CATCH
				ROLLBACK TRAN
				PRINT 'Deleting Enquiry failed'
				RETURN 1
			END CATCH
		END
    END
    ELSE
    BEGIN
        PRINT 'Enquiry does not exist'
        RETURN 2
    END

	