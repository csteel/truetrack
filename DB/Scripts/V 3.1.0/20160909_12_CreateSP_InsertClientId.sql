--USE DevTrueTrack2
USE EnquiryWorkSheet
GO
-- ================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT'Start database script'
GO
PRINT '*** Create/Alter Stored procedure InsertClientId ***'
GO
--******************************************
-- InsertClientId

DECLARE @SQLCreateSP_InsertClientId nvarchar(max) = '
CREATE PROCEDURE [dbo].[InsertClientId] 
-- =============================================
-- Author:		Christopher Steel
-- Create date: 9 September 2016
-- Description:	check Customer table entry exists, if does then update the ClientId details
-- =============================================
	@CustomerId int,
	@ClientNum VARCHAR(8) ='''',
	@ClientTypeId VARCHAR(5) =''''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT * FROM Customer WHERE CustomerId = @CustomerId)
	BEGIN
		UPDATE  Customer
		SET     ClientNum = @ClientNum, 
				ClientTypeId = @ClientTypeId
		WHERE   (CustomerId = @CustomerId)					
	  
		RETURN 1
	END
	ELSE -- Customer does not exist
		RETURN 0   
	
	END'
--******
DECLARE @SQLAlterSP_InsertClientId nvarchar(max) = '
ALTER PROCEDURE [dbo].[InsertClientId] 
-- =============================================
-- Author:		Christopher Steel
-- Create date: 9 September 2016
-- Description:	check Customer table entry exists, if does then update the ClientId details
-- =============================================
	@CustomerId int,
	@ClientNum VARCHAR(8) ='''',
	@ClientTypeId VARCHAR(5) =''''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT * FROM Customer WHERE CustomerId = @CustomerId)
	BEGIN
		UPDATE  Customer
		SET     ClientNum = @ClientNum, 
				ClientTypeId = @ClientTypeId
		WHERE   (CustomerId = @CustomerId)					
	  
		RETURN 1
	END
	ELSE -- Customer does not exist
		RETURN 0   
	
	END'
--******
IF OBJECT_ID(N'dbo.[InsertClientId]', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure InsertClientId'
	END
	BEGIN
		EXEC (@SQLCreateSP_InsertClientId);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure InsertClientId'
	END
	BEGIN
		EXEC (@SQLAlterSP_InsertClientId);
	END
END
GO

