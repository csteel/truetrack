--USE [DevTrueTrack2]
USE EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[InsertNewCustomer]    Script Date: 7/07/2016 3:28:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Christopher Steel
-- Create date: 07/07/2016
-- Description:	Insert new customer record and CustomerEnquiry record
-- =============================================
PRINT'Start database script'
GO
PRINT '*** Create/Alter Stored procedure InsertNewCustomer ***'
GO
DECLARE @SQLCreateSP nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 07/07/2016
-- Description:	Insert new customer record and CustomerEnquiry record
-- =============================================

CREATE PROCEDURE [dbo].[InsertNewCustomer] 
	-- Add the parameters for the stored procedure here
	@EnquiryId int = 0, 
	@CustomerType int,
	@CustomerEnquiryType int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TopIdNum int = 0
	SET @TopIdNum = (SELECT MAX(Id_number) from CustomerEnquiry
				WHERE EnquiryId = @EnquiryId)

	If @TopIdNum IS NULL
	BEGIN
		SET @TopIdNum = 0
	END
	
    -- Insert statements for Customer table
	INSERT INTO Customer (CustomerType) VALUES (@CustomerType)
	
	DECLARE @CustomerIdentity int
	SET @CustomerIdentity = SCOPE_IDENTITY()	
	
	--CustomerEnquiry
	INSERT INTO [CustomerEnquiry](CustomerId,EnquiryId,[Type],Id_number)
		VALUES(@CustomerIdentity,@EnquiryId,@CustomerEnquiryType,(@TopIdNum + 1))

	-- Retreive data form table		
	SELECT * FROM Customer WHERE CustomerId = @CustomerIdentity

END'
--******************************************
DECLARE @SQLAlterSP nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 07/07/2016
-- Description:	Insert new customer record and CustomerEnquiry record
-- =============================================

ALTER PROCEDURE [dbo].[InsertNewCustomer] 
	-- Add the parameters for the stored procedure here
	@EnquiryId int = 0, 
	@CustomerType int,
	@CustomerEnquiryType int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TopIdNum int = 0
	SET @TopIdNum = (SELECT MAX(Id_number) from CustomerEnquiry
				WHERE EnquiryId = @EnquiryId)

	If @TopIdNum IS NULL
	BEGIN
		SET @TopIdNum = 0
	END
	
    -- Insert statements for Customer table
	INSERT INTO Customer (CustomerType) VALUES (@CustomerType)
	
	DECLARE @CustomerIdentity int
	SET @CustomerIdentity = SCOPE_IDENTITY()	
	
	--CustomerEnquiry
	INSERT INTO [CustomerEnquiry](CustomerId,EnquiryId,[Type],Id_number)
		VALUES(@CustomerIdentity,@EnquiryId,@CustomerEnquiryType,(@TopIdNum + 1))

	-- Retreive data form table		
	SELECT * FROM Customer WHERE CustomerId = @CustomerIdentity

END'
--******************************************
IF OBJECT_ID(N'dbo.InsertNewCustomer', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure InsertNewCustomer'
	END
	BEGIN
		EXEC (@SQLCreateSP);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure InsertNewCustomer'
	END
	BEGIN
		EXEC (@SQLAlterSP);
	END
END
GO

PRINT'*********************************************************'
GO
PRINT'Database script finished'
GO


