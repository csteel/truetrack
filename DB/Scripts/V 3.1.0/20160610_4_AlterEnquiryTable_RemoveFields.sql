--***************************************************************************
--Script to remove fields from the Enquiry table'
--Written by Christopher 10/06/2016
--***************************************************************************

--USE Dev_TrueTrack
--Use TestEnquiryWorkSheet
--USE DevTrueTrack2
USE EnquiryWorkSheet
GO

PRINT 'Starting database script'
GO
PRINT'Alter Enquiry table, remove fields from the Enquiry table'
GO
PRINT '**************************'
GO
--check fields have not been removed already
IF exists(select * from sys.columns 
            where Name = N'TenancyLength' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does Exist
	BEGIN
		PRINT 'Removing Tenancy fields from the Enquiry table'
	END
	BEGIN
		-- remove Tenancy Constraints from table
		IF OBJECT_ID('DF_Enquiry_TenancyLength', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_TenancyLength]
			END
		IF OBJECT_ID('DF_Enquiry_TenancyType', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_TenancyType]
			END
		IF OBJECT_ID('DF_Enquiry_TenancySatisfactory', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_TenancySatisfactory]
			END
		IF OBJECT_ID('DF_Enquiry_TenancyEnvironment', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_TenancyEnvironment]
			END
		IF OBJECT_ID('DF_Enquiry_TenancyEstablish', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_TenancyEstablish]
			END
		-- remove Tenancy columns from table	
		ALTER TABLE Enquiry
			DROP COLUMN [TenancyLength], [TenancyType],[TenancySatisfactory],[TenancyEnvironment],[TenancyEstablish]
				
	END	
	BEGIN
		-- remove Joint Name Tenancy Constraints from table
		IF OBJECT_ID('DF_Enquiry_JNTenancyLength', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNTenancyLength]
			END
		IF OBJECT_ID('DF_Enquiry_JNTenancyType', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNTenancyType]
			END
		IF OBJECT_ID('DF_Enquiry_JNTenancySatisfactory', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNTenancySatisfactory]
			END
		IF OBJECT_ID('DF_Enquiry_JNTenancyEnvironment', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNTenancyEnvironment]
			END
		IF OBJECT_ID('DF_Enquiry_JNTenancyEstablish', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNTenancyEstablish]
			END
		-- remove Joint Name Tenancy columns from table	
		ALTER TABLE Enquiry 
			DROP COLUMN [JNTenancyLength], [JNTenancyType],[JNTenancySatisfactory],[JNTenancyEnvironment],[JNTenancyEstablish]
				
	END	
END
GO
--check fields have not been removed already
IF exists(select * from sys.columns 
            where Name = N'EmployerName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does Exist
	BEGIN
		PRINT 'Removing Employment fields from the Enquiry table'
	END
	BEGIN
		-- remove Employment CONSTRAINTS from table
		IF OBJECT_ID('DF_Enquiry_EmployerName', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_EmployerName]
			END
		IF OBJECT_ID('DF_Enquiry_EmployerNotRung', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_EmployerNotRung]
			END
		IF OBJECT_ID('DF_Enquiry_EmployerSpoketoName', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_EmployerSpoketoName]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ1', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ1]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ2', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ2]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ3', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ3]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ4', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ4]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ5', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ5]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ6', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ6]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ7', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ7]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ8', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ8]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ9', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ9]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityQ10', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityQ10]
			END
		IF OBJECT_ID('DF_Enquiry_StabilityComments', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_StabilityComments]
			END
		IF OBJECT_ID('DF_Enquiry_SelfEmployed', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_SelfEmployed]
			END
		IF OBJECT_ID('DF_Enquiry_Beneficiary', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_Beneficiary]
			END
		-- remove Employment columns from table	
		ALTER TABLE Enquiry
		DROP COLUMN [EmployerName], [EmployerRangDate],[EmployerSpoketoName],[StabilityQ1],[StabilityQ2],[StabilityQ3],[StabilityQ4],[StabilityQ5],
			[StabilityQ6],[StabilityQ7],[StabilityQ8],[StabilityQ9],[StabilityQ10],[StabilityComments],[SelfEmployed],[Beneficiary],[EmployerNotRung]				
	END		
	BEGIN
		-- remove Joint Name Employment CONSTRAINTS from table
		IF OBJECT_ID('DF_Enquiry_JNEmployerName', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNEmployerName]
			END
		IF OBJECT_ID('DF_Enquiry_JNEmployerNotRung', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNEmployerNotRung]
			END
		IF OBJECT_ID('DF_Enquiry_JNEmployerSpoketoName', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNEmployerSpoketoName]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ1', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ1]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ2', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ2]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ3', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ3]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ4', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ4]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ5', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ5]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ6', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ6]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ7', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ7]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ8', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ8]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ9', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ9]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityQ10', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityQ10]
			END
		IF OBJECT_ID('DF_Enquiry_JNStabilityComments', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNStabilityComments]
			END
		IF OBJECT_ID('DF_Enquiry_JNSelfEmployed', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNSelfEmployed]
			END
		IF OBJECT_ID('DF_Enquiry_JNBeneficiary', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNBeneficiary]
			END
		-- remove Joint Name Employment columns from table
		ALTER TABLE Enquiry
		DROP COLUMN [JNEmployerName], [JNEmployerRangDate],[JNEmployerSpoketoName],[JNStabilityQ1],[JNStabilityQ2],[JNStabilityQ3],[JNStabilityQ4],[JNStabilityQ5],
			[JNStabilityQ6],[JNStabilityQ7],[JNStabilityQ8],[JNStabilityQ9],[JNStabilityQ10],[JNStabilityComments],[JNSelfEmployed],[JNBeneficiary],[JNEmployerNotRung]
	END	
END
GO
--check fields have not been removed already
IF exists(select * from sys.columns 
            where Name = N'CreditComments' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does Exist
	BEGIN
		PRINT 'Removing Credit fields from the Enquiry table'
	END
	BEGIN
		-- remove Credit Contraints from table
		IF OBJECT_ID('DF_Enquiry_CreditComments', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditComments]
			END
		IF OBJECT_ID('DF_Enquiry_CreditExplanation', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditExplanation]
			END
		IF OBJECT_ID('DF_Enquiry_CreditOther', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditOther]
			END
		IF OBJECT_ID('DF_Enquiry_CreditQuestion1', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditQuestion1]
			END
		IF OBJECT_ID('DF_Enquiry_CreditQuestion2', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditQuestion2]
			END
		IF OBJECT_ID('DF_Enquiry_CreditQuestion3', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditQuestion3]
			END
		IF OBJECT_ID('DF_Enquiry_CreditQuestion4', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditQuestion4]
			END
		IF OBJECT_ID('DF_Enquiry_CreditQuestion5', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CreditQuestion5]
			END
		-- remove Credit columns from table
		ALTER TABLE Enquiry
		DROP COLUMN [CreditComments],[CreditExplanation],[CreditOther],[CreditQuestion1],[CreditQuestion2],[CreditQuestion3],[CreditQuestion4],[CreditQuestion5]
	END	
	BEGIN
		-- remove Joint Name Credit Contraints from table
		IF OBJECT_ID('DF_Enquiry_JNCreditComments', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditComments]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditExplanation', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditExplanation]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditOther', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditOther]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditQuestion1', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditQuestion1]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditQuestion2', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditQuestion2]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditQuestion3', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditQuestion3]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditQuestion4', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditQuestion4]
			END
		IF OBJECT_ID('DF_Enquiry_JNCreditQuestion5', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_JNCreditQuestion5]
			END
		-- remove Joint Name Credit columns from table
		ALTER TABLE Enquiry
		DROP COLUMN [JNCreditComments],[JNCreditExplanation],[JNCreditOther],[JNCreditQuestion1],[JNCreditQuestion2],[JNCreditQuestion3],[JNCreditQuestion4],[JNCreditQuestion5]
	END
END
GO
--check fields have not been removed already
IF exists(select * from sys.columns 
            where Name = N'CompanyRegistered' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does Exist
	BEGIN
		PRINT 'Removing Company Info fields from the Enquiry table'
	END	
	BEGIN
		-- remove Company Info contraints from table
		IF OBJECT_ID('DF_Enquiry_CompanyRegistered', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRegistered]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyReturnsFiled', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyReturnsFiled]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyPaidUpCapital', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyPaidUpCapital]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyShareholders', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyShareholders]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyDirectors', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyDirectors]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyDirectorNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyDirectorNotes]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyShareholderNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyShareholderNotes]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyHistorySatisfactory', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyHistorySatisfactory]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyHistoryNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyHistoryNotes]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyAddressSatisfactory', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyAddressSatisfactory]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyAddressNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyAddressNotes]
			END
		-- remove Company Info columns from table
		ALTER TABLE Enquiry
		DROP COLUMN [CompanyRegistered],[CompanyReturnsFiled],[CompanyPaidUpCapital],[CompanyShareholders],[CompanyDirectors],[CompanyDirectorNotes],[CompanyShareholderNotes],
			[CompanyHistorySatisfactory],[CompanyHistoryNotes],[CompanyAddressSatisfactory],[CompanyAddressNotes]
	END
END
GO
--check fields have not been removed already
IF exists(select * from sys.columns 
            where Name = N'CompanyApplicationFilledOut' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does Exist
	BEGIN
		PRINT 'Removing Company Guarantors & Comments fields from the Enquiry table'
	END	
	BEGIN
		-- remove Company Guarantors & Comments contraints from table
		IF OBJECT_ID('DF_Enquiry_CompanyApplicationFilledOut', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyApplicationFilledOut]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyGuarantorNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyGuarantorNotes]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyAssetNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyAssetNotes]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyOtherNotes', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyOtherNotes]
			END
		-- remove Company Guarantors & Comments columns from table
		ALTER TABLE Enquiry
		DROP COLUMN [CompanyApplicationFilledOut],[CompanyGuarantorNotes],[CompanyAssetNotes],[CompanyOtherNotes]
	END
END
GO
--check fields have not been removed already
IF exists(select * from sys.columns 
            where Name = N'CompanyRef1Name' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does Exist
	BEGIN
		PRINT 'Removing Company References fields from the Enquiry table'
	END	
	BEGIN
		-- remove Company References constraints from table
		IF OBJECT_ID('DF_Enquiry_CompanyRef1Name', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef1Name]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef1Acct', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef1Acct]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef1Spend', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef1Spend]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef1Pay', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef1Pay]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef1Comments', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef1Comments]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef2Name', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef2Name]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef2Acct', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef2Acct]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef2Spend', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef2Spend]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef2Pay', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef2Pay]
			END
		IF OBJECT_ID('DF_Enquiry_CompanyRef2Comments', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_CompanyRef2Comments]
			END
		-- remove Company References columns from table
		ALTER TABLE Enquiry
		DROP COLUMN [CompanyRef1Name],[CompanyRef1Acct],[CompanyRef1Spend],[CompanyRef1Pay],[CompanyRef1Comments],[CompanyRef2Name],[CompanyRef2Acct],[CompanyRef2Spend],
			[CompanyRef2Pay],[CompanyRef2Comments]
	END	
END
GO
--check fields have not been removed already	
if exists(select * from sys.columns 
    where Name = N'ClientId' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
	-- Column does Exist
	BEGIN			
		PRINT 'Delete redundant ClientId field from the Enquiry table'
	END
	BEGIN
		--remove ClientId  constraint from Enquiry table
		IF OBJECT_ID('FK_Enquiry_Client', 'F') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [FK_Enquiry_Client]
			END
		IF OBJECT_ID('DF_Enquiry_ClientId', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].Enquiry DROP CONSTRAINT [DF_Enquiry_ClientId]
			END
		--remove ClientId field from Enquiry table
		ALTER TABLE dbo.Enquiry DROP COLUMN ClientId
	END		
	BEGIN
		PRINT 'Deletion of redundant ClientId field completed'
	END
END
GO
--check fields have not been removed already	
if exists(select * from sys.columns 
    where Name = N'ClientId' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
	-- Column does Exist
	BEGIN			
		PRINT 'Delete redundant ClientId field from the ArchiveEnquiry table'
	END
	BEGIN
		--remove ClientId  constraint from ArchiveEnquiry table
		IF OBJECT_ID('FK_ArchiveEnquiry_Client', 'F') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].ArchiveEnquiry DROP CONSTRAINT [FK_ArchiveEnquiry_Client]
			END
		IF OBJECT_ID('DF_Archive_Enquiry_ClientId', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].ArchiveEnquiry DROP CONSTRAINT [DF_Archive_Enquiry_ClientId]
			END
		--remove ClientId field from Enquiry table
		ALTER TABLE dbo.ArchiveEnquiry DROP COLUMN ClientId
	END		
	BEGIN
		PRINT 'Deletion of redundant ClientId field completed'
	END
END
GO
PRINT '****************************'
GO
PRINT 'Removing fields from the Enquiry table completed'
GO
PRINT '****************************'
GO
--check fields have not been removed already	
if exists(select * from sys.columns 
    where Name = N'ClientNum' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
	-- Column does Exist
	BEGIN			
		PRINT 'Delete redundant ClientNum and ClientTypeId fields from the DueDiligence table'
	END
	BEGIN
		--remove ClientNum and ClientTypeId  constraints from DueDiligence table
		IF OBJECT_ID('DF_DueDiligence_ClientNum', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].DueDiligence DROP CONSTRAINT [DF_DueDiligence_ClientNum]
			END
		IF OBJECT_ID('DF_DueDiligence_Client1TypeId', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].DueDiligence DROP CONSTRAINT [DF_DueDiligence_Client1TypeId]
			END
		IF OBJECT_ID('DF_DueDiligence_ClientNum2', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].DueDiligence DROP CONSTRAINT [DF_DueDiligence_ClientNum2]
			END
		IF OBJECT_ID('DF_DueDiligence_Client2TypeId', 'D') IS NOT NULL 
			BEGIN
				ALTER TABLE [dbo].DueDiligence DROP CONSTRAINT [DF_DueDiligence_Client2TypeId]
			END
		--remove ClientNum and ClientTypeId fields from Enquiry table
		ALTER TABLE dbo.DueDiligence DROP COLUMN ClientNum, Client1TypeId, ClientNum2, Client2TypeId
	END		
	BEGIN
		PRINT 'Deletion of redundant ClientNum and ClientTypeId fields completed'
	END
END
GO
PRINT '****************************'
GO
PRINT 'Removing fields from the DueDiligence table completed'
GO
--********************************** finished
PRINT '****************************'
GO
PRINT'Database script completed'
GO