--USE DevTrueTrack2
USE EnquiryWorkSheet
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT'Start database script'
GO
PRINT '*** Create/Alter Stored procedure DeleteThisCustomer ***'
GO
--******************************************
--DeleteThisCustomer
--******************************************
DECLARE @SQLCreateSP_DeleteThisCustomer nvarchar(max) = '
-- =============================================
-- Author: Christopher Steel
-- Create date: 08/08/2016
-- Description:	Delete a Customer
-- =============================================
CREATE PROCEDURE [dbo].[DeleteThisCustomer] 
	-- Add the parameters for the stored procedure here
	@CustomerId int
AS
    -- Declare variables used in error checking.
    DECLARE @error_var int, @rowcount_var int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    -- Test if record already exits
    SELECT CustomerId 
    FROM dbo.Customer
    WHERE CustomerId = @CustomerId
    -- Save the @@ERROR and @@ROWCOUNT values in local 
	-- variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 1 
        BEGIN
            BEGIN TRY
            BEGIN TRAN
            
	        -- Delete from TradeReferences
            DELETE FROM dbo.TradeReferences
            WHERE CustomerId = @CustomerId
            -- Delete from DirectorTrusteeShareHolderBeneficiary
            DELETE FROM dbo.DirectorTrusteeShareHolderBeneficiary
            WHERE CustomerId = @CustomerId
            -- Delete from CustomerEnquiry
            DELETE FROM dbo.CustomerEnquiry
            WHERE CustomerId = @CustomerId
            -- Delete from CustomerArchiveEnquiry
            DELETE FROM dbo.CustomerArchiveEnquiry
            WHERE CustomerId = @CustomerId
            -- Delete from Customer
            DELETE FROM dbo.Customer
            WHERE CustomerId = @CustomerId              
            
	        COMMIT TRAN
	        RETURN 0
	        END TRY
	
	        BEGIN CATCH
	            ROLLBACK TRAN
	            PRINT ''Deleting Customer failed''
	            RETURN 1
	        END CATCH
        END
    ELSE
        BEGIN
            PRINT ''Customer does not exist''
            RETURN 2
        END'
--******
DECLARE @SQLAlterSP_DeleteThisCustomer nvarchar(max) = '
-- =============================================
-- Author: Christopher Steel
-- Create date: 08/08/2016
-- Description:	Delete a Customer
-- =============================================
ALTER PROCEDURE [dbo].[DeleteThisCustomer] 
	-- Add the parameters for the stored procedure here
	@CustomerId int
AS
    -- Declare variables used in error checking.
    DECLARE @error_var int, @rowcount_var int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    -- Test if record already exits
    SELECT CustomerId 
    FROM dbo.Customer
    WHERE CustomerId = @CustomerId
    -- Save the @@ERROR and @@ROWCOUNT values in local 
	-- variables before they are cleared.
	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
    IF @rowcount_var = 1 
        BEGIN
            BEGIN TRY
            BEGIN TRAN
            
	        -- Delete from TradeReferences
            DELETE FROM dbo.TradeReferences
            WHERE CustomerId = @CustomerId
            -- Delete from DirectorTrusteeShareHolderBeneficiary
            DELETE FROM dbo.DirectorTrusteeShareHolderBeneficiary
            WHERE CustomerId = @CustomerId
            -- Delete from CustomerEnquiry
            DELETE FROM dbo.CustomerEnquiry
            WHERE CustomerId = @CustomerId
            -- Delete from CustomerArchiveEnquiry
            DELETE FROM dbo.CustomerArchiveEnquiry
            WHERE CustomerId = @CustomerId
            -- Delete from Customer
            DELETE FROM dbo.Customer
            WHERE CustomerId = @CustomerId              
            
	        COMMIT TRAN
	        RETURN 0
	        END TRY
	
	        BEGIN CATCH
	            ROLLBACK TRAN
	            PRINT ''Deleting Customer failed''
	            RETURN 1
	        END CATCH
        END
    ELSE
        BEGIN
            PRINT ''Customer does not exist''
            RETURN 2
        END'
--******
IF OBJECT_ID(N'dbo.[DeleteThisCustomer]', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure DeleteThisCustomer'
	END
	BEGIN
		EXEC (@SQLCreateSP_DeleteThisCustomer);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure DeleteThisCustomer'
	END
	BEGIN
		EXEC (@SQLAlterSP_DeleteThisCustomer);
	END
END
GO