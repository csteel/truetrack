--USE [Dev_TrueTrack]
--USE DevTrueTrack2
USE EnquiryWorkSheet
GO

/****** Object:  View [dbo].[ActiveClientEnquiries]    Script Date: 9/06/2016 3:10:10 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[ActiveClientEnquiries]
--11/06/2016 removed dbo.Enquiry.ClientId, dbo.Client.Salutation, dbo.Client.BranchId
--11/06/2016 added dbo.Enquiry.BranchId, dbo.Enquiry.ContactDisplayName
AS
SELECT TOP (100) PERCENT dbo.Enquiry.EnquiryCode, dbo.Enquiry.ApplicationCode, dbo.Enquiry.ContactDisplayName, dbo.Enquiry.[DateTime]
    ,dbo.Enquiry.ActiveStatus,dbo.Enquiry.BranchId,dbo.Enquiry.EnquiryManagerId,dbo.Users.UserName,dbo.Enquiry.CurrentStatus, dbo.Enquiry.EnquiryId
	FROM         dbo.Enquiry INNER JOIN dbo.Users ON dbo.Enquiry.EnquiryManagerId = dbo.Users.UserId
	ORDER BY dbo.Enquiry.[DateTime]


GO


/****** Object:  View [dbo].[ActiveEnquiryClients]    Script Date: 17/08/2016 2:33:56 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[ActiveEnquiryClients]
--17/08/2016 Christopher: Changed Client details to Enquiry Contact details
AS
SELECT     dbo.Enquiry.ContactFirstName, dbo.Enquiry.ContactLastName, dbo.Enquiry.ContactSuburb, dbo.Enquiry.ContactCity, dbo.Enquiry.EnquiryId, dbo.Enquiry.EnquiryCode, dbo.Enquiry.ApplicationCode, 
                      dbo.Enquiry.DateTime, dbo.Enquiry.CurrentStatus
FROM         dbo.Enquiry 
WHERE     (dbo.Enquiry.ActiveStatus = 1)

GO