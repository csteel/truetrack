--Script written by Christopher 25/05/2016
--After creating a new table: Customer we now need to migrate the existing data to the new table.
-- Keep the Enquiry but create a new customer using the new Customer table
-- If has JointName then create a new customer
-- Some data on Enquiry table moved to Customer table
--30/06/2016 Christopher: added Migrate data to Enquiry and ArchiveEnquiry tables 
--21/07/2016 Christopher: added LegalName = @Salutation for Customer so Company will have legal name
--29/07/2016 Christopher: added migration for INSERT INTO TradeReferences
--*********************************
--USE DevTrueTrack2
--USE Dev_TrueTrack
USE EnquiryWorkSheet
GO

PRINT 'Starting DB script'
GO
PRINT '****************************************'
GO
PRINT'Start data migration of Contact Information to Enquiry table'
GO
PRINT '****'
GO
--***********************************************
--start Contact Information migration
--test if Contact details fields exist
if EXISTS(select * from sys.columns where Name = N'ContactLastName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN	
	--perform migration
	--Contact's fields: ContactTitle, ContactFirstName, ContactLastName, ContactDisplayName, ContactSuburb, ContactCity, ContactPhone, ContactEmail
	DECLARE @enquiryId int
	DECLARE @Title varchar(40)
	DECLARE @FirstName varchar(40)
	DECLARE @LastName varchar(40)
	DECLARE @Salutation varchar(50)
	DECLARE @Suburb varchar(50)
	DECLARE @City varchar(50)
	DECLARE @PrelimContact varchar(20)
	DECLARE @ContactLastName varchar(40)
	DECLARE @BranchId varchar(5)		
	DECLARE @enquiryCount int
	
	SET @enquiryCount = 0	

	DECLARE cur CURSOR LOCAL for
		select c.Title, c.FirstName, c.LastName, c.Salutation, c.Suburb, c.City, c.BranchId, e.PrelimContact, e.EnquiryId, e.ContactLastName
			From Enquiry e LEFT JOIN
			Client c ON c.ClientId = e.ClientId				
	OPEN cur

	fetch next from cur into @Title, @FirstName, @LastName, @Salutation, @Suburb, @City, @BranchId, @PrelimContact, @enquiryId, @ContactLastName
	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row
		
		If @ContactLastName IS NULL
		BEGIN
			PRINT CAST(@enquiryId AS VARCHAR)  
			Update [Enquiry] SET 
			ContactTitle = @Title,
			ContactFirstName = @FirstName,
			ContactLastName = @LastName,
			ContactDisplayName = @Salutation,
			ContactSuburb = @Suburb,
			ContactCity = @City,
			ContactPhone = @PrelimContact,
			BranchId = @BranchId			
			WHERE EnquiryId = @EnquiryId				
		END
		ELSE
		BEGIN
			PRINT CAST(@enquiryId AS VARCHAR) + ' Enquiry already migrated'
		END
		SELECT @enquiryCount = @enquiryCount + 1;
	
		fetch next from cur into  @Title, @FirstName, @LastName, @Salutation, @Suburb, @City, @BranchId, @PrelimContact, @enquiryId, @ContactLastName

	END

	close cur
	deallocate cur

	PRINT 'Data migrated for ' + CAST(@enquiryId AS VARCHAR)  + ' applications'
	-- finished migrate existing data!!!!! ***************************************************
	PRINT 'Finished migrating Contact Information data.'	
END
ELSE
BEGIN
	PRINT 'Contact details fields not present in Enquiry table'
	PRINT 'Contact Information migration aborted' 
END
GO
--******************************************************************************
--start ArchiveEnquiry ContactDisplayName and BranchId Information migration
--test if ContactDisplayName field exists
if EXISTS(select * from sys.columns where Name = N'ContactDisplayName' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN	
	BEGIN TRY
		--perform migration
		--Contact's fields: ContactDisplayName
		DECLARE @enquiryId int
		DECLARE @Salutation varchar(50)	
		DECLARE @ContactDisplayName varchar(40)
		DECLARE @BranchId varchar(5)		
		DECLARE @enquiryCount int
	
		SET @enquiryCount = 0	

		DECLARE cur CURSOR LOCAL for
			select c.Salutation, c.BranchId, ae.EnquiryId, ae.ContactDisplayName
				From ArchiveEnquiry ae LEFT JOIN
				Client c ON c.ClientId = ae.ClientId				
		OPEN cur

		fetch next from cur into @Salutation, @BranchId, @enquiryId, @ContactDisplayName
		while @@FETCH_STATUS = 0 BEGIN
			--execute  on each row
		
			If @ContactDisplayName IS NULL
			BEGIN
				PRINT CAST(@enquiryId AS VARCHAR)  
				Update [ArchiveEnquiry] SET 			
				ContactDisplayName = @Salutation,			
				BranchId = @BranchId			
				WHERE EnquiryId = @EnquiryId				
			END
			ELSE
			BEGIN
				PRINT CAST(@enquiryId AS VARCHAR) + ' ArchiveEnquiry already migrated'
			END
			SELECT @enquiryCount = @enquiryCount + 1;
	
			fetch next from cur into  @Salutation, @BranchId, @enquiryId, @ContactDisplayName

		END

		close cur
		deallocate cur

		PRINT 'Data migrated for ' + CAST(@enquiryId AS VARCHAR)  + ' applications'
		-- finished migrate existing data!!!!! ***************************************************
		PRINT 'Finished migrating Contact Information data for ArchiveEnquiry.'	
	END TRY
	BEGIN CATCH
		--returns the complete original error message as a result set
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage

		--will return the complete original error message as an error message
		DECLARE @ErrorMessage nvarchar(400), @ErrorNumber int, @ErrorSeverity int, @ErrorState int, @ErrorLine int
		SELECT @ErrorMessage = N'Error %d, Line %d, Message: '+ERROR_MESSAGE() + ' ArchiveEnquiry ContactDisplayName and BranchId Information migration for Enquiry ' + CAST(@enquiryId AS VARCHAR),@ErrorNumber = ERROR_NUMBER(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE(),@ErrorLine = ERROR_LINE()
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber,@ErrorLine)
	END CATCH
END
ELSE
BEGIN
	PRINT 'Contact details fields not present in ArchiveEnquiry table'
	PRINT 'Contact Information migration for ArchiveEnquiry aborted' 
END
GO
-- Finished: takes about 25 seconds to complete to this stage.
PRINT '****'
GO
--***************************************************
--Migrate Customer values from Enquiry and Client tables to new Customer table
PRINT 'Migrate Customer values from Enquiry and Client tables to new Customer table'
GO
-- migrate existing data!!!!! ***************************************************
BEGIN TRY
	DECLARE @applicationCount INT

	--Existing Enquiry table
	DECLARE @EnquiryId int
	--DECLARE @ClientId int
	DECLARE @TenancyLength varchar(20)
	DECLARE @TenancyType varchar(30)
	DECLARE @TenancySatisfactory varchar(max)
	DECLARE @TenancyEnvironment varchar(max)
	DECLARE @TenancyEstablish varchar(max)
	DECLARE @EmployerName varchar(60)
	DECLARE @EmployerRangDate smalldatetime
	DECLARE @EmployerSpoketoName varchar(60)
	DECLARE @StabilityQ1 varchar(50)
	DECLARE @StabilityQ2 varchar(50)
	DECLARE @StabilityQ3 varchar(50)
	DECLARE @StabilityQ4 varchar(50)
	DECLARE @StabilityQ5 varchar(50)
	DECLARE @StabilityQ6 varchar(50)
	DECLARE @StabilityQ7 varchar(50)
	DECLARE @StabilityQ8 varchar(50)
	DECLARE @StabilityQ9 varchar(50)
	DECLARE @StabilityQ10 varchar(50)
	DECLARE @StabilityComments varchar(max)
	DECLARE @SelfEmployed bit
	DECLARE @Beneficiary bit
	DECLARE @EmployerNotRung bit
	DECLARE @CreditComments varchar(max)
	DECLARE @CreditExplanation varchar(max)
	DECLARE @CreditOther varchar(max)
	DECLARE @CreditQuestion1 bit
	DECLARE @CreditQuestion2 bit
	DECLARE @CreditQuestion3 bit
	DECLARE @CreditQuestion4 bit
	DECLARE @CreditQuestion5 bit
	--Joint Name
	DECLARE @JNTenancyLength varchar(20)
	DECLARE @JNTenancyType varchar(30)
	DECLARE @JNTenancySatisfactory varchar(max)
	DECLARE @JNTenancyEnvironment varchar(max)
	DECLARE @JNTenancyEstablish varchar(max)
	DECLARE @JNEmployerName varchar(60)
	DECLARE @JNEmployerRangDate smalldatetime
	DECLARE @JNEmployerSpoketoName varchar(60)
	DECLARE @JNStabilityQ1 varchar(50)
	DECLARE @JNStabilityQ2 varchar(50)
	DECLARE @JNStabilityQ3 varchar(50)
	DECLARE @JNStabilityQ4 varchar(50)
	DECLARE @JNStabilityQ5 varchar(50)
	DECLARE @JNStabilityQ6 varchar(50)
	DECLARE @JNStabilityQ7 varchar(50)
	DECLARE @JNStabilityQ8 varchar(50)
	DECLARE @JNStabilityQ9 varchar(50)
	DECLARE @JNStabilityQ10 varchar(50)
	DECLARE @JNStabilityComments varchar(max)
	DECLARE @JNSelfEmployed bit
	DECLARE @JNBeneficiary bit
	DECLARE @JNEmployerNotRung bit
	DECLARE @JNCreditComments varchar(max)
	DECLARE @JNCreditExplanation varchar(max)
	DECLARE @JNCreditOther varchar(max)
	DECLARE @JNCreditQuestion1 bit
	DECLARE @JNCreditQuestion2 bit
	DECLARE @JNCreditQuestion3 bit
	DECLARE @JNCreditQuestion4 bit
	DECLARE @JNCreditQuestion5 bit
	--Company
	DECLARE @BusinessRegistered bit
	DECLARE @BusinessReturnsFiled bit
	DECLARE @BusinessPaidUpCapital bit
	DECLARE @BusinessShareholders bit
	DECLARE @BusinessDirectors bit
	DECLARE @BusinessDirectorNotes varchar(1024)
	DECLARE @BusinessShareholderNotes varchar(1024)
	DECLARE @BusinessHistorySatisfactory bit
	DECLARE @BusinessHistoryNotes varchar(1024)
	DECLARE @BusinessAddressSatisfactory bit
	DECLARE @BusinessAddressNotes varchar(1024)
	DECLARE @BusinessGuarantorID bit
	DECLARE @BusinessApplicationFilledOut bit
	DECLARE @BusinessGuarantorNotes varchar(1024)
	DECLARE @BusinessAssetNotes varchar(1024)
	DECLARE @BusinessGuarantorTenancyType varchar(30)
	DECLARE @BusinessGuarantorTenancy varchar(1024)
	DECLARE @BusinessTenancy varchar(20)
	DECLARE @BusinessTenancyLength varchar(20)
	DECLARE @BusinessTenancyNotes varchar(1024)
	DECLARE @BusinessOtherNotes varchar(1024)
	--Business Ref
	DECLARE @BusinessRef1Name varchar(100)
	DECLARE @BusinessRef1Acct varchar(50)
	DECLARE @BusinessRef1Spend varchar(20)
	DECLARE @BusinessRef1Pay varchar(50)
	DECLARE @BusinessRef1Comments varchar(1024)
	DECLARE @BusinessRef2Name varchar(100)
	DECLARE @BusinessRef2Acct varchar(50)
	DECLARE @BusinessRef2Spend varchar(20)
	DECLARE @BusinessRef2Pay varchar(50)
	DECLARE @BusinessRef2Comments varchar(1024)
	--End of Existing Enquiry table
	--******************************
	--Existing Client table
	DECLARE @ClientId int
	DECLARE @Type varchar(40)
	DECLARE @EnumType int
	DECLARE @Title varchar(40)
	DECLARE @FirstName varchar(40)
	DECLARE @MiddleNames varchar(60)
	DECLARE @LastName varchar(40)
	DECLARE @Salutation varchar(50)
	--DECLARE @DateofBirth smalldatetime
	--DECLARE @Gender varchar(10)
	--DECLARE @MaritalStatus varchar(20)
	--DECLARE @Occupation varchar(50)
	--DECLARE @ClientTypeId varchar(5)
	DECLARE @BranchId varchar(5)
	--DECLARE @UserField1 varchar(50)
	DECLARE @Address varchar(100)
	DECLARE @Suburb varchar(50)
	DECLARE @City varchar(50)
	DECLARE @Postcode varchar(10)
	--DECLARE @PAAddress varchar(100)
	--DECLARE @PASuburb varchar(50)
	--DECLARE @PACity varchar(50)
	--DECLARE @PAPostcode varchar(10)
	DECLARE @IDCurrent bit
	DECLARE @IDPerson varchar(255)
	DECLARE @ResStatusAccept varchar(255)
	DECLARE @IDLegitimate varchar(512)
	DECLARE @AMLRisk smallint
	--Joint Name
	DECLARE @JNTitle varchar(40)
	DECLARE @JNFirstName varchar(40)
	DECLARE @JNMiddleNames varchar(60)
	DECLARE @JNLastName varchar(40)
	DECLARE @JNSalutation varchar(50)
	--DECLARE @JNDateofBirth smalldatetime
	--DECLARE @JNGender varchar(10)
	--DECLARE @JNMaritalStatus varchar(20)
	--DECLARE @JNOccupation varchar(50)
	DECLARE @JNAddress varchar(100)
	DECLARE @JNSuburb varchar(50)
	DECLARE @JNCity varchar(50)
	DECLARE @JNPostcode varchar(10)
	DECLARE @JNIDCurrent bit
	DECLARE @JNIDPerson varchar(255)
	DECLARE @JNResStatusAccept varchar(255)
	DECLARE @JNIDLegitimate varchar(512)
	DECLARE @JNAMLRisk smallint
	--End of Existing Client table
	--******************************
	--Existing DueDiligence table
	DECLARE @ClientNum varchar(8)
	DECLARE @ClientNum2 varchar(8)
	DECLARE @Client1TypeId varchar(5)
	DECLARE @Client2TypeId varchar(5)
	--End of Existing DueDiligence table
	DECLARE @ClientIdentity int
	SET @applicationCount = 0

	declare cur CURSOR LOCAL for
	SELECT Enquiry.EnquiryId,Enquiry.TenancyLength,Enquiry.TenancyType,Enquiry.TenancySatisfactory,Enquiry.TenancyEnvironment,Enquiry.TenancyEstablish
		  ,Enquiry.EmployerName,Enquiry.EmployerRangDate,Enquiry.EmployerSpoketoName,Enquiry.StabilityQ1,Enquiry.StabilityQ2,Enquiry.StabilityQ3,Enquiry.StabilityQ4,Enquiry.StabilityQ5,Enquiry.StabilityQ6
		  ,Enquiry.StabilityQ7,Enquiry.StabilityQ8,Enquiry.StabilityQ9,Enquiry.StabilityQ10,Enquiry.StabilityComments,Enquiry.SelfEmployed,Enquiry.Beneficiary,Enquiry.EmployerNotRung	  
		  ,Enquiry.CreditComments,Enquiry.CreditExplanation,Enquiry.CreditOther,Enquiry.CreditQuestion1,Enquiry.CreditQuestion2,Enquiry.CreditQuestion3,Enquiry.CreditQuestion4,Enquiry.CreditQuestion5
		  --Joint Name
		  ,Enquiry.JNTenancyLength,Enquiry.JNTenancyType,Enquiry.JNTenancySatisfactory,Enquiry.JNTenancyEnvironment,Enquiry.JNTenancyEstablish
		  ,Enquiry.JNEmployerName,Enquiry.JNEmployerRangDate,Enquiry.JNEmployerSpokeToName,Enquiry.JNStabilityQ1,Enquiry.JNStabilityQ2,Enquiry.JNStabilityQ3,Enquiry.JNStabilityQ4,Enquiry.JNStabilityQ5
		  ,Enquiry.JNStabilityQ6,Enquiry.JNStabilityQ7,Enquiry.JNStabilityQ8,Enquiry.JNStabilityQ9,Enquiry.JNStabilityQ10,Enquiry.JNStabilityComments,Enquiry.JNSelfEmployed,Enquiry.JNBeneficiary,Enquiry.JNEmployerNotRung
		  ,JNCreditComments,Enquiry.JNCreditExplanation,Enquiry.JNCreditOther,Enquiry.JNCreditQuestion1,Enquiry.JNCreditQuestion2,Enquiry.JNCreditQuestion3,Enquiry.JNCreditQuestion4,Enquiry.JNCreditQuestion5
		  --Company
		  ,Enquiry.CompanyRegistered,Enquiry.CompanyReturnsFiled,Enquiry.CompanyPaidUpCapital,Enquiry.CompanyShareholders,Enquiry.CompanyDirectors,Enquiry.CompanyDirectorNotes
		  ,Enquiry.CompanyShareholderNotes,Enquiry.CompanyHistorySatisfactory,Enquiry.CompanyHistoryNotes,Enquiry.CompanyAddressSatisfactory,Enquiry.CompanyAddressNotes
		  ,Enquiry.CompanyGuarantorID,Enquiry.CompanyApplicationFilledOut,Enquiry.CompanyGuarantorNotes,Enquiry.CompanyAssetNotes,Enquiry.CompanyGuarantorTenancyType
		  ,Enquiry.CompanyGuarantorTenancy,Enquiry.CompanyTenancy,Enquiry.CompanyTenancyLength,Enquiry.CompanyTenancyNotes,Enquiry.CompanyOtherNotes
		  --Company Ref
		  ,Enquiry.CompanyRef1Name,Enquiry.CompanyRef1Acct,Enquiry.CompanyRef1Spend,Enquiry.CompanyRef1Pay,Enquiry.CompanyRef1Comments
		  ,Enquiry.CompanyRef2Name,Enquiry.CompanyRef2Acct,Enquiry.CompanyRef2Spend,Enquiry.CompanyRef2Pay,Enquiry.CompanyRef2Comments
		  --Client
		  ,Client.ClientId,Client.[Type],Client.Title,Client.FirstName,Client.MiddleNames,Client.LastName,Client.Salutation
		  --,Client.DateofBirth,Client.Gender,Client.MaritalStatus,Client.Occupation
		  ,Client.BranchId
		  --,Client.UserField1
		  ,Client.[Address],Client.Suburb,Client.City,Client.Postcode
		  --,Client.PAAddress,Client.PASuburb,Client.PACity,Client.PAPostcode
		  ,Client.IDCurrent,Client.IDPerson,Client.ResStatusAccept,Client.IDLegitimate,Client.AMLRisk
		  --Joint Name
		  ,Client.JNTitle,Client.JNFirstName,Client.JNMiddleNames,Client.JNLastName,Client.JNSalutation 
		  --,Client.JNDateofBirth,Client.JNGender,Client.JNMaritalStatus,Client.JNOccupation
		  ,Client.JNAddress,Client.JNSuburb,Client.JNCity,Client.JNPostcode	  
		  ,Client.JNIDCurrent,Client.JNIDPerson,Client.JNResStatusAccept,Client.JNIDLegitimate,Client.JNAMLRisk
		  --DueDiligence
		  ,dbo.DueDiligence.ClientNum,dbo.DueDiligence.ClientNum2,dbo.DueDiligence.Client1TypeId,dbo.DueDiligence.Client2TypeId
	  FROM [dbo].[Enquiry] INNER JOIN 
	  dbo.Client ON dbo.Enquiry.ClientId = dbo.Client.ClientId LEFT OUTER JOIN
	  dbo.DueDiligence ON dbo.Enquiry.EnquiryId = dbo.DueDiligence.EnquiryId
	
	open cur

	fetch next from cur into 	@EnquiryId,@TenancyLength,@TenancyType,@TenancySatisfactory,@TenancyEnvironment,@TenancyEstablish
			,@EmployerName,@EmployerRangDate,@EmployerSpoketoName,@StabilityQ1,@StabilityQ2,@StabilityQ3,@StabilityQ4,@StabilityQ5,@StabilityQ6,@StabilityQ7,@StabilityQ8,@StabilityQ9,@StabilityQ10
			,@StabilityComments,@SelfEmployed,@Beneficiary,@EmployerNotRung,@CreditComments,@CreditExplanation,@CreditOther,@CreditQuestion1,@CreditQuestion2,@CreditQuestion3,@CreditQuestion4,@CreditQuestion5
			--joint Name
			,@JNTenancyLength,@JNTenancyType,@JNTenancySatisfactory,@JNTenancyEnvironment,@JNTenancyEstablish,@JNEmployerName,@JNEmployerRangDate,@JNEmployerSpoketoName
			,@JNStabilityQ1,@JNStabilityQ2,@JNStabilityQ3,@JNStabilityQ4,@JNStabilityQ5,@JNStabilityQ6,@JNStabilityQ7,@JNStabilityQ8,@JNStabilityQ9,@JNStabilityQ10,@JNStabilityComments
			,@JNSelfEmployed,@JNBeneficiary,@JNEmployerNotRung,@JNCreditComments,@JNCreditExplanation,@JNCreditOther,@JNCreditQuestion1,@JNCreditQuestion2,@JNCreditQuestion3,@JNCreditQuestion4,@JNCreditQuestion5
			--Company
			,@BusinessRegistered,@BusinessReturnsFiled,@BusinessPaidUpCapital,@BusinessShareholders,@BusinessDirectors,@BusinessDirectorNotes,@BusinessShareholderNotes,@BusinessHistorySatisfactory,@BusinessHistoryNotes
			,@BusinessAddressSatisfactory,@BusinessAddressNotes,@BusinessGuarantorID,@BusinessApplicationFilledOut,@BusinessGuarantorNotes,@BusinessAssetNotes,@BusinessGuarantorTenancyType,@BusinessGuarantorTenancy
			,@BusinessTenancy,@BusinessTenancyLength,@BusinessTenancyNotes,@BusinessOtherNotes
			--Business Ref
			,@BusinessRef1Name,@BusinessRef1Acct,@BusinessRef1Spend,@BusinessRef1Pay,@BusinessRef1Comments
			,@BusinessRef2Name,@BusinessRef2Acct,@BusinessRef2Spend,@BusinessRef2Pay,@BusinessRef2Comments
		  --Client
			,@ClientId,@Type,@Title,@FirstName,@MiddleNames,@LastName,@Salutation	 
			,@BranchId
			,@Address,@Suburb,@City,@Postcode
			,@IDCurrent,@IDPerson,@ResStatusAccept,@IDLegitimate,@AMLRisk
		  --Joint Name
			,@JNTitle,@JNFirstName,@JNMiddleNames,@JNLastName,@JNSalutation 
			,@JNAddress,@JNSuburb,@JNCity,@JNPostcode	  
			,@JNIDCurrent,@JNIDPerson,@JNResStatusAccept,@JNIDLegitimate,@JNAMLRisk
		  --DueDiligence
		  ,@ClientNum,@ClientNum2,@Client1TypeId,@Client2TypeId

	while @@FETCH_STATUS = 0 BEGIN
		PRINT CAST(@EnquiryId AS VARCHAR)
		--execute  on each row
		SET @EnumType = CASE @Type --added 06/07/2016 Christopher
			WHEN 'Joint' THEN 0
			WHEN 'Individual' THEN 0
			WHEN 'Company' THEN 1
			WHEN 'Trust' THEN 2
			WHEN 'SoleTrader' THEN 3
			END

		--Customer
		INSERT INTO [Customer](CustomerType,LegalName--,TradingName,CompanyNumber,CompanyOfficers
			,Title,FirstName,MiddleNames,LastName,Salutation,BranchId,[Address],Suburb,City,Postcode
			,IDCurrent,IDPerson,ResStatusAccept,IDLegitimate,AMLRisk,ClientTypeId,ClientNum
			,TenancyLength,TenancyType,TenancySatisfactory,TenancyEnvironment,TenancyEstablish
			,EmployerName,EmployerRangDate,EmployerSpoketoName
			,StabilityQ1,StabilityQ2,StabilityQ3,StabilityQ4,StabilityQ5,StabilityQ6,StabilityQ7,StabilityQ8,StabilityQ9,StabilityQ10,StabilityComments
			,SelfEmployed,Beneficiary,EmployerNotRung,CreditComments,CreditExplanation,CreditOther,CreditQuestion1,CreditQuestion2,CreditQuestion3,CreditQuestion4,CreditQuestion5
			,BusinessRegistered,BusinessReturnsFiled,BusinessPaidUpCapital,BusinessShareholders,BusinessDirectors,BusinessDirectorNotes,BusinessShareholderNotes
			,BusinessHistorySatisfactory,BusinessHistoryNotes,BusinessAddressSatisfactory,BusinessAddressNotes
			,BusinessApplicationFilledOut,BusinessGuarantorNotes,BusinessAssetNotes,BusinessOtherNotes
			--,BusinessRef1Name,BusinessRef1Acct,BusinessRef1Spend,BusinessRef1Pay,BusinessRef1Comments,BusinessRef2Name,BusinessRef2Acct,BusinessRef2Spend,BusinessRef2Pay,BusinessRef2Comments
			)
			VALUES (@EnumType,@Salutation--,@TradingName,@LegalName,@CompanyNumber,@CompanyOfficers
			,@Title,@FirstName,@MiddleNames,@LastName,@Salutation,@BranchId,@Address,@Suburb,@City,@Postcode
			,@IDCurrent,@IDPerson,@ResStatusAccept,@IDLegitimate,@AMLRisk,@Client1TypeId,@ClientNum
			,@TenancyLength,@TenancyType,@TenancySatisfactory,@TenancyEnvironment,@TenancyEstablish
			,@EmployerName,@EmployerRangDate,@EmployerSpoketoName
			,@StabilityQ1,@StabilityQ2,@StabilityQ3,@StabilityQ4,@StabilityQ5,@StabilityQ6,@StabilityQ7,@StabilityQ8,@StabilityQ9,@StabilityQ10,@StabilityComments
			,@SelfEmployed,@Beneficiary,@EmployerNotRung,@CreditComments,@CreditExplanation,@CreditOther,@CreditQuestion1,@CreditQuestion2,@CreditQuestion3,@CreditQuestion4,@CreditQuestion5
			,@BusinessRegistered,@BusinessReturnsFiled,@BusinessPaidUpCapital,@BusinessShareholders,@BusinessDirectors,@BusinessDirectorNotes,@BusinessShareholderNotes
			,@BusinessHistorySatisfactory,@BusinessHistoryNotes,@BusinessAddressSatisfactory,@BusinessAddressNotes
			,@BusinessApplicationFilledOut,@BusinessGuarantorNotes,@BusinessAssetNotes,@BusinessOtherNotes
			--,@BusinessRef1Name,@BusinessRef1Acct,@BusinessRef1Spend,@BusinessRef1Pay,@BusinessRef1Comments,@BusinessRef2Name,@BusinessRef2Acct,@BusinessRef2Spend,@BusinessRef2Pay,@BusinessRef2Comments
			);

			SET @ClientIdentity = SCOPE_IDENTITY()

		--CustomerEnquiry
		INSERT INTO [CustomerEnquiry](CustomerId,EnquiryId,[Type],Id_number)
			VALUES(@ClientIdentity,@EnquiryId,0,1)

		--update Enquiry (mainCustomerType)
		UPDATE Enquiry SET dbo.Enquiry.mainCustomerType = @EnumType
		WHERE dbo.Enquiry.EnquiryId = @EnquiryId

		--Trade References
		IF ISNULL(@BusinessRef1Name,'') <> ''
		BEGIN
			INSERT INTO	[TradeReferences]([CustomerId],[BusinessName],[Account],[Spend],[Pay],[Comments])
			VALUES (@ClientIdentity,SUBSTRING(@BusinessRef1Name,1,50),@BusinessRef1Acct,@BusinessRef1Spend,@BusinessRef1Pay,@BusinessRef1Comments)	
		END
		IF ISNULL(@BusinessRef2Name,'') <> ''
		BEGIN
			INSERT INTO	[TradeReferences]([CustomerId],[BusinessName],[Account],[Spend],[Pay],[Comments])
			VALUES (@ClientIdentity,SUBSTRING(@BusinessRef2Name,1,50),@BusinessRef2Acct,@BusinessRef2Spend,@BusinessRef2Pay,@BusinessRef2Comments)	
		END

		--Joint Customer
		If ISNULL(@JNLastName,'') <> ''
		BEGIN		
			BEGIN
				INSERT INTO [Customer](CustomerType--,TradingName,LegalName,CompanyNumber,CompanyOfficers
				,Title,FirstName,MiddleNames,LastName,Salutation,BranchId,[Address],Suburb,City,Postcode
				,IDCurrent,IDPerson,ResStatusAccept,IDLegitimate,AMLRisk,ClientTypeId,ClientNum
				,TenancyLength,TenancyType,TenancySatisfactory,TenancyEnvironment,TenancyEstablish
				,EmployerName,EmployerRangDate,EmployerSpoketoName
				,StabilityQ1,StabilityQ2,StabilityQ3,StabilityQ4,StabilityQ5,StabilityQ6,StabilityQ7,StabilityQ8,StabilityQ9,StabilityQ10,StabilityComments
				,SelfEmployed,Beneficiary,EmployerNotRung,CreditComments,CreditExplanation,CreditOther,CreditQuestion1,CreditQuestion2,CreditQuestion3,CreditQuestion4,CreditQuestion5)
				VALUES (@EnumType--,@TradingName,@LegalName,@CompanyNumber,@CompanyOfficers
				,@JNTitle,@JNFirstName,@JNMiddleNames,@JNLastName,@JNSalutation,@BranchId,@JNAddress,@JNSuburb,@JNCity,@JNPostcode
				,@JNIDCurrent,@JNIDPerson,@JNResStatusAccept,@JNIDLegitimate,@JNAMLRisk,@Client2TypeId,@ClientNum2
				,@JNTenancyLength,@JNTenancyType,@JNTenancySatisfactory,@JNTenancyEnvironment,@JNTenancyEstablish
				,@JNEmployerName,@JNEmployerRangDate,@JNEmployerSpoketoName
				,@JNStabilityQ1,@JNStabilityQ2,@JNStabilityQ3,@JNStabilityQ4,@JNStabilityQ5,@JNStabilityQ6,@JNStabilityQ7,@JNStabilityQ8,@JNStabilityQ9,@JNStabilityQ10,@JNStabilityComments
				,@JNSelfEmployed,@JNBeneficiary,@JNEmployerNotRung,@JNCreditComments,@JNCreditExplanation,@JNCreditOther,@JNCreditQuestion1,@JNCreditQuestion2,@JNCreditQuestion3,@JNCreditQuestion4,@JNCreditQuestion5)

				SET @ClientIdentity = SCOPE_IDENTITY()
			END
			BEGIN
				INSERT INTO [CustomerEnquiry](CustomerId,EnquiryId,[Type],Id_number)
				VALUES(@ClientIdentity,@EnquiryId,1,2)
			END
		END
		--update Enquiry (BranchId)
		UPDATE Enquiry SET dbo.Enquiry.BranchId = @BranchId
		WHERE dbo.Enquiry.EnquiryId = @EnquiryId
		--End
		SELECT @applicationCount = @applicationCount + 1;

	
		fetch next from cur into @EnquiryId,@TenancyLength,@TenancyType,@TenancySatisfactory,@TenancyEnvironment,@TenancyEstablish
			,@EmployerName,@EmployerRangDate,@EmployerSpoketoName,@StabilityQ1,@StabilityQ2,@StabilityQ3,@StabilityQ4,@StabilityQ5,@StabilityQ6,@StabilityQ7,@StabilityQ8,@StabilityQ9,@StabilityQ10
			,@StabilityComments,@SelfEmployed,@Beneficiary,@EmployerNotRung,@CreditComments,@CreditExplanation,@CreditOther,@CreditQuestion1,@CreditQuestion2,@CreditQuestion3,@CreditQuestion4,@CreditQuestion5
			--joint Name
			,@JNTenancyLength,@JNTenancyType,@JNTenancySatisfactory,@JNTenancyEnvironment,@JNTenancyEstablish,@JNEmployerName,@JNEmployerRangDate,@JNEmployerSpoketoName
			,@JNStabilityQ1,@JNStabilityQ2,@JNStabilityQ3,@JNStabilityQ4,@JNStabilityQ5,@JNStabilityQ6,@JNStabilityQ7,@JNStabilityQ8,@JNStabilityQ9,@JNStabilityQ10,@JNStabilityComments
			,@JNSelfEmployed,@JNBeneficiary,@JNEmployerNotRung,@JNCreditComments,@JNCreditExplanation,@JNCreditOther,@JNCreditQuestion1,@JNCreditQuestion2,@JNCreditQuestion3,@JNCreditQuestion4,@JNCreditQuestion5
			--Company
			,@BusinessRegistered,@BusinessReturnsFiled,@BusinessPaidUpCapital,@BusinessShareholders,@BusinessDirectors,@BusinessDirectorNotes,@BusinessShareholderNotes,@BusinessHistorySatisfactory,@BusinessHistoryNotes
			,@BusinessAddressSatisfactory,@BusinessAddressNotes,@BusinessGuarantorID,@BusinessApplicationFilledOut,@BusinessGuarantorNotes,@BusinessAssetNotes,@BusinessGuarantorTenancyType,@BusinessGuarantorTenancy
			,@BusinessTenancy,@BusinessTenancyLength,@BusinessTenancyNotes,@BusinessOtherNotes
			--Business Ref
			,@BusinessRef1Name,@BusinessRef1Acct,@BusinessRef1Spend,@BusinessRef1Pay,@BusinessRef1Comments
			,@BusinessRef2Name,@BusinessRef2Acct,@BusinessRef2Spend,@BusinessRef2Pay,@BusinessRef2Comments
		  --Client
			,@ClientId,@Type,@Title,@FirstName,@MiddleNames,@LastName,@Salutation	 
			,@BranchId
			,@Address,@Suburb,@City,@Postcode
			,@IDCurrent,@IDPerson,@ResStatusAccept,@IDLegitimate,@AMLRisk
		  --Joint Name
			,@JNTitle,@JNFirstName,@JNMiddleNames,@JNLastName,@JNSalutation 
			,@JNAddress,@JNSuburb,@JNCity,@JNPostcode	  
			,@JNIDCurrent,@JNIDPerson,@JNResStatusAccept,@JNIDLegitimate,@JNAMLRisk
		  --DueDiligence
		  ,@ClientNum,@ClientNum2,@Client1TypeId,@Client2TypeId

		END
	close cur
	deallocate cur

	PRINT 'Data migrated for ' + CAST(@applicationCount AS VARCHAR)  + ' enquiries'

	-- finished migrate existing data!!!!! ***************************************************
	PRINT 'Finished migrating Enquiry data.'
END TRY
BEGIN CATCH
	--returns the complete original error message as a result set
	SELECT 
		ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage

	--will return the complete original error message as an error message
	DECLARE @ErrorMessage nvarchar(400), @ErrorNumber int, @ErrorSeverity int, @ErrorState int, @ErrorLine int
	SELECT @ErrorMessage = N'Error %d, Line %d, Message: '+ERROR_MESSAGE() + ' Migrate Customer values from Enquiry and Client tables to new Customer table for Enquiry ' + CAST(@EnquiryId AS VARCHAR),@ErrorNumber = ERROR_NUMBER(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE(),@ErrorLine = ERROR_LINE()
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber,@ErrorLine)
END CATCH
GO
PRINT '****************************************' --**** All OK to this point ****
GO
-- migrate existing data!!!!! ***************************************************
PRINT 'Start to Populate the new CustomerArchiveEnquiry table'
GO

DECLARE @enquiryCount INT

--Info required for new CustomerArchiveEnquiry table
DECLARE @CustomerId int
DECLARE @EnquiryId int
DECLARE @Type varchar(20)
DECLARE @EnumType int
DECLARE @Id_number int
--Existing Client table
DECLARE @ClientId int
--DECLARE @Type varchar(40)
DECLARE @Title varchar(40)
DECLARE @FirstName varchar(40)
DECLARE @MiddleNames varchar(60)
DECLARE @LastName varchar(40)
DECLARE @Salutation varchar(50)
DECLARE @BranchId varchar(5)
DECLARE @Address varchar(100)
DECLARE @Suburb varchar(50)
DECLARE @City varchar(50)
DECLARE @Postcode varchar(10)
DECLARE @IDCurrent bit
DECLARE @IDPerson varchar(255)
DECLARE @ResStatusAccept varchar(255)
DECLARE @IDLegitimate varchar(512)
DECLARE @AMLRisk smallint
--Joint Name
DECLARE @JNTitle varchar(40)
DECLARE @JNFirstName varchar(40)
DECLARE @JNMiddleNames varchar(60)
DECLARE @JNLastName varchar(40)
DECLARE @JNSalutation varchar(50)
DECLARE @JNAddress varchar(100)
DECLARE @JNSuburb varchar(50)
DECLARE @JNCity varchar(50)
DECLARE @JNPostcode varchar(10)
DECLARE @JNIDCurrent bit
DECLARE @JNIDPerson varchar(255)
DECLARE @JNResStatusAccept varchar(255)
DECLARE @JNIDLegitimate varchar(512)
DECLARE @JNAMLRisk smallint
DECLARE @ClientIdentity int

SET @enquiryCount = 0

declare cur CURSOR LOCAL for
SELECT        dbo.ArchiveEnquiry.EnquiryId,dbo.Client.ClientId,dbo.Client.[Type],dbo.Client.Title,dbo.Client.FirstName,dbo.Client.MiddleNames,dbo.Client.LastName,dbo.Client.Salutation
				,dbo.Client.BranchId,dbo.Client.[Address],dbo.Client.Suburb,dbo.Client.City,dbo.Client.Postcode
				,dbo.Client.IDCurrent,dbo.Client.IDPerson,dbo.Client.ResStatusAccept,dbo.Client.IDLegitimate,dbo.Client.AMLRisk
				--Joint Name
                ,dbo.Client.JNTitle,dbo.Client.JNFirstName,dbo.Client.JNMiddleNames,dbo.Client.JNLastName,dbo.Client.JNSalutation
				,dbo.Client.JNAddress,dbo.Client.JNSuburb,dbo.Client.JNCity,dbo.Client.JNPostcode
                ,dbo.Client.JNIDCurrent,dbo.Client.JNIDPerson, dbo.Client.JNResStatusAccept,dbo.Client.JNIDLegitimate,dbo.Client.JNAMLRisk
FROM            dbo.ArchiveEnquiry INNER JOIN
                dbo.Client ON dbo.ArchiveEnquiry.ClientId = dbo.Client.ClientId

open cur

fetch next from cur into 	@EnquiryId,@ClientId,@Type,@Title,@FirstName,@MiddleNames,@LastName,@Salutation	 
		,@BranchId,@Address,@Suburb,@City,@Postcode
		,@IDCurrent,@IDPerson,@ResStatusAccept,@IDLegitimate,@AMLRisk
	  --Joint Name
		,@JNTitle,@JNFirstName,@JNMiddleNames,@JNLastName,@JNSalutation 
		,@JNAddress,@JNSuburb,@JNCity,@JNPostcode	  
		,@JNIDCurrent,@JNIDPerson,@JNResStatusAccept,@JNIDLegitimate,@JNAMLRisk

while @@FETCH_STATUS = 0 BEGIN
	PRINT CAST(@EnquiryId AS VARCHAR)
    --execute  on each row
	SET @EnumType = CASE @Type --added 06/07/2016 Christopher
		WHEN 'Joint' THEN 0
		WHEN 'Individual' THEN 0
		WHEN 'Company' THEN 1
		WHEN 'Trust' THEN 2
		END

	--Customer
	INSERT INTO [Customer](CustomerType,Title,FirstName,MiddleNames,LastName,Salutation,BranchId,[Address],Suburb,City,Postcode
		,IDCurrent,IDPerson,ResStatusAccept,IDLegitimate,AMLRisk)
	VALUES (@EnumType,@Title,@FirstName,@MiddleNames,@LastName,@Salutation,@BranchId,@Address,@Suburb,@City,@Postcode
		,@IDCurrent,@IDPerson,@ResStatusAccept,@IDLegitimate,@AMLRisk)

	SET @ClientIdentity = SCOPE_IDENTITY()

	INSERT INTO [CustomerArchiveEnquiry](CustomerId,EnquiryId,[Type],Id_number)
		VALUES(@ClientIdentity,@EnquiryId,0,1)
	--Joint Customer
	If ISNULL(@JNLastName,'') <> ''
	BEGIN
		BEGIN
			INSERT INTO [Customer](CustomerType,Title,FirstName,MiddleNames,LastName,Salutation,BranchId,[Address],Suburb,City,Postcode
			,IDCurrent,IDPerson,ResStatusAccept,IDLegitimate,AMLRisk)
			VALUES (@EnumType,@JNTitle,@JNFirstName,@JNMiddleNames,@JNLastName,@JNSalutation,@BranchId,@JNAddress,@JNSuburb,@JNCity,@JNPostcode	  
			,@JNIDCurrent,@JNIDPerson,@JNResStatusAccept,@JNIDLegitimate,@JNAMLRisk)

			SET @ClientIdentity = SCOPE_IDENTITY()
		END
		BEGIN
			INSERT INTO [CustomerArchiveEnquiry](CustomerId,EnquiryId,[Type],Id_number)
				VALUES(@ClientIdentity,@EnquiryId,1,2)
		END
	END
	--update ArchiveEnquiry (BranchId)
	UPDATE ArchiveEnquiry SET dbo.ArchiveEnquiry.BranchId = @BranchId
	WHERE ArchiveEnquiry.EnquiryId = @EnquiryId
	
	SELECT @enquiryCount = @enquiryCount + 1;

	fetch next from cur into @EnquiryId,@ClientId,@Type,@Title,@FirstName,@MiddleNames,@LastName,@Salutation	 
		,@BranchId,@Address,@Suburb,@City,@Postcode
		,@IDCurrent,@IDPerson,@ResStatusAccept,@IDLegitimate,@AMLRisk
		--Joint Name
		,@JNTitle,@JNFirstName,@JNMiddleNames,@JNLastName,@JNSalutation 
		,@JNAddress,@JNSuburb,@JNCity,@JNPostcode	  
		,@JNIDCurrent,@JNIDPerson,@JNResStatusAccept,@JNIDLegitimate,@JNAMLRisk		
END	
close cur
deallocate cur

PRINT 'Data migrated for ' + CAST(@enquiryCount AS VARCHAR)  + ' enquiries'

-- finished migrate existing data!!!!! ***************************************************
PRINT 'Finished migrating ArchiveEnquiry data.' --takes about 40 seconds
GO
PRINT '****************************************'
GO
PRINT 'DB script completed'
GO
