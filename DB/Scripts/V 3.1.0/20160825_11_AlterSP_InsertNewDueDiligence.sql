--USE [DevTrueTrack2]
USE EnquiryWorkSheet
GO

/****** Object:  StoredProcedure [dbo].[InsertNewDueDiligence]    Script Date: 25/08/2016 2:23:55 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Christopher Steel
-- Create date: 2 March 2011
-- Description:	check id DueDilgence table entry exists, if not then Insert with EnquiryId to create record
-- =============================================
-- 01/05/2013 Sunil Modified to Insert LoanNum into the table
-- 04/06/2013 Sunil Condition added to check ClientNum2 is NULL
-- 07/06/2013 Sunil Added Parameter @Client1TypeId, @Client2TypeId
--25/08/2016 Christopher: Removed @ClientNum,@ClientNum2,@Client1TypeId, @Client2TypeId 
-- =============================================
ALTER PROCEDURE [dbo].[InsertNewDueDiligence] 
	-- Add the parameters for the stored procedure here
	@EnquiryId int
	,@LoanNum VARCHAR(8)=''
	--,@ClientNum VARCHAR(8) =''
	--,@ClientNum2 VARCHAR(8) =''
	--,@Client1TypeId VARCHAR(5) =''
	--,@Client2TypeId VARCHAR(5) =''
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Check if record already exists
    IF NOT EXISTS(SELECT * FROM DueDiligence WHERE EnquiryId = @EnquiryId)
BEGIN
 INSERT INTO DueDiligence(EnquiryId, LoanNum) --, ClientNum, ClientNum2, Client1TypeId, Client2TypeId) 
					VALUES (@EnquiryId , @LoanNum) --, @ClientNum, @ClientNum2, @Client1TypeId, @Client2TypeId)
					
  -- Retreive data form table		
	SELECT * FROM DueDiligence WHERE EnquiryId = @EnquiryId
RETURN 1
END
ELSE -- Enquiry exists
	--UPDATE LoanNum If LoanNum is not NULL 
	IF NOT @LoanNum IS NULL
	BEGIN
	-- IF NOT @ClientNum IS NULL AND NOT @ClientNum2 IS NULL 
	--	 BEGIN
	--			UPDATE DueDiligence SET LoanNum=@LoanNum, ClientNum=@ClientNum, ClientNum2=@ClientNum2 ,
	--			Client1TypeId =@Client1TypeId, Client2TypeId=@Client2TypeId WHERE EnquiryId = @EnquiryId
	--	 END
	-- ELSE IF NOT @ClientNum IS NULL 
	--	 BEGIN
	--			--if ClientNUM is NOT NULL and ClientNUM2 null
	--			UPDATE DueDiligence SET LoanNum=@LoanNum, ClientNum=@ClientNum,
	--			Client1TypeId =@Client1TypeId WHERE EnquiryId = @EnquiryId
	--	 END	
	-- ELSE IF NOT @ClientNum2 IS NULL 
	--	 BEGIN
	--		--if ClientNUM2 is NOT NULL and ClientNUM null
	--			UPDATE DueDiligence SET LoanNum=@LoanNum, ClientNum2=@ClientNum2,
	--			Client2TypeId=@Client2TypeId WHERE EnquiryId = @EnquiryId
	--	 END			 	 
	--ELSE
	--	BEGIN
				--Both ClientNum and ClientNUM2 are NULL
		UPDATE DueDiligence SET LoanNum=@LoanNum WHERE EnquiryId = @EnquiryId
		 --END
	END

	-- Retreive data form table		
	SELECT * FROM DueDiligence WHERE EnquiryId = @EnquiryId
RETURN 0
    
	
END

GO


