--USE [Dev_TrueTrack]
--USE DevTrueTrack2
USE EnquiryWorkSheet
GO
/****** Object:  StoredProcedure [dbo].[InsertAndReturnNewEnquiry]    Script Date: 9/06/2016 3:52:36 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		CSteel
-- Create date: 07/11/2010
-- Description:	Insert new Enquiry
-- 09/06/2016: modify for multiple clients; 
-- Remove 'Setup Client table'
-- Add Branchid to Enquiry table
-- =============================================


ALTER PROCEDURE [dbo].[InsertAndReturnNewEnquiry] 
	-- Add the parameters for the stored procedure here
	@CurrentStatus varchar(20) = 'Preliminary',
	@Status30mins varchar(20) = 'Preliminary',
	@EnquiryManagerId int,
	@LoggedinBranchId varchar(5) = 'YFMAN',
	@StartTime datetime = GETDATE
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	---- Setup Client table
	--INSERT INTO Client (Type,BranchId) VALUES ('Individual', @LoggedinBranchId)
	--DECLARE @ClientIdentity int
	--SET @ClientIdentity = SCOPE_IDENTITY()
	
	-- Setup Enquiry table
	DECLARE @CurrentDateTime datetime, @ActiveStatus bit, @WizardStatus smallint
	Set @ActiveStatus = 1
    Set @WizardStatus = 0
	
    -- Insert statements for Enquiry table
	INSERT INTO Enquiry ([DateTime], Status30mins, CurrentStatus, ActiveStatus, WizardStatus, EnquiryManagerId, CurrentStatusSetDate, BranchId) 
	VALUES (@StartTime, @Status30mins, @CurrentStatus, @ActiveStatus, @WizardStatus, @EnquiryManagerId, @StartTime, @LoggedinBranchId)
	
	DECLARE @Identity int
	SET @Identity = SCOPE_IDENTITY()
	
	-- Retreive data form table		
	SELECT * FROM Enquiry WHERE EnquiryId = @Identity
	
END