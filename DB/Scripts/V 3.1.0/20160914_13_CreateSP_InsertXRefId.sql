--USE DevTrueTrack2
USE EnquiryWorkSheet
GO
-- ================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT'Start database script'
GO
PRINT '*** Create/Alter Stored procedure InsertXRefId ***'
GO
--******************************************
-- InsertClientId

DECLARE @SQLCreateSP_InsertXRefId nvarchar(max) = '
CREATE PROCEDURE [dbo].[InsertXRefId] 
-- =============================================
-- Author:		Christopher Steel
-- Create date: 14 September 2016
-- Description:	check Customer table entry exists, if does then update the XRefId details
-- =============================================
	@CustomerId int,
	@XRefId VARCHAR(20) =''''	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT * FROM Customer WHERE CustomerId = @CustomerId)
	BEGIN
		UPDATE  Customer
		SET     XRefId = @XRefId				
		WHERE   (CustomerId = @CustomerId)					
	  
		RETURN 1
	END
	ELSE -- Customer does not exist
		RETURN 0   
	
	END'
--******
DECLARE @SQLAlterSP_InsertXRefId nvarchar(max) = '
ALTER PROCEDURE [dbo].[InsertXRefId] 
-- =============================================
-- Author:		Christopher Steel
-- Create date: 14 September 2016
-- Description:	check Customer table entry exists, if does then update the XRefId details
-- =============================================
	@CustomerId int,
	@XRefId VARCHAR(20) =''''	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT * FROM Customer WHERE CustomerId = @CustomerId)
	BEGIN
		UPDATE  Customer
		SET     XRefId = @XRefId				
		WHERE   (CustomerId = @CustomerId)					
	  
		RETURN 1
	END
	ELSE -- Customer does not exist
		RETURN 0   
	
	END'
--******
IF OBJECT_ID(N'dbo.[InsertXRefId]', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure InsertXRefId'
	END
	BEGIN
		EXEC (@SQLCreateSP_InsertXRefId);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure InsertXRefId'
	END
	BEGIN
		EXEC (@SQLAlterSP_InsertXRefId);
	END
END
GO

