--Project TrueTrack 3.0.0
--Series of scripts to modify database schema and migrate data so can use Multiple Customers
--Run scripts in succession from C:\Projects\TrueTrack-All 20160623\truetrack\DB\Scripts
--(1) 20160525_1_CreateNewTables.sql
--(2) 20160525_2_MigrateCustomerData.sql
--(3) 20160609_3_SP_InsertAndReturnNewEnquiry.sql
--(4) 20160610_4_AlterEnquiryTable_RemoveFields.sql
--(5) 20160611_5_AlterViewActiveClientEnquiries.sql
--(6) 20160611_6_AlterSP_ArchiveThisEnquiry.sql

--USE [DevTrueTrack2]
--USE [Dev_TrueTrack]
--USE EnquiryWorkSheet
--USE TestEnquiryWorkSheet
Use EnquiryWorkSheet
GO
PRINT 'Starting database script'
GO
PRINT '****************************'
GO
--***************************************************************************
--Script by Christopher 25/05/2016
--Create Customer table and populate it
--Create CustomerEnquiry table
--Create CustomerArchiveEnquiry table
--Create DirectorTrustee table
--Create EnquiryCustomers view
--Create Enum_CustomerType table
--Create Enum_CustomerEnquiryType table
--Modify script by Christopher 30/06/2016
--Alter Enquiry: add fields to the Enquiry table: Contact's title, firstName, lastName, displayName, suburb, city, phoneNumber, emailAddress, BranchId
--add BranchId field to the ArchiveEnquiry table
--ALTER VIEW ActiveClientEnquiries
--ALTER VIEW DisplayAllNames
--05/07/2016 Christopher: Alter Enquiry: add mainCustomerType field | Change [CustomerEnquiry]: made Type an integer (for Enum)
--08/07/2016 Christopher: Create Enum_AMLRisk table
--20/07/2016 Christopher: Create DirectorTrusteeShareHolderBeneficiary table | Create Enum_DTSB_Type table | Drop DirectorTrustee table
--21/07/2016 Christopher: Altered EnquiryCustomers view (canotate name)
--28/07/2016 Christopher: Add [BusinessShareIdentified] and [BusinessShareStructure] to customer table
--29/07/2016 Christopher: Create TradeReferences table
--29/07/2016 Christopher: Remove TradeReferences section from Create Customer table
--03/08/2016 Christopher: Add foreign key to TradeReferences table
--05/08/2016 Christopher: Alter View DisplayAllNames: change parameter 'main' to '0'
--08/08/2016 Christopher: Alter DirectorTrusteeShareHolderBeneficiary table: Add CustomerId foreign key
--17/08/2016 Christopher: Alter Customer table: Add AcceptanceMethod [int] field
--19/08/2016 Christopher: Alter Customer table: Add IsaPEP [bit] field
--24/08/2016 Christopher: Alter EnquiryCustomers View: add ClientTypeId & ClientNum
--31/10/2016 Christopher: Create AppSettings table
--14/11/2016 Christopher: Create VIEW [dbo].[EnquiryCustomers]: add case CustomerType = 3
--14/11/2016 Christopher: Create Enum_CustomerType table: add CustomerType 3 (SoleTrader)
--17/11/2016 Christopher: Alter Appsettings table: Added Boolean field to table schema
--23/11/2016 Christopher: Alter Enum_EnquiryType: Add Classification(Enum) field, populate with Unsecured EnquiryTypes
--05/12/2016 Christopher: Alter Enum_EnquiryType: Add foreign key FK_Enum_EnquiryType_Enum_Classification
--***************************************************************************
--Create Customer table
--Test if table exists
if(object_id(N'[dbo].[Customer]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Customer table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Customer](
			[CustomerId] [int] IDENTITY(1,1) NOT NULL,
			--[CustomerType] [varchar](40) NULL, --individual, company, sole trader, partnership, trust ...
			[CustomerType] int NULL,
			[TradingName] [varchar](50) NULL,
			[LegalName] [varchar](50) NULL,
			[CompanyNumber] [varchar](20) NULL,
			[CompanyOfficers] [varchar](250) NULL,
			[Title] [varchar](40) NULL,
			[FirstName] [varchar](40) NULL,
			[MiddleNames] [varchar](60) NULL,
			[LastName] [varchar](40) NULL,
			[Salutation] [varchar](50) NULL,
			[DateofBirth] [smalldatetime] NULL,
			[Gender] [varchar](10) NULL,
			--[MaritalStatus] [varchar](20) NULL,
			--[Occupation] [varchar](50) NULL,				
			[BranchId] [varchar](5) NULL,
			--[UserField1] [varchar](50) NULL,
			[Address] [varchar](100) NULL,
			[Suburb] [varchar](50) NULL,
			[City] [varchar](50) NULL,
			[Postcode] [varchar](10) NULL,
			--Person Acting: Company, Trust
			--[PAAddress] [varchar](100) NULL,
			--[PASuburb] [varchar](50) NULL,
			--[PACity] [varchar](50) NULL,
			--[PAPostcode] [varchar](10) NULL,
			--Identity: Individual
			[AcceptanceMethod] int NULL,
			[IDCurrent] [bit] NULL CONSTRAINT [DF_Customer_IDCurrent]  DEFAULT ((0)),
			[IDPerson] [varchar](255) NULL,
			[ResStatusAccept] [varchar](255) NULL,
			[IDLegitimate] [varchar](512) NULL,
			[AMLRisk] [smallint] NULL DEFAULT ((0)),
			[AML_UserId] [int] NULL, --user that saved Customer AML section	
			[ClientTypeId] [varchar](5) NULL, --finPower client type (C)
			[ClientNum] [varchar](8) NULL, --finPower Client number
			[XRefId] [varchar](20) NULL, --OAC CustomerId			
			--Tenancy: Individual and Company etc
			[TenancyLength] [varchar](20) NULL CONSTRAINT [DF_Customer_TenancyLength]  DEFAULT (''),
			[TenancyType] [varchar](30) NULL CONSTRAINT [DF_Customer_TenancyType]  DEFAULT (''),
			[TenancySatisfactory] [varchar](max) NULL CONSTRAINT [DF_Customer_TenancySatisfactory]  DEFAULT (''),
			[TenancyEnvironment] [varchar](max) NULL CONSTRAINT [DF_Customer_TenancyEnvironment]  DEFAULT (''),
			[TenancyEstablish] [varchar](max) NULL CONSTRAINT [DF_Customer_TenancyEstablish]  DEFAULT (''),
			--Employment: Individual
			[EmployerName] [varchar](60) NULL CONSTRAINT [DF_Customer_EmployerName]  DEFAULT (''),
			[EmployerRangDate] [smalldatetime] NULL,
			[EmployerSpoketoName] [varchar](60) NULL CONSTRAINT [DF_Customer_EmployerSpoketoName]  DEFAULT (''),
			[StabilityQ1] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ1]  DEFAULT (''),
			[StabilityQ2] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ2]  DEFAULT (''),
			[StabilityQ3] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ3]  DEFAULT (''),
			[StabilityQ4] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ4]  DEFAULT (''),
			[StabilityQ5] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ5]  DEFAULT ('0.00'),
			[StabilityQ6] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ6]  DEFAULT (''),
			[StabilityQ7] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ7]  DEFAULT (''),
			[StabilityQ8] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ8]  DEFAULT (''),
			[StabilityQ9] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ9]  DEFAULT (''),
			[StabilityQ10] [varchar](50) NULL CONSTRAINT [DF_Customer_StabilityQ10]  DEFAULT (''),
			[StabilityComments] [varchar](max) NULL CONSTRAINT [DF_Customer_StabilityComments]  DEFAULT (''),
			[SelfEmployed] [bit] NULL CONSTRAINT [DF_Customer_SelfEmployed]  DEFAULT ((0)),
			[Beneficiary] [bit] NULL CONSTRAINT [DF_Customer_Beneficiary]  DEFAULT ((0)),
			[EmployerNotRung] [bit] NULL CONSTRAINT [DF_Customer_EmployerNotRung]  DEFAULT ((0)),
			--Credit: Individual and Company etc
			[CreditComments] [varchar](max) NULL CONSTRAINT [DF_Customer_CreditComments]  DEFAULT (''),
			[CreditExplanation] [varchar](max) NULL CONSTRAINT [DF_Customer_CreditExplanation]  DEFAULT (''),
			[CreditOther] [varchar](max) NULL CONSTRAINT [DF_Customer_CreditOther]  DEFAULT (''),
			[CreditQuestion1] [bit] NULL CONSTRAINT [DF_Customer_CreditQuestion1]  DEFAULT ((0)),
			[CreditQuestion2] [bit] NULL CONSTRAINT [DF_Customer_CreditQuestion2]  DEFAULT ((0)),
			[CreditQuestion3] [bit] NULL CONSTRAINT [DF_Customer_CreditQuestion3]  DEFAULT ((1)),
			[CreditQuestion4] [bit] NULL CONSTRAINT [DF_Customer_CreditQuestion4]  DEFAULT ((0)),
			[CreditQuestion5] [bit] NULL CONSTRAINT [DF_Customer_CreditQuestion5]  DEFAULT ((0)),
			--Business info
			[BusinessRegistered] [bit] NULL CONSTRAINT [DF_Customer_BusinessRegistered]  DEFAULT ((0)),
			[CoExtractTrustDeed] [bit] NULL CONSTRAINT [DF_Customer_CoExtractTrustDeed]  DEFAULT ((0)),
			[BusinessReturnsFiled] [bit] NULL CONSTRAINT [DF_Customer_BusinessReturnsFiled]  DEFAULT ((0)),
			[BusinessPaidUpCapital] [bit] NULL CONSTRAINT [DF_Customer_BusinessPaidUpCapital]  DEFAULT ((0)),
			[BusinessHistorySatisfactory] [bit] NULL CONSTRAINT [DF_Customer_BusinessHistorySatisfactory]  DEFAULT ((0)),
			[BusinessHistoryNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessHistoryNotes]  DEFAULT (''),
			[BusinessAddressSatisfactory] [bit] NULL CONSTRAINT [DF_Customer_BusinessAddressSatisfactory]  DEFAULT ((0)),
			[BusinessAddressNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessAddressNotes]  DEFAULT (''),
			--Shareholders/Beneficiaries
			[BusinessShareholders] [bit] NULL CONSTRAINT [DF_Customer_BusinessShareholders]  DEFAULT ((0)),
			[BusinessShareholderNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessShareholderNotes]  DEFAULT (''),
			[BusinessShareIdentified] [bit] NULL CONSTRAINT [DF_Customer_BusinessShareIdentified]  DEFAULT ((0)),
			[BusinessShareStructure] [bit] NULL CONSTRAINT [DF_Customer_BusinessShareStructure]  DEFAULT ((0)),
			--Directors/Trustees
			[BusinessDirectors] [bit] NULL CONSTRAINT [DF_Customer_BusinessDirectors]  DEFAULT ((0)),
			[BusinessDirectorNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessDirectorNotes]  DEFAULT (''),	
			--Business guarantors
			[BusinessApplicationFilledOut] [bit] NULL CONSTRAINT [DF_Customer_BusinessApplicationFilledOut]  DEFAULT ((0)),
			[BusinessGuarantorNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessGuarantorNotes]  DEFAULT (''),
			[BusinessAssetNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessAssetNotes]  DEFAULT (''),
			--Business Comments
			[BusinessOtherNotes] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessOtherNotes]  DEFAULT (''),
			----Business References
			--[BusinessRef1Name] [varchar](100) NULL CONSTRAINT [DF_Customer_BusinessRef1Name]  DEFAULT (''),
			--[BusinessRef1Acct] [varchar](50) NULL CONSTRAINT [DF_Customer_BusinessRef1Acct]  DEFAULT (''),
			--[BusinessRef1Spend] [varchar](20) NULL CONSTRAINT [DF_Customer_BusinessRef1Spend]  DEFAULT (''),
			--[BusinessRef1Pay] [varchar](50) NULL CONSTRAINT [DF_Customer_BusinessRef1Pay]  DEFAULT (''),
			--[BusinessRef1Comments] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessRef1Comments]  DEFAULT (''),
			--[BusinessRef2Name] [varchar](100) NULL CONSTRAINT [DF_Customer_BusinessRef2Name]  DEFAULT (''),
			--[BusinessRef2Acct] [varchar](50) NULL CONSTRAINT [DF_Customer_BusinessRef2Acct]  DEFAULT (''),
			--[BusinessRef2Spend] [varchar](20) NULL CONSTRAINT [DF_Customer_BusinessRef2Spend]  DEFAULT (''),
			--[BusinessRef2Pay] [varchar](50) NULL CONSTRAINT [DF_Customer_BusinessRef2Pay]  DEFAULT (''),
			--[BusinessRef2Comments] [varchar](1024) NULL CONSTRAINT [DF_Customer_BusinessRef2Comments]  DEFAULT (''),
			[IsaPEP] [bit] NULL CONSTRAINT [DF_Customer_IsaPEP]  DEFAULT ((0)),

		 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
		(
			[CustomerId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Customer table created'
	END
END
ELSE
BEGIN 
	PRINT 'Customer table already exists'	
END
GO
--****************************************************************
--Create CustomerEnquiry table
--Test if table exists
if(object_id(N'[dbo].[CustomerEnquiry]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create CustomerEnquiry table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[CustomerEnquiry](
			[CustomerId] [int] NOT NULL,
			[EnquiryId] [int] NOT NULL,
			--[Type] [varchar](20) NULL CONSTRAINT [DF_CustomerEnquiry_Type]  DEFAULT (''), --main, co-main, guarantor
			[Type] [int] NULL, --main = 0, co-main = 1, guarantor = 2
			[Id_number] [int] NULL, --ID number used locally for Customer for use in document export; 1, 2, 3 ... (Unique for each EnquiryId)
		 CONSTRAINT [PK_CustomerEnquiry] PRIMARY KEY CLUSTERED 
		(
			[CustomerId] ASC,
			[EnquiryId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerEnquiry]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerEnquiry_Enquiry] FOREIGN KEY([EnquiryId])
		REFERENCES [dbo].[Enquiry] ([EnquiryId])
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerEnquiry] CHECK CONSTRAINT [FK_CustomerEnquiry_Enquiry]
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerEnquiry]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerEnquiry_Customer] FOREIGN KEY([CustomerId])
		REFERENCES [dbo].[Customer] ([CustomerId])
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerEnquiry] CHECK CONSTRAINT [FK_CustomerEnquiry_Customer]
	END
	BEGIN
		PRINT'CustomerEnquiry table created'
	END
END
ELSE
BEGIN 
	PRINT 'CustomerEnquiry table already exists'	
END
GO
--****************************************************************
--Create CustomerArchiveEnquiry table
--Test if table exists
if(object_id(N'[dbo].[CustomerArchiveEnquiry]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create CustomerArchiveEnquiry table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[CustomerArchiveEnquiry](
			[CustomerId] [int] NOT NULL,
			[EnquiryId] [int] NOT NULL,
			--[Type] [varchar](20) NULL CONSTRAINT [DF_CustomerArchiveEnquiry_Type]  DEFAULT (''), --main, co-main, guarantor
			[Type] [int] NULL, --main = 0, co-main = 1, guarantor = 2
			[Id_number] [int] NULL, --ID number used locally for Customer for use in document export; 1, 2, 3 ... (Unique for each EnquiryId)
		 CONSTRAINT [PK_CustomerArchiveEnquiry] PRIMARY KEY CLUSTERED 
		(
			[CustomerId] ASC,
			[EnquiryId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerArchiveEnquiry]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerArchiveEnquiry_Enquiry] FOREIGN KEY([EnquiryId])
		REFERENCES [dbo].[ArchiveEnquiry] ([EnquiryId])
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerArchiveEnquiry] CHECK CONSTRAINT [FK_CustomerArchiveEnquiry_Enquiry]
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerArchiveEnquiry]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerArchiveEnquiry_Customer] FOREIGN KEY([CustomerId])
		REFERENCES [dbo].[Customer] ([CustomerId])
	END
	BEGIN
		ALTER TABLE [dbo].[CustomerArchiveEnquiry] CHECK CONSTRAINT [FK_CustomerArchiveEnquiry_Customer]
	END
	BEGIN
		PRINT'CustomerArchiveEnquiry table created'
	END
END
ELSE
BEGIN 
	PRINT 'CustomerArchiveEnquiry table already exists'	
END
GO
--****************************************************************
--Create DirectorTrusteeShareHolderBeneficiary table
--Test if table exists
if(object_id(N'[dbo].[DirectorTrusteeShareHolderBeneficiary]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create DirectorTrusteeShareHolderBeneficiary table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[DirectorTrusteeShareHolderBeneficiary](
			[Id]              [int]        IDENTITY(1,1) NOT NULL,
			[CustomerId]      [int]        NOT NULL,
			[Type]            [int]        NOT NULL,
			[FirstName]       [varchar] (40) NULL,
			[LastName]        [varchar] (40) NULL,
			[DateOfBirth]     [smalldatetime]       NULL,
			[IdVerified]      BIT        NULL,
			[AddressVerified] BIT        NULL,
		 CONSTRAINT [DIRECTORTRUSTEESHAREHOLDERBENEFICIARY_ID_PK] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		 ALTER TABLE [dbo].[DirectorTrusteeShareHolderBeneficiary]  WITH CHECK ADD  CONSTRAINT [FK_DirectorTrusteeShareHolderBeneficiary_Customer] FOREIGN KEY([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
	END
	BEGIN
		 ALTER TABLE [dbo].[DirectorTrusteeShareHolderBeneficiary] CHECK CONSTRAINT [FK_DirectorTrusteeShareHolderBeneficiary_Customer]
	END
	BEGIN
		PRINT'DirectorTrusteeShareHolderBeneficiary table created'
	END
END
ELSE
BEGIN 
	PRINT 'DirectorTrusteeShareHolderBeneficiary table already exists'	
END
GO
--****************************************************************
--Drop DirectorTrustee table
--Test if table exists
if(object_id(N'[dbo].[DirectorTrustee]', 'U') IS NOT NULL)
BEGIN --table exists
	BEGIN
		PRINT 'DirectorTrustee table exits'
	END
	BEGIN
		DROP TABLE [dbo].[DirectorTrustee]
	END
	BEGIN
		PRINT'DirectorTrustee table dropped'
	END	
END
GO
--****************************************************************
--Create TradeReferences table
--Test if table exists
if(object_id(N'[dbo].[TradeReferences]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create TradeReferences table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[TradeReferences](
			[Id]              [int]        IDENTITY(1,1) NOT NULL,
			[CustomerId]      [int]        NOT NULL,
			[BusinessName] [varchar](50) NULL,
			[Account] [varchar](50) NULL,
			[Spend] [varchar](20) NULL,
			[Pay] [varchar](50) NULL,
			[Comments] [varchar](1024) NULL
			CONSTRAINT [PK_TradeReferences] PRIMARY KEY CLUSTERED
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		ALTER TABLE [dbo].[TradeReferences]  WITH CHECK ADD  CONSTRAINT [FK_TradeReferences_Customer] FOREIGN KEY([CustomerId]) 
		REFERENCES [dbo].[Customer] ([CustomerId])
	END
	BEGIN
		ALTER TABLE [dbo].[TradeReferences] CHECK CONSTRAINT [FK_TradeReferences_Customer]	
	END
	BEGIN
		PRINT'TradeReferences table created'
	END
END
ELSE
BEGIN 
	PRINT 'TradeReferences table already exists'	
END
GO
--****************************************************************
--Create EnquiryCustomers view
DECLARE @SQLCreateView nvarchar(max) = '
CREATE VIEW [dbo].[EnquiryCustomers]
--15/06/2016: Create View by Christopher
AS
SELECT  dbo.CustomerEnquiry.Id_number
		, CASE WHEN dbo.Customer.CustomerType = 0 THEN ISNULL(dbo.Customer.FirstName,'''') + '' '' + ISNULL(LastName,'''') 
		WHEN dbo.Customer.CustomerType = 1 THEN ISNULL(dbo.Customer.LegalName,ISNULL(dbo.Customer.TradingName,''''))
		WHEN dbo.Customer.CustomerType = 2 THEN ISNULL(dbo.Customer.TradingName,'''') 
		WHEN dbo.Customer.CustomerType = 3 THEN ISNULL(dbo.Customer.FirstName,'''') + '' '' + ISNULL(LastName,'''')  END as [Name]
		, dbo.CustomerEnquiry.[Type]
		, dbo.Customer.CustomerType
		, dbo.Customer.CustomerId
		, dbo.CustomerEnquiry.EnquiryId
		, dbo.Customer.ClientTypeId
		, dbo.Customer.ClientNum
		, dbo.Customer.XRefId
	FROM  dbo.Customer INNER JOIN
		dbo.CustomerEnquiry ON dbo.Customer.CustomerId = dbo.CustomerEnquiry.CustomerId'
		-- ****
DECLARE @SQLAlterView nvarchar(max) = '
ALTER VIEW [dbo].[EnquiryCustomers]
--15/06/2016: Create View by Christopher
AS
SELECT  dbo.CustomerEnquiry.Id_number
		, CASE WHEN dbo.Customer.CustomerType = 0 THEN ISNULL(dbo.Customer.FirstName,'''') + '' '' + ISNULL(LastName,'''') 
		WHEN dbo.Customer.CustomerType = 1 THEN ISNULL(dbo.Customer.LegalName,ISNULL(dbo.Customer.TradingName,''''))
		WHEN dbo.Customer.CustomerType = 2 THEN ISNULL(dbo.Customer.TradingName,'''') 
		WHEN dbo.Customer.CustomerType = 3 THEN ISNULL(dbo.Customer.FirstName,'''') + '' '' + ISNULL(LastName,'''')  END as [Name]
		, dbo.CustomerEnquiry.[Type]
		, dbo.Customer.CustomerType
		, dbo.Customer.CustomerId
		, dbo.CustomerEnquiry.EnquiryId
		, dbo.Customer.ClientTypeId
		, dbo.Customer.ClientNum
		, dbo.Customer.XRefId
	FROM  dbo.Customer INNER JOIN
		dbo.CustomerEnquiry ON dbo.Customer.CustomerId = dbo.CustomerEnquiry.CustomerId'
--Test if view exists
if(object_id(N'[dbo].[EnquiryCustomers]', 'V') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create EnquiryCustomers view'
	END
	BEGIN
		EXEC (@SQLCreateView);
	END	
	BEGIN
		PRINT'EnquiryCustomers view created'
	END
END
ELSE
BEGIN 
	PRINT 'EnquiryCustomers view already exists'	
	BEGIN
		PRINT 'Alter EnquiryCustomers view'
	END
	BEGIN
		EXEC (@SQLAlterView);
	END	
	BEGIN
		PRINT'EnquiryCustomers view altered'
	END
END
GO
--****************************************************************
--Create Enum_CustomerType table
--Test if table exists
if(object_id(N'[dbo].[Enum_CustomerType]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Enum_CustomerType table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Enum_CustomerType](
			[Enum] [int] NOT NULL,
			[Name] [varchar](50) NULL,
			[Description] [varchar](50) NULL,
		 CONSTRAINT [PK_Enum_CustomerType] PRIMARY KEY CLUSTERED 
		(
			[Enum] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Enum_CustomerType table created'
	END
END
ELSE
BEGIN 
	BEGIN
		PRINT 'Enum_CustomerType table exists'
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_CustomerType', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_CustomerType table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_CustomerType CONSTRAINT from Enum_CustomerType table
			ALTER TABLE [dbo].[Enum_CustomerType] DROP CONSTRAINT [chk_read_only_Enum_CustomerType]
			END
		END
	END
END
GO
--Delete data from table
BEGIN
        BEGIN
            PRINT 'Delete records from Enum_CustomerType table'
        END
        BEGIN
			DELETE FROM Enum_CustomerType
        END
END
GO
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate Enum_CustomerType table'
        END
        BEGIN
        INSERT INTO [Enum_CustomerType] ([Enum],[Name],[Description]) VALUES (0,'Individual','Individual');
        INSERT INTO [Enum_CustomerType] ([Enum],[Name],[Description]) VALUES (1,'Company','Company');
		INSERT INTO [Enum_CustomerType] ([Enum],[Name],[Description]) VALUES (2,'Trust','Trust'); 
		INSERT INTO [Enum_CustomerType] ([Enum],[Name],[Description]) VALUES (3,'SoleTrader','Sole trader');        
        END
END
GO
--Protect table
BEGIN
	BEGIN
		PRINT 'Protect Enum_CustomerType table'
    END
    BEGIN		
		ALTER TABLE [dbo].Enum_CustomerType WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_CustomerType CHECK( 1 = 0 )				
	END
END
GO
--***************************************************************************
--Create Enum_CustomerEnquiryType table
--Test if table exists
if(object_id(N'[dbo].[Enum_CustomerEnquiryType]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Enum_CustomerEnquiryType table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Enum_CustomerEnquiryType](
			[Enum] [int] NOT NULL,
			[Name] [varchar](50) NULL,
			[Description] [varchar](50) NULL,
		 CONSTRAINT [PK_Enum_CustomerEnquiryType] PRIMARY KEY CLUSTERED 
		(
			[Enum] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Enum_CustomerEnquiryType table created'
	END
END
ELSE
BEGIN 
	BEGIN
		PRINT 'Enum_CustomerEnquiryType table exists'
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_CustomerEnquiryType', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_CustomerEnquiryType table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_CustomerType CONSTRAINT from Enum_CustomerType table
			ALTER TABLE [dbo].[Enum_CustomerEnquiryType] DROP CONSTRAINT [chk_read_only_Enum_CustomerEnquiryType]
			END
		END
	END
END
GO
--Delete data from table
BEGIN
        BEGIN
            PRINT 'Delete records from Enum_CustomerEnquiryType table'
        END
        BEGIN
			DELETE FROM Enum_CustomerEnquiryType
        END
END
GO
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate Enum_CustomerEnquiryType table'
        END
        BEGIN
        INSERT INTO [Enum_CustomerEnquiryType] ([Enum],[Name],[Description]) VALUES (0,'main','main');
        INSERT INTO [Enum_CustomerEnquiryType] ([Enum],[Name],[Description]) VALUES (1,'co-main','co-main');
		INSERT INTO [Enum_CustomerEnquiryType] ([Enum],[Name],[Description]) VALUES (2,'guarantor','guarantor');        
        END
END
GO
--Protect table
BEGIN
	BEGIN
		PRINT 'Protect Enum_CustomerEnquiryType table'
    END
    BEGIN		
		ALTER TABLE [dbo].Enum_CustomerEnquiryType WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_CustomerEnquiryType CHECK( 1 = 0 )				
	END
END
GO
--****************************************************************
--Create Enum_AMLRisk table
--Test if table exists
if(object_id(N'[dbo].[Enum_AMLRisk]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Enum_AMLRisk table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Enum_AMLRisk](
			[Enum] [int] NOT NULL,
			[Name] [varchar](50) NULL,
			[Description] [varchar](50) NULL,
		 CONSTRAINT [PK_Enum_AMLRisk] PRIMARY KEY CLUSTERED 
		(
			[Enum] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Enum_AMLRisk table created'
	END
END
ELSE
BEGIN 
	BEGIN
		PRINT 'Enum_AMLRisk table exists'
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_AMLRisk', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_AMLRisk table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_AMLRisk CONSTRAINT from Enum_AMLRisk table
			ALTER TABLE [dbo].[Enum_AMLRisk] DROP CONSTRAINT [chk_read_only_Enum_AMLRisk]
			END
		END
	END
END
GO
--Delete data from table
BEGIN
        BEGIN
            PRINT 'Delete records from Enum_AMLRisk table'
        END
        BEGIN
			DELETE FROM Enum_AMLRisk
        END
END
GO
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate Enum_AMLRisk table'
        END
        BEGIN
        INSERT INTO [Enum_AMLRisk] ([Enum],[Name],[Description]) VALUES (0,'SelectOne','Select one');
        INSERT INTO [Enum_AMLRisk] ([Enum],[Name],[Description]) VALUES (1,'Low','Low');
		INSERT INTO [Enum_AMLRisk] ([Enum],[Name],[Description]) VALUES (2,'Medium','Medium');  
		INSERT INTO [Enum_AMLRisk] ([Enum],[Name],[Description]) VALUES (3,'High','High');      
        END
END
GO
--Protect table
BEGIN
	BEGIN
		PRINT 'Protect Enum_AMLRisk table'
    END
    BEGIN		
		ALTER TABLE [dbo].Enum_AMLRisk WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_AMLRisk CHECK( 1 = 0 )				
	END
END
GO
--****************************************************************
--Create Enum_DTSB_Type table
--Test if table exists
if(object_id(N'[dbo].[Enum_DTSB_Type]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Enum_DTSB_Type table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Enum_DTSB_Type] (
			[Enum]        INT        NOT NULL,
			[Name]        [varchar] (50) NULL,
			[Description] [varchar] (50) NULL,    
		 CONSTRAINT [PK_Enum_DTSB_Type] PRIMARY KEY CLUSTERED 
		(
			[Enum] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Enum_DTSB_Type table created'
	END
END
ELSE
BEGIN 
	BEGIN
		PRINT 'Enum_DTSB_Type table exists'
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_DTSB_Type', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_DTSB_Type table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_DTSB_Type CONSTRAINT from Enum_DTSB_Type table
			ALTER TABLE [dbo].[Enum_DTSB_Type] DROP CONSTRAINT [chk_read_only_Enum_DTSB_Type]
			END
		END
	END
END
GO
--Delete data from table
BEGIN
        BEGIN
            PRINT 'Delete records from Enum_DTSB_Type table'
        END
        BEGIN
			DELETE FROM Enum_DTSB_Type
        END
END
GO
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate Enum_DTSB_Type table'
        END
        BEGIN
        INSERT INTO [Enum_DTSB_Type] ([Enum],[Name],[Description]) VALUES (0,'Director','Director');
        INSERT INTO [Enum_DTSB_Type] ([Enum],[Name],[Description]) VALUES (1,'Trustee','Trustee');
		INSERT INTO [Enum_DTSB_Type] ([Enum],[Name],[Description]) VALUES (2,'Shareholder','Shareholder');  
		INSERT INTO [Enum_DTSB_Type] ([Enum],[Name],[Description]) VALUES (3,'Beneficiary','Beneficiary');      
        END
END
GO
--Protect table
BEGIN
	BEGIN
		PRINT 'Protect Enum_DTSB_Type table'
    END
    BEGIN		
		ALTER TABLE [dbo].[Enum_DTSB_Type] WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_DTSB_Type CHECK( 1 = 0 )				
	END
END
GO
--****************************************************************
--Create Enum_Classification table
--Test if table exists
if(object_id(N'[dbo].[Enum_Classification]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create Enum_Classification table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[Enum_Classification] (
			[Enum]        INT        NOT NULL,
			[Name]        [varchar] (50) NULL,
			[Description] [varchar] (50) NULL,    
		 CONSTRAINT [PK_Enum_Classification] PRIMARY KEY CLUSTERED 
		(
			[Enum] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'Enum_Classification table created'
	END
END
ELSE
BEGIN 
	BEGIN
		PRINT 'Enum_Classification table exists'
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_Classification', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_Classification table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_Classification CONSTRAINT from Enum_Classification table
			ALTER TABLE [dbo].[Enum_Classification] DROP CONSTRAINT [chk_read_only_Enum_Classification]
			END
		END
	END
END
GO
--Delete data from table
BEGIN
	--check if FK_Enum_EnquiryType_Enum_Classification exists
    IF OBJECT_ID('FK_Enum_EnquiryType_Enum_Classification', 'F') IS NOT NULL
	BEGIN
		--remove Enum_EnquiryType protection			
		--Check constraint exists
		IF OBJECT_ID('chk_read_only_Enum_EnquiryType', 'C') IS NOT NULL  
		BEGIN
			BEGIN			
				PRINT 'Un-protect Enum_EnquiryType table'
			END		
			BEGIN
			-- remove chk_read_only_Enum_EnquiryType CONSTRAINT from Enum_EnquiryType table
			ALTER TABLE [dbo].[Enum_EnquiryType] DROP CONSTRAINT [chk_read_only_Enum_EnquiryType]
			END
			BEGIN
				PRINT 'Drop constraint chk_read_only_Enum_EnquiryType'
			END
		END	
		BEGIN
			BEGIN
				-- remove FK_Enum_EnquiryType_Enum_Classification CONSTRAINT from Enum_EnquiryType table
				ALTER TABLE [dbo].[Enum_EnquiryType] DROP CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification]
			END
			BEGIN
				PRINT 'Drop constraint FK_Enum_EnquiryType_Enum_Classification'
			END
		END			
	END	
END
GO
BEGIN	
	BEGIN
        PRINT 'Try to delete records from Enum_Classification table'
    END
    BEGIN
		DELETE FROM Enum_Classification
    END	
	BEGIN
        PRINT 'Deleted records from Enum_Classification table'
    END	
	--BEGIN
	--IF OBJECT_ID('FK_Enum_EnquiryType_Enum_Classification', 'F') IS NULL
	--	BEGIN
	--		PRINT 'Try to add Constraint FK_Enum_EnquiryType_Enum_Classification'
	--	END
	--	BEGIN			
	--	--add Enum_EnquiryType foreign key back
	--		ALTER TABLE [dbo].[Enum_EnquiryType]  WITH CHECK ADD  CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification] FOREIGN KEY([Classification])
	--		REFERENCES [dbo].[Enum_Classification] ([Enum])
	--	END
	--	BEGIN
	--		ALTER TABLE [dbo].[Enum_EnquiryType] CHECK CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification]
	--	END
	--	BEGIN
	--		PRINT 'Added Constraint FK_Enum_EnquiryType_Enum_Classification'
	--	END	
	--END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_EnquiryType', 'C') IS NULL  
		BEGIN	
		--add Enum_EnquiryType readOnly constraint	
			ALTER TABLE [dbo].Enum_EnquiryType WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_EnquiryType CHECK( 1 = 0 )
		END	
		BEGIN
			PRINT 'Added Constraint chk_read_only_Enum_EnquiryType'
		END				
	END
END
GO
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate Enum_Classification table'
        END
        BEGIN
        INSERT INTO [Enum_Classification] ([Enum],[Name],[Description]) VALUES (0,'BranchPersonalLoansSecured','Branch Personal Loans Secured');
        INSERT INTO [Enum_Classification] ([Enum],[Name],[Description]) VALUES (1,'BranchPersonalLoansUnSecured','Branch Personal Loans Un-secured'); 
		INSERT INTO [Enum_Classification] ([Enum],[Name],[Description]) VALUES (2,'BranchBusinessLoans','Branch Business Loans');  
		INSERT INTO [Enum_Classification] ([Enum],[Name],[Description]) VALUES (3,'ConsumerGoodsLoans','Consumer Goods Loans');  
		INSERT INTO [Enum_Classification] ([Enum],[Name],[Description]) VALUES (4,'MotorVehicleLoans','Motor Vehicle Loans');    
        END
END
GO
--Protect table
BEGIN
	BEGIN
		PRINT 'Protect Enum_Classification table'
    END
    BEGIN		
		ALTER TABLE [dbo].[Enum_Classification] WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_Classification CHECK( 1 = 0 )				
	END
END
GO
--****************************************************************
--Create Enum_EnquiryType] table (should already exist)
--Pre-requisite Enum_Classification table
if (object_id(N'[dbo].[Enum_Classification]', 'U') IS NOT NULL)
BEGIN
	--Test if table exists
	if (object_id(N'[dbo].[Enum_EnquiryType]', 'U') IS NULL)
	BEGIN --table does not exist
		BEGIN
			PRINT 'Create Enum_EnquiryType] table'
		END
		BEGIN
			SET ANSI_NULLS ON
		END
		BEGIN
			SET QUOTED_IDENTIFIER ON
		END
		BEGIN
			SET ANSI_PADDING ON
		END
		BEGIN
			CREATE TABLE [dbo].[Enum_EnquiryType](
				[Enum] [int] NOT NULL,
				[Name] [varchar](50) NULL,
				[Description] [varchar](50) NULL,
				[Classification] [int] NOT NULL,
			 CONSTRAINT [PK_Enum_EnquiryType] PRIMARY KEY CLUSTERED 
			(
				[Enum] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
		BEGIN
			SET ANSI_PADDING OFF
		END
		BEGIN
			ALTER TABLE [dbo].[Enum_EnquiryType]  WITH CHECK ADD  CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification] FOREIGN KEY([Classification])
			REFERENCES [dbo].[Enum_Classification] ([Enum])
		END
		BEGIN
			ALTER TABLE [dbo].[Enum_EnquiryType] CHECK CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification]
		END
		BEGIN
			PRINT'Enum_EnquiryType table created'
		END
	END
	ELSE --table exists
	BEGIN 
		BEGIN
			PRINT 'Enum_EnquiryType table exists'
		END
		BEGIN
		--Check constraint exists
		IF NOT OBJECT_ID ('chk_read_only_Enum_EnquiryType', 'C') IS NULL  
			BEGIN
				BEGIN			
					PRINT 'Un-protect Enum_EnquiryType table'
				END		
				BEGIN
				-- remove chk_read_only_Enum_EnquiryType CONSTRAINT from Enum_EnquiryType table
				ALTER TABLE [dbo].[Enum_EnquiryType] DROP CONSTRAINT [chk_read_only_Enum_EnquiryType]
				END
			END
		END
	END
END
GO
if (object_id(N'[dbo].[Enum_Classification]', 'U') IS NOT NULL)
BEGIN	
	--Delete data from table
	BEGIN
			BEGIN
				PRINT 'Delete records from Enum_EnquiryType table'
			END
			BEGIN
				DELETE FROM Enum_EnquiryType
			END
	END
	--Alter table, add Classification column
	if NOT exists(select * from sys.columns 
				where Name = N'Classification' and Object_ID = Object_ID(N'Enum_EnquiryType'))
	BEGIN
		-- Column does not Exist
		BEGIN
			PRINT 'Add Classification field to Enum_EnquiryType table'
		END
		BEGIN
			-- add new column to table
			ALTER TABLE Enum_EnquiryType
			ADD [Classification] [int] NOT NULL
		END
	END	
	--Check Foreign Key exists
	BEGIN
		IF OBJECT_ID ('FK_Enum_EnquiryType_Enum_Classification', 'F') IS NULL
		BEGIN
			BEGIN
				ALTER TABLE [dbo].[Enum_EnquiryType]  WITH CHECK ADD  CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification] FOREIGN KEY([Classification])
				REFERENCES [dbo].[Enum_Classification] ([Enum])
			END
			BEGIN
				ALTER TABLE [dbo].[Enum_EnquiryType] CHECK CONSTRAINT [FK_Enum_EnquiryType_Enum_Classification]
			END
		END
	END
END
GO
if (object_id(N'[dbo].[Enum_Classification]', 'U') IS NOT NULL)
BEGIN
	--Populate table
	BEGIN
			BEGIN
				PRINT 'Populate Enum_EnquiryType table'
			END
			BEGIN
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (0,'PersonalLoanNew','Personal Loan (New)',0)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (1,'PersonalLoanRefinance','Personal Loan (Refinance)',0)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (2,'PersonalLoanVariation','Personal Loan (Variation)',0)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (3,'PersonalSplitLoanDrawdown','Personal Split Loan Drawdown',0)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (4,'ConsumerRetail','Consumer Retail',3)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (5,'ConsumerVehicle','Consumer Vehicle',4)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (6,'BusinessLoanNew','Business Loan (New)',2)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (7,'BusinessLoanRefinance','Business Loan (Refinance)',2)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (8,'BusinessLoanVariation','Business Loan (Variation)',2)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (9,'BusinessSplitLoanDrawdown','Business Split Loan Drawdown',2)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (10,'UnsecuredPersonalLoanNew','Unsecured Personal Loan (New)',1)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (11,'UnsecuredPersonalLoanRefinance','Unsecured Personal Loan (Refinance)',1)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (12,'UnsecuredPersonalLoanVariation','Unsecured Personal Loan (Variation)',1)
			INSERT INTO [dbo].[Enum_EnquiryType] ([Enum],[Name],[Description],[Classification]) VALUES (13,'UnsecuredPersonalSplitLoanDrawdown','Unsecured Personal Split Loan Drawdown',1)
			END
	END
	--Protect table
	BEGIN
		BEGIN
			PRINT 'Protect Enum_EnquiryType table'
		END
		BEGIN		
			ALTER TABLE [dbo].Enum_EnquiryType WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_EnquiryType CHECK( 1 = 0 )				
		END
	END
END
ELSE
BEGIN
	PRINT 'ERROR: Enum_Classification table does not exist *****'
END
GO
--****************************************************************

--Alter Enquiry table
PRINT '**************************'
GO
PRINT'Alter Enquiry table,'
GO
PRINT'add fields for Contact''s name, displayName, suburb, city, phoneNumber, emailAddress'
GO
PRINT 'and add mainCustomerType'
GO
PRINT '****'
GO
--add ContactTitle column
if NOT exists(select * from sys.columns 
            where Name = N'ContactTitle' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactTitle field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactTitle varchar(40) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactTitle field already exists in Enquiry table'
END
GO
--add ContactFirstName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactFirstName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactFirstName field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactFirstName varchar(40) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactFirstName field already exists in Enquiry table'
END
GO
--add ContactLastName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactLastName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactLastName field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactLastName varchar(40) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactLastName field already exists in Enquiry table'
END
GO
--add ContactDisplayName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactDisplayName' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactDisplayName field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactDisplayName varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactDisplayName field already exists in Enquiry table'
END
GO
--add ContactSuburb column
if NOT exists(select * from sys.columns 
            where Name = N'ContactSuburb' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactSuburb field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactSuburb varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactSuburb field already exists in Enquiry table'
END
GO
--add ContactCity column
if NOT exists(select * from sys.columns 
            where Name = N'ContactCity' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactCity field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactCity varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactCity field already exists in Enquiry table'
END
GO
--add ContactPhone column
if NOT exists(select * from sys.columns 
            where Name = N'ContactPhone' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactPhone field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactPhone varchar(20) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactPhone field already exists in Enquiry table'
END
GO
--add ContactEmail column
if NOT exists(select * from sys.columns 
            where Name = N'ContactEmail' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactEmail field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD ContactEmail varchar(125) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactEmail field already exists in Enquiry table'
END
GO
--add BranchId column
if NOT exists(select * from sys.columns 
            where Name = N'BranchId' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add BranchId field to Enquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE Enquiry
		ADD BranchId varchar(5) NULL
	END		
END
ELSE
BEGIN
	PRINT 'BranchId field already exists in Enquiry table'
END
GO
--add mainCustomerType field
IF NOT EXISTS(select * from sys.columns 
            where Name = N'mainCustomerType' and Object_ID = Object_ID(N'Enquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Adding mainCustomerType field to the Enquiry table'
	END
	BEGIN					
		ALTER TABLE Enquiry 
		ADD mainCustomerType int NULL
	END
END
ELSE
BEGIN
	PRINT 'mainCustomerType field already exists in Enquiry table'
END
GO
--****
PRINT '****'
GO
PRINT 'Alter Enquiry table finished'
GO
PRINT '**************************'
GO
--***********************************************
PRINT'Start Alter ArchiveEnquiry table,'
GO
PRINT'add fields for BranchId'
GO
PRINT '****'
GO
--add BranchId column
if NOT exists(select * from sys.columns 
            where Name = N'BranchId' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add BranchId field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD BranchId varchar(5) NULL
	END		
END
ELSE
BEGIN
	PRINT 'BranchId field already exists in ArchiveEnquiry table'
END
GO
--add ContactDisplayName column
if NOT exists(select * from sys.columns 
            where Name = N'ContactDisplayName' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add ContactDisplayName field to ArchiveEnquiry table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE ArchiveEnquiry
		ADD ContactDisplayName varchar(50) NULL
	END		
END
ELSE
BEGIN
	PRINT 'ContactDisplayName field already exists in ArchiveEnquiry table'
END
GO
PRINT '****'
GO
PRINT 'Alter ArchiveEnquiry table finished'
GO
PRINT '**************************'
GO
--***********************************************
--Alter ActiveClientEnquiries view
DECLARE @SQLAlterView nvarchar(max) = '
ALTER VIEW [dbo].[ActiveClientEnquiries]
--30/06/2016 removed dbo.Enquiry.ClientId, dbo.Client.Salutation, dbo.Client.BranchId
--30/06/2016 added dbo.Enquiry.BranchId, dbo.Enquiry.ContactDisplayName
AS
SELECT TOP (100) PERCENT dbo.Enquiry.EnquiryCode, dbo.Enquiry.ApplicationCode, dbo.Enquiry.ContactDisplayName, dbo.Enquiry.[DateTime]
    ,dbo.Enquiry.ActiveStatus,dbo.Enquiry.BranchId,dbo.Enquiry.EnquiryManagerId,dbo.Users.UserName,dbo.Enquiry.CurrentStatus, dbo.Enquiry.EnquiryId
	FROM         dbo.Enquiry INNER JOIN dbo.Users ON dbo.Enquiry.EnquiryManagerId = dbo.Users.UserId
	ORDER BY dbo.Enquiry.[DateTime]'
--Test if view exists
if(object_id(N'[dbo].[ActiveClientEnquiries]', 'V') IS NOT NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Alter ActiveClientEnquiries view'
	END
	BEGIN
		EXEC (@SQLAlterView);
	END	
	BEGIN
		PRINT'ActiveClientEnquiries view altered'
	END
END
ELSE
BEGIN 
	PRINT 'ActiveClientEnquiries view does not exist!'	
END
GO
--***********************************************
--Alter DisplayAllNames view
DECLARE @SQLAlterView2 nvarchar(max) = '
ALTER VIEW [dbo].[DisplayAllNames]
--30/06/2016 written by Christopher
--altered the view to take intop account the new database schema
AS
SELECT TOP (100) PERCENT SubData.ContactDisplayName, dbo.Customer.LastName, dbo.Customer.FirstName, dbo.Customer.Suburb, dbo.Customer.City, SubData.[DateTime], SubData.CurrentStatus
		, SubData.EnquiryCode, SubData.ApplicationCode, SubData.Archived, SubData.EnquiryId
FROM   (SELECT Enquiry.EnquiryId, Enquiry.EnquiryCode, Enquiry.[DateTime], Enquiry.CurrentStatus, Enquiry.Archived, Enquiry.ApplicationCode
		, Enquiry.ContactDisplayName, CustomerEnquiry.Type, CustomerEnquiry.CustomerId
        FROM dbo.Enquiry 
			INNER JOIN dbo.CustomerEnquiry ON dbo.Enquiry.EnquiryId = dbo.CustomerEnquiry.EnquiryId
        UNION
        SELECT ArchiveEnquiry.EnquiryId, ArchiveEnquiry.EnquiryCode, ArchiveEnquiry.[DateTime], ArchiveEnquiry.CurrentStatus, ArchiveEnquiry.Archived, ArchiveEnquiry.ApplicationCode
		,ArchiveEnquiry.ContactDisplayName, CustomerArchiveEnquiry.Type, CustomerArchiveEnquiry.CustomerId
        FROM dbo.ArchiveEnquiry 
			INNER JOIN dbo.CustomerArchiveEnquiry ON dbo.ArchiveEnquiry.EnquiryId = dbo.CustomerArchiveEnquiry.EnquiryId) AS SubData 
			INNER JOIN dbo.Customer ON SubData.CustomerId = dbo.Customer.CustomerId
WHERE   (SubData.Type = 0) -- 0 = main
ORDER BY dbo.Customer.LastName, dbo.Customer.FirstName'
--Test if view exists
if(object_id(N'[dbo].[DisplayAllNames]', 'V') IS NOT NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Alter DisplayAllNames view'
	END
	BEGIN
		EXEC (@SQLAlterView2);
	END	
	BEGIN
		PRINT'DisplayAllNames view altered'
	END
END
ELSE
BEGIN 
	PRINT 'DisplayAllNames view does not exist!'	
END
GO
--****************************************************************
--Create AppSettings table (added 31/10/2016)
if(object_id(N'[dbo].[AppSettings]', 'U') IS NULL)
BEGIN --table does not exist
	BEGIN
		PRINT 'Create AppSettings table'
	END
	BEGIN
		SET ANSI_NULLS ON
	END
	BEGIN
		SET QUOTED_IDENTIFIER ON
	END
	BEGIN
		SET ANSI_PADDING ON
	END
	BEGIN
		CREATE TABLE [dbo].[AppSettings](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](50) NULL,
		[Decimal] [decimal](18, 4) NULL,
		[Text] [varchar](500) NULL,
		[Boolean] [bit] NULL,
		[Comments] [varchar](250) NULL,
		CONSTRAINT [PK_AppSettings] PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		PRINT'AppSettings table created'
	END
END
ELSE
BEGIN 
	PRINT 'AppSettings table already exists'	
END
GO
--Delete data from table
BEGIN
        BEGIN
            PRINT 'Delete records from AppSettings table'
        END
        BEGIN
			DELETE FROM AppSettings
        END
END
GO
--add [Boolean] column
if NOT exists(select * from sys.columns 
            where Name = N'Boolean' and Object_ID = Object_ID(N'AppSettings'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Add Boolean field to AppSettings table'
	END
	BEGIN
		-- add new column to table
		ALTER TABLE AppSettings
		ADD Boolean [bit] NULL
	END		
END
ELSE
BEGIN
	PRINT 'Boolean field already exists in AppSettings table'
END
GO
--Populate table
BEGIN
        BEGIN
			PRINT 'Populate AppSettings table'
        END
		BEGIN
			SET IDENTITY_INSERT [AppSettings] ON
		END
        BEGIN
        INSERT INTO [AppSettings] ([Id],[Name],[Decimal],[Text],[Boolean],[Comments]) VALUES (1,'DependSingle',800.00,NULL,NULL,'Monthly dependancy amount for a single person');
		INSERT INTO [AppSettings] ([Id],[Name],[Decimal],[Text],[Boolean],[Comments]) VALUES (2,'DependCouple',1000.00,NULL,NULL,'Monthly dependancy amount for a couple');
		INSERT INTO [AppSettings] ([Id],[Name],[Decimal],[Text],[Boolean],[Comments]) VALUES (3,'DependChild',200.00,NULL,NULL,'Monthly dependancy amount for a child');
		INSERT INTO [AppSettings] ([Id],[Name],[Decimal],[Text],[Boolean],[Comments]) VALUES (4,'SystemCanImportCustomers',NULL,NULL,1,'Whether current application can import customers from 3rd party (OAC)');
        END
		BEGIN
			SET IDENTITY_INSERT [AppSettings] OFF
		END
END
GO


--Finished
PRINT '****************************'
GO
PRINT 'Database script finished'
GO












