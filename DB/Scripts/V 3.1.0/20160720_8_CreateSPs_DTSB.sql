--USE DevTrueTrack2
USE EnquiryWorkSheet
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT'Start database script'
GO
PRINT '*** Create/Alter Stored procedure DirectorTrusteeShareHolderBeneficiary ***'
GO
--******************************************
--GetDTSBs
--******************************************
DECLARE @SQLCreateSP_GetDTSBs nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
CREATE PROCEDURE [dbo].[GetDTSBs]
	@CustomerId int = 0,
	@DTSBtype int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.Type, 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, Enum_DTSB_Type.Description
	FROM   DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.Type = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId
	WHERE      (DirectorTrusteeShareHolderBeneficiary.CustomerId = @CustomerId) AND (DirectorTrusteeShareHolderBeneficiary.Type = @DTSBtype)

END'
--******
DECLARE @SQLAlterSP_GetDTSBs nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
ALTER PROCEDURE [dbo].[GetDTSBs]
	@CustomerId int = 0,
	@DTSBtype int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.Type, 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, Enum_DTSB_Type.Description
	FROM   DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.Type = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId
	WHERE  (DirectorTrusteeShareHolderBeneficiary.CustomerId = @CustomerId) AND (DirectorTrusteeShareHolderBeneficiary.Type = @DTSBtype)

END'
--******
IF OBJECT_ID(N'dbo.GetDTSBs', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure GetDTSBs'
	END
	BEGIN
		EXEC (@SQLCreateSP_GetDTSBs);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure GetDTSBs'
	END
	BEGIN
		EXEC (@SQLAlterSP_GetDTSBs);
	END
END
GO
--******************************************
--GetSingleDTSB
--******************************************
DECLARE @SQLCreateSP_GetSingleDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
CREATE PROCEDURE [dbo].[GetSingleDTSB]
		@recordId int = 0	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.Type, 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, Enum_DTSB_Type.Description
	FROM       DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.Type = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId
	WHERE      (DirectorTrusteeShareHolderBeneficiary.Id = @recordId)
END'
--******
DECLARE @SQLAlterSP_GetSingleDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
ALTER PROCEDURE [dbo].[GetSingleDTSB]
		@recordId int = 0	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT DirectorTrusteeShareHolderBeneficiary.Id, DirectorTrusteeShareHolderBeneficiary.CustomerId, DirectorTrusteeShareHolderBeneficiary.Type, 
			DirectorTrusteeShareHolderBeneficiary.FirstName, DirectorTrusteeShareHolderBeneficiary.LastName, DirectorTrusteeShareHolderBeneficiary.DateOfBirth, 
			DirectorTrusteeShareHolderBeneficiary.IdVerified, DirectorTrusteeShareHolderBeneficiary.AddressVerified, Enum_DTSB_Type.Description
	FROM       DirectorTrusteeShareHolderBeneficiary INNER JOIN
           Enum_DTSB_Type ON DirectorTrusteeShareHolderBeneficiary.Type = Enum_DTSB_Type.Enum INNER JOIN
           Customer ON DirectorTrusteeShareHolderBeneficiary.CustomerId = Customer.CustomerId
	WHERE      (DirectorTrusteeShareHolderBeneficiary.Id = @recordId)
END'
--******
IF OBJECT_ID(N'dbo.GetSingleDTSB', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure GetSingleDTSB'
	END
	BEGIN
		EXEC (@SQLCreateSP_GetSingleDTSB);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure GetSingleDTSB'
	END
	BEGIN
		EXEC (@SQLAlterSP_GetSingleDTSB);
	END
END
GO
--******************************************
--InsertDTSB
--******************************************
DECLARE @SQLCreateSP_InsertDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
CREATE PROCEDURE [dbo].[InsertDTSB]	
	@Type int = 0,
	@CustomerId int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO DirectorTrusteeShareHolderBeneficiary ([Type], CustomerId, FirstName, LastName, DateOfBirth, IdVerified, AddressVerified)
	VALUES(@Type, @CustomerId, @FirstName, @LastName, @DateOfBirth, @IdVerified, @AddressVerified)
	--Get back the Id
	SELECT @Id = Id
	FROM DirectorTrusteeShareHolderBeneficiary
	WHERE @@ROWCOUNT > 0 AND Id = SCOPE_IDENTITY()	
END'
--******
DECLARE @SQLAlterSP_InsertDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
ALTER PROCEDURE [dbo].[InsertDTSB]	
	@Type int = 0,
	@CustomerId int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO DirectorTrusteeShareHolderBeneficiary ([Type], CustomerId, FirstName, LastName, DateOfBirth, IdVerified, AddressVerified)
	VALUES(@Type, @CustomerId, @FirstName, @LastName, @DateOfBirth, @IdVerified, @AddressVerified)
	--Get back the Id
	SELECT @Id = Id
	FROM DirectorTrusteeShareHolderBeneficiary
	WHERE @@ROWCOUNT > 0 AND Id = SCOPE_IDENTITY()	
END'
--******
IF OBJECT_ID(N'dbo.InsertDTSB', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure InsertDTSB'
	END
	BEGIN
		EXEC (@SQLCreateSP_InsertDTSB);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure InsertDTSB'
	END
	BEGIN
		EXEC (@SQLAlterSP_InsertDTSB);
	END
END
GO
--******************************************
--UpdateDTSB
--******************************************
DECLARE @SQLCreateSP_UpdateDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDTSB]
	@Id int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0
AS
BEGIN
	UPDATE  DirectorTrusteeShareHolderBeneficiary
	SET     FirstName = @FirstName, 
			LastName = @LastName, 			
			DateOfBirth = @DateOfBirth, 
			IdVerified = @IdVerified, 
			AddressVerified = @AddressVerified
	WHERE   (Id = @Id)
END'
--******
DECLARE @SQLAlterSP_UpdateDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
ALTER PROCEDURE [dbo].[UpdateDTSB]
	@Id int = 0,
	@FirstName nvarchar(40),
	@LastName nvarchar(40),
	@DateOfBirth date,
	@IdVerified bit = 0,
	@AddressVerified bit = 0
AS
BEGIN
	UPDATE  DirectorTrusteeShareHolderBeneficiary
	SET     FirstName = @FirstName, 
			LastName = @LastName, 			
			DateOfBirth = @DateOfBirth, 
			IdVerified = @IdVerified, 
			AddressVerified = @AddressVerified
	WHERE   (Id = @Id)
END'
--******
IF OBJECT_ID(N'dbo.UpdateDTSB', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure UpdateDTSB'
	END
	BEGIN
		EXEC (@SQLCreateSP_UpdateDTSB);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure UpdateDTSB'
	END
	BEGIN
		EXEC (@SQLAlterSP_UpdateDTSB);
	END
END
GO
--******************************************
--DeleteDTSB
--******************************************
DECLARE @SQLCreateSP_DeleteDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDTSB]
	@Id int = 0	
AS
BEGIN
	DELETE FROM DirectorTrusteeShareHolderBeneficiary
	WHERE        (Id = @Id)
END'
--******
DECLARE @SQLAlterSP_DeleteDTSB nvarchar(max) = '
-- =============================================
-- Author:		Christopher Steel
-- Create date: 20/07/2016
-- Description:	Create Stored procedures for DirectorTrusteeShareHolderBeneficiary
-- =============================================
ALTER PROCEDURE [dbo].[DeleteDTSB]
	@Id int = 0	
AS
BEGIN
	DELETE FROM DirectorTrusteeShareHolderBeneficiary
	WHERE        (Id = @Id)
END'
--******
IF OBJECT_ID(N'dbo.DeleteDTSB', N'P') IS NULL
BEGIN
	BEGIN
		PRINT'Create Stored Procedure DeleteDTSB'
	END
	BEGIN
		EXEC (@SQLCreateSP_DeleteDTSB);
	END
END
ELSE
BEGIN
	BEGIN
		PRINT'Alter Stored Procedure DeleteDTSB'
	END
	BEGIN
		EXEC (@SQLAlterSP_DeleteDTSB);
	END
END
GO
--******************************************
--End
PRINT'*********************************************************'
GO
PRINT'Database script finished'
GO
