--Alter ArchiveEnquiry table, add fields ContactSuburb and ContactCity to the ArchiveEnquiry table
--*****************************
--12/12/2016: Christopher: First write
--*****************************
Use TestEnquiryWorkSheet
--USE DevTrueTrack2
--USE EnquiryWorkSheet
GO

PRINT 'Starting database script'
GO
PRINT'Alter ArchiveEnquiry table, add fields to the ArchiveEnquiry table'
GO
PRINT '**************************'
GO
--check fields have not been added already
IF NOT exists(select * from sys.columns 
            where Name = N'ContactSuburb' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Adding ContactSuburb field to the ArchiveEnquiry table'
	END
	BEGIN
		ALTER TABLE ArchiveEnquiry 
		ADD ContactSuburb varchar(50) NULL				
	END	
END
ELSE
BEGIN
	PRINT 'ContactCity already exists in the ArchiveEnquiry table'
END
GO
IF NOT exists(select * from sys.columns 
            where Name = N'ContactCity' and Object_ID = Object_ID(N'ArchiveEnquiry'))
BEGIN
    -- Column does not Exist
	BEGIN
		PRINT 'Adding ContactCity field to the ArchiveEnquiry table'
	END
	BEGIN
		ALTER TABLE ArchiveEnquiry 
		ADD ContactCity varchar(50) NULL				
	END	
END
ELSE
BEGIN
	PRINT 'ContactCity already exists in the ArchiveEnquiry table'
END
GO
PRINT '****************************'
GO
PRINT'Database script completed'
GO