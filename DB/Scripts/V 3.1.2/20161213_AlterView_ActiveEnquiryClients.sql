USE [TestEnquiryWorkSheet]
GO

/****** Object:  View [dbo].[ActiveEnquiryClients]    Script Date: 13/12/2016 6:17:01 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[ActiveEnquiryClients]
--17/08/2016 Christopher: Changed Client details to Enquiry Contact details
--13/12/2016 Christopher: Changed WHERE (dbo.Enquiry.ActiveStatus = 1), to (dbo.Enquiry.Archived = 0)
AS
SELECT     dbo.Enquiry.ContactFirstName, dbo.Enquiry.ContactLastName, dbo.Enquiry.ContactSuburb, dbo.Enquiry.ContactCity, dbo.Enquiry.EnquiryId, dbo.Enquiry.EnquiryCode, dbo.Enquiry.ApplicationCode, 
                      dbo.Enquiry.DateTime, dbo.Enquiry.CurrentStatus
FROM         dbo.Enquiry 
WHERE     (dbo.Enquiry.Archived = 0)


GO


