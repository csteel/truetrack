--Create Trigger [dbo].[StatusHistory]  
--Written by Christopher: 31/01/2017
--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
PRINT'Starting database script'
GO
PRINT'************************'
GO

--Drop TRIGGER
IF OBJECT_ID (N'tr_Enquiry_update' , N'TR' ) IS NOT NULL
BEGIN
	DROP TRIGGER [tr_Enquiry_update];
END
BEGIN
	PRINT'Dropped tr_Enquiry_update trigger'
END
GO
--Create TRIGGER
CREATE TRIGGER [dbo].[tr_Enquiry_update]
	ON Enquiry
	AFTER UPDATE
	AS
	--Make sure CurrentStatus has changed
	IF NOT UPDATE(CurrentStatus)
	BEGIN
		RETURN
	END 
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM inserted a 
		JOIN deleted b ON a.EnquiryId = b.EnquiryId
		WHERE b.CurrentStatus <> a.CurrentStatus)
		BEGIN
			DECLARE @EnquiryId int
			DECLARE @Status varchar(20)
			DECLARE @dateTimeNow datetime
			SET @EnquiryId = (SELECT EnquiryId from inserted)
			SET @Status = (SELECT CurrentStatus from inserted)
			SET @dateTimeNow = getdate()
			INSERT INTO StatusHistory (EnquiryId,[Status],StatusChangeDate)
			VALUES (@EnquiryId,@Status,@dateTimeNow);
		END
	END
GO
--end of Create
IF OBJECT_ID (N'tr_Enquiry_update' , N'TR' ) IS NOT NULL
BEGIN
	PRINT'tr_Enquiry_update trigger created'
END
GO

PRINT'************************'
GO
PRINT'Database script finished'
GO

