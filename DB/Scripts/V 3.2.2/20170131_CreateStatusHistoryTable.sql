--Create Table [dbo].[StatusHistory]  
--Written by Christopher: 31/01/2017
--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
PRINT'Starting database script'
GO
PRINT'************************'
GO

--Create table
IF OBJECT_ID (N'StatusHistory' , N'U' ) IS NULL
BEGIN
	 -- table does not exist
    BEGIN
         PRINT 'Create StatusHistory table'
    END	
    BEGIN
		SET ANSI_NULLS ON
    END
    BEGIN
		SET QUOTED_IDENTIFIER ON
    END    
    BEGIN
		CREATE TABLE [dbo].[StatusHistory](
       [Id] [INT] identity NOT NULL, --Self explanatory
	   [EnquiryId] [int] NOT NULL,
       [Status] [VARCHAR](20) NULL, --the currentStatus    
	   [StatusChangeDate] [datetime] NULL   -- the datetime status was updated (Date.Now)
		CONSTRAINT [PK_Id] PRIMARY KEY NONCLUSTERED 
		(
			   [Id] ASC
		)
		)		
	END	
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		CREATE CLUSTERED INDEX [IX_StatusHistory] ON [dbo].[StatusHistory]
		(
			[StatusChangeDate] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	END
END
ELSE
BEGIN
	PRINT 'StatusHistory table already exits'
END
GO
--end of Create

PRINT'************************'
GO
PRINT'Database script finished'
GO

