--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
--19/05/2017
--Transform data for new EnquiryComments table
--To be run immediately after creating the new EnquiryComments table
--written by Christopher 19/05/2017
--13/06/2017: added Truncate Table section

PRINT 'Starting database script'
GO
PRINT '**************************'
GO
PRINT'Transform data for new EnquiryComment table'
GO

--Copy data from Enquiry.LoanComments data to new EnquiryComments table
IF OBJECT_ID (N'EnquiryComment' , N'U' ) IS NOT NULL
BEGIN
	BEGIN
		PRINT 'Truncate  LoanComments table'
	END
	BEGIN
		Truncate Table EnquiryComment
	END
	BEGIN
		PRINT '**************************'
	END
	BEGIN
		-- Column exists
		DECLARE @enquiryId int
		DECLARE @userId varchar(255)
		DECLARE @dateCreated datetime
		DECLARE @text varchar(max)
		DECLARE @enquiryCount int

		SET @enquiryCount = 0
		SET @userId = 'System'
		SET @dateCreated = '2017-05-23T00:00:00.000'

		DECLARE cur CURSOR LOCAL for
			select e.EnquiryId, e.LoanComments
				From Enquiry e	
				Order by e.[DateTime] Asc			
		OPEN cur

		fetch next from cur into @enquiryId, @text
		while @@FETCH_STATUS = 0 
		BEGIN
			--execute  on each row		
			BEGIN
				--PRINT 'EnquiryId ' + CAST(@enquiryId AS VARCHAR)  
				INSERT INTO EnquiryComment ([EnquiryId],[UserId],[DateCreated],[Text])
				VALUES (@enquiryId,@userId,@dateCreated,@text);						
			END	
		
			SELECT @enquiryCount = @enquiryCount + 1;	
			fetch next from cur into  @enquiryId, @text

		END

		close cur
		deallocate cur

		PRINT 'Data transformed for ' + CAST(@enquiryCount AS VARCHAR)    + ' enquiries'
		-- finished transforming existing data!!!!! ***************************************************
	
	END
END
ELSE
BEGIN
	PRINT 'EnquiryComment table does not exist'
END
GO
PRINT '**************************'
GO
--End
PRINT 'DB script completed'
GO
