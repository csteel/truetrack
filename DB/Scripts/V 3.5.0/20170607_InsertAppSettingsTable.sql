-- Add an AppSetting to the AppSettings table
-- Christopher Steel 07/06/2017
USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('SystemBusinessName'
           ,0
           ,'Yes Finance Ltd'
           ,0
           ,'System Business Name used in email signature etc')
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('SystemBusinessPhone'
           ,0
           ,'09 278 9161'
           ,0
           ,'System Business Phone Number used in email signature etc')
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('SystemBusinessFax'
           ,0
           ,'09 277 8003'
           ,0
           ,'System Business Fax Number used in email signature etc')
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('SystemBusinessWebsite'
           ,0
           ,'www.yesfinance.co.nz'
           ,0
           ,'System Business Website used in email signature etc')
GO


