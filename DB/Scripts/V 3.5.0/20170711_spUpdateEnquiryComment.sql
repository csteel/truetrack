USE [TestEnquiryWorkSheet]
GO
/****** Object:  StoredProcedure [dbo].[UpdateEnquiryComments]    Script Date: 12/07/2017 2:14:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Christopher
-- Create date: 11/07/2017
-- Description:	Update the Text in EnquiryComments table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateEnquiryComment] 
	-- Add the parameters for the stored procedure here
	@Id int, 
	@Text nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE EnquiryComment
	SET Text = @Text
	WHERE Id = @Id
	
	-- Retreive data form table		
	
	-- SELECT <@Param1, sysname, *>, <@Param2, sysname, @p2>
END
