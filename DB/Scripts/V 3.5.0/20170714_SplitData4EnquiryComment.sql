--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
--19/05/2017
--Transform data for new EnquiryComments table
--To be run immediately after creating the new EnquiryComments table
--written by Christopher 19/05/2017
--13/06/2017: added Truncate Table section
--14/07/2017: added Splitting comments

PRINT 'Starting database script'
GO
PRINT '**************************'
GO
PRINT'Split data for new EnquiryComment table'
GO

--Split data from Enquiry.LoanComments field to a temp table
--Use temp table to split data again and build a insert statement 
--for entering record into EnquiryComment table
IF OBJECT_ID (N'EnquiryComment' , N'U' ) IS NOT NULL
BEGIN
	BEGIN
		PRINT 'Truncate  LoanComments table'
	END
	BEGIN
		Truncate Table EnquiryComment
	END
	BEGIN
		PRINT '**************************'
	END
	BEGIN
		If(OBJECT_ID('tempdb..#SplitComments') Is Not Null)
			Begin
				Drop Table #SplitComments
			End
	END
	BEGIN
	-- Column exists
		--Declare tempory table
		CREATE TABLE #SplitComments ( Id INT IDENTITY(1,1) NOT NULL, Comment NVARCHAR(max) );
		--get comments for each enquiry
		DECLARE @enquiryId int
		DECLARE @commentsText varchar(max)		
		DECLARE cur CURSOR LOCAL for
			select e.EnquiryId, e.LoanComments
				From Enquiry e	
				Order by e.[DateTime] Asc			
		OPEN cur

		fetch next from cur into @enquiryId, @commentsText
		while @@FETCH_STATUS = 0 
			BEGIN
				PRINT '*** EnquiryId = ' + CONVERT(VARCHAR, @enquiryId) + ' ***'
				--execute  on each row	
				INSERT INTO #SplitComments	
				SELECT Item FROM dbo.SplitString(@commentsText, '--')  
				WHERE LTRIM(RTRIM(Item)) <> '';  
				--get text for each row in Item
				DECLARE @id int
				DECLARE @comment nvarchar(max)
				DECLARE cur2 CURSOR LOCAL for
					select sc.Id, sc.Comment
						From #SplitComments sc									
				OPEN cur2
				fetch next from cur2 into @id, @comment
				while @@FETCH_STATUS = 0 
					BEGIN
					--PRINT 'Comment: ' + RTRIM(@comment)
					--put comment into field values (DateCreated, UserId, [Text])
					--and insert into EnquiryComments table
					--@enquiryId already declared
					DECLARE @userId varchar(255)
					DECLARE @dateCreated datetime
					DECLARE @text varchar(max)
					DECLARE @tempDate varchar(255)
					DECLARE @tempMonth varchar(255)
					DECLARE @tempYear varchar(255)
					DECLARE @tempTime varchar(255)
					DECLARE @day int
					DECLARE @month int
					DECLARE @thisDateTime varchar(255)
					DECLARE @convertedDate datetime
					DECLARE @newComment varchar(max)
					DECLARE @startOfText int
					--Comment example
					---- Friday, 14 July 2017 2:48 p.m. | tmaan
					--VERIFICATION: I verify that the Application has passed the Audit
					--Status change to Payout
					--End Comment example

					--get date
					SET @tempDate = LTRIM(RTRIM(SUBSTRING(@comment,CHARINDEX(',',@comment)+1,CHARINDEX('|',@comment)-CHARINDEX(',',@comment)-1)))
					--PRINT 'Date string = ' + @tempDate
					SET @day = CONVERT(INT,SUBSTRING(@tempDate,1,CHARINDEX(' ',@tempDate)))
					--PRINT 'Day = ' + CONVERT(VARCHAR,@day)
					SET @tempMonth = SUBSTRING(@tempDate,CHARINDEX(' ',@tempDate)+1,CHARINDEX(' ',@tempDate,CHARINDEX(' ',@tempDate)+1)-CHARINDEX(' ',@tempDate)-1)
					--PRINT 'Month = ' + @tempMonth
					SET @month = DATEPART(MM,@tempMonth + ' 1 2014')
					--Print 'Month number = ' + CONVERT(VARCHAR, @month)
					SET @tempYear = SUBSTRING(@tempDate,CHARINDEX(' ',@tempDate,CHARINDEX(' ',@tempDate)+1)+1,(CHARINDEX(' ',@tempDate, CHARINDEX(' ',@tempDate,CHARINDEX(' ',@tempDate)+1)+1))-(CHARINDEX(' ',@tempDate,CHARINDEX(' ',@tempDate)+1)+1))
					--PRINT 'Year = ' + @tempYear
					SET @tempTime = SUBSTRING(@tempDate,CHARINDEX(' ',@tempDate, CHARINDEX(' ',@tempDate,CHARINDEX(' ',@tempDate)+1)+1)+1, CHARINDEX('.m.',@tempDate)+2 - CHARINDEX(' ',@tempDate, CHARINDEX(' ',@tempDate,CHARINDEX(' ',@tempDate)+1)+1)+1)
					--PRINT 'Time segment: ' + @tempTime
					SET @thisDateTime = CONVERT(VARCHAR,@day) + '/' + CONVERT(VARCHAR, @month) + '/' + @tempYear  + ' ' + @tempTime
					--PRINT 'DateTime: ' + @thisDateTime
					SET @convertedDate = CONVERT(DATETIME, REPLACE(@thisDateTime, '.', ''), 103)
					SET @dateCreated = @convertedDate
					--PRINT 'Converted Date: ' + CONVERT(VARCHAR,@convertedDate,120)
					--get User
					SET @newComment = REPLACE(@comment , CHAR(13) + CHAR(10) , ' ')
					--PRINT 'New comment: ' + @newComment
					SET @userId = LTRIM(RTRIM(SUBSTRING(@newComment,CHARINDEX('|',@newComment)+2,CHARINDEX(' ',@newComment,CHARINDEX('|',@newComment)+2)-CHARINDEX('|',@newComment)-2)))
					--PRINT 'UserId: ' + @userId
					--get text
					SET @startOfText = CHARINDEX(' ',@newComment,CHARINDEX('|',@newComment)+2) + 1
					--PRINT 'Start of Text = ' + CONVERT(VARCHAR,@startOfText)
					SET @text = SUBSTRING(RTRIM(@comment),@startOfText + 1,LEN(@comment) - (@startOfText + 1))
					DECLARE @reversedText varchar(max) 
					SET @reversedText = REVERSE(@text)
					DECLARE @substringText varchar(max) 
					SET  @substringText = SUBSTRING(@text,1, LEN(@reversedText) - (select patindex('%[a-z]%', @reversedText) - 1))
					
					--PRINT 'Finished Comment: ' + @substringText + ' $$'
					
					---PRINT '*** End ***'

					--Date string = 9 November 2016 12:36 p.m.
					--Day = 9
					--Month = November
					--Month number = 11
					--TempTest = 12
					--TempTest2 = 16
					--Year = 2016
					--Time segment: 10:13 a.m.
					--DateTime: 2/5/2016 10:13 a.m.
					--Converted Date: 2016-05-02 10:13:00

					INSERT INTO EnquiryComment ([EnquiryId],[UserId],[DateCreated],[Text])
					VALUES (@enquiryId,@userId,@dateCreated,@substringText);
						
					fetch next from cur2 into  @id, @comment
					END
				close cur2
				deallocate cur2
				Truncate Table #SplitComments

				fetch next from cur into  @enquiryId, @commentsText
			END
		close cur
		deallocate cur	

		If(OBJECT_ID('tempdb..#SplitComments') Is Not Null)
			Begin
				Drop Table #SplitComments
			End
	
	END
END
ELSE
BEGIN
	PRINT 'EnquiryComment table does not exist'
END
GO
PRINT '**************************'
GO
--End
PRINT 'DB script completed'
GO
