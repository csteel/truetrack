USE [TestEnquiryWorkSheet]
GO
/****** Object:  StoredProcedure [dbo].[InsertEnquiryComment]    Script Date: 12/07/2017 2:37:23 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Christopher Steel
-- Create date: 11/07/2017
-- Description:	Insert a new Enquiry Comment
-- =============================================
CREATE PROCEDURE [dbo].[InsertEnquiryComment]	
	@EnquiryId int = 0,
	@UserId varchar(255) = 0,	
	@DateCreated datetime,
	@Text nvarchar(max),
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO EnquiryComment (EnquiryId, UserId, DateCreated, [Text])
	VALUES(@EnquiryId, @UserId, @DateCreated, @Text)
	--Get back the Id
	SELECT @Id = Id
	FROM EnquiryComment
	WHERE @@ROWCOUNT > 0 AND Id = SCOPE_IDENTITY()	
END