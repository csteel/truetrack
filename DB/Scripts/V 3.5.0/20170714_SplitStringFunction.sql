--Create a Split function
--Created by Christopher 14/07/2017
Use TestEnquiryWorkSheet
GO

CREATE FUNCTION dbo.SplitString
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(2)
)
RETURNS @Output TABLE (
	Id INT IDENTITY(1,1) NOT NULL,
    Item NVARCHAR(max)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO