--Create Table [dbo].[EnquiryComment]  
--Written by Christopher: 23/05/2017
--Use EnquiryWorkSheet
USE TestEnquiryWorkSheet
GO
PRINT'Starting database script'
GO
PRINT'************************'
GO

--Create table
IF OBJECT_ID (N'EnquiryComment' , N'U' ) IS NULL
BEGIN
	 -- table does not exist
    BEGIN
         PRINT 'Create EnquiryComment table'
    END	
    BEGIN
		SET ANSI_NULLS ON
    END
    BEGIN
		SET QUOTED_IDENTIFIER ON
    END    
    BEGIN
		CREATE TABLE [dbo].[EnquiryComment](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[EnquiryId] [int] NOT NULL,
			[UserId] [varchar](255) NULL,
			[DateCreated] [datetime] NULL,
			[Text] [nvarchar](max) NULL,
		 CONSTRAINT [PK_EnquiryComment] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	END	
	BEGIN
		SET ANSI_PADDING OFF
	END
	BEGIN
		ALTER TABLE [dbo].[EnquiryComment]  WITH CHECK ADD  CONSTRAINT [FK_Enquiry_EnquiryComment] FOREIGN KEY([EnquiryId]) REFERENCES [dbo].[Enquiry] ([EnquiryId])
	END
	BEGIN
		ALTER TABLE [dbo].[EnquiryComment] CHECK CONSTRAINT [FK_Enquiry_EnquiryComment]
	END
END
ELSE
BEGIN
	PRINT 'EnquiryComment table already exits'
END
GO
--end of Create

PRINT'************************'
GO
PRINT'Database script finished'
GO

