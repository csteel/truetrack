--Add TradeIn money field in DueDiligence table
--12/12/2017: Christopher
--Use TestEnquiryWorkSheet
Use EnquiryWorkSheet
GO

--DueDiligence table
--Add the TradeIn column to DueDiligence table
IF Not exists(select * from sys.columns 
            where Name = N'TradeIn' and Object_ID = Object_ID(N'DueDiligence'))
BEGIN
	BEGIN
		-- add new column to table
		ALTER TABLE DueDiligence
		ADD TradeIn money NULL;
	END
	BEGIN
		PRINT 'Added TradeIn field to DueDiligence table'
	END	
END
ELSE
BEGIN
	PRINT 'TradeIn field already exists in DueDiligence table'
END
GO

