--25/09/2017: Christopher: Refactor the IDType enums
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO


UPDATE Customer SET
IdType = NULL
WHERE IdType = 0
GO

UPDATE Customer SET
IdType = 0
WHERE IdType = 1
GO

UPDATE Customer SET
IdType = 1
WHERE IdType = 2
GO

UPDATE Customer SET
IdType = 2
WHERE IdType = 3
GO

UPDATE Customer SET
IdType = 3
WHERE IdType = 4
GO

UPDATE Customer SET
IdType = 4
WHERE IdType = 5
GO

SELECT        dbo.Customer.CustomerId, dbo.Customer.CustomerType, dbo.Customer.FirstName, dbo.Customer.LastName, dbo.Customer.IdType, dbo.Customer.DriverLicenceType, dbo.CustomerEnquiry.EnquiryId
FROM            dbo.Customer INNER JOIN
                         dbo.CustomerEnquiry ON dbo.Customer.CustomerId = dbo.CustomerEnquiry.CustomerId
WHERE        (dbo.Customer.IdType IS NOT NULL)