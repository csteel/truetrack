--Insert [Enum_EnquiryType]
--add Financial Facility
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet

PRINT'Starting database script'
GO
PRINT'************************'
GO
--[Enum_Classification]
IF NOT EXISTS(SELECT * FROM [Enum_Classification] WHERE Enum = 5)
BEGIN
	BEGIN
         PRINT 'Insert Financial Facility into [Enum_Classification] table'
    END	
	BEGIN			
		PRINT 'Un-protect Enum_Classification table'
	END		
	BEGIN
	-- remove chk_read_only_Enum_Classification CONSTRAINT from Enum_Classification table
	ALTER TABLE [dbo].[Enum_Classification] DROP CONSTRAINT [chk_read_only_Enum_Classification]
	END
	BEGIN
		INSERT  [Enum_Classification] (Enum, Name, [Description])
		Values (5, 'FinancialFacility', 'Financial Facility')
	END
	BEGIN
		PRINT 'Protect Enum_Classification table'
    END
    BEGIN		
		ALTER TABLE [dbo].[Enum_Classification] WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_Classification CHECK( 1 = 0 )				
	END
	BEGIN
		PRINT 'Added Constraint chk_read_only_Enum_Classification'
	END		
END
ELSE
BEGIN
	BEGIN
         PRINT 'Financial Facility already exists in [Enum_Classification] table'
    END	
	PRINT 'INSERT aborted'
END
GO
PRINT'----------------------'
GO
--[Enum_EnquiryType]
IF NOT EXISTS(SELECT * FROM [Enum_EnquiryType] WHERE Enum = 14)
BEGIN
	BEGIN
         PRINT 'Insert Financial Facility into [Enum_EnquiryType] table'
    END	
	BEGIN			
		PRINT 'Un-protect Enum_EnquiryType table'
	END		
	BEGIN
	-- remove chk_read_only_Enum_EnquiryType CONSTRAINT from Enum_EnquiryType table
	ALTER TABLE [dbo].[Enum_EnquiryType] DROP CONSTRAINT [chk_read_only_Enum_EnquiryType]
	END
	BEGIN
		PRINT 'Drop constraint chk_read_only_Enum_EnquiryType'
	END
	BEGIN
		INSERT  [Enum_EnquiryType] (Enum, Name, [Description], Classification)
		Values (14, 'FinancialFacility', 'Financial Facility', 5)
	END
	BEGIN
	--Check constraint exists
	IF OBJECT_ID ('chk_read_only_Enum_EnquiryType', 'C') IS NULL  
		BEGIN	
		--add Enum_EnquiryType readOnly constraint	
			ALTER TABLE [dbo].Enum_EnquiryType WITH NOCHECK ADD CONSTRAINT chk_read_only_Enum_EnquiryType CHECK( 1 = 0 )
		END	
		BEGIN
			PRINT 'Added Constraint chk_read_only_Enum_EnquiryType'
		END				
	END
END
ELSE
BEGIN
	BEGIN
         PRINT 'Financial Facility already exists in [Enum_EnquiryType] table'
    END	
	PRINT 'INSERT aborted'
END
GO

PRINT'************************'
GO
PRINT'Database script finished'
GO


SELECT * FROM Enum_EnquiryType
GO
SELECT * FROM Enum_Classification
GO