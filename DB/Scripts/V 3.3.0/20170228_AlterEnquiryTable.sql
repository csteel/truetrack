--Change the ApplicationCode field size from 12 to 14
--same as is for the OAC
--need to alter Enquiry and ArchiveEnquiry tables
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

ALTER TABLE Enquiry
ALTER COLUMN ApplicationCode varchar(14)
GO

ALTER TABLE ArchiveEnquiry
ALTER COLUMN ApplicationCode varchar(14)
GO