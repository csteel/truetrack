--Change db field ClientNum from varchar(8) to varchar(10)
--same as is for the finPower
--need to alter Customer tables
Use TestEnquiryWorkSheet
--Use EnquiryWorkSheet
GO

PRINT 'Starting DB script'
GO
PRINT '****************************************'
GO
PRINT'Start data migration of ClientNum Information in the Customer table'
GO
PRINT '****'
GO

Print'Alter the ClientNum fieldsize in the Customer table'
GO
ALTER TABLE Customer
ALTER COLUMN ClientNum varchar(10)
GO
Print'Finished altering the ClientNum fieldsize in the Customer table'
GO

PRINT'Starting data update'
GO

DECLARE @customerId int
DECLARE @clientTypeId varchar(5)
DECLARE @clientNum varchar(10)
DECLARE @customerCount int
	
SET @customerCount = 0	

DECLARE cur CURSOR LOCAL for
		select c.CustomerId, c.ClientTypeId, c.ClientNum FROM Customer c				
OPEN cur

fetch next from cur into @customerId, @clientTypeId, @clientNum
	while @@FETCH_STATUS = 0 BEGIN
		--execute  on each row		
		If NOT @clientNum IS NULL
		BEGIN
			IF NOT @clientTypeId IS NULL
			BEGIN
				If UPPER(RTRIM(LTRIM(@clientTypeId))) = 'C'
				BEGIN
					Update [Customer] SET 					
					ClientTypeId = UPPER(RTRIM(LTRIM(@clientTypeId))),
					ClientNum = UPPER(RTRIM(LTRIM(@clientTypeId))) + LTRIM(@clientNum)
					WHERE CustomerId = @CustomerId	
				END	
			END					
		END
		SELECT @customerCount = @customerCount + 1;
	
		fetch next from cur into  @customerId, @clientTypeId, @clientNum

	END

close cur
deallocate cur

PRINT 'Data migrated for ' + CAST(@customerCount AS VARCHAR)  + ' customers'

GO
