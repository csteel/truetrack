USE [TestEnquiryWorkSheet]
--Use EnquiryWorkSheet
GO

INSERT INTO [dbo].[AppSettings]
           ([Name]
           ,[Decimal]
           ,[Text]
           ,[Boolean]
           ,[Comments])
     VALUES
           ('DaysBeforeArchive'
           ,10
           ,Null
           ,0
           ,'Days to wait before enquiry can be archived')
GO


