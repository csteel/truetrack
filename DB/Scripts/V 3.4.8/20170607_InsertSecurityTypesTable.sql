-- Add a Collateral type to the Security Types table
-- Christopher Steel 07/06/2017
USE [TestEnquiryWorkSheet]
--USE EnquiryWorkSheet
GO

INSERT INTO [dbo].[SecurityTypes]
           ([SecurityTypeId]
           ,[Description])
     VALUES
           ('APAPP','All Present and After Acquired Personal Property')
GO


