﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MembersCentreArchive
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MembersCentreArchive))
        Me.dgvMails = New System.Windows.Forms.DataGridView()
        Me.ssFormStatus = New System.Windows.Forms.StatusStrip()
        Me.lblFormStatus2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblFormStatus = New System.Windows.Forms.Label()
        Me.WebBrowserPanel = New System.Windows.Forms.Panel()
        Me.WebBrowserArchive = New System.Windows.Forms.WebBrowser()
        Me.SearchPanel = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lblApplicationCode = New System.Windows.Forms.Label()
        Me.txtApplicationCode = New System.Windows.Forms.TextBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        CType(Me.dgvMails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ssFormStatus.SuspendLayout()
        Me.WebBrowserPanel.SuspendLayout()
        Me.SearchPanel.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvMails
        '
        Me.dgvMails.AllowUserToAddRows = False
        Me.dgvMails.AllowUserToDeleteRows = False
        Me.dgvMails.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight
        Me.dgvMails.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvMails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMails.Location = New System.Drawing.Point(3, 76)
        Me.dgvMails.MultiSelect = False
        Me.dgvMails.Name = "dgvMails"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMails.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvMails.RowHeadersVisible = False
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        Me.dgvMails.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvMails.ShowEditingIcon = False
        Me.dgvMails.Size = New System.Drawing.Size(922, 160)
        Me.dgvMails.TabIndex = 3
        '
        'ssFormStatus
        '
        Me.ssFormStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblFormStatus2})
        Me.ssFormStatus.Location = New System.Drawing.Point(3, 699)
        Me.ssFormStatus.Name = "ssFormStatus"
        Me.ssFormStatus.Size = New System.Drawing.Size(922, 22)
        Me.ssFormStatus.TabIndex = 47
        Me.ssFormStatus.Text = "StatusStrip1"
        '
        'lblFormStatus2
        '
        Me.lblFormStatus2.Name = "lblFormStatus2"
        Me.lblFormStatus2.Size = New System.Drawing.Size(0, 17)
        '
        'lblFormStatus
        '
        Me.lblFormStatus.AutoSize = True
        Me.lblFormStatus.Location = New System.Drawing.Point(16, 880)
        Me.lblFormStatus.Name = "lblFormStatus"
        Me.lblFormStatus.Size = New System.Drawing.Size(73, 13)
        Me.lblFormStatus.TabIndex = 49
        Me.lblFormStatus.Text = "dfgherghqegh"
        '
        'WebBrowserPanel
        '
        Me.WebBrowserPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.WebBrowserPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.WebBrowserPanel.Controls.Add(Me.WebBrowserArchive)
        Me.WebBrowserPanel.Location = New System.Drawing.Point(3, 234)
        Me.WebBrowserPanel.Name = "WebBrowserPanel"
        Me.WebBrowserPanel.Size = New System.Drawing.Size(922, 466)
        Me.WebBrowserPanel.TabIndex = 50
        '
        'WebBrowserArchive
        '
        Me.WebBrowserArchive.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowserArchive.Location = New System.Drawing.Point(0, 0)
        Me.WebBrowserArchive.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowserArchive.Name = "WebBrowserArchive"
        Me.WebBrowserArchive.Size = New System.Drawing.Size(920, 464)
        Me.WebBrowserArchive.TabIndex = 5
        '
        'SearchPanel
        '
        Me.SearchPanel.BackColor = System.Drawing.SystemColors.Control
        Me.SearchPanel.Controls.Add(Me.Label1)
        Me.SearchPanel.Controls.Add(Me.GroupBox1)
        Me.SearchPanel.Controls.Add(Me.btnSearch)
        Me.SearchPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.SearchPanel.Location = New System.Drawing.Point(3, 3)
        Me.SearchPanel.Name = "SearchPanel"
        Me.SearchPanel.Size = New System.Drawing.Size(922, 71)
        Me.SearchPanel.TabIndex = 51
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Search Results"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtLastName)
        Me.GroupBox1.Controls.Add(Me.lblApplicationCode)
        Me.GroupBox1.Controls.Add(Me.txtApplicationCode)
        Me.GroupBox1.Controls.Add(Me.lblLastName)
        Me.GroupBox1.Location = New System.Drawing.Point(274, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(406, 59)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filters"
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(6, 33)
        Me.txtLastName.MaxLength = 40
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(248, 20)
        Me.txtLastName.TabIndex = 10
        '
        'lblApplicationCode
        '
        Me.lblApplicationCode.AutoSize = True
        Me.lblApplicationCode.Location = New System.Drawing.Point(283, 16)
        Me.lblApplicationCode.Name = "lblApplicationCode"
        Me.lblApplicationCode.Size = New System.Drawing.Size(87, 13)
        Me.lblApplicationCode.TabIndex = 13
        Me.lblApplicationCode.Text = "Application Code"
        '
        'txtApplicationCode
        '
        Me.txtApplicationCode.Location = New System.Drawing.Point(260, 33)
        Me.txtApplicationCode.MaxLength = 20
        Me.txtApplicationCode.Name = "txtApplicationCode"
        Me.txtApplicationCode.Size = New System.Drawing.Size(132, 20)
        Me.txtApplicationCode.TabIndex = 11
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Location = New System.Drawing.Point(80, 16)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 13)
        Me.lblLastName.TabIndex = 14
        Me.lblLastName.Text = "Last Name"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(835, 35)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 12
        Me.btnSearch.Text = "&Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'MembersCentreArchive
        '
        Me.AcceptButton = Me.btnSearch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 724)
        Me.Controls.Add(Me.SearchPanel)
        Me.Controls.Add(Me.WebBrowserPanel)
        Me.Controls.Add(Me.lblFormStatus)
        Me.Controls.Add(Me.dgvMails)
        Me.Controls.Add(Me.ssFormStatus)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(0, 300)
        Me.MaximumSize = New System.Drawing.Size(944, 1050)
        Me.MinimumSize = New System.Drawing.Size(944, 752)
        Me.Name = "MembersCentreArchive"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Application Archive Search"
        CType(Me.dgvMails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ssFormStatus.ResumeLayout(False)
        Me.ssFormStatus.PerformLayout()
        Me.WebBrowserPanel.ResumeLayout(False)
        Me.SearchPanel.ResumeLayout(False)
        Me.SearchPanel.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvMails As System.Windows.Forms.DataGridView
    Friend WithEvents ssFormStatus As System.Windows.Forms.StatusStrip
    Friend WithEvents lblFormStatus As System.Windows.Forms.Label
    Friend WithEvents WebBrowserPanel As System.Windows.Forms.Panel
    Friend WithEvents WebBrowserArchive As System.Windows.Forms.WebBrowser
    Friend WithEvents SearchPanel As System.Windows.Forms.Panel
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents txtApplicationCode As System.Windows.Forms.TextBox
    Friend WithEvents lblApplicationCode As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblFormStatus2 As System.Windows.Forms.ToolStripStatusLabel

End Class
