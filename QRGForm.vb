﻿Imports System.Data.SqlClient

Public Class QRGForm
    Dim ThisEnquiryId As Integer ' set New Enquiry Id 
    Dim ThisEnquiryCode As String ' set Enquiry Code
    Dim ThisContactSalutation As String 'set Client salutation
    Dim QRGStatusTrack As Integer = 0
    Dim ThisCurrentStatus As String

    'Pass Variable Form Load
    Friend Sub PassVariable(ByVal EnquiryIdVar As Integer, ByVal EnquiryCodeVar As String, ByVal ContactSalutationVar As String)
        'get user settings
        'Me.ClientSize = New Size(My.Settings.AppFormSize)
        'Me.Location = New Point(My.Settings.AppFormLocation)
        ' setup the reference variables
        ThisEnquiryId = EnquiryIdVar
        ThisEnquiryCode = EnquiryCodeVar
        ThisContactSalutation = ContactSalutationVar
        'MsgBox("ThisQRGListId = " & ThisQRGListId)
        'Set form title
        Me.Text = Me.Text & " - " & ThisContactSalutation
        Me.TableAdapterManager.QRGListTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.QRGList, ThisEnquiryId)
        'Dim Test As Integer = EnquiryWorkSheetDataSet.QRGList.Rows(QRGListBindingSource.Position()).Item("QRGListId")
        'MsgBox("Test QRGListId = " & Test)
        'check Current status and disable controls if appropiate

        '*************** get current status
        Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection As New SqlConnection(connectionString)
        Dim selectStatement As String = "SELECT CurrentStatus FROM dbo.Enquiry WHERE EnquiryId = '" & ThisEnquiryId & "'"
        'MsgBox("Select Statement = " & selectStatement)
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        Dim EnquiryTempDataAdapter As New SqlDataAdapter(selectCommand)
        Dim EnquiryTempDataSet As New DataSet
        Dim EnquiryTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            EnquiryTempDataAdapter.Fill(EnquiryTempDataTable)
            'if no matching rows .....
            If EnquiryTempDataTable.Rows.Count = 0 Then
                MessageBox.Show("No Dealer Name, please try again.")
                'clear the dataTable and the Connect information
                EnquiryTempDataAdapter = Nothing
                EnquiryTempDataTable.Clear()
                'if there is a matching row
            ElseIf EnquiryTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim EnquiryTempDataRow As DataRow = EnquiryTempDataTable.Rows(0)
                ThisCurrentStatus = EnquiryTempDataRow.Item(0)
                'clear the dataTable and the Connect information
                EnquiryTempDataAdapter = Nothing
                EnquiryTempDataTable.Clear()
            End If
            'close the connection
            If connection.State <> ConnectionState.Closed Then
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '*************** End of get current status
        '************* disable controls if appropiate
        Select Case ThisCurrentStatus
            Case "Declined"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case "Waiting Sign-up"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case "Audit"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case "Audit Failed"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case "Payout"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case "Saved"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case "Exported"
                'disable controls on form
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case Else
                'enable all controls
                Dim FormControl As Control
                For Each FormControl In Me.Controls
                    FormControl.Enabled = True
                Next
        End Select
        '********end of disable controls if appropiate
        '*************** Set permissions
        Select Case LoggedinPermissionLevel
            Case Is < 3 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- < 3
                gpbxQuickRefGuide.Enabled = False
                btnUpdateQRG.Enabled = False
            Case Else '> 3

        End Select
        '*************** end of Set permissions

    End Sub 'end of 'Pass Variable Form Load

    Private Sub QRGForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub

    Private Sub QRGForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'SuspendLayout()
        'check if data has changed
        Dim bindSrc1 As BindingSource = Me.QRGListBindingSource
        Dim rowView1 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not bindSrc1.Current Is Nothing Then
            rowView1 = CType(bindSrc1.Current, DataRowView)
            'test if any fields have changed
            If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'set QRGStatus
                        Dim QRGListRow As EnquiryWorkSheetDataSet.QRGListRow
                        QRGListRow = Me.EnquiryWorkSheetDataSet.QRGList(0)
                        QRGListRow.BeginEdit()
                        QRGListRow.QRGStatus = QRGStatusTrack
                        'save changes
                        Me.Validate()
                        bindSrc1.EndEdit()
                        Me.QRGListTableAdapter.Update(Me.EnquiryWorkSheetDataSet.QRGList)

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Else
                    ' User choose No. Do nothing
                    Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
    End Sub

    Private Sub cbChangeColor(ByVal cbName As String)
        'change the colour of check box label if checkboxes checked
        Dim cbReqName As String
        Dim cbRecName As String
        Dim cblblName As String
        cbReqName = "cb" & cbName & "Required"
        cbRecName = "cb" & cbName & "Received"
        cblblName = "lbl" & cbName

        ' Now we have to find the "cbReqName" CheckBox Control, if it exists... Search through all controls on the form.
        Dim controls1() As Control = Me.Controls.Find(cbReqName, True)
        If controls1.Count > 0 Then
            ' It exists, let us cast it to the right control type (CheckBox)
            Dim ThisCheckBox1 As CheckBox = DirectCast(controls1(0), CheckBox)
            ' We're working with a checkbox now...
            'if "cbReqName" checkbox is ticked...
            If ThisCheckBox1.CheckState = 1 Then
                ' First we have to find the control, if it exists... Search through all controls on the form.
                Dim controls2() As Control = Me.Controls.Find(cbRecName, True)
                If controls2.Count > 0 Then
                    ' It exists, let us cast it to the right control type (CheckBox)
                    Dim ThisCheckBox2 As CheckBox = DirectCast(controls2(0), CheckBox)
                    ' We're working with a checkbox now...
                    'if "cb cbName Received" checkbox is ticked...
                    If ThisCheckBox2.CheckState = 1 Then
                        ' First we have to find the Label Control, if it exists... Search through all controls on the form.
                        Dim controls3() As Control = Me.Controls.Find(cblblName, True)
                        If controls3.Count > 0 Then
                            ' It exists, let us cast it to the right control type (Label)
                            Dim ThisLabel As Label = DirectCast(controls3(0), Label)
                            ' We're working with a Label now...
                            ThisLabel.BackColor = Color.LightGreen
                            ThisLabel.AutoSize = False
                            ThisLabel.Size = New Size(130, 13)
                        End If
                    Else
                        ' First we have to find the Label Control, if it exists... Search through all controls on the form.
                        Dim controls3() As Control = Me.Controls.Find(cblblName, True)
                        If controls3.Count > 0 Then
                            ' It exists, let us cast it to the right control type (Label)
                            Dim ThisLabel As Label = DirectCast(controls3(0), Label)
                            ' We're working with a Label now...
                            ThisLabel.BackColor = Color.Tomato
                            ThisLabel.AutoSize = False
                            ThisLabel.Size = New Size(130, 13)
                        End If
                    End If
                Else
                    ' First we have to find the Label Control, if it exists... Search through all controls on the form.
                    Dim controls3() As Control = Me.Controls.Find(cblblName, True)
                    If controls3.Count > 0 Then
                        ' It exists, let us cast it to the right control type (Label)
                        Dim ThisLabel As Label = DirectCast(controls3(0), Label)
                        ' We're working with a Label now...
                        ThisLabel.BackColor = Color.Transparent
                        ThisLabel.AutoSize = False
                        ThisLabel.Size = New Size(130, 13)
                    End If
                End If
            Else
                ' First we have to find the Label Control, if it exists... Search through all controls on the form.
                Dim controls3() As Control = Me.Controls.Find(cblblName, True)
                If controls3.Count > 0 Then
                    ' It exists, let us cast it to the right control type (Label)
                    Dim ThisLabel As Label = DirectCast(controls3(0), Label)
                    ' We're working with a Label now...
                    ThisLabel.BackColor = Color.Transparent
                    ThisLabel.AutoSize = False
                    ThisLabel.Size = New Size(130, 13)
                End If
            End If
        End If
        changelblQRGBacLayer()

    End Sub

    Private Sub changelblQRGBacLayer()
        ' Check Controls in GroupBox gpbxQuickRefGuide, if are label check BackColor
        QRGStatusTrack = 0 'nothing
        'set variable to track backcolor
        Dim BkClr As Color = Color.Transparent
        'reset lblQRGBacLayer.BackColor so is not caught in loop 
        lblQRGBacLayer.BackColor = Color.Transparent
        'loop through controls in groupbox
        For Each ctrl As Control In gpbxQuickRefGuide.Controls
            'test if control is a label
            If TypeOf ctrl Is Label Then
                'MsgBox("control is " & ctrl.Name & " and colour is " & ctrl.BackColor.Name)
                If ctrl.BackColor = Color.LightGreen Then
                    BkClr = Color.LightGreen
                    QRGStatusTrack = 2 'green
                ElseIf ctrl.BackColor = Color.Tomato Then
                    BkClr = Color.Tomato
                    QRGStatusTrack = 1 'red
                    Exit For
                End If

            End If
        Next
        Me.lblQRGBacLayer.BackColor = BkClr

        'MsgBox("QRGStatusTrack = " & QRGStatusTrack)



    End Sub

#Region "QRG checkboxes"

    Private Sub cbIDRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbIDRequired.CheckedChanged
        cbChangeColor("ID")
    End Sub

    Private Sub cbIDReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbIDReceived.CheckedChanged
        cbChangeColor("ID")
    End Sub

    Private Sub cbVOSARequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVOSARequired.CheckedChanged
        cbChangeColor("VOSA")
    End Sub

    Private Sub cbVOSAReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVOSAReceived.CheckedChanged
        cbChangeColor("VOSA")
    End Sub

    Private Sub cbVedaRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVedaRequired.CheckedChanged
        cbChangeColor("Veda")
    End Sub

    Private Sub cbVedaReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVedaReceived.CheckedChanged
        cbChangeColor("Veda")
    End Sub

    Private Sub cbVehicleValRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVehicleValRequired.CheckedChanged
        cbChangeColor("VehicleVal")
    End Sub

    Private Sub cbVehicleValReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVehicleValReceived.CheckedChanged
        cbChangeColor("VehicleVal")
    End Sub


    Private Sub cbCarJamRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCarJamRequired.CheckedChanged
        cbChangeColor("CarJam")
    End Sub

    Private Sub cbCarJamReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCarJamReceived.CheckedChanged
        cbChangeColor("CarJam")
    End Sub

    Private Sub cbMotorCkRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbMotorCkRequired.CheckedChanged
        cbChangeColor("MotorCk")
    End Sub

    Private Sub cbMotorCkReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbMotorCkReceived.CheckedChanged
        cbChangeColor("MotorCk")
    End Sub

    Private Sub cbPPSRRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPSRRequired.CheckedChanged
        cbChangeColor("PPSR")
    End Sub

    Private Sub cbPPSRReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPSRReceived.CheckedChanged
        cbChangeColor("PPSR")
    End Sub

    Private Sub cbCofResRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCofResRequired.CheckedChanged
        cbChangeColor("CofRes")
    End Sub

    Private Sub cbCofResReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCofResReceived.CheckedChanged
        cbChangeColor("CofRes")
    End Sub

    Private Sub cbInsuranceRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbInsuranceRequired.CheckedChanged
        cbChangeColor("Insurance")
    End Sub

    Private Sub cbInsuranceReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbInsuranceReceived.CheckedChanged
        cbChangeColor("Insurance")
    End Sub

    Private Sub cbEmploymentRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEmploymentRequired.CheckedChanged
        cbChangeColor("Employment")
    End Sub

    Private Sub cbEmploymentReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEmploymentReceived.CheckedChanged
        cbChangeColor("Employment")
    End Sub

    Private Sub cbGoodsrequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGoodsRequired.CheckedChanged
        cbChangeColor("Goods")
    End Sub

    Private Sub cbGoodsReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGoodsReceived.CheckedChanged
        cbChangeColor("Goods")
    End Sub

    Private Sub cbBankRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbBankRequired.CheckedChanged
        cbChangeColor("Bank")
    End Sub

    Private Sub cbBankReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbBankReceived.CheckedChanged
        cbChangeColor("Bank")
    End Sub

    Private Sub cbChattelRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbChattelRequired.CheckedChanged
        cbChangeColor("Chattel")
    End Sub

    Private Sub cbChattelReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbChattelReceived.CheckedChanged
        cbChangeColor("Chattel")
    End Sub

    Private Sub cbPayRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPayRequired.CheckedChanged
        cbChangeColor("Pay")
    End Sub

    Private Sub cbPayReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPayReceived.CheckedChanged
        cbChangeColor("Pay")
    End Sub

    Private Sub cbTenancyRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTenancyRequired.CheckedChanged
        cbChangeColor("Tenancy")
    End Sub

    Private Sub cbTenancyReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTenancyReceived.CheckedChanged
        cbChangeColor("Tenancy")
    End Sub

    Private Sub cbCompanyRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCompanyRequired.CheckedChanged
        cbChangeColor("Company")
    End Sub

    Private Sub cbCompanyReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCompanyReceived.CheckedChanged
        cbChangeColor("Company")
    End Sub

    Private Sub cbHomeRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbHomeRequired.CheckedChanged
        cbChangeColor("Home")
    End Sub

    Private Sub cbHomeReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbHomeReceived.CheckedChanged
        cbChangeColor("Home")
    End Sub

    Private Sub cbGSTRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGSTRequired.CheckedChanged
        cbChangeColor("GST")
    End Sub

    Private Sub cbGSTReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGSTReceived.CheckedChanged
        cbChangeColor("GST")
    End Sub

    Private Sub cbAddressRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbAddressRequired.CheckedChanged
        cbChangeColor("Address")
    End Sub

    Private Sub cbAddressReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbAddressReceived.CheckedChanged
        cbChangeColor("Address")
    End Sub

    Private Sub cbCreditRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCreditRequired.CheckedChanged
        cbChangeColor("Credit")
    End Sub

    Private Sub cbCreditReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCreditReceived.CheckedChanged
        cbChangeColor("Credit")
    End Sub


    Private Sub cbVedaXReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVedaXReceived.CheckedChanged
        cbChangeColor("VedaX")
    End Sub

    Private Sub cbVedaXRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVedaXRequired.CheckedChanged
        cbChangeColor("VedaX")
    End Sub

    Private Sub cbDriverCkRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDriverCkRequired.CheckedChanged
        cbChangeColor("DriverCk")
    End Sub

    Private Sub cbDriverCkReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDriverCkReceived.CheckedChanged
        cbChangeColor("DriverCk")
    End Sub

    Private Sub cbWinzRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbWinzRequired.CheckedChanged
        cbChangeColor("Winz")
    End Sub

    Private Sub cbWinzReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbWinzReceived.CheckedChanged
        cbChangeColor("Winz")
    End Sub

    Private Sub cbDealerAppShtRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDealerAppShtRequired.CheckedChanged
        cbChangeColor("DealerAppSht")
    End Sub

    Private Sub cbDealerAppShtReceived_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDealerAppShtReceived.CheckedChanged
        cbChangeColor("DealerAppSht")
    End Sub

#End Region

    Private Sub btnUpdateQRG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateQRG.Click 'Save
        Try
            'MsgBox("QRGStatusTrack [updating] = " & QRGStatusTrack)
            Dim QRGListRow As EnquiryWorkSheetDataSet.QRGListRow
            QRGListRow = Me.EnquiryWorkSheetDataSet.QRGList(0)
            QRGListRow.BeginEdit()
            QRGListRow.QRGStatus = QRGStatusTrack
            'test
            'MsgBox("test QRGStatus = " & QRGListRow.QRGStatus)
            'MsgBox("QRGList_Id = " & QRGListRow.QRGListId)

            Me.Validate()
            'EnquiryWorkSheetDataSet.QRGList.Rows(QRGListBindingSource.Position()).Item("QRGStatus") = QRGStatusTrack
            QRGListRow.EndEdit()
            Me.QRGListTableAdapter.Update(Me.EnquiryWorkSheetDataSet.QRGList)
            Me.TableAdapterManager.QRGListTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.QRGList, ThisEnquiryId)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.QRGListBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub




End Class