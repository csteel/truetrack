﻿Imports System.IO

Public Class frmRenameFile
    Public Property newFileName As String
    Dim str As String = String.Empty 'file name with extension
    Dim strExt As String = String.Empty 'file extension
    Dim strName As String = String.Empty 'file name with extension


    Friend Sub PassVariable(ByVal currentFilePathName As String)

        Dim finfo As New FileInfo(currentFilePathName)
        If finfo.Exists Then
            str = finfo.Name
            strExt = finfo.Extension
            If Not strExt = "" Then
                strName = str.Replace(strExt, "")
            Else
                strName = str
            End If
        End If

        lblExistingName.Text = strName
        lblExistingFileNameExt.Text = strExt
        lblFileNameExt.Text = strExt
        txtNewName.Text = strName



    End Sub







    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Cursor.Current = Cursors.WaitCursor
        Dim checkFieldsMessage As String = String.Empty
        'Validate textbox
        If txtNewName.Text = String.Empty Then
            checkFieldsMessage = checkFieldsMessage + "Please enter the new file name!" & vbCrLf
        End If
        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
        Else
            Try
                newFileName = txtNewName.Text & strExt
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        End If

       
    End Sub
End Class