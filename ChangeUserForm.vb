﻿Imports System.Data.SqlClient

Public Class ChangeUserForm
    Dim ThisEnquiryId As Integer
    Dim rowCount As Integer
    Dim ThisEnquiryManagerId As Integer
    Dim UserNameDetect As Boolean

    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer)
        ThisEnquiryId = LoadEnquiryVar
        'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table. 
        Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
        rowCount = EnquiryWorkSheetDataSet.Enquiry.Rows.Count
        'loads data into the 'EnquiryWorkSheetDataSet.Users' table.
        Me.UsersTableAdapter1.Fill(Me.EnquiryWorkSheetDataSet.Users) 'loading fillByActiveUsers causeda foreign key problem
        If rowCount > 0 Then
            ThisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            'Get EnquiryManagerId Name
            Try
                'Get Connection String Settings
                Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
                Dim connection As New SqlConnection(connectionString)
                Dim selectStatement As String = "SELECT  UserName FROM Users WHERE  UserId = " & ThisEnquiryManagerId
                Dim selectCommand As New SqlCommand(selectStatement, connection)
                Dim UserNameDataAdapter As New SqlDataAdapter(selectCommand)
                Dim UserNameDataSet As New DataSet
                Dim UserNameDataTable As New DataTable
                'dumps results into datatable AppValuesDataTable
                UserNameDataAdapter.Fill(UserNameDataTable)
                'if no matching rows .....
                If UserNameDataTable.Rows.Count = 0 Then
                    'set appDetect variable
                    UserNameDetect = False
                    'set StatusStrip text
                    lblCurrentManValue.ForeColor = Color.Red
                    lblCurrentManValue.Text = "Detection of Current Manager Name failed!"
                    ' clear the dataTable and the Connect information
                    UserNameDataAdapter = Nothing
                    UserNameDataTable.Clear()
                    'if there is a matching row
                ElseIf UserNameDataTable.Rows.Count > 0 Then
                    'set appDetect variable
                    UserNameDetect = True
                    'get DataRow value
                    Dim UserNameDataRow As DataRow = UserNameDataTable.Rows(0)
                    'assign UserName to Form lblCurrentManValue 
                    lblCurrentManValue.Text = UserNameDataRow.Item(0)
                    ' clear the dataTable and the Connect information
                    UserNameDataAdapter = Nothing
                    UserNameDataTable.Clear()
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

    End Sub

    Private Sub ChangeUserForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'check there is data in table
            If rowCount > 0 Then
                Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                EnquiryRow = EnquiryWorkSheetDataSet.Enquiry(0)
                EnquiryRow.BeginEdit()
                EnquiryRow.EnquiryManagerId = cmbxNewManName.SelectedValue
                'create log note
                'Dim DateStr As String
                'Dim NewCommentStr As String
                Dim CommentStr As String
                'Dim LoanComments As String = EnquiryRow.LoanComments
                ''Create Date string
                'DateStr = "-- " & DateTime.Now.ToString("f")
                ''Add Date string to comments
                CommentStr = "Enquiry Manager changed"
                ''check loan comments from dataset exist
                'If Not LoanComments = "" Then
                '    NewCommentStr = CommentStr & LoanComments
                'Else
                '    NewCommentStr = CommentStr
                'End If
                ''update LoanComments 
                'EnquiryRow.LoanComments = NewCommentStr

                Dim commentId As Integer
                commentId = Util.SetComment(ThisEnquiryId, LoggedinName, CommentStr)

                'save
                Me.Validate()
                EnquiryBindingSource.EndEdit()
                Me.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)

                'close form
                Me.DialogResult = System.Windows.Forms.DialogResult.OK

            Else
                MsgBox("There is no data in Enquiry DataTable." & vbCrLf & "Operation Cancelled")
            End If
        Catch ex As Exception
            MsgBox("btnSave caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnquiryBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub
End Class