﻿Imports System.IO
Imports System.Drawing.Printing

Public Class ArchiveForm

    Dim thisEnquiryId As String
    Dim thisApplicationCode As String
    Dim thisEnquiryManagerId As Integer ' set ThisEnquiryManagerId
    Dim thisDealerId As String = String.Empty 'dealer name id

    Dim formTitle As String = Me.Text

    Dim thisContactDisplayName As String
    Dim contactSuburb As String
    Dim contactCity As String

    Dim thisEnquiryType As Integer

    'define print page info
    Private printPageSettings As New PageSettings
    Private stringToPrint As String
    Private printFont As New Font("Arial", 9)
    Private finishedString As String
    'Edit function
    Dim detailsEditToggle As Boolean = False


    Friend Sub PassVariable(ByVal enquiryIdVar As String)
        'get user settings
        Me.ClientSize = New Size(My.Settings.AppFormSize)
        Me.Location = New Point(My.Settings.AppFormLocation)
        thisEnquiryId = enquiryIdVar

        Try
            Me.ArchiveEnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.ArchiveEnquiry, thisEnquiryId)
            thisApplicationCode = EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ApplicationCode")
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("EnquiryManagerId")
            'get type of Enquiry
            thisEnquiryType = EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher
            'set EnquiryType label
            lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
            'Load User table
            UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            'get UserName
            lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource1.Position()).Item("FullName")

            '************************* Get Contact Display Name
            If IsDBNull(EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ContactDisplayName")) Then
                MsgBox("Load Archive Form:" + vbCrLf + "ContactDisplayName is Null")
                thisContactDisplayName = ""
            Else
                thisContactDisplayName = EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ContactDisplayName")
            End If
            lblContactName.Text = thisContactDisplayName
            '************************* Contact address
            If IsDBNull(EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ContactSuburb")) Then
                MsgBox("Load Archive Form:" + vbCrLf + "ContactSuburb is Null")
                contactSuburb = ""
            Else
                contactSuburb = EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ContactSuburb")
            End If
            If IsDBNull(EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ContactCity")) Then
                MsgBox("Load Archive Form:" + vbCrLf + "ContactCity is Null")
                contactCity = ""
            Else
                contactCity = EnquiryWorkSheetDataSet.ArchiveEnquiry.Rows(ArchiveEnquiryBindingSource.Position()).Item("ContactCity")
            End If
            lblContactAddress.Text = contactSuburb & "  " & contactCity

            '************************* Set form title
            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & thisContactDisplayName & " - " & formTitle
            Else
                Me.Text = thisContactDisplayName & " - " & formTitle
            End If


        Catch ex As Exception
            MsgBox("Load Archive Form:" + vbCrLf + ex.Message)
        End Try

        '****************** Documents ************************************
        'Load list of file info for ThisEnquiryId into Documents Tab
        ' ListView name = lvDocuments
        Try
            'set properties
            Me.lvDocuments.BackColor = SystemColors.Window
            lvDocuments.FullRowSelect = True
            lvDocuments.View = View.Details
            lvDocuments.GridLines = True
            ' Display check boxes.
            'lvDocuments.CheckBoxes = True
            ' Create columns for the items and subitems.
            ' Width of -2 indicates auto-size.
            lvDocuments.Columns.Add("Name", 410, HorizontalAlignment.Left)
            lvDocuments.Columns.Add("Creation Time", 150, HorizontalAlignment.Left)
            lvDocuments.Columns.Add("File Size", 100, HorizontalAlignment.Right)

            'Get Files        
            'Get String to WorkSheet Drive
            Dim worksheetDrive As String = Switch.GetWorksheetArchiveFolder
            Dim folderPath As String = worksheetDrive & "\" & thisEnquiryId
            'Check directory exists
            If Directory.Exists(folderPath) Then
                'get new image
                btnFolder.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                'set visiblity of note
                lblDocNote.Visible = False
                'Create place to store files
                Dim aFiles() As String 'array type string
                aFiles = Directory.GetFiles(folderPath)
                'loop through aFiles
                For Each sfile As String In aFiles
                    'create str array
                    Dim str(3) As String
                    Dim itm As ListViewItem
                    'assign str array values
                    Dim finfo As New FileInfo(sfile)
                    If finfo.Exists Then
                        str(0) = finfo.Name
                        str(1) = finfo.CreationTime
                        If finfo.Length < 1000 Then
                            str(2) = finfo.Length.ToString & " Bytes"
                        ElseIf finfo.Length >= 1000 And finfo.Length < 1000000 Then
                            str(2) = (finfo.Length / 1000).ToString & " KB"
                        Else
                            str(2) = (finfo.Length / 1000000).ToString & " MB"
                        End If
                        itm = New ListViewItem(str)
                        'itm.Checked = True
                        'debug
                        MessageBox.Show(String.Format("Name: {0}, Creation time: {1}, Length: {2}", str(0), str(1), str(2)), "File details", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        'add ListViewItem to ListView
                        lvDocuments.Items.Add(itm)
                    End If
                Next sfile
            Else
                'set visiblity of note
                lblDocNote.Visible = True
                Me.lvDocuments.BackColor = Color.LavenderBlush

            End If

            '#######################################
            'Edit text permission
            If ApplicationAccessRights.CanEditComments Then
                btnDetailsEdit.Visible = True
                btnDetailsEdit.Enabled = True
            Else
                btnDetailsEdit.Visible = False
                btnDetailsEdit.Enabled = False
            End If

            btnSaveSummary.Visible = False
            btnSaveSummary.Enabled = False
            '#######################################

        Catch ex As Exception
            MsgBox("Load Form: Loading Documents List generated an Exception Message: " & vbCrLf & ex.Message)
        End Try

        '*************** End of Documents ********************************

    End Sub



    '******************** Documents *********************************
    Private Sub lvDocuments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDocuments.DoubleClick
        Try
            Cursor.Current = Cursors.WaitCursor
            'Get String to WorkSheet Drive
            Dim worksheetDrive = Switch.GetWorksheetArchiveFolder
            Dim filePath As String = worksheetDrive & "\" & thisEnquiryId & "\"
            Dim fileList As ListView.SelectedListViewItemCollection = lvDocuments.SelectedItems
            Dim item As ListViewItem
            Dim fileName As String
            For Each item In fileList
                fileName = item.SubItems(0).Text
                'MsgBox("Selected file: = " & filePath & fileName)
                'start program to open file
                System.Diagnostics.Process.Start(filePath & fileName)
            Next
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox("The opening of Folder Files caused an exception:" & vbCrLf & ex.Message)
        End Try
    End Sub
    '******************** end of Documents **************************
    '****************************** Printing ************************
    Private Sub PageSetupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageSetupToolStripMenuItem.Click
        Try
            'load page settings and display page setup dialog box
            PageSetupDialog1.PageSettings = printPageSettings
            PageSetupDialog1.ShowDialog()
        Catch ex As Exception
            'display error message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PrintLoanDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintLoanDetailsToolStripMenuItem.Click
        Try
            'Specify current page settings
            PrintDocument1.DefaultPageSettings = printPageSettings
            'Specify document for print preview dialog box and show
            finishedString = "Comments Report for Enquiry " & thisEnquiryId & vbCrLf & vbCrLf & txtbxDetails.Text & vbCrLf & vbCrLf & "Printed by: " & LoggedinName & vbCrLf & DateTime.Now.ToString 'Progress comments txtbx
            stringToPrint = finishedString
            PrintPreviewDialog1.Document = PrintDocument1
            PrintPreviewDialog1.ShowDialog()
        Catch ex As Exception
            'Display error message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim numChars As Integer
        Dim numLines As Integer
        Dim stringForPage As String
        Dim strFormat As New StringFormat
        'Based on page setup, define drawable rectangle on page
        Dim rectDraw As New RectangleF(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, e.MarginBounds.Height)
        'Define area to determine how much text can fit on a page
        'Make height one line shorter to ensure text doesn't clip
        Dim sizeMeasure As New SizeF(e.MarginBounds.Width, e.MarginBounds.Height - printFont.GetHeight(e.Graphics))

        'When drawing long strings, break between words
        strFormat.Trimming = StringTrimming.Word
        'Compute how many chars and lines can fit based on sizeMeasure
        e.Graphics.MeasureString(stringToPrint, printFont, sizeMeasure, strFormat, numChars, numLines)
        'Compute string that will fit on page
        stringForPage = stringToPrint.Substring(0, numChars)
        'Print string on current page
        e.Graphics.DrawString(stringForPage, printFont, Brushes.Black, rectDraw, strFormat)
        'If there is more text, indicate there are more pages
        If numChars < stringToPrint.Length Then
            'Subtract text from string that has been printed
            stringToPrint = stringToPrint.Substring(numChars)
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            'All text has been printed, so restore string
            stringToPrint = finishedString
        End If
    End Sub
    '********************** end of  Printing ************************

    Private Sub ArchiveForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'get minimumSize
        Dim minFormHeight As Integer = Me.MinimumSize.Height - 1
        Dim minFormWidth As Integer = Me.MinimumSize.Width - 1
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            'resize tab control
            TabControl1.Size = New Size(formWidth - 40, formHeight - 175)
            'resize listbox
            lvDocuments.Location = New Point(3, 34)
            lvDocuments.Size = New Size(formWidth - 54, formHeight - 238)
        End If
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub

    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim DisplayDealersFrm As New DisplayDealersForm
            DisplayDealersFrm.PassVariable(thisDealerId)
            DisplayDealersFrm.ShowDialog()
            DisplayDealersFrm.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub MCApplicationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MCApplicationsToolStripMenuItem.Click
        Dim frmMembersCentreArchive As New MembersCentreArchive
        frmMembersCentreArchive.StartPosition = FormStartPosition.CenterParent
        frmMembersCentreArchive.txtApplicationCode.Text = thisApplicationCode
        frmMembersCentreArchive.ShowDialog()


    End Sub
    '************ end of Check Names

    Private Sub btnDetailsEdit_Click(sender As Object, e As EventArgs) Handles btnDetailsEdit.Click
        ToolStripStatusLabel1.Text = ""

        If detailsEditToggle = False Then
            txtbxDetails.ReadOnly = False
            tbpgLoanDetails.BackColor = Color.MistyRose
            btnDetailsEdit.Text = "Cancel"
            btnDetailsEdit.BackColor = Color.MistyRose
            btnSaveSummary.Visible = True
            btnSaveSummary.Enabled = True
            detailsEditToggle = True
        Else
            txtbxDetails.ReadOnly = True
            tbpgLoanDetails.BackColor = Color.YellowGreen
            btnDetailsEdit.Text = "Edit"
            btnDetailsEdit.BackColor = SystemColors.Control
            btnSaveSummary.Visible = False
            btnSaveSummary.Enabled = False
            detailsEditToggle = False

        End If

    End Sub

    Private Sub btnSaveSummary_Click(sender As Object, e As EventArgs) Handles btnSaveSummary.Click
        'update fields
        Try
            Dim archiveEnquiryRow As EnquiryWorkSheetDataSet.ArchiveEnquiryRow
            archiveEnquiryRow = Me.EnquiryWorkSheetDataSet.ArchiveEnquiry(0)
            archiveEnquiryRow.BeginEdit()
            archiveEnquiryRow.EnquirySummary = txtbxDetails.Text
            Me.Validate()
            ArchiveEnquiryBindingSource.EndEdit()
            Me.ArchiveEnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.ArchiveEnquiry)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Enquiry Summary updated."
        Catch ex As Exception
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "Error updating Enquiry Summary!"
            MessageBox.Show(ex.Message, "ERROR updating", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        

    End Sub

    
End Class