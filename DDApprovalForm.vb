﻿Public Class DDApprovalForm

    Dim thisEnquiryId As Integer
    Dim thisEnquiryType As Integer

    'Pass Variable Form Load
    Friend Sub PassVariable(ByVal EnquiryIdVar As Integer, ByVal ContactDisplayNameVar As String, ByVal enquiryType As Integer)

        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)
        cmbxLoanManager.SelectedIndex = -1
        Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, EnquiryIdVar)
        thisEnquiryId = EnquiryIdVar
        thisEnquiryType = enquiryType
        'Set form title
        Me.Text = Me.Text & " - " & ContactDisplayNameVar

    End Sub


    Private Sub DDApprovalForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
        

    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            'Get Row in Enquiry dataset
            Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            EnquiryRow.BeginEdit()
            EnquiryRow.WizardStatus = 7
            'update approval status 0 = nothing, 1 = declined, 2 = approved
            EnquiryRow.ApprovalStatus = MyEnums.ApprovalStatus.Approved
            EnquiryRow.ApproverName = LoggedinName
            EnquiryRow.DDAprovalDate = Date.Now
            'update Current Status
            EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_CAD_APPROVED
            EnquiryRow.CurrentStatusSetDate = DateTime.Now
            'Set Original Enquiry Time
            Dim OriginalTime As DateTime
            OriginalTime = EnquiryRow.DateTime
            'calculate time difference for 30min status
            Dim StatusTimeDiff As Integer
            StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
            If StatusTimeDiff < 30 Then
                EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_CAD_APPROVED
            End If
            'save changes
            Me.Validate()
            EnquiryRow.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

            'create log note
            Dim CommentStr As String
            If thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                CommentStr = "VERIFICATION:Approving this Split Loan Drawdown I have checked the Terms and Conditions of the loan and the drawdown complies with the documentation." & vbCrLf & "Approved Application and Status changed to " & Constants.CONST_CURRENT_STATUS_CAD_APPROVED
            Else
                CommentStr = "VERIFICATION: Approving this application I am satisfied the company's criteria has been met and is within my C.A.D." & _
            vbCrLf & "Approved Application and Status changed to " & Constants.CONST_CURRENT_STATUS_CAD_APPROVED
            End If
            Dim commentId As Integer
            commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Catch ex As Exception
            MsgBox("Due Diligence Approval caused an error:" & vbCrLf & ex.Message)
        End Try
       

    End Sub

    Private Sub btnDeclined_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeclined.Click
        Try
            'Get Row in Enquiry dataset
            Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

            'Application declined
            EnquiryRow.BeginEdit()
            EnquiryRow.WizardStatus = 7
            'update approval status 0 = nothing, 1 = declined, 2 = approved
            EnquiryRow.ApprovalStatus = MyEnums.ApprovalStatus.Declined
            EnquiryRow.ApproverName = LoggedinName
            EnquiryRow.DDAprovalDate = Date.Now
            'update Current Status
            EnquiryRow.CurrentStatus = Constants.CONST_CURRENT_STATUS_DECLINED
            EnquiryRow.CurrentStatusSetDate = DateTime.Now
            'Set Original Enquiry Time
            Dim OriginalTime As DateTime
            OriginalTime = EnquiryRow.DateTime
            'calculate time difference for 30min status
            Dim StatusTimeDiff As Integer
            StatusTimeDiff = DateDiff(DateInterval.Minute, OriginalTime, Now)
            If StatusTimeDiff < 30 Then
                EnquiryRow.Status30mins = Constants.CONST_CURRENT_STATUS_DECLINED
            End If

            'create log note
            Dim CommentStr As String
            CommentStr = "Declined Application and Status changed to " & Constants.CONST_CURRENT_STATUS_DECLINED
            Dim commentId As Integer
            commentId = Util.SetComment(thisEnquiryId, LoggedinName, CommentStr)

            'save changes
            Me.Validate()
            EnquiryRow.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

            Me.DialogResult = System.Windows.Forms.DialogResult.No
        Catch ex As Exception
            MsgBox("Due Diligence Approval caused an error:" & vbCrLf & ex.Message)
        End Try
        

    End Sub

   
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.EnquiryBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

  
End Class