﻿Imports System.Runtime.CompilerServices
Imports System.ComponentModel

Module MainModule
    <STAThread()> _
    Sub Main()
        ' Show msgbox to indicate application has started, comment out later
        'MsgBox("The Main prpcedure is starting the application")

        'EnableVisualStyles
        System.Windows.Forms.Application.EnableVisualStyles()


        '' Provide Splash Form service.
        'Dim SplashFrm As New SplashForm()
        'SplashFrm.ShowDialog()

        ' Provide Login service.
        Dim LoginFrm As New LoginForm()
        LoginFrm.ShowDialog()
        If LoginFrm.DialogResult = DialogResult.Cancel Then
            End
        End If
        ' Declare  variable named frm1 of type Form1.
        Dim frm1 As Form1
        ' Instantiate (create) a new Form1 object and assign
        ' it to variable frm1.
        frm1 = New Form1()
        ' Call the Application class' Run method
        ' passing it the Form1 object created above.
        Application.Run(frm1)
    End Sub
    ' Application wide variables
    'User
    Public UserRole As String
    Public LoggedinName As String 'csteel
    Public LoggedinFullName As String 'Christopher Steel
    Public LoggedinId As Integer
    Public LoggedinBranchId As String
    Public LoggedinEmailAddress As String
    Public LoggedinPermissionLevel As Integer
    Public sw As Stopwatch
    'Searches
    'Public SearchForLastNameLike As String
    'for sizing
    'Public ClientsizeWidthDiff As Integer = 16
    'Public ClientSizeHeightDiff As Integer = 36

    Function loggedinUser() As String
        'get logged in user
        Dim UserIdentityInfo As System.Security.Principal.WindowsIdentity
        Dim user As String
        UserIdentityInfo = System.Security.Principal.WindowsIdentity.GetCurrent()
        user = UserIdentityInfo.Name
        Return user
    End Function

    ''' <summary>
    ''' Tests whether a string represents a valid e-mail address.
    ''' </summary>
    ''' <param name="email"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>This method validates that e-mail addresses have the form "someone@example.com"</remarks>
    Function ValidateEmail(ByVal email As String) As Boolean
        Dim emailRegex As New System.Text.RegularExpressions.Regex("^(?<user>[^@]+)@(?<host>.+)$")
        Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(email)
        Return emailMatch.Success
    End Function

    ''' <summary>
    ''' Get the Description attribute of an Enum
    ''' </summary>
    ''' <param name="enumConstant"></param>
    ''' <returns>String</returns>
    ''' <remarks>Returns Description if attribute exists else with return enum.ToString</remarks>
    <Extension()> Public Function GetEnumDescription(ByVal enumConstant As [Enum]) As String
        Dim attr() As DescriptionAttribute = DirectCast(enumConstant.GetType().GetField(enumConstant.ToString()).GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
        Return If(attr.Length > 0, attr(0).Description, enumConstant.ToString)
    End Function



End Module
