﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports System.ComponentModel



Public Class Form1
    'set variables
    Dim RowCount As Integer 'count the rows in Datatable
    Dim SelectedAgent As Integer = 0
    Dim AllAgentsSelected As Boolean = 0
    Dim SelectedBranch As String = ""
    Dim ThisMaxWidth As Integer
    Dim ThisMaxHeight As Integer
    Dim NewWidth As Integer
    Dim NewHeight As Integer
    Dim NewLocX As Integer
    Dim NewLocY As Integer
    Dim ThisFormText As String
    'to remember selected row
    Dim selectedRowEnquiryId As Integer = 0
    Dim sortedColumnName As String
    Dim sortedColumnIndex As Integer
    'track activation
    Dim activationHasRun As Boolean = False
    Dim sortDirection As ListSortDirection = ListSortDirection.Descending

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Remove Handlers
        RemoveHandler cmbxAgentName.SelectedIndexChanged, AddressOf cmbxAgentName_SelectedIndexChanged
        RemoveHandler cmbxBranch.SelectedIndexChanged, AddressOf cmbxBranch_SelectedIndexChanged
        'RemoveHandler Me.Activated, AddressOf Form1_Activated
        '************* Set defaults
        sortedColumnName = ActiveClientEnquiriesDataGridView.Columns(1).Name
        sortedColumnIndex = ActiveClientEnquiriesDataGridView.Columns(1).Index
        '****************************** check window size and location
        'if no Form1Size settings then
        'get monitor screen size        
        If My.Settings.Form1Size.IsEmpty Then
            If SystemInformation.WorkingArea.Width < Me.MaximumSize.Width Then
                ThisMaxWidth = SystemInformation.WorkingArea.Width
            Else
                ThisMaxWidth = Me.MaximumSize.Width
            End If
            If SystemInformation.WorkingArea.Height < Me.MaximumSize.Height Then
                ThisMaxHeight = SystemInformation.WorkingArea.Height
            Else
                ThisMaxHeight = Me.MaximumSize.Height
            End If
            NewWidth = ThisMaxWidth
            NewHeight = ThisMaxHeight
            Me.Size = New Size(NewWidth, NewHeight)
            'set location
            Me.Location = New Point(0, 0)
        Else
            If SystemInformation.WorkingArea.Width < Me.MaximumSize.Width Then
                ThisMaxWidth = SystemInformation.WorkingArea.Width
            Else
                ThisMaxWidth = Me.MaximumSize.Width
            End If
            If SystemInformation.WorkingArea.Height < Me.MaximumSize.Height Then
                ThisMaxHeight = SystemInformation.WorkingArea.Height
            Else
                ThisMaxHeight = Me.MaximumSize.Height
            End If
            'check user size settings
            If ThisMaxWidth < My.Settings.Form1Size.Width Then
                NewWidth = ThisMaxWidth
            Else
                NewWidth = My.Settings.Form1Size.Width
            End If
            If ThisMaxHeight < My.Settings.Form1Size.Height Then
                NewHeight = ThisMaxHeight
            Else
                NewHeight = My.Settings.Form1Size.Height
            End If
            Me.ClientSize = New Size(NewWidth, NewHeight)
            'check user location settings
            If Me.Location.X > ThisMaxWidth Or Me.Location.X < 0 Then
                NewLocX = 0
            Else
                NewLocX = Me.Location.X
            End If
            If Me.Location.Y > ThisMaxHeight Or Me.Location.Y < 0 Then
                NewLocY = 0
            Else
                NewLocY = Me.Location.Y
            End If
            Me.Location = New Point(NewLocX, NewLocY)
        End If
        '****************************** end of check window size and location
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.Branches' table.
            Me.BranchesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Branches)
            'Set combobox index
            cmbxBranch.SelectedIndex = -1
            'This line of code loads data into the 'EnquiryWorkSheetDataSet.Users' table.
            Me.UsersTableAdapter.FillByActiveUsers(Me.EnquiryWorkSheetDataSet.Users)
            'Set combobox index
            cmbxAgentName.SelectedIndex = -1
            'load data into the 'EnquiryWorkSheetDataSet.ActiveClientEnquiries' table for loggedin User (Enquiry manager)
            Me.ActiveClientEnquiriesTableAdapter.FillByEnquiryManagerId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, LoggedinId)
            'count rows
            RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            If RowCount = 0 Then
                Me.ActiveClientEnquiriesTableAdapter.FillByBranchId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, "YFMAN")
                RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            End If
        Catch ex As Exception
            MsgBox("Loading Data tables caused an error: " & vbCrLf & ex.Message)
        End Try

        'get logged in userName
        lblLoggedInUserValue.Text = "Logged in as: " & LoggedinName
        'Set Form Title
        If My.Settings.IsTest = True Then
            ThisFormText = Me.Text
            Me.Text = "Test Application | " & ThisFormText
            MenuStrip1.BackColor = Color.MistyRose
            StatusStripMessage.BackColor = Color.MistyRose
            ActiveClientEnquiriesBindingNavigator.BackColor = Color.MistyRose
            Me.BackColor = Color.MistyRose
            ActiveClientEnquiriesDataGridView.RowsDefaultCellStyle.SelectionBackColor = Color.OrangeRed
        End If

        ToolStripTxtbxVersion.Text = "Version " & My.Settings.Version

        '*************** Set permissions
        'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
        Select Case LoggedinPermissionLevel
            Case Is = 1 'Readonly
                NewEnquiryToolStripButton.Enabled = False
            Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- 2

            Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                'Can see /use Archive Enquiry in File menu for their user status

            Case Is = 4 'Manager Level

            Case Is = 5
                'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                '--------------------------------------- 5
                ToolsToolStripMenuItem.Visible = True
                AdministrationToolStripMenuItem.Visible = True


            Case Is > 5 'Systems Manager Level, Open.
                '--------------------------------------- 5
                ToolsToolStripMenuItem.Visible = True
                AdministrationToolStripMenuItem.Visible = True
                NetworkToolsToolStripMenuItem.Visible = True
                SystemInformationToolStripMenuItem.Visible = True
                ViewSettingsToolStripMenuItem.Visible = True
                DeleteEnquiryToolStripMenuItem.Visible = True

            Case Else 'Readonly
        End Select
        '*************** end of Set permissions

        'set StatusStrip text
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = CStr(RowCount) + " records found.  Status: Ready"

        'Add Handlers
        AddHandler cmbxAgentName.SelectedIndexChanged, AddressOf cmbxAgentName_SelectedIndexChanged
        AddHandler cmbxBranch.SelectedIndexChanged, AddressOf cmbxBranch_SelectedIndexChanged
        'AddHandler Me.Activated, AddressOf Form1_Activated


    End Sub 'End of Form1 Load

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            NewLocX = 0
        Else
            NewLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            NewLocY = 0
        Else
            NewLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.Form1Location = New Point(NewLocX, NewLocY)
        My.Settings.Form1Size = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
        '******************** check if other forms open and close them first.
        Try
            'create collection to all opened forms (OpenForms collection will change each time you close a form)
            Dim FormsList As New List(Of Form)
            'add all opened forms 
            Dim frm As Form
            For Each frm In Application.OpenForms
                If Not frm.Name = "Form1" Then
                    FormsList.Add(frm)
                End If
            Next
            'now close the forms
            For Each frm In FormsList
                'MsgBox("Closing Form " & frm.Name.ToString)
                frm.Close()
            Next

        Catch ex As Exception
            MsgBox("Closing OpenForms Collection caused an error: " & vbCrLf & ex.Message)
        End Try

        '******************** end of check if other forms open and close them first.

    End Sub

    Private Sub Form1_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout

        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'get minimumSize
        Dim minFormHeight As Integer = Me.MinimumSize.Height - 1
        Dim minFormWidth As Integer = Me.MinimumSize.Width - 1
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            'resize DataGridView
            ActiveClientEnquiriesDataGridView.Size = New Size(formWidth - 16, formHeight - 140)
        End If
    End Sub

    Private Sub ActiveClientEnquiriesDataGridView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles ActiveClientEnquiriesDataGridView.CellFormatting
        Try
            'Set image of folder image and drop enable for DataGridView
            If ActiveClientEnquiriesDataGridView.Columns(e.ColumnIndex).Name = "Folder" Then
                'get WorksheetSharedFolder string
                Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
                'get Enquiry Id
                Dim ActiveEnquiryCode As String = CStr(ActiveClientEnquiriesDataGridView.Rows(e.RowIndex).Cells(2).Value)
                'MsgBox("ActiveEnquiryCode = " + ActiveEnquiryCode)
                Dim fname As String = WkshtStr & "\" & ActiveEnquiryCode
                'Determine whether the directory exists.
                If Directory.Exists(fname) Then
                    'get new image
                    'Dim fileName As String = "AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw"
                    'e.Value = Image.FromFile(fileName)
                    e.Value = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ActiveClientEnquiriesDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ActiveClientEnquiriesDataGridView.DoubleClick
        Dim OpenFormSwitch As Boolean = True
        'set cursor to wait cursor
        Cursor.Current = Cursors.WaitCursor
        Dim WizardStatus As Integer = 0
        Dim SelectedRowView As Data.DataRowView
        Dim SelectedRow As EnquiryWorkSheetDataSet.ActiveClientEnquiriesRow
        'set StatusStrip text
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Opening Client Enquiry Worksheet."
        'Get selected row
        SelectedRowView = CType(ActiveClientEnquiriesBindingSource.Current, System.Data.DataRowView)
        If CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.ActiveClientEnquiriesRow) IsNot Nothing Then
            SelectedRow = CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.ActiveClientEnquiriesRow)
            selectedRowEnquiryId = SelectedRow.EnquiryId
            'reset cursor
            Cursor.Current = Cursors.Default
            'check form with tag value of EnquiryId is not already open, if so then pass focus
            Dim frm As Form
            For Each frm In Application.OpenForms
                If frm.Tag = SelectedRow.EnquiryId Then
                    Dim frmNewLocX As Integer
                    Dim frmNewLocY As Integer
                    'set OpenFormSwitch
                    OpenFormSwitch = False
                    'set form focus
                    'frm.Focus()
                    If frm.Location.X < 0 Then
                        frmNewLocX = 0
                    Else
                        frmNewLocX = frm.Location.X
                    End If
                    If frm.Location.Y < 0 Then
                        frmNewLocY = 0
                    Else
                        frmNewLocY = frm.Location.Y
                    End If
                    frm.Location = New Point(frmNewLocX, frmNewLocY)
                    frm.WindowState = FormWindowState.Normal
                    frm.Activate()
                    frm.FlashWindow(FlashOptions.TIMERNOFG, True, 5)

                    Exit For
                End If
            Next

            If OpenFormSwitch = True Then
                Try
                    'set cursor to wait cursor
                    Cursor.Current = Cursors.WaitCursor
                    'Get Wizard Status for selected Enquiry
                    'Get Connection String Settings
                    Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
                    Dim connection As New SqlConnection(connectionString)
                    Dim selectStatement As String = "SELECT WizardStatus FROM Enquiry WHERE EnquiryId ='" & SelectedRow.EnquiryId & "'"
                    Dim selectCommand As New SqlCommand(selectStatement, connection)
                    Dim EnquiryWizDataAdapter As New SqlDataAdapter(selectCommand)
                    Dim EnquiryWizDataSet As New DataSet
                    Dim EnquiryWizDataTable As New DataTable

                    'dumps results into datatable LoginDataTable
                    EnquiryWizDataAdapter.Fill(EnquiryWizDataTable)
                    'if no matching rows .....
                    If EnquiryWizDataTable.Rows.Count = 0 Then
                        'set StatusStrip text
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Detect Wizard Status failed, Opening Standard Worksheet Form."
                        ' clear the dataTable and the Connect information
                        EnquiryWizDataAdapter = Nothing
                        EnquiryWizDataTable.Clear()
                        'Open Standard Worksheet Form
                        WizardStatus = MyEnums.WizardStatus.AppForm

                        'if there is a matching row
                    ElseIf EnquiryWizDataTable.Rows.Count = 1 Then
                        'get WizardStatus value
                        Dim EnquiryWizDataRow As DataRow = EnquiryWizDataTable.Rows(0)

                        'assign wizardstatus to Form variable WizardStatus 
                        WizardStatus = EnquiryWizDataRow.Item(0)

                        ' clear the dataTable and the Connect information
                        EnquiryWizDataAdapter = Nothing
                        EnquiryWizDataTable.Clear()
                    End If
                    'reset cursor
                    Cursor.Current = Cursors.Default
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                'set cursor to wait cursor
                Cursor.Current = Cursors.WaitCursor
                'launch Application Form and pass Keys
                Select Case WizardStatus
                    Case Is >= MyEnums.WizardStatus.AppForm
                        'launch  Standard Worksheet Form
                        Dim EnquiryForm As New AppForm
                        Try
                            EnquiryForm.PassVariable(SelectedRow.EnquiryId)
                            EnquiryForm.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.Preliminary
                        'launch PreliminaryForm and pass Enquiry Id
                        Dim OldEnquiryForm As New PreliminaryForm
                        Try
                            OldEnquiryForm.PassVariable(SelectedRow.EnquiryId)
                            OldEnquiryForm.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.FollowUp
                        'launch FollowUpForm and pass Enquiry Id
                        Dim FollowUpFrm1 As New FollowUpForm
                        Try
                            FollowUpFrm1.PassVariable(SelectedRow.EnquiryId)
                            FollowUpFrm1.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm1
                        'launch WkShtWizForm1 and pass Enquiry Id
                        Dim WkShtWizFrm1 As New WkShtWizForm1
                        Try
                            WkShtWizFrm1.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm1.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm2
                        'launch WkShtWizForm2 and pass Enquiry Id
                        Dim WkShtWizFrm2 As New WkShtWizForm2
                        Try
                            WkShtWizFrm2.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm2.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm3
                        'launch WkShtWizForm3 and pass Enquiry Id
                        Dim WkShtWizFrm3 As New WkShtWizForm3
                        Try
                            WkShtWizFrm3.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm3.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        'Case Is = 5
                        '    'launch WkShtWizForm4 and pass Enquiry Id
                        '    Dim WkShtWizFrm4 As New WkShtWizForm4
                        '    Try
                        '        WkShtWizFrm4.PassVariable(SelectedRow.EnquiryId)
                        '        WkShtWizFrm4.Show()
                        '    Catch ex As Exception
                        '        MsgBox(ex.Message)
                        '    End Try
                        'Case Is = 6
                        '    'launch WkShtWizForm5 and pass Enquiry Id
                        '    Dim WkShtWizFrm5 As New WkShtWizForm5
                        '    Try
                        '        WkShtWizFrm5.PassVariable(SelectedRow.EnquiryId)
                        '        WkShtWizFrm5.Show()
                        '    Catch ex As Exception
                        '        MsgBox(ex.Message)
                        '    End Try

                End Select
                'reset FormActivated switch
                activationHasRun = False

            Else 'OpenFormSwitch = False
                'reset the cursor
                Cursor.Current = Cursors.Default
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Enquiry Form already open!"
            End If


        Else
            MsgBox("Please select a row")
        End If




    End Sub

    Private Sub ActiveClientEnquiriesDataGridView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ActiveClientEnquiriesDataGridView.KeyDown
        'the ENTER keys.
        If e.KeyCode = Keys.Enter Then
            'set cursor to wait cursor
            Cursor.Current = Cursors.WaitCursor
            Dim WizardStatus As Integer = 0
            Dim SelectedRowView As Data.DataRowView
            Dim SelectedRow As EnquiryWorkSheetDataSet.ActiveClientEnquiriesRow
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Opening Client Enquiry Worksheet."
            'Get selected row
            SelectedRowView = CType(ActiveClientEnquiriesBindingSource.Current, System.Data.DataRowView)
            SelectedRow = CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.ActiveClientEnquiriesRow)
            selectedRowEnquiryId = SelectedRow.EnquiryId
            'reset cursor
            Cursor.Current = Cursors.Default
            'check form with tag value of EnquiryId is not already open, if so then pass focus
            Dim OpenFormSwitch As Boolean = True
            Dim frm As Form
            For Each frm In Application.OpenForms
                If frm.Tag = SelectedRow.EnquiryId Then
                    Dim frmNewLocX As Integer
                    Dim frmNewLocY As Integer
                    'set OpenFormSwitch
                    OpenFormSwitch = False
                    'set form focus
                    'frm.Focus()
                    If frm.Location.X < 0 Then
                        frmNewLocX = 0
                    Else
                        frmNewLocX = frm.Location.X
                    End If
                    If frm.Location.Y < 0 Then
                        frmNewLocY = 0
                    Else
                        frmNewLocY = frm.Location.Y
                    End If
                    frm.Location = New Point(frmNewLocX, frmNewLocY)
                    frm.WindowState = FormWindowState.Normal
                    frm.Activate()
                    frm.FlashWindow(FlashOptions.TIMERNOFG, True, 5)
                    Exit For
                End If
            Next

            If OpenFormSwitch = True Then
                Try
                    'set cursor to wait cursor
                    Cursor.Current = Cursors.WaitCursor
                    'Get Wizard Status for selected Enquiry
                    'Get Connection String Settings
                    Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
                    Dim connection As New SqlConnection(connectionString)
                    Dim selectStatement As String = "SELECT WizardStatus FROM Enquiry WHERE EnquiryId ='" & SelectedRow.EnquiryId & "'"
                    Dim selectCommand As New SqlCommand(selectStatement, connection)
                    Dim EnquiryWizDataAdapter As New SqlDataAdapter(selectCommand)
                    Dim EnquiryWizDataSet As New DataSet
                    Dim EnquiryWizDataTable As New DataTable

                    'dumps results into datatable LoginDataTable
                    EnquiryWizDataAdapter.Fill(EnquiryWizDataTable)
                    'if no matching rows .....
                    If EnquiryWizDataTable.Rows.Count = 0 Then
                        'set StatusStrip text
                        ToolStripStatusLabel1.ForeColor = Color.Red
                        ToolStripStatusLabel1.Text = "Detect Wizard Status failed, Opening Standard Worksheet Form."
                        ' clear the dataTable and the Connect information
                        EnquiryWizDataAdapter = Nothing
                        EnquiryWizDataTable.Clear()
                        'Open Standard Worksheet Form
                        WizardStatus = MyEnums.WizardStatus.AppForm

                        'if there is a matching row
                    ElseIf EnquiryWizDataTable.Rows.Count = 1 Then
                        'get WizardStatus value
                        Dim EnquiryWizDataRow As DataRow = EnquiryWizDataTable.Rows(0)

                        'assign wizardstatus to Form variable WizardStatus 
                        WizardStatus = EnquiryWizDataRow.Item(0)

                        ' clear the dataTable and the Connect information
                        EnquiryWizDataAdapter = Nothing
                        EnquiryWizDataTable.Clear()
                    End If
                    'reset cursor
                    Cursor.Current = Cursors.Default
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                'set cursor to wait cursor
                Cursor.Current = Cursors.WaitCursor
                'launch Application Form and pass Keys
                Select Case WizardStatus
                    Case Is >= MyEnums.WizardStatus.AppForm
                        'launch  Standard Worksheet Form
                        Dim EnquiryForm As New AppForm
                        Try
                            EnquiryForm.PassVariable(SelectedRow.EnquiryId)
                            EnquiryForm.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.Preliminary
                        'launch PreliminaryForm and pass Enquiry Id
                        Dim OldEnquiryForm As New PreliminaryForm
                        Try
                            OldEnquiryForm.PassVariable(SelectedRow.EnquiryId)
                            OldEnquiryForm.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.FollowUp
                        'launch FollowUpForm and pass Enquiry Id
                        Dim FollowUpFrm1 As New FollowUpForm
                        Try
                            FollowUpFrm1.PassVariable(SelectedRow.EnquiryId)
                            FollowUpFrm1.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm1
                        'launch WkShtWizForm1 and pass Enquiry Id
                        Dim WkShtWizFrm1 As New WkShtWizForm1
                        Try
                            WkShtWizFrm1.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm1.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm2
                        'launch WkShtWizForm2 and pass Enquiry Id
                        Dim WkShtWizFrm2 As New WkShtWizForm2
                        Try
                            WkShtWizFrm2.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm2.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm3
                        'launch WkShtWizForm3 and pass Enquiry Id
                        Dim WkShtWizFrm3 As New WkShtWizForm3
                        Try
                            WkShtWizFrm3.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm3.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Case Is = MyEnums.WizardStatus.WizForm4
                        'launch WkShtWizForm4 and pass Enquiry Id
                        Dim WkShtWizFrm4 As New WkShtWizForm4
                        Try
                            WkShtWizFrm4.PassVariable(SelectedRow.EnquiryId)
                            WkShtWizFrm4.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        'Case Is = 6
                        '    'launch WkShtWizForm5 and pass Enquiry Id
                        '    Dim WkShtWizFrm5 As New WkShtWizForm5
                        '    Try
                        '        WkShtWizFrm5.PassVariable(SelectedRow.EnquiryId)
                        '        WkShtWizFrm5.Show()
                        '    Catch ex As Exception
                        '        MsgBox(ex.Message)
                        '    End Try

                End Select

                'reset FormActivated switch
                activationHasRun = False

            Else 'OpenFormSwitch = False
                'reset the cursor
                Cursor.Current = Cursors.Default
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Enquiry Form already open!"
            End If
        End If
    End Sub

    Private Sub Form1_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'Check the following
        'AllAgentsSelected = False
        'SelectedAgent = 0
        'SelectedBranch = ""

        'refresh form on activation
        If activationHasRun = False Then
            If Not SelectedAgent = 0 Then
                Me.ActiveClientEnquiriesTableAdapter.FillByEnquiryManagerId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, SelectedAgent)
            ElseIf Not AllAgentsSelected = False Then
                'load data into the 'EnquiryWorkSheetDataSet.ActiveClientEnquiries' table for All Users
                Me.ActiveClientEnquiriesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries)
            ElseIf Not SelectedBranch = "" Then
                Me.ActiveClientEnquiriesTableAdapter.FillByBranchId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, SelectedBranch)
            Else
                Me.ActiveClientEnquiriesTableAdapter.FillByEnquiryManagerId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, LoggedinId)
                RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
                If RowCount = 0 Then
                    Me.ActiveClientEnquiriesTableAdapter.FillByBranchId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, "YFMAN")
                    RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
                End If
            End If

            'count rows
            RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            If RowCount > 0 Then
                Select Case sortedColumnIndex
                    Case 1
                        sortDirection = ListSortDirection.Descending
                    Case 2
                        sortDirection = ListSortDirection.Descending
                    Case 3
                        sortDirection = ListSortDirection.Descending
                    Case 4
                        sortDirection = ListSortDirection.Descending
                    Case 5
                        sortDirection = ListSortDirection.Ascending
                    Case 6
                        sortDirection = ListSortDirection.Descending
                    Case 7
                        sortDirection = ListSortDirection.Ascending
                    Case Else

                End Select
                'set the sort
                ActiveClientEnquiriesDataGridView.Sort(ActiveClientEnquiriesDataGridView.Columns(sortedColumnIndex), sortDirection)
            End If

            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = CStr(RowCount) + " records found.  Status: Ready"
        End If

        activationHasRun = True

    End Sub

#Region "Menu"

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        'Remove Handlers
        RemoveHandler cmbxAgentName.SelectedIndexChanged, AddressOf cmbxAgentName_SelectedIndexChanged
        'Close this Form
        Me.Close()
    End Sub

    Private Sub DeleteEnquiryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteEnquiryToolStripMenuItem.Click
        Try
            Dim DeleteEnquiryFrm As New DeleteEnquiryForm
            DeleteEnquiryFrm.ShowDialog()
            DeleteEnquiryFrm.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ResetSizeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetSizeToolStripMenuItem.Click
        Dim mHeight As Integer
        Dim mWidth As Integer
        If SystemInformation.WorkingArea.Width < Me.MaximumSize.Width Then
            mWidth = SystemInformation.WorkingArea.Width
        Else
            mWidth = Me.MaximumSize.Width
        End If
        If SystemInformation.WorkingArea.Height < Me.MaximumSize.Height Then
            mHeight = SystemInformation.WorkingArea.Height
        Else
            mHeight = Me.MaximumSize.Height
        End If
        'reset user settings
        My.Settings.AppFormSize = New Size(mWidth, mHeight)
        My.Settings.AppFormLocation = New Point(0, 0)
        My.Settings.Form1Size = New Size(mWidth, mHeight)
        My.Settings.Form1Location = New Point(0, 0)
        My.Settings.PrelimFormSize = New Size(mWidth, mHeight)
        My.Settings.PrelimFormLocation = New Point(0, 0)
        My.Settings.AddPrelimCommentFormLocation = New Point(0, 0)
        My.Settings.AddPrelimCommentFormSize = New Size(800, 400)
        My.Settings.AddCommentFormLocation = New Point(0, 0)
        My.Settings.AddCommentFormSize = New Size(800, 400)
        'save user settings
        My.Settings.Save()
    End Sub

    Private Sub FinPowerNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FinPowerNamesToolStripMenuItem.Click
        Try
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.ShowDialog()
            DisplayFinPowerNamesFrm.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TrueTrackNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrueTrackNamesToolStripMenuItem.Click
        'Call the Results form
        Dim DisplayAllNamesFrm As New DisplayAllNamesForm
        DisplayAllNamesFrm.PassVariable()
        DisplayAllNamesFrm.ShowDialog()
        DisplayAllNamesFrm.Dispose()
    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim DisplayDealersFrm As New DisplayDealersForm
            DisplayDealersFrm.PassVariable()
            DisplayDealersFrm.ShowDialog()
            DisplayDealersFrm.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CheckEnquiryNamesToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripButton.Click
        'Call the Results form
        Dim DisplayAllNamesFrm As New DisplayAllNamesForm
        DisplayAllNamesFrm.PassVariable()
        DisplayAllNamesFrm.ShowDialog()
        DisplayAllNamesFrm.Dispose()
    End Sub

    '********************** Reports
    Private Sub EnquiriesDateRangeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnquiriesDateRangeToolStripMenuItem.Click
        Try
            'launch ReportSelectionsForm and pass Enquiry
            Dim RptSelectFrm As New EnquiryReportSelectionsForm
            RptSelectFrm.ReportSelections_PassVariable("Enquiry")
            RptSelectFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub DDApprovalsbyDateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DDApprovalsbyDateToolStripMenuItem.Click
        Try
            'launch ReportSelectionsForm and pass Enquiry
            Dim ApprovalRptSelectFrm As New ApprovalReportSelectionsForm
            ApprovalRptSelectFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub PayoutsCompletedbyDateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PayoutsCompletedbyDateToolStripMenuItem.Click
        Try
            'launch ReportSelectionsForm and pass Enquiry
            Dim PayoutReportSelectionsFrm As New PayoutReportSelectionsForm
            PayoutReportSelectionsFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching PayoutReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try

    End Sub

    Private Sub PayoutsCompletedbyMonthToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PayoutsCompletedbyMonthToolStripMenuItem.Click
        Try
            'launch ReportSelectionsForm and pass Enquiry
            Dim PayoutMonthReportSelectionsFrm As New PayoutMonthReportSelectionsForm
            PayoutMonthReportSelectionsFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching PayoutMonthReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub CurrentStatusToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CurrentStatusToolStripMenuItem.Click
        Try
            'launch StatusReportSelectionsForm and pass Enquiry
            Dim StatusReportSelectionsFrm As New StatusReportSelectionsForm
            StatusReportSelectionsFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching StatusReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub TsmMarketingEnquiriesDateRange_Click(sender As Object, e As EventArgs) Handles TsmMarketingEnquiriesDateRange.Click
        Try
            'launch MarketingReportSelections and pass Enquiry
            Dim rptSelectFrm As New MarketingEnquiryReportSelectionsForm
            rptSelectFrm.ReportSelections_PassVariable("Enquiry")
            rptSelectFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub KpiSummaryReportToolStripMenuItemClick(sender As Object, e As EventArgs) Handles KPISummaryReportToolStripMenuItem.Click
        Try
            'launch frmKpiSumReport 
            Dim frmKpiSummaryReport As New FrmKpiSumReport
            frmKpiSummaryReport.Show()
        Catch ex As Exception
            MsgBox("Error launching KPI Summary Report Form: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub AMLCTReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AMLCTReportToolStripMenuItem.Click
        Try
            'launch AMLReport 
            Dim frmAMLReport As New AMLReport
            frmAMLReport.Show()
        Catch ex As Exception
            MsgBox("Error launching AML Report Form: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        'Check the following
        'AllAgentsSelected = False
        'SelectedAgent = 0
        'SelectedBranch = ""

        'Neutralise Me.Activated
        activationHasRun = True
        'refresh form 
        If Not SelectedAgent = 0 Then
            Me.ActiveClientEnquiriesTableAdapter.FillByEnquiryManagerId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, SelectedAgent)
        ElseIf Not AllAgentsSelected = False Then
            'load data into the 'EnquiryWorkSheetDataSet.ActiveClientEnquiries' table for All Users
            Me.ActiveClientEnquiriesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries)
        ElseIf Not SelectedBranch = "" Then
            Me.ActiveClientEnquiriesTableAdapter.FillByBranchId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, SelectedBranch)
        Else
            Me.ActiveClientEnquiriesTableAdapter.FillByEnquiryManagerId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, LoggedinId)
            RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            If RowCount = 0 Then
                Me.ActiveClientEnquiriesTableAdapter.FillByBranchId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, "YFMAN")
                RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            End If
        End If
        'count rows
        RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count

        If RowCount > 0 Then
            'set the sort
            ActiveClientEnquiriesDataGridView.Sort(ActiveClientEnquiriesDataGridView.Columns(sortedColumnIndex), ListSortDirection.Descending)
        End If

        If RowCount > 0 Then
            'reselect selected Enquiry in dgv
            If selectedRowEnquiryId > 0 Then
                For Each row As DataGridViewRow In ActiveClientEnquiriesDataGridView.Rows
                    If row.Cells(1).Value.ToString = selectedRowEnquiryId Then
                        ActiveClientEnquiriesBindingSource.Position = row.Index
                        Exit For
                    End If
                Next
            End If
        End If
        'set StatusStrip text
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = CStr(RowCount) + " records found.  Status: Ready"
    End Sub

    Private Sub NewEnquiryToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewEnquiryToolStripButton.Click
        'Launch New Enquiry Form
        Try
            Dim NewEnquiryForm As New PreliminaryForm
            NewEnquiryForm.PassVariable()
            NewEnquiryForm.Show()
            'reset FormActivated switch
            activationHasRun = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSelectAllAgents_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAllAgents.Click
        AllAgentsSelected = True
        SelectedAgent = 0
        SelectedBranch = ""
        'load data into the 'EnquiryWorkSheetDataSet.ActiveClientEnquiries' table for All Users
        Me.ActiveClientEnquiriesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries)
        'count rows
        RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
        'set StatusStrip text
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = CStr(RowCount) + " records found.  Status: Ready"
    End Sub

    Private Sub cmbxAgentName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'load data into the 'EnquiryWorkSheetDataSet.ActiveClientEnquiries' table for selected User (Enquiry manager)
        Try
            SelectedAgent = CInt(cmbxAgentName.SelectedValue)
            AllAgentsSelected = False
            SelectedBranch = ""
            'reload dataset
            Me.ActiveClientEnquiriesTableAdapter.FillByEnquiryManagerId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, SelectedAgent)
            'count rows
            RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = CStr(RowCount) + " records found.  Status: Ready"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub cmbxBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxBranch.SelectedIndexChanged
        'load data into the 'EnquiryWorkSheetDataSet.ActiveClientEnquiries' table for selected User (Enquiry manager)
        If cmbxBranch.SelectedIndex > -1 Then
            SelectedBranch = CStr(cmbxBranch.SelectedValue)
            AllAgentsSelected = False
            SelectedAgent = 0
            'reload dataset
            Me.ActiveClientEnquiriesTableAdapter.FillByBranchId(Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries, SelectedBranch)
            'count rows
            RowCount = Me.EnquiryWorkSheetDataSet.ActiveClientEnquiries.Rows.Count
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = CStr(RowCount) + " records found.  Status: Ready"
        End If
    End Sub

#End Region

#Region "Admin Menu"

    Private Sub AdministrationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdministrationToolStripMenuItem.Click
        Try
            Dim AdminFrm As New AdminForm()
            AdminFrm.ShowDialog()

            'When Add Comment Form closed
            AdminFrm.Dispose()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ViewSettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewSettingsToolStripMenuItem.Click
        'ViewSettingsForm
        Try
            Dim ViewSettingsFrm As New ViewSettingsForm
            ViewSettingsFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ViewSettingsForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub SystemInformationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SystemInformationToolStripMenuItem.Click
        'SystemInfoBrowserForm
        Try
            Dim SystemInfoBrowserFrm As New SystemInfoBrowserForm
            SystemInfoBrowserFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching SystemInfoBrowserForm: " + vbCrLf + ex.Message)
        End Try
    End Sub

    Private Sub NetworkToolsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NetworkToolsToolStripMenuItem.Click
        Try
            Dim NetworkFrm As New NetworkForm()
            NetworkFrm.ShowDialog()

            'When Add Comment Form closed
            NetworkFrm.Dispose()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region




    Private Sub FillByActiveUsersToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Me.UsersTableAdapter.FillByActiveUsers(Me.EnquiryWorkSheetDataSet.Users)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub MCToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MCToolStripButton.Click
        Dim MembersCentreUtilObject As New WebUtil
        MembersCentreUtilObject.MCLink()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = MembersCentreUtilObject.GetLabelForeColor
        ToolStripStatusLabel1.Text = MembersCentreUtilObject.GetLabelText

    End Sub

    Private Sub MCApplicationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MCApplicationsToolStripMenuItem.Click
        MessageBox.Show("Sorry, currently this function is not available", "Not Functioning", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'Dim frmMembersCentreArchive As New MembersCentreArchive
        'frmMembersCentreArchive.StartPosition = FormStartPosition.CenterParent
        'frmMembersCentreArchive.ShowDialog()
    End Sub



    Private Sub ActiveClientEnquiriesDataGridView_Sorted(sender As Object, e As EventArgs) Handles ActiveClientEnquiriesDataGridView.Sorted
        'set the sorted column
        sortedColumnName = ActiveClientEnquiriesDataGridView.SortedColumn.Name
        sortedColumnIndex = ActiveClientEnquiriesDataGridView.SortedColumn.Index
        'reselect selected Enquiry in dgv
        If selectedRowEnquiryId > 0 Then
            For Each row As DataGridViewRow In ActiveClientEnquiriesDataGridView.Rows
                If row.Cells(1).Value.ToString = selectedRowEnquiryId Then
                    ActiveClientEnquiriesBindingSource.Position = row.Index
                    Exit For
                End If
            Next
        End If
    End Sub


   
End Class