﻿Imports System.Data.SqlClient

Public Class EditDetailsForm

    Dim thisEnquiryId As Integer ' set New Enquiry Id
    Dim thisEnquiryCode As String
    Dim thisContactDisplayName As String = "" 'set Contact  Display name
    Dim currentContactDisplayName As String = ""
    'Dim loanComments As String
    Dim dealerNameId As String
    Dim thisDealerName As String = ""
    Dim thisEnquiryManagerId As Integer
    Dim thisEnquiryManagerName As String
    Dim thisEnquiryType As Integer
    Dim thisCurrentStatus As String

    'Loads form with parameters from PreliminaryForm1
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)
        thisEnquiryId = loadEnquiryVar
        Try
            'Remove Handlers
            'RemoveHandler cmbxTown.SelectedIndexChanged, AddressOf cmbxTown_SelectedIndexChanged
            'RemoveHandler cmbxTitle.SelectedIndexChanged, AddressOf cmbxTitle_SelectedIndexChanged
            RemoveHandler txtbxFirstName.TextChanged, AddressOf txtbxFirstName_TextChanged
            RemoveHandler txtbxLastName.TextChanged, AddressOf txtbxLastName_TextChanged
            RemoveHandler cmbxEnquiryType.SelectedIndexChanged, AddressOf cmbxEnquiryType_SelectedIndexChanged

            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, loadEnquiryVar)
            thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
            thisCurrentStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentStatus")
            'loanComments = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanComments")
            dealerNameId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
            thisDealerName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerName")
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher            
            'load users table
            Me.TableAdapterManager.UsersTableAdapter.FillByUserId(Me.EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            currentContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
            'loads data into the 'EnquiryWorkSheetDataSet.NZTowns' table.
            Me.NZTownsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.NZTowns)
            'loads data into the 'EnquiryWorkSheetDataSet.LoanPurpose' table.
            Me.LoanPurposeTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.LoanPurpose)
            'loads data for Suburb list
            Me.SuburbListTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.SuburbList)
            'Set form title
            Me.Text = Me.Text & " - " & thisEnquiryCode

        Catch ex As Exception
            MsgBox("PassVariable caused an error:" & vbCrLf & ex.Message)
        End Try

        Try
            'set datasource for combo box using the Enum members
            cmbxEnquiryType.DisplayMember = "Value" '10/05/2016 Christopher
            cmbxEnquiryType.DataSource = MyEnums.ToList(GetType(MyEnums.EnquiryType))
            cmbxEnquiryType.SelectedIndex = thisEnquiryType
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '*************** set visibility on LoanReason (type of loan)
        If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
            lblCurrentLoanAmt.Visible = True
            txtbxCurrentLoanAmt.Visible = True
            lblTopupNote.Visible = True
        ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            lblCurrentLoanAmt.Visible = False
            txtbxCurrentLoanAmt.Visible = False
            lblTopupNote.Visible = False
            lblBorrow.Visible = False
            txtbxAmount.Visible = False
            lblLoanPurpose.Visible = False
            cmbxLoanPurpose.Visible = False
        Else
            lblCurrentLoanAmt.Visible = False
            txtbxCurrentLoanAmt.Visible = False
            lblTopupNote.Visible = False
            lblBorrow.Visible = True
            txtbxAmount.Visible = True
            lblLoanPurpose.Visible = True
            cmbxLoanPurpose.Visible = True
        End If

        '*************** Get Dealer name
        If Not thisDealerName.Length > 0 Then
            Try
                Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection As New SqlConnection(connectionString)
                Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & dealerNameId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand As New SqlCommand(selectStatement, connection)
                Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand)
                'Dim DealerTempDataSet As New DataSet
                Dim dealerTempDataTable As New DataTable
                'dumps results into datatable LoginDataTable
                dealerTempDataAdapter.Fill(dealerTempDataTable)
                'if no matching rows .....
                If dealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf dealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                    thisDealerName = dealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    lblDealerName.Text = thisDealerName
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                MsgBox("Get Dealer name caused an error:" & vbCrLf & ex.Message)
            End Try
        Else
            lblDealerName.Text = thisDealerName
        End If
        '*************** End of Get Dealer name
        '*************** Set enabled due to ThisCurrentStatus
        Select Case thisCurrentStatus
            Case Constants.CONST_CURRENT_STATUS_CAD_APPROVED
                btnChangeDealer.Enabled = False
                cmbxEnquiryType.Enabled = False
                cmbxLoanPurpose.Enabled = False
                txtbxAmount.Enabled = False
                txtbxCurrentLoanAmt.Enabled = False
            Case Constants.CONST_CURRENT_STATUS_DECLINED, Constants.CONST_CURRENT_STATUS_WITHDRAWN, Constants.CONST_CURRENT_STATUS_ENQUIRY_ENDED, Constants.CONST_CURRENT_STATUS_AUDIT, Constants.CONST_CURRENT_STATUS_AUDIT_FAILED, Constants.CONST_CURRENT_STATUS_PAYOUT, Constants.CONST_CURRENT_STATUS_COMPLETED, Constants.CONST_CURRENT_STATUS_EXPORTED
                '"Declined", "Withdrawn", "Enquiry Ended", "Audit", "Audit Failed", "Payout", "Completed", "Exported"
                btnChangeDealer.Enabled = False
                cmbxEnquiryType.Enabled = False
                cmbxLoanPurpose.Enabled = False
                txtbxAmount.Enabled = False
                txtbxCurrentLoanAmt.Enabled = False
                cmbxTitle.Enabled = False
                txtbxFirstName.Enabled = False
                txtbxLastName.Enabled = False

        End Select
        '*************** Set permissions
        Select Case LoggedinPermissionLevel
            Case Is = 1 'Readonly

            Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- 2
                '--------------------------------------
            Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                'Can see /use Archive Enquiry in File menu for their user status
                '--------------------------------------- 2
                '--------------------------------------- 3
                btnChangeManager.Visible = True
            Case Is = 4 'Manager level
                '--------------------------------------- 2
                '--------------------------------------- 3
                btnChangeManager.Visible = True
                '--------------------------------------- 4

            Case Is = 5
                'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                '--------------------------------------- 2
                '--------------------------------------- 3
                btnChangeManager.Visible = True
                '--------------------------------------- 4

            Case Is > 5 'Systems Manager Level, Open.
                '--------------------------------------- 2
                '--------------------------------------- 3
                btnChangeManager.Visible = True
                '--------------------------------------- 4

            Case Else 'Readonly

        End Select
        '*************** end of Set permissions



    End Sub

    Private Sub EditDetailsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Add Handlers
        'AddHandler cmbxTitle.SelectedIndexChanged, AddressOf cmbxTitle_SelectedIndexChanged
        AddHandler txtbxFirstName.TextChanged, AddressOf txtbxFirstName_TextChanged
        AddHandler txtbxLastName.TextChanged, AddressOf txtbxLastName_TextChanged
        AddHandler cmbxEnquiryType.SelectedIndexChanged, AddressOf cmbxEnquiryType_SelectedIndexChanged

    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.EnquiryBindingSource.CancelEdit()
        'Me.ClientBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'reset values
        thisEnquiryType = cmbxEnquiryType.SelectedIndex.ToString()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        Dim checkFieldsMessage As String = ""
        Try
            'Check Field have value
            If cmbxTitle.SelectedItem = "" And cmbxTitle.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Title" + vbCrLf
            End If
            If txtbxFirstName.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter First Name of Enquirer" + vbCrLf
            End If
            If txtbxLastName.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Last Name of Enquirer" + vbCrLf
            End If
            If txtbxPrelimContact.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Contact Number" + vbCrLf
            End If
            If txtbxSalutation.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Display Name" + vbCrLf
            End If
            If cmbxSuburb.SelectedValue Is Nothing And cmbxSuburb.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please select the Suburb" + vbCrLf
            End If
            If cmbxEnquiryType.SelectedValue Is Nothing And cmbxEnquiryType.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter Reason for Enquiry" + vbCrLf
            End If
            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                If cmbxLoanPurpose.SelectedValue Is Nothing And cmbxLoanPurpose.Text = "" Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter Loan Purpose for Enquiry" + vbCrLf
                End If
                If Not CInt(txtbxAmount.Text) > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter the Amount wanted to borrow" + vbCrLf
                End If
            End If
            
            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                If Not txtbxCurrentLoanAmt.Text.Length > 0 Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter the Current Loan Amount" + vbCrLf
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Else
            Try
                Cursor.Current = Cursors.WaitCursor
                Dim progressStatus As Boolean = False
                'Dim bindSrc1 As BindingSource = Me.ClientBindingSource
                Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
                'Dim result As DialogResult
                'Dim rowView1 As DataRowView
                'Dim rowView2 As DataRowView

                'leave if bindingsource.current is nothing
                If Not bindSrc2.Current Is Nothing Then
                    'rowView1 = CType(bindSrc1.Current, DataRowView)
                    'rowView2 = CType(bindSrc2.Current, DataRowView)
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Saving changed data."

                    ''update Client
                    'Dim clientRow As EnquiryWorkSheetDataSet.ClientRow
                    'clientRow = Me.EnquiryWorkSheetDataSet.Client(0)
                    'clientRow.BeginEdit()
                    ''ClientRow.Postcode = txtbxPostCode.Text
                    ''Update Client Salutation
                    'If Not thisContactDisplayName = "" Then
                    '    clientRow.Salutation = txtbxSalutation.Text
                    'End If

                    'update Enquiry fields
                    Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                    enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    enquiryRow.BeginEdit()
                    enquiryRow.EnquiryType = cmbxEnquiryType.SelectedIndex.ToString() '22/04/2016 Christopher
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        enquiryRow.LoanPurpose = cmbxLoanPurpose.SelectedValue
                    End If
                    If Not thisContactDisplayName = "" Then
                        enquiryRow.ContactDisplayName = txtbxSalutation.Text
                    End If

                    'create log note
                    'Dim dateStr As String
                    'Dim newCommentStr As String
                    Dim commentStr As String
                    ''Create Date string
                    'dateStr = "-- " & DateTime.Now.ToString("f")
                    ''Add Date string to comments
                    commentStr = "Enquiry Details edited"
                    ''check loan comments from dataset exist
                    'If Not loanComments = "" Then
                    '    newCommentStr = commentStr & loanComments
                    'Else
                    '    newCommentStr = commentStr
                    'End If
                    ''update LoanComments with the Preliminary Comments
                    'enquiryRow.LoanComments = newCommentStr

                    Dim commentId As Integer
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

                    'save changes
                    Me.Validate()
                    'bindSrc1.EndEdit()
                    bindSrc2.EndEdit()
                    Me.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                    'Me.ClientTableAdapter.Update(EnquiryWorkSheetDataSet.Client)
                    'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                    progressStatus = True
                    Cursor.Current = Cursors.Default

                    'get changes???
                    'Dim changedRecordsTable As DataTable = dataTable1.GetChanges()

                Else
                    'no current record in binding source - do not proceed
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                    progressStatus = False
                End If

                If progressStatus = True Then
                    Cursor.Current = Cursors.Default
                    'Close this Form
                    Me.DialogResult = System.Windows.Forms.DialogResult.OK

                Else
                    'MsgBox("No record to save, please enter information")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No record to save, please enter information!"

                End If

                Cursor.Current = Cursors.Default
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub txtbxFirstName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxFirstName.TextChanged
        Try
            Dim char1FirstName As String
            'Check String has at least 1 Character
            If txtbxFirstName.Text.Length >= 1 Then
                'Get the  1st Character 
                char1FirstName = txtbxFirstName.Text.Substring(0, 1) & " "
            Else
                char1FirstName = ""
            End If
            thisContactDisplayName = char1FirstName & txtbxLastName.Text
            txtbxSalutation.ForeColor = Color.Red
            txtbxSalutation.Text = thisContactDisplayName
            lblUsed2b.Visible = True
            lblCurrentSalutation.Text = currentContactDisplayName
            btnResetDisplayName.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub cmbxTitle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxTitle.SelectedIndexChanged
    '    Try
    '        Dim Char1FirstName As String
    '        'Check String has at least 1 Character
    '        If txtbxFirstName.Text.Length >= 1 Then
    '            'Get the  1st Character 
    '            Char1FirstName = txtbxFirstName.Text.Substring(0, 1) & " "
    '        Else
    '            Char1FirstName = ""
    '        End If
    '        ThisClientSalutation = cmbxTitle.Text & " " & Char1FirstName & txtbxLastName.Text
    '        txtbxSalutation.Text = ThisClientSalutation
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub txtbxLastName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxLastName.TextChanged
        Try
            Dim char1FirstName As String
            'Check String has at least 1 Character
            If txtbxFirstName.Text.Length >= 1 Then
                'Get the  1st Character 
                char1FirstName = txtbxFirstName.Text.Substring(0, 1) & " "
            Else
                char1FirstName = ""
            End If
            thisContactDisplayName = char1FirstName & txtbxLastName.Text
            txtbxSalutation.ForeColor = Color.Red
            txtbxSalutation.Text = thisContactDisplayName
            lblUsed2b.Visible = True
            lblCurrentSalutation.Text = currentContactDisplayName
            btnResetDisplayName.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub cmbxTown_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxTown.SelectedIndexChanged
    '    Try
    '        Dim ChosenTown As String
    '        ChosenTown = CStr(cmbxTown.SelectedValue)
    '        'This line of code loads data into the 'SuburbList' table for ChosenTown
    '        Me.TableAdapterManager.SuburbListTableAdapter.FillByCity(Me.EnquiryWorkSheetDataSet.SuburbList, ChosenTown)
    '        'MsgBox("Set Town = " & SetTown)
    '        Me.SuburbListBindingSource.ResetBindings(False)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub

    'Private Sub cmbxSuburb_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxSuburb.SelectedIndexChanged
    '    txtbxPostCode.Text = EnquiryWorkSheetDataSet.SuburbList.Rows(SuburbListBindingSource.Position()).Item("PostCode")
    '    CommentsTextBox.Text = EnquiryWorkSheetDataSet.SuburbList.Rows(SuburbListBindingSource.Position()).Item("Comments")
    'End Sub



    Private Sub btnChangeDealer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeDealer.Click
        'launch ChangeDealerForm and pass Enquiry Id
        Dim changeDealerFrm As New ChangeDealerForm
        changeDealerFrm.PassVariable(thisEnquiryId)
        'ChangeDealerFrm.ShowDialog()
        If (changeDealerFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            '*************** Get Dealer name
            Try
                'get Dealer ID
                dealerNameId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
                Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection As New SqlConnection(connectionString)
                Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & dealerNameId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand As New SqlCommand(selectStatement, connection)
                Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand)
                'Dim dealerTempDataSet As New DataSet
                Dim dealerTempDataTable As New DataTable
                'dumps results into datatable LoginDataTable
                dealerTempDataAdapter.Fill(dealerTempDataTable)
                'if no matching rows .....
                If dealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf dealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                    thisDealerName = dealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    lblDealerName.Text = thisDealerName
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                MsgBox("Get Dealer name caused an error:" & vbCrLf & ex.Message)
            End Try

            '*************** End of Get Dealer name
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Dealer Record updated successfully!"
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Updating of Dealer Record cancelled. Status: Ready."
        End If
        changeDealerFrm.Dispose()


    End Sub

    Private Sub btnChangeManager_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeManager.Click
        'launch ChangeDealerForm and pass Enquiry Id
        Dim changeUserFrm As New ChangeUserForm
        changeUserFrm.PassVariable(thisEnquiryId)
        'ChangeDealerFrm.ShowDialog()
        If (changeUserFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'get enquiry manger id
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            'load users table
            Me.TableAdapterManager.UsersTableAdapter.FillByUserId(Me.EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Enquiry Manager Record updated successfully!"
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Updating of Enquiry Manager Record cancelled. Status: Ready."
        End If
        changeUserFrm.Dispose()
    End Sub

    Private Sub btnResetDisplayName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetDisplayName.Click
        txtbxSalutation.Text = currentContactDisplayName
    End Sub

    Private Sub cmbxEnquiryType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxEnquiryType.SelectedIndexChanged
        'reset values
        thisEnquiryType = cmbxEnquiryType.SelectedIndex.ToString()
        If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
            lblCurrentLoanAmt.Visible = True
            txtbxCurrentLoanAmt.Visible = True
            lblTopupNote.Visible = True
            If Not txtbxCurrentLoanAmt.TextLength > 0 Then
                btnUpdate.Enabled = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Please enter the Current Loan Amount"
            Else
                btnUpdate.Enabled = True
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Status:  Check the Current Loan Amount"
            End If

        ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            lblCurrentLoanAmt.Visible = False
            txtbxCurrentLoanAmt.Visible = False
            lblTopupNote.Visible = False
            lblBorrow.Visible = False
            txtbxAmount.Visible = False
            lblLoanPurpose.Visible = False
            cmbxLoanPurpose.Visible = False
            btnUpdate.Enabled = True
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status:  Ready"

        Else
            lblCurrentLoanAmt.Visible = False
            txtbxCurrentLoanAmt.Visible = False
            lblTopupNote.Visible = False
            lblBorrow.Visible = True
            txtbxAmount.Visible = True
            lblLoanPurpose.Visible = True
            cmbxLoanPurpose.Visible = True
            btnUpdate.Enabled = True
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status:  Ready"
        End If

       
    End Sub



    Private Sub txtbxCurrentLoanAmt_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxCurrentLoanAmt.LostFocus
        If txtbxCurrentLoanAmt.Text.Length > 0 Then
            btnUpdate.Enabled = True
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status:  Ready"
        End If
    End Sub

   
   
    
End Class