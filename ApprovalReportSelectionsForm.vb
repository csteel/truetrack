﻿Imports System.Globalization

Public Class ApprovalReportSelectionsForm
    Dim selectedrb As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim StartDate As Date
    Dim EndDate As Date
    Dim GroupBy As String
    Dim OrderBy As String
    Dim User As String = ""
    Dim ApprovalStatus As Integer

    Private Sub ActivityReportSelectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'loads data into the 'EnquiryWorkSheetDataSet.Users' table. 
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)
        'set default values
        Me.cmbxGroupBy.SelectedIndex = 0
        Me.cmbxOrderBy.SelectedIndex = 0

    End Sub

    Private Sub ckbxUsers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxUsers.CheckedChanged
        If ckbxUsers.CheckState = CheckState.Checked Then
            lstbxUsers.Enabled = True
        Else
            lstbxUsers.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Cursor.Current = Cursors.WaitCursor
        'get values
        StartDate = DateTimePicker1.Value.ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        EndDate = DateTimePicker2.Value.AddDays(1).ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        'get users if selected
        If lstbxUsers.Enabled = True Then
            User = lstbxUsers.SelectedValue.ToString
            'MsgBox("Selected User = " + User)
        Else
            User = ""
        End If
        'get Approval status
        If rbApproved.Checked = True Then
            ApprovalStatus = MyEnums.ApprovalStatus.Approved
        ElseIf rbDeclined.Checked = True Then
            ApprovalStatus = MyEnums.ApprovalStatus.Declined
        End If
        'get Group By
        GroupBy = cmbxGroupBy.SelectedItem.ToString
        'get Order by
        OrderBy = cmbxOrderBy.SelectedItem.ToString
        Try
            'launch ReportSelectionsForm and pass Enquiry parameters
            Dim ReportApprovalFrm As New ReportApprovalForm
            ReportApprovalFrm.ReportSelections_PassVariable(StartDate, EndDate, GroupBy, OrderBy, User, ApprovalStatus)
            ReportApprovalFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportApprovalForm: " + vbCrLf + ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

  
End Class