﻿Public Class DeleteRecordForm

    Dim ThisEnquiryId As Integer
    Dim ThisEnquiryResult As String

    'Pass Variable Form Load
    Friend Sub PassVariable(ByVal EnquiryIdVar As Integer, ByVal ClientSalutationVar As String)
        ThisEnquiryId = EnquiryIdVar
        Me.EnquiryTableAdapter1.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, EnquiryIdVar)
        'Set form title
        Me.Text = ClientSalutationVar & " - " & Me.Text

        '' Set the MaximizeBox to false to remove the maximize box.
        'Me.MaximizeBox = False
        '' Set the MinimizeBox to false to remove the minimize box.
        'Me.MinimizeBox = False
        


    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.EnquiryBindingSource1.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'check there is text in txtbxDeleteExplain.Text
        Dim checkFieldsMessage As String = ""
        Try
            'Check Field have value
            If txtbxDeleteExplain.Text = "" Then
                checkFieldsMessage = "Please enter reason for deleting this Budget Item " + vbCrLf
            End If
            If Not checkFieldsMessage = "" Then
                MsgBox(checkFieldsMessage)
                Me.DialogResult = System.Windows.Forms.DialogResult.None
                txtbxDeleteExplain.Select()
            Else
                'Get Row in Enquiry dataset
                Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)

                'Application declined
                EnquiryRow.BeginEdit()
                Dim BudgetComments As String = EnquiryRow.BudgetExplanation
                Dim CommentStr As String = ""
                Dim NewCommentStr As String = ""
                'Create Date string
                Dim DateStr As String = "-- " & DateTime.Now.ToString("f")
                'Add Date string to comments
                CommentStr = DateStr & " | " & LoggedinName & ": Budget record deleted " & vbCrLf & txtbxDeleteExplain.Text & vbCrLf & vbCrLf
                'check loan comments from dataset exist
                If BudgetComments.Length > 0 Then
                    NewCommentStr = CommentStr & BudgetComments
                Else
                    NewCommentStr = CommentStr
                End If
                'update BudgetComments 
                EnquiryRow.BudgetExplanation = NewCommentStr
                'save changes
                Me.Validate()
                EnquiryRow.EndEdit()
                Me.EnquiryTableAdapter1.Update(Me.EnquiryWorkSheetDataSet.Enquiry)

                Me.DialogResult = System.Windows.Forms.DialogResult.OK
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

   
End Class