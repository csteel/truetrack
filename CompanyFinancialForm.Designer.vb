﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CompanyFinancialForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CompanyFinancialForm))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeclineToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WithdrawToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.lblContactAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblContactName = New System.Windows.Forms.Label()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblContactNumber = New System.Windows.Forms.Label()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnQRGList = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.gpbxIncomeExpenses = New System.Windows.Forms.GroupBox()
        Me.tlpIncomeExpenses = New System.Windows.Forms.TableLayoutPanel()
        Me.ckbxIncomeExpenses = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ckbxForcast = New System.Windows.Forms.CheckBox()
        Me.ckbxGST = New System.Windows.Forms.CheckBox()
        Me.ckbxYTD = New System.Windows.Forms.CheckBox()
        Me.ckbxPreviousYear = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtbxIncomeNotes = New AppWhShtB.SpellBox()
        Me.txtbxExpenseNotes = New AppWhShtB.SpellBox()
        Me.gpbxPosition = New System.Windows.Forms.GroupBox()
        Me.tlpPosition2 = New System.Windows.Forms.TableLayoutPanel()
        Me.ckbxAccountsPayable = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ckbxGSTreturns = New System.Windows.Forms.CheckBox()
        Me.ckbxIncomeTax = New System.Windows.Forms.CheckBox()
        Me.ckbxPAYEreturns = New System.Windows.Forms.CheckBox()
        Me.lblTaxes = New System.Windows.Forms.Label()
        Me.txtbxAccountsPayable = New AppWhShtB.SpellBox()
        Me.txtbxTaxes = New AppWhShtB.SpellBox()
        Me.tlpPosition = New System.Windows.Forms.TableLayoutPanel()
        Me.lblDebt = New System.Windows.Forms.Label()
        Me.ckbxAssetLiabilities = New System.Windows.Forms.CheckBox()
        Me.txtbxDebt = New AppWhShtB.SpellBox()
        Me.gpbxEarnings = New System.Windows.Forms.GroupBox()
        Me.tlpEarnings = New System.Windows.Forms.TableLayoutPanel()
        Me.ckbxEarnings = New System.Windows.Forms.CheckBox()
        Me.lblProfit = New System.Windows.Forms.Label()
        Me.txtbxEarnings = New AppWhShtB.SpellBox()
        Me.gpbxSalaries = New System.Windows.Forms.GroupBox()
        Me.tlpSalaries = New System.Windows.Forms.TableLayoutPanel()
        Me.ckbxSalaries = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtbxSalaries = New AppWhShtB.SpellBox()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.lblCurrentLoan = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.gpbxManager.SuspendLayout()
        Me.gpbxStatus.SuspendLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.gpbxIncomeExpenses.SuspendLayout()
        Me.tlpIncomeExpenses.SuspendLayout()
        Me.gpbxPosition.SuspendLayout()
        Me.tlpPosition2.SuspendLayout()
        Me.tlpPosition.SuspendLayout()
        Me.gpbxEarnings.SuspendLayout()
        Me.tlpEarnings.SuspendLayout()
        Me.gpbxSalaries.SuspendLayout()
        Me.tlpSalaries.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.WWWToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 248
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Enabled = False
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEToolStripMenuItem
        '
        Me.EndEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeclineToolStripMenuItem, Me.WithdrawToolStripMenuItem})
        Me.EndEToolStripMenuItem.Name = "EndEToolStripMenuItem"
        Me.EndEToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEToolStripMenuItem.Text = "End Enquiry"
        Me.EndEToolStripMenuItem.Visible = False
        '
        'DeclineToolStripMenuItem
        '
        Me.DeclineToolStripMenuItem.Name = "DeclineToolStripMenuItem"
        Me.DeclineToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DeclineToolStripMenuItem.Text = "Declined"
        '
        'WithdrawToolStripMenuItem
        '
        Me.WithdrawToolStripMenuItem.Name = "WithdrawToolStripMenuItem"
        Me.WithdrawToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.WithdrawToolStripMenuItem.Text = "Withdrawn"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "Enquiries Names"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(153, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "YFL Website"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Website - Ctrl+Alt+W"
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeyDisplayString = "CTRL+ALT+M"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search TrueTrack Names - Ctrl+Alt+M"
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(740, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 250
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(842, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 249
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblContactAddress
        '
        Me.lblContactAddress.AutoSize = True
        Me.lblContactAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblContactAddress.Name = "lblContactAddress"
        Me.lblContactAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblContactAddress.TabIndex = 263
        Me.lblContactAddress.Text = "ClientAddress"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 262
        Me.lblAddress.Text = "Address:"
        '
        'lblContactName
        '
        Me.lblContactName.AutoSize = True
        Me.lblContactName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactName.Location = New System.Drawing.Point(15, 62)
        Me.lblContactName.Name = "lblContactName"
        Me.lblContactName.Size = New System.Drawing.Size(75, 13)
        Me.lblContactName.TabIndex = 261
        Me.lblContactName.Text = "Client Name"
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(59, 86)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(0, 13)
        Me.lblDealerName.TabIndex = 260
        '
        'lblContactNumber
        '
        Me.lblContactNumber.AutoSize = True
        Me.lblContactNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNumber.Location = New System.Drawing.Point(594, 62)
        Me.lblContactNumber.Name = "lblContactNumber"
        Me.lblContactNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblContactNumber.TabIndex = 259
        Me.lblContactNumber.Text = "Number"
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 258
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanReason", True))
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(299, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(190, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 257
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(492, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 256
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 86)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 255
        Me.lblDealer.Text = "Dealer:"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(187, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 254
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(155, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 253
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(92, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 252
        Me.lblEnquiryNumberValue.Text = "0000"
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 251
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 990)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(944, 22)
        Me.StatusStrip1.TabIndex = 266
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Ready"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnViewDocs)
        Me.Panel1.Controls.Add(Me.btnQRGList)
        Me.Panel1.Controls.Add(Me.btnDocs)
        Me.Panel1.Controls.Add(Me.btnSaveAndExit)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnBack)
        Me.Panel1.Controls.Add(Me.btnNext)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 961)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 29)
        Me.Panel1.TabIndex = 267
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(34, 3)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 28
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnQRGList
        '
        Me.btnQRGList.Location = New System.Drawing.Point(106, 3)
        Me.btnQRGList.Name = "btnQRGList"
        Me.btnQRGList.Size = New System.Drawing.Size(75, 23)
        Me.btnQRGList.TabIndex = 27
        Me.btnQRGList.Text = "QRGuide"
        Me.btnQRGList.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(3, 3)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 26
        Me.ToolTip1.SetToolTip(Me.btnDocs, "You can 'Drag and Drop' documents from Windows Explorer or your Desktop.")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(275, 3)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 25
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'btnFinish
        '
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(617, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.TabIndex = 17
        Me.btnFinish.Text = "Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(374, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(455, 3)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 15
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(536, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 16
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.gpbxIncomeExpenses)
        Me.Panel2.Controls.Add(Me.gpbxPosition)
        Me.Panel2.Controls.Add(Me.gpbxEarnings)
        Me.Panel2.Controls.Add(Me.gpbxSalaries)
        Me.Panel2.Location = New System.Drawing.Point(0, 114)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(944, 846)
        Me.Panel2.TabIndex = 268
        '
        'gpbxIncomeExpenses
        '
        Me.gpbxIncomeExpenses.Controls.Add(Me.tlpIncomeExpenses)
        Me.gpbxIncomeExpenses.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxIncomeExpenses.Location = New System.Drawing.Point(12, 3)
        Me.gpbxIncomeExpenses.Name = "gpbxIncomeExpenses"
        Me.gpbxIncomeExpenses.Size = New System.Drawing.Size(910, 301)
        Me.gpbxIncomeExpenses.TabIndex = 4
        Me.gpbxIncomeExpenses.TabStop = False
        Me.gpbxIncomeExpenses.Text = "Income and Expenses"
        '
        'tlpIncomeExpenses
        '
        Me.tlpIncomeExpenses.ColumnCount = 3
        Me.tlpIncomeExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpIncomeExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpIncomeExpenses.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpIncomeExpenses.Controls.Add(Me.ckbxIncomeExpenses, 0, 6)
        Me.tlpIncomeExpenses.Controls.Add(Me.Label3, 0, 5)
        Me.tlpIncomeExpenses.Controls.Add(Me.Label2, 0, 4)
        Me.tlpIncomeExpenses.Controls.Add(Me.ckbxForcast, 1, 3)
        Me.tlpIncomeExpenses.Controls.Add(Me.ckbxGST, 1, 2)
        Me.tlpIncomeExpenses.Controls.Add(Me.ckbxYTD, 1, 1)
        Me.tlpIncomeExpenses.Controls.Add(Me.ckbxPreviousYear, 1, 0)
        Me.tlpIncomeExpenses.Controls.Add(Me.Label1, 0, 0)
        Me.tlpIncomeExpenses.Controls.Add(Me.txtbxIncomeNotes, 1, 4)
        Me.tlpIncomeExpenses.Controls.Add(Me.txtbxExpenseNotes, 1, 5)
        Me.tlpIncomeExpenses.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpIncomeExpenses.Location = New System.Drawing.Point(6, 19)
        Me.tlpIncomeExpenses.Name = "tlpIncomeExpenses"
        Me.tlpIncomeExpenses.Padding = New System.Windows.Forms.Padding(3)
        Me.tlpIncomeExpenses.RowCount = 7
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75.0!))
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75.0!))
        Me.tlpIncomeExpenses.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpIncomeExpenses.Size = New System.Drawing.Size(898, 276)
        Me.tlpIncomeExpenses.TabIndex = 0
        '
        'ckbxIncomeExpenses
        '
        Me.ckbxIncomeExpenses.AutoSize = True
        Me.ckbxIncomeExpenses.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyIncomeExpensesSatis", True))
        Me.ckbxIncomeExpenses.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxIncomeExpenses.Location = New System.Drawing.Point(96, 256)
        Me.ckbxIncomeExpenses.Name = "ckbxIncomeExpenses"
        Me.ckbxIncomeExpenses.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxIncomeExpenses.Size = New System.Drawing.Size(204, 19)
        Me.ckbxIncomeExpenses.TabIndex = 12
        Me.ckbxIncomeExpenses.Text = "Income and expenses are satisfactory"
        Me.ckbxIncomeExpenses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxIncomeExpenses.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label3.Location = New System.Drawing.Point(16, 178)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(284, 75)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Comment on Past, Present and Future expenses, these are satisfactory because ..."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label2.Location = New System.Drawing.Point(45, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(255, 75)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Comment on Past, Present and Future income, this is satisfactory because ..."
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxForcast
        '
        Me.ckbxForcast.AutoSize = True
        Me.ckbxForcast.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyIncomeForcast", True))
        Me.ckbxForcast.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxForcast.Location = New System.Drawing.Point(306, 81)
        Me.ckbxForcast.Name = "ckbxForcast"
        Me.ckbxForcast.Size = New System.Drawing.Size(67, 19)
        Me.ckbxForcast.TabIndex = 5
        Me.ckbxForcast.Text = "Forecast"
        Me.ckbxForcast.UseVisualStyleBackColor = True
        '
        'ckbxGST
        '
        Me.ckbxGST.AutoSize = True
        Me.ckbxGST.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyIncomeGST", True))
        Me.ckbxGST.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxGST.Location = New System.Drawing.Point(306, 56)
        Me.ckbxGST.Name = "ckbxGST"
        Me.ckbxGST.Size = New System.Drawing.Size(83, 19)
        Me.ckbxGST.TabIndex = 4
        Me.ckbxGST.Text = "GST returns"
        Me.ckbxGST.UseVisualStyleBackColor = True
        '
        'ckbxYTD
        '
        Me.ckbxYTD.AutoSize = True
        Me.ckbxYTD.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyIncomeYTD", True))
        Me.ckbxYTD.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxYTD.Location = New System.Drawing.Point(306, 31)
        Me.ckbxYTD.Name = "ckbxYTD"
        Me.ckbxYTD.Size = New System.Drawing.Size(131, 19)
        Me.ckbxYTD.TabIndex = 3
        Me.ckbxYTD.Text = "Year to date financials"
        Me.ckbxYTD.UseVisualStyleBackColor = True
        '
        'ckbxPreviousYear
        '
        Me.ckbxPreviousYear.AutoSize = True
        Me.ckbxPreviousYear.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyIncomePrevious", True))
        Me.ckbxPreviousYear.Dock = System.Windows.Forms.DockStyle.Left
        Me.ckbxPreviousYear.Location = New System.Drawing.Point(306, 6)
        Me.ckbxPreviousYear.Name = "ckbxPreviousYear"
        Me.ckbxPreviousYear.Size = New System.Drawing.Size(142, 19)
        Me.ckbxPreviousYear.TabIndex = 2
        Me.ckbxPreviousYear.Text = "Previous years financials"
        Me.ckbxPreviousYear.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label1.Location = New System.Drawing.Point(175, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 25)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Income is established by:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtbxIncomeNotes
        '
        Me.tlpIncomeExpenses.SetColumnSpan(Me.txtbxIncomeNotes, 2)
        Me.txtbxIncomeNotes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyIncomeNotes", True))
        Me.txtbxIncomeNotes.IsReadOnly = False
        Me.txtbxIncomeNotes.Location = New System.Drawing.Point(306, 106)
        Me.txtbxIncomeNotes.MaxLength = 1024
        Me.txtbxIncomeNotes.MultiLine = True
        Me.txtbxIncomeNotes.Name = "txtbxIncomeNotes"
        Me.txtbxIncomeNotes.SelectedText = ""
        Me.txtbxIncomeNotes.SelectionLength = 0
        Me.txtbxIncomeNotes.SelectionStart = 0
        Me.txtbxIncomeNotes.Size = New System.Drawing.Size(586, 69)
        Me.txtbxIncomeNotes.TabIndex = 8
        Me.txtbxIncomeNotes.WordWrap = True
        Me.txtbxIncomeNotes.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxExpenseNotes
        '
        Me.tlpIncomeExpenses.SetColumnSpan(Me.txtbxExpenseNotes, 2)
        Me.txtbxExpenseNotes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyExpensesNotes", True))
        Me.txtbxExpenseNotes.IsReadOnly = False
        Me.txtbxExpenseNotes.Location = New System.Drawing.Point(306, 181)
        Me.txtbxExpenseNotes.MaxLength = 1024
        Me.txtbxExpenseNotes.MultiLine = True
        Me.txtbxExpenseNotes.Name = "txtbxExpenseNotes"
        Me.txtbxExpenseNotes.SelectedText = ""
        Me.txtbxExpenseNotes.SelectionLength = 0
        Me.txtbxExpenseNotes.SelectionStart = 0
        Me.txtbxExpenseNotes.Size = New System.Drawing.Size(586, 69)
        Me.txtbxExpenseNotes.TabIndex = 9
        Me.txtbxExpenseNotes.WordWrap = True
        Me.txtbxExpenseNotes.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxPosition
        '
        Me.gpbxPosition.Controls.Add(Me.tlpPosition2)
        Me.gpbxPosition.Controls.Add(Me.tlpPosition)
        Me.gpbxPosition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxPosition.Location = New System.Drawing.Point(12, 515)
        Me.gpbxPosition.Name = "gpbxPosition"
        Me.gpbxPosition.Size = New System.Drawing.Size(910, 304)
        Me.gpbxPosition.TabIndex = 3
        Me.gpbxPosition.TabStop = False
        Me.gpbxPosition.Text = "Financial position"
        '
        'tlpPosition2
        '
        Me.tlpPosition2.ColumnCount = 3
        Me.tlpPosition2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 303.0!))
        Me.tlpPosition2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpPosition2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpPosition2.Controls.Add(Me.ckbxAccountsPayable, 0, 0)
        Me.tlpPosition2.Controls.Add(Me.Label5, 0, 1)
        Me.tlpPosition2.Controls.Add(Me.ckbxGSTreturns, 0, 2)
        Me.tlpPosition2.Controls.Add(Me.ckbxIncomeTax, 0, 3)
        Me.tlpPosition2.Controls.Add(Me.ckbxPAYEreturns, 0, 4)
        Me.tlpPosition2.Controls.Add(Me.lblTaxes, 0, 5)
        Me.tlpPosition2.Controls.Add(Me.txtbxAccountsPayable, 1, 1)
        Me.tlpPosition2.Controls.Add(Me.txtbxTaxes, 1, 5)
        Me.tlpPosition2.Location = New System.Drawing.Point(6, 92)
        Me.tlpPosition2.Name = "tlpPosition2"
        Me.tlpPosition2.RowCount = 6
        Me.tlpPosition2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpPosition2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpPosition2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpPosition2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpPosition2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpPosition2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpPosition2.Size = New System.Drawing.Size(898, 202)
        Me.tlpPosition2.TabIndex = 21
        '
        'ckbxAccountsPayable
        '
        Me.ckbxAccountsPayable.AutoSize = True
        Me.ckbxAccountsPayable.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyAcctsPaySatis", True))
        Me.ckbxAccountsPayable.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxAccountsPayable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxAccountsPayable.Location = New System.Drawing.Point(37, 3)
        Me.ckbxAccountsPayable.Name = "ckbxAccountsPayable"
        Me.ckbxAccountsPayable.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxAccountsPayable.Size = New System.Drawing.Size(263, 36)
        Me.ckbxAccountsPayable.TabIndex = 15
        Me.ckbxAccountsPayable.Text = "Accounts payable are satisfactory and have been " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "identified"
        Me.ckbxAccountsPayable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ckbxAccountsPayable.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(60, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(240, 42)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Comment on Accounts payable, are they current?"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxGSTreturns
        '
        Me.ckbxGSTreturns.AutoSize = True
        Me.ckbxGSTreturns.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyGstUTD", True))
        Me.ckbxGSTreturns.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxGSTreturns.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxGSTreturns.Location = New System.Drawing.Point(79, 87)
        Me.ckbxGSTreturns.Name = "ckbxGSTreturns"
        Me.ckbxGSTreturns.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxGSTreturns.Size = New System.Drawing.Size(221, 19)
        Me.ckbxGSTreturns.TabIndex = 17
        Me.ckbxGSTreturns.Text = "GST returns and payments are up to date"
        Me.ckbxGSTreturns.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxGSTreturns.UseVisualStyleBackColor = True
        '
        'ckbxIncomeTax
        '
        Me.ckbxIncomeTax.AutoSize = True
        Me.ckbxIncomeTax.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyITUTD", True))
        Me.ckbxIncomeTax.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxIncomeTax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxIncomeTax.Location = New System.Drawing.Point(45, 112)
        Me.ckbxIncomeTax.Name = "ckbxIncomeTax"
        Me.ckbxIncomeTax.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxIncomeTax.Size = New System.Drawing.Size(255, 19)
        Me.ckbxIncomeTax.TabIndex = 18
        Me.ckbxIncomeTax.Text = "Income Tax returns and payments are up to date"
        Me.ckbxIncomeTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxIncomeTax.UseVisualStyleBackColor = True
        '
        'ckbxPAYEreturns
        '
        Me.ckbxPAYEreturns.AutoSize = True
        Me.ckbxPAYEreturns.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyPayeUTD", True))
        Me.ckbxPAYEreturns.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxPAYEreturns.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbxPAYEreturns.Location = New System.Drawing.Point(73, 137)
        Me.ckbxPAYEreturns.Name = "ckbxPAYEreturns"
        Me.ckbxPAYEreturns.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxPAYEreturns.Size = New System.Drawing.Size(227, 19)
        Me.ckbxPAYEreturns.TabIndex = 19
        Me.ckbxPAYEreturns.Text = "PAYE returns and payments are up to date"
        Me.ckbxPAYEreturns.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxPAYEreturns.UseVisualStyleBackColor = True
        '
        'lblTaxes
        '
        Me.lblTaxes.AutoSize = True
        Me.lblTaxes.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTaxes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxes.Location = New System.Drawing.Point(202, 159)
        Me.lblTaxes.Name = "lblTaxes"
        Me.lblTaxes.Size = New System.Drawing.Size(98, 43)
        Me.lblTaxes.TabIndex = 22
        Me.lblTaxes.Text = "Comment on Taxes"
        Me.lblTaxes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtbxAccountsPayable
        '
        Me.tlpPosition2.SetColumnSpan(Me.txtbxAccountsPayable, 2)
        Me.txtbxAccountsPayable.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyAcctsPayNotes", True))
        Me.txtbxAccountsPayable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxAccountsPayable.IsReadOnly = False
        Me.txtbxAccountsPayable.Location = New System.Drawing.Point(306, 45)
        Me.txtbxAccountsPayable.MaxLength = 1024
        Me.txtbxAccountsPayable.MultiLine = True
        Me.txtbxAccountsPayable.Name = "txtbxAccountsPayable"
        Me.txtbxAccountsPayable.SelectedText = ""
        Me.txtbxAccountsPayable.SelectionLength = 0
        Me.txtbxAccountsPayable.SelectionStart = 0
        Me.txtbxAccountsPayable.Size = New System.Drawing.Size(586, 36)
        Me.txtbxAccountsPayable.TabIndex = 16
        Me.txtbxAccountsPayable.WordWrap = True
        Me.txtbxAccountsPayable.Child = New System.Windows.Controls.TextBox()
        '
        'txtbxTaxes
        '
        Me.tlpPosition2.SetColumnSpan(Me.txtbxTaxes, 2)
        Me.txtbxTaxes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyTaxesNotes", True))
        Me.txtbxTaxes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbxTaxes.IsReadOnly = False
        Me.txtbxTaxes.Location = New System.Drawing.Point(306, 162)
        Me.txtbxTaxes.MaxLength = 1024
        Me.txtbxTaxes.MultiLine = True
        Me.txtbxTaxes.Name = "txtbxTaxes"
        Me.txtbxTaxes.SelectedText = ""
        Me.txtbxTaxes.SelectionLength = 0
        Me.txtbxTaxes.SelectionStart = 0
        Me.txtbxTaxes.Size = New System.Drawing.Size(586, 36)
        Me.txtbxTaxes.TabIndex = 20
        Me.txtbxTaxes.WordWrap = True
        Me.txtbxTaxes.Child = New System.Windows.Controls.TextBox()
        '
        'tlpPosition
        '
        Me.tlpPosition.ColumnCount = 3
        Me.tlpPosition.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpPosition.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpPosition.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpPosition.Controls.Add(Me.lblDebt, 0, 1)
        Me.tlpPosition.Controls.Add(Me.ckbxAssetLiabilities, 0, 0)
        Me.tlpPosition.Controls.Add(Me.txtbxDebt, 1, 1)
        Me.tlpPosition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpPosition.Location = New System.Drawing.Point(6, 19)
        Me.tlpPosition.Name = "tlpPosition"
        Me.tlpPosition.Padding = New System.Windows.Forms.Padding(3)
        Me.tlpPosition.RowCount = 2
        Me.tlpPosition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpPosition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpPosition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpPosition.Size = New System.Drawing.Size(898, 74)
        Me.tlpPosition.TabIndex = 0
        '
        'lblDebt
        '
        Me.lblDebt.AutoSize = True
        Me.lblDebt.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblDebt.Location = New System.Drawing.Point(37, 28)
        Me.lblDebt.Name = "lblDebt"
        Me.lblDebt.Size = New System.Drawing.Size(263, 43)
        Me.lblDebt.TabIndex = 0
        Me.lblDebt.Text = "Comment on Business Debt to others and on Assets to Liabilities"
        Me.lblDebt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ckbxAssetLiabilities
        '
        Me.ckbxAssetLiabilities.AutoSize = True
        Me.ckbxAssetLiabilities.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyAssetRatio", True))
        Me.ckbxAssetLiabilities.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxAssetLiabilities.Location = New System.Drawing.Point(97, 6)
        Me.ckbxAssetLiabilities.Name = "ckbxAssetLiabilities"
        Me.ckbxAssetLiabilities.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxAssetLiabilities.Size = New System.Drawing.Size(203, 19)
        Me.ckbxAssetLiabilities.TabIndex = 13
        Me.ckbxAssetLiabilities.Text = "Assets to Liabilities ratio is satisfactory"
        Me.ckbxAssetLiabilities.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxAssetLiabilities.UseVisualStyleBackColor = True
        '
        'txtbxDebt
        '
        Me.tlpPosition.SetColumnSpan(Me.txtbxDebt, 2)
        Me.txtbxDebt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyDebtNotes", True))
        Me.txtbxDebt.IsReadOnly = False
        Me.txtbxDebt.Location = New System.Drawing.Point(306, 31)
        Me.txtbxDebt.MaxLength = 1024
        Me.txtbxDebt.MultiLine = True
        Me.txtbxDebt.Name = "txtbxDebt"
        Me.txtbxDebt.SelectedText = ""
        Me.txtbxDebt.SelectionLength = 0
        Me.txtbxDebt.SelectionStart = 0
        Me.txtbxDebt.Size = New System.Drawing.Size(586, 36)
        Me.txtbxDebt.TabIndex = 14
        Me.txtbxDebt.WordWrap = True
        Me.txtbxDebt.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxEarnings
        '
        Me.gpbxEarnings.Controls.Add(Me.tlpEarnings)
        Me.gpbxEarnings.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxEarnings.Location = New System.Drawing.Point(12, 412)
        Me.gpbxEarnings.Name = "gpbxEarnings"
        Me.gpbxEarnings.Size = New System.Drawing.Size(910, 96)
        Me.gpbxEarnings.TabIndex = 2
        Me.gpbxEarnings.TabStop = False
        Me.gpbxEarnings.Text = "Earnings before interest and taxes"
        '
        'tlpEarnings
        '
        Me.tlpEarnings.ColumnCount = 3
        Me.tlpEarnings.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpEarnings.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpEarnings.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpEarnings.Controls.Add(Me.ckbxEarnings, 0, 1)
        Me.tlpEarnings.Controls.Add(Me.lblProfit, 0, 0)
        Me.tlpEarnings.Controls.Add(Me.txtbxEarnings, 1, 0)
        Me.tlpEarnings.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpEarnings.Location = New System.Drawing.Point(6, 19)
        Me.tlpEarnings.Name = "tlpEarnings"
        Me.tlpEarnings.Padding = New System.Windows.Forms.Padding(3)
        Me.tlpEarnings.RowCount = 2
        Me.tlpEarnings.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpEarnings.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpEarnings.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpEarnings.Size = New System.Drawing.Size(898, 71)
        Me.tlpEarnings.TabIndex = 0
        '
        'ckbxEarnings
        '
        Me.ckbxEarnings.AutoSize = True
        Me.ckbxEarnings.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanyProfitSatis", True))
        Me.ckbxEarnings.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxEarnings.Location = New System.Drawing.Point(184, 48)
        Me.ckbxEarnings.Name = "ckbxEarnings"
        Me.ckbxEarnings.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxEarnings.Size = New System.Drawing.Size(116, 19)
        Me.ckbxEarnings.TabIndex = 12
        Me.ckbxEarnings.Text = "Profit is satisfactory"
        Me.ckbxEarnings.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxEarnings.UseVisualStyleBackColor = True
        '
        'lblProfit
        '
        Me.lblProfit.AutoSize = True
        Me.lblProfit.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblProfit.Location = New System.Drawing.Point(40, 3)
        Me.lblProfit.Name = "lblProfit"
        Me.lblProfit.Size = New System.Drawing.Size(260, 42)
        Me.lblProfit.TabIndex = 0
        Me.lblProfit.Text = "Comment on the Business's performance and ability to service YFL loan."
        Me.lblProfit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtbxEarnings
        '
        Me.tlpEarnings.SetColumnSpan(Me.txtbxEarnings, 2)
        Me.txtbxEarnings.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanyEarningsNotes", True))
        Me.txtbxEarnings.IsReadOnly = False
        Me.txtbxEarnings.Location = New System.Drawing.Point(306, 6)
        Me.txtbxEarnings.MaxLength = 1024
        Me.txtbxEarnings.MultiLine = True
        Me.txtbxEarnings.Name = "txtbxEarnings"
        Me.txtbxEarnings.SelectedText = ""
        Me.txtbxEarnings.SelectionLength = 0
        Me.txtbxEarnings.SelectionStart = 0
        Me.txtbxEarnings.Size = New System.Drawing.Size(586, 36)
        Me.txtbxEarnings.TabIndex = 10
        Me.txtbxEarnings.WordWrap = True
        Me.txtbxEarnings.Child = New System.Windows.Controls.TextBox()
        '
        'gpbxSalaries
        '
        Me.gpbxSalaries.Controls.Add(Me.tlpSalaries)
        Me.gpbxSalaries.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxSalaries.Location = New System.Drawing.Point(12, 310)
        Me.gpbxSalaries.Name = "gpbxSalaries"
        Me.gpbxSalaries.Size = New System.Drawing.Size(910, 96)
        Me.gpbxSalaries.TabIndex = 1
        Me.gpbxSalaries.TabStop = False
        Me.gpbxSalaries.Text = "Salaries"
        '
        'tlpSalaries
        '
        Me.tlpSalaries.ColumnCount = 3
        Me.tlpSalaries.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300.0!))
        Me.tlpSalaries.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSalaries.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSalaries.Controls.Add(Me.ckbxSalaries, 0, 1)
        Me.tlpSalaries.Controls.Add(Me.Label4, 0, 0)
        Me.tlpSalaries.Controls.Add(Me.txtbxSalaries, 1, 0)
        Me.tlpSalaries.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlpSalaries.Location = New System.Drawing.Point(6, 19)
        Me.tlpSalaries.Name = "tlpSalaries"
        Me.tlpSalaries.Padding = New System.Windows.Forms.Padding(3)
        Me.tlpSalaries.RowCount = 2
        Me.tlpSalaries.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.tlpSalaries.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlpSalaries.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpSalaries.Size = New System.Drawing.Size(898, 72)
        Me.tlpSalaries.TabIndex = 0
        '
        'ckbxSalaries
        '
        Me.ckbxSalaries.AutoSize = True
        Me.ckbxSalaries.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.EnquiryBindingSource, "CompanySalariesSatis", True))
        Me.ckbxSalaries.Dock = System.Windows.Forms.DockStyle.Right
        Me.ckbxSalaries.Location = New System.Drawing.Point(163, 48)
        Me.ckbxSalaries.Name = "ckbxSalaries"
        Me.ckbxSalaries.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbxSalaries.Size = New System.Drawing.Size(137, 19)
        Me.ckbxSalaries.TabIndex = 11
        Me.ckbxSalaries.Text = "Salaries are satisfactory"
        Me.ckbxSalaries.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbxSalaries.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label4.Location = New System.Drawing.Point(6, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(294, 42)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Comment on Salaries to Directors, Shareholders and Owners, these are satisfactory" & _
    " because ..."
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtbxSalaries
        '
        Me.tlpSalaries.SetColumnSpan(Me.txtbxSalaries, 2)
        Me.txtbxSalaries.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CompanySalariesNotes", True))
        Me.txtbxSalaries.IsReadOnly = False
        Me.txtbxSalaries.Location = New System.Drawing.Point(306, 6)
        Me.txtbxSalaries.MaxLength = 1024
        Me.txtbxSalaries.MultiLine = True
        Me.txtbxSalaries.Name = "txtbxSalaries"
        Me.txtbxSalaries.SelectedText = ""
        Me.txtbxSalaries.SelectionLength = 0
        Me.txtbxSalaries.SelectionStart = 0
        Me.txtbxSalaries.Size = New System.Drawing.Size(586, 36)
        Me.txtbxSalaries.TabIndex = 9
        Me.txtbxSalaries.WordWrap = True
        Me.txtbxSalaries.Child = New System.Windows.Controls.TextBox()
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter1
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(639, 39)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmt.TabIndex = 270
        Me.lblCurrentLoanAmt.Text = "0.00"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'lblCurrentLoan
        '
        Me.lblCurrentLoan.AutoSize = True
        Me.lblCurrentLoan.Location = New System.Drawing.Point(567, 39)
        Me.lblCurrentLoan.Name = "lblCurrentLoan"
        Me.lblCurrentLoan.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrentLoan.TabIndex = 269
        Me.lblCurrentLoan.Text = "Current Loan"
        Me.lblCurrentLoan.Visible = False
        '
        'CompanyFinancialForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 1012)
        Me.Controls.Add(Me.lblCurrentLoanAmt)
        Me.Controls.Add(Me.lblCurrentLoan)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblContactAddress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.lblContactName)
        Me.Controls.Add(Me.lblDealerName)
        Me.Controls.Add(Me.lblContactNumber)
        Me.Controls.Add(Me.lblContact)
        Me.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.Controls.Add(Me.lblLoanAmount)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblDateAndTimeValue)
        Me.Controls.Add(Me.lblDateAndTime)
        Me.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Controls.Add(Me.lblEnquiryCode)
        Me.Controls.Add(Me.gpbxManager)
        Me.Controls.Add(Me.gpbxStatus)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "CompanyFinancialForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Due Diligence Company Financial"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.gpbxIncomeExpenses.ResumeLayout(False)
        Me.tlpIncomeExpenses.ResumeLayout(False)
        Me.tlpIncomeExpenses.PerformLayout()
        Me.gpbxPosition.ResumeLayout(False)
        Me.tlpPosition2.ResumeLayout(False)
        Me.tlpPosition2.PerformLayout()
        Me.tlpPosition.ResumeLayout(False)
        Me.tlpPosition.PerformLayout()
        Me.gpbxEarnings.ResumeLayout(False)
        Me.tlpEarnings.ResumeLayout(False)
        Me.tlpEarnings.PerformLayout()
        Me.gpbxSalaries.ResumeLayout(False)
        Me.tlpSalaries.ResumeLayout(False)
        Me.tlpSalaries.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeclineToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WithdrawToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblContactAddress As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblContactName As System.Windows.Forms.Label
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblContactNumber As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents btnQRGList As System.Windows.Forms.Button
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents gpbxSalaries As System.Windows.Forms.GroupBox
    Friend WithEvents tlpSalaries As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gpbxEarnings As System.Windows.Forms.GroupBox
    Friend WithEvents tlpEarnings As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblProfit As System.Windows.Forms.Label
    Friend WithEvents gpbxPosition As System.Windows.Forms.GroupBox
    Friend WithEvents tlpPosition As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblDebt As System.Windows.Forms.Label
    Friend WithEvents ckbxAssetLiabilities As System.Windows.Forms.CheckBox
    Friend WithEvents gpbxIncomeExpenses As System.Windows.Forms.GroupBox
    Friend WithEvents tlpIncomeExpenses As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ckbxForcast As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxGST As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxYTD As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxPreviousYear As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ckbxIncomeExpenses As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ckbxSalaries As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxEarnings As System.Windows.Forms.CheckBox
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoan As System.Windows.Forms.Label
    Friend WithEvents tlpPosition2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ckbxAccountsPayable As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ckbxGSTreturns As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxIncomeTax As System.Windows.Forms.CheckBox
    Friend WithEvents ckbxPAYEreturns As System.Windows.Forms.CheckBox
    Friend WithEvents lblTaxes As System.Windows.Forms.Label
    Friend WithEvents txtbxIncomeNotes As AppWhShtB.SpellBox
    Friend WithEvents txtbxExpenseNotes As AppWhShtB.SpellBox
    Friend WithEvents txtbxSalaries As AppWhShtB.SpellBox
    Friend WithEvents txtbxDebt As AppWhShtB.SpellBox
    Friend WithEvents txtbxEarnings As AppWhShtB.SpellBox
    Friend WithEvents txtbxAccountsPayable As AppWhShtB.SpellBox
    Friend WithEvents txtbxTaxes As AppWhShtB.SpellBox
End Class
