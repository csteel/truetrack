﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WkShtWizForm2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WkShtWizForm2))
        Me.SecurityDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SecurityValued = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.SecurityInsured = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Comments = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InsuranceComments = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblSecurityComments = New System.Windows.Forms.Label()
        Me.lblEquityValue = New System.Windows.Forms.Label()
        Me.lblSecurityLoanAmountValue = New System.Windows.Forms.Label()
        Me.lblTotalLendValue = New System.Windows.Forms.Label()
        Me.btnDeleteSecurity = New System.Windows.Forms.Button()
        Me.lblXcessEquity = New System.Windows.Forms.Label()
        Me.lblLessLoanAmt = New System.Windows.Forms.Label()
        Me.lblTotLendValue = New System.Windows.Forms.Label()
        Me.btnAddSecurity = New System.Windows.Forms.Button()
        Me.gpbxListOfSecurities = New System.Windows.Forms.GroupBox()
        Me.PanelList = New System.Windows.Forms.Panel()
        Me.SecurityBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnQRGList = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblContactAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblContactName = New System.Windows.Forms.Label()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblContactNumber = New System.Windows.Forms.Label()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.PanelMiddle = New System.Windows.Forms.Panel()
        Me.lblCurrentLoanAmtCalc = New System.Windows.Forms.Label()
        Me.lblCurntLoan = New System.Windows.Forms.Label()
        Me.lblTotalAssetValue = New System.Windows.Forms.Label()
        Me.lblTotalAsset = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeclineToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WithdrawToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DealersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.lblCurrentLoan = New System.Windows.Forms.Label()
        Me.SecurityTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtbxSecurityComments = New AppWhShtB.SpellBox()
        CType(Me.SecurityDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxListOfSecurities.SuspendLayout()
        Me.PanelList.SuspendLayout()
        CType(Me.SecurityBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SecurityBindingNavigator.SuspendLayout()
        Me.StatusStripMessage.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gpbxStatus.SuspendLayout()
        Me.PanelMiddle.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.gpbxManager.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SecurityDataGridView
        '
        Me.SecurityDataGridView.AllowUserToAddRows = False
        Me.SecurityDataGridView.AllowUserToDeleteRows = False
        Me.SecurityDataGridView.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Menu
        Me.SecurityDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.SecurityDataGridView.AutoGenerateColumns = False
        Me.SecurityDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SecurityDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SecurityDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn4, Me.SecurityValued, Me.SecurityInsured, Me.Comments, Me.InsuranceComments, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn5})
        Me.SecurityDataGridView.DataSource = Me.SecurityBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SecurityDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.SecurityDataGridView.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SecurityDataGridView.EnableHeadersVisualStyles = False
        Me.SecurityDataGridView.Location = New System.Drawing.Point(0, 28)
        Me.SecurityDataGridView.MultiSelect = False
        Me.SecurityDataGridView.Name = "SecurityDataGridView"
        Me.SecurityDataGridView.ReadOnly = True
        Me.SecurityDataGridView.RowHeadersWidth = 10
        Me.SecurityDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SecurityDataGridView.Size = New System.Drawing.Size(914, 271)
        Me.SecurityDataGridView.TabIndex = 20
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "SecurityId"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Id"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 35
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.HeaderText = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 50
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "LendingValue"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DataGridViewTextBoxColumn12.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn12.HeaderText = "LendingValue"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 75
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 250
        '
        'SecurityValued
        '
        Me.SecurityValued.DataPropertyName = "SecurityValued"
        Me.SecurityValued.HeaderText = "Valued"
        Me.SecurityValued.Name = "SecurityValued"
        Me.SecurityValued.ReadOnly = True
        Me.SecurityValued.Width = 50
        '
        'SecurityInsured
        '
        Me.SecurityInsured.DataPropertyName = "SecurityInsured"
        Me.SecurityInsured.HeaderText = "Insured"
        Me.SecurityInsured.Name = "SecurityInsured"
        Me.SecurityInsured.ReadOnly = True
        Me.SecurityInsured.Width = 50
        '
        'Comments
        '
        Me.Comments.DataPropertyName = "Comments"
        Me.Comments.HeaderText = "Comments"
        Me.Comments.Name = "Comments"
        Me.Comments.ReadOnly = True
        Me.Comments.Width = 200
        '
        'InsuranceComments
        '
        Me.InsuranceComments.DataPropertyName = "InsuranceComments"
        Me.InsuranceComments.HeaderText = "InsuranceComments"
        Me.InsuranceComments.Name = "InsuranceComments"
        Me.InsuranceComments.ReadOnly = True
        Me.InsuranceComments.Width = 200
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "TodayValue"
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn9.HeaderText = "TodayValue"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "TodayValuer"
        Me.DataGridViewTextBoxColumn10.HeaderText = "TodayValuer"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "LendingPercentage"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Lending%"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 75
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "LastValued"
        Me.DataGridViewTextBoxColumn5.HeaderText = "LastValued"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "Security"
        Me.SecurityBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnFinish
        '
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(617, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.TabIndex = 17
        Me.btnFinish.Text = "Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(536, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 16
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(455, 3)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 15
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(374, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblSecurityComments
        '
        Me.lblSecurityComments.AutoSize = True
        Me.lblSecurityComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSecurityComments.Location = New System.Drawing.Point(3, 48)
        Me.lblSecurityComments.Name = "lblSecurityComments"
        Me.lblSecurityComments.Size = New System.Drawing.Size(234, 13)
        Me.lblSecurityComments.TabIndex = 21
        Me.lblSecurityComments.Text = "Other Insurance and Security Comments"
        '
        'lblEquityValue
        '
        Me.lblEquityValue.AutoSize = True
        Me.lblEquityValue.Location = New System.Drawing.Point(626, 6)
        Me.lblEquityValue.Name = "lblEquityValue"
        Me.lblEquityValue.Size = New System.Drawing.Size(28, 13)
        Me.lblEquityValue.TabIndex = 31
        Me.lblEquityValue.Text = "0.00"
        '
        'lblSecurityLoanAmountValue
        '
        Me.lblSecurityLoanAmountValue.AutoSize = True
        Me.lblSecurityLoanAmountValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblSecurityLoanAmountValue.Location = New System.Drawing.Point(406, 6)
        Me.lblSecurityLoanAmountValue.Name = "lblSecurityLoanAmountValue"
        Me.lblSecurityLoanAmountValue.Size = New System.Drawing.Size(28, 13)
        Me.lblSecurityLoanAmountValue.TabIndex = 30
        Me.lblSecurityLoanAmountValue.Text = "0.00"
        '
        'lblTotalLendValue
        '
        Me.lblTotalLendValue.AutoSize = True
        Me.lblTotalLendValue.Location = New System.Drawing.Point(108, 21)
        Me.lblTotalLendValue.Name = "lblTotalLendValue"
        Me.lblTotalLendValue.Size = New System.Drawing.Size(28, 13)
        Me.lblTotalLendValue.TabIndex = 29
        Me.lblTotalLendValue.Text = "0.00"
        '
        'btnDeleteSecurity
        '
        Me.btnDeleteSecurity.BackColor = System.Drawing.Color.MistyRose
        Me.btnDeleteSecurity.Location = New System.Drawing.Point(294, 37)
        Me.btnDeleteSecurity.Name = "btnDeleteSecurity"
        Me.btnDeleteSecurity.Size = New System.Drawing.Size(103, 23)
        Me.btnDeleteSecurity.TabIndex = 28
        Me.btnDeleteSecurity.TabStop = False
        Me.btnDeleteSecurity.Text = "Delete Security"
        Me.btnDeleteSecurity.UseVisualStyleBackColor = False
        '
        'lblXcessEquity
        '
        Me.lblXcessEquity.AutoSize = True
        Me.lblXcessEquity.Location = New System.Drawing.Point(546, 6)
        Me.lblXcessEquity.Name = "lblXcessEquity"
        Me.lblXcessEquity.Size = New System.Drawing.Size(77, 13)
        Me.lblXcessEquity.TabIndex = 26
        Me.lblXcessEquity.Text = "Lending Equity"
        '
        'lblLessLoanAmt
        '
        Me.lblLessLoanAmt.AutoSize = True
        Me.lblLessLoanAmt.Location = New System.Drawing.Point(268, 6)
        Me.lblLessLoanAmt.Name = "lblLessLoanAmt"
        Me.lblLessLoanAmt.Size = New System.Drawing.Size(95, 13)
        Me.lblLessLoanAmt.TabIndex = 25
        Me.lblLessLoanAmt.Text = "Less Loan Amount"
        '
        'lblTotLendValue
        '
        Me.lblTotLendValue.AutoSize = True
        Me.lblTotLendValue.Location = New System.Drawing.Point(3, 21)
        Me.lblTotLendValue.Name = "lblTotLendValue"
        Me.lblTotLendValue.Size = New System.Drawing.Size(102, 13)
        Me.lblTotLendValue.TabIndex = 24
        Me.lblTotLendValue.Text = "Total Lending Value"
        '
        'btnAddSecurity
        '
        Me.btnAddSecurity.BackColor = System.Drawing.Color.LightGreen
        Me.btnAddSecurity.Location = New System.Drawing.Point(549, 37)
        Me.btnAddSecurity.Name = "btnAddSecurity"
        Me.btnAddSecurity.Size = New System.Drawing.Size(95, 23)
        Me.btnAddSecurity.TabIndex = 5
        Me.btnAddSecurity.Text = "Add a Security"
        Me.btnAddSecurity.UseVisualStyleBackColor = False
        '
        'gpbxListOfSecurities
        '
        Me.gpbxListOfSecurities.Controls.Add(Me.PanelList)
        Me.gpbxListOfSecurities.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxListOfSecurities.Location = New System.Drawing.Point(12, 106)
        Me.gpbxListOfSecurities.Name = "gpbxListOfSecurities"
        Me.gpbxListOfSecurities.Size = New System.Drawing.Size(920, 318)
        Me.gpbxListOfSecurities.TabIndex = 20
        Me.gpbxListOfSecurities.TabStop = False
        Me.gpbxListOfSecurities.Text = "List of Securities"
        '
        'PanelList
        '
        Me.PanelList.Controls.Add(Me.SecurityBindingNavigator)
        Me.PanelList.Controls.Add(Me.SecurityDataGridView)
        Me.PanelList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelList.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelList.Location = New System.Drawing.Point(3, 16)
        Me.PanelList.Name = "PanelList"
        Me.PanelList.Size = New System.Drawing.Size(914, 299)
        Me.PanelList.TabIndex = 21
        '
        'SecurityBindingNavigator
        '
        Me.SecurityBindingNavigator.AddNewItem = Nothing
        Me.SecurityBindingNavigator.BindingSource = Me.SecurityBindingSource
        Me.SecurityBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SecurityBindingNavigator.DeleteItem = Nothing
        Me.SecurityBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SecurityBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SecurityBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SecurityBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SecurityBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SecurityBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SecurityBindingNavigator.Name = "SecurityBindingNavigator"
        Me.SecurityBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SecurityBindingNavigator.Size = New System.Drawing.Size(914, 25)
        Me.SecurityBindingNavigator.TabIndex = 1
        Me.SecurityBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 990)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStripMessage.Size = New System.Drawing.Size(944, 22)
        Me.StatusStripMessage.TabIndex = 36
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnViewDocs)
        Me.Panel1.Controls.Add(Me.btnQRGList)
        Me.Panel1.Controls.Add(Me.btnDocs)
        Me.Panel1.Controls.Add(Me.btnSaveAndExit)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnBack)
        Me.Panel1.Controls.Add(Me.btnNext)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 961)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 29)
        Me.Panel1.TabIndex = 10
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(34, 3)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 28
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnQRGList
        '
        Me.btnQRGList.Location = New System.Drawing.Point(106, 3)
        Me.btnQRGList.Name = "btnQRGList"
        Me.btnQRGList.Size = New System.Drawing.Size(75, 23)
        Me.btnQRGList.TabIndex = 27
        Me.btnQRGList.Text = "QRGuide"
        Me.btnQRGList.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(3, 3)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 26
        Me.ToolTip1.SetToolTip(Me.btnDocs, "You can 'Drag and Drop' documents from Windows Explorer or your Desktop.")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(275, 3)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 25
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(842, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 85
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContactAddress
        '
        Me.lblContactAddress.AutoSize = True
        Me.lblContactAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblContactAddress.Name = "lblContactAddress"
        Me.lblContactAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblContactAddress.TabIndex = 99
        Me.lblContactAddress.Text = "ClientAddress"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 98
        Me.lblAddress.Text = "Address:"
        '
        'lblContactName
        '
        Me.lblContactName.AutoSize = True
        Me.lblContactName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactName.Location = New System.Drawing.Point(15, 62)
        Me.lblContactName.Name = "lblContactName"
        Me.lblContactName.Size = New System.Drawing.Size(75, 13)
        Me.lblContactName.TabIndex = 97
        Me.lblContactName.Text = "Client Name"
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(59, 86)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(0, 13)
        Me.lblDealerName.TabIndex = 95
        '
        'lblContactNumber
        '
        Me.lblContactNumber.AutoSize = True
        Me.lblContactNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNumber.Location = New System.Drawing.Point(594, 62)
        Me.lblContactNumber.Name = "lblContactNumber"
        Me.lblContactNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblContactNumber.TabIndex = 94
        Me.lblContactNumber.Text = "Number"
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 93
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(299, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(190, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 92
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(492, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 91
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 86)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 90
        Me.lblDealer.Text = "Dealer:"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(187, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 89
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(155, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 88
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(92, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 87
        Me.lblEnquiryNumberValue.Text = "0000"
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 86
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'PanelMiddle
        '
        Me.PanelMiddle.Controls.Add(Me.lblCurrentLoanAmtCalc)
        Me.PanelMiddle.Controls.Add(Me.lblCurntLoan)
        Me.PanelMiddle.Controls.Add(Me.lblTotalAssetValue)
        Me.PanelMiddle.Controls.Add(Me.lblTotalAsset)
        Me.PanelMiddle.Controls.Add(Me.lblTotLendValue)
        Me.PanelMiddle.Controls.Add(Me.lblSecurityComments)
        Me.PanelMiddle.Controls.Add(Me.btnAddSecurity)
        Me.PanelMiddle.Controls.Add(Me.lblLessLoanAmt)
        Me.PanelMiddle.Controls.Add(Me.lblXcessEquity)
        Me.PanelMiddle.Controls.Add(Me.btnDeleteSecurity)
        Me.PanelMiddle.Controls.Add(Me.lblTotalLendValue)
        Me.PanelMiddle.Controls.Add(Me.lblSecurityLoanAmountValue)
        Me.PanelMiddle.Controls.Add(Me.lblEquityValue)
        Me.PanelMiddle.Location = New System.Drawing.Point(12, 423)
        Me.PanelMiddle.Name = "PanelMiddle"
        Me.PanelMiddle.Size = New System.Drawing.Size(920, 66)
        Me.PanelMiddle.TabIndex = 102
        '
        'lblCurrentLoanAmtCalc
        '
        Me.lblCurrentLoanAmtCalc.AutoSize = True
        Me.lblCurrentLoanAmtCalc.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmtCalc.Location = New System.Drawing.Point(406, 21)
        Me.lblCurrentLoanAmtCalc.Name = "lblCurrentLoanAmtCalc"
        Me.lblCurrentLoanAmtCalc.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmtCalc.TabIndex = 35
        Me.lblCurrentLoanAmtCalc.Text = "0.00"
        Me.lblCurrentLoanAmtCalc.Visible = False
        '
        'lblCurntLoan
        '
        Me.lblCurntLoan.AutoSize = True
        Me.lblCurntLoan.Location = New System.Drawing.Point(268, 21)
        Me.lblCurntLoan.Name = "lblCurntLoan"
        Me.lblCurntLoan.Size = New System.Drawing.Size(132, 13)
        Me.lblCurntLoan.TabIndex = 34
        Me.lblCurntLoan.Text = "Less Current Loan Amount"
        Me.lblCurntLoan.Visible = False
        '
        'lblTotalAssetValue
        '
        Me.lblTotalAssetValue.AutoSize = True
        Me.lblTotalAssetValue.Location = New System.Drawing.Point(108, 6)
        Me.lblTotalAssetValue.Name = "lblTotalAssetValue"
        Me.lblTotalAssetValue.Size = New System.Drawing.Size(28, 13)
        Me.lblTotalAssetValue.TabIndex = 33
        Me.lblTotalAssetValue.Text = "0.00"
        '
        'lblTotalAsset
        '
        Me.lblTotalAsset.AutoSize = True
        Me.lblTotalAsset.Location = New System.Drawing.Point(3, 6)
        Me.lblTotalAsset.Name = "lblTotalAsset"
        Me.lblTotalAsset.Size = New System.Drawing.Size(90, 13)
        Me.lblTotalAsset.TabIndex = 32
        Me.lblTotalAsset.Text = "Total Asset Value"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.WWWToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem, Me.MCToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 247
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Enabled = False
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEToolStripMenuItem
        '
        Me.EndEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeclineToolStripMenuItem, Me.WithdrawToolStripMenuItem})
        Me.EndEToolStripMenuItem.Name = "EndEToolStripMenuItem"
        Me.EndEToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEToolStripMenuItem.Text = "End Enquiry"
        Me.EndEToolStripMenuItem.Visible = False
        '
        'DeclineToolStripMenuItem
        '
        Me.DeclineToolStripMenuItem.Name = "DeclineToolStripMenuItem"
        Me.DeclineToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DeclineToolStripMenuItem.Text = "Declined"
        '
        'WithdrawToolStripMenuItem
        '
        Me.WithdrawToolStripMenuItem.Name = "WithdrawToolStripMenuItem"
        Me.WithdrawToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.WithdrawToolStripMenuItem.Text = "Withdrawn"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem, Me.DealersToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "Enquiries Names"
        '
        'DealersToolStripMenuItem
        '
        Me.DealersToolStripMenuItem.Name = "DealersToolStripMenuItem"
        Me.DealersToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.DealersToolStripMenuItem.Text = "Dealers"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1, Me.EmailApprovalToolStripMenuItem})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(154, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'EmailApprovalToolStripMenuItem
        '
        Me.EmailApprovalToolStripMenuItem.Name = "EmailApprovalToolStripMenuItem"
        Me.EmailApprovalToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EmailApprovalToolStripMenuItem.Text = "Email Approval"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "YFL Website"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Website - Ctrl+Alt+W"
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeyDisplayString = "CTRL+ALT+M"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search TrueTrack Names - Ctrl+Alt+M"
        '
        'MCToolStripMenuItem
        '
        Me.MCToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.MCToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.OAC16
        Me.MCToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MCToolStripMenuItem.Name = "MCToolStripMenuItem"
        Me.MCToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.MCToolStripMenuItem.Text = "MembersCentre Link"
        Me.MCToolStripMenuItem.ToolTipText = "OAC Link"
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(740, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 248
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(639, 39)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmt.TabIndex = 251
        Me.lblCurrentLoanAmt.Text = "0.00"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'lblCurrentLoan
        '
        Me.lblCurrentLoan.AutoSize = True
        Me.lblCurrentLoan.Location = New System.Drawing.Point(567, 39)
        Me.lblCurrentLoan.Name = "lblCurrentLoan"
        Me.lblCurrentLoan.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrentLoan.TabIndex = 250
        Me.lblCurrentLoan.Text = "Current Loan"
        Me.lblCurrentLoan.Visible = False
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'txtbxSecurityComments
        '
        Me.txtbxSecurityComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "SecurityComments", True))
        Me.txtbxSecurityComments.IsReadOnly = False
        Me.txtbxSecurityComments.Location = New System.Drawing.Point(15, 489)
        Me.txtbxSecurityComments.MaxLength = 0
        Me.txtbxSecurityComments.MultiLine = True
        Me.txtbxSecurityComments.Name = "txtbxSecurityComments"
        Me.txtbxSecurityComments.SelectedText = ""
        Me.txtbxSecurityComments.SelectionLength = 0
        Me.txtbxSecurityComments.SelectionStart = 0
        Me.txtbxSecurityComments.Size = New System.Drawing.Size(917, 466)
        Me.txtbxSecurityComments.TabIndex = 252
        Me.txtbxSecurityComments.WordWrap = True
        Me.txtbxSecurityComments.Child = New System.Windows.Controls.TextBox()
        '
        'WkShtWizForm2
        '
        Me.AcceptButton = Me.btnNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.PrelimFormSize
        Me.Controls.Add(Me.txtbxSecurityComments)
        Me.Controls.Add(Me.lblCurrentLoanAmt)
        Me.Controls.Add(Me.lblCurrentLoan)
        Me.Controls.Add(Me.gpbxManager)
        Me.Controls.Add(Me.gpbxStatus)
        Me.Controls.Add(Me.gpbxListOfSecurities)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.PanelMiddle)
        Me.Controls.Add(Me.lblContactAddress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.lblContactName)
        Me.Controls.Add(Me.lblDealerName)
        Me.Controls.Add(Me.lblContactNumber)
        Me.Controls.Add(Me.lblContact)
        Me.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.Controls.Add(Me.lblLoanAmount)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblDateAndTimeValue)
        Me.Controls.Add(Me.lblDateAndTime)
        Me.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Controls.Add(Me.lblEnquiryCode)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "PrelimFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "PrelimFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.PrelimFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "WkShtWizForm2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Due Diligence Security"
        CType(Me.SecurityDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxListOfSecurities.ResumeLayout(False)
        Me.PanelList.ResumeLayout(False)
        Me.PanelList.PerformLayout()
        CType(Me.SecurityBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SecurityBindingNavigator.ResumeLayout(False)
        Me.SecurityBindingNavigator.PerformLayout()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        Me.PanelMiddle.ResumeLayout(False)
        Me.PanelMiddle.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SecurityDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblSecurityComments As System.Windows.Forms.Label
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents lblEquityValue As System.Windows.Forms.Label
    Friend WithEvents lblSecurityLoanAmountValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalLendValue As System.Windows.Forms.Label
    Friend WithEvents btnDeleteSecurity As System.Windows.Forms.Button
    Friend WithEvents lblXcessEquity As System.Windows.Forms.Label
    Friend WithEvents lblLessLoanAmt As System.Windows.Forms.Label
    Friend WithEvents lblTotLendValue As System.Windows.Forms.Label
    Friend WithEvents btnAddSecurity As System.Windows.Forms.Button
    Friend WithEvents gpbxListOfSecurities As System.Windows.Forms.GroupBox
    Friend WithEvents SecurityBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents btnQRGList As System.Windows.Forms.Button
    Friend WithEvents lblContactAddress As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblContactName As System.Windows.Forms.Label
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblContactNumber As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents PanelMiddle As System.Windows.Forms.Panel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeclineToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WithdrawToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblTotalAsset As System.Windows.Forms.Label
    Friend WithEvents lblTotalAssetValue As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoan As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoanAmtCalc As System.Windows.Forms.Label
    Friend WithEvents lblCurntLoan As System.Windows.Forms.Label
    Friend WithEvents EmailApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DealersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtbxSecurityComments As AppWhShtB.SpellBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SecurityValued As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents SecurityInsured As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Comments As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InsuranceComments As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PanelList As System.Windows.Forms.Panel
End Class
