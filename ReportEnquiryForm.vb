﻿Imports Microsoft.Reporting.WinForms

Public Class ReportEnquiryForm
    Dim ThisStartdate As Date
    Dim ThisEndDate As Date
    Dim ThisGroupBy As String
    Dim ThisOrderBy As String
    Dim ThisArchiveStatus As String
    Dim ThisUser As String
    'user settings
    Dim NewLocX As Integer
    Dim NewLocY As Integer

    'Loads form with parameters from EnquiryReportSelectionsForm
    Friend Sub ReportSelections_PassVariable(ByVal Startdate As Date, ByVal EndDate As Date, ByVal GroupBy As String, ByVal OrderBy As String, ByVal User As String, ByVal ArchiveStatus As String)
        ThisStartdate = Startdate
        ThisEndDate = EndDate
        ThisGroupBy = GroupBy
        ThisOrderBy = OrderBy
        ThisUser = User
        'MsgBox("Group By: " & ThisGroupBy)
        'MsgBox("User: " & ThisUser)
        'MsgBox("SortBy: " & ThisOrderBy)
        'get user settings
        Me.ClientSize = New Size(My.Settings.ReportEnquiryFormSize)
        Me.Location = New Point(My.Settings.ReportEnquiryFormLocation)
        Select Case ArchiveStatus
            Case Is = "Include Archives"
                ReportViewer2.Visible = False
                ReportViewer1.Visible = True
                Try
                    'load tableAdapter
                    Me.CurrentAndArchiveEnquiryTableAdapter.FillByDates(Me.EnquiryWorkSheetDataSet.CurrentAndArchiveEnquiry, ThisStartdate, ThisEndDate)
                    'set report parameters
                    'Title
                    Dim p As New ReportParameter("parReportTitle", "Enquiry List")
                    'Group By
                    Dim q As New ReportParameter("parReportGroupBy", ThisGroupBy)
                    'OrderBy
                    Dim r As New ReportParameter("parReportOrderBy", ThisOrderBy)
                    'set LoggedinName
                    Dim t As New ReportParameter("parReportLoggedinName", LoggedinName)
                    'set parReportUser
                    Dim u As New ReportParameter("parReportUser", ThisUser)
                    'set parReportStartDate
                    Dim v As New ReportParameter("parReportStartDate", ThisStartdate)
                    'set parReportEnddate
                    Dim w As New ReportParameter("parReportEndDate", ThisEndDate)
                    ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p, q, r, t, u, v, w})

                    Me.ReportViewer1.RefreshReport()

                Catch e As System.Exception
                    Dim inner As Exception = e.InnerException
                    If Not (inner Is Nothing) Then
                        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + inner.Message)
                        inner = inner.InnerException
                    Else
                        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + e.Message)
                    End If
                End Try
            Case Is = "Current"
                ReportViewer2.Visible = True
                ReportViewer1.Visible = False
                Try
                    'load tableAdapter
                    Me.CurrentEnquiryReportTableAdapter.FillByDates(Me.EnquiryWorkSheetDataSet.CurrentEnquiryReport, ThisStartdate, ThisEndDate)
                    'set report parameters
                    'Title
                    Dim p As New ReportParameter("parReportTitle", "Enquiry List")
                    'Group By
                    Dim q As New ReportParameter("parReportGroupBy", ThisGroupBy)
                    'OrderBy
                    Dim r As New ReportParameter("parReportOrderBy", ThisOrderBy)
                    'set LoggedinName
                    Dim t As New ReportParameter("parReportLoggedinName", LoggedinName)
                    'set parReportUser
                    Dim u As New ReportParameter("parReportUser", ThisUser)
                    'set parReportStartDate
                    Dim v As New ReportParameter("parReportStartDate", ThisStartdate)
                    'set parReportEnddate
                    Dim w As New ReportParameter("parReportEndDate", ThisEndDate)
                    ReportViewer2.LocalReport.SetParameters(New ReportParameter() {p, q, r, t, u, v, w})

                    Me.ReportViewer2.RefreshReport()

                Catch e As System.Exception
                    Dim inner As Exception = e.InnerException
                    If Not (inner Is Nothing) Then
                        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + inner.Message)
                        inner = inner.InnerException
                    Else
                        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + e.Message)
                    End If
                End Try

            Case Else

        End Select

    End Sub

    Private Sub ReportEnquiryForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            NewLocX = 0
        Else
            NewLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            NewLocY = 0
        Else
            NewLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.ReportEnquiryFormLocation = New Point(NewLocX, NewLocY)
        My.Settings.ReportEnquiryFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
    End Sub

    
    
    
    Private Sub ReportEnquiryForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

    End Sub
End Class