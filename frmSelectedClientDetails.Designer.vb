﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectedClientDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gpbxMC = New System.Windows.Forms.GroupBox()
        Me.txtManager = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtClientId = New System.Windows.Forms.TextBox()
        Me.lblClientId = New System.Windows.Forms.Label()
        Me.lblPostal = New System.Windows.Forms.Label()
        Me.lblPhysical = New System.Windows.Forms.Label()
        Me.lblPostalSuburb = New System.Windows.Forms.Label()
        Me.txtPostalSuburb = New System.Windows.Forms.TextBox()
        Me.lblPostalPostcode = New System.Windows.Forms.Label()
        Me.txtPostalPostcode = New System.Windows.Forms.TextBox()
        Me.lblPostalCity = New System.Windows.Forms.Label()
        Me.txtPostalCity = New System.Windows.Forms.TextBox()
        Me.ibiPostalAddress = New System.Windows.Forms.Label()
        Me.txtPostalAddress = New System.Windows.Forms.TextBox()
        Me.dgvContactMethods = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSuburb = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblPostCode = New System.Windows.Forms.Label()
        Me.txtPostCode = New System.Windows.Forms.TextBox()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.txtCity = New System.Windows.Forms.TextBox()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.txtDOB = New System.Windows.Forms.TextBox()
        Me.lblDOB = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.gpbxMC.SuspendLayout()
        CType(Me.dgvContactMethods, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gpbxMC
        '
        Me.gpbxMC.BackColor = System.Drawing.Color.LightGray
        Me.gpbxMC.Controls.Add(Me.txtManager)
        Me.gpbxMC.Controls.Add(Me.Label3)
        Me.gpbxMC.Controls.Add(Me.txtClientId)
        Me.gpbxMC.Controls.Add(Me.lblClientId)
        Me.gpbxMC.Controls.Add(Me.lblPostal)
        Me.gpbxMC.Controls.Add(Me.lblPhysical)
        Me.gpbxMC.Controls.Add(Me.lblPostalSuburb)
        Me.gpbxMC.Controls.Add(Me.txtPostalSuburb)
        Me.gpbxMC.Controls.Add(Me.lblPostalPostcode)
        Me.gpbxMC.Controls.Add(Me.txtPostalPostcode)
        Me.gpbxMC.Controls.Add(Me.lblPostalCity)
        Me.gpbxMC.Controls.Add(Me.txtPostalCity)
        Me.gpbxMC.Controls.Add(Me.ibiPostalAddress)
        Me.gpbxMC.Controls.Add(Me.txtPostalAddress)
        Me.gpbxMC.Controls.Add(Me.dgvContactMethods)
        Me.gpbxMC.Controls.Add(Me.Label2)
        Me.gpbxMC.Controls.Add(Me.txtSuburb)
        Me.gpbxMC.Controls.Add(Me.Label1)
        Me.gpbxMC.Controls.Add(Me.lblPostCode)
        Me.gpbxMC.Controls.Add(Me.txtPostCode)
        Me.gpbxMC.Controls.Add(Me.lblCity)
        Me.gpbxMC.Controls.Add(Me.txtCity)
        Me.gpbxMC.Controls.Add(Me.lblAddress)
        Me.gpbxMC.Controls.Add(Me.txtFirstName)
        Me.gpbxMC.Controls.Add(Me.txtAddress)
        Me.gpbxMC.Controls.Add(Me.lblFirstName)
        Me.gpbxMC.Controls.Add(Me.txtLastName)
        Me.gpbxMC.Controls.Add(Me.lblLastName)
        Me.gpbxMC.Controls.Add(Me.txtDOB)
        Me.gpbxMC.Controls.Add(Me.lblDOB)
        Me.gpbxMC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbxMC.Location = New System.Drawing.Point(12, 7)
        Me.gpbxMC.Name = "gpbxMC"
        Me.gpbxMC.Size = New System.Drawing.Size(392, 546)
        Me.gpbxMC.TabIndex = 32
        Me.gpbxMC.TabStop = False
        Me.gpbxMC.Text = "Selected Details For Update"
        '
        'txtManager
        '
        Me.txtManager.BackColor = System.Drawing.SystemColors.Window
        Me.txtManager.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtManager.Location = New System.Drawing.Point(125, 127)
        Me.txtManager.Name = "txtManager"
        Me.txtManager.ReadOnly = True
        Me.txtManager.Size = New System.Drawing.Size(228, 20)
        Me.txtManager.TabIndex = 62
        Me.txtManager.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(34, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 63
        Me.Label3.Text = "Manager"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtClientId
        '
        Me.txtClientId.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClientId.Location = New System.Drawing.Point(125, 22)
        Me.txtClientId.Name = "txtClientId"
        Me.txtClientId.ReadOnly = True
        Me.txtClientId.Size = New System.Drawing.Size(228, 20)
        Me.txtClientId.TabIndex = 61
        Me.txtClientId.TabStop = False
        '
        'lblClientId
        '
        Me.lblClientId.AutoSize = True
        Me.lblClientId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientId.Location = New System.Drawing.Point(38, 25)
        Me.lblClientId.Name = "lblClientId"
        Me.lblClientId.Size = New System.Drawing.Size(45, 13)
        Me.lblClientId.TabIndex = 60
        Me.lblClientId.Text = "Client Id"
        Me.lblClientId.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPostal
        '
        Me.lblPostal.AutoSize = True
        Me.lblPostal.Location = New System.Drawing.Point(15, 238)
        Me.lblPostal.Name = "lblPostal"
        Me.lblPostal.Size = New System.Drawing.Size(91, 13)
        Me.lblPostal.TabIndex = 59
        Me.lblPostal.Text = "Postal Address"
        '
        'lblPhysical
        '
        Me.lblPhysical.AutoSize = True
        Me.lblPhysical.Location = New System.Drawing.Point(15, 166)
        Me.lblPhysical.Name = "lblPhysical"
        Me.lblPhysical.Size = New System.Drawing.Size(103, 13)
        Me.lblPhysical.TabIndex = 58
        Me.lblPhysical.Text = "Physical Address"
        '
        'lblPostalSuburb
        '
        Me.lblPostalSuburb.AutoSize = True
        Me.lblPostalSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalSuburb.Location = New System.Drawing.Point(12, 283)
        Me.lblPostalSuburb.Name = "lblPostalSuburb"
        Me.lblPostalSuburb.Size = New System.Drawing.Size(41, 13)
        Me.lblPostalSuburb.TabIndex = 57
        Me.lblPostalSuburb.Text = "Suburb"
        '
        'txtPostalSuburb
        '
        Me.txtPostalSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostalSuburb.Location = New System.Drawing.Point(63, 280)
        Me.txtPostalSuburb.Name = "txtPostalSuburb"
        Me.txtPostalSuburb.ReadOnly = True
        Me.txtPostalSuburb.Size = New System.Drawing.Size(80, 20)
        Me.txtPostalSuburb.TabIndex = 56
        Me.txtPostalSuburb.TabStop = False
        '
        'lblPostalPostcode
        '
        Me.lblPostalPostcode.AutoSize = True
        Me.lblPostalPostcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalPostcode.Location = New System.Drawing.Point(249, 283)
        Me.lblPostalPostcode.Name = "lblPostalPostcode"
        Me.lblPostalPostcode.Size = New System.Drawing.Size(53, 13)
        Me.lblPostalPostcode.TabIndex = 55
        Me.lblPostalPostcode.Text = "PostCode"
        '
        'txtPostalPostcode
        '
        Me.txtPostalPostcode.BackColor = System.Drawing.SystemColors.Window
        Me.txtPostalPostcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostalPostcode.Location = New System.Drawing.Point(304, 280)
        Me.txtPostalPostcode.MaxLength = 5
        Me.txtPostalPostcode.Name = "txtPostalPostcode"
        Me.txtPostalPostcode.ReadOnly = True
        Me.txtPostalPostcode.Size = New System.Drawing.Size(49, 20)
        Me.txtPostalPostcode.TabIndex = 54
        Me.txtPostalPostcode.TabStop = False
        '
        'lblPostalCity
        '
        Me.lblPostalCity.AutoSize = True
        Me.lblPostalCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalCity.Location = New System.Drawing.Point(143, 283)
        Me.lblPostalCity.Name = "lblPostalCity"
        Me.lblPostalCity.Size = New System.Drawing.Size(24, 13)
        Me.lblPostalCity.TabIndex = 53
        Me.lblPostalCity.Text = "City"
        '
        'txtPostalCity
        '
        Me.txtPostalCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtPostalCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostalCity.Location = New System.Drawing.Point(169, 280)
        Me.txtPostalCity.Name = "txtPostalCity"
        Me.txtPostalCity.ReadOnly = True
        Me.txtPostalCity.Size = New System.Drawing.Size(80, 20)
        Me.txtPostalCity.TabIndex = 52
        Me.txtPostalCity.TabStop = False
        '
        'ibiPostalAddress
        '
        Me.ibiPostalAddress.AutoSize = True
        Me.ibiPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ibiPostalAddress.Location = New System.Drawing.Point(12, 257)
        Me.ibiPostalAddress.Name = "ibiPostalAddress"
        Me.ibiPostalAddress.Size = New System.Drawing.Size(45, 13)
        Me.ibiPostalAddress.TabIndex = 51
        Me.ibiPostalAddress.Text = "Address"
        '
        'txtPostalAddress
        '
        Me.txtPostalAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtPostalAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostalAddress.Location = New System.Drawing.Point(63, 254)
        Me.txtPostalAddress.MaxLength = 100
        Me.txtPostalAddress.Name = "txtPostalAddress"
        Me.txtPostalAddress.ReadOnly = True
        Me.txtPostalAddress.Size = New System.Drawing.Size(290, 20)
        Me.txtPostalAddress.TabIndex = 50
        Me.txtPostalAddress.TabStop = False
        '
        'dgvContactMethods
        '
        Me.dgvContactMethods.AllowUserToAddRows = False
        Me.dgvContactMethods.AllowUserToDeleteRows = False
        Me.dgvContactMethods.AllowUserToOrderColumns = True
        Me.dgvContactMethods.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvContactMethods.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvContactMethods.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvContactMethods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContactMethods.ColumnHeadersVisible = False
        Me.dgvContactMethods.Location = New System.Drawing.Point(18, 336)
        Me.dgvContactMethods.MultiSelect = False
        Me.dgvContactMethods.Name = "dgvContactMethods"
        Me.dgvContactMethods.RowHeadersVisible = False
        Me.dgvContactMethods.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        Me.dgvContactMethods.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvContactMethods.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvContactMethods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContactMethods.Size = New System.Drawing.Size(335, 195)
        Me.dgvContactMethods.TabIndex = 46
        Me.dgvContactMethods.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 211)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "Suburb"
        '
        'txtSuburb
        '
        Me.txtSuburb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSuburb.Location = New System.Drawing.Point(63, 208)
        Me.txtSuburb.Name = "txtSuburb"
        Me.txtSuburb.ReadOnly = True
        Me.txtSuburb.Size = New System.Drawing.Size(80, 20)
        Me.txtSuburb.TabIndex = 48
        Me.txtSuburb.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 318)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Contact Methods"
        '
        'lblPostCode
        '
        Me.lblPostCode.AutoSize = True
        Me.lblPostCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCode.Location = New System.Drawing.Point(249, 211)
        Me.lblPostCode.Name = "lblPostCode"
        Me.lblPostCode.Size = New System.Drawing.Size(53, 13)
        Me.lblPostCode.TabIndex = 15
        Me.lblPostCode.Text = "PostCode"
        '
        'txtPostCode
        '
        Me.txtPostCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtPostCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostCode.Location = New System.Drawing.Point(304, 208)
        Me.txtPostCode.MaxLength = 5
        Me.txtPostCode.Name = "txtPostCode"
        Me.txtPostCode.ReadOnly = True
        Me.txtPostCode.Size = New System.Drawing.Size(49, 20)
        Me.txtPostCode.TabIndex = 14
        Me.txtPostCode.TabStop = False
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(143, 211)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(24, 13)
        Me.lblCity.TabIndex = 13
        Me.lblCity.Text = "City"
        '
        'txtCity
        '
        Me.txtCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCity.Location = New System.Drawing.Point(169, 208)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.ReadOnly = True
        Me.txtCity.Size = New System.Drawing.Size(80, 20)
        Me.txtCity.TabIndex = 12
        Me.txtCity.TabStop = False
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(12, 185)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(45, 13)
        Me.lblAddress.TabIndex = 11
        Me.lblAddress.Text = "Address"
        '
        'txtFirstName
        '
        Me.txtFirstName.BackColor = System.Drawing.SystemColors.Window
        Me.txtFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.Location = New System.Drawing.Point(125, 47)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.ReadOnly = True
        Me.txtFirstName.Size = New System.Drawing.Size(228, 20)
        Me.txtFirstName.TabIndex = 0
        Me.txtFirstName.TabStop = False
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(63, 182)
        Me.txtAddress.MaxLength = 100
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ReadOnly = True
        Me.txtAddress.Size = New System.Drawing.Size(290, 20)
        Me.txtAddress.TabIndex = 10
        Me.txtAddress.TabStop = False
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(26, 50)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(57, 13)
        Me.lblFirstName.TabIndex = 2
        Me.lblFirstName.Text = "First Name"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLastName
        '
        Me.txtLastName.BackColor = System.Drawing.SystemColors.Window
        Me.txtLastName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.Location = New System.Drawing.Point(125, 73)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.ReadOnly = True
        Me.txtLastName.Size = New System.Drawing.Size(228, 20)
        Me.txtLastName.TabIndex = 3
        Me.txtLastName.TabStop = False
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(25, 76)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 13)
        Me.lblLastName.TabIndex = 4
        Me.lblLastName.Text = "Last Name"
        Me.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDOB
        '
        Me.txtDOB.BackColor = System.Drawing.SystemColors.Window
        Me.txtDOB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDOB.Location = New System.Drawing.Point(125, 99)
        Me.txtDOB.Name = "txtDOB"
        Me.txtDOB.ReadOnly = True
        Me.txtDOB.Size = New System.Drawing.Size(228, 20)
        Me.txtDOB.TabIndex = 5
        Me.txtDOB.TabStop = False
        '
        'lblDOB
        '
        Me.lblDOB.AutoSize = True
        Me.lblDOB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDOB.Location = New System.Drawing.Point(53, 102)
        Me.lblDOB.Name = "lblDOB"
        Me.lblDOB.Size = New System.Drawing.Size(30, 13)
        Me.lblDOB.TabIndex = 6
        Me.lblDOB.Text = "DOB"
        Me.lblDOB.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(139, 577)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(123, 28)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmSelectedClientDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(415, 617)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gpbxMC)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSelectedClientDetails"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Selected Client Details"
        Me.gpbxMC.ResumeLayout(False)
        Me.gpbxMC.PerformLayout()
        CType(Me.dgvContactMethods, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gpbxMC As System.Windows.Forms.GroupBox
    Friend WithEvents lblPostal As System.Windows.Forms.Label
    Friend WithEvents lblPhysical As System.Windows.Forms.Label
    Friend WithEvents lblPostalSuburb As System.Windows.Forms.Label
    Friend WithEvents txtPostalSuburb As System.Windows.Forms.TextBox
    Friend WithEvents lblPostalPostcode As System.Windows.Forms.Label
    Friend WithEvents txtPostalPostcode As System.Windows.Forms.TextBox
    Friend WithEvents lblPostalCity As System.Windows.Forms.Label
    Friend WithEvents txtPostalCity As System.Windows.Forms.TextBox
    Friend WithEvents ibiPostalAddress As System.Windows.Forms.Label
    Friend WithEvents txtPostalAddress As System.Windows.Forms.TextBox
    Friend WithEvents dgvContactMethods As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSuburb As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblPostCode As System.Windows.Forms.Label
    Friend WithEvents txtPostCode As System.Windows.Forms.TextBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtCity As System.Windows.Forms.TextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents txtDOB As System.Windows.Forms.TextBox
    Friend WithEvents lblDOB As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtClientId As System.Windows.Forms.TextBox
    Friend WithEvents lblClientId As System.Windows.Forms.Label
    Friend WithEvents txtManager As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
