﻿Imports System.Data.SqlClient
Imports System.IO

Public Class WkShtWizForm4 'Budget Due Diligence

    Dim thisEnquiryId As Integer ' set New Enquiry Id
    Dim thisEnquiryCode As String ' set Enquiry Code
    Dim thisEnquiryManagerId As Integer 'set EnquiryManagerId
    Dim thisTypeOfMainCustomer As Integer ' set Type of Main Client
    'Dim ThisLoanReason As String
    Dim thisEnquiryType As Integer
    Dim thisWizardStatus As Integer ' set New Wizard Status
    Dim thisContactDisplayName As String
    Dim contactName As String
    Dim contactLastName As String
    Dim contactAddress As String
    Dim thisSingleDepend As Decimal 'set DependSingle 
    Dim thisCoupleDepend As Decimal 'set DependCouple
    Dim thisDependChild As Decimal 'set DependChild
    Dim clientDependency As Decimal 'set Client dependency
    'Dim appDetect As Boolean 'Application values (ValuesId = 1) switch
    Dim budgetDependTotal As Decimal 'set Budget Dependency
    Dim incomeTotal As Decimal = 0 'set Total Income
    Dim expenseTotal As Decimal = 0 'set Total Expenses
    Dim thisCurrentStatus As String
    Dim qrgStatus As Integer 'set QRG Status for colour
    'Dim ThisTypeofClient As String
    'Dim ClientName As String 'Cannotate client name
    'Dim ClientLastName As String
    'Dim ClientAddress As String
    'Dim JointName As String 'Cannotated Joint Name
    Dim thisDealerId As String
    Dim thisDealerName As String
    Dim overtimeSelectedrb As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim overtimeState As Boolean 'track state of Overtime Question
    Dim thisEnquiryResult As String = ""
    Dim progressStatus As Boolean = False
    Dim formTitle As String = "Due-diligence Budget" 'Me.Text
    'user settings
    Dim newLocX As Integer
    Dim newLocY As Integer
    'store form load information
    Public frmLoadInfo As System.Text.StringBuilder = New System.Text.StringBuilder
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Loads form with parameters from WkShtWizForm2
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)
        'Display step
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Loading new form."
        Application.DoEvents()
        SuspendLayout()
        'Remove Handlers
        RemoveHandler dtpPSDate.ValueChanged, AddressOf dtpPSDate_ValueChanged
        RemoveHandler dtpPEDate.ValueChanged, AddressOf dtpPEDate_ValueChanged
        RemoveHandler ckbxQuestion1No.CheckedChanged, AddressOf ckbxQuestion1No_CheckedChanged
        RemoveHandler ckbxQuestion2No.CheckedChanged, AddressOf ckbxQuestion2No_CheckedChanged
        RemoveHandler ckbxQuestion3No.CheckedChanged, AddressOf ckbxQuestion3No_CheckedChanged
        RemoveHandler ckbxQuestion4No.CheckedChanged, AddressOf ckbxQuestion4No_CheckedChanged
        RemoveHandler ckbxQuestion5No.CheckedChanged, AddressOf ckbxQuestion5No_CheckedChanged
        RemoveHandler ckbxQuestion6No.CheckedChanged, AddressOf ckbxQuestion6No_CheckedChanged
        RemoveHandler ckbxQuestion7No.CheckedChanged, AddressOf ckbxQuestion7No_CheckedChanged
        RemoveHandler ckbxQuestion8No.CheckedChanged, AddressOf ckbxQuestion8No_CheckedChanged
        RemoveHandler ckbxQuestion9No.CheckedChanged, AddressOf ckbxQuestion9No_CheckedChanged
        RemoveHandler ckbxQuestion10No.CheckedChanged, AddressOf ckbxQuestion10No_CheckedChanged
        RemoveHandler ckbxQuestion11No.CheckedChanged, AddressOf ckbxQuestion11No_CheckedChanged
        RemoveHandler ckbxQuestion12No.CheckedChanged, AddressOf ckbxQuestion12No_CheckedChanged
        RemoveHandler rbOvertimeNo.CheckedChanged, AddressOf rbOvertimeNo_CheckedChanged
        RemoveHandler rbOvertimeYes.CheckedChanged, AddressOf rbOvertimeYes_CheckedChanged
        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)

        ' setup the reference variables
        thisEnquiryId = loadEnquiryVar
        'set the form tag with EnquiryId
        Me.Tag = thisEnquiryId
        'reset variables
        incomeTotal = 0
        expenseTotal = 0
        'Load datatables

        Try
            'This line of code loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
            'get EnquiryManagerId
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            'Load User table
            UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            'get UserName
            lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
            'get type of Enquiry
            thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher    
            'set EnquiryType label
            lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
            'Get Wizard Status
            thisWizardStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("WizardStatus")
            thisCurrentStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentStatus")
            thisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
            'Get Type of main Customer
            thisTypeOfMainCustomer = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")
            'load Budget data by Enquiry Id
            Me.BudgetTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Budget, thisEnquiryId)

            '************************* Get Contact Display Name
            thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
            '************************* Set form title
            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & thisContactDisplayName & " - " & formTitle
                Panel1.BackColor = Color.MistyRose
                Panel2.BackColor = Color.MistyRose
                Panel3.BackColor = Color.MistyRose
            Else
                Me.Text = thisContactDisplayName & " - " & formTitle
            End If
            '************************* Cannotate Contact Name           
            contactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
            contactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " &
            EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & contactLastName
            'set lblContactName
            lblContactName.Text = contactName
            'Contact address
            contactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
            lblContactAddress.Text = contactAddress

            '*************** Set LoanReason display options
            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurrentLoan.Visible = True
                lblCurrentLoanAmt.Visible = True
            Else
                lblCurrentLoan.Visible = False
                lblCurrentLoanAmt.Visible = False
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        '************************************************
        ''Get Application values (ValuesId = 1)
        Try
            Dim thisAppsettings As New AppSettings
            thisSingleDepend = thisAppsettings.DependSingle
            thisCoupleDepend = thisAppsettings.DependCouple
            thisDependChild = thisAppsettings.DependChild
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try


        '************************************************
        Try
            'Calculate Budget YTD
            Dim budgetNetAmount As Decimal
            Dim budgetGross As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetGross"))
            Dim budgetPaye As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPAYE"))
            Dim budgetDeductions As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDeductions"))
            budgetNetAmount = budgetGross - budgetPaye - budgetDeductions
            'Get 31st March Year
            Dim year As Integer = Date.Now.Year
            Dim datTim1 As Date
            Dim datTim2 As Date
            Dim weekNum As Integer
            'check Period Starting has value
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodStarting") IsNot DBNull.Value Then
                datTim1 = CDate(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodStarting"))
            Else
                datTim1 = New DateTime(year, 4, 1)
            End If
            'check Period Ending has value
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodEnding") IsNot DBNull.Value Then
                datTim2 = CDate(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetPeriodEnding"))
            Else
                datTim2 = Date.Now
            End If
            'Calculate week number from 31st march
            If datTim1 < datTim2 Then
                weekNum = DateDiff(DateInterval.WeekOfYear, datTim1, datTim2)
            Else
                datTim1 = New DateTime((year - 1), 4, 1)
                weekNum = DateDiff(DateInterval.WeekOfYear, datTim1, datTim2)
            End If
            'set period starting DateTimePicker
            dtpPSDate.Value = datTim1
            'Calculate AverageYTD
            If budgetNetAmount > 0 And weekNum > 0 Then
                Dim averageYtd As Decimal = budgetNetAmount / weekNum * 52 / 12
                lblAvYTDValue.Text = averageYtd.ToString("C")
            End If
            'End of Calculate Budget YTD
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        '************************************************
        Try
            'Set Client Dependency from ThisClientType
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyClient") IsNot DBNull.Value Then
                clientDependency = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyClient"))
                If Not clientDependency > 0 Then
                    'If ThisClientType = "Individual" Then
                    '    ClientDependency = ThisSingleDepend
                    '    lblDependClient.Text = ThisSingleDepend.ToString("C")
                    'ElseIf ThisClientType = "Joint" Then
                    '    ClientDependency = ThisCoupleDepend
                    '    lblDependClient.Text = ThisCoupleDepend.ToString("C")
                    'End If
                    clientDependency = thisSingleDepend
                    lblDependClient.Text = thisSingleDepend.ToString("C")
                Else
                    lblDependClient.Text = clientDependency.ToString("C")
                End If
            Else
                'If ThisClientType = "Individual" Then
                '    ClientDependency = ThisSingleDepend
                '    lblDependClient.Text = ThisSingleDepend.ToString("C")
                'ElseIf ThisClientType = "Joint" Then
                '    ClientDependency = ThisCoupleDepend
                '    lblDependClient.Text = ThisCoupleDepend.ToString("C")
                'End If
                clientDependency = thisSingleDepend
                lblDependClient.Text = thisSingleDepend.ToString("C")
            End If
            EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyClient") = clientDependency

            'Budget dependency (get BudgetDependencyChildren)
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyChildren") IsNot DBNull.Value Then
                budgetDependTotal = clientDependency + (CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("BudgetDependencyChildren")) * thisDependChild)
            Else
                budgetDependTotal = clientDependency
            End If
            lblTotalDependValue.Text = budgetDependTotal.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        '************************************************
        Try
            'Total Incomes and Expenses
            lblMarginValue.ForeColor = Color.Black
            Dim budgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
            'add up the budget items
            For Each budgetItemRow In EnquiryWorkSheetDataSet.Budget
                incomeTotal = incomeTotal + CDec(budgetItemRow.Item("IncomeValue"))
                expenseTotal = expenseTotal + CDec(budgetItemRow.Item("ExpenseValue"))
            Next
            'Add NetIncomeStated to Total Incomes
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated") IsNot DBNull.Value Then
                incomeTotal = incomeTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated"))
            End If
            lblTotalIncomeValue.Text = incomeTotal.ToString("C")
            'Add ExpensesStated to Total Expenses
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated") IsNot DBNull.Value Then
                expenseTotal = expenseTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated"))
            End If
            lblTotalExpensesValue.Text = expenseTotal.ToString("C")
            '******************************************
            'Calculate margin
            lblMarginValue.ForeColor = Color.Black
            Dim budgetMargin As Decimal = incomeTotal - expenseTotal - budgetDependTotal
            If budgetMargin < 0 Then
                lblMarginValue.ForeColor = Color.Red
            End If
            lblMarginValue.Text = budgetMargin.ToString("C")

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        '************************************************
        'Set Question check boxes and buttons
        'Display step
        ToolStripStatusLabel1.Text = "Setting Question check boxes and buttons."

        Try
            ' set Overtime radioButtons
            SetOvertimeRadioButtonState()
            'Set Question 1 ckbx and btn
            ChangeStateQuestion(1)
            'Set Question 2 ckbx and btn
            ChangeStateQuestion(2)
            'Set Question 3 ckbx and btn
            ChangeStateQuestion(3)
            'Set Question 4 ckbx and btn
            ChangeStateQuestion(4)
            'Set Question 5 ckbx and btn
            ChangeStateQuestion(5)
            'Set Question 6 ckbx and btn
            ChangeStateQuestion(6)
            'Set Question 7 ckbx and btn
            ChangeStateQuestion(7)
            'Set Question 8 ckbx and btn
            ChangeStateQuestion(8)
            'Set Question 9 ckbx and btn
            ChangeStateQuestion(9)
            'Set Question 10 ckbx and btn
            ChangeStateQuestion(10)
            'Set Question 11 ckbx and btn
            ChangeStateQuestion(11)
            'Set Question 12 ckbx and btn
            ChangeStateQuestion(12)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        '*************** Get Dealer name
        'Display step
        ToolStripStatusLabel1.Text = "Getting Dealer name"

        Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection2 As New SqlConnection(connectionString2)
        Dim selectStatement2 As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & thisDealerId & "'"
        'MsgBox("Select Statement = " & selectStatement1)
        Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
        Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand2)
        'Dim dealerTempDataSet As New DataSet
        Dim dealerTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            dealerTempDataAdapter.Fill(dealerTempDataTable)
            'if no matching rows .....
            If dealerTempDataTable.Rows.Count = 0 Then
                MessageBox.Show("No Dealer Name, please try again.")
                'clear the dataTable and the Connect information
                dealerTempDataAdapter = Nothing
                dealerTempDataTable.Clear()
                'if there is a matching row
            ElseIf dealerTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                thisDealerName = dealerTempDataRow.Item(0)
                'clear the dataTable and the Connect information
                dealerTempDataAdapter = Nothing
                dealerTempDataTable.Clear()
            End If
            'close the connection
            If connection2.State <> ConnectionState.Closed Then
                connection2.Close()
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        lblDealerName.Text = thisDealerName
        '*************** End of Get Dealer name
        '***************  Docs folder
        'Display step
        ToolStripStatusLabel1.Text = "Setting up Docs folder"

        'set documents button visibility
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
        'Check directory exists
        If Directory.Exists(folderPath) Then
            'get new image
            btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
            'activate drag and drop
            Me.btnDocs.AllowDrop = True
        Else
            'get new image
            btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
            'deactivate drag and drop
            Me.btnDocs.AllowDrop = False
        End If

        '*****************************  get QRGList status
        'Display step
        ToolStripStatusLabel1.Text = "Setting QRGList"

        Dim connectionString1 As String = My.Settings.EnquiryWorkSheetConnectionString
        Dim connection1 As New SqlConnection(connectionString1)
        Dim selectStatement1 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
        'MsgBox("Select Statement = " & selectStatement1)
        Dim selectCommand1 As New SqlCommand(selectStatement1, connection1)
        Dim qrgListTempDataAdapter As New SqlDataAdapter(selectCommand1)
        'Dim qrgListTempDataSet As New DataSet
        Dim qrgListTempDataTable As New DataTable
        Try
            'dumps results into datatable LoginDataTable
            qrgListTempDataAdapter.Fill(qrgListTempDataTable)
            'if no matching rows .....
            If qrgListTempDataTable.Rows.Count = 0 Then
                MessageBox.Show("ERROR: No QRG List, please try again.")
                'clear the dataTable and the Connect information
                qrgListTempDataAdapter = Nothing
                qrgListTempDataTable.Clear()
                'if there is a matching row
            ElseIf qrgListTempDataTable.Rows.Count = 1 Then
                'get active value
                Dim qrgListTempDataRow As DataRow = qrgListTempDataTable.Rows(0)
                qrgStatus = qrgListTempDataRow.Item(0)
                'MsgBox("QRG Status = " & QRGStatus)
                'clear the dataTable and the Connect information
                qrgListTempDataAdapter = Nothing
                qrgListTempDataTable.Clear()
            End If
            'close the connection
            If connection1.State <> ConnectionState.Closed Then
                connection1.Close()
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        '************* set QRGStatus button colour
        Select Case qrgStatus
            Case 0
                btnQRGList.BackColor = Color.Transparent
            Case 1
                btnQRGList.BackColor = Color.Tomato
            Case 2
                btnQRGList.BackColor = Color.LightGreen
            Case Else
                btnQRGList.BackColor = Color.Transparent
        End Select
        '************************************************
        '*************** Set permissions
        '*************** Set permissions
        'Display step
        ToolStripStatusLabel1.Text = "Settings permissions"

        'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
        Select Case LoggedinPermissionLevel
            Case Is = 1 'Readonly
                UpdateAllToolStripMenuItem.Visible = False
                btnSaveAndExit.Enabled = False
                btnFinish.Enabled = False
                btnDocs.Enabled = False
                gpbxIncome.Enabled = False
                gpbxExpenses.Enabled = False
                gpbxDependency.Enabled = False
                btnDeleteSelectedRow.Enabled = False
                txtbxOtherExplain.IsReadOnly = True
                txtbxOvertime.IsReadOnly = True

            Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                '--------------------------------------
                btnFinish.Enabled = False
                gpbxIncome.Enabled = False
                gpbxExpenses.Enabled = False
                gpbxDependency.Enabled = False
                btnDeleteSelectedRow.Enabled = False
                txtbxOtherExplain.IsReadOnly = True
                txtbxOvertime.IsReadOnly = True

            Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                'Can see /use Archive Enquiry in File menu for their user status
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True
            Case Is = 4
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True

            Case Is = 5
                'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True
            Case Is > 5 'Systems Manager Level, Open.
                '--------------------------------------- 2
                AddCommentToolStripMenuItem1.Visible = True
                AddCommentToolStripMenuItem.Visible = True
                EmailToolStripMenuItem.Visible = True
                '--------------------------------------- 3
                ApprovalFormToolStripMenuItem1.Visible = True
                EditCandDToolStripMenuItem.Visible = True
                ClientDetailsToolStripMenuItem.Visible = True
                EndEnquiryToolStripMenuItem.Visible = True

            Case Else 'Readonly
                UpdateAllToolStripMenuItem.Visible = False
                btnSaveAndExit.Enabled = False
                btnFinish.Enabled = False
                btnDocs.Enabled = False

        End Select
        '*************** end of Set permissions

        ''test appDetect switch
        'If appDetect = True Then
        '    'set StatusStrip text
        '    ToolStripStatusLabel1.ForeColor = Color.Black
        '    ToolStripStatusLabel1.Text = "Status: Ready."
        'End If


        'MsgBox("ThisEnquiryId = " & ThisEnquiryId & vbCrLf _
        '        & "ThisWizardStatus = " & ThisWizardStatus & vbCrLf _
        '        & "ThisClientId = " & ThisClientId & vbCrLf _
        '        & "ThisClientSalutation = " & ThisClientSalutation & vbCrLf _
        '        & "ThisClientType = " & ThisClientType & vbCrLf _
        '        & "ThisSingleDepend = " & CStr(ThisSingleDepend) & vbCrLf _
        '        & "ThisCoupleDepend = " & CStr(ThisCoupleDepend) & vbCrLf _
        '        & "ThisDependChild = " & CStr(ThisDependChild) & vbCrLf _
        '        & "ClientDependency = " & CStr(ClientDependency) & vbCrLf _
        '        & "BudgetDependTotal = " & CStr(BudgetDependTotal))
        ResumeLayout()

    End Sub 'Loading Form

    Private Sub WkShtWizForm4_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SuspendLayout()
        'Add Handlers
        AddHandler dtpPSDate.ValueChanged, AddressOf dtpPSDate_ValueChanged
        AddHandler dtpPEDate.ValueChanged, AddressOf dtpPEDate_ValueChanged
        AddHandler ckbxQuestion1No.CheckedChanged, AddressOf ckbxQuestion1No_CheckedChanged
        AddHandler ckbxQuestion2No.CheckedChanged, AddressOf ckbxQuestion2No_CheckedChanged
        AddHandler ckbxQuestion3No.CheckedChanged, AddressOf ckbxQuestion3No_CheckedChanged
        AddHandler ckbxQuestion4No.CheckedChanged, AddressOf ckbxQuestion4No_CheckedChanged
        AddHandler ckbxQuestion5No.CheckedChanged, AddressOf ckbxQuestion5No_CheckedChanged
        AddHandler ckbxQuestion6No.CheckedChanged, AddressOf ckbxQuestion6No_CheckedChanged
        AddHandler ckbxQuestion7No.CheckedChanged, AddressOf ckbxQuestion7No_CheckedChanged
        AddHandler ckbxQuestion8No.CheckedChanged, AddressOf ckbxQuestion8No_CheckedChanged
        AddHandler ckbxQuestion9No.CheckedChanged, AddressOf ckbxQuestion9No_CheckedChanged
        AddHandler ckbxQuestion10No.CheckedChanged, AddressOf ckbxQuestion10No_CheckedChanged
        AddHandler ckbxQuestion11No.CheckedChanged, AddressOf ckbxQuestion11No_CheckedChanged
        AddHandler ckbxQuestion12No.CheckedChanged, AddressOf ckbxQuestion12No_CheckedChanged
        AddHandler rbOvertimeNo.CheckedChanged, AddressOf rbOvertimeNo_CheckedChanged
        AddHandler rbOvertimeYes.CheckedChanged, AddressOf rbOvertimeYes_CheckedChanged

        ResumeLayout()
        'set StatusStrip text
        ToolStripStatusLabel1.BackColor = Color.Empty
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Page Loaded."

    End Sub

    Private Sub WkShtWizForm4_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        ToolStripStatusLabel1.Text = "Status: Ready."
    End Sub

    Private Sub FormRefresh()
        Call PassVariable(thisEnquiryId)

    End Sub

    '********************** Question checkboxes and buttons *********************
    Private Sub ChangeStateQuestion(ByVal num As String)
        Dim budgetQuestion As String = "BudgetQuestion" & num
        Dim ckbx As String = "ckbxQuestion" & num & "No"
        Dim btn As String = "btnQuestion" & num & "Add"
        'get State of Question from database
        Dim stateQuestion As Integer = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item(budgetQuestion)
        'Look through Controls Collection to find 'ckbx'
        'restrict find to groupbox added 27/10/2016 
        Dim controls1() As Control
        If num > 2 Then
            controls1 = Me.gpbxExpenses.Controls.Find(ckbx, True)
        Else
            controls1 = Me.gpbxIncome.Controls.Find(ckbx, True)
        End If

        If controls1.Count > 0 Then
            ' It exists, let us cast it to the right control type (CheckBox)
            Dim thisCheckBox1 As CheckBox = DirectCast(controls1(0), CheckBox)
            ' We're working with a checkbox now...
            Select Case stateQuestion
                Case 1 'no
                    thisCheckBox1.Checked = True
                    thisCheckBox1.Enabled = True
                Case 2 'add has been activated
                    thisCheckBox1.Checked = False
                    thisCheckBox1.Enabled = False
                Case Else 'nothing has happened
                    thisCheckBox1.Checked = False
                    thisCheckBox1.Enabled = True
            End Select
        End If
        'Look through Controls Collection to find 'btn'
        'restrict find to groupbox added 27/10/2016 
        Dim controls2() As Control
        If num > 2 Then
            controls2 = Me.gpbxExpenses.Controls.Find(btn, True)
        Else
            controls2 = Me.gpbxIncome.Controls.Find(btn, True)
        End If
        If controls2.Count > 0 Then
            ' It exists, let us cast it to the right control type (CheckBox)
            Dim thisBtn1 As Button = DirectCast(controls2(0), Button)
            ' We're working with a Button now...
            Select Case stateQuestion
                Case 1
                    thisBtn1.Enabled = False
                Case 2
                    thisBtn1.Enabled = True
                Case Else
                    thisBtn1.Enabled = True
            End Select
        End If
    End Sub

    Private Sub btnDependSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDependSingle.Click
        'Get Value for Single dependency
        Try
            lblDependClient.Text = thisSingleDepend.ToString("C")
            CalcTotalDependValue()
            CalcMargin()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnDependCouple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDependCouple.Click
        'Get Value for Couple dependency
        Try
            lblDependClient.Text = thisCoupleDepend.ToString("C")
            CalcTotalDependValue()
            CalcMargin()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtbxNumChildren_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxNumChildren.LostFocus
        Try
            CalcTotalDependValue()
            CalcMargin()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CalcTotalDependValue()
        Try
            'Budget dependency
            ' BudgetDependTotal already set in top of document
            If CDec(txtbxNumChildren.Text) > 0 Then
                budgetDependTotal = CDec(lblDependClient.Text) + (CDec(txtbxNumChildren.Text) * thisDependChild)
            Else
                budgetDependTotal = CDec(lblDependClient.Text)
            End If
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "CalcTotalDependValue(): BudgetDependTotal = " & budgetDependTotal & ". Status: Ready."
            'MsgBox("CalcTotalDependValue(): BudgetDependTotal = " & BudgetDependTotal)
            lblTotalDependValue.Text = budgetDependTotal.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CalcMargin()
        Try
            Dim marginValue As Decimal = CDec(lblTotalIncomeValue.Text) - CDec(lblTotalExpensesValue.Text) - CDec(lblTotalDependValue.Text)
            If marginValue < 0 Then
                lblMarginValue.ForeColor = Color.Red
            Else
                lblMarginValue.ForeColor = Color.Black
            End If
            lblMarginValue.Text = marginValue.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtbxGross_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxGross.LostFocus
        CalcAvYTDValue()
    End Sub

    Private Sub txtbxPAYE_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxPAYE.LostFocus
        CalcAvYTDValue()
    End Sub

    Private Sub txtbxDeductions_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxDeductions.LostFocus
        CalcAvYTDValue()
    End Sub

    Private Sub dtpPSDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPSDate.ValueChanged
        CalcAvYTDValue()
    End Sub

    Private Sub dtpPEDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPEDate.ValueChanged
        CalcAvYTDValue()
    End Sub

    Private Sub CalcAvYTDValue()
        Try
            'Calculate Budget
            Dim budgetNetAmount As Decimal
            'Dim budgetGross As Decimal = CDec(txtbxGross.Text)
            'Dim budgetPaye As Decimal = CDec(txtbxPAYE.Text)
            'Dim budgetDeductions As Decimal = CDec(txtbxDeductions.Text)
            Dim budgetGross As Decimal = CDec(If(String.IsNullOrEmpty(txtbxGross.Text), "0", txtbxGross.Text))
            Dim budgetPaye As Decimal = CDec(If(String.IsNullOrEmpty(txtbxPAYE.Text), "0", txtbxPAYE.Text))
            Dim budgetDeductions As Decimal = CDec(If(String.IsNullOrEmpty(txtbxDeductions.Text), "0", txtbxDeductions.Text))
            Dim periodEnding As Date = CDate(dtpPEDate.Value)
            Dim periodStarting As Date = CDate(dtpPSDate.Value)
            budgetNetAmount = budgetGross - budgetPaye - budgetDeductions
            Dim weekNum As Integer
            'check Period Ending has value
            If periodEnding = Nothing Then
                periodEnding = Date.Now
            End If
            'Calculate week number from PeriodStarting
            If periodStarting < periodEnding Then
                weekNum = DateDiff(DateInterval.WeekOfYear, periodStarting, periodEnding)
            Else
                MessageBox.Show("Starting Period is OLDER than Ending Period", "Calculating YYD Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            'Calculate AverageYTD
            If budgetNetAmount > 0 And weekNum > 0 Then
                Dim averageYtd As Decimal = budgetNetAmount / weekNum * 52 / 12
                lblAvYTDValue.Text = averageYtd.ToString("C")
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TotalIncomeExpenses()
        Try
            'Total Incomes and Expenses
            incomeTotal = 0
            expenseTotal = 0
            Dim budgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
            For Each budgetItemRow In EnquiryWorkSheetDataSet.Budget
                incomeTotal = incomeTotal + CDec(budgetItemRow.Item("IncomeValue"))
                expenseTotal = expenseTotal + CDec(budgetItemRow.Item("ExpenseValue"))
            Next
            'Total Incomes
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated") IsNot DBNull.Value Then
                incomeTotal = incomeTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated"))
            End If
            lblTotalIncomeValue.Text = incomeTotal.ToString("C")
            'Total Expenses
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated") IsNot DBNull.Value Then
                expenseTotal = expenseTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated"))
            End If
            lblTotalExpensesValue.Text = expenseTotal.ToString("C")
            '******************************************
            'Budget dependency
            Dim dependClient As Decimal
            'Dim BudgetDependTotal As Decimal, already set at top of page
            'Client Dependency
            If Not lblDependClient.Text = Nothing Or Not lblDependClient.Text = "" Then
                dependClient = CDec(lblDependClient.Text)
            Else
                'If ThisClientType = "Joint" Then
                '    dependClient = thisCoupleDepend
                'End If
                dependClient = thisSingleDepend
            End If
            'Children Dependency
            'MsgBox("txtbxNumChildren.Text = " & txtbxNumChildren.Text)
            If Not txtbxNumChildren.Text = Nothing Or Not txtbxNumChildren.Text = "" Or CDec(txtbxNumChildren.Text) > 0 Then
                budgetDependTotal = dependClient + (CDec(txtbxNumChildren.Text) * thisDependChild)
            Else
                budgetDependTotal = dependClient
            End If
            lblTotalDependValue.Text = budgetDependTotal.ToString("C")
            '******************************************
            'Calculate margin
            Dim budgetMargin As Decimal = incomeTotal - expenseTotal - budgetDependTotal
            If budgetMargin < 0 Then
                lblMarginValue.ForeColor = Color.Red
            Else
                lblMarginValue.ForeColor = Color.Black
            End If
            lblMarginValue.Text = budgetMargin.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub CalcExpenseIncomeTotals()
        Try
            'Total Incomes and Expenses
            lblMarginValue.ForeColor = Color.Black
            Dim incomeTotal As Decimal = 0
            Dim expenseTotal As Decimal = 0
            Dim budgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
            For Each budgetItemRow In EnquiryWorkSheetDataSet.Budget
                incomeTotal = incomeTotal + CDec(budgetItemRow.Item("IncomeValue"))
                expenseTotal = expenseTotal + CDec(budgetItemRow.Item("ExpenseValue"))
            Next
            'Total Incomes
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated") IsNot DBNull.Value Then
                incomeTotal = incomeTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("NetIncomeStated"))
            End If
            lblTotalIncomeValue.Text = incomeTotal.ToString("C")
            'Total Expenses
            If EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated") IsNot DBNull.Value Then
                expenseTotal = expenseTotal + CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ExpensesStated"))
            End If
            lblTotalExpensesValue.Text = expenseTotal.ToString("C")
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.EnquiryBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            'check if data has changed
            Dim rowView2 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not Me.EnquiryBindingSource.Current Is Nothing Then
                rowView2 = CType(Me.EnquiryBindingSource.Current, DataRowView)
                'test if any fields have changed
                If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        Try
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            Me.Validate()
                            Me.EnquiryBindingSource.EndEdit()
                            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Changes saved successfully."
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            MsgBox(ex.Message)
                        End Try
                    Else
                        ' User choose No. Do nothing
                    End If
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'launch next form and pass Enquiry Id
        Select Case thisTypeOfMainCustomer
            Case MyEnums.mainCustomerType.SoleTrader, MyEnums.mainCustomerType.Partnership
                Try
                    'launch CompanyFinancialForm
                    Dim companyFinancialFrm As New CompanyFinancialForm
                    companyFinancialFrm.PassVariable(thisEnquiryId)
                    companyFinancialFrm.Show()
                    'Close this Form
                    Me.Close()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("Launching CompanyFinancialForm caused an error:" & vbCrLf & ex.Message)
                End Try
            Case Else
                Try
                    'launch WkShtWizForm2 (security)
                    Dim wkShtWizFrm2 As New WkShtWizForm2
                    wkShtWizFrm2.PassVariable(thisEnquiryId)
                    wkShtWizFrm2.Show()
                    'Close this Form
                    Me.Close()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox("Launching WkShtWizForm2 caused an error:" & vbCrLf & ex.Message)
                End Try
        End Select

        'Close this Form
        Me.Close()

        
    End Sub

    Private Sub BtnNextClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        sw = New Stopwatch
        sw.Start()
        frmLoadInfo.Clear()
        frmLoadInfo.Append("Start Trace **************" & " ")
        frmLoadInfo.Append("BtnNextClick: start sub - " & sw.Elapsed.ToString & ". ")
        'log.Debug(frmLoadInfo.ToString)
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        Application.DoEvents()

        'SuspendLayout()

        'check data entry
        Dim checkFieldsMessage As String = ""
        If overtimeState = True Then
            If txtbxOvertime.Text.Length = 0 Then
                checkFieldsMessage = checkFieldsMessage + "Please enter what was done to verify the continuity of Earnings." + vbCrLf
            End If
        End If
        If checkFieldsMessage.Length > 0 Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: There are errors please view message box."
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Else
            Try
                frmLoadInfo.Append("BtnNextClick: Validating data - " & sw.Elapsed.ToString & ". ")
                ToolStripStatusLabel1.Text = "Validating data."
                Application.DoEvents()
                Dim progressStatus = False
                Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
                'Dim rowView2 As DataRowView
                'leave if bindingsource.current is nothing (no data)
                If Not bindSrc2.Current Is Nothing Then
                    'rowView2 = CType(bindSrc2.Current, DataRowView)
                    'test if any fields have changed
                    'If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                    '    ''update WizardStatus
                    '    'Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                    '    'EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    '    'EnquiryRow.BeginEdit()
                    '    'EnquiryRow.WizardStatus = 6
                    '    'save changes
                    '    Me.Validate()
                    '    bindSrc2.EndEdit()
                    '    Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                    '    ProgressStatus = True
                    'Else
                    '    'no changes to save - proceed
                    '    'save anyway (because of radio buttons)
                    '    Me.Validate()
                    '    bindSrc2.EndEdit()
                    '    Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                    '    ProgressStatus = True
                    'End If
                    Me.Validate()
                    bindSrc2.EndEdit()
                    Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                    progressStatus = True
                Else
                    'no current record in binding source - do not proceed
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                    progressStatus = False
                End If

                If progressStatus = True Then
                    Try
                        ''launch WkShtWizForm5 and pass Enquiry Id
                        'Dim WkShtWizFrm5 As New WkShtWizForm5
                        'WkShtWizFrm5.PassVariable(ThisEnquiryId)
                        'WkShtWizFrm5.Show()
                        frmLoadInfo.Append("BtnNextClick: Launching next form - " & sw.Elapsed.ToString & ". ")
                        log.Debug(frmLoadInfo.ToString)
                        ToolStripStatusLabel1.Text = "Launching next form."
                        Application.DoEvents()
                        'launch WkShtWizForm3 and pass Enquiry Id
                        Dim wkShtWizFrm3 As New WkShtWizForm3
                        wkShtWizFrm3.PassVariable(thisEnquiryId)
                        wkShtWizFrm3.Show()
                        'Close this Form
                        Me.Close()

                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try

                Else
                    'MsgBox("No record to save, please enter information")
                    'set StatusStrip text
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No record to save, please enter information."
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
        End If
        'ResumeLayout()

        Cursor.Current = Cursors.Default
    End Sub

    Private Sub btnIncomeStatedAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncomeStatedAdd.Click
        Try
            'Save current values
            Me.Validate()
            Me.BudgetBindingSource.EndEdit()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Try
            Dim addBudgetIncomeItemFrm As New AddBudgetIncomeItemForm
            addBudgetIncomeItemFrm.PassVariable(thisEnquiryId)
            addBudgetIncomeItemFrm.ShowDialog()
            'If (AddBudgetItemFrmShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            '    'Call the function you used to populate the data grid..
            '    MsgBox("AddBudgetItemFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
            '    Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, ThisEnquiryId)
            'End If
            addBudgetIncomeItemFrm.Dispose()
            Me.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
            'Total incomes and expenses
            TotalIncomeExpenses()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnExpensesStated_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExpensesStated.Click
        Try
            'Save current values
            Me.Validate()
            Me.BudgetBindingSource.EndEdit()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        Try
            Dim addBudgetExpenseItemFrm As New AddBudgetExpenseItemForm
            addBudgetExpenseItemFrm.PassVariable(thisEnquiryId)
            addBudgetExpenseItemFrm.ShowDialog()
            'If (AddBudgetItemFrmShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            '    'Call the function you used to populate the data grid..
            '    MsgBox("AddBudgetItemFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
            '    Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, ThisEnquiryId)
            'End If
            addBudgetExpenseItemFrm.Dispose()
            Me.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
            'Total incomes and expenses
            TotalIncomeExpenses()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtbxNetIncomeStatedValue_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxNetIncomeStatedValue.LostFocus
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Stated Income successfully updated. Status: Ready."
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        TotalIncomeExpenses()
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub txtbxNetExpensesStatedValue_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxNetExpensesStatedValue.LostFocus
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Stated Expenses successfully updated. Status: Ready."
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        TotalIncomeExpenses()
        Cursor.Current = Cursors.Default
    End Sub

    'Private Sub AddBudgetIncomeItem()
    '    'Try
    '    '    'Save current values
    '    '    Me.Validate()
    '    '    Me.BudgetBindingSource.EndEdit()
    '    '    Me.EnquiryBindingSource.EndEdit()
    '    '    Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '    'Catch ex As Exception
    '    '    MsgBox(ex.Message)
    '    'End Try
    '    'Try
    '    '    Dim AddBudgetIncomeItemFrm As New AddBudgetIncomeItemForm
    '    '    AddBudgetIncomeItemFrm.PassVariable(ThisEnquiryId)
    '    '    AddBudgetIncomeItemFrm.ShowDialog()
    '    '    'If (AddBudgetItemFrmShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '    '    '    'Call the function you used to populate the data grid..
    '    '    '    MsgBox("AddBudgetItemFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
    '    '    '    Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, ThisEnquiryId)
    '    '    'End If
    '    '    AddBudgetIncomeItemFrm.Dispose()
    '    '    Me.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, ThisEnquiryId)
    '    '    'Total incomes and expenses
    '    '    TotalIncomeExpenses()
    '    'Catch ex As Exception
    '    '    MsgBox(ex.Message)
    '    'End Try
    'End Sub

    Private Sub BudgetDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles BudgetDataGridView.DoubleClick
        Try
            'test there is data rows
            If Not BudgetBindingSource.Current Is Nothing Then
                'Get Selected Row
                Dim selectedRowView As Data.DataRowView
                Dim selectedRow As EnquiryWorkSheetDataSet.BudgetRow
                selectedRowView = CType(BudgetBindingSource.Current, System.Data.DataRowView)
                selectedRow = CType(selectedRowView.Row, EnquiryWorkSheetDataSet.BudgetRow)
                'Get Income Value
                Dim incomeFieldVal As Decimal = CDec(selectedRow.IncomeValue)
                'Get Expense value
                Dim expenseFieldVal As Decimal = CDec(selectedRow.ExpenseValue)
                'Test Values
                If incomeFieldVal > expenseFieldVal Then
                    'Launch Add Income Form
                    Dim modBudgetFrm As New AddBudgetIncomeItemForm
                    modBudgetFrm.PassVariable(thisEnquiryId, selectedRow.BudgetId)
                    ' Show ModBudgetFrm as a modal dialog and determine if DialogResult = OK.
                    If modBudgetFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                        'Call the function you used to populate the data grid..
                        Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                    End If
                    modBudgetFrm.Dispose()
                Else
                    'Launch Add Expense Form
                    Dim modBudgetFrm As New AddBudgetExpenseItemForm
                    modBudgetFrm.PassVariable(thisEnquiryId, selectedRow.BudgetId)
                    ' Show ModBudgetFrm as a modal dialog and determine if DialogResult = OK.
                    If modBudgetFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                        'Call the function you used to populate the data grid..
                        Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                    End If
                    modBudgetFrm.Dispose()
                End If
                'Total incomes and expenses
                TotalIncomeExpenses()
            Else
                'MsgBox("No Data Row selected." + vbCrLf + "Please select a Data Row")
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No Data Row selected, Please select a Data Row!"
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonQuestionAddIncomeClick(ByVal num As String)
        Dim budgetQuestion As String = "BudgetQuestion" & num
        Dim ckbx As String = "ckbxQuestion" & num & "No"
        'Find the checkbox
        Dim controls1() As Control
        If num > 2 Then
            controls1 = Me.gpbxExpenses.Controls.Find(ckbx, True)
        Else
            controls1 = Me.gpbxIncome.Controls.Find(ckbx, True)
        End If
        'launch Add Budget Income Form
        Try
            Dim addBudgetIncomeItemFrm As New AddBudgetIncomeItemForm
            addBudgetIncomeItemFrm.PassVariable(thisEnquiryId)
            ' Show AddBudgetIncomeItemFrm as a modal dialog and determine if DialogResult = OK.
            If addBudgetIncomeItemFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'set CheckBox State here so is not set if AddBudgetIncomeItem is cancelled
                If controls1.Count > 0 Then
                    ' It exists, let us cast it to the right control type (CheckBox)
                    Dim thisCheckBox1 As CheckBox = DirectCast(controls1(0), CheckBox)
                    ' We're working with a checkbox now...
                    'MsgBox("ThisCheckbox = " & ThisCheckBox1.Name)
                    If thisCheckBox1.Checked = False Then
                        thisCheckBox1.Enabled = False
                    End If
                End If
                'set BudgetQuestion[Num] to 2: means item added and ckbxQuestion1No set disabled
                Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                enquiryRow.BeginEdit()
                enquiryRow.Item(budgetQuestion) = 2
                'save changes
                enquiryRow.EndEdit()
                'update
                Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                'Call the function you used to populate the data grid..
                Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
            End If
            addBudgetIncomeItemFrm.Dispose()
            'Total incomes and expenses
            TotalIncomeExpenses()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub ButtonQuestionAddExpenseClick(ByVal num As String)
        Dim budgetQuestion As String = "BudgetQuestion" & num
        Dim ckbx As String = "ckbxQuestion" & num & "No"
        'Find the checkbox
        Dim controls1() As Control
        If num > 2 Then
            controls1 = Me.gpbxExpenses.Controls.Find(ckbx, True)
        Else
            controls1 = Me.gpbxIncome.Controls.Find(ckbx, True)
        End If
        'launch Add Budget Expense Form
        Try
            Dim addBudgetExpenseItemFrm As New AddBudgetExpenseItemForm
            addBudgetExpenseItemFrm.PassVariable(thisEnquiryId)
            ' Show AAddBudgetExpenseItemFrm as a modal dialog and determine if DialogResult = OK.
            If addBudgetExpenseItemFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
                'set CheckBox State here so is not set if AddBudgetIncomeItem is cancelled
                If controls1.Count > 0 Then
                    ' It exists, let us cast it to the right control type (CheckBox)
                    Dim thisCheckBox1 As CheckBox = DirectCast(controls1(0), CheckBox)
                    ' We're working with a checkbox now...
                    'MsgBox("ThisCheckbox = " & ThisCheckBox1.Name)
                    If thisCheckBox1.Checked = False Then
                        thisCheckBox1.Enabled = False
                    End If
                End If
                'set BudgetQuestion[Num] to 2: means item added and ckbxQuestion1No set disabled
                Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                enquiryRow.BeginEdit()
                enquiryRow.Item(budgetQuestion) = 2
                'save changes
                Me.EnquiryBindingSource.EndEdit()
                'update
                Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                'Call the function you used to populate the data grid..
                Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
            End If
            addBudgetExpenseItemFrm.Dispose()
            'Total incomes and expenses
            TotalIncomeExpenses()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ButtonQuestionAddExpenseClick caused an exception: " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub ckbxQuestionCheckedChanged(ByVal num As String)
        Dim budgetQuestion As String = "BudgetQuestion" & num
        Dim ckbx As String = "ckbxQuestion" & num & "No"
        Dim btn As String = "btnQuestion" & num & "Add"
        Try
            Dim controls1() As Control
            Dim controls2() As Control
            If num > 2 Then
                controls1 = Me.gpbxExpenses.Controls.Find(ckbx, True)
                controls2 = Me.gpbxExpenses.Controls.Find(btn, True)
            Else
                controls1 = Me.gpbxIncome.Controls.Find(ckbx, True)
                controls2 = Me.gpbxIncome.Controls.Find(btn, True)
            End If
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim thisCheckBox1 As CheckBox = DirectCast(controls1(0), CheckBox)
                ' We're working with a checkbox now...
                If controls2.Count > 0 Then
                    ' It exists, let us cast it to the right control type (Button)
                    Dim thisBtn1 As Button = DirectCast(controls2(0), Button)
                    ' We're working with a Button now...
                    If thisCheckBox1.Checked = True Then
                        thisBtn1.Enabled = False
                        'set BudgetQuestion1 to 1: means item not added and ckbxQuestion1No set checked
                        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                        enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                        enquiryRow.BeginEdit()
                        enquiryRow.Item(budgetQuestion) = 1
                        'save changes
                        Me.EnquiryBindingSource.EndEdit()
                        'update
                        Me.TableAdapterManager.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                        'Call the function you used to populate the data grid..
                        Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                    Else
                        thisBtn1.Enabled = True
                        'set BudgetQuestion1 to 0: means item not added and ckbxQuestion1No not checked
                        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                        enquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                        enquiryRow.BeginEdit()
                        enquiryRow.Item(budgetQuestion) = 0
                        'save changes
                        Me.EnquiryBindingSource.EndEdit()
                        'update
                        Me.TableAdapterManager.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                        'Call the function you used to populate the data grid..
                        Me.TableAdapterManager.BudgetTableAdapter.FillByEnquiryId(EnquiryWorkSheetDataSet.Budget, thisEnquiryId)
                    End If
                End If
            End If

        Catch ex As DBConcurrencyException
            Dim customErrorMessage As String
            customErrorMessage = "Concurrency violation" & vbCrLf
            customErrorMessage += CType(ex.Row.Item(0), String)
            MessageBox.Show(customErrorMessage)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

#Region "Questions"
    Private Sub ckbxQuestion1No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion1No.CheckedChanged
        ckbxQuestionCheckedChanged(1)
    End Sub

    Private Sub btnQuestion1Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion1Add.Click
        ButtonQuestionAddIncomeClick(1)
    End Sub

    Private Sub ckbxQuestion2No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion2No.CheckedChanged
        ckbxQuestionCheckedChanged(2)
    End Sub

    Private Sub btnQuestion2Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion2Add.Click
        ButtonQuestionAddIncomeClick(2)
    End Sub

    Private Sub ckbxQuestion3No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion3No.CheckedChanged
        ckbxQuestionCheckedChanged(3)
    End Sub

    Private Sub btnQuestion3Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion3Add.Click
        ButtonQuestionAddExpenseClick(3)
    End Sub

    Private Sub ckbxQuestion4No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion4No.CheckedChanged
        ckbxQuestionCheckedChanged(4)
    End Sub

    Private Sub btnQuestion4Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion4Add.Click
        ButtonQuestionAddExpenseClick(4)
    End Sub
    Private Sub ckbxQuestion5No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion5No.CheckedChanged
        ckbxQuestionCheckedChanged(5)
    End Sub

    Private Sub btnQuestion5Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion5Add.Click
        ButtonQuestionAddExpenseClick(5)
    End Sub
    Private Sub ckbxQuestion6No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion6No.CheckedChanged
        ckbxQuestionCheckedChanged(6)
    End Sub

    Private Sub btnQuestion6Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion6Add.Click
        ButtonQuestionAddExpenseClick(6)
    End Sub
    Private Sub ckbxQuestion7No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion7No.CheckedChanged
        ckbxQuestionCheckedChanged(7)
    End Sub

    Private Sub btnQuestion7Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion7Add.Click
        ButtonQuestionAddExpenseClick(7)
    End Sub
    Private Sub ckbxQuestion8No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion8No.CheckedChanged
        ckbxQuestionCheckedChanged(8)
    End Sub

    Private Sub btnQuestion8Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion8Add.Click
        ButtonQuestionAddExpenseClick(8)
    End Sub

    Private Sub ckbxQuestion9No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion9No.CheckedChanged
        ckbxQuestionCheckedChanged(9)
    End Sub

    Private Sub btnQuestion9Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion9Add.Click
        ButtonQuestionAddExpenseClick(9)
    End Sub

    Private Sub ckbxQuestion10No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion10No.CheckedChanged
        ckbxQuestionCheckedChanged(10)
    End Sub

    Private Sub btnQuestion10Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion10Add.Click
        ButtonQuestionAddExpenseClick(10)
    End Sub

    Private Sub ckbxQuestion11No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion11No.CheckedChanged
        ckbxQuestionCheckedChanged(11)
        If ckbxQuestion11No.CheckState = CheckState.Checked Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Green
            ToolStripStatusLabel1.Text = "Board/Rent/Mortgage must have an explanation in the Comments Box!"
        End If
    End Sub

    Private Sub btnQuestion11Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion11Add.Click
        ButtonQuestionAddExpenseClick(11)
    End Sub

    Private Sub ckbxQuestion12No_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxQuestion12No.CheckedChanged
        ckbxQuestionCheckedChanged(12)
    End Sub

    Private Sub btnQuestion12Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuestion12Add.Click
        ButtonQuestionAddExpenseClick(12)
    End Sub

#End Region




    Private Sub btnDeleteSelectedRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSelectedRow.Click
        'Check a row is selected
        If BudgetDataGridView.Rows.Count > 0 Then
            'Get Selected Row Index
            Dim rowIndex As Integer = BudgetDataGridView.CurrentRow.Index
            'launch DeleteRecord Form
            Dim deleteRecordFrm As New DeleteRecordForm
            deleteRecordFrm.PassVariable(thisEnquiryId, thisContactDisplayName)
            deleteRecordFrm.ShowDialog(Me)
            If deleteRecordFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                Try
                    'mark row for deletion
                    BudgetDataGridView.Rows.RemoveAt(rowIndex)
                    'Commit the deletion and update dataset
                    BudgetTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Budget)
                    Formrefresh()
                    'Recalculate Income and expenses
                    TotalIncomeExpenses()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
                deleteRecordFrm.Dispose()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Deleting record completed. Status: Ready."

            ElseIf deleteRecordFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                deleteRecordFrm.Dispose()
                'User choose Cancel, do nothing
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Deleting record cancelled. Status: Ready."
            End If

            '    'Get user confirmation
            '    Dim msg As String
            '    Dim title As String
            '    Dim style As MsgBoxStyle
            '    Dim response As MsgBoxResult
            '    msg = "Do you want to Delete Selected Record?"   ' Define message.
            '    style = MsgBoxStyle.DefaultButton2 Or _
            '       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
            '    title = "Record Deletion"   ' Define title.
            '    ' Display message.
            '    response = MsgBox(msg, style, title)
            '    If response = MsgBoxResult.Yes Then   ' User choose Yes.
            '        Try
            '            'Get Selected Row Index
            '            Dim rowIndex As Integer = BudgetDataGridView.CurrentRow.Index
            '            'mark row for deletion
            '            BudgetDataGridView.Rows.RemoveAt(rowIndex)
            '            'Commit the deletion and update dataset
            '            BudgetTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Budget)
            '            'Recalculate Income and expenses
            '            TotalIncomeExpenses()
            '        Catch ex As Exception
            '            MsgBox(ex.Message)
            '        End Try
            '    Else
            '        ' Perform some other action.
            '    End If

        Else
            'no row selected
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "No Data Row selected, Please select a Data Row!"
        End If

    End Sub

    Private Sub WkShtWizForm4_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SuspendLayout()

        'check if data has changed
        Dim bindSrc1 As BindingSource = Me.EnquiryBindingSource
        Dim rowView1 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not bindSrc1.Current Is Nothing Then
            rowView1 = CType(bindSrc1.Current, DataRowView)
            'test if any fields have changed
            If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        bindSrc1.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If

        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            newLocX = 0
        Else
            newLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            newLocY = 0
        Else
            newLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.PrelimFormLocation = New Point(newLocX, newLocY)
        My.Settings.PrelimFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings

    End Sub

    Private Sub WkShtWizForm4_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            Panel1.Size = New Size(formWidth - 16, formHeight - 197)
            gpbxCalculator.Size = New Size(formWidth - 48, 80)
            tlpCalculator.Size = New Size(formWidth - 60, 51)
            gpbxIncome.Size = New Size(formWidth - 48, 150)
            tlpIncomeStated.Size = New Size(formWidth - 60, 29)
            tlpIncome.Size = New Size(formWidth - 60, 29)
            tlpOvertime.Size = New Size(formWidth - 60, 67)
            txtbxOvertime.Size = New Size(formWidth - 403, 35)
            gpbxExpenses.Size = New Size(formWidth - 48, 205)
            tlpExpenseStated.Size = New Size(formWidth - 60, 29)
            tlpExpenses.Size = New Size(formWidth - 60, 149)
            gpbxDependency.Size = New Size(formWidth - 48, 59)
            tlpDependency.Size = New Size(formWidth - 60, 29)
            gpbxBudgetSummary.Size = New Size(formWidth - 48, 210)
            txtbxOtherExplain.Size = New Size(formWidth - 60, 127)
        End If

    End Sub

    Private Sub btnSaveAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndExit.Click
        Try
            'save changes
            Cursor.Current = Cursors.WaitCursor
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
            'close form
            Me.Close()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Save and Exit: " + vbCrLf + ex.Message)
        End Try
    End Sub

    '******************* Documents
    Private Sub btnDocs_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Drag and drop successful.    Status: Ready"
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Drag and Drop caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnDocs_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        Else
            Try
                'create directory
                IO.Directory.CreateDirectory(fname)
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
                'refresh form
                Formrefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
            Catch ex As System.IO.DirectoryNotFoundException
                ' Let the user know that the directory did not exist.
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

        End If
    End Sub
    '*************** End of Documents

    Private Sub btnQRGList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQRGList.Click
        Try
            'MsgBox("ThisQRGListId = " & ThisQRGListId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Quick reference Guide List."
            Dim QRGFrm As New QRGForm
            QRGFrm.PassVariable(thisEnquiryId, thisEnquiryCode, thisContactDisplayName)
            'AddSecurityFrm.ShowDialog(Me)
            If (QRGFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                '*****************************  get QRGList status
                Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection2 As New SqlConnection(connectionString2)
                Dim selectStatement2 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand2)
                Dim QRGListTempDataSet As New DataSet
                Dim QRGListTempDataTable As New DataTable
                Try
                    'dumps results into datatable LoginDataTable
                    QRGListTempDataAdapter.Fill(QRGListTempDataTable)
                    'if no matching rows .....
                    If QRGListTempDataTable.Rows.Count = 0 Then
                        MessageBox.Show("ERROR: No QRG List, please try again.")
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                        'if there is a matching row
                    ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                        'get  QRGStatus value
                        Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                        qrgStatus = QRGListTempDataRow.Item(0)
                        'MsgBox("QRG Status = " & QRGStatus)
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                    End If
                    'close the connection
                    If connection2.State <> ConnectionState.Closed Then
                        connection2.Close()
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

                'set QRGStatus button colour
                Select Case qrgStatus
                    Case 0
                        btnQRGList.BackColor = Color.Transparent
                    Case 1
                        btnQRGList.BackColor = Color.Tomato
                    Case 2
                        btnQRGList.BackColor = Color.LightGreen
                    Case Else
                        btnQRGList.BackColor = Color.Transparent
                End Select
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Quick reference Guide List updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Quick reference Guide List cancelled. Status: Ready."
            End If
            QRGFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub rbOvertimeNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOvertimeNo.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    overtimeSelectedrb = rb
                    overtimeState = False
                    EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("OvertimeState") = overtimeState
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Overtime Question = " & overtimeSelectedrb.Text
                End If
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbOvertimeYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOvertimeYes.CheckedChanged
        'check sender is a RadioButton
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    overtimeSelectedrb = rb
                    overtimeState = True
                    EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("OvertimeState") = overtimeState
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Overtime Question = " & overtimeSelectedrb.Text
                End If
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub SetOvertimeRadioButtonState()
        'set the radio button states for Joint Name Credit Questions
        Dim QuestionYesSet As String = "rbOvertimeYes"
        Dim QuestionNoset As String = "rbOvertimeNo"
        Dim CheckState As String = "OvertimeState"
        'get State of Question from database
        overtimeState = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item(CheckState)
        Try
            'Look through Controls Collection to find 'QuestionYesSet'
            Dim controls1() As Control = Me.Controls.Find(QuestionYesSet, True)
            If controls1.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton1 As RadioButton = DirectCast(controls1(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case overtimeState
                    Case 0
                        ThisRadioButton1.Checked = False
                    Case 1
                        ThisRadioButton1.Checked = True
                    Case Else
                        ThisRadioButton1.Checked = False
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Try
            'Look through Controls Collection to find 'QuestionNoSet'
            Dim controls2() As Control = Me.Controls.Find(QuestionNoset, True)
            If controls2.Count > 0 Then
                ' It exists, let us cast it to the right control type (CheckBox)
                Dim ThisRadioButton2 As RadioButton = DirectCast(controls2(0), RadioButton)
                ' We're working with a radiobutton now...
                Select Case overtimeState
                    Case 0
                        ThisRadioButton2.Checked = True
                    Case 1
                        ThisRadioButton2.Checked = False
                    Case Else
                        ThisRadioButton2.Checked = True
                End Select
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnViewDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Not Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Shared Folder does not exist!."
        Else
            Dim ViewDocsFrm As New ViewDocsForm
            ViewDocsFrm.PassVariable(thisEnquiryCode, thisContactDisplayName)
            ViewDocsFrm.ShowDialog(Me)
            If ViewDocsFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                ViewDocsFrm.Dispose()
            Else
                ViewDocsFrm.Dispose()
            End If
        End If
    End Sub

    '******************************* MenuStrip code
#Region "MenuStrip"

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub


    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim DisplayDealersFrm As New DisplayDealersForm
            DisplayDealersFrm.PassVariable(thisDealerId)
            DisplayDealersFrm.ShowDialog()
            DisplayDealersFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '************ end of Check Names

    Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            'Me.ClientBindingSource.EndEdit()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        FormRefresh()
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EditCandDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCandDToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailFrm As New EmailForm
        EmailFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailFrm.Dispose()

    End Sub

    Private Sub ApprovalFormToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem1.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub EmailApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailApprovalToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailAppFrm As New EmailApprovalForm
        EmailAppFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailAppFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailAppFrm.Dispose()
    End Sub

    Private Sub WWWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WWWToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        'launch internet browser for web application
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching web browser."
        System.Diagnostics.Process.Start(My.Settings.YFL_website)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub DeclinedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeclinedToolStripMenuItem.Click
        thisEnquiryResult = "Declined"
        progressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                DeclinedFrm.Dispose()

                progressStatus = True

            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub WithdrawnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawnToolStripMenuItem.Click
        thisEnquiryResult = "Withdrawn"
        progressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim declinedFrm As New DeclinedForm
            declinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            declinedFrm.ShowDialog(Me)
            If declinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                declinedFrm.Dispose()

                progressStatus = True

            ElseIf declinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                declinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Withdrawing of Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub MCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MCToolStripMenuItem.Click
        Dim MembersCentreUtilObject As New WebUtil
        MembersCentreUtilObject.MCLink()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = MembersCentreUtilObject.GetLabelForeColor
        ToolStripStatusLabel1.Text = MembersCentreUtilObject.GetLabelText
    End Sub

#End Region
    '******************************* end of MenuStrip code

    
  
    
    
    
   
    
   
   
End Class