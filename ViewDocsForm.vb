﻿Imports System.IO

Public Class ViewDocsForm
    Dim thisEnquiryCode As String
    Dim thisClientSalutation As String
    Dim prevLoaded As Boolean = False

    Friend Sub PassVariable(ByVal enquiryCodeVar As String, ByVal clientSalutationVar As String)
        thisClientSalutation = clientSalutationVar
        thisEnquiryCode = enquiryCodeVar
        'Set form title
        Me.Text = Me.Text & " - " & thisClientSalutation
        '****************** Documents ************************************
        'Load list of file info for ThisEnquiryId into Documents Tab
        ' ListView name = lvDocuments
        Try
            'set properties
            If prevLoaded = False Then
                Me.lvDocuments.BackColor = SystemColors.Window
                lvDocuments.FullRowSelect = True
                lvDocuments.View = View.Details
                lvDocuments.GridLines = True
                ' Display check boxes.
                'lvDocuments.CheckBoxes = True
                ' Create columns for the items and subitems.
                ' Width of -2 indicates auto-size.
                lvDocuments.Columns.Add("Name", 389, HorizontalAlignment.Left)
                lvDocuments.Columns.Add("Creation Time", 125, HorizontalAlignment.Left)
                lvDocuments.Columns.Add("File Size", 100, HorizontalAlignment.Right)
            End If
            'Get Files        
            'Get String to WorkSheet Drive
            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
            Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
            'Check directory exists
            If Directory.Exists(folderPath) Then
                
                'de activate drag and drop
                Me.lvDocuments.AllowDrop = False
                'Create place to store files
                Dim aFiles() As String 'array type string
                aFiles = Directory.GetFiles(folderPath)
                'loop through aFiles
                For Each sfile As String In aFiles
                    'create str array
                    Dim str(3) As String
                    Dim itm As ListViewItem
                    'assign str array values
                    Dim finfo As New FileInfo(sfile)
                    If finfo.Exists Then
                        str(0) = finfo.Name
                        str(1) = finfo.CreationTime
                        If finfo.Length < 1000 Then
                            str(2) = finfo.Length.ToString & " Bytes"
                        ElseIf finfo.Length >= 1000 And finfo.Length < 1000000 Then
                            str(2) = (finfo.Length / 1000).ToString & " KB"
                        Else
                            str(2) = (finfo.Length / 1000000).ToString & " MB"
                        End If
                        itm = New ListViewItem(str)
                        'itm.Checked = True
                        'add ListViewItem to ListView
                        lvDocuments.Items.Add(itm)
                    End If
                Next sfile
            Else
                'deactivate drag and drop
                Me.lvDocuments.AllowDrop = False
                Me.lvDocuments.BackColor = Color.LavenderBlush
            End If

            prevLoaded = True

        Catch ex As Exception
            MsgBox("Load Form: Loading Documents List generated an Exception Message: " & vbCrLf & ex.Message)
        End Try

        '*************** End of Documents ********************************

    End Sub

    Private Sub lvDocuments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDocuments.DoubleClick
        Try
            Cursor.Current = Cursors.WaitCursor
            'Get String to WorkSheet Drive
            Dim worksheetDrive = Switch.GetWorksheetSharedFolder
            Dim filePath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
            Dim fileList As ListView.SelectedListViewItemCollection = lvDocuments.SelectedItems
            Dim item As ListViewItem
            Dim fileName As String
            For Each item In fileList
                fileName = item.SubItems(0).Text
                'MsgBox("Selected file: = " & filePath & fileName)
                'start program to open file
                System.Diagnostics.Process.Start(filePath & fileName)
            Next
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            MsgBox("The opening of Folder Files caused an exception:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub lvDocuments_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvDocuments.MouseDown
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            If lvDocuments.SelectedIndices.Count > 0 Then
                'launch contextMenu
                Dim contextMenu1 As New ContextMenu()
                Dim addedMenu As System.Windows.Forms.MenuItem
                addedMenu = contextMenu1.MenuItems.Add("rename")
                AddHandler addedMenu.Click, AddressOf renameEventHandler
                lvDocuments.ContextMenu = contextMenu1
            Else
                'lvDocuments.ContextMenu = Nothing
            End If
        End If
    End Sub

    Private Sub renameEventHandler(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Cursor.Current = Cursors.WaitCursor
            'Get String to WorkSheet Drive
            Dim worksheetDrive = Switch.GetWorksheetSharedFolder
            Dim filePath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
            Dim fileList As ListView.SelectedListViewItemCollection = lvDocuments.SelectedItems
            Dim item As ListViewItem
            Dim fileName As String = String.Empty
            Dim nFileName As String = String.Empty
            For Each item In fileList
                fileName = item.SubItems(0).Text
                'MsgBox("Selected file: = " & filePath & fileName)
                'load a modal form to rename the file
                Dim renameFileFrm As New frmRenameFile
                renameFileFrm.PassVariable(filePath & fileName)
                renameFileFrm.ShowDialog(Me)
                If renameFileFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                    nFileName = renameFileFrm.newFileName
                    My.Computer.FileSystem.RenameFile(filePath & fileName, nFileName)
                    renameFileFrm.Dispose()
                Else
                    'do nothing
                    renameFileFrm.Dispose()
                End If
            Next

            Cursor.Current = Cursors.WaitCursor
        Catch ex As Exception
            MsgBox("The renaming of file caused an exception:" & vbCrLf & ex.Message)
        End Try

        FormRefresh()
    End Sub

    Private Sub FormRefresh()
        Cursor.Current = Cursors.WaitCursor
        lvDocuments.Items.Clear()

        Try
            Call PassVariable(thisEnquiryCode, thisClientSalutation)
        Catch ex As Exception
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

End Class