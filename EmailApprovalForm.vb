﻿Imports System.Net.Mail
Imports System.IO
Public Class EmailApprovalForm
    Dim thisEnquiryId As Integer
    Dim signature As String
    Dim thisApplicationCode As String
    Dim thisContactDisplayName As String
    Dim thisDealerName As String
    Dim fileAttch As New ArrayList 'List of attachments
    Dim attachListString As String = "" 'String listing attachment file names
    Dim attachmentError As Boolean = False
    Dim thisDealerId As String
    Dim dealerEmail As String
    Dim addPostScript As Boolean = True

    'Loads form with parameters from Calling Form
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer, ByVal loadDealerIdVar As String)
        thisEnquiryId = loadEnquiryVar
        thisDealerId = loadDealerIdVar
        'set email from address
        txtbxEmailFrom.Text = LoggedinEmailAddress
        'signature
        signature = Util.GetSignature(True)
        lblSignature.Text = Util.GetSignature(False)
        'get Dealer email address
        Me.DealerContactDetailsTableAdapter.FillByDealerEmail(EnquiryWorkSheetDataSet.DealerContactDetails, thisDealerId, "Email")
        For Each dr As DataRow In EnquiryWorkSheetDataSet.Tables("DealerContactDetails").Rows
            If dr.Item("ContactMethod") = Constants.CONST_CM_EMAIL Then
                cmbxEmailTo.Items.Add(dr.Item("ContactValue"))
                cmbxCC.Items.Add(dr.Item("ContactValue"))
            End If
        Next
        'get Dealer name
        Me.DealerContactDetailsTableAdapter.FillByDistinctDealerId(EnquiryWorkSheetDataSet.DealerContactDetails, thisDealerId)
        'get Dealer Name(Trading Name)
        thisDealerName = EnquiryWorkSheetDataSet.DealerContactDetails.Rows(DealerContactDetailsBindingSource.Position()).Item("Name")
        'set default Dealer Name
        txtbxDealer.Text = thisDealerName
        'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
        Me.EnquiryTableAdapter1.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
        'get Application code
        thisApplicationCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource1.Position()).Item("ApplicationCode")
        thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource1.Position()).Item("ContactDisplayName")
        'set default Client name
        txtbxClient.Text = thisContactDisplayName
        'set default Email Subject
        txtbxEmailSubject.Text = "Application Result: Application " & thisApplicationCode & "; " & thisContactDisplayName

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAttachments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttachments.Click
        'Add an attachment
        Dim thisFileName As String = ""
        ' Displays an OpenFileDialog so the user can select a file.
        Dim openFileDialog1 As New OpenFileDialog()
        openFileDialog1.Filter = "Text|*.txt|All|*.*"
        openFileDialog1.Title = "Select a Document File"
        'Show the Dialog.
        'If the user clicked OK in the dialog and a document file was selected, get the file path
        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            thisFileName = openFileDialog1.FileName
            'MsgBox(ThisFileName)
            fileAttch.Add(thisFileName)
            attachListString = ""
            For Each listFileName As String In fileAttch
                Dim finfo As New FileInfo(listFileName)
                If finfo.Exists Then
                    attachListString = attachListString & finfo.Name & "; "
                Else
                    MessageBox.Show("Can not find file " & finfo.Name, "Missing file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Next
        End If
        txtbxAttachmentsList.Text = attachListString
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Cursor.Current = Cursors.WaitCursor
        Dim checkFieldsMessage As String = ""
        'check Dealer
        If txtbxDealer.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a Dealer!" + vbCrLf
        End If
        'check Client
        If txtbxClient.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a Client!" + vbCrLf
        End If
        'check email to field
        Dim emailToAddresses As ArrayList
        Dim emailCcAddresses As ArrayList
        Dim emailPart As String
        If Not cmbxEmailTo.Text.Length > 0 Then
            checkFieldsMessage = checkFieldsMessage + "Please enter address for 'email to'!" + vbCrLf
        ElseIf cmbxEmailTo.Text.Contains(",") Then
            emailToAddresses = New ArrayList(cmbxEmailTo.Text.Split(New Char() {","c}))
            For Each emailPart In emailToAddresses
                If Not ValidateEmail(emailPart.Trim) Then
                    checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a vaild email address!" + vbCrLf
                End If
            Next
        ElseIf cmbxEmailTo.Text.Contains(";") Then
            emailToAddresses = New ArrayList(cmbxEmailTo.Text.Split(New Char() {";"c}))
            For Each emailPart In emailToAddresses
                If Not ValidateEmail(emailPart.Trim) Then
                    checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a vaild email address!" + vbCrLf
                End If
            Next
        Else
            emailToAddresses = New ArrayList
            emailToAddresses.Add(cmbxEmailTo.Text.Trim)
            If Not ValidateEmail(cmbxEmailTo.Text.Trim) Then
                checkFieldsMessage = checkFieldsMessage + "Please enter a valid 'to' email address!" + vbCrLf
            End If
        End If
        'check email CC
        If cmbxCC.Text.Length > 0 Then
            If cmbxCC.Text.Contains(",") Then
                'emailAddresses = cmbxEmailTo.Text.Split(New Char() {","c})
                emailCcAddresses = New ArrayList(cmbxCC.Text.Split(New Char() {","c}))
                For Each emailPart In emailCcAddresses
                    If Not ValidateEmail(emailPart.Trim) Then
                        checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a valid email address!" + vbCrLf
                    End If
                Next
            ElseIf cmbxCC.Text.Contains(";") Then
                'emailAddresses = cmbxEmailTo.Text.Split(New Char() {";"c})
                emailCcAddresses = New ArrayList(cmbxCC.Text.Split(New Char() {";"c}))
                For Each emailPart In emailCcAddresses
                    If Not ValidateEmail(emailPart.Trim) Then
                        checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a vaild email address!" + vbCrLf
                    End If
                Next
            Else
                emailCcAddresses = New ArrayList
                emailCcAddresses.Add(cmbxCC.Text.Trim)
                If Not ValidateEmail(cmbxCC.Text.Trim) Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter a valid 'CC' email address!" + vbCrLf
                End If
            End If
        End If

        'check email subject
        If txtbxEmailSubject.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a subject!" + vbCrLf
        End If
        'check result
        If cmbxResult.SelectedValue Is Nothing And Not cmbxResult.Text.Length > 0 Then
            checkFieldsMessage = checkFieldsMessage + "Please select a result!" + vbCrLf
        End If
        'check email message
        If rtbConditions.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter conditions!" + vbCrLf
        End If
        'check for error messages
        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            Me.DialogResult = System.Windows.Forms.DialogResult.None
        Else
            Dim mailBodyText As String = ""
            Try
                'Convert rtf to html
                Dim r2h As New RTFtoHTML
                'r2h.rtf = rtbConditions.Rtf (Spellbox does not do RTF)
                r2h.text = rtbConditions.Text
                Dim rtfPostScript As String
                If addPostScript = True Then
                    rtfPostScript = "<tr><td colspan='2'><p><br />Please respond to this email with:" & _
                "<ul><li>A detailed description of Goods</li><li>Payment start date</li><li>Delivery date</li></ul></p></td></tr>"
                Else
                    rtfPostScript = ""
                End If
                'construct mail body
                mailBodyText = "<table width='598' border='0' cellspacing='3' cellpadding='3'>" & _
                "<caption><h1>Application Result</h1></caption>" & _
                "<tr><td width='150'><strong>Date:</strong></td><td width='448'>" & Date.Now.ToLongDateString & "</td></tr>" & _
                "<tr><td><strong>Dealer:</strong></td><td>" & txtbxDealer.Text & "</td></tr>" & _
                "<tr><td><strong>Client:</strong></td><td>" & txtbxClient.Text & "</td></tr>" & _
                "<tr><td><strong>Result:</strong></td><td>" & cmbxResult.Text & "</td></tr>" & _
                "<tr><td colspan='2'>Based on information supplied, subject to due diligence and the terms of your dealer agreement</td></tr>" & _
                "<tr><td  valign='top'><strong>Other conditions:</strong></td><td>" & r2h.html & "</td></tr>" & rtfPostScript & _
                "</table><p>" & signature & "</p>"
                'send email
            Catch ex As Exception
                MsgBox("Error formatting HTML:" & vbCrLf & ex.Message)
                Exit Sub
            End Try
            Try
                Dim thisMail As New MailMessage
                thisMail.From = New MailAddress(txtbxEmailFrom.Text)
                'ThisMail.To
                If emailToAddresses.Count > 0 Then
                    For Each emailPart In emailToAddresses
                        thisMail.To.Add(emailPart.Trim)
                    Next
                End If
                'ThisMail.CC
                If cmbxCC.Text.Length > 0 Then
                    If emailCcAddresses.Count > 0 Then
                        For Each emailPart In emailCcAddresses
                            thisMail.CC.Add(emailPart.Trim)
                        Next
                    End If
                End If
                'set content
                thisMail.Subject = txtbxEmailSubject.Text
                thisMail.Body = mailBodyText
                thisMail.IsBodyHtml = True
                'add attachments
                Dim attachment As System.Net.Mail.Attachment
                For Each ListFileName As String In fileAttch
                    Dim finfo As New FileInfo(ListFileName)
                    If finfo.Exists Then
                        attachment = New Net.Mail.Attachment(ListFileName)
                        thisMail.Attachments.Add(attachment)
                    Else
                        MessageBox.Show("Can not find file " & finfo.Name, "Missing file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        attachmentError = True
                    End If
                Next

                'send mail
                Dim client As New SmtpClient(My.Settings.SmtpClient)
                client.Port = My.Settings.SmtpClientPort

                '*********************** adjust credentials
                client.UseDefaultCredentials = True
                'client.Credentials = New Net.NetworkCredential("csteel", "Art1s@n1")
                '*********************** end of adjust credentials
                If attachmentError = True Then
                    MessageBox.Show("Attachment error detected, sending email aborted", "Attachment Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    client.Send(thisMail)
                    Cursor.Current = Cursors.Default

                    '  Optional user reassurance:
                    MessageBox.Show(String.Format("Message Subject: ' {0} ' successfully sent" & vbCrLf & "from: {1}" & vbCrLf & "to: {2}" & vbCrLf & "CC: {3}", thisMail.Subject, thisMail.From, thisMail.To.ToString, thisMail.CC.ToString), "EMail", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                    Cursor.Current = Cursors.WaitCursor
                    'add comments to LoanComments data
                    Dim commentStr As String
                    Dim theseAttachments As String
                    Dim postScript As String
                    If addPostScript = True Then
                        postScript = "Please respond to this email with:" & vbCrLf & _
                    "A detailed description of Goods, Payment start date, Delivery date."
                    Else
                        postScript = ""
                    End If
                    'Create Attachments string
                    If txtbxAttachmentsList.Text.Length > 0 Then
                        theseAttachments = "Attachments sent: " & txtbxAttachmentsList.Text
                    Else
                        theseAttachments = ""
                    End If
                    'Add Date string to comments
                    commentStr = "Email sent to " & thisMail.To.ToString & vbCrLf & "CC: " & thisMail.CC.ToString & vbCrLf & "Subject: " & _
                    thisMail.Subject & ": " & vbCrLf & "Dealer: " & txtbxDealer.Text & " - Client: " & txtbxClient.Text & vbCrLf & cmbxResult.Text & _
                    ": Based on information supplied, subject to due diligence and the terms of your dealer agreement" & vbCrLf & _
                    "Conditions: " & rtbConditions.Text & vbCrLf & postScript & vbCrLf & theseAttachments
                    'debug
                    'MsgBox(commentStr)
                    Dim commentId As Integer
                    commentId = Util.SetComment(thisEnquiryId, LoggedinName, commentStr)

                    'Housekeeping
                    thisMail.Dispose()
                    Cursor.Current = Cursors.Default
                    'Close Form
                    Me.DialogResult = System.Windows.Forms.DialogResult.OK
                End If


            Catch ex As FormatException
                Dim inner As Exception = ex.InnerException
                If Not (inner Is Nothing) Then
                    MsgBox("Format Exception: " + vbCrLf + inner.Message)
                    inner = inner.InnerException
                Else
                    MsgBox("Format Exception: " + vbCrLf + ex.Message)
                End If
            Catch ex As SmtpException
                Dim inner As Exception = ex.InnerException
                If Not (inner Is Nothing) Then
                    MsgBox("SMTP Exception: " + vbCrLf + inner.Message)
                    inner = inner.InnerException
                Else
                    MsgBox("SMTP Exception: " + vbCrLf + ex.Message)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        End If

    End Sub

    Private Sub cmbxResult_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbxResult.SelectedIndexChanged
        'Pending
        'Approval with full recourse
        'Approval with partial recourse
        'Approval with no recourse
        'Declined()
        Dim selectedResult As String = cmbxResult.SelectedItem.ToString
        If selectedResult = "Pending" Or selectedResult = "Declined" Then
            addPostScript = False
            lblPostScript.Visible = False
        Else
            addPostScript = True
            lblPostScript.Visible = True
        End If
    End Sub

#Region "Information buttons"
    Private Sub pbEmail_Click(sender As Object, e As EventArgs) Handles pbEmail.Click
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\EmailInformation.rtf", "Email information", MyEnums.MsgBoxMode.FilePath)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()
    End Sub

    Private Sub pbEmail_MouseHover(sender As Object, e As EventArgs) Handles pbEmail.MouseHover
        pbEmail.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbEmail_MouseLeave(sender As Object, e As EventArgs) Handles pbEmail.MouseLeave
        pbEmail.Image = My.Resources.questionBsm
    End Sub

#End Region


End Class