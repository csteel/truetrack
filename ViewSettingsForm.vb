﻿Imports System.IO
Imports System.Xml

Public Class ViewSettingsForm

    Friend Sub ViewSettingsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''Dim fs As New FileStream("..\..\app.config", FileMode.Open)
        'Try
        '    Dim fs As New FileStream("AppWhStB.exe.config", FileMode.Open)
        '    Dim tr As XmlReader = XmlReader.Create(fs)
        '    Dim i As Integer = 0
        '    While Not tr.EOF
        '        If tr.MoveToContent = XmlNodeType.Element And tr.Name = "setting" Then
        '            RichTextBox1.AppendText(tr.GetAttribute(0) & ":  ")
        '            tr.Read()
        '            If tr.MoveToContent = XmlNodeType.Element And tr.Name = "value" Then
        '                RichTextBox1.AppendText(tr.ReadElementContentAsString & vbNewLine)
        '            Else
        '                tr.Read()
        '            End If
        '        Else
        '            tr.Read()
        '        End If
        '    End While
        'Catch ex As Exception
        '    MsgBox("View Settings caused an error:" & vbCrLf & ex.Message)
        'End Try
        ''above code works with ("..\..\app.config", FileMode.Open)while in debugger but does not work
        ''when compiled even using ("AppWhStB.exe.config", FileMode.Open), get 
        ''error file not found.

        Try
            'user settings
            lblForm1SizeValue.Text = My.Settings.Form1Size.ToString
            lblForm1LocValue.Text = My.Settings.Form1Location.ToString
            lblPrelimSizeValue.Text = My.Settings.PrelimFormSize.ToString
            lblPrelimLocValue.Text = My.Settings.PrelimFormLocation.ToString
            lblAppFormSizeValue.Text = My.Settings.AppFormSize.ToString
            lblAppFormLocValue.Text = My.Settings.AppFormLocation.ToString
            'application settings
            lblEWSConnStringValue.Text = My.Settings.EnquiryWorkSheetConnectionString.ToString
            lblWebAppPageValue.Text = My.Settings.WebApplicationPage_PROD.ToString
            lblSizeValue.Text = Me.Size.ToString
            lblClientSizeValue.Text = Me.ClientSize.ToString
            lblGetForm1SizeValue.Text = Form1.Size.ToString
            lblGetForm1ClientSizeValue.Text = Form1.ClientSize.ToString
            lblGetForm1MaxSizeValue.Text = Form1.MaximumSize.ToString
            lblWorkingAreaValue.Text = SystemInformation.WorkingArea.ToString
            'test if AppForm exits
            Dim frm As Form
            For Each frm In Application.OpenForms
                If frm.Name = "AppForm" Then
                    lblGetAppformSizeValue.Visible = True
                    lblGetAppformSizeValue.Text = AppForm.Size.ToString
                    Exit For
                End If
            Next





        Catch ex As Exception
            MsgBox("View Settings caused an error:" & vbCrLf & ex.Message)
        End Try
        

    End Sub


End Class