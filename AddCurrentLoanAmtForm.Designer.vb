﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddCurrentLoanAmtForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddCurrentLoanAmtForm))
        Me.txtbxCurrentLoanAmtValue = New System.Windows.Forms.TextBox()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblDeleteTitle = New System.Windows.Forms.Label()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.lblDisplayName = New System.Windows.Forms.Label()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.lblLoanAmtNote = New System.Windows.Forms.Label()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtbxCurrentLoanAmtValue
        '
        Me.txtbxCurrentLoanAmtValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxCurrentLoanAmtValue.Location = New System.Drawing.Point(120, 118)
        Me.txtbxCurrentLoanAmtValue.Name = "txtbxCurrentLoanAmtValue"
        Me.txtbxCurrentLoanAmtValue.Size = New System.Drawing.Size(215, 20)
        Me.txtbxCurrentLoanAmtValue.TabIndex = 43
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(367, 177)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 46
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.LightGreen
        Me.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnSave.Location = New System.Drawing.Point(12, 177)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 45
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        Me.btnSave.Visible = False
        '
        'lblDeleteTitle
        '
        Me.lblDeleteTitle.AutoSize = True
        Me.lblDeleteTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteTitle.Location = New System.Drawing.Point(112, 15)
        Me.lblDeleteTitle.Name = "lblDeleteTitle"
        Me.lblDeleteTitle.Size = New System.Drawing.Size(230, 20)
        Me.lblDeleteTitle.TabIndex = 44
        Me.lblDeleteTitle.Text = "Enter Current Loan Amount"
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryCode.Location = New System.Drawing.Point(120, 66)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(70, 13)
        Me.lblEnquiryCode.TabIndex = 47
        Me.lblEnquiryCode.Text = "Enquiry Code"
        '
        'lblDisplayName
        '
        Me.lblDisplayName.AutoSize = True
        Me.lblDisplayName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactDisplayName", True))
        Me.lblDisplayName.Location = New System.Drawing.Point(120, 89)
        Me.lblDisplayName.Name = "lblDisplayName"
        Me.lblDisplayName.Size = New System.Drawing.Size(72, 13)
        Me.lblDisplayName.TabIndex = 48
        Me.lblDisplayName.Text = "Display Name"
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Location = New System.Drawing.Point(147, 145)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(161, 13)
        Me.lblNote.TabIndex = 49
        Me.lblNote.Text = "enter whole dollars only i.e. 1234"
        '
        'lblLoanAmtNote
        '
        Me.lblLoanAmtNote.AutoSize = True
        Me.lblLoanAmtNote.Location = New System.Drawing.Point(171, 39)
        Me.lblLoanAmtNote.Name = "lblLoanAmtNote"
        Me.lblLoanAmtNote.Size = New System.Drawing.Size(119, 13)
        Me.lblLoanAmtNote.TabIndex = 50
        Me.lblLoanAmtNote.Text = "(Amount before Top-up)"
        '
        'AddCurrentLoanAmtForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 212)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLoanAmtNote)
        Me.Controls.Add(Me.lblNote)
        Me.Controls.Add(Me.lblDisplayName)
        Me.Controls.Add(Me.lblEnquiryCode)
        Me.Controls.Add(Me.txtbxCurrentLoanAmtValue)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblDeleteTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(470, 250)
        Me.MinimumSize = New System.Drawing.Size(470, 250)
        Me.Name = "AddCurrentLoanAmtForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Current Loan Amount"
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtbxCurrentLoanAmtValue As System.Windows.Forms.TextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblDeleteTitle As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ClientTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ClientTableAdapter
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents lblDisplayName As System.Windows.Forms.Label
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmtNote As System.Windows.Forms.Label
End Class
