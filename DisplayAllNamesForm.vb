﻿Imports System.Data.SqlClient

Public Class DisplayAllNamesForm
    Dim NewLocX As Integer
    Dim NewLocY As Integer

    ''' <summary>
    ''' Check Names from TrueTrack database (DisplayAllNames table)
    ''' </summary>
    ''' <param name="lastNameLike"></param>
    ''' <param name="filterArchived"></param>
    ''' <param name="includeContactDisplayName"></param>
    ''' <param name="displayNameLike"></param>
    ''' <remarks></remarks>
    Friend Sub PassVariable(Optional ByVal lastNameLike As String = "", Optional ByVal filterArchived As Boolean = False, Optional ByVal includeContactDisplayName As Boolean = False, Optional ByVal displayNameLike As String = "")
        Cursor.Current = Cursors.WaitCursor
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.DisplayAllNames' table. 
            Me.DisplayAllNamesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.DisplayAllNames)
        Catch ex As Exception
            MsgBox("Filling DisplayAllNames Datatable cause an error:" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
        'set filter
        Dim filterString As String = String.Empty
        'TODO: add search  for all names with and without apostrophe if the searched name has an apostrophe
        If lastNameLike.Length > 0 Then
            txtbxLastNameFilter.Text = lastNameLike
            lastNameLike = lastNameLike.Replace("'", "''")
            If includeContactDisplayName = True Then
                If filterArchived = True Then
                    If displayNameLike.Length > 0 Then
                        filterString = "Archived = 0 AND (LastName like '" & lastNameLike & "%' OR ContactDisplayName like '%" & displayNameLike.Replace("'", "''") & "%')"
                    Else
                        filterString = "Archived = 0 AND (LastName like '" & lastNameLike & "%')"
                    End If
                    txtbxDisplayNameFilter.Text = displayNameLike
                Else
                    filterString = "LastName like '" & lastNameLike & "%' OR ContactDisplayName like '%" & displayNameLike.Replace("'", "''") & "%'"
                    txtbxDisplayNameFilter.Text = displayNameLike
                End If
            Else
                If filterArchived = True Then
                    filterString = "LastName like '" & lastNameLike & "%' AND Archived = 0"
                Else
                    filterString = "LastName like '" & lastNameLike & "%'"
                End If
            End If

            DisplayAllNamesBindingSource.Filter = filterString
        End If

        DisplayAllNamesBindingSource.Sort = "DateTime DESC"

        Cursor.Current = Cursors.Default
    End Sub

    Private Sub DisplayAllNamesForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get user settings
        Me.ClientSize = New Size(My.Settings.DisplayAllNamesSize)
        Me.Location = New Point(My.Settings.DisplayAllNamesLocation)
        'Select the DisplayName filter box
        Me.Show()
        txtbxDisplayNameFilter.Select()

    End Sub

#Region "Filters"
    Private Sub txtbxDisplayNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxDisplayNameFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "ContactDisplayName like '%" & txtbxDisplayNameFilter.Text.Replace("'", "''") & "%'"
        'debug
        'MessageBox.Show("txtbxDisplayNameFilter.Text = " & txtbxDisplayNameFilter.Text)
    End Sub

    Private Sub txtbxLastNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxLastNameFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "LastName like '" & txtbxLastNameFilter.Text.Replace("'", "''") & "%'"

    End Sub

    Private Sub txtbxFirstNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxFirstNameFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "FirstName like '" & txtbxFirstNameFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxSuburbFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxSuburbFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "ContactSuburb like '" & txtbxSuburbFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxCityFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxCityFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "ContactCity like '" & txtbxCityFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxEnquiryCodeFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxEnquiryCodeFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "EnquiryCode like '" & txtbxEnquiryCodeFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxAppCodeFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxAppCodeFilter.TextChanged
        DisplayAllNamesBindingSource.Filter = "ApplicationCode like '" & txtbxAppCodeFilter.Text.Replace("'", "''") & "%'"
    End Sub

#End Region
    

    Private Sub DisplayAllNamesDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DisplayAllNamesDataGridView.DoubleClick
        'set cursor to wait cursor
        Cursor.Current = Cursors.WaitCursor
        Dim WizardStatus As Integer = 0

        Try
            If DisplayAllNamesDataGridView.Rows.Count > 0 Then
                'Dim rowIndex As Integer = SearchWithParametersDataGridView.CurrentCell.RowIndex
                'Dim row As DataGridViewRow = SearchWithParametersDataGridView.Rows(rowIndex)
                Dim row As DataGridViewRow = DisplayAllNamesDataGridView.CurrentRow
                Dim cellEid As DataGridViewCell = row.Cells.Item("EnquiryId")
                Dim cell10 As DataGridViewCell = row.Cells.Item("Archived")
                Dim cell11 As DataGridViewCell = row.Cells.Item("IsArchived")
                Dim enquiryIdValue As String = If(IsDBNull(cellEid.Value), String.Empty, CStr(cellEid.Value))
                Dim enquiryArchived As Boolean = If(IsDBNull(cell10.Value), False, cell10.Value)
                Dim enquiryIsArchived As Boolean = If(IsDBNull(cell11.Value), False, cell11.Value)
                'MsgBox("Enquiry Archived Value = " & EnquiryArchived.ToString)
                'MsgBox("EnquiryId = " & enquiryIdValue)

                If enquiryArchived = True Then 'destructive archive(purged)
                    Dim ArchiveFrm As New ArchiveForm
                    Try
                        'launch ArchiveForm passing EnquiryCodeValue
                        ArchiveFrm.PassVariable(enquiryIdValue)
                        ArchiveFrm.Show()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                Else
                    If enquiryIsArchived = True Then 'IsArchived (non distructive archive)
                        'Open Standard Worksheet Form
                        Dim EnquiryForm As New AppForm
                        Try
                            EnquiryForm.PassVariable(enquiryIdValue)
                            EnquiryForm.Show()
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try

                    Else
                        Try
                            'Get Wizard Status for selected Enquiry
                            'Get Connection String Settings
                            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
                            Dim connection As New SqlConnection(connectionString)
                            Dim selectStatement As String = "SELECT WizardStatus, EnquiryId FROM Enquiry WHERE EnquiryId ='" & enquiryIdValue & "'"
                            Dim selectCommand As New SqlCommand(selectStatement, connection)
                            Dim EnquiryWizDataAdapter As New SqlDataAdapter(selectCommand)
                            Dim EnquiryWizDataSet As New DataSet
                            Dim EnquiryWizDataTable As New DataTable

                            'dumps results into datatable EnquiryWizDataTable
                            EnquiryWizDataAdapter.Fill(EnquiryWizDataTable)
                            'if no matching rows .....
                            If EnquiryWizDataTable.Rows.Count = 0 Then
                                MsgBox("Detect Wizard Status failed, Opening Standard Worksheet Form.")
                                ' clear the dataTable and the Connect information
                                EnquiryWizDataAdapter = Nothing
                                EnquiryWizDataTable.Clear()
                                'Open Standard Worksheet Form
                                WizardStatus = MyEnums.WizardStatus.AppForm

                                'if there is a matching row
                            ElseIf EnquiryWizDataTable.Rows.Count = 1 Then
                                'get WizardStatus value
                                Dim EnquiryWizDataRow As DataRow = EnquiryWizDataTable.Rows(0)

                                'assign wizarsstatus to Form variable WizardStatus 
                                WizardStatus = EnquiryWizDataRow.Item(0)
                                'get EnquiryId
                                'EnquiryIdValue = EnquiryWizDataRow.Item(1)

                                ' clear the dataTable and the Connect information
                                EnquiryWizDataAdapter = Nothing
                                EnquiryWizDataTable.Clear()
                            End If

                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try

                        Select Case WizardStatus
                            Case Is >= MyEnums.WizardStatus.AppForm
                                'launch  Standard Worksheet Form

                                Dim EnquiryForm As New AppForm
                                Try
                                    EnquiryForm.PassVariable(enquiryIdValue)
                                    EnquiryForm.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Case Is = MyEnums.WizardStatus.Preliminary
                                'launch PreliminaryForm and pass Enquiry Id
                                Dim OldEnquiryForm As New PreliminaryForm
                                Try
                                    OldEnquiryForm.PassVariable(enquiryIdValue)
                                    OldEnquiryForm.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Case Is = MyEnums.WizardStatus.FollowUp
                                'launch FollowUpForm and pass Enquiry Id
                                Dim FollowUpFrm1 As New FollowUpForm
                                Try
                                    FollowUpFrm1.PassVariable(enquiryIdValue)
                                    FollowUpFrm1.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Case Is = MyEnums.WizardStatus.WizForm1
                                'launch WkShtWizForm1 and pass Enquiry Id
                                Dim WkShtWizFrm1 As New WkShtWizForm1
                                Try
                                    WkShtWizFrm1.PassVariable(enquiryIdValue)
                                    WkShtWizFrm1.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Case Is = MyEnums.WizardStatus.WizForm2
                                'launch WkShtWizForm2 and pass Enquiry Id
                                Dim WkShtWizFrm2 As New WkShtWizForm2
                                Try
                                    WkShtWizFrm2.PassVariable(enquiryIdValue)
                                    WkShtWizFrm2.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Case Is = MyEnums.WizardStatus.WizForm3
                                'launch WkShtWizForm3 and pass Enquiry Id
                                Dim WkShtWizFrm3 As New WkShtWizForm3
                                Try
                                    WkShtWizFrm3.PassVariable(enquiryIdValue)
                                    WkShtWizFrm3.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Case Is = MyEnums.WizardStatus.WizForm4
                                'launch WkShtWizForm4 and pass Enquiry Id
                                Dim WkShtWizFrm4 As New WkShtWizForm4
                                Try
                                    WkShtWizFrm4.PassVariable(enquiryIdValue)
                                    WkShtWizFrm4.Show()
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                                'Case Is = 6
                                '    'launch WkShtWizForm5 and pass Enquiry Id
                                '    Dim WkShtWizFrm5 As New WkShtWizForm5
                                '    Try
                                '        WkShtWizFrm5.PassVariable(enquiryIdValue)
                                '        WkShtWizFrm5.Show()
                                '    Catch ex As Exception
                                '        MsgBox(ex.Message)
                                '    End Try

                        End Select

                    End If


                End If
            Else
                ' Perform some other action.
                MsgBox("No Data Row selected, Please select a Data Row!")
            End If
            'reset the cursor
            Cursor.Current = Cursors.Default

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DisplayAllNamesForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            NewLocX = 0
        Else
            NewLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            NewLocY = 0
        Else
            NewLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.DisplayAllNamesLocation = New Point(NewLocX, NewLocY)
        My.Settings.DisplayAllNamesSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
    End Sub

    Private Sub DisplayAllNamesForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            DisplayAllNamesDataGridView.Size = New Size(formWidth - 16, formHeight - 127)
        End If
    End Sub



    'Private Sub DisplayAllNamesDataGridView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DisplayAllNamesDataGridView.CellFormatting
    '    If DisplayAllNamesDataGridView.Columns(e.ColumnIndex).Name = "Archived" Then
    '        Dim row As DataGridViewRow = DisplayAllNamesDataGridView.CurrentRow
    '        Dim cell11 As DataGridViewCell = row.Cells.Item("IsArchived")
    '        If cell11.Value = True Then
    '            e.Value = True
    '        End If
    '    End If

    'End Sub


End Class