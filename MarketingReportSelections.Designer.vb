﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MarketingEnquiryReportSelectionsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MarketingEnquiryReportSelectionsForm))
        Me.gpbxdateRange = New System.Windows.Forms.GroupBox()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.gpbxActiveStatus = New System.Windows.Forms.GroupBox()
        Me.rbtnBoth = New System.Windows.Forms.RadioButton()
        Me.rbtnActive = New System.Windows.Forms.RadioButton()
        Me.gpbxSorting = New System.Windows.Forms.GroupBox()
        Me.cmbxOrderBy = New System.Windows.Forms.ComboBox()
        Me.lblOrderBy = New System.Windows.Forms.Label()
        Me.cmbxGroupBy = New System.Windows.Forms.ComboBox()
        Me.lblGroupBy = New System.Windows.Forms.Label()
        Me.gpbxFilter = New System.Windows.Forms.GroupBox()
        Me.ckbxLoanType = New System.Windows.Forms.CheckBox()
        Me.lstbxLoanType = New System.Windows.Forms.ListBox()
        Me.PrelimReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.ckbxLoanPurpose = New System.Windows.Forms.CheckBox()
        Me.lstbxLoanPurpose = New System.Windows.Forms.ListBox()
        Me.LoanPurposeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ckbxSource = New System.Windows.Forms.CheckBox()
        Me.lstbxSource = New System.Windows.Forms.ListBox()
        Me.PrelimSourceBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.LoanPurposeTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanPurposeTableAdapter()
        Me.PrelimSourceTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimSourceTableAdapter()
        Me.PrelimReasonsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter()
        Me.gpbxdateRange.SuspendLayout()
        Me.gpbxActiveStatus.SuspendLayout()
        Me.gpbxSorting.SuspendLayout()
        Me.gpbxFilter.SuspendLayout()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoanPurposeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimSourceBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbxdateRange
        '
        Me.gpbxdateRange.Controls.Add(Me.DateTimePicker2)
        Me.gpbxdateRange.Controls.Add(Me.lblEndDate)
        Me.gpbxdateRange.Controls.Add(Me.DateTimePicker1)
        Me.gpbxdateRange.Controls.Add(Me.lblStartDate)
        Me.gpbxdateRange.Location = New System.Drawing.Point(12, 12)
        Me.gpbxdateRange.Name = "gpbxdateRange"
        Me.gpbxdateRange.Size = New System.Drawing.Size(410, 58)
        Me.gpbxdateRange.TabIndex = 0
        Me.gpbxdateRange.TabStop = False
        Me.gpbxdateRange.Text = "Date Range"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "d/m/yyyy"
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(287, 22)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 3
        '
        'lblEndDate
        '
        Me.lblEndDate.AutoSize = True
        Me.lblEndDate.Location = New System.Drawing.Point(229, 25)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(52, 13)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "d/m/yyyy"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(67, 22)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 1
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Location = New System.Drawing.Point(6, 25)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(55, 13)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        '
        'gpbxActiveStatus
        '
        Me.gpbxActiveStatus.Controls.Add(Me.rbtnBoth)
        Me.gpbxActiveStatus.Controls.Add(Me.rbtnActive)
        Me.gpbxActiveStatus.Enabled = False
        Me.gpbxActiveStatus.Location = New System.Drawing.Point(12, 76)
        Me.gpbxActiveStatus.Name = "gpbxActiveStatus"
        Me.gpbxActiveStatus.Size = New System.Drawing.Size(410, 50)
        Me.gpbxActiveStatus.TabIndex = 1
        Me.gpbxActiveStatus.TabStop = False
        Me.gpbxActiveStatus.Text = "Enquiries"
        '
        'rbtnBoth
        '
        Me.rbtnBoth.AutoSize = True
        Me.rbtnBoth.Checked = True
        Me.rbtnBoth.Location = New System.Drawing.Point(232, 19)
        Me.rbtnBoth.Name = "rbtnBoth"
        Me.rbtnBoth.Size = New System.Drawing.Size(104, 17)
        Me.rbtnBoth.TabIndex = 2
        Me.rbtnBoth.TabStop = True
        Me.rbtnBoth.Text = "Include Archives"
        Me.rbtnBoth.UseVisualStyleBackColor = True
        '
        'rbtnActive
        '
        Me.rbtnActive.AutoSize = True
        Me.rbtnActive.Location = New System.Drawing.Point(9, 19)
        Me.rbtnActive.Name = "rbtnActive"
        Me.rbtnActive.Size = New System.Drawing.Size(59, 17)
        Me.rbtnActive.TabIndex = 0
        Me.rbtnActive.TabStop = True
        Me.rbtnActive.Text = "Current"
        Me.rbtnActive.UseVisualStyleBackColor = True
        '
        'gpbxSorting
        '
        Me.gpbxSorting.Controls.Add(Me.cmbxOrderBy)
        Me.gpbxSorting.Controls.Add(Me.lblOrderBy)
        Me.gpbxSorting.Controls.Add(Me.cmbxGroupBy)
        Me.gpbxSorting.Controls.Add(Me.lblGroupBy)
        Me.gpbxSorting.Location = New System.Drawing.Point(12, 132)
        Me.gpbxSorting.Name = "gpbxSorting"
        Me.gpbxSorting.Size = New System.Drawing.Size(410, 85)
        Me.gpbxSorting.TabIndex = 2
        Me.gpbxSorting.TabStop = False
        Me.gpbxSorting.Text = "Sorting"
        '
        'cmbxOrderBy
        '
        Me.cmbxOrderBy.FormattingEnabled = True
        Me.cmbxOrderBy.Items.AddRange(New Object() {"None", "Current Status", "Date", "Dealer", "Enquiry Code", "Loan Amount", "Source", "Type of Loan", "Manager", "Loan Purpose"})
        Me.cmbxOrderBy.Location = New System.Drawing.Point(107, 49)
        Me.cmbxOrderBy.Name = "cmbxOrderBy"
        Me.cmbxOrderBy.Size = New System.Drawing.Size(292, 21)
        Me.cmbxOrderBy.TabIndex = 3
        '
        'lblOrderBy
        '
        Me.lblOrderBy.AutoSize = True
        Me.lblOrderBy.Location = New System.Drawing.Point(6, 52)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(48, 13)
        Me.lblOrderBy.TabIndex = 2
        Me.lblOrderBy.Text = "Order By"
        '
        'cmbxGroupBy
        '
        Me.cmbxGroupBy.FormattingEnabled = True
        Me.cmbxGroupBy.Items.AddRange(New Object() {"None", "Manager", "Source", "Current Status", "Dealer", "Type of Loan", "Loan Purpose"})
        Me.cmbxGroupBy.Location = New System.Drawing.Point(107, 22)
        Me.cmbxGroupBy.Name = "cmbxGroupBy"
        Me.cmbxGroupBy.Size = New System.Drawing.Size(292, 21)
        Me.cmbxGroupBy.TabIndex = 1
        '
        'lblGroupBy
        '
        Me.lblGroupBy.AutoSize = True
        Me.lblGroupBy.Location = New System.Drawing.Point(6, 25)
        Me.lblGroupBy.Name = "lblGroupBy"
        Me.lblGroupBy.Size = New System.Drawing.Size(51, 13)
        Me.lblGroupBy.TabIndex = 0
        Me.lblGroupBy.Text = "Group By"
        '
        'gpbxFilter
        '
        Me.gpbxFilter.Controls.Add(Me.ckbxLoanType)
        Me.gpbxFilter.Controls.Add(Me.lstbxLoanType)
        Me.gpbxFilter.Controls.Add(Me.ckbxLoanPurpose)
        Me.gpbxFilter.Controls.Add(Me.lstbxLoanPurpose)
        Me.gpbxFilter.Controls.Add(Me.ckbxSource)
        Me.gpbxFilter.Controls.Add(Me.lstbxSource)
        Me.gpbxFilter.Location = New System.Drawing.Point(12, 223)
        Me.gpbxFilter.Name = "gpbxFilter"
        Me.gpbxFilter.Size = New System.Drawing.Size(410, 433)
        Me.gpbxFilter.TabIndex = 3
        Me.gpbxFilter.TabStop = False
        Me.gpbxFilter.Text = "Filter Information"
        '
        'ckbxLoanType
        '
        Me.ckbxLoanType.AutoSize = True
        Me.ckbxLoanType.Location = New System.Drawing.Point(10, 291)
        Me.ckbxLoanType.Name = "ckbxLoanType"
        Me.ckbxLoanType.Size = New System.Drawing.Size(89, 17)
        Me.ckbxLoanType.TabIndex = 7
        Me.ckbxLoanType.Text = "Type of Loan"
        Me.ckbxLoanType.UseVisualStyleBackColor = True
        '
        'lstbxLoanType
        '
        Me.lstbxLoanType.ColumnWidth = 190
        Me.lstbxLoanType.Enabled = False
        Me.lstbxLoanType.FormattingEnabled = True
        Me.lstbxLoanType.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.lstbxLoanType.Location = New System.Drawing.Point(10, 314)
        Me.lstbxLoanType.MultiColumn = True
        Me.lstbxLoanType.Name = "lstbxLoanType"
        Me.lstbxLoanType.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstbxLoanType.Size = New System.Drawing.Size(390, 108)
        Me.lstbxLoanType.TabIndex = 6
        '
        'PrelimReasonsBindingSource
        '
        Me.PrelimReasonsBindingSource.DataMember = "PrelimReasons"
        Me.PrelimReasonsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ckbxLoanPurpose
        '
        Me.ckbxLoanPurpose.AutoSize = True
        Me.ckbxLoanPurpose.Location = New System.Drawing.Point(9, 155)
        Me.ckbxLoanPurpose.Name = "ckbxLoanPurpose"
        Me.ckbxLoanPurpose.Size = New System.Drawing.Size(92, 17)
        Me.ckbxLoanPurpose.TabIndex = 5
        Me.ckbxLoanPurpose.Text = "Loan Purpose"
        Me.ckbxLoanPurpose.UseVisualStyleBackColor = True
        '
        'lstbxLoanPurpose
        '
        Me.lstbxLoanPurpose.ColumnWidth = 190
        Me.lstbxLoanPurpose.DataSource = Me.LoanPurposeBindingSource
        Me.lstbxLoanPurpose.DisplayMember = "LoanPurpose"
        Me.lstbxLoanPurpose.Enabled = False
        Me.lstbxLoanPurpose.FormattingEnabled = True
        Me.lstbxLoanPurpose.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.lstbxLoanPurpose.Location = New System.Drawing.Point(9, 178)
        Me.lstbxLoanPurpose.MultiColumn = True
        Me.lstbxLoanPurpose.Name = "lstbxLoanPurpose"
        Me.lstbxLoanPurpose.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstbxLoanPurpose.Size = New System.Drawing.Size(390, 108)
        Me.lstbxLoanPurpose.TabIndex = 4
        Me.lstbxLoanPurpose.ValueMember = "LoanPurpose"
        '
        'LoanPurposeBindingSource
        '
        Me.LoanPurposeBindingSource.DataMember = "LoanPurpose"
        Me.LoanPurposeBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'ckbxSource
        '
        Me.ckbxSource.AutoSize = True
        Me.ckbxSource.Location = New System.Drawing.Point(9, 19)
        Me.ckbxSource.Name = "ckbxSource"
        Me.ckbxSource.Size = New System.Drawing.Size(60, 17)
        Me.ckbxSource.TabIndex = 3
        Me.ckbxSource.Text = "Source"
        Me.ckbxSource.UseVisualStyleBackColor = True
        '
        'lstbxSource
        '
        Me.lstbxSource.ColumnWidth = 190
        Me.lstbxSource.DataSource = Me.PrelimSourceBindingSource
        Me.lstbxSource.DisplayMember = "PrelimSource"
        Me.lstbxSource.Enabled = False
        Me.lstbxSource.FormattingEnabled = True
        Me.lstbxSource.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.lstbxSource.Location = New System.Drawing.Point(9, 42)
        Me.lstbxSource.MultiColumn = True
        Me.lstbxSource.Name = "lstbxSource"
        Me.lstbxSource.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstbxSource.Size = New System.Drawing.Size(390, 108)
        Me.lstbxSource.TabIndex = 2
        Me.lstbxSource.ValueMember = "PrelimSource"
        '
        'PrelimSourceBindingSource
        '
        Me.PrelimSourceBindingSource.DataMember = "PrelimSource"
        Me.PrelimSourceBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(22, 662)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 4
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(336, 662)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 689)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(442, 22)
        Me.StatusStripMessage.TabIndex = 6
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(83, 17)
        Me.ToolStripStatusLabel1.Text = "Status:  Ready."
        '
        'LoanPurposeTableAdapter
        '
        Me.LoanPurposeTableAdapter.ClearBeforeFill = True
        '
        'PrelimSourceTableAdapter
        '
        Me.PrelimSourceTableAdapter.ClearBeforeFill = True
        '
        'PrelimReasonsTableAdapter
        '
        Me.PrelimReasonsTableAdapter.ClearBeforeFill = True
        '
        'MarketingEnquiryReportSelectionsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(442, 711)
        Me.ControlBox = False
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.gpbxFilter)
        Me.Controls.Add(Me.gpbxSorting)
        Me.Controls.Add(Me.gpbxActiveStatus)
        Me.Controls.Add(Me.gpbxdateRange)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MarketingEnquiryReportSelectionsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enquiry Report Selections"
        Me.gpbxdateRange.ResumeLayout(False)
        Me.gpbxdateRange.PerformLayout()
        Me.gpbxActiveStatus.ResumeLayout(False)
        Me.gpbxActiveStatus.PerformLayout()
        Me.gpbxSorting.ResumeLayout(False)
        Me.gpbxSorting.PerformLayout()
        Me.gpbxFilter.ResumeLayout(False)
        Me.gpbxFilter.PerformLayout()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoanPurposeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimSourceBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gpbxdateRange As System.Windows.Forms.GroupBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents gpbxActiveStatus As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnBoth As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnActive As System.Windows.Forms.RadioButton
    Friend WithEvents gpbxSorting As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxGroupBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroupBy As System.Windows.Forms.Label
    Friend WithEvents cmbxOrderBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents gpbxFilter As System.Windows.Forms.GroupBox
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents LoanPurposeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents lstbxSource As System.Windows.Forms.ListBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ckbxSource As System.Windows.Forms.CheckBox
    Friend WithEvents LoanPurposeTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanPurposeTableAdapter
    Friend WithEvents ckbxLoanPurpose As System.Windows.Forms.CheckBox
    Friend WithEvents lstbxLoanPurpose As System.Windows.Forms.ListBox
    Friend WithEvents PrelimSourceBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimSourceTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimSourceTableAdapter
    Friend WithEvents ckbxLoanType As System.Windows.Forms.CheckBox
    Friend WithEvents lstbxLoanType As System.Windows.Forms.ListBox
    Friend WithEvents PrelimReasonsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimReasonsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter
End Class
