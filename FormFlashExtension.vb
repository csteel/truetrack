﻿Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Module FormFlashExtension

    Declare Function FlashWindowEx Lib "user32.dll" (ByRef pInfo As FlashWindowInfo) As Boolean

    Enum FlashOptions As UInteger
        ''' <summary>
        ''' Flash both the window caption and taskbar button. 
        ''' </summary>
        ALL = &H3

        ''' <summary>
        ''' Flash the window caption.
        ''' </summary>
        CAPTION = &H1

        ''' <summary>
        ''' Stop flashing. The system restores the window to its original state. 
        ''' </summary>
        [STOP] = 0

        ''' <summary>
        ''' Flash continuously, until the FLASHW_STOP flag is set. 
        ''' </summary>
        TIMER = &H4

        ''' <summary>
        ''' Flash continuously until the window comes to the foreground. 
        ''' </summary>
        TIMERNOFG = &HC

        ''' <summary>
        ''' Flash the taskbar button.
        ''' </summary>
        TRAY = &H2
    End Enum

    Structure FlashWindowInfo
        Public cbSize As Integer
        Public hwnd As IntPtr
        Public dwFlags As UInteger
        Public uCount As UInteger
        Public dwTimeout As UInteger
    End Structure

    <Extension()> _
    Public Sub FlashWindow(ByVal FormToFlash As Form, ByVal Options As FlashOptions, ByVal FlashIfFocused As Boolean, Optional ByVal FlashTimes As UInteger = 5)
        If Not FlashIfFocused AndAlso FormToFlash.ContainsFocus Then Exit Sub

        Dim info As FlashWindowInfo
        With info
            .cbSize = Marshal.SizeOf(info)
            .dwFlags = Options ' See enumeration for flag values 
            .dwTimeout = 0 'Flash rate in ms or default cursor blink rate 
            .hwnd = FormToFlash.Handle()
            .uCount = FlashTimes ' Number of times to flash 
        End With
        FlashWindowEx(info)
    End Sub



End Module
