﻿Public Class AddBudgetExpenseItemForm
    ' set New Enquiry Id 
    Dim ThisEnquiryId As Integer
    'set BudgetId
    Dim ThisBudgetId As Integer
    Dim recordSaveSwitch As Boolean = False
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)


    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer, Optional ByVal LoadBudgetVar As Integer = 0)
        ThisEnquiryId = LoadEnquiryVar
        ThisBudgetId = LoadBudgetVar

        Try
            If ThisBudgetId <> 0 Then
                Me.BudgetTableAdapter.FillByBudgetId(Me.EnquiryWorkSheetDataSet.Budget, ThisBudgetId)
                btnAddBudgetItem.Visible = False
                btnUpdate.Visible = True
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        '*************** Set permissions
        Select Case LoggedinPermissionLevel
            Case Is < 3 'Base level, Can take enquiries add comments to current enquiries.
                '--------------------------------------- < 3
                txtbxDescription.IsReadOnly = True
                txtbxBudgetAmount.ReadOnly = True
                cmbxPeriod.Enabled = False
                txtbxComments.IsReadOnly = True
                btnAddBudgetItem.Enabled = False
                btnUpdate.Enabled = False
            Case Else '>= 3

        End Select
        '*************** end of Set permissions

    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.BudgetBindingSource.CancelEdit()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAddBudgetItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBudgetItem.Click
        'check field entry
        Dim checkFieldsMessage As String = ""
        Try
            If txtbxDescription.Text = "" Then
                checkFieldsMessage = "Please enter a Description" + vbCrLf
            End If
            If txtbxBudgetAmount.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter an Amount" + vbCrLf
            End If
            If cmbxPeriod.SelectedItem Is Nothing Then
                checkFieldsMessage = checkFieldsMessage + "Please select a Payment Period" + vbCrLf
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
        Else
            Try
                Me.Validate()
                Me.BudgetBindingSource.EndEdit()
                Me.BudgetTableAdapter.InsertWithEnquiryId(ThisEnquiryId, txtbxDescription.Text, 0, lblMonthlyValue.Text, txtbxComments.Text)
                recordSaveSwitch = True
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'check field entry
        Dim checkFieldsMessage As String = ""
        Try
            If txtbxDescription.Text = "" Then
                checkFieldsMessage = "Please enter a Description" + vbCrLf
            End If
            If txtbxBudgetAmount.Text = "" Then
                checkFieldsMessage = checkFieldsMessage + "Please enter an Amount" + vbCrLf
            End If
            If cmbxPeriod.SelectedItem Is Nothing Then
                checkFieldsMessage = checkFieldsMessage + "Please select a Payment Period" + vbCrLf
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
        Else
            Try
                Me.Validate()
                Me.BudgetBindingSource.EndEdit()
                Me.BudgetTableAdapter.Update(EnquiryWorkSheetDataSet.Budget)
                recordSaveSwitch = True
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        End If
    End Sub


    Private Sub txtbxBudgetAmount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxBudgetAmount.TextChanged
        CalcMonthlyValue()
    End Sub

    Private Sub CalcMonthlyValue()
        Dim BudgetItemAmount As Decimal = CDec(txtbxBudgetAmount.Text)
        Dim Period As String = cmbxPeriod.SelectedItem
        Dim MonthlyValue As Decimal

        Select Case Period
            Case "weekly"
                MonthlyValue = BudgetItemAmount * 52 / 12
            Case "fortnightly"
                MonthlyValue = BudgetItemAmount * 26 / 12
            Case "4 weekly"
                MonthlyValue = BudgetItemAmount * 52 / 48
            Case "monthly"
                MonthlyValue = BudgetItemAmount
            Case Else
                MonthlyValue = 0
        End Select
        lblMonthlyValue.Text = MonthlyValue.ToString("C")
    End Sub

    Private Sub cmbxPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxPeriod.SelectedIndexChanged
        CalcMonthlyValue()
    End Sub


End Class