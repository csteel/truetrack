﻿Public Class DisplayFinPowerNamesForm

    Dim NewLocX As Integer
    Dim NewLocY As Integer
    ''' <summary>
    ''' Check Names from finPower database (DisplayFinPowerNames table)
    ''' </summary>
    ''' <param name="lastNameLike"></param>
    ''' <param name="displayNameLike"></param>
    ''' <remarks>Returns all names with and without apostrophe if the searched name has an apostrophe</remarks>
    Friend Sub PassVariable(Optional ByVal lastNameLike As String = "", Optional ByVal displayNameLike As String = "")
        Cursor.Current = Cursors.WaitCursor
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.finPowerNames' table.
            Me.FinPowerNamesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.finPowerNames)
        Catch ex As Exception
            MsgBox("Filling DisplayFinPowerNames Datatable caused an error:" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
        Dim lastNameLikeMinus As String
        Dim displayNameLikeMinus As String
        'set filter
        'filter with and without apostrophes
        If lastNameLike.Length > 0 Then
            If displayNameLike.Length > 0 Then
                txtbxLastNameFilter.Text = lastNameLike
                txtbxDisplayNameFilter.Text = displayNameLike
                txtLegalNameFilter.Text = displayNameLike
                If lastNameLike.IndexOf("'") > -1 And displayNameLike.IndexOf("'") > -1 Then
                    lastNameLikeMinus = lastNameLike.Replace("'", "")
                    displayNameLikeMinus = displayNameLike.Replace("'", "")
                    FinPowerNamesBindingSource.Filter = "LastName like '" & lastNameLike.Replace("'", "''") & "%' OR LastName like '" & lastNameLikeMinus & "%' OR LegalName like '" & displayNameLike.Replace("'", "''") & "%' OR LegalName like '" & displayNameLikeMinus & "%' OR Name like '%" & displayNameLike.Replace("'", "''") & "%' OR Name like '%" & displayNameLikeMinus & "%'"
                ElseIf lastNameLike.IndexOf("'") > -1 Then
                    lastNameLikeMinus = lastNameLike.Replace("'", "")
                    FinPowerNamesBindingSource.Filter = "LastName like '" & lastNameLike.Replace("'", "''") & "%' OR LastName like '" & lastNameLikeMinus & "%' OR LegalName like '" & displayNameLike.Replace("'", "''") & "%' OR Name like '%" & displayNameLike.Replace("'", "''") & "%'"
                Else
                    FinPowerNamesBindingSource.Filter = "LastName like '" & lastNameLike.Replace("'", "''") & "%' OR LegalName like '" & displayNameLike.Replace("'", "''") & "%' OR Name like '%" & displayNameLike.Replace("'", "''") & "%'"
                End If
            Else
                txtbxLastNameFilter.Text = lastNameLike
                If lastNameLike.IndexOf("'") > -1 Then
                    lastNameLikeMinus = lastNameLike.Replace("'", "")
                    FinPowerNamesBindingSource.Filter = "LastName like '" & lastNameLike.Replace("'", "''") & "%' OR LastName like '" & lastNameLikeMinus & "%'"
                Else
                    FinPowerNamesBindingSource.Filter = "LastName like '" & lastNameLike.Replace("'", "''") & "%'"
                End If
            End If
        End If

    End Sub

    Private Sub DisplayFinPowerNamesForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'get user settings
        Me.ClientSize = New Size(My.Settings.DisplayAllNamesSize)
        Me.Location = New Point(My.Settings.DisplayAllNamesLocation)
    End Sub

#Region "Filters"
    Private Sub txtbxDisplayNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxDisplayNameFilter.TextChanged
        FinPowerNamesBindingSource.Filter = "Name like '" & txtbxDisplayNameFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxLastNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxLastNameFilter.TextChanged
        FinPowerNamesBindingSource.Filter = "LastName like '" & txtbxLastNameFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxFirstNameFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxFirstNameFilter.TextChanged
        FinPowerNamesBindingSource.Filter = "FirstName like '" & txtbxFirstNameFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxSuburbFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxSuburbFilter.TextChanged
        FinPowerNamesBindingSource.Filter = "City like '" & txtbxSuburbFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtbxCityFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxCityFilter.TextChanged
        FinPowerNamesBindingSource.Filter = "State like '" & txtbxCityFilter.Text.Replace("'", "''") & "%'"
    End Sub

    Private Sub txtLegalNameFilter_TextChanged(sender As Object, e As EventArgs) Handles txtLegalNameFilter.TextChanged
        FinPowerNamesBindingSource.Filter = "LegalName like '%" & txtLegalNameFilter.Text.Replace("'", "''") & "%'"
    End Sub

#End Region

    Private Sub DisplayFinPowerNamesForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            NewLocX = 0
        Else
            NewLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            NewLocY = 0
        Else
            NewLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.DisplayAllNamesLocation = New Point(NewLocX, NewLocY)
        My.Settings.DisplayAllNamesSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
    End Sub

    Private Sub DisplayFinPowerNamesForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            FinPowerNamesDataGridView.Size = New Size(formWidth - 16, formHeight - 129)
        End If
    End Sub

    Private Sub FinPowerNamesDataGridView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles FinPowerNamesDataGridView.CellFormatting
        ' If the column is the CreditRating column, check the value.
        'If value is "bad" then change the row BackColor
        If Me.FinPowerNamesDataGridView.Columns(e.ColumnIndex).Index = 5 Then 'Index count starts at 0 (CreditRating column)
            If e.Value IsNot Nothing Then
                ' Check for the string "bad" in the cell.
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()
                If ((stringValue.IndexOf("bad") > -1)) Then
                    Me.FinPowerNamesDataGridView.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.MistyRose
                    'e.CellStyle.BackColor = Color.Pink
                End If

            End If
        End If

    End Sub


    
End Class