﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StatusReportSelectionsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StatusReportSelectionsForm))
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.gpbxFilter = New System.Windows.Forms.GroupBox()
        Me.ckbxUsers = New System.Windows.Forms.CheckBox()
        Me.lstbxUsers = New System.Windows.Forms.ListBox()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.gpbxSorting = New System.Windows.Forms.GroupBox()
        Me.cmbxOrderBy = New System.Windows.Forms.ComboBox()
        Me.lblOrderBy = New System.Windows.Forms.Label()
        Me.cmbxGroupBy = New System.Windows.Forms.ComboBox()
        Me.lblGroupBy = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lstbxStatus = New System.Windows.Forms.ListBox()
        Me.ckbxStatus = New System.Windows.Forms.CheckBox()
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.StatusStripMessage.SuspendLayout()
        Me.gpbxFilter.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxSorting.SuspendLayout()
        Me.gpbxStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 392)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(434, 22)
        Me.StatusStripMessage.TabIndex = 19
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(83, 17)
        Me.ToolStripStatusLabel1.Text = "Status:  Ready."
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(347, 366)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 18
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(12, 366)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 17
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'gpbxFilter
        '
        Me.gpbxFilter.Controls.Add(Me.ckbxUsers)
        Me.gpbxFilter.Controls.Add(Me.lstbxUsers)
        Me.gpbxFilter.Location = New System.Drawing.Point(12, 227)
        Me.gpbxFilter.Name = "gpbxFilter"
        Me.gpbxFilter.Size = New System.Drawing.Size(410, 133)
        Me.gpbxFilter.TabIndex = 16
        Me.gpbxFilter.TabStop = False
        Me.gpbxFilter.Text = "Filter Information"
        '
        'ckbxUsers
        '
        Me.ckbxUsers.AutoSize = True
        Me.ckbxUsers.Location = New System.Drawing.Point(9, 19)
        Me.ckbxUsers.Name = "ckbxUsers"
        Me.ckbxUsers.Size = New System.Drawing.Size(73, 17)
        Me.ckbxUsers.TabIndex = 3
        Me.ckbxUsers.Text = "Managers"
        Me.ckbxUsers.UseVisualStyleBackColor = True
        '
        'lstbxUsers
        '
        Me.lstbxUsers.DataSource = Me.UsersBindingSource
        Me.lstbxUsers.DisplayMember = "UserName"
        Me.lstbxUsers.Enabled = False
        Me.lstbxUsers.FormattingEnabled = True
        Me.lstbxUsers.Location = New System.Drawing.Point(96, 22)
        Me.lstbxUsers.MultiColumn = True
        Me.lstbxUsers.Name = "lstbxUsers"
        Me.lstbxUsers.Size = New System.Drawing.Size(303, 95)
        Me.lstbxUsers.TabIndex = 2
        Me.lstbxUsers.ValueMember = "UserName"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'gpbxSorting
        '
        Me.gpbxSorting.Controls.Add(Me.cmbxOrderBy)
        Me.gpbxSorting.Controls.Add(Me.lblOrderBy)
        Me.gpbxSorting.Controls.Add(Me.cmbxGroupBy)
        Me.gpbxSorting.Controls.Add(Me.lblGroupBy)
        Me.gpbxSorting.Location = New System.Drawing.Point(12, 136)
        Me.gpbxSorting.Name = "gpbxSorting"
        Me.gpbxSorting.Size = New System.Drawing.Size(410, 85)
        Me.gpbxSorting.TabIndex = 15
        Me.gpbxSorting.TabStop = False
        Me.gpbxSorting.Text = "Sorting"
        '
        'cmbxOrderBy
        '
        Me.cmbxOrderBy.FormattingEnabled = True
        Me.cmbxOrderBy.Items.AddRange(New Object() {"None", "Manager", "Current Status", "Status Date", "Enquiry Date", "Type of Loan", "Loan Amount", "Dealer", "Enquiry Code"})
        Me.cmbxOrderBy.Location = New System.Drawing.Point(96, 49)
        Me.cmbxOrderBy.Name = "cmbxOrderBy"
        Me.cmbxOrderBy.Size = New System.Drawing.Size(303, 21)
        Me.cmbxOrderBy.TabIndex = 3
        '
        'lblOrderBy
        '
        Me.lblOrderBy.AutoSize = True
        Me.lblOrderBy.Location = New System.Drawing.Point(6, 52)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(48, 13)
        Me.lblOrderBy.TabIndex = 2
        Me.lblOrderBy.Text = "Order By"
        '
        'cmbxGroupBy
        '
        Me.cmbxGroupBy.FormattingEnabled = True
        Me.cmbxGroupBy.Items.AddRange(New Object() {"None", "Manager", "Current Status", "Type of Loan", "Dealer"})
        Me.cmbxGroupBy.Location = New System.Drawing.Point(96, 22)
        Me.cmbxGroupBy.Name = "cmbxGroupBy"
        Me.cmbxGroupBy.Size = New System.Drawing.Size(303, 21)
        Me.cmbxGroupBy.TabIndex = 1
        '
        'lblGroupBy
        '
        Me.lblGroupBy.AutoSize = True
        Me.lblGroupBy.Location = New System.Drawing.Point(6, 25)
        Me.lblGroupBy.Name = "lblGroupBy"
        Me.lblGroupBy.Size = New System.Drawing.Size(51, 13)
        Me.lblGroupBy.TabIndex = 0
        Me.lblGroupBy.Text = "Group By"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Controls.Add(Me.lstbxStatus)
        Me.gpbxStatus.Controls.Add(Me.ckbxStatus)
        Me.gpbxStatus.Location = New System.Drawing.Point(12, 3)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(410, 127)
        Me.gpbxStatus.TabIndex = 14
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Filter Current Status for Current Enquiries"
        '
        'lstbxStatus
        '
        Me.lstbxStatus.Enabled = False
        Me.lstbxStatus.FormattingEnabled = True
        Me.lstbxStatus.Items.AddRange(New Object() {"Follow-up", "Enquiry Ended", "Due Diligence", "CAD Approved", "Audit", "Payout", "Completed", "Exported", "Declined", "Withdrawn", "Audit failed"})
        Me.lstbxStatus.Location = New System.Drawing.Point(96, 19)
        Me.lstbxStatus.MultiColumn = True
        Me.lstbxStatus.Name = "lstbxStatus"
        Me.lstbxStatus.Size = New System.Drawing.Size(303, 95)
        Me.lstbxStatus.TabIndex = 1
        '
        'ckbxStatus
        '
        Me.ckbxStatus.AutoSize = True
        Me.ckbxStatus.Location = New System.Drawing.Point(9, 19)
        Me.ckbxStatus.Name = "ckbxStatus"
        Me.ckbxStatus.Size = New System.Drawing.Size(56, 17)
        Me.ckbxStatus.TabIndex = 0
        Me.ckbxStatus.Text = "Status"
        Me.ckbxStatus.UseVisualStyleBackColor = True
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'StatusReportSelectionsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 414)
        Me.ControlBox = False
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.gpbxFilter)
        Me.Controls.Add(Me.gpbxSorting)
        Me.Controls.Add(Me.gpbxStatus)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "StatusReportSelectionsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Status Report Selections"
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.gpbxFilter.ResumeLayout(False)
        Me.gpbxFilter.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxSorting.ResumeLayout(False)
        Me.gpbxSorting.PerformLayout()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents gpbxFilter As System.Windows.Forms.GroupBox
    Friend WithEvents ckbxUsers As System.Windows.Forms.CheckBox
    Friend WithEvents lstbxUsers As System.Windows.Forms.ListBox
    Friend WithEvents gpbxSorting As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxOrderBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents cmbxGroupBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroupBy As System.Windows.Forms.Label
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents lstbxStatus As System.Windows.Forms.ListBox
    Friend WithEvents ckbxStatus As System.Windows.Forms.CheckBox
End Class
