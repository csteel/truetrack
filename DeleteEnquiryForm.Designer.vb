﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeleteEnquiryForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DeleteEnquiryForm))
        Me.lblDeleteTitle = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.lblDeleteNote = New System.Windows.Forms.Label
        Me.txtbxEnquiryCode = New System.Windows.Forms.TextBox
        Me.lblDisplayName = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblDeleteTitle
        '
        Me.lblDeleteTitle.AutoSize = True
        Me.lblDeleteTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteTitle.Location = New System.Drawing.Point(164, 16)
        Me.lblDeleteTitle.Name = "lblDeleteTitle"
        Me.lblDeleteTitle.Size = New System.Drawing.Size(127, 20)
        Me.lblDeleteTitle.TabIndex = 35
        Me.lblDeleteTitle.Text = "Delete Enquiry"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(367, 178)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 40
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.LightGreen
        Me.btnDelete.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnDelete.Location = New System.Drawing.Point(12, 178)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 39
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        Me.btnDelete.Visible = False
        '
        'lblDeleteNote
        '
        Me.lblDeleteNote.AutoSize = True
        Me.lblDeleteNote.Location = New System.Drawing.Point(118, 60)
        Me.lblDeleteNote.Name = "lblDeleteNote"
        Me.lblDeleteNote.Size = New System.Drawing.Size(218, 13)
        Me.lblDeleteNote.TabIndex = 41
        Me.lblDeleteNote.Text = "Enter Enquiry Code  for enquiry to be deleted"
        '
        'txtbxEnquiryCode
        '
        Me.txtbxEnquiryCode.Location = New System.Drawing.Point(120, 83)
        Me.txtbxEnquiryCode.Name = "txtbxEnquiryCode"
        Me.txtbxEnquiryCode.Size = New System.Drawing.Size(215, 20)
        Me.txtbxEnquiryCode.TabIndex = 0
        Me.txtbxEnquiryCode.Text = "E"
        '
        'lblDisplayName
        '
        Me.lblDisplayName.AutoSize = True
        Me.lblDisplayName.Location = New System.Drawing.Point(192, 110)
        Me.lblDisplayName.Name = "lblDisplayName"
        Me.lblDisplayName.Size = New System.Drawing.Size(70, 13)
        Me.lblDisplayName.TabIndex = 42
        Me.lblDisplayName.Text = "Display name"
        Me.lblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DeleteEnquiryForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 214)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblDisplayName)
        Me.Controls.Add(Me.txtbxEnquiryCode)
        Me.Controls.Add(Me.lblDeleteNote)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblDeleteTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(470, 250)
        Me.MinimumSize = New System.Drawing.Size(470, 250)
        Me.Name = "DeleteEnquiryForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Delete Enquiry Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblDeleteTitle As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents lblDeleteNote As System.Windows.Forms.Label
    Friend WithEvents txtbxEnquiryCode As System.Windows.Forms.TextBox
    Friend WithEvents lblDisplayName As System.Windows.Forms.Label
End Class
