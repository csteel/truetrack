﻿Imports System.Globalization
Imports System.Text

Public Class EnquiryReportSelectionsForm
    Dim selectedrb As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim StartDate As Date
    Dim EndDate As Date
    Dim GroupBy As String
    Dim OrderBy As String
    Dim ArchiveStatus As String
    'Dim User As String = ""
    Dim strUser As New StringBuilder

    'Loads form with parameters from Form1
    Friend Sub ReportSelections_PassVariable(ByVal ReportTypeVar As String)
        'loads data into the 'EnquiryWorkSheetDataSet.Users' table. 
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)

        Select Case ReportTypeVar
            Case "Enquiry"
                Me.Text = Text + " - Enquiry"
                'set default values
                Me.cmbxGroupBy.SelectedIndex = 0
                Me.cmbxOrderBy.SelectedIndex = 0


            Case Else
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR: ReportTypeVar not recognised! Please try again."
        End Select


    End Sub

    Private Sub ReportSelectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Sub ckbxUsers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxUsers.CheckedChanged
        If ckbxUsers.CheckState = CheckState.Checked Then
            lstbxUsers.Enabled = True
        Else
            lstbxUsers.Enabled = False
        End If
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Cursor.Current = Cursors.WaitCursor
        'reset lists
        strUser.Clear()
        'get values
        StartDate = DateTimePicker1.Value.ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        EndDate = DateTimePicker2.Value.AddDays(1).ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))

        'get users if selected
        If lstbxUsers.Enabled = True Then
            If lstbxUsers.SelectedItems.Count > 0 Then
                Dim drv As DataRowView = Nothing
                Dim sep As String = ", "
                'DataRowView
                drv = lstbxUsers.SelectedItems(0)
                'build stringbuilder
                strUser.Append(drv("UserName").ToString)
                Dim i As Integer
                For i = 1 To lstbxUsers.SelectedItems.Count - 1
                    drv = lstbxUsers.SelectedItems(i)
                    strUser.Append(sep).Append(drv("UserName").ToString)
                Next i
            End If
        Else
            strUser.Clear()
        End If

        ''get users if selected
        'If lstbxUsers.Enabled = True Then
        '    User = lstbxUsers.SelectedValue.ToString
        '    'MsgBox("Selected User = " + User)
        'Else
        '    User = ""
        'End If

        'get Group By
        GroupBy = cmbxGroupBy.SelectedItem.ToString
        'get Order by
        OrderBy = cmbxOrderBy.SelectedItem.ToString
        'get ActiveStatus filter
        ArchiveStatus = selectedrb.Text
        'debug
        MsgBox("StartDate: " & StartDate & vbCrLf & "EndDate: " & EndDate & vbCrLf & "User: " & strUser.ToString() & vbCrLf & "GroupBy: " & GroupBy & vbCrLf & _
               "OrderBy: " & OrderBy & vbCrLf & "ArchiveStatus: " & ArchiveStatus)

        Try
            'launch ReportSelectionsForm and pass Enquiry parameters
            Dim EnquiryRptFrm As New ReportEnquiryForm
            EnquiryRptFrm.ReportSelections_PassVariable(StartDate, EndDate, GroupBy, OrderBy, strUser.ToString(), ArchiveStatus)
            EnquiryRptFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub rbtnNotActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    selectedrb = rb
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Approval = " & selectedrb.Text
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnActive.CheckedChanged
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    selectedrb = rb
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Enquiries = " & selectedrb.Text
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnBoth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnBoth.CheckedChanged
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    selectedrb = rb
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Enquiries = " & selectedrb.Text
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class