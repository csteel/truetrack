﻿//
// Copyright © 2008, Nathan B. Evans
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//
//   - Redistributions in binary form must reproduce the above copyright notice, 
//     this list of conditions and the following disclaimer in the documentation 
//     and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
// OF SUCH DAMAGE.
//

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Threading;

using AnimatedCircle.Controls.UI;

namespace AnimatedCircle
{

    /// <summary>
    /// Used for dialogs that need to indicate potentially long-duration asynchronous operations to the user.
    /// </summary>
    //[ToolboxItem(false)]
    public class AsyncBaseAnimation : Form {
       public Panel externalPanel { get; set; }


        #region Private Fields
        //private FlickerFreePanel asyncPanel = new FlickerFreePanel();
        public LoadingCircle barberPole = new LoadingCircle();
        private int reference_count = 0;

        //private SolidBrush fillBrush = new SolidBrush(Color.FromArgb(225, SystemColors.Window));
        //private Bitmap background = null; 
        #endregion

        /// <summary>
        /// Determines if any asynchronous tasks are still running.
        /// </summary>
        protected bool IsAsyncBusy { get { return (reference_count > 0); } }



        #region Begin/End AsyncIndication Methods
        /// <summary>
        /// Begin indicating that an asynchronous operation is occuring.
        /// </summary>
        public void BeginAsyncIndication()
        {

             barberPole.Active = true;

            //
            // Increment reference count...
            reference_count++;
        }

        /// <summary>
        /// Ends the current asynchronous indication.
        /// </summary>
        public void EndAsyncIndication()
        {
            //
            // Decrement reference count if it is still > 0...
            if (IsAsyncBusy)
                reference_count--;

            //
            // If the reference count == 0 then stop the async indication...
            if (!IsAsyncBusy) {
                    barberPole.Active = false;
            }
        } 
        #endregion        


        public void OnLoad() {


            barberPole.BackColor = Color.Transparent;
            barberPole.Dock = DockStyle.Fill;
            barberPole.StylePreset = LoadingCircle.StylePresets.Custom;
            barberPole.SpokeThickness = 3;
            barberPole.InnerCircleRadius = 8;
            barberPole.OuterCircleRadius = 10;
            barberPole.NumberSpoke = 100;
            barberPole.Color = SystemColors.ControlDark;
            barberPole.RotationSpeed = 35;

           

            if (externalPanel != null)
            {
                externalPanel.Controls.Add(barberPole);
            }
  
        }



    }//class

}//namespace
