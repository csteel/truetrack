﻿//
// Copyright © 2008, Nathan B. Evans
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//
//   - Redistributions in binary form must reproduce the above copyright notice, 
//     this list of conditions and the following disclaimer in the documentation 
//     and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
// OF SUCH DAMAGE.
//

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
//using System.Drawing.Imaging;
using System.Threading;

using AnimatedCircle.Controls.UI;

namespace AnimatedCircle {
    public enum AnimatedCirclePanelAlign
    {
        Left,
        Right
    }


    /*
     * NOTES: To addan animation to winform
     * Initialize AsyncAnimation class and pass a panel to show to animation.
     * Size of the panel should be set for the animation image.
     * call the animation by calling the AsyncAnimation.StartAnimation
     * To end animation call AsyncAnimation.EndAnimation
     */

    /// <summary>
    /// Used for dialogs that need to indicate potentially long-duration asynchronous operations to the user.
    /// </summary>
    public class AsyncAnimation {
        public Panel animatedCirclePanel { get; set; }
        int _innerCircleRadius =0;
        int _outerCircleRadius =0;

        /// <summary>
        /// Constructor initialises class with Panel for animation circles 
        /// </summary>
        /// <param name="panel"></param>
        public AsyncAnimation(Panel panel)
        {
            animatedCirclePanel = panel;
            OnLoad();
        }

        /// <summary>
        /// Constructor initialises class with Panel for animation circles and Color of circle
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="animationColor"></param>
        public AsyncAnimation(Panel panel, Color animationColor)
        {
            animatedCirclePanel = panel;
            AnimationColor = animationColor;
            OnLoad();
        }


        /// <summary>
        /// Constructor initialises class with Panel for animation circles inner and outer circle radius and Color of circle
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="innerCircleRadius">radius of inner circle</param>
        /// <param name="outerCircleRadius">radius of outer circle</param>
        /// <param name="animationColor">color of animation circles</param>
        public AsyncAnimation(Panel panel, int innerCircleRadius, int outerCircleRadius, Color animationColor)
        {
            animatedCirclePanel = panel;
            InnerCircleRadius = innerCircleRadius;
            OuterCircleRadius = outerCircleRadius;
            AnimationColor = animationColor;
            OnLoad();
        }


        #region properties

        /// <summary>
        /// Sets the default animation circle radius as it is not set explicitly
        /// </summary>
        public void setDefaultAnimationCircleRadius()
        {
            int _circleDiameter = 0;
            int _circleRadius = 0;
            if (animatedCirclePanel.Width >= animatedCirclePanel.Height)
            {
                _circleDiameter = animatedCirclePanel.Height;
            }
            if (animatedCirclePanel.Height > animatedCirclePanel.Width)
            {
                _circleDiameter = animatedCirclePanel.Width;
            }
            _circleRadius = (int)Math.Floor((decimal)(_circleDiameter / 2));
            if (OuterCircleRadius <= 0 && InnerCircleRadius <= 0)
            {

                if (_circleRadius <= 4)
                {
                    OuterCircleRadius = _circleRadius;
                    InnerCircleRadius = 1;
                }
                if (_circleRadius > 4 && _circleRadius <= 40)
                {
                    OuterCircleRadius = (int)Math.Floor((decimal)(_circleRadius / 2));
                    InnerCircleRadius = OuterCircleRadius-2;
                }
                else
                {
                    OuterCircleRadius = 35;
                    InnerCircleRadius = 30;
                }
            }
            else
            {
                if (OuterCircleRadius > 0)
                {
                    if (OuterCircleRadius < 4)
                    {
                        InnerCircleRadius = 1;
                    }
                    else
                    {
                        InnerCircleRadius = OuterCircleRadius - 2;
                    }
                }
                else
                {
                    if (InnerCircleRadius > 0)
                    {
                        OuterCircleRadius = InnerCircleRadius + 2;
                    }
                }
            }

        }
        


        public Color AnimationColor { get; set; }

        public int InnerCircleRadius { 
                get{
                    if (_innerCircleRadius < 1)
                    {
                        if (animatedCirclePanel != null)
                        {
                            setDefaultAnimationCircleRadius();
                        }
                    }
                    return _innerCircleRadius;
                    }
                set{
                    _innerCircleRadius = value;
                }
        }

        public int OuterCircleRadius { 
                get{
                    if (_outerCircleRadius < 1)
                    {
                        if (animatedCirclePanel != null)
                        {
                            setDefaultAnimationCircleRadius();
                        }
                    }
                    return _outerCircleRadius;
                    }
                set{
                    _outerCircleRadius = value;
                }
        }
        #endregion



        /// <summary>
        /// Starts animation in the specified panel
        /// </summary>
        /// <param name="relativePosition">position next to this the panel will be displayed</param>
        /// <param name="animatedCirclePanelAlign">panel aligned to the position </param>
        public void StartAnimation(int relativePosition, AnimatedCirclePanelAlign animatedCirclePanelAlign)
        {
                animatedCirclePanel.Visible = true;
                if (animatedCirclePanelAlign == AnimatedCirclePanelAlign.Left)
                {
                    animatedCirclePanel.Left = relativePosition - animatedCirclePanel.Width - 2;
                }
                else
                {
                    animatedCirclePanel.Left = relativePosition  + 2;
                }
                BeginAsyncIndication();
        }

        /// <summary>
        /// Ends animation
        /// </summary>
        public void EndAnimation()
        {
            animatedCirclePanel.Visible = false;
            EndAsyncIndication();
        }

        #region Private Fields
        //private FlickerFreePanel asyncPanel = new FlickerFreePanel();
        public LoadingCircle loadingCircle = new LoadingCircle();
        private int reference_count = 0;

        //private SolidBrush fillBrush = new SolidBrush(Color.FromArgb(225, SystemColors.Window));
        //private Bitmap background = null; 
        #endregion

        /// <summary>
        /// Determines if any asynchronous tasks are still running.
        /// </summary>
        protected bool IsAsyncBusy { get { return (reference_count > 0); } }



        #region Begin/End AsyncIndication Methods
        /// <summary>
        /// Begin indicating that an asynchronous operation is occuring.
        /// </summary>
        public void BeginAsyncIndication()
        {

            loadingCircle.Active = true;

            //
            // Increment reference count...
            reference_count++;
        }

        /// <summary>
        /// Ends the current asynchronous indication.
        /// </summary>
        public void EndAsyncIndication()
        {
            //
            // Decrement reference count if it is still > 0...
            if (IsAsyncBusy)
                reference_count--;

            //
            // If the reference count == 0 then stop the async indication...
            if (!IsAsyncBusy)
            {
                loadingCircle.Active = false;
            }
        }
        #endregion

        #region CommentedOriginalCode
        /*
        public void OnLoad()
        {
            //loadingCircle.BackColor = Color.Transparent;
            //loadingCircle.Dock = DockStyle.Fill;
            //loadingCircle.StylePreset = LoadingCircle.StylePresets.Custom;
            //loadingCircle.SpokeThickness = 3;
            //loadingCircle.InnerCircleRadius = 8;
            //loadingCircle.OuterCircleRadius = 10;
            //loadingCircle.NumberSpoke = 100;
            //loadingCircle.Color = SystemColors.ControlDark;
            //loadingCircle.RotationSpeed = 35;
            //if (animatedCirclePanel != null)
            //{
            //    animatedCirclePanel.Controls.Add(loadingCircle);
            //}
            OnLoad(8, 10, SystemColors.ControlDark);
        }
        */
        #endregion

        /// <summary>
        /// initializes the animation
        /// </summary>
        public void OnLoad()
        {
            //AnimationColor = SystemColors.ControlDark;            
            loadingCircle.BackColor = Color.Transparent;
            loadingCircle.Dock = DockStyle.Fill;
            loadingCircle.StylePreset = LoadingCircle.StylePresets.Custom;
            loadingCircle.SpokeThickness = 1;
            loadingCircle.InnerCircleRadius =  7; // InnerCircleRadius; //
            loadingCircle.OuterCircleRadius = 9; // OuterCircleRadius; // 
            loadingCircle.NumberSpoke = 100;
            loadingCircle.Color = AnimationColor;
            loadingCircle.RotationSpeed = 35;

            if (animatedCirclePanel != null)
            {
                animatedCirclePanel.Controls.Add(loadingCircle);
            }
        }

    }//class

}//namespace
