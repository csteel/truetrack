﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeleteRecordForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DeleteRecordForm))
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.txtbxDeleteExplain = New System.Windows.Forms.TextBox
        Me.lblDeleteExplain = New System.Windows.Forms.Label
        Me.lblDeleteTitle = New System.Windows.Forms.Label
        Me.EnquiryTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Me.EnquiryBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet
        CType(Me.EnquiryBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(370, 180)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 38
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.LightGreen
        Me.btnDelete.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnDelete.Location = New System.Drawing.Point(15, 180)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 37
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'txtbxDeleteExplain
        '
        Me.txtbxDeleteExplain.Location = New System.Drawing.Point(15, 67)
        Me.txtbxDeleteExplain.Multiline = True
        Me.txtbxDeleteExplain.Name = "txtbxDeleteExplain"
        Me.txtbxDeleteExplain.Size = New System.Drawing.Size(430, 107)
        Me.txtbxDeleteExplain.TabIndex = 36
        '
        'lblDeleteExplain
        '
        Me.lblDeleteExplain.AutoSize = True
        Me.lblDeleteExplain.Location = New System.Drawing.Point(28, 49)
        Me.lblDeleteExplain.Name = "lblDeleteExplain"
        Me.lblDeleteExplain.Size = New System.Drawing.Size(250, 13)
        Me.lblDeleteExplain.TabIndex = 35
        Me.lblDeleteExplain.Text = "Please enter reason for deleting this Budget Record"
        '
        'lblDeleteTitle
        '
        Me.lblDeleteTitle.AutoSize = True
        Me.lblDeleteTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteTitle.Location = New System.Drawing.Point(96, 7)
        Me.lblDeleteTitle.Name = "lblDeleteTitle"
        Me.lblDeleteTitle.Size = New System.Drawing.Size(263, 20)
        Me.lblDeleteTitle.TabIndex = 34
        Me.lblDeleteTitle.Text = "Delete Budget Item Information"
        '
        'EnquiryTableAdapter1
        '
        Me.EnquiryTableAdapter1.ClearBeforeFill = True
        '
        'EnquiryBindingSource1
        '
        Me.EnquiryBindingSource1.DataMember = "Enquiry"
        Me.EnquiryBindingSource1.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DeleteRecordForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(464, 224)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.txtbxDeleteExplain)
        Me.Controls.Add(Me.lblDeleteExplain)
        Me.Controls.Add(Me.lblDeleteTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(470, 250)
        Me.MinimumSize = New System.Drawing.Size(470, 250)
        Me.Name = "DeleteRecordForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Delete Record Form"
        CType(Me.EnquiryBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents txtbxDeleteExplain As System.Windows.Forms.TextBox
    Friend WithEvents lblDeleteExplain As System.Windows.Forms.Label
    Friend WithEvents lblDeleteTitle As System.Windows.Forms.Label
    Friend WithEvents EnquiryTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
End Class
