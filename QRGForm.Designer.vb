﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QRGForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(QRGForm))
        Me.gpbxQuickRefGuide = New System.Windows.Forms.GroupBox
        Me.cbDealerAppShtReceived = New System.Windows.Forms.CheckBox
        Me.QRGListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet
        Me.cbDealerAppShtRequired = New System.Windows.Forms.CheckBox
        Me.lblDealerAppSht = New System.Windows.Forms.Label
        Me.cbWinzReceived = New System.Windows.Forms.CheckBox
        Me.cbWinzRequired = New System.Windows.Forms.CheckBox
        Me.lblWinz = New System.Windows.Forms.Label
        Me.cbDriverCkReceived = New System.Windows.Forms.CheckBox
        Me.cbDriverCkRequired = New System.Windows.Forms.CheckBox
        Me.lblDriverCk = New System.Windows.Forms.Label
        Me.lblQRGOverlay = New System.Windows.Forms.Label
        Me.cbAddressReceived = New System.Windows.Forms.CheckBox
        Me.cbAddressRequired = New System.Windows.Forms.CheckBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.cbGSTReceived = New System.Windows.Forms.CheckBox
        Me.cbGSTRequired = New System.Windows.Forms.CheckBox
        Me.lblGST = New System.Windows.Forms.Label
        Me.cbHomeReceived = New System.Windows.Forms.CheckBox
        Me.cbHomeRequired = New System.Windows.Forms.CheckBox
        Me.lblHome = New System.Windows.Forms.Label
        Me.cbCompanyReceived = New System.Windows.Forms.CheckBox
        Me.cbCompanyRequired = New System.Windows.Forms.CheckBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.cbTenancyReceived = New System.Windows.Forms.CheckBox
        Me.cbTenancyRequired = New System.Windows.Forms.CheckBox
        Me.lblTenancy = New System.Windows.Forms.Label
        Me.cbPayReceived = New System.Windows.Forms.CheckBox
        Me.cbPayRequired = New System.Windows.Forms.CheckBox
        Me.lblPay = New System.Windows.Forms.Label
        Me.cbChattelReceived = New System.Windows.Forms.CheckBox
        Me.cbChattelRequired = New System.Windows.Forms.CheckBox
        Me.lblChattel = New System.Windows.Forms.Label
        Me.cbBankReceived = New System.Windows.Forms.CheckBox
        Me.cbBankRequired = New System.Windows.Forms.CheckBox
        Me.lblBank = New System.Windows.Forms.Label
        Me.cbGoodsReceived = New System.Windows.Forms.CheckBox
        Me.cbGoodsRequired = New System.Windows.Forms.CheckBox
        Me.lblGoods = New System.Windows.Forms.Label
        Me.cbEmploymentReceived = New System.Windows.Forms.CheckBox
        Me.cbEmploymentRequired = New System.Windows.Forms.CheckBox
        Me.lblEmployment = New System.Windows.Forms.Label
        Me.cbCreditReceived = New System.Windows.Forms.CheckBox
        Me.cbCreditRequired = New System.Windows.Forms.CheckBox
        Me.lblCredit = New System.Windows.Forms.Label
        Me.cbInsuranceReceived = New System.Windows.Forms.CheckBox
        Me.cbInsuranceRequired = New System.Windows.Forms.CheckBox
        Me.lblInsurance = New System.Windows.Forms.Label
        Me.cbCofResReceived = New System.Windows.Forms.CheckBox
        Me.cbCofResRequired = New System.Windows.Forms.CheckBox
        Me.lblCofRes = New System.Windows.Forms.Label
        Me.cbMotorCkReceived = New System.Windows.Forms.CheckBox
        Me.cbMotorCkRequired = New System.Windows.Forms.CheckBox
        Me.lblMotorCk = New System.Windows.Forms.Label
        Me.cbPPSRReceived = New System.Windows.Forms.CheckBox
        Me.cbPPSRRequired = New System.Windows.Forms.CheckBox
        Me.lblPPSR = New System.Windows.Forms.Label
        Me.cbCarJamReceived = New System.Windows.Forms.CheckBox
        Me.cbCarJamRequired = New System.Windows.Forms.CheckBox
        Me.lblCarJam = New System.Windows.Forms.Label
        Me.cbVedaXReceived = New System.Windows.Forms.CheckBox
        Me.cbVedaXRequired = New System.Windows.Forms.CheckBox
        Me.lblVedaX = New System.Windows.Forms.Label
        Me.cbVehicleValReceived = New System.Windows.Forms.CheckBox
        Me.cbVehicleValRequired = New System.Windows.Forms.CheckBox
        Me.lblVehicleVal = New System.Windows.Forms.Label
        Me.cbVedaReceived = New System.Windows.Forms.CheckBox
        Me.cbVedaRequired = New System.Windows.Forms.CheckBox
        Me.lblVeda = New System.Windows.Forms.Label
        Me.cbVOSAReceived = New System.Windows.Forms.CheckBox
        Me.cbVOSARequired = New System.Windows.Forms.CheckBox
        Me.lblVOSA = New System.Windows.Forms.Label
        Me.cbIDReceived = New System.Windows.Forms.CheckBox
        Me.cbIDRequired = New System.Windows.Forms.CheckBox
        Me.lblID = New System.Windows.Forms.Label
        Me.lblQRGBacLayer = New System.Windows.Forms.Label
        Me.btnUpdateQRG = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.QRGListTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.QRGListTableAdapter
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
        Me.gpbxQuickRefGuide.SuspendLayout()
        CType(Me.QRGListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gpbxQuickRefGuide
        '
        Me.gpbxQuickRefGuide.BackColor = System.Drawing.Color.Transparent
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbDealerAppShtReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbDealerAppShtRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblDealerAppSht)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbWinzReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbWinzRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblWinz)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbDriverCkReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbDriverCkRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblDriverCk)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblQRGOverlay)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbAddressReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbAddressRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblAddress)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbGSTReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbGSTRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblGST)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbHomeReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbHomeRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblHome)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCompanyReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCompanyRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblCompany)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbTenancyReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbTenancyRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblTenancy)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbPayReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbPayRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblPay)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbChattelReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbChattelRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblChattel)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbBankReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbBankRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblBank)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbGoodsReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbGoodsRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblGoods)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbEmploymentReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbEmploymentRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblEmployment)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCreditReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCreditRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblCredit)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbInsuranceReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbInsuranceRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblInsurance)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCofResReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCofResRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblCofRes)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbMotorCkReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbMotorCkRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblMotorCk)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbPPSRReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbPPSRRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblPPSR)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCarJamReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbCarJamRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblCarJam)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVedaXReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVedaXRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblVedaX)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVehicleValReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVehicleValRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblVehicleVal)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVedaReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVedaRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblVeda)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVOSAReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbVOSARequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblVOSA)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbIDReceived)
        Me.gpbxQuickRefGuide.Controls.Add(Me.cbIDRequired)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblID)
        Me.gpbxQuickRefGuide.Controls.Add(Me.lblQRGBacLayer)
        Me.gpbxQuickRefGuide.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gpbxQuickRefGuide.Location = New System.Drawing.Point(17, 19)
        Me.gpbxQuickRefGuide.Name = "gpbxQuickRefGuide"
        Me.gpbxQuickRefGuide.Size = New System.Drawing.Size(581, 295)
        Me.gpbxQuickRefGuide.TabIndex = 22
        Me.gpbxQuickRefGuide.TabStop = False
        Me.gpbxQuickRefGuide.Text = "Quick Reference Guide"
        '
        'cbDealerAppShtReceived
        '
        Me.cbDealerAppShtReceived.AutoSize = True
        Me.cbDealerAppShtReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "DealerAppShtSent", True))
        Me.cbDealerAppShtReceived.Location = New System.Drawing.Point(502, 268)
        Me.cbDealerAppShtReceived.Name = "cbDealerAppShtReceived"
        Me.cbDealerAppShtReceived.Size = New System.Drawing.Size(46, 17)
        Me.cbDealerAppShtReceived.TabIndex = 79
        Me.cbDealerAppShtReceived.Text = "sent"
        Me.cbDealerAppShtReceived.UseVisualStyleBackColor = True
        '
        'QRGListBindingSource
        '
        Me.QRGListBindingSource.DataMember = "QRGList"
        Me.QRGListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'cbDealerAppShtRequired
        '
        Me.cbDealerAppShtRequired.AutoSize = True
        Me.cbDealerAppShtRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "DealerAppShtRequired", True))
        Me.cbDealerAppShtRequired.Location = New System.Drawing.Point(432, 268)
        Me.cbDealerAppShtRequired.Name = "cbDealerAppShtRequired"
        Me.cbDealerAppShtRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbDealerAppShtRequired.TabIndex = 78
        Me.cbDealerAppShtRequired.Text = "required"
        Me.cbDealerAppShtRequired.UseVisualStyleBackColor = True
        '
        'lblDealerAppSht
        '
        Me.lblDealerAppSht.AutoSize = True
        Me.lblDealerAppSht.Location = New System.Drawing.Point(301, 269)
        Me.lblDealerAppSht.Name = "lblDealerAppSht"
        Me.lblDealerAppSht.Size = New System.Drawing.Size(114, 13)
        Me.lblDealerAppSht.TabIndex = 77
        Me.lblDealerAppSht.Text = "Dealer Approval Sheet"
        '
        'cbWinzReceived
        '
        Me.cbWinzReceived.AutoSize = True
        Me.cbWinzReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "WINZIRDReceived", True))
        Me.cbWinzReceived.Location = New System.Drawing.Point(502, 199)
        Me.cbWinzReceived.Name = "cbWinzReceived"
        Me.cbWinzReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbWinzReceived.TabIndex = 76
        Me.cbWinzReceived.Text = "received"
        Me.cbWinzReceived.UseVisualStyleBackColor = True
        '
        'cbWinzRequired
        '
        Me.cbWinzRequired.AutoSize = True
        Me.cbWinzRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "WINZIRDRequired", True))
        Me.cbWinzRequired.Location = New System.Drawing.Point(432, 199)
        Me.cbWinzRequired.Name = "cbWinzRequired"
        Me.cbWinzRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbWinzRequired.TabIndex = 75
        Me.cbWinzRequired.Text = "required"
        Me.cbWinzRequired.UseVisualStyleBackColor = True
        '
        'lblWinz
        '
        Me.lblWinz.AutoSize = True
        Me.lblWinz.Location = New System.Drawing.Point(301, 200)
        Me.lblWinz.Name = "lblWinz"
        Me.lblWinz.Size = New System.Drawing.Size(90, 13)
        Me.lblWinz.TabIndex = 74
        Me.lblWinz.Text = "WINZ/IRD Letter"
        '
        'cbDriverCkReceived
        '
        Me.cbDriverCkReceived.AutoSize = True
        Me.cbDriverCkReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "DriverCkReceived", True))
        Me.cbDriverCkReceived.Location = New System.Drawing.Point(502, 131)
        Me.cbDriverCkReceived.Name = "cbDriverCkReceived"
        Me.cbDriverCkReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbDriverCkReceived.TabIndex = 73
        Me.cbDriverCkReceived.Text = "received"
        Me.cbDriverCkReceived.UseVisualStyleBackColor = True
        '
        'cbDriverCkRequired
        '
        Me.cbDriverCkRequired.AutoSize = True
        Me.cbDriverCkRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "DriverCkRequired", True))
        Me.cbDriverCkRequired.Location = New System.Drawing.Point(432, 131)
        Me.cbDriverCkRequired.Name = "cbDriverCkRequired"
        Me.cbDriverCkRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbDriverCkRequired.TabIndex = 72
        Me.cbDriverCkRequired.Text = "required"
        Me.cbDriverCkRequired.UseVisualStyleBackColor = True
        '
        'lblDriverCk
        '
        Me.lblDriverCk.AutoSize = True
        Me.lblDriverCk.Location = New System.Drawing.Point(301, 132)
        Me.lblDriverCk.Name = "lblDriverCk"
        Me.lblDriverCk.Size = New System.Drawing.Size(69, 13)
        Me.lblDriverCk.TabIndex = 71
        Me.lblDriverCk.Text = "Driver Check"
        '
        'lblQRGOverlay
        '
        Me.lblQRGOverlay.BackColor = System.Drawing.Color.Transparent
        Me.lblQRGOverlay.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblQRGOverlay.Location = New System.Drawing.Point(8, 0)
        Me.lblQRGOverlay.Name = "lblQRGOverlay"
        Me.lblQRGOverlay.Size = New System.Drawing.Size(130, 13)
        Me.lblQRGOverlay.TabIndex = 19
        '
        'cbAddressReceived
        '
        Me.cbAddressReceived.AutoSize = True
        Me.cbAddressReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "ProofAddressReceived", True))
        Me.cbAddressReceived.Location = New System.Drawing.Point(207, 268)
        Me.cbAddressReceived.Name = "cbAddressReceived"
        Me.cbAddressReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbAddressReceived.TabIndex = 68
        Me.cbAddressReceived.Text = "received"
        Me.cbAddressReceived.UseVisualStyleBackColor = True
        '
        'cbAddressRequired
        '
        Me.cbAddressRequired.AutoSize = True
        Me.cbAddressRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "ProofAddressRequired", True))
        Me.cbAddressRequired.Location = New System.Drawing.Point(137, 268)
        Me.cbAddressRequired.Name = "cbAddressRequired"
        Me.cbAddressRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbAddressRequired.TabIndex = 67
        Me.cbAddressRequired.Text = "required"
        Me.cbAddressRequired.UseVisualStyleBackColor = True
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(6, 269)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(114, 13)
        Me.lblAddress.TabIndex = 66
        Me.lblAddress.Text = "Other Proof of Address"
        '
        'cbGSTReceived
        '
        Me.cbGSTReceived.AutoSize = True
        Me.cbGSTReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "GSTRtrnReceived", True))
        Me.cbGSTReceived.Location = New System.Drawing.Point(502, 245)
        Me.cbGSTReceived.Name = "cbGSTReceived"
        Me.cbGSTReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbGSTReceived.TabIndex = 65
        Me.cbGSTReceived.Text = "received"
        Me.cbGSTReceived.UseVisualStyleBackColor = True
        '
        'cbGSTRequired
        '
        Me.cbGSTRequired.AutoSize = True
        Me.cbGSTRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "GSTRtrnRequired", True))
        Me.cbGSTRequired.Location = New System.Drawing.Point(432, 245)
        Me.cbGSTRequired.Name = "cbGSTRequired"
        Me.cbGSTRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbGSTRequired.TabIndex = 64
        Me.cbGSTRequired.Text = "required"
        Me.cbGSTRequired.UseVisualStyleBackColor = True
        '
        'lblGST
        '
        Me.lblGST.AutoSize = True
        Me.lblGST.Location = New System.Drawing.Point(301, 246)
        Me.lblGST.Name = "lblGST"
        Me.lblGST.Size = New System.Drawing.Size(69, 13)
        Me.lblGST.TabIndex = 63
        Me.lblGST.Text = "GST Returns"
        '
        'cbHomeReceived
        '
        Me.cbHomeReceived.AutoSize = True
        Me.cbHomeReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "HomeOwnerReceived", True))
        Me.cbHomeReceived.Location = New System.Drawing.Point(207, 245)
        Me.cbHomeReceived.Name = "cbHomeReceived"
        Me.cbHomeReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbHomeReceived.TabIndex = 62
        Me.cbHomeReceived.Text = "received"
        Me.cbHomeReceived.UseVisualStyleBackColor = True
        '
        'cbHomeRequired
        '
        Me.cbHomeRequired.AutoSize = True
        Me.cbHomeRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "HomeOwnerRequired", True))
        Me.cbHomeRequired.Location = New System.Drawing.Point(137, 245)
        Me.cbHomeRequired.Name = "cbHomeRequired"
        Me.cbHomeRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbHomeRequired.TabIndex = 61
        Me.cbHomeRequired.Text = "required"
        Me.cbHomeRequired.UseVisualStyleBackColor = True
        '
        'lblHome
        '
        Me.lblHome.AutoSize = True
        Me.lblHome.Location = New System.Drawing.Point(6, 246)
        Me.lblHome.Name = "lblHome"
        Me.lblHome.Size = New System.Drawing.Size(128, 13)
        Me.lblHome.TabIndex = 60
        Me.lblHome.Text = "Proof of Home Ownership"
        '
        'cbCompanyReceived
        '
        Me.cbCompanyReceived.AutoSize = True
        Me.cbCompanyReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "CompanyFinReceived", True))
        Me.cbCompanyReceived.Location = New System.Drawing.Point(502, 222)
        Me.cbCompanyReceived.Name = "cbCompanyReceived"
        Me.cbCompanyReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbCompanyReceived.TabIndex = 59
        Me.cbCompanyReceived.Text = "received"
        Me.cbCompanyReceived.UseVisualStyleBackColor = True
        '
        'cbCompanyRequired
        '
        Me.cbCompanyRequired.AutoSize = True
        Me.cbCompanyRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "CompanyFinRequired", True))
        Me.cbCompanyRequired.Location = New System.Drawing.Point(432, 222)
        Me.cbCompanyRequired.Name = "cbCompanyRequired"
        Me.cbCompanyRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbCompanyRequired.TabIndex = 58
        Me.cbCompanyRequired.Text = "required"
        Me.cbCompanyRequired.UseVisualStyleBackColor = True
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Location = New System.Drawing.Point(301, 223)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(101, 13)
        Me.lblCompany.TabIndex = 57
        Me.lblCompany.Text = "Company Financials"
        '
        'cbTenancyReceived
        '
        Me.cbTenancyReceived.AutoSize = True
        Me.cbTenancyReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "TenancyAgrmtReceived", True))
        Me.cbTenancyReceived.Location = New System.Drawing.Point(207, 222)
        Me.cbTenancyReceived.Name = "cbTenancyReceived"
        Me.cbTenancyReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbTenancyReceived.TabIndex = 56
        Me.cbTenancyReceived.Text = "received"
        Me.cbTenancyReceived.UseVisualStyleBackColor = True
        '
        'cbTenancyRequired
        '
        Me.cbTenancyRequired.AutoSize = True
        Me.cbTenancyRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "TenancyAgrmtRequired", True))
        Me.cbTenancyRequired.Location = New System.Drawing.Point(137, 222)
        Me.cbTenancyRequired.Name = "cbTenancyRequired"
        Me.cbTenancyRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbTenancyRequired.TabIndex = 55
        Me.cbTenancyRequired.Text = "required"
        Me.cbTenancyRequired.UseVisualStyleBackColor = True
        '
        'lblTenancy
        '
        Me.lblTenancy.AutoSize = True
        Me.lblTenancy.Location = New System.Drawing.Point(6, 223)
        Me.lblTenancy.Name = "lblTenancy"
        Me.lblTenancy.Size = New System.Drawing.Size(103, 13)
        Me.lblTenancy.TabIndex = 54
        Me.lblTenancy.Text = "Tenancy Agreement"
        '
        'cbPayReceived
        '
        Me.cbPayReceived.AutoSize = True
        Me.cbPayReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "PayslipReceived", True))
        Me.cbPayReceived.Location = New System.Drawing.Point(207, 199)
        Me.cbPayReceived.Name = "cbPayReceived"
        Me.cbPayReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbPayReceived.TabIndex = 50
        Me.cbPayReceived.Text = "received"
        Me.cbPayReceived.UseVisualStyleBackColor = True
        '
        'cbPayRequired
        '
        Me.cbPayRequired.AutoSize = True
        Me.cbPayRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "PayslipRequired", True))
        Me.cbPayRequired.Location = New System.Drawing.Point(137, 199)
        Me.cbPayRequired.Name = "cbPayRequired"
        Me.cbPayRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbPayRequired.TabIndex = 49
        Me.cbPayRequired.Text = "required"
        Me.cbPayRequired.UseVisualStyleBackColor = True
        '
        'lblPay
        '
        Me.lblPay.AutoSize = True
        Me.lblPay.Location = New System.Drawing.Point(6, 200)
        Me.lblPay.Name = "lblPay"
        Me.lblPay.Size = New System.Drawing.Size(40, 13)
        Me.lblPay.TabIndex = 48
        Me.lblPay.Text = "Payslip"
        '
        'cbChattelReceived
        '
        Me.cbChattelReceived.AutoSize = True
        Me.cbChattelReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "ChattelValReceived", True))
        Me.cbChattelReceived.Location = New System.Drawing.Point(502, 176)
        Me.cbChattelReceived.Name = "cbChattelReceived"
        Me.cbChattelReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbChattelReceived.TabIndex = 47
        Me.cbChattelReceived.Text = "received"
        Me.cbChattelReceived.UseVisualStyleBackColor = True
        '
        'cbChattelRequired
        '
        Me.cbChattelRequired.AutoSize = True
        Me.cbChattelRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "ChattelValRequired", True))
        Me.cbChattelRequired.Location = New System.Drawing.Point(432, 176)
        Me.cbChattelRequired.Name = "cbChattelRequired"
        Me.cbChattelRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbChattelRequired.TabIndex = 46
        Me.cbChattelRequired.Text = "required"
        Me.cbChattelRequired.UseVisualStyleBackColor = True
        '
        'lblChattel
        '
        Me.lblChattel.AutoSize = True
        Me.lblChattel.Location = New System.Drawing.Point(301, 177)
        Me.lblChattel.Name = "lblChattel"
        Me.lblChattel.Size = New System.Drawing.Size(87, 13)
        Me.lblChattel.TabIndex = 45
        Me.lblChattel.Text = "Chattel Valuation"
        '
        'cbBankReceived
        '
        Me.cbBankReceived.AutoSize = True
        Me.cbBankReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "BankStateReceived", True))
        Me.cbBankReceived.Location = New System.Drawing.Point(207, 176)
        Me.cbBankReceived.Name = "cbBankReceived"
        Me.cbBankReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbBankReceived.TabIndex = 44
        Me.cbBankReceived.Text = "received"
        Me.cbBankReceived.UseVisualStyleBackColor = True
        '
        'cbBankRequired
        '
        Me.cbBankRequired.AutoSize = True
        Me.cbBankRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "BankStateRequired", True))
        Me.cbBankRequired.Location = New System.Drawing.Point(137, 176)
        Me.cbBankRequired.Name = "cbBankRequired"
        Me.cbBankRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbBankRequired.TabIndex = 43
        Me.cbBankRequired.Text = "required"
        Me.cbBankRequired.UseVisualStyleBackColor = True
        '
        'lblBank
        '
        Me.lblBank.AutoSize = True
        Me.lblBank.Location = New System.Drawing.Point(6, 177)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(83, 13)
        Me.lblBank.TabIndex = 42
        Me.lblBank.Text = "Bank Statement"
        '
        'cbGoodsReceived
        '
        Me.cbGoodsReceived.AutoSize = True
        Me.cbGoodsReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "GoodsInvReceived", True))
        Me.cbGoodsReceived.Location = New System.Drawing.Point(502, 153)
        Me.cbGoodsReceived.Name = "cbGoodsReceived"
        Me.cbGoodsReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbGoodsReceived.TabIndex = 41
        Me.cbGoodsReceived.Text = "received"
        Me.cbGoodsReceived.UseVisualStyleBackColor = True
        '
        'cbGoodsRequired
        '
        Me.cbGoodsRequired.AutoSize = True
        Me.cbGoodsRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "GoodsInvRequired", True))
        Me.cbGoodsRequired.Location = New System.Drawing.Point(432, 153)
        Me.cbGoodsRequired.Name = "cbGoodsRequired"
        Me.cbGoodsRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbGoodsRequired.TabIndex = 40
        Me.cbGoodsRequired.Text = "required"
        Me.cbGoodsRequired.UseVisualStyleBackColor = True
        '
        'lblGoods
        '
        Me.lblGoods.AutoSize = True
        Me.lblGoods.Location = New System.Drawing.Point(301, 154)
        Me.lblGoods.Name = "lblGoods"
        Me.lblGoods.Size = New System.Drawing.Size(76, 13)
        Me.lblGoods.TabIndex = 39
        Me.lblGoods.Text = "Goods Invoice"
        '
        'cbEmploymentReceived
        '
        Me.cbEmploymentReceived.AutoSize = True
        Me.cbEmploymentReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "EmploymentReceived", True))
        Me.cbEmploymentReceived.Location = New System.Drawing.Point(207, 153)
        Me.cbEmploymentReceived.Name = "cbEmploymentReceived"
        Me.cbEmploymentReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbEmploymentReceived.TabIndex = 38
        Me.cbEmploymentReceived.Text = "received"
        Me.cbEmploymentReceived.UseVisualStyleBackColor = True
        '
        'cbEmploymentRequired
        '
        Me.cbEmploymentRequired.AutoSize = True
        Me.cbEmploymentRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "EmploymentRequired", True))
        Me.cbEmploymentRequired.Location = New System.Drawing.Point(137, 153)
        Me.cbEmploymentRequired.Name = "cbEmploymentRequired"
        Me.cbEmploymentRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbEmploymentRequired.TabIndex = 37
        Me.cbEmploymentRequired.Text = "required"
        Me.cbEmploymentRequired.UseVisualStyleBackColor = True
        '
        'lblEmployment
        '
        Me.lblEmployment.AutoSize = True
        Me.lblEmployment.Location = New System.Drawing.Point(6, 154)
        Me.lblEmployment.Name = "lblEmployment"
        Me.lblEmployment.Size = New System.Drawing.Size(113, 13)
        Me.lblEmployment.TabIndex = 36
        Me.lblEmployment.Text = "Employment confirmed"
        '
        'cbCreditReceived
        '
        Me.cbCreditReceived.AutoSize = True
        Me.cbCreditReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "CreditRefReceived", True))
        Me.cbCreditReceived.Location = New System.Drawing.Point(207, 130)
        Me.cbCreditReceived.Name = "cbCreditReceived"
        Me.cbCreditReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbCreditReceived.TabIndex = 32
        Me.cbCreditReceived.Text = "received"
        Me.cbCreditReceived.UseVisualStyleBackColor = True
        '
        'cbCreditRequired
        '
        Me.cbCreditRequired.AutoSize = True
        Me.cbCreditRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "CreditRefRequired", True))
        Me.cbCreditRequired.Location = New System.Drawing.Point(137, 130)
        Me.cbCreditRequired.Name = "cbCreditRequired"
        Me.cbCreditRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbCreditRequired.TabIndex = 31
        Me.cbCreditRequired.Text = "required"
        Me.cbCreditRequired.UseVisualStyleBackColor = True
        '
        'lblCredit
        '
        Me.lblCredit.AutoSize = True
        Me.lblCredit.Location = New System.Drawing.Point(6, 131)
        Me.lblCredit.Name = "lblCredit"
        Me.lblCredit.Size = New System.Drawing.Size(87, 13)
        Me.lblCredit.TabIndex = 30
        Me.lblCredit.Text = "Credit Reference"
        '
        'cbInsuranceReceived
        '
        Me.cbInsuranceReceived.AutoSize = True
        Me.cbInsuranceReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "InsuranceReceived", True))
        Me.cbInsuranceReceived.Location = New System.Drawing.Point(502, 107)
        Me.cbInsuranceReceived.Name = "cbInsuranceReceived"
        Me.cbInsuranceReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbInsuranceReceived.TabIndex = 29
        Me.cbInsuranceReceived.Text = "received"
        Me.cbInsuranceReceived.UseVisualStyleBackColor = True
        '
        'cbInsuranceRequired
        '
        Me.cbInsuranceRequired.AutoSize = True
        Me.cbInsuranceRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "InsuranceRequired", True))
        Me.cbInsuranceRequired.Location = New System.Drawing.Point(432, 107)
        Me.cbInsuranceRequired.Name = "cbInsuranceRequired"
        Me.cbInsuranceRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbInsuranceRequired.TabIndex = 28
        Me.cbInsuranceRequired.Text = "required"
        Me.cbInsuranceRequired.UseVisualStyleBackColor = True
        '
        'lblInsurance
        '
        Me.lblInsurance.AutoSize = True
        Me.lblInsurance.Location = New System.Drawing.Point(301, 108)
        Me.lblInsurance.Name = "lblInsurance"
        Me.lblInsurance.Size = New System.Drawing.Size(115, 13)
        Me.lblInsurance.TabIndex = 27
        Me.lblInsurance.Text = "Insurance Confirmation"
        '
        'cbCofResReceived
        '
        Me.cbCofResReceived.AutoSize = True
        Me.cbCofResReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "ResidencyReceived", True))
        Me.cbCofResReceived.Location = New System.Drawing.Point(207, 107)
        Me.cbCofResReceived.Name = "cbCofResReceived"
        Me.cbCofResReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbCofResReceived.TabIndex = 26
        Me.cbCofResReceived.Text = "received"
        Me.cbCofResReceived.UseVisualStyleBackColor = True
        '
        'cbCofResRequired
        '
        Me.cbCofResRequired.AutoSize = True
        Me.cbCofResRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "ResidencyRequired", True))
        Me.cbCofResRequired.Location = New System.Drawing.Point(137, 107)
        Me.cbCofResRequired.Name = "cbCofResRequired"
        Me.cbCofResRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbCofResRequired.TabIndex = 25
        Me.cbCofResRequired.Text = "required"
        Me.cbCofResRequired.UseVisualStyleBackColor = True
        '
        'lblCofRes
        '
        Me.lblCofRes.AutoSize = True
        Me.lblCofRes.Location = New System.Drawing.Point(6, 108)
        Me.lblCofRes.Name = "lblCofRes"
        Me.lblCofRes.Size = New System.Drawing.Size(130, 13)
        Me.lblCofRes.TabIndex = 24
        Me.lblCofRes.Text = "Confirmation of Residency"
        '
        'cbMotorCkReceived
        '
        Me.cbMotorCkReceived.AutoSize = True
        Me.cbMotorCkReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "MotorCkReceived", True))
        Me.cbMotorCkReceived.Location = New System.Drawing.Point(502, 84)
        Me.cbMotorCkReceived.Name = "cbMotorCkReceived"
        Me.cbMotorCkReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbMotorCkReceived.TabIndex = 23
        Me.cbMotorCkReceived.Text = "received"
        Me.cbMotorCkReceived.UseVisualStyleBackColor = True
        '
        'cbMotorCkRequired
        '
        Me.cbMotorCkRequired.AutoSize = True
        Me.cbMotorCkRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "MotorCkRequired", True))
        Me.cbMotorCkRequired.Location = New System.Drawing.Point(432, 84)
        Me.cbMotorCkRequired.Name = "cbMotorCkRequired"
        Me.cbMotorCkRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbMotorCkRequired.TabIndex = 22
        Me.cbMotorCkRequired.Text = "required"
        Me.cbMotorCkRequired.UseVisualStyleBackColor = True
        '
        'lblMotorCk
        '
        Me.lblMotorCk.AutoSize = True
        Me.lblMotorCk.Location = New System.Drawing.Point(301, 85)
        Me.lblMotorCk.Name = "lblMotorCk"
        Me.lblMotorCk.Size = New System.Drawing.Size(68, 13)
        Me.lblMotorCk.TabIndex = 21
        Me.lblMotorCk.Text = "Motor Check"
        '
        'cbPPSRReceived
        '
        Me.cbPPSRReceived.AutoSize = True
        Me.cbPPSRReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "PPSRReceived", True))
        Me.cbPPSRReceived.Location = New System.Drawing.Point(207, 84)
        Me.cbPPSRReceived.Name = "cbPPSRReceived"
        Me.cbPPSRReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbPPSRReceived.TabIndex = 20
        Me.cbPPSRReceived.Text = "received"
        Me.cbPPSRReceived.UseVisualStyleBackColor = True
        '
        'cbPPSRRequired
        '
        Me.cbPPSRRequired.AutoSize = True
        Me.cbPPSRRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "PPSRRequired", True))
        Me.cbPPSRRequired.Location = New System.Drawing.Point(137, 84)
        Me.cbPPSRRequired.Name = "cbPPSRRequired"
        Me.cbPPSRRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbPPSRRequired.TabIndex = 19
        Me.cbPPSRRequired.Text = "required"
        Me.cbPPSRRequired.UseVisualStyleBackColor = True
        '
        'lblPPSR
        '
        Me.lblPPSR.AutoSize = True
        Me.lblPPSR.Location = New System.Drawing.Point(6, 85)
        Me.lblPPSR.Name = "lblPPSR"
        Me.lblPPSR.Size = New System.Drawing.Size(60, 13)
        Me.lblPPSR.TabIndex = 18
        Me.lblPPSR.Text = "PPSR/Xref"
        '
        'cbCarJamReceived
        '
        Me.cbCarJamReceived.AutoSize = True
        Me.cbCarJamReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "CarJamReceived", True))
        Me.cbCarJamReceived.Location = New System.Drawing.Point(502, 61)
        Me.cbCarJamReceived.Name = "cbCarJamReceived"
        Me.cbCarJamReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbCarJamReceived.TabIndex = 17
        Me.cbCarJamReceived.Text = "received"
        Me.cbCarJamReceived.UseVisualStyleBackColor = True
        '
        'cbCarJamRequired
        '
        Me.cbCarJamRequired.AutoSize = True
        Me.cbCarJamRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "CarJamRequired", True))
        Me.cbCarJamRequired.Location = New System.Drawing.Point(432, 61)
        Me.cbCarJamRequired.Name = "cbCarJamRequired"
        Me.cbCarJamRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbCarJamRequired.TabIndex = 16
        Me.cbCarJamRequired.Text = "required"
        Me.cbCarJamRequired.UseVisualStyleBackColor = True
        '
        'lblCarJam
        '
        Me.lblCarJam.AutoSize = True
        Me.lblCarJam.Location = New System.Drawing.Point(301, 62)
        Me.lblCarJam.Name = "lblCarJam"
        Me.lblCarJam.Size = New System.Drawing.Size(104, 13)
        Me.lblCarJam.TabIndex = 15
        Me.lblCarJam.Text = "Turners Price Check"
        '
        'cbVedaXReceived
        '
        Me.cbVedaXReceived.AutoSize = True
        Me.cbVedaXReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VedaXRefReceived", True))
        Me.cbVedaXReceived.Location = New System.Drawing.Point(207, 61)
        Me.cbVedaXReceived.Name = "cbVedaXReceived"
        Me.cbVedaXReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbVedaXReceived.TabIndex = 14
        Me.cbVedaXReceived.Text = "received"
        Me.cbVedaXReceived.UseVisualStyleBackColor = True
        '
        'cbVedaXRequired
        '
        Me.cbVedaXRequired.AutoSize = True
        Me.cbVedaXRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VedaXRefRequired", True))
        Me.cbVedaXRequired.Location = New System.Drawing.Point(137, 61)
        Me.cbVedaXRequired.Name = "cbVedaXRequired"
        Me.cbVedaXRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbVedaXRequired.TabIndex = 13
        Me.cbVedaXRequired.Text = "required"
        Me.cbVedaXRequired.UseVisualStyleBackColor = True
        '
        'lblVedaX
        '
        Me.lblVedaX.AutoSize = True
        Me.lblVedaX.Location = New System.Drawing.Point(6, 62)
        Me.lblVedaX.Name = "lblVedaX"
        Me.lblVedaX.Size = New System.Drawing.Size(54, 13)
        Me.lblVedaX.TabIndex = 12
        Me.lblVedaX.Text = "Veda Xref"
        '
        'cbVehicleValReceived
        '
        Me.cbVehicleValReceived.AutoSize = True
        Me.cbVehicleValReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VehicleValReceived", True))
        Me.cbVehicleValReceived.Location = New System.Drawing.Point(502, 38)
        Me.cbVehicleValReceived.Name = "cbVehicleValReceived"
        Me.cbVehicleValReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbVehicleValReceived.TabIndex = 11
        Me.cbVehicleValReceived.Text = "received"
        Me.cbVehicleValReceived.UseVisualStyleBackColor = True
        '
        'cbVehicleValRequired
        '
        Me.cbVehicleValRequired.AutoSize = True
        Me.cbVehicleValRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VehicleValRequired", True))
        Me.cbVehicleValRequired.Location = New System.Drawing.Point(432, 38)
        Me.cbVehicleValRequired.Name = "cbVehicleValRequired"
        Me.cbVehicleValRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbVehicleValRequired.TabIndex = 10
        Me.cbVehicleValRequired.Text = "required"
        Me.cbVehicleValRequired.UseVisualStyleBackColor = True
        '
        'lblVehicleVal
        '
        Me.lblVehicleVal.AutoSize = True
        Me.lblVehicleVal.Location = New System.Drawing.Point(301, 39)
        Me.lblVehicleVal.Name = "lblVehicleVal"
        Me.lblVehicleVal.Size = New System.Drawing.Size(89, 13)
        Me.lblVehicleVal.TabIndex = 9
        Me.lblVehicleVal.Text = "Vehicle Valuation"
        '
        'cbVedaReceived
        '
        Me.cbVedaReceived.AutoSize = True
        Me.cbVedaReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VedaReceived", True))
        Me.cbVedaReceived.Location = New System.Drawing.Point(207, 38)
        Me.cbVedaReceived.Name = "cbVedaReceived"
        Me.cbVedaReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbVedaReceived.TabIndex = 8
        Me.cbVedaReceived.Text = "received"
        Me.cbVedaReceived.UseVisualStyleBackColor = True
        '
        'cbVedaRequired
        '
        Me.cbVedaRequired.AutoSize = True
        Me.cbVedaRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VedaRequired", True))
        Me.cbVedaRequired.Location = New System.Drawing.Point(137, 38)
        Me.cbVedaRequired.Name = "cbVedaRequired"
        Me.cbVedaRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbVedaRequired.TabIndex = 7
        Me.cbVedaRequired.Text = "required"
        Me.cbVedaRequired.UseVisualStyleBackColor = True
        '
        'lblVeda
        '
        Me.lblVeda.AutoSize = True
        Me.lblVeda.Location = New System.Drawing.Point(6, 39)
        Me.lblVeda.Name = "lblVeda"
        Me.lblVeda.Size = New System.Drawing.Size(32, 13)
        Me.lblVeda.TabIndex = 6
        Me.lblVeda.Text = "Veda"
        '
        'cbVOSAReceived
        '
        Me.cbVOSAReceived.AutoSize = True
        Me.cbVOSAReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VOSAReceived", True))
        Me.cbVOSAReceived.Location = New System.Drawing.Point(502, 15)
        Me.cbVOSAReceived.Name = "cbVOSAReceived"
        Me.cbVOSAReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbVOSAReceived.TabIndex = 5
        Me.cbVOSAReceived.Text = "received"
        Me.cbVOSAReceived.UseVisualStyleBackColor = True
        '
        'cbVOSARequired
        '
        Me.cbVOSARequired.AutoSize = True
        Me.cbVOSARequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "VOSARequired", True))
        Me.cbVOSARequired.Location = New System.Drawing.Point(432, 15)
        Me.cbVOSARequired.Name = "cbVOSARequired"
        Me.cbVOSARequired.Size = New System.Drawing.Size(64, 17)
        Me.cbVOSARequired.TabIndex = 4
        Me.cbVOSARequired.Text = "required"
        Me.cbVOSARequired.UseVisualStyleBackColor = True
        '
        'lblVOSA
        '
        Me.lblVOSA.AutoSize = True
        Me.lblVOSA.Location = New System.Drawing.Point(301, 16)
        Me.lblVOSA.Name = "lblVOSA"
        Me.lblVOSA.Size = New System.Drawing.Size(59, 13)
        Me.lblVOSA.TabIndex = 3
        Me.lblVOSA.Text = "VOSA/SIN"
        '
        'cbIDReceived
        '
        Me.cbIDReceived.AutoSize = True
        Me.cbIDReceived.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "IdReceived", True))
        Me.cbIDReceived.Location = New System.Drawing.Point(207, 15)
        Me.cbIDReceived.Name = "cbIDReceived"
        Me.cbIDReceived.Size = New System.Drawing.Size(67, 17)
        Me.cbIDReceived.TabIndex = 2
        Me.cbIDReceived.Text = "received"
        Me.cbIDReceived.UseVisualStyleBackColor = True
        '
        'cbIDRequired
        '
        Me.cbIDRequired.AutoSize = True
        Me.cbIDRequired.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.QRGListBindingSource, "IdRequired", True))
        Me.cbIDRequired.Location = New System.Drawing.Point(137, 15)
        Me.cbIDRequired.Name = "cbIDRequired"
        Me.cbIDRequired.Size = New System.Drawing.Size(64, 17)
        Me.cbIDRequired.TabIndex = 1
        Me.cbIDRequired.Text = "required"
        Me.cbIDRequired.UseVisualStyleBackColor = True
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(6, 16)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(18, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID"
        '
        'lblQRGBacLayer
        '
        Me.lblQRGBacLayer.BackColor = System.Drawing.Color.Transparent
        Me.lblQRGBacLayer.Location = New System.Drawing.Point(1, 6)
        Me.lblQRGBacLayer.Name = "lblQRGBacLayer"
        Me.lblQRGBacLayer.Size = New System.Drawing.Size(677, 7)
        Me.lblQRGBacLayer.TabIndex = 70
        '
        'btnUpdateQRG
        '
        Me.btnUpdateQRG.BackColor = System.Drawing.Color.LightBlue
        Me.btnUpdateQRG.Location = New System.Drawing.Point(80, 327)
        Me.btnUpdateQRG.Name = "btnUpdateQRG"
        Me.btnUpdateQRG.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdateQRG.TabIndex = 69
        Me.btnUpdateQRG.Text = "Save"
        Me.btnUpdateQRG.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(449, 327)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 70
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'QRGListTableAdapter
        '
        Me.QRGListTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Me.QRGListTableAdapter
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'QRGForm
        '
        Me.AcceptButton = Me.btnUpdateQRG
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(618, 364)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gpbxQuickRefGuide)
        Me.Controls.Add(Me.btnUpdateQRG)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(634, 400)
        Me.MinimumSize = New System.Drawing.Size(634, 400)
        Me.Name = "QRGForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Quick Reference Guide"
        Me.gpbxQuickRefGuide.ResumeLayout(False)
        Me.gpbxQuickRefGuide.PerformLayout()
        CType(Me.QRGListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gpbxQuickRefGuide As System.Windows.Forms.GroupBox
    Friend WithEvents lblQRGOverlay As System.Windows.Forms.Label
    Friend WithEvents cbAddressReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbAddressRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents cbGSTReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbGSTRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblGST As System.Windows.Forms.Label
    Friend WithEvents cbHomeReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbHomeRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblHome As System.Windows.Forms.Label
    Friend WithEvents cbCompanyReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbCompanyRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents cbTenancyReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbTenancyRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblTenancy As System.Windows.Forms.Label
    Friend WithEvents cbPayReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbPayRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblPay As System.Windows.Forms.Label
    Friend WithEvents cbChattelReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbChattelRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblChattel As System.Windows.Forms.Label
    Friend WithEvents cbBankReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbBankRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblBank As System.Windows.Forms.Label
    Friend WithEvents cbGoodsReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbGoodsRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblGoods As System.Windows.Forms.Label
    Friend WithEvents cbEmploymentReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbEmploymentRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblEmployment As System.Windows.Forms.Label
    Friend WithEvents cbCreditReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbCreditRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblCredit As System.Windows.Forms.Label
    Friend WithEvents cbInsuranceReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbInsuranceRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblInsurance As System.Windows.Forms.Label
    Friend WithEvents cbCofResReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbCofResRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblCofRes As System.Windows.Forms.Label
    Friend WithEvents cbMotorCkReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbMotorCkRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblMotorCk As System.Windows.Forms.Label
    Friend WithEvents cbPPSRReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbPPSRRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblPPSR As System.Windows.Forms.Label
    Friend WithEvents cbCarJamReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbCarJamRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblCarJam As System.Windows.Forms.Label
    Friend WithEvents cbVedaXReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbVedaXRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblVedaX As System.Windows.Forms.Label
    Friend WithEvents cbVehicleValReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbVehicleValRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblVehicleVal As System.Windows.Forms.Label
    Friend WithEvents cbVedaReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbVedaRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblVeda As System.Windows.Forms.Label
    Friend WithEvents cbVOSAReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbVOSARequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblVOSA As System.Windows.Forms.Label
    Friend WithEvents cbIDReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbIDRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblQRGBacLayer As System.Windows.Forms.Label
    Friend WithEvents btnUpdateQRG As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents cbDriverCkReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbDriverCkRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblDriverCk As System.Windows.Forms.Label
    Friend WithEvents cbWinzReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbWinzRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblWinz As System.Windows.Forms.Label
    Friend WithEvents cbDealerAppShtReceived As System.Windows.Forms.CheckBox
    Friend WithEvents cbDealerAppShtRequired As System.Windows.Forms.CheckBox
    Friend WithEvents lblDealerAppSht As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents QRGListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents QRGListTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.QRGListTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
End Class
