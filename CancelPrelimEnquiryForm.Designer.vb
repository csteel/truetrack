﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CancelPrelimEnquiryForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CancelPrelimEnquiryForm))
        Me.btnCancelEnquiry = New System.Windows.Forms.Button
        Me.btnGoBack = New System.Windows.Forms.Button
        Me.lblCancelTitle = New System.Windows.Forms.Label
        Me.EnquiryTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet
        Me.lblOtherEnquiryCode = New System.Windows.Forms.Label
        Me.txtbxOtherEnquiryCode = New System.Windows.Forms.TextBox
        Me.ActiveEnquiryClientsTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveEnquiryClientsTableAdapter
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelEnquiry
        '
        Me.btnCancelEnquiry.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancelEnquiry.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCancelEnquiry.Location = New System.Drawing.Point(329, 115)
        Me.btnCancelEnquiry.Name = "btnCancelEnquiry"
        Me.btnCancelEnquiry.Size = New System.Drawing.Size(113, 23)
        Me.btnCancelEnquiry.TabIndex = 43
        Me.btnCancelEnquiry.Text = "Cancel Enquiry"
        Me.btnCancelEnquiry.UseVisualStyleBackColor = False
        '
        'btnGoBack
        '
        Me.btnGoBack.BackColor = System.Drawing.Color.LightGreen
        Me.btnGoBack.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnGoBack.Location = New System.Drawing.Point(12, 115)
        Me.btnGoBack.Name = "btnGoBack"
        Me.btnGoBack.Size = New System.Drawing.Size(75, 23)
        Me.btnGoBack.TabIndex = 42
        Me.btnGoBack.Text = "Go Back"
        Me.btnGoBack.UseVisualStyleBackColor = False
        '
        'lblCancelTitle
        '
        Me.lblCancelTitle.AutoSize = True
        Me.lblCancelTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancelTitle.Location = New System.Drawing.Point(93, 9)
        Me.lblCancelTitle.Name = "lblCancelTitle"
        Me.lblCancelTitle.Size = New System.Drawing.Size(221, 20)
        Me.lblCancelTitle.TabIndex = 39
        Me.lblCancelTitle.Text = "Cancel Preliminary Enquiry"
        '
        'EnquiryTableAdapter1
        '
        Me.EnquiryTableAdapter1.ClearBeforeFill = True
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblOtherEnquiryCode
        '
        Me.lblOtherEnquiryCode.AutoSize = True
        Me.lblOtherEnquiryCode.Location = New System.Drawing.Point(28, 65)
        Me.lblOtherEnquiryCode.Name = "lblOtherEnquiryCode"
        Me.lblOtherEnquiryCode.Size = New System.Drawing.Size(195, 13)
        Me.lblOtherEnquiryCode.TabIndex = 44
        Me.lblOtherEnquiryCode.Text = "Please enter Client's other Enquiry Code"
        '
        'txtbxOtherEnquiryCode
        '
        Me.txtbxOtherEnquiryCode.Location = New System.Drawing.Point(229, 62)
        Me.txtbxOtherEnquiryCode.Name = "txtbxOtherEnquiryCode"
        Me.txtbxOtherEnquiryCode.Size = New System.Drawing.Size(213, 20)
        Me.txtbxOtherEnquiryCode.TabIndex = 1
        '
        'ActiveEnquiryClientsTableAdapter1
        '
        Me.ActiveEnquiryClientsTableAdapter1.ClearBeforeFill = True
        '
        'CancelPrelimEnquiryForm
        '
        Me.AcceptButton = Me.btnCancelEnquiry
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnGoBack
        Me.ClientSize = New System.Drawing.Size(454, 164)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtbxOtherEnquiryCode)
        Me.Controls.Add(Me.lblOtherEnquiryCode)
        Me.Controls.Add(Me.btnCancelEnquiry)
        Me.Controls.Add(Me.btnGoBack)
        Me.Controls.Add(Me.lblCancelTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(470, 200)
        Me.MinimumSize = New System.Drawing.Size(470, 200)
        Me.Name = "CancelPrelimEnquiryForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cancel Preliminary Enquiry Form"
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelEnquiry As System.Windows.Forms.Button
    Friend WithEvents btnGoBack As System.Windows.Forms.Button
    Friend WithEvents lblCancelTitle As System.Windows.Forms.Label
    Friend WithEvents EnquiryTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents lblOtherEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents txtbxOtherEnquiryCode As System.Windows.Forms.TextBox
    Friend WithEvents ActiveEnquiryClientsTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveEnquiryClientsTableAdapter
End Class
