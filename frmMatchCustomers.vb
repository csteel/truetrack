﻿Imports System.Data.SqlClient

Public Class FrmMatchCustomers

    'Private connectString As String
    'Private connection As SqlConnection
    'Dim ds As DataSet
    Public EnquiryId As Integer
    Public EnquiryCode As String = String.Empty
    Public ApplicationCode As String = String.Empty
    Public EnquiryType As Integer
    Private finPowerOpObj As finPowerOps
    Private xmlClientInfo As finPowerOps.ClientInfo
    Private ttCustomerInfo As finPowerOps.ttCustomerInfo
    Private unmatchedXmlClients As New Dictionary(Of Integer, finPowerOps.CustomerInfo) 'list of xmlClientInfo.Clients (Name, CustomerId)
    Private unmatchedTTCustomers As New Dictionary(Of Integer, finPowerOps.CustomerInfo) 'list of ttCustomerInfo.Customers (XRefId, Name, CustomerId)



    Public Sub New(enquiryId As Integer, enquiryCode As String, applicationCode As String, enquiryType As Integer)
        'This call is required by the designer.
        InitializeComponent()
        Me.EnquiryId = enquiryId
        Me.EnquiryCode = enquiryCode
        Me.ApplicationCode = applicationCode
        Me.EnquiryType = enquiryType
    End Sub

    Private Sub FrmMatchCustomers_Layout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        Dim custCount As Integer
        Dim dgvHeight As Integer
        custCount = dgvMatchCustomers.Rows.Count
        dgvHeight = custCount * 22 + 23
        dgvMatchCustomers.Size = New Size(543, dgvHeight)
        Me.Size = New Size(595, dgvMatchCustomers.Location.Y + dgvHeight + 100)

    End Sub

    Private Sub FrmMatchCustomersLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim xRefValue As String
        Dim found As Boolean

        SetFormCaption()

        If finPowerOpObj Is Nothing Then
            finPowerOpObj = New finPowerOps(EnquiryCode, ApplicationCode)
            'Get XML Customers' details
            xmlClientInfo = finPowerOpObj.GetXmlClientInfo()
            If xmlClientInfo IsNot Nothing Then
                If Not String.IsNullOrWhiteSpace(xmlClientInfo.ErrorMessage) Then
                    MessageBox.Show(xmlClientInfo.ErrorMessage, "Xml parsing error", MessageBoxButtons.OK)
                End If
            End If

            'Get TrueTrack Customers' details
            ttCustomerInfo = finPowerOpObj.GetTTCustomerInfo(EnquiryId)
            If ttCustomerInfo IsNot Nothing Then
                If Not String.IsNullOrWhiteSpace(ttCustomerInfo.ErrorMessage) Then
                    MessageBox.Show(ttCustomerInfo.ErrorMessage, "Xml parsing error", MessageBoxButtons.OK)
                End If
            End If

        End If

        'Populate lists for matching
        'Match Customers by XRefId (Imported customers should already be matched)
        If xmlClientInfo.Status = True And ttCustomerInfo.Status = True Then
            Dim i As Integer
            Dim k As Integer = 0
            Dim l As Integer = 0
            Dim customer As finPowerOps.CustomerInfo
            Dim msgtxt As String
            unmatchedXmlClients.Clear()
            unmatchedTTCustomers.Clear()
            'Fill UnmatchedXmlClients unless  xmlClientInfo.Clients.CustomerType = Partnership
            For i = 1 To xmlClientInfo.Clients.Count
                If Not xmlClientInfo.Clients(i).CustomerType = MyEnums.GetDescription(DirectCast(3, MyEnums.mainCustomerType)) Then 'Partnership
                    k += 1
                    customer = New finPowerOps.CustomerInfo
                    customer.Name = xmlClientInfo.Clients(i).ApplicantName
                    customer.CustomerId = xmlClientInfo.Clients(i).XREfId
                    unmatchedXmlClients.Add(k, customer)
                End If
            Next


            For i = 1 To ttCustomerInfo.Customers.Count
                If ttCustomerInfo.Customers(i).XRefId = String.Empty Then
                    'If XRefId is null then add ttCustomer to UnmatchedTTCustomers list
                    'If this is a Financefacility Enquiry then only add Type= main{0} to unmatchedXmlClients
                    If EnquiryType = MyEnums.EnquiryType.FinanceFacility And ttCustomerInfo.Customers(i).Type = 0 Then
                        l += 1
                        customer = New finPowerOps.CustomerInfo
                        customer.Name = ttCustomerInfo.Customers(i).Name
                        customer.CustomerId = ttCustomerInfo.Customers(i).CustomerId
                        unmatchedTTCustomers.Add(l, customer)
                    ElseIf Not EnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        l += 1
                        customer = New finPowerOps.CustomerInfo
                        customer.Name = ttCustomerInfo.Customers(i).Name
                        customer.CustomerId = ttCustomerInfo.Customers(i).CustomerId
                        unmatchedTTCustomers.Add(l, customer)
                    End If

                Else
                    'If XRefId matches with xmlClientInfo.Client.ClientId then remove from UnmatchedXmlClients list
                    'If XRefId no match found then add ttCustomer to UnmatchedTTCustomers list
                    xRefValue = ttCustomerInfo.Customers(i).XRefId
                    found = False

                    ' can not use For Count loop as count may not match the keys
                    For Each kvp As KeyValuePair(Of Integer, finPowerOps.CustomerInfo) In unmatchedXmlClients
                        Try
                            If xRefValue = kvp.Value.CustomerId Then
                                unmatchedXmlClients.Remove(kvp.Key)
                                found = True
                                Exit For
                            End If
                        Catch ex As Exception
                            MsgBox("FrmMatchCustomersLoad caused an error" & vbNewLine & ex.Message.ToString)
                        End Try
                    Next

                    If found = False Then
                        'if is FinanceFacility and not the mainCustomer then do not add unmatchedTTCustomer 
                        '(only one customer in XML but may be more in TrueTrack e.g. Guarantor)
                        If EnquiryType = MyEnums.EnquiryType.FinanceFacility And Not ttCustomerInfo.Customers(i).Type = MyEnums.CustomerEnquiryType.Main Then
                            'do nothing
                        Else
                            l += 1
                            customer = New finPowerOps.CustomerInfo
                            customer.Name = ttCustomerInfo.Customers(i).Name
                            customer.CustomerId = ttCustomerInfo.Customers(i).CustomerId
                            unmatchedTTCustomers.Add(l, customer)
                        End If

                    End If


                End If
            Next

            'Count those not matched, if count > 0
            'Display those not able to be matched and ask user to match (unless CustomerType is Partnership)
            If unmatchedTTCustomers.Count > 0 Then
                'load up datagrid
                'add datasource for combobox
                '==================================================================
                'Use the 'DictionaryEntry' object to pair keys and values.
                'Set the 'DisplayMember' and 'ValueMember' properties to:
                '                Me.myComboBox.DisplayMember = "Key"
                '                Me.myComboBox.ValueMember = "Value"
                'To add items to the ComboBox:
                '                Me.myComboBox.Items.Add(New DictionaryEntry("Text to be displayed", 1))
                'To retreive items like this:
                '                MsgBox(Me.myComboBox.SelectedItem.Key & " " & Me.myComboBox.SelectedItem.Value)
                '==================================================================
                OACCustomers.DisplayMember = "Key"
                OACCustomers.ValueMember = "Value"
                ' can not use For Count loop as count does may not match the keys
                For Each kvp As KeyValuePair(Of Integer, finPowerOps.CustomerInfo) In unmatchedXmlClients
                    Try
                        OACCustomers.Items.Add(New DictionaryEntry(kvp.Value.Name, kvp.Value.CustomerId))
                    Catch ex As Exception
                        MsgBox("FrmMatchCustomersLoad caused an error" & vbNewLine & ex.Message.ToString)
                    End Try
                Next

                For Each cust As finPowerOps.CustomerInfo In unmatchedTTCustomers.Values
                    With cust
                        dgvMatchCustomers.Rows.Add(.CustomerId.ToString(), .Name, "")
                    End With
                Next

                'set form focus
                Me.dgvMatchCustomers.Focus()
                Me.dgvMatchCustomers.CurrentCell = Me.dgvMatchCustomers.Rows(0).Cells(2)


            Else
                If unmatchedXmlClients.Count > 0 Then
                    msgtxt = "There is unmatched OAC Customers but no TrueTrack customer to match with" & vbNewLine & "Reset TrueTrack and add another customer"
                    MessageBox.Show(msgtxt, "Customer Match Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    DialogResult = System.Windows.Forms.DialogResult.None
                    'Me.Close()
                Else
                    'No OAC customers and no TrueTrack customers to match > OK
                    DialogResult = System.Windows.Forms.DialogResult.OK
                    'Me.Close()
                End If


            End If


        Else
            MessageBox.Show("Error in fetching Customer details for matching", "Loading form error", MessageBoxButtons.OK)
            DialogResult = System.Windows.Forms.DialogResult.None
            'Me.Close()
        End If

    End Sub

    Public Sub SetFormCaption()
        If Not EnquiryCode = String.Empty Then
            Me.Text = EnquiryCode & " | Match currently unmatched Customers"
        Else
            Me.Text = "Match currently unmatched Customers"
        End If

    End Sub

   


    ''' <summary>
    ''' Save matches, abort if any unmatched
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        'XML:Save matches: 
        'on each row get OacCustomer.CustomerId and save to ttCustomer.XRefId
        'Remove current OacCustomer and ttCustomer from their respective lists
        'At end if any OacCustomers left in list then raise error message and abort
        'At end if any ttCustomer left in list then raise error message and abort
        'After matching complete then assign AMLRisk?? Not done now.
        Dim ttCustid As Integer
        Dim oacCustId As String

        For Each row As DataGridViewRow In dgvMatchCustomers.Rows
            ttCustid = CInt(row.Cells(0).Value)
            oacCustId = CStr(DirectCast(row.Cells(2), DataGridViewComboBoxCell).Value)
            If Not oacCustId Is Nothing And Not oacCustId = String.Empty Then

                finPowerOpObj.UpdateCustXRefId(ttCustid, oacCustId)

                'For OAC Customers                
                For Each kvp As KeyValuePair(Of Integer, finPowerOps.CustomerInfo) In unmatchedXmlClients
                    Try
                        If CInt(oacCustId) = kvp.Value.CustomerId Then
                            unmatchedXmlClients.Remove(kvp.Key)
                            Exit For
                        End If
                    Catch ex As Exception
                        MsgBox("FrmMatchCustomersLoad caused an error" & vbNewLine & ex.Message.ToString)
                    End Try
                Next

                'For TrueTrack Customers
                For Each kvp As KeyValuePair(Of Integer, finPowerOps.CustomerInfo) In unmatchedTTCustomers
                    Try
                        If CInt(ttCustid) = kvp.Value.CustomerId Then
                            unmatchedTTCustomers.Remove(kvp.Key)
                            Exit For
                        End If
                    Catch ex As Exception
                        MsgBox("FrmMatchCustomersLoad caused an error" & vbNewLine & ex.Message.ToString)
                    End Try
                Next

            End If
        Next

        If unmatchedXmlClients.Count > 0 Or unmatchedTTCustomers.Count > 0 Then
            Dim msgtxt As String = String.Empty
            If unmatchedXmlClients.Count > 0 Then
                msgtxt = msgtxt & "There is an unmatched OAC Customer"
            End If
            If unmatchedTTCustomers.Count > 0 Then
                If msgtxt.Length > 0 Then
                    msgtxt = msgtxt & vbNewLine & vbNewLine
                End If
                msgtxt = msgtxt & "There is an unmatched TrueTrack Customer"
            End If

            If msgtxt.Length > 0 Then
                MessageBox.Show(msgtxt, "Customer Match Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            DialogResult = System.Windows.Forms.DialogResult.None
            Me.Close()
        Else
            'Everything OK
            DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        End If

    End Sub
End Class

