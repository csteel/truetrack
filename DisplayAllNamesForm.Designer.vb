﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DisplayAllNamesForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisplayAllNamesForm))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DisplayAllNamesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.DisplayAllNamesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.DisplayAllNamesDataGridView = New System.Windows.Forms.DataGridView()
        Me.gpbxFilters = New System.Windows.Forms.GroupBox()
        Me.lblAppCodeFilter = New System.Windows.Forms.Label()
        Me.txtbxAppCodeFilter = New System.Windows.Forms.TextBox()
        Me.lblEnquiryCodeFilter = New System.Windows.Forms.Label()
        Me.txtbxEnquiryCodeFilter = New System.Windows.Forms.TextBox()
        Me.lblFirstNameFilter = New System.Windows.Forms.Label()
        Me.txtbxFirstNameFilter = New System.Windows.Forms.TextBox()
        Me.lblCityFilter = New System.Windows.Forms.Label()
        Me.txtbxCityFilter = New System.Windows.Forms.TextBox()
        Me.lblSuburbFilter = New System.Windows.Forms.Label()
        Me.txtbxSuburbFilter = New System.Windows.Forms.TextBox()
        Me.lblDisplayNameFilter = New System.Windows.Forms.Label()
        Me.txtbxDisplayNameFilter = New System.Windows.Forms.TextBox()
        Me.lblLastNameFilter = New System.Windows.Forms.Label()
        Me.txtbxLastNameFilter = New System.Windows.Forms.TextBox()
        Me.DisplayAllNamesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DisplayAllNamesTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Archived = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IsArchived = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.EnquiryId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactDisplayName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactSuburb = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactCity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApplicationCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DisplayAllNamesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DisplayAllNamesBindingNavigator.SuspendLayout()
        CType(Me.DisplayAllNamesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DisplayAllNamesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxFilters.SuspendLayout()
        Me.SuspendLayout()
        '
        'DisplayAllNamesBindingNavigator
        '
        Me.DisplayAllNamesBindingNavigator.AddNewItem = Nothing
        Me.DisplayAllNamesBindingNavigator.BindingSource = Me.DisplayAllNamesBindingSource
        Me.DisplayAllNamesBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.DisplayAllNamesBindingNavigator.DeleteItem = Nothing
        Me.DisplayAllNamesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.DisplayAllNamesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.DisplayAllNamesBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.DisplayAllNamesBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.DisplayAllNamesBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.DisplayAllNamesBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.DisplayAllNamesBindingNavigator.Name = "DisplayAllNamesBindingNavigator"
        Me.DisplayAllNamesBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.DisplayAllNamesBindingNavigator.Size = New System.Drawing.Size(944, 25)
        Me.DisplayAllNamesBindingNavigator.TabIndex = 0
        Me.DisplayAllNamesBindingNavigator.Text = "BindingNavigator1"
        '
        'DisplayAllNamesBindingSource
        '
        Me.DisplayAllNamesBindingSource.DataMember = "DisplayAllNames"
        Me.DisplayAllNamesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'DisplayAllNamesDataGridView
        '
        Me.DisplayAllNamesDataGridView.AllowUserToAddRows = False
        Me.DisplayAllNamesDataGridView.AllowUserToDeleteRows = False
        Me.DisplayAllNamesDataGridView.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        Me.DisplayAllNamesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DisplayAllNamesDataGridView.AutoGenerateColumns = False
        Me.DisplayAllNamesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DisplayAllNamesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Archived, Me.IsArchived, Me.EnquiryId, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.ContactDisplayName, Me.ContactSuburb, Me.ContactCity, Me.DataGridViewTextBoxColumn4, Me.EnquiryCode, Me.ApplicationCode})
        Me.DisplayAllNamesDataGridView.DataSource = Me.DisplayAllNamesBindingSource
        Me.DisplayAllNamesDataGridView.Location = New System.Drawing.Point(0, 91)
        Me.DisplayAllNamesDataGridView.MultiSelect = False
        Me.DisplayAllNamesDataGridView.Name = "DisplayAllNamesDataGridView"
        Me.DisplayAllNamesDataGridView.ReadOnly = True
        Me.DisplayAllNamesDataGridView.RowHeadersWidth = 20
        Me.DisplayAllNamesDataGridView.RowTemplate.Height = 20
        Me.DisplayAllNamesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DisplayAllNamesDataGridView.Size = New System.Drawing.Size(932, 611)
        Me.DisplayAllNamesDataGridView.TabIndex = 1
        '
        'gpbxFilters
        '
        Me.gpbxFilters.Controls.Add(Me.lblAppCodeFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxAppCodeFilter)
        Me.gpbxFilters.Controls.Add(Me.lblEnquiryCodeFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxEnquiryCodeFilter)
        Me.gpbxFilters.Controls.Add(Me.lblFirstNameFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxFirstNameFilter)
        Me.gpbxFilters.Controls.Add(Me.lblCityFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxCityFilter)
        Me.gpbxFilters.Controls.Add(Me.lblSuburbFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxSuburbFilter)
        Me.gpbxFilters.Controls.Add(Me.lblDisplayNameFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxDisplayNameFilter)
        Me.gpbxFilters.Controls.Add(Me.lblLastNameFilter)
        Me.gpbxFilters.Controls.Add(Me.txtbxLastNameFilter)
        Me.gpbxFilters.Location = New System.Drawing.Point(12, 28)
        Me.gpbxFilters.Name = "gpbxFilters"
        Me.gpbxFilters.Size = New System.Drawing.Size(920, 57)
        Me.gpbxFilters.TabIndex = 13
        Me.gpbxFilters.TabStop = False
        Me.gpbxFilters.Text = "Filters"
        '
        'lblAppCodeFilter
        '
        Me.lblAppCodeFilter.AutoSize = True
        Me.lblAppCodeFilter.Location = New System.Drawing.Point(787, 12)
        Me.lblAppCodeFilter.Name = "lblAppCodeFilter"
        Me.lblAppCodeFilter.Size = New System.Drawing.Size(87, 13)
        Me.lblAppCodeFilter.TabIndex = 25
        Me.lblAppCodeFilter.Text = "Application Code"
        '
        'txtbxAppCodeFilter
        '
        Me.txtbxAppCodeFilter.Location = New System.Drawing.Point(774, 33)
        Me.txtbxAppCodeFilter.Name = "txtbxAppCodeFilter"
        Me.txtbxAppCodeFilter.Size = New System.Drawing.Size(110, 20)
        Me.txtbxAppCodeFilter.TabIndex = 24
        '
        'lblEnquiryCodeFilter
        '
        Me.lblEnquiryCodeFilter.AutoSize = True
        Me.lblEnquiryCodeFilter.Location = New System.Drawing.Point(676, 12)
        Me.lblEnquiryCodeFilter.Name = "lblEnquiryCodeFilter"
        Me.lblEnquiryCodeFilter.Size = New System.Drawing.Size(70, 13)
        Me.lblEnquiryCodeFilter.TabIndex = 23
        Me.lblEnquiryCodeFilter.Text = "Enquiry Code"
        '
        'txtbxEnquiryCodeFilter
        '
        Me.txtbxEnquiryCodeFilter.Location = New System.Drawing.Point(658, 33)
        Me.txtbxEnquiryCodeFilter.Name = "txtbxEnquiryCodeFilter"
        Me.txtbxEnquiryCodeFilter.Size = New System.Drawing.Size(110, 20)
        Me.txtbxEnquiryCodeFilter.TabIndex = 22
        '
        'lblFirstNameFilter
        '
        Me.lblFirstNameFilter.AutoSize = True
        Me.lblFirstNameFilter.Location = New System.Drawing.Point(320, 12)
        Me.lblFirstNameFilter.Name = "lblFirstNameFilter"
        Me.lblFirstNameFilter.Size = New System.Drawing.Size(57, 13)
        Me.lblFirstNameFilter.TabIndex = 21
        Me.lblFirstNameFilter.Text = "First Name"
        '
        'txtbxFirstNameFilter
        '
        Me.txtbxFirstNameFilter.Location = New System.Drawing.Point(296, 33)
        Me.txtbxFirstNameFilter.Name = "txtbxFirstNameFilter"
        Me.txtbxFirstNameFilter.Size = New System.Drawing.Size(107, 20)
        Me.txtbxFirstNameFilter.TabIndex = 20
        '
        'lblCityFilter
        '
        Me.lblCityFilter.AutoSize = True
        Me.lblCityFilter.Location = New System.Drawing.Point(583, 12)
        Me.lblCityFilter.Name = "lblCityFilter"
        Me.lblCityFilter.Size = New System.Drawing.Size(24, 13)
        Me.lblCityFilter.TabIndex = 19
        Me.lblCityFilter.Text = "City"
        '
        'txtbxCityFilter
        '
        Me.txtbxCityFilter.Location = New System.Drawing.Point(543, 33)
        Me.txtbxCityFilter.Name = "txtbxCityFilter"
        Me.txtbxCityFilter.Size = New System.Drawing.Size(109, 20)
        Me.txtbxCityFilter.TabIndex = 18
        '
        'lblSuburbFilter
        '
        Me.lblSuburbFilter.AutoSize = True
        Me.lblSuburbFilter.Location = New System.Drawing.Point(452, 12)
        Me.lblSuburbFilter.Name = "lblSuburbFilter"
        Me.lblSuburbFilter.Size = New System.Drawing.Size(41, 13)
        Me.lblSuburbFilter.TabIndex = 17
        Me.lblSuburbFilter.Text = "Suburb"
        '
        'txtbxSuburbFilter
        '
        Me.txtbxSuburbFilter.Location = New System.Drawing.Point(409, 33)
        Me.txtbxSuburbFilter.Name = "txtbxSuburbFilter"
        Me.txtbxSuburbFilter.Size = New System.Drawing.Size(128, 20)
        Me.txtbxSuburbFilter.TabIndex = 16
        '
        'lblDisplayNameFilter
        '
        Me.lblDisplayNameFilter.AutoSize = True
        Me.lblDisplayNameFilter.Location = New System.Drawing.Point(65, 12)
        Me.lblDisplayNameFilter.Name = "lblDisplayNameFilter"
        Me.lblDisplayNameFilter.Size = New System.Drawing.Size(72, 13)
        Me.lblDisplayNameFilter.TabIndex = 15
        Me.lblDisplayNameFilter.Text = "Display Name"
        '
        'txtbxDisplayNameFilter
        '
        Me.txtbxDisplayNameFilter.Location = New System.Drawing.Point(36, 33)
        Me.txtbxDisplayNameFilter.Name = "txtbxDisplayNameFilter"
        Me.txtbxDisplayNameFilter.Size = New System.Drawing.Size(133, 20)
        Me.txtbxDisplayNameFilter.TabIndex = 14
        '
        'lblLastNameFilter
        '
        Me.lblLastNameFilter.AutoSize = True
        Me.lblLastNameFilter.Location = New System.Drawing.Point(202, 12)
        Me.lblLastNameFilter.Name = "lblLastNameFilter"
        Me.lblLastNameFilter.Size = New System.Drawing.Size(58, 13)
        Me.lblLastNameFilter.TabIndex = 13
        Me.lblLastNameFilter.Text = "Last Name"
        '
        'txtbxLastNameFilter
        '
        Me.txtbxLastNameFilter.Location = New System.Drawing.Point(175, 33)
        Me.txtbxLastNameFilter.Name = "txtbxLastNameFilter"
        Me.txtbxLastNameFilter.Size = New System.Drawing.Size(115, 20)
        Me.txtbxLastNameFilter.TabIndex = 12
        '
        'DisplayAllNamesTableAdapter
        '
        Me.DisplayAllNamesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "EnquiryCode"
        Me.DataGridViewTextBoxColumn2.HeaderText = "EnquiryCode"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 110
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "DateTime"
        Me.DataGridViewTextBoxColumn7.HeaderText = "DateTime"
        Me.DataGridViewTextBoxColumn7.MinimumWidth = 110
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "CurrentStatus"
        Me.DataGridViewTextBoxColumn10.HeaderText = "CurrentStatus"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 95
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "EnquiryCode"
        Me.DataGridViewTextBoxColumn11.HeaderText = "EnquiryCode"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 80
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "ApplicationCode"
        Me.DataGridViewTextBoxColumn12.HeaderText = "ApplicationCode"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 90
        '
        'Archived
        '
        Me.Archived.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Archived.DataPropertyName = "Archived"
        Me.Archived.HeaderText = "Archived"
        Me.Archived.Name = "Archived"
        Me.Archived.ReadOnly = True
        Me.Archived.Visible = False
        '
        'IsArchived
        '
        Me.IsArchived.DataPropertyName = "IsArchived"
        Me.IsArchived.HeaderText = "IsArchived"
        Me.IsArchived.Name = "IsArchived"
        Me.IsArchived.ReadOnly = True
        Me.IsArchived.Visible = False
        '
        'EnquiryId
        '
        Me.EnquiryId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.EnquiryId.DataPropertyName = "EnquiryId"
        Me.EnquiryId.HeaderText = "EnquiryId"
        Me.EnquiryId.MinimumWidth = 110
        Me.EnquiryId.Name = "EnquiryId"
        Me.EnquiryId.ReadOnly = True
        Me.EnquiryId.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "DateTime"
        Me.DataGridViewTextBoxColumn3.HeaderText = "DateTime"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 75
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "LastName"
        Me.DataGridViewTextBoxColumn5.HeaderText = "LastName"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 105
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "FirstName"
        Me.DataGridViewTextBoxColumn6.HeaderText = "FirstName"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 95
        '
        'ContactDisplayName
        '
        Me.ContactDisplayName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ContactDisplayName.DataPropertyName = "ContactDisplayName"
        Me.ContactDisplayName.HeaderText = "ContactDisplayName"
        Me.ContactDisplayName.MinimumWidth = 110
        Me.ContactDisplayName.Name = "ContactDisplayName"
        Me.ContactDisplayName.ReadOnly = True
        '
        'ContactSuburb
        '
        Me.ContactSuburb.DataPropertyName = "ContactSuburb"
        Me.ContactSuburb.HeaderText = "ContactSuburb"
        Me.ContactSuburb.Name = "ContactSuburb"
        Me.ContactSuburb.ReadOnly = True
        '
        'ContactCity
        '
        Me.ContactCity.DataPropertyName = "ContactCity"
        Me.ContactCity.HeaderText = "ContactCity"
        Me.ContactCity.Name = "ContactCity"
        Me.ContactCity.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "CurrentStatus"
        Me.DataGridViewTextBoxColumn4.HeaderText = "CurrentStatus"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 95
        '
        'EnquiryCode
        '
        Me.EnquiryCode.DataPropertyName = "EnquiryCode"
        Me.EnquiryCode.HeaderText = "EnquiryCode"
        Me.EnquiryCode.Name = "EnquiryCode"
        Me.EnquiryCode.ReadOnly = True
        Me.EnquiryCode.Width = 80
        '
        'ApplicationCode
        '
        Me.ApplicationCode.DataPropertyName = "ApplicationCode"
        Me.ApplicationCode.HeaderText = "ApplicationCode"
        Me.ApplicationCode.Name = "ApplicationCode"
        Me.ApplicationCode.ReadOnly = True
        Me.ApplicationCode.Width = 90
        '
        'DisplayAllNamesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.DisplayAllNamesSize
        Me.Controls.Add(Me.gpbxFilters)
        Me.Controls.Add(Me.DisplayAllNamesDataGridView)
        Me.Controls.Add(Me.DisplayAllNamesBindingNavigator)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "DisplayAllNamesLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "DisplayAllNamesSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.DisplayAllNamesLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(960, 750)
        Me.Name = "DisplayAllNamesForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "TrueTrack Names"
        CType(Me.DisplayAllNamesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DisplayAllNamesBindingNavigator.ResumeLayout(False)
        Me.DisplayAllNamesBindingNavigator.PerformLayout()
        CType(Me.DisplayAllNamesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DisplayAllNamesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxFilters.ResumeLayout(False)
        Me.gpbxFilters.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents DisplayAllNamesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DisplayAllNamesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DisplayAllNamesTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents DisplayAllNamesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents DisplayAllNamesDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxFilters As System.Windows.Forms.GroupBox
    Friend WithEvents lblFirstNameFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxFirstNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblCityFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxCityFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblSuburbFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxSuburbFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblDisplayNameFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxDisplayNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblLastNameFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxLastNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblEnquiryCodeFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxEnquiryCodeFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblAppCodeFilter As System.Windows.Forms.Label
    Friend WithEvents txtbxAppCodeFilter As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Archived As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsArchived As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents EnquiryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactDisplayName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactSuburb As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnquiryCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ApplicationCode As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
