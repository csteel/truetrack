﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewDocsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ViewDocsForm))
        Me.lvDocuments = New System.Windows.Forms.ListView()
        Me.SuspendLayout()
        '
        'lvDocuments
        '
        Me.lvDocuments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDocuments.Location = New System.Drawing.Point(0, 0)
        Me.lvDocuments.Name = "lvDocuments"
        Me.lvDocuments.Size = New System.Drawing.Size(618, 362)
        Me.lvDocuments.TabIndex = 0
        Me.lvDocuments.UseCompatibleStateImageBehavior = False
        Me.lvDocuments.View = System.Windows.Forms.View.Details
        '
        'ViewDocsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(618, 362)
        Me.Controls.Add(Me.lvDocuments)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(634, 400)
        Me.MinimumSize = New System.Drawing.Size(634, 400)
        Me.Name = "ViewDocsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "View Documents Form"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvDocuments As System.Windows.Forms.ListView
End Class
