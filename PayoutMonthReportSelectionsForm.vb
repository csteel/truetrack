﻿Public Class PayoutMonthReportSelectionsForm
    Dim GroupBy As String
    Dim OrderBy As String
    Dim User As String = ""
    Dim ThisPayoutStartMonth As Integer = 0
    Dim ThisPayoutStartYear As Integer = 0
    Dim ThisPayoutEndMonth As Integer = 0
    Dim ThisPayoutEndYear As Integer = 0


    Private Sub PayoutMonthReportSelectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.Users' table. You can move, or remove it, as needed.
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)
        'set default values
        Me.cmbxGroupBy.SelectedIndex = 0
        Me.cmbxOrderBy.SelectedIndex = 0

    End Sub

    Private Sub ckbxUsers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxUsers.CheckedChanged
        If ckbxUsers.CheckState = CheckState.Checked Then
            lstbxUsers.Enabled = True
        Else
            lstbxUsers.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'check all date boxes selected
        Dim checkFieldsMessage As String = ""
        Try
            If Not IsNothing(ThisPayoutStartMonth) And Not IsNothing(ThisPayoutStartYear) And Not IsNothing(ThisPayoutEndMonth) And Not IsNothing(ThisPayoutEndYear) Then
                If ThisPayoutStartMonth < 1 Or ThisPayoutStartYear < 1 Or ThisPayoutEndMonth < 1 Or ThisPayoutEndYear < 1 Then
                    checkFieldsMessage = checkFieldsMessage + "Please check your Date Entries" + vbCrLf
                End If
            Else
                checkFieldsMessage = checkFieldsMessage + "Please check your Date Entries" + vbCrLf
            End If
        Catch ex As Exception
            MsgBox("Error checking values: " & ex.Message)
        End Try
        'set dates
        Try
            Dim StartDate As New Date(CInt(ThisPayoutStartYear), CInt(ThisPayoutStartMonth), 1, 0, 0, 0)
            Dim EndDate As New Date(CInt(ThisPayoutEndYear), CInt(ThisPayoutEndMonth), 1, 0, 0, 0)
            'check dates are viable
            If Not StartDate <= EndDate Then
                checkFieldsMessage = checkFieldsMessage + "Please check your Date Entries" + vbCrLf
            End If

            If Not checkFieldsMessage = "" Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR: There are errors please view message box."
                MsgBox(checkFieldsMessage)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
            Else
                Cursor.Current = Cursors.WaitCursor
                'get values

                'get users if selected
                If lstbxUsers.Enabled = True Then
                    User = lstbxUsers.SelectedValue.ToString
                    'MsgBox("Selected User = " + User)
                Else
                    User = ""
                End If
                'get Group By
                GroupBy = cmbxGroupBy.SelectedItem.ToString
                'get Order by
                OrderBy = cmbxOrderBy.SelectedItem.ToString
                Try
                    'launch ReportPayoutDateForm and pass Enquiry parameters
                    Dim ReportPayoutMonthFrm As New ReportPayoutMonthForm
                    ReportPayoutMonthFrm.ReportSelections_PassVariable(StartDate, EndDate, GroupBy, OrderBy, User)
                    ReportPayoutMonthFrm.Show()
                Catch ex As Exception
                    MsgBox("Error launching ReportPayoutMonthForm: " + vbCrLf + ex.Message)
                End Try
                Cursor.Current = Cursors.Default
            End If

        Catch ex As ArgumentOutOfRangeException
            Dim inner As Exception = ex.InnerException
            If Not (inner Is Nothing) Then
                MsgBox("ArgumentOutOfRangeException: " & vbCrLf & inner.Message & vbCrLf & "Please check your Date Entries")
                inner = inner.InnerException
            Else
                MsgBox("ArgumentOutOfRangeException Error: " & vbCrLf & ex.Message & vbCrLf & "Please check your Date Entries")
            End If


        Catch ex As Exception
            MsgBox("Error checking values: " & ex.Message)
        End Try
       

    End Sub

    Private Sub cmbxStartMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxStartMonth.SelectedIndexChanged
        ThisPayoutStartMonth = cmbxStartMonth.SelectedIndex + 1
    End Sub

    Private Sub cmbxStartYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxStartYear.SelectedIndexChanged
        ThisPayoutStartYear = cmbxStartYear.SelectedItem
    End Sub

    Private Sub cmbxEndMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxEndMonth.SelectedIndexChanged
        ThisPayoutEndMonth = cmbxEndMonth.SelectedIndex + 1
    End Sub

    Private Sub cmbxEndYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxEndYear.SelectedIndexChanged
        ThisPayoutEndYear = cmbxEndYear.SelectedItem
    End Sub
End Class