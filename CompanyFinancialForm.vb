﻿Imports System.IO
Imports System.Data.SqlClient

Public Class CompanyFinancialForm
    ' set New Enquiry Id 
    Dim ThisEnquiryId As Integer
    Dim ThisEnquiryCode As String ' set Enquiry Code
    Dim ThisEnquiryType As Integer
    ' set New Wizard Status 
    Dim ThisWizardStatus As Integer
    Dim QRGStatus As Integer 'set QRG Status for colour
    ' set New Client Id 
    'Dim ThisClientId As Integer
    '' set Client Salutation
    'Dim ThisClientSalutation As String
    Dim ThisEnquiryManagerId As Integer 'set EnquiryManagerId
    'Dim ThisLoanReason As String
    'Dim ThisTypeOfClient As String ' set Type of Client
    'Dim ClientName As String 'Cannotate client name
    'Dim ClientLastName As String
    'Dim ClientAddress As String
    'Dim JointName As String 'Cannotated Joint Name
    Dim ThisContactDisplayName As String
    Dim ContactName As String
    Dim ContactLastName As String
    Dim ContactAddress As String
    Dim ThisTypeOfMainCustomer As Integer ' set Type of Main Client

    Dim ThisDealerId As String
    Dim ThisDealerName As String
    Dim ThisEnquiryResult As String = ""
    Dim ProgressStatus As Boolean = False
    Dim FormTitle As String = "Due-diligence Financials" 'Me.Text
    'user settings
    Dim NewLocX As Integer
    Dim NewLocY As Integer
    'store form load information
    Public frmLoadInfo As System.Text.StringBuilder = New System.Text.StringBuilder
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Loads form with parameters from WkShtWizForm1
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)
        Me.SuspendLayout()
        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)
        ' setup the reference variables
        ThisEnquiryId = loadEnquiryVar
        'set the form tag with EnquiryId
        Me.Tag = ThisEnquiryId
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
            ThisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
            ThisWizardStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("WizardStatus")
            ThisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            ThisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType")
            ThisTypeOfMainCustomer = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")
            'set EnquiryType label
            lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(ThisEnquiryType, MyEnums.EnquiryType))
            'Load User table
            UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, ThisEnquiryManagerId)
            'get UserName
            lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
            'get Dealer ID
            ThisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")

            '************************* Set EnquiryType (old LoanReason) display options
            If ThisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or ThisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or ThisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or ThisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or ThisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or ThisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurrentLoan.Visible = True
                lblCurrentLoanAmt.Visible = True
            Else
                lblCurrentLoan.Visible = False
                lblCurrentLoanAmt.Visible = False
            End If

            'set documents button visibility
            'Get String to WorkSheet Drive
            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
            Dim folderPath As String = worksheetDrive & "\" & ThisEnquiryCode
            'Check directory exists
            If Directory.Exists(folderPath) Then
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
            Else
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
                'deactivate drag and drop
                Me.btnDocs.AllowDrop = False
            End If

            '************************* Get Contact Display Name
            ThisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
            '************************* Set form title
            Select Case ThisTypeOfMainCustomer
                Case MyEnums.mainCustomerType.SoleTrader, MyEnums.mainCustomerType.Partnership
                    FormTitle = "Due Diligence - Trading Activity"
                Case MyEnums.mainCustomerType.Company
                    FormTitle = "Due Diligence - Company Financials"
                Case MyEnums.mainCustomerType.Trust
                    FormTitle = "Due Diligence - Trust Financials"
                Case Else

            End Select

            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & ThisContactDisplayName & " - " & FormTitle
            Else
                Me.Text = ThisContactDisplayName & " - " & FormTitle
            End If

            '************************* Cannotate Contact Name           
            ContactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
            ContactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " & _
            EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & ContactLastName
            'set lblContactName
            lblContactName.Text = ContactName
            'Contact address
            ContactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
            lblContactAddress.Text = ContactAddress

            '*************** Get Dealer name
            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & ThisDealerId & "'"
            'MsgBox("Select Statement = " & selectStatement)
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim DealerTempDataAdapter As New SqlDataAdapter(selectCommand)
            Dim DealerTempDataSet As New DataSet
            Dim DealerTempDataTable As New DataTable
            Try
                'dumps results into datatable LoginDataTable
                DealerTempDataAdapter.Fill(DealerTempDataTable)
                'if no matching rows .....
                If DealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    DealerTempDataAdapter = Nothing
                    DealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf DealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim DealerTempDataRow As DataRow = DealerTempDataTable.Rows(0)
                    ThisDealerName = DealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    DealerTempDataAdapter = Nothing
                    DealerTempDataTable.Clear()
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
            lblDealerName.Text = ThisDealerName
            '*************** End of Get Dealer name

            '*****************************  get QRGList status
            Dim connectionString1 As String = My.Settings.EnquiryWorkSheetConnectionString
            Dim connection1 As New SqlConnection(connectionString1)
            Dim selectStatement1 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & ThisEnquiryId & "'"
            'MsgBox("Select Statement = " & selectStatement1)
            Dim selectCommand1 As New SqlCommand(selectStatement1, connection1)
            Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand1)
            Dim QRGListTempDataSet As New DataSet
            Dim QRGListTempDataTable As New DataTable
            Try
                'dumps results into datatable LoginDataTable
                QRGListTempDataAdapter.Fill(QRGListTempDataTable)
                'if no matching rows .....
                If QRGListTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("ERROR: No QRG List, please try again.")
                    'clear the dataTable and the Connect information
                    QRGListTempDataAdapter = Nothing
                    QRGListTempDataTable.Clear()
                    'if there is a matching row
                ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                    QRGStatus = QRGListTempDataRow.Item(0)
                    'MsgBox("QRG Status = " & QRGStatus)
                    'clear the dataTable and the Connect information
                    QRGListTempDataAdapter = Nothing
                    QRGListTempDataTable.Clear()
                End If
                'close the connection
                If connection1.State <> ConnectionState.Closed Then
                    connection1.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

            'set QRGStatus button colour
            Select Case QRGStatus
                Case 0
                    btnQRGList.BackColor = Color.Transparent
                Case 1
                    btnQRGList.BackColor = Color.Tomato
                Case 2
                    btnQRGList.BackColor = Color.LightGreen
                Case Else
                    btnQRGList.BackColor = Color.Transparent
            End Select
            '************* End of QRG List
            '*************** Set permissions
            'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
            Select Case LoggedinPermissionLevel
                Case Is = 1 'Readonly
                    UpdateAllToolStripMenuItem.Visible = False
                    btnSaveAndExit.Enabled = False
                    btnFinish.Enabled = False
                    btnDocs.Enabled = False
                    '-----------------------------------
                    gpbxIncomeExpenses.Enabled = False
                    gpbxSalaries.Enabled = False
                    gpbxEarnings.Enabled = False
                    gpbxPosition.Enabled = False

                Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    btnFinish.Enabled = False
                    '-----------------------------------
                    gpbxIncomeExpenses.Enabled = False
                    gpbxSalaries.Enabled = False
                    gpbxEarnings.Enabled = False
                    gpbxPosition.Enabled = False

                Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                    'Can see /use Archive Enquiry in File menu for their user status
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True
                Case Is = 4
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True

                Case Is = 5
                    'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True

                Case Is > 5 'Systems Manager Level, Open.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True

                Case Else 'Readonly
                    UpdateAllToolStripMenuItem.Visible = False
                    btnSaveAndExit.Enabled = False
                    btnFinish.Enabled = False
                    btnDocs.Enabled = False


            End Select
            '*************** end of Set permissions

            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try



    End Sub

    Private Sub CompanyFinancialForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case ThisTypeOfMainCustomer
            Case MyEnums.mainCustomerType.SoleTrader, MyEnums.mainCustomerType.Partnership
                gpbxSalaries.Enabled = False
                gpbxSalaries.Visible = False
                tlpPosition.Enabled = False
                tlpPosition.Visible = False
                ckbxPAYEreturns.Enabled = False
                ckbxPAYEreturns.Visible = False
            Case Else

        End Select

        
        Me.ResumeLayout()

    End Sub

    '******************************* MenuStrip code
#Region "MenuStrip"

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            'Me.ClientBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(ThisEnquiryId)
            'AddSecurityFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()

                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            log.Error("SearchForLastNameLike ERROR:  " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub


    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error("DisplayAllNamesForm ERROR:  " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    '************ end of Check Names

    Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            'Me.ClientBindingSource.EndEdit()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        FormRefresh()
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(ThisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EditCandDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCandDToolStripMenuItem.Click

        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(ThisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(ThisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailFrm As New EmailForm
        EmailFrm.PassVariable(ThisEnquiryId, ThisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailFrm.Dispose()

    End Sub

    Private Sub ApprovalFormToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem1.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub WWWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WWWToolStripMenuItem.Click
        Dim navUrl As String = ""
        Try
            Cursor.Current = Cursors.WaitCursor
            'launch internet browser for web application
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Launching web browser."
            If My.Settings.IsTest = True Then
                navUrl = My.Settings.WebApplicationPage_TEST
            Else
                navUrl = My.Settings.WebApplicationPage_PROD
            End If
            System.Diagnostics.Process.Start(navUrl)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Launching web browser problem: " & ex.Message)
        End Try
    End Sub

    Private Sub DeclineToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeclineToolStripMenuItem.Click
        ThisEnquiryResult = Constants.CONST_CURRENT_STATUS_DECLINED
        ProgressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        'If Not EnquiryBindingSource.Current Is Nothing And Not ClientBindingSource.Current Is Nothing Then
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(ThisEnquiryId, ThisContactDisplayName, ThisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                DeclinedFrm.Dispose()

                ProgressStatus = True

            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                ProgressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If ProgressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(ThisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub WithdrawToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawToolStripMenuItem.Click
        ThisEnquiryResult = Constants.CONST_CURRENT_STATUS_WITHDRAWN
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        'If Not EnquiryBindingSource.Current Is Nothing And Not ClientBindingSource.Current Is Nothing Then
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(ThisEnquiryId, ThisContactDisplayName, ThisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                DeclinedFrm.Dispose()

                ProgressStatus = True

            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                ProgressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Withdrawing of Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If ProgressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(ThisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

#End Region
    '******************************* end of MenuStrip code

    Private Sub FormRefresh()
        Try
            Call PassVariable(ThisEnquiryId)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '************************** Document buttons

    Private Sub btnDocs_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & ThisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Drag and drop successful.    Status: Ready"
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Drag and Drop caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnDocs_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & ThisEnquiryCode
        'Determine whether the directory exists.
        If Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        Else
            Try
                'create directory
                IO.Directory.CreateDirectory(fname)
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
                'refresh form
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
            Catch ex As System.IO.DirectoryNotFoundException
                ' Let the user know that the directory did not exist.
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

        End If
    End Sub

    Private Sub btnViewDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDocs.Click
        Dim WkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = WkshtStr & "\" & ThisEnquiryCode
        'Determine whether the directory exists.
        If Not Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Shared Folder does not exist!."
        Else
            Dim viewDocsFrm As New ViewDocsForm
            viewDocsFrm.PassVariable(ThisEnquiryCode, ThisContactDisplayName)
            viewDocsFrm.ShowDialog(Me)
            If viewDocsFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                'ViewDocsFrm.Dispose()
            Else
                'ViewDocsFrm.Dispose()
            End If
            viewDocsFrm.Dispose()
        End If
    End Sub

    Private Sub btnQRGList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQRGList.Click
        Try
            'MsgBox("ThisQRGListId = " & ThisQRGListId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Quick reference Guide List."
            Dim QRGFrm As New QRGForm
            QRGFrm.PassVariable(ThisEnquiryId, ThisEnquiryCode, ThisContactDisplayName)
            'AddSecurityFrm.ShowDialog(Me)
            If (QRGFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                '*****************************  get QRGList status
                Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection2 As New SqlConnection(connectionString2)
                Dim selectStatement2 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & ThisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                Dim QRGListTempDataAdapter As New SqlDataAdapter(selectCommand2)
                Dim QRGListTempDataSet As New DataSet
                Dim QRGListTempDataTable As New DataTable
                Try
                    'dumps results into datatable LoginDataTable
                    QRGListTempDataAdapter.Fill(QRGListTempDataTable)
                    'if no matching rows .....
                    If QRGListTempDataTable.Rows.Count = 0 Then
                        MessageBox.Show("ERROR: No QRG List, please try again.")
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                        'if there is a matching row
                    ElseIf QRGListTempDataTable.Rows.Count = 1 Then
                        'get  QRGStatus value
                        Dim QRGListTempDataRow As DataRow = QRGListTempDataTable.Rows(0)
                        QRGStatus = QRGListTempDataRow.Item(0)
                        'MsgBox("QRG Status = " & QRGStatus)
                        'clear the dataTable and the Connect information
                        QRGListTempDataAdapter = Nothing
                        QRGListTempDataTable.Clear()
                    End If
                    'close the connection
                    If connection2.State <> ConnectionState.Closed Then
                        connection2.Close()
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

                'set QRGStatus button colour
                Select Case QRGStatus
                    Case 0
                        btnQRGList.BackColor = Color.Transparent
                    Case 1
                        btnQRGList.BackColor = Color.Tomato
                    Case 2
                        btnQRGList.BackColor = Color.LightGreen
                    Case Else
                        btnQRGList.BackColor = Color.Transparent
                End Select
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Quick reference Guide List updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Quick reference Guide List cancelled. Status: Ready."
            End If
            QRGFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub

    '************************** end of Document buttons
    '************************** Navigation buttons

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnquiryBindingSource.CancelEdit()
        'ClientBindingSource.CancelEdit()
        UsersBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            'check if data has changed
            Dim rowView2 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not EnquiryBindingSource.Current Is Nothing Then
                rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
                'test if any fields have changed
                If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or _
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        Try
                            Dim ThisResult As Integer
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            Me.Validate()
                            EnquiryBindingSource.EndEdit()
                            ThisResult = Me.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                            'update Status Strip
                            MsgBox("Result = " & ThisResult)
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Changes saved successfully."
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            MsgBox(ex.Message)
                        End Try
                    Else
                        ' User choose No. Do nothing
                    End If
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'launch WkShtWizForm2 (security) 
        Dim WkShtWizFrm2 As New WkShtWizForm2
        Try
            WkShtWizFrm2.PassVariable(ThisEnquiryId)
            WkShtWizFrm2.Show()
            'Close this Form
            Me.Close()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Cursor.Current = Cursors.WaitCursor
        sw = New Stopwatch
        sw.Start()
        frmLoadInfo.Clear()
        frmLoadInfo.Append("Start Trace **************" & " ")
        frmLoadInfo.Append("BtnNextClick: start sub - " & sw.Elapsed.ToString & ". ")
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        'check data entry
        Dim checkFieldsMessage As String = ""


        If checkFieldsMessage.Length > 0 Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR: There are errors please view message box."
            MsgBox(checkFieldsMessage)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Else
            Try
                Dim ProgressStatus As Boolean = False
                Dim rowView2 As DataRowView
                'leave if bindingsource.current is nothing (no data)
                If Not Me.EnquiryBindingSource.Current Is Nothing Then
                    rowView2 = CType(Me.EnquiryBindingSource.Current, DataRowView)
                    'test if any fields have changed
                    If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        ''update WizardStatus
                        'Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                        'EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                        'EnquiryRow.BeginEdit()
                        'EnquiryRow.WizardStatus = 6
                        'save changes
                        Me.Validate()
                        Me.EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        ProgressStatus = True
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Else
                        'no changes to save - proceed
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "No changes to save."
                        ProgressStatus = True
                    End If
                Else
                    'no current record in binding source - do not proceed
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                    ProgressStatus = False
                End If

                If ProgressStatus = True Then
                    Select Case ThisTypeOfMainCustomer
                        Case MyEnums.mainCustomerType.SoleTrader, MyEnums.mainCustomerType.Partnership
                            'launch WkShtWizForm4 (personal budget)
                            Try
                                sw.Stop()
                                Dim wkShtWizFrm4 As New WkShtWizForm4
                                wkShtWizFrm4.PassVariable(ThisEnquiryId)
                                wkShtWizFrm4.Show()
                                'Close this Form
                                Me.Close()
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox("Launching WkShtWizFrm4 caused an error:" & vbCrLf & ex.Message)
                            End Try
                        Case Else
                            'launch WkShtWizForm3 (customers)
                            Try
                                frmLoadInfo.Append("BtnNextClick: Launching next form - " & sw.Elapsed.ToString & ". ")
                                log.Debug(frmLoadInfo.ToString)
                                ToolStripStatusLabel1.Text = "Launching next form."
                                Application.DoEvents()
                                Dim wkShtWizFrm3 As New WkShtWizForm3
                                wkShtWizFrm3.PassVariable(ThisEnquiryId)
                                wkShtWizFrm3.Show()
                                log.Debug("BtnNextClick: wkShtWizFrm3.Show() - " & sw.Elapsed.ToString)
                                'Close this Form
                                Me.Close()
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox("Launching WkShtWizFrm3 caused an error:" & vbCrLf & ex.Message)
                            End Try
                    End Select
                Else
                    'MsgBox("No record to save, please enter information")
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Red
                    ToolStripStatusLabel1.Text = "No record to save, please enter information!"
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
        End If
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub btnSaveAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndExit.Click
        Try
            'save changes
            Cursor.Current = Cursors.WaitCursor
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            'Me.ClientBindingSource.CancelEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
            'close form
            Me.Close()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Save and Exit: " + vbCrLf + ex.Message)
        End Try
    End Sub

    '************************** end of Navigation buttons

    Private Sub CompanyFinancialForm_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            'lblStabilityTitle.Location = New Point(352 - (960 - formWidth) / 2, 5)
            Panel2.Size = New Size(formWidth - 16, formHeight - 204)
            'IncomeExpenses
            gpbxIncomeExpenses.Size = New Size(formWidth - 50, 301)
            tlpIncomeExpenses.Size = New Size(formWidth - 62, 276)
            txtbxIncomeNotes.Size = New Size(formWidth - 371, 69)
            txtbxExpenseNotes.Size = New Size(formWidth - 371, 69)
            Select Case ThisTypeOfMainCustomer
                Case MyEnums.mainCustomerType.SoleTrader, MyEnums.mainCustomerType.Partnership
                    gpbxEarnings.Location = New Point(12, 310)
                    'Position
                    gpbxPosition.Location = New Point(12, 413)
                    gpbxPosition.Size = New Size(formWidth - 50, 227)
                    tlpPosition2.Location = New Point(6, 19)
                Case Else
                    'Salaries
                    gpbxSalaries.Size = New Size(formWidth - 50, 96)
                    tlpSalaries.Size = New Size(formWidth - 62, 72)
                    txtbxSalaries.Size = New Size(formWidth - 371, 36)
                    'Position
                    gpbxPosition.Size = New Size(formWidth - 50, 298)
                    tlpPosition.Size = New Size(formWidth - 62, 74)
                    txtbxDebt.Size = New Size(formWidth - 371, 36)
                    
            End Select
            'Earnings
            gpbxEarnings.Size = New Size(formWidth - 50, 96)
            tlpEarnings.Size = New Size(formWidth - 62, 71)
            txtbxEarnings.Size = New Size(formWidth - 371, 36)
            'Position
            tlpPosition2.Size = New Size(formWidth - 62, 202)
            txtbxAccountsPayable.Size = New Size(formWidth - 371, 36)
            txtbxTaxes.Size = New Size(formWidth - 371, 36)

        End If
    End Sub

    Private Sub CompanyFinancialForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SuspendLayout()
        'check if data has changed
        Dim rowView1 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not Me.EnquiryBindingSource.Current Is Nothing Then
            rowView1 = CType(Me.EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        Me.EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If

        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            NewLocX = 0
        Else
            NewLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            NewLocY = 0
        Else
            NewLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.PrelimFormLocation = New Point(NewLocX, NewLocY)
        My.Settings.PrelimFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
    End Sub

#Region "Handle MouseWheel"

    ''' <summary>
    ''' Capture mouse wheel events to ride them up to the host panel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ControlsInPanel_MouseWheel(sender As Object, e As MouseEventArgs)
        Dim parentPanel As Control
        parentPanel = Util.GetParentPanel(sender)
        parentPanel.Focus()
    End Sub


#End Region
   
End Class