﻿Public Class ChangeDealerForm

    Dim ThisEnquiryId As Integer


    'Loads form with parameters from WkShtWizForm1
    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer)
        ThisEnquiryId = LoadEnquiryVar
        'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
        Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
        Dim RowCount As Integer = EnquiryWorkSheetDataSet.Enquiry.Rows.Count
        'MsgBox("Enquiry Id = " & ThisEnquiryId & vbCrLf & "Number of rows in Enquiry Table = " & RowCount)

    End Sub

    Private Sub ChangeDealerForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'loads data into the 'EnquiryWorkSheetDataSet.ActiveDealers' table.
        Me.ActiveDealersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.ActiveDealers)
        cmbxDealerName.SelectedIndex = -1

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'update fields
            Dim RowCount As Integer = EnquiryWorkSheetDataSet.Enquiry.Rows.Count
            If RowCount > 0 Then
                Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                EnquiryRow.BeginEdit()
                EnquiryRow.DealerId = lblDealerIdValue.Text
                'create log note
                'Dim DateStr As String
                'Dim NewCommentStr As String
                Dim CommentStr As String
                'Dim LoanComments As String = EnquiryRow.LoanComments
                ''Create Date string
                'DateStr = "-- " & DateTime.Now.ToString("f")
                ''Add Date string to comments
                CommentStr = "Dealer changed"
                ''check loan comments from dataset exist
                'If Not LoanComments = "" Then
                '    NewCommentStr = CommentStr & LoanComments
                'Else
                '    NewCommentStr = CommentStr
                'End If
                ''update LoanComments 
                'EnquiryRow.LoanComments = NewCommentStr

                Dim commentId As Integer
                commentId = Util.SetComment(ThisEnquiryId, LoggedinName, CommentStr)

                'save changes
                Me.Validate()
                EnquiryRow.EndEdit()
                EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)

                'Close this Form
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Else
                MsgBox("There is no data in Enquiry Datatable" & vbCrLf & "Operation cancelled")
            End If
            
        Catch ex As Exception
            MsgBox("btnSave caused an error:" & vbCrLf & ex.Message)
        End Try
        
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnquiryBindingSource.CancelEdit()
        'Close this Form
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

   
End Class