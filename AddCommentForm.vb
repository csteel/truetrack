﻿Imports System.Text


Public Class AddCommentForm
    ' set New Enquiry Id 
    Dim thisEnquiryId As Integer
    'Dim LoanComments As String
    Dim SaveToolStripButtonClicked As Boolean = False
    'to allow editing of existing comment
    Dim thisCommentId As Integer
    Dim thisFormMode As Integer
    Dim isDirty As Boolean
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Private Sub AddCommentForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer, Optional ByVal commentId As Integer = 0, Optional ByVal formMode As Integer = 1)
        SuspendLayout()
        ' setup the reference variables
        thisEnquiryId = LoadEnquiryVar
        thisCommentId = commentId
        thisFormMode = formMode

        'get user settings (on the Load Form Event)
        Me.ClientSize = New Size(My.Settings.AddPrelimCommentFormSize)
        Me.Location = New Point(My.Settings.AddPrelimCommentFormLocation)

        If thisCommentId > 0 Then 'load comment
            Try
                Me.EnquiryCommentTableAdapter.FillByCommentId(Me.EnquiryWorkSheetDataSet.EnquiryComment, thisCommentId)
                Dim commentDataRow As DataRow = Me.EnquiryWorkSheetDataSet.EnquiryComment.Rows(0)
                txtbxComment.Text = commentDataRow.Item("Text")

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Error loading comment", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

        'Set Mode
        If formMode = MyEnums.Mode.ReadOnly Then 'ReadOnly
            ToolStrip1.Enabled = False
        End If

        If txtbxComment.IsUndoEnabled = False Then
            UndoToolStripButton.Image = AppWhShtB.My.Resources.undoImageNotActive
            UndoToolStripButton.Text = "Un&do Not Active"
        End If

        ResumeLayout()
    End Sub

    Private Sub SaveToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripButton.Click
        SaveToolStripButtonClicked = True

        If thisCommentId > 0 Then 'Update
            Try
                If Util.UpdateComment(Trim(txtbxComment.Text), thisCommentId) Then
                    'Close Form
                    Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Else
                    MessageBox.Show("Error updating comment", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    SaveToolStripButtonClicked = False
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error saving comment", MessageBoxButtons.OK, MessageBoxIcon.Error)
                SaveToolStripButtonClicked = False
            End Try
        Else
            Try
                'Save  Loan comments and populate the commentId variable
                thisCommentId = Util.SetComment(thisEnquiryId, LoggedinName, Trim(txtbxComment.Text))
                'Debug
                'MessageBox.Show("CommentId = " & CommentId, "Saved comment Id")

                'Close Form
                Me.DialogResult = System.Windows.Forms.DialogResult.OK

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Error saving comment", MessageBoxButtons.OK, MessageBoxIcon.Error)
                SaveToolStripButtonClicked = False
            End Try
        End If

    End Sub


    Private Sub CancelToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelToolStripButton.Click
        Me.EnquiryCommentBS.CancelEdit()
        'Close Form
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub CutToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CutToolStripButton.Click
        If txtbxComment.SelectedText <> "" Then
            ' Cut the selected text in the control and paste it into the Clipboard.
            txtbxComment.Cut()
        End If
    End Sub

    Private Sub CopyToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripButton.Click
        ' Ensure that text is selected in the text box.   
        If txtbxComment.SelectionLength > 0 Then
            ' Copy the selected text to the Clipboard.
            txtbxComment.Copy()
        End If
    End Sub

    Private Sub PasteToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripButton.Click
        ' Determine if there is any text in the Clipboard to paste into the text box.
        If Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) = True Then
            ' Determine if any text is selected in the text box.
            If txtbxComment.SelectionLength > 0 Then
                ' Ask user if they want to paste over currently selected text.
                If MessageBox.Show("Do you want to paste over current selection?", _
                    "Cut Example", MessageBoxButtons.YesNo) = DialogResult.No Then
                    ' Move selection to the point after the current selection and paste.
                    txtbxComment.SelectionStart = txtbxComment.SelectionStart + txtbxComment.SelectionLength
                End If
            End If
            ' Paste current text in Clipboard into text box.
            txtbxComment.Paste()
        End If
    End Sub

    Private Sub UndoToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UndoToolStripButton.Click
        ' Determine if last operation can be undone in text box.   
        If txtbxComment.CanUndo = True Then
            ' Undo the last operation.
            txtbxComment.Undo()
            ' Clear the undo buffer to prevent last action from being redone.
            'txtbxComment.ClearUndo()
        End If
    End Sub

    Private Sub AddCommentForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            'leave if txtbxPrelimComment.Text is empty (no data)
            If Not txtbxComment.Text = "" Then
                If SaveToolStripButtonClicked = False And thisFormMode = MyEnums.Mode.Edit Then
                    
                    If isDirty Then
                        'Get user confirmation
                        Dim msg As String
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        msg = "Do you want to Save Changed Data?"   ' Define message.
                        style = MsgBoxStyle.DefaultButton2 Or _
                           MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                        title = "Save Record?"   ' Define title.
                        ' Display message.
                        response = MsgBox(msg, style, title)
                        If response = MsgBoxResult.Yes Then   ' User choose Yes.
                            Try
                                Dim CommentId As Integer
                                'set SaveToolStripButtonClicked
                                SaveToolStripButtonClicked = True
                                'Save  Loan comments
                                CommentId = Util.SetComment(thisEnquiryId, LoggedinName, Trim(txtbxComment.Text))
                                'Debug
                                'MessageBox.Show("CommentId = " & CommentId, "Saved comment Id")
                                'Close Form
                                Me.DialogResult = System.Windows.Forms.DialogResult.OK

                            Catch ex As Exception
                                MsgBox(ex.Message)
                            End Try
                        Else
                            ' User choose No. Do nothing
                        End If
                        'no data entered - proceed
                    End If

                End If

            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'save user settings (on the FormClosing Form Event)
        My.Settings.AddCommentFormSize = New Size(Me.ClientSize)
        My.Settings.AddCommentFormLocation = New Point(Me.Location)

    End Sub

    Private Sub AddCommentForm_Layout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        txtbxComment.Size = New Size(Me.Width - 40, Me.Height - 78)
    End Sub

    Private Sub AddCommentForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        isDirty = False
    End Sub

    Private Sub txtbxComment_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbxComment.TextChanged
        isDirty = True
    End Sub

End Class