﻿Public Class AdminForm

   

    Private Sub AdminForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'loads data into the 'EnquiryWorkSheetDataSet.ApplicationValues' table. You can move, or remove it, as needed.
        Me.AppSettingsTableAdapter1.Fill(Me.EnquiryWorkSheetDataSet.AppSettings)
        'loads data into the 'EnquiryWorkSheetDataSet.TypeOfTenancy' table. You can move, or remove it, as needed.
        Me.TypeOfTenancyTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.TypeOfTenancy)
        'loads data into the 'EnquiryWorkSheetDataSet.TypeOfClient' table.
        'Me.TypeOfClientTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.TypeOfClient)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.CurrentStatus' table. 
        'Me.CurrentStatusTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.CurrentStatus)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.UserRoles' table. 
        Me.UserRolesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.UserRoles)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.Branches' table.
        Me.BranchesTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Branches)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.Users' table.
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.TypeOfLoan' table.
        'Me.TypeOfLoanTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.TypeOfLoan)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.PrelimResults' table.
        Me.PrelimResultsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimResults)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.PrelimReasons' table.
        'Me.PrelimReasonsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimReasons)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.PrelimSource' table.
        Me.PrelimSourceTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimSource)
        'This line of code loads data into the 'EnquiryWorkSheetDataSet.EnquiryMethodList' table. 
        Me.EnquiryMethodListTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.EnquiryMethodList)

        Me.LatencyPointsTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.LatencyPoints)

        'set StatusStrip text
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."


    End Sub

    Private Sub btnSavePrelimSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSavePrelimSource.Click
        Try
            Me.Validate()
            Me.PrelimSourceBindingSource.EndEdit()
            Me.PrelimSourceTableAdapter.Update(EnquiryWorkSheetDataSet.PrelimSource)
            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'MsgBox("update successful")
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Preliminary Source Update successful."

        Catch ex As System.Data.NoNullAllowedException
            If Me.PrelimSourceDataGridView.CurrentCell.ColumnIndex = 1 Then
                'MsgBox("Column 'PrelimSource' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column 'PrelimSource' must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub btnSavePrelimReasons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.Validate()
    '        Me.PrelimReasonsBindingSource.EndEdit()
    '        Me.PrelimReasonsTableAdapter.Update(EnquiryWorkSheetDataSet.PrelimReasons)
    '        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '        'MsgBox("update successful")
    '        'set StatusStrip text
    '        ToolStripStatusLabel1.ForeColor = Color.Black
    '        ToolStripStatusLabel1.Text = "Status: Preliminary Reasons Update successful."
    '    Catch ex As System.Data.NoNullAllowedException
    '        If Me.PrelimReasonsDataGridView.CurrentCell.ColumnIndex = 1 Then
    '            'MsgBox("Column 'PrelimReason' must be filled out")
    '            'set StatusStrip text
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error: Column 'PrelimReason' must be filled out!"
    '        Else
    '            MsgBox(ex.Message)
    '        End If
    '        ' catch other exceptions
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnSavePrelimResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSavePrelimResults.Click
        Try
            Me.Validate()
            Me.PrelimResultsBindingSource.EndEdit()
            Me.PrelimResultsTableAdapter.Update(EnquiryWorkSheetDataSet.PrelimResults)
            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'MsgBox("update successful")
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Preliminary Results Update successful."
        Catch ex As System.Data.NoNullAllowedException
            If Me.PrelimResultsDataGridView.CurrentCell.ColumnIndex = 1 Then
                'MsgBox("Column 'PrelimResults' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column 'PrelimResults' must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub btnSaveTypeOfLoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTypeOfLoan.Click
    '    Try
    '        Me.Validate()
    '        Me.TypeOfLoanBindingSource.EndEdit()
    '        Me.TypeOfLoanTableAdapter.Update(EnquiryWorkSheetDataSet.TypeOfLoan)
    '        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '        'MsgBox("update successful")
    '        'set StatusStrip text
    '        ToolStripStatusLabel1.ForeColor = Color.Black
    '        ToolStripStatusLabel1.Text = "Status: Type of Loan Update successful."
    '    Catch ex As System.Data.NoNullAllowedException
    '        If Me.TypeOfLoanDataGridView.CurrentCell.ColumnIndex = 0 Then
    '            'MsgBox("Column 'TypeName' must be filled out")
    '            'set StatusStrip text
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error: Column 'TypeName' must be filled out!"
    '        Else
    '            MsgBox(ex.Message)
    '        End If
    '        ' catch other exceptions
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnSaveUsers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveUsers.Click
        Try
            'check fields
            Dim UserMessage As String = ""
            If UserNameTextBox.Text = "" Then
                UserMessage = UserMessage + "User name must be filled out" + vbCrLf
            End If
            If PasswordTextBox.Text = "" Then
                UserMessage = UserMessage + "Password must be filled out" + vbCrLf
            End If
            If BranchIdComboBox.SelectedValue Is Nothing Or BranchIdComboBox.SelectedValue = "" Then
                UserMessage = UserMessage + "BranchId must be selected" + vbCrLf
            End If
            If RoleComboBox.SelectedValue Is Nothing Or RoleComboBox.SelectedValue = "" Then
                UserMessage = UserMessage + "Role must be selected" + vbCrLf
            End If
            If Not UserMessage = "" Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  View Message Box for errors!"
                MsgBox(UserMessage)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
            Else
                Me.Validate()
                Me.UsersBindingSource.EndEdit()
                Me.UsersTableAdapter.Update(EnquiryWorkSheetDataSet.Users)
                'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                'MsgBox("update successful")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Users Update successful."
            End If
        Catch ex As System.Data.NoNullAllowedException
            'MsgBox(ex.Message)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR:  " & ex.Message
            'catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BranchIdComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BranchIdComboBox.SelectedIndexChanged
        Try
            Dim ChosenBranch As String
            Dim ChosenBranchName As String
            ChosenBranch = CStr(BranchIdComboBox.SelectedValue)
            'get value of Branch name for ChosenBranch Id
            Dim BranchesItemRow As EnquiryWorkSheetDataSet.BranchesRow
            For Each BranchesItemRow In EnquiryWorkSheetDataSet.Branches
                If BranchesItemRow.Item("BranchId") = ChosenBranch Then
                    ChosenBranchName = BranchesItemRow.Item("FullName")
                    lblBranchName.Text = ChosenBranchName
                    Exit For
                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveBranches_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveBranches.Click
        Try
            'check fields
            Dim BranchMessage As String = ""
            If BranchIdTextBox.Text = "" Then
                BranchMessage = BranchMessage + "Branch Id must be filled out" + vbCrLf
            End If
            If FullNameTextBox.Text = "" Then
                BranchMessage = BranchMessage + "Branch Name must be filled out" + vbCrLf
            End If
            If AddressTextBox.Text = "" Then
                BranchMessage = BranchMessage + "Branch Address must be selected" + vbCrLf
            End If
            If CityTextBox.Text = "" Then
                BranchMessage = BranchMessage + "Branch City must be selected" + vbCrLf
            End If
            If Not BranchMessage = "" Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR:  View Message Box for errors!"
                MsgBox(BranchMessage)
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Ready."
            Else
                Me.Validate()
                Me.BranchesBindingSource.EndEdit()
                'Me.UsersTableAdapter.Update(EnquiryWorkSheetDataSet.Users)
                Me.BranchesTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Branches)
                'MsgBox("update successful")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Status: Branches Update successful."
            End If
        Catch ex As System.Data.NoNullAllowedException
            'MsgBox(ex.Message)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "ERROR:  " & ex.Message
            'catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    
    Private Sub btnSaveUserRoles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveUserRoles.Click
        Try
            Me.Validate()
            Me.UserRolesBindingSource.EndEdit()
            Me.UserRolesTableAdapter.Update(EnquiryWorkSheetDataSet.UserRoles)
            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'MsgBox("update successful")
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Users Roles Update successful."

        Catch ex As System.Data.NoNullAllowedException
            If Me.UserRolesDataGridView.CurrentCell.ColumnIndex = 0 Then
                'MsgBox("Column 'Role' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column 'Role' must be filled out!"
            ElseIf Me.UserRolesDataGridView.CurrentCell.ColumnIndex = 1 Then
                'MsgBox("Column 'Permission Level' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column 'Permission Level' must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveEnquiryMethod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEnquiryMethod.Click
        Try
            Me.Validate()
            Me.EnquiryMethodListBindingSource.EndEdit()
            Me.EnquiryMethodListTableAdapter.Update(EnquiryWorkSheetDataSet.EnquiryMethodList)
            'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'MsgBox("update successful")
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Enquiry Method Update successful."
        Catch ex As System.Data.NoNullAllowedException
            If Me.EnquiryMethodListDataGridView.CurrentCell.ColumnIndex = 1 Then
                'MsgBox("Column 'EnquiryMethod' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column 'Enquiry Method' must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub btnSaveCurrentStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.Validate()
    '        Me.CurrentStatusBindingSource.EndEdit()
    '        Me.CurrentStatusTableAdapter.Update(EnquiryWorkSheetDataSet.CurrentStatus)
    '        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
    '        'MsgBox("update successful")
    '        'set StatusStrip text
    '        ToolStripStatusLabel1.ForeColor = Color.Black
    '        ToolStripStatusLabel1.Text = "Status: Current Status Update successful."
    '    Catch ex As System.Data.NoNullAllowedException
    '        If Me.CurrentStatusDataGridView.CurrentCell.ColumnIndex = 0 Then
    '            'MsgBox("Column 'Status' must be filled out")
    '            'set StatusStrip text
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error: Column 'Status' must be filled out!"
    '        Else
    '            MsgBox(ex.Message)
    '        End If
    '        ' catch other exceptions
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    'Private Sub btnSaveTypeOfClient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.Validate()
    '        Me.TypeOfClientBindingSource.EndEdit()
    '        Me.TypeOfClientTableAdapter.Update(EnquiryWorkSheetDataSet.TypeOfClient)
    '        'MsgBox("update successful")
    '        'set StatusStrip text
    '        ToolStripStatusLabel1.ForeColor = Color.Black
    '        ToolStripStatusLabel1.Text = "Status: Type of Client Update successful."
    '    Catch ex As System.Data.NoNullAllowedException
    '        If Me.TypeOfClientDataGridView.CurrentCell.ColumnIndex = 0 Then
    '            'MsgBox("Column 'Type' must be filled out")
    '            'set StatusStrip text
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error: Column 'Type' must be filled out!"
    '        ElseIf Me.TypeOfClientDataGridView.CurrentCell.ColumnIndex = 1 Then
    '            'MsgBox("Column 'Description' must be filled out")
    '            'set StatusStrip text
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error: Column 'Description' must be filled out!"
    '        Else
    '            MsgBox(ex.Message)
    '        End If
    '        ' catch other exceptions
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub tpUsers_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles tpBranches.LostFocus
        'remove handlers
        RemoveHandler BranchIdComboBox.SelectedIndexChanged, AddressOf BranchIdComboBox_SelectedIndexChanged

    End Sub

    Private Sub tpUsers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tpUsers.Click
        'add handlers
        AddHandler BranchIdComboBox.SelectedIndexChanged, AddressOf BranchIdComboBox_SelectedIndexChanged
    End Sub

    Private Sub SaveToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripButton.Click
        Try
            Me.Validate()
            Me.TypeOfTenancyBindingSource.EndEdit()
            Me.TypeOfTenancyTableAdapter.Update(EnquiryWorkSheetDataSet.TypeOfTenancy)
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Type of Tenancy Update successful."
        Catch ex As System.Data.NoNullAllowedException
            If Me.TypeOfTenancyDataGridView.CurrentCell.ColumnIndex = 0 Then
                'MsgBox("Column 'TypeName' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column 'TenancyType' must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

   
    
    Private Sub btnSaveAppValues_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAppValues.Click
        Try
            Me.Validate()
            Me.AppSettingsBindingSource.EndEdit()
            Me.AppSettingsTableAdapter1.Update(EnquiryWorkSheetDataSet.AppSettings)
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: AppSettings Update successful."
        Catch ex As System.Data.NoNullAllowedException
            If Me.AppSettingsDataGridView.CurrentCell.ColumnIndex = 0 Then
                'MsgBox("Column 'TypeName' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column  must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub




    Private Sub tsbtnLatencyPoints_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnLatencyPoints.Click

        Dim i As Integer
        Dim SortNumAsInt As Integer
        Dim LatencydataValid As Boolean = True
        Dim SortVal As String = ""
        dgvLatencyPoints.EndEdit()
        For i = 0 To dgvLatencyPoints.RowCount - 1
            If Not dgvLatencyPoints.Rows(i).Cells(0).Value Is Nothing Then
                If Not dgvLatencyPoints.Rows(i).Cells(0).Value.ToString = "" Then
                    Me.dgvLatencyPoints.Rows(i).ErrorText = ""
                Else
                    If Not (dgvLatencyPoints.Rows(i).Cells(1).Value.ToString = "" And dgvLatencyPoints.Rows(i).Cells(3).Value.ToString = "") Then
                        Me.dgvLatencyPoints.Rows(i).ErrorText = "select a log type from drop down"
                        MessageBox.Show("select a log type from drop down")
                        LatencydataValid = False
                        Exit For
                    End If
                End If
                SortVal = dgvLatencyPoints.Rows(i).Cells(3).Value
                'If SortVal = "6" Then
                '    Exit For
                'End If
                If Not SortVal.Trim = "" Then
                    If Integer.TryParse(SortVal, SortNumAsInt) Then
                        dgvLatencyPoints.Rows(i).ErrorText = ""
                    Else
                        MessageBox.Show("Enter a valid integer value in Sort Number column")
                        dgvLatencyPoints.Rows(i).ErrorText = "improper data in row"
                        LatencydataValid = False
                        Exit For
                    End If

                End If
            End If
        Next
        If Not LatencydataValid Then Exit Sub
        Try
            Me.Validate()
            Me.LatencyPointsBindingSource.EndEdit()
            Me.LatencyPointsTableAdapter.Update(EnquiryWorkSheetDataSet.LatencyPoints)
            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Latency Points Update successful."
        Catch ex As System.Data.NoNullAllowedException
            If Me.dgvLatencyPoints.CurrentCell.ColumnIndex = 0 Then
                'MsgBox("Column 'TypeName' must be filled out")
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "Error: Column  must be filled out!"
            Else
                MsgBox(ex.Message)
            End If
            ' catch other exceptions
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgvLatencyPoints_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLatencyPoints.CellEndEdit

        dgvLatencyPoints.Rows(e.RowIndex).ErrorText = String.Empty

    End Sub




    Private Sub dgvLatencyPoints_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvLatencyPoints.DataError
        Dim SortNumAsInt As Integer



        Select Case e.ColumnIndex
            Case 0
                If dgvLatencyPoints.Rows(e.RowIndex).Cells(e.ColumnIndex).FormattedValue = "" And dgvLatencyPoints.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue = "" Then
                    Me.dgvLatencyPoints.Rows(e.RowIndex).ErrorText = ""
                Else
                    Me.dgvLatencyPoints.Rows(e.RowIndex).ErrorText = "select a log type from drop down"
                    e.Cancel = True
                End If
            Case 1
                If dgvLatencyPoints.Rows(e.RowIndex).Cells(e.ColumnIndex).FormattedValue = "" And dgvLatencyPoints.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue = "" Then
                    Me.dgvLatencyPoints.Rows(e.RowIndex).ErrorText = ""
                Else
                    Me.dgvLatencyPoints.Rows(e.RowIndex).ErrorText = "select enter Subject"
                    e.Cancel = True
                End If
            Case 3
                If Integer.TryParse(dgvLatencyPoints.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue, SortNumAsInt) Then
                    ' successfully parsed as Integer
                    Me.dgvLatencyPoints.Rows(e.RowIndex).ErrorText = ""
                Else
                    Me.dgvLatencyPoints.Rows(e.RowIndex).ErrorText = "the value must be a non-negative integer"
                    e.Cancel = True
                End If
        End Select
    End Sub


End Class