﻿Imports System.Globalization
Imports System.Text


Public Class MarketingEnquiryReportSelectionsForm
    Dim selectedrb As RadioButton ' Keep track of the selected RadioButton by saving a reference to it.
    Dim StartDate As Date
    Dim EndDate As Date
    Dim GroupBy As String
    Dim OrderBy As String
    Dim ArchiveStatus As String
    Dim strLoanPurpose As New StringBuilder
    Dim strSource As New StringBuilder
    Dim strLoanType As New StringBuilder

    'Loads form with parameters from Form1
    Friend Sub ReportSelections_PassVariable(ByVal reportTypeVar As String)
        Select Case reportTypeVar
            Case "Enquiry"
                Me.Text = Text + " - Enquiry"
                'set default values
                Me.cmbxGroupBy.SelectedIndex = 0
                Me.cmbxOrderBy.SelectedIndex = 0

            Case Else
                'set StatusStrip text
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "ERROR: ReportTypeVar not recognised! Please try again."
        End Select


    End Sub

    Private Sub ReportSelectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'load data into the 'EnquiryWorkSheetDataSet.PrelimSource' table. 
        Me.PrelimSourceTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.PrelimSource)
        'loads data into the 'EnquiryWorkSheetDataSet.LoanPurpose' table. 
        Me.LoanPurposeTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.LoanPurpose)

        Try
            'set datasource for listbox using the Enum members
            lstbxLoanType.DataSource = MyEnums.GetDescriptions(GetType(MyEnums.EnquiryType)) '10/05/2016  Christopher

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Sub ckbxLoanPurpose_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxLoanPurpose.CheckedChanged
        If ckbxLoanPurpose.CheckState = CheckState.Checked Then
            lstbxLoanPurpose.Enabled = True
        Else
            lstbxLoanPurpose.Enabled = False
        End If
    End Sub

    Private Sub ckbxSource_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxSource.CheckedChanged
        If ckbxSource.CheckState = CheckState.Checked Then
            lstbxSource.Enabled = True
        Else
            lstbxSource.Enabled = False
        End If
    End Sub

    Private Sub ckbxLoanType_CheckedChanged(sender As Object, e As EventArgs) Handles ckbxLoanType.CheckedChanged
        If ckbxLoanType.CheckState = CheckState.Checked Then
            lstbxLoanType.Enabled = True
        Else
            lstbxLoanType.Enabled = False
        End If
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Cursor.Current = Cursors.WaitCursor
        strLoanPurpose.Clear()
        strSource.Clear()
        strLoanType.Clear()
        'get values
        StartDate = DateTimePicker1.Value.ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        EndDate = DateTimePicker2.Value.AddDays(1).ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        'get LoanPurpose if selected
        If lstbxLoanPurpose.Enabled = True Then
            If lstbxLoanPurpose.SelectedItems.Count > 0 Then
                Dim drv As DataRowView = Nothing
                Dim sep As String = ", "
                'DataRowView
                drv = lstbxLoanPurpose.SelectedItems(0)
                'build stringbuilder
                strLoanPurpose.Append(drv("LoanPurpose").ToString)
                Dim i As Integer
                For i = 1 To lstbxLoanPurpose.SelectedItems.Count - 1
                    drv = lstbxLoanPurpose.SelectedItems(i)
                    strLoanPurpose.Append(sep).Append(drv("LoanPurpose").ToString)
                Next i
            End If
        Else
            strLoanPurpose.Clear()
        End If
        'get Source if selected
        If lstbxSource.Enabled = True Then
            If lstbxSource.SelectedItems.Count > 0 Then
                Dim drv As DataRowView = Nothing
                Dim sep As String = ", "
                'DataRowView
                drv = lstbxSource.SelectedItems(0)
                'build stringbuilder
                strSource.Append(drv("PrelimSource").ToString)
                Dim i As Integer
                For i = 1 To lstbxSource.SelectedItems.Count - 1
                    drv = lstbxSource.SelectedItems(i)
                    strSource.Append(sep).Append(drv("PrelimSource").ToString)
                Next i
            End If
        Else
            strSource.Clear()
        End If
        'get LoanType if selected
        If lstbxLoanType.Enabled = True Then
            If lstbxLoanType.SelectedItems.Count > 0 Then
                'Dim drv As DataRowView = Nothing
                Dim sep As String = ", "
                'DataRowView
                'drv = lstbxLoanType.SelectedItems(0)
                'build stringbuilder
                strLoanType.Append(lstbxLoanType.SelectedItems(0).ToString)
                Dim i As Integer
                For i = 1 To lstbxLoanType.SelectedItems.Count - 1
                    'drv = lstbxLoanType.SelectedItems(i)
                    'strLoanType.Append(sep).Append(drv("PrelimReason").ToString)
                    strLoanType.Append(sep).Append(lstbxLoanType.SelectedItems(i).ToString)
                Next i
            End If
        Else
            strLoanType.Clear()
        End If
        'get Group By
        GroupBy = cmbxGroupBy.SelectedItem.ToString
        'get Order by
        OrderBy = cmbxOrderBy.SelectedItem.ToString
        'get ActiveStatus filter
        'ArchiveStatus = selectedrb.Text
        ArchiveStatus = "Include Archives"
        'debug
        'MsgBox("StartDate: " & StartDate & vbCrLf & "EndDate: " & EndDate & vbCrLf & "LoanPurpose: " & strLoanPurpose.ToString() & vbCrLf & "Source: " & strSource.ToString() & vbCrLf & "GroupBy: " & GroupBy & vbCrLf & "OrderBy: " & OrderBy & vbCrLf & "ArchiveStatus: " & ArchiveStatus)

        Try
            'launch ReportSelectionsForm and pass Enquiry parameters
            Dim marketingRptFrm As New ReportMarketingForm
            marketingRptFrm.ReportSelections_PassVariable(StartDate, EndDate, GroupBy, OrderBy, strLoanPurpose.ToString(), strSource.ToString(), strLoanType.ToString(), ArchiveStatus)
            marketingRptFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportSelectionsForm: " + vbCrLf + ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub rbtnActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnActive.CheckedChanged
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    selectedrb = rb
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Enquiries = " & selectedrb.Text
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub rbtnBoth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnBoth.CheckedChanged
        Dim rb As RadioButton = TryCast(sender, RadioButton)
        Try
            If Not rb Is Nothing Then
                'MessageBox.Show("Sender is a RadioButton")
                ' Ensure that the RadioButton.Checked property changed to true.
                If rb.Checked Then
                    ' Keep track of the selected RadioButton by saving a reference to it.
                    selectedrb = rb
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Selected radio button for Enquiries = " & selectedrb.Text
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    
End Class