﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DisplayFinPowerNamesForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisplayFinPowerNamesForm))
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.FinPowerNamesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FinPowerNamesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.finPowerNamesTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.FinPowerNamesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.FinPowerNamesDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LegalName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblDisplayName = New System.Windows.Forms.Label()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.lblSuburb = New System.Windows.Forms.Label()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.txtbxDisplayNameFilter = New System.Windows.Forms.TextBox()
        Me.txtbxLastNameFilter = New System.Windows.Forms.TextBox()
        Me.txtbxFirstNameFilter = New System.Windows.Forms.TextBox()
        Me.txtbxSuburbFilter = New System.Windows.Forms.TextBox()
        Me.txtbxCityFilter = New System.Windows.Forms.TextBox()
        Me.gpbxFilters = New System.Windows.Forms.GroupBox()
        Me.lblLegalName = New System.Windows.Forms.Label()
        Me.txtLegalNameFilter = New System.Windows.Forms.TextBox()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FinPowerNamesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FinPowerNamesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FinPowerNamesBindingNavigator.SuspendLayout()
        CType(Me.FinPowerNamesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxFilters.SuspendLayout()
        Me.SuspendLayout()
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FinPowerNamesBindingSource
        '
        Me.FinPowerNamesBindingSource.DataMember = "finPowerNames"
        Me.FinPowerNamesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'FinPowerNamesTableAdapter
        '
        Me.FinPowerNamesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Me.FinPowerNamesTableAdapter
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'FinPowerNamesBindingNavigator
        '
        Me.FinPowerNamesBindingNavigator.AddNewItem = Nothing
        Me.FinPowerNamesBindingNavigator.BindingSource = Me.FinPowerNamesBindingSource
        Me.FinPowerNamesBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.FinPowerNamesBindingNavigator.DeleteItem = Nothing
        Me.FinPowerNamesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.FinPowerNamesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.FinPowerNamesBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.FinPowerNamesBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.FinPowerNamesBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.FinPowerNamesBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.FinPowerNamesBindingNavigator.Name = "FinPowerNamesBindingNavigator"
        Me.FinPowerNamesBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.FinPowerNamesBindingNavigator.Size = New System.Drawing.Size(944, 25)
        Me.FinPowerNamesBindingNavigator.TabIndex = 0
        Me.FinPowerNamesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'FinPowerNamesDataGridView
        '
        Me.FinPowerNamesDataGridView.AllowUserToAddRows = False
        Me.FinPowerNamesDataGridView.AllowUserToDeleteRows = False
        Me.FinPowerNamesDataGridView.AutoGenerateColumns = False
        Me.FinPowerNamesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.FinPowerNamesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.LegalName, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.FinPowerNamesDataGridView.DataSource = Me.FinPowerNamesBindingSource
        Me.FinPowerNamesDataGridView.Location = New System.Drawing.Point(12, 93)
        Me.FinPowerNamesDataGridView.Name = "FinPowerNamesDataGridView"
        Me.FinPowerNamesDataGridView.ReadOnly = True
        Me.FinPowerNamesDataGridView.RowHeadersWidth = 20
        Me.FinPowerNamesDataGridView.RowTemplate.Height = 20
        Me.FinPowerNamesDataGridView.Size = New System.Drawing.Size(920, 609)
        Me.FinPowerNamesDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Name"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Display Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 135
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "LastName"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Last Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 135
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "FirstName"
        Me.DataGridViewTextBoxColumn3.HeaderText = "First Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 135
        '
        'LegalName
        '
        Me.LegalName.DataPropertyName = "LegalName"
        Me.LegalName.HeaderText = "LegalName"
        Me.LegalName.Name = "LegalName"
        Me.LegalName.ReadOnly = True
        Me.LegalName.Width = 135
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "City"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Suburb"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "State"
        Me.DataGridViewTextBoxColumn5.HeaderText = "City"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "CreditRating"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Credit Rating"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "ClientId"
        Me.DataGridViewTextBoxColumn7.HeaderText = "ClientId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'lblDisplayName
        '
        Me.lblDisplayName.AutoSize = True
        Me.lblDisplayName.Location = New System.Drawing.Point(77, 14)
        Me.lblDisplayName.Name = "lblDisplayName"
        Me.lblDisplayName.Size = New System.Drawing.Size(72, 13)
        Me.lblDisplayName.TabIndex = 2
        Me.lblDisplayName.Text = "Display Name"
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Location = New System.Drawing.Point(215, 14)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 13)
        Me.lblLastName.TabIndex = 3
        Me.lblLastName.Text = "Last Name"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Location = New System.Drawing.Point(347, 14)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(57, 13)
        Me.lblFirstName.TabIndex = 4
        Me.lblFirstName.Text = "First Name"
        '
        'lblSuburb
        '
        Me.lblSuburb.AutoSize = True
        Me.lblSuburb.Location = New System.Drawing.Point(619, 14)
        Me.lblSuburb.Name = "lblSuburb"
        Me.lblSuburb.Size = New System.Drawing.Size(41, 13)
        Me.lblSuburb.TabIndex = 5
        Me.lblSuburb.Text = "Suburb"
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Location = New System.Drawing.Point(758, 14)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(24, 13)
        Me.lblCity.TabIndex = 6
        Me.lblCity.Text = "City"
        '
        'txtbxDisplayNameFilter
        '
        Me.txtbxDisplayNameFilter.Location = New System.Drawing.Point(51, 32)
        Me.txtbxDisplayNameFilter.Name = "txtbxDisplayNameFilter"
        Me.txtbxDisplayNameFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxDisplayNameFilter.TabIndex = 7
        '
        'txtbxLastNameFilter
        '
        Me.txtbxLastNameFilter.Location = New System.Drawing.Point(182, 32)
        Me.txtbxLastNameFilter.Name = "txtbxLastNameFilter"
        Me.txtbxLastNameFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxLastNameFilter.TabIndex = 8
        '
        'txtbxFirstNameFilter
        '
        Me.txtbxFirstNameFilter.Location = New System.Drawing.Point(313, 32)
        Me.txtbxFirstNameFilter.Name = "txtbxFirstNameFilter"
        Me.txtbxFirstNameFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxFirstNameFilter.TabIndex = 9
        '
        'txtbxSuburbFilter
        '
        Me.txtbxSuburbFilter.Location = New System.Drawing.Point(575, 32)
        Me.txtbxSuburbFilter.Name = "txtbxSuburbFilter"
        Me.txtbxSuburbFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxSuburbFilter.TabIndex = 10
        '
        'txtbxCityFilter
        '
        Me.txtbxCityFilter.Location = New System.Drawing.Point(706, 32)
        Me.txtbxCityFilter.Name = "txtbxCityFilter"
        Me.txtbxCityFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtbxCityFilter.TabIndex = 11
        '
        'gpbxFilters
        '
        Me.gpbxFilters.Controls.Add(Me.lblLegalName)
        Me.gpbxFilters.Controls.Add(Me.txtLegalNameFilter)
        Me.gpbxFilters.Controls.Add(Me.lblLastName)
        Me.gpbxFilters.Controls.Add(Me.txtbxCityFilter)
        Me.gpbxFilters.Controls.Add(Me.lblDisplayName)
        Me.gpbxFilters.Controls.Add(Me.txtbxSuburbFilter)
        Me.gpbxFilters.Controls.Add(Me.lblFirstName)
        Me.gpbxFilters.Controls.Add(Me.txtbxFirstNameFilter)
        Me.gpbxFilters.Controls.Add(Me.lblSuburb)
        Me.gpbxFilters.Controls.Add(Me.txtbxLastNameFilter)
        Me.gpbxFilters.Controls.Add(Me.lblCity)
        Me.gpbxFilters.Controls.Add(Me.txtbxDisplayNameFilter)
        Me.gpbxFilters.Location = New System.Drawing.Point(12, 28)
        Me.gpbxFilters.Name = "gpbxFilters"
        Me.gpbxFilters.Size = New System.Drawing.Size(920, 59)
        Me.gpbxFilters.TabIndex = 12
        Me.gpbxFilters.TabStop = False
        Me.gpbxFilters.Text = "Filters"
        '
        'lblLegalName
        '
        Me.lblLegalName.AutoSize = True
        Me.lblLegalName.Location = New System.Drawing.Point(479, 14)
        Me.lblLegalName.Name = "lblLegalName"
        Me.lblLegalName.Size = New System.Drawing.Size(64, 13)
        Me.lblLegalName.TabIndex = 12
        Me.lblLegalName.Text = "Legal Name"
        '
        'txtLegalNameFilter
        '
        Me.txtLegalNameFilter.Location = New System.Drawing.Point(444, 32)
        Me.txtLegalNameFilter.Name = "txtLegalNameFilter"
        Me.txtLegalNameFilter.Size = New System.Drawing.Size(125, 20)
        Me.txtLegalNameFilter.TabIndex = 13
        '
        'DisplayFinPowerNamesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 714)
        Me.Controls.Add(Me.gpbxFilters)
        Me.Controls.Add(Me.FinPowerNamesDataGridView)
        Me.Controls.Add(Me.FinPowerNamesBindingNavigator)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(0, 300)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(960, 750)
        Me.Name = "DisplayFinPowerNamesForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "FinPower Names"
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FinPowerNamesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FinPowerNamesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FinPowerNamesBindingNavigator.ResumeLayout(False)
        Me.FinPowerNamesBindingNavigator.PerformLayout()
        CType(Me.FinPowerNamesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxFilters.ResumeLayout(False)
        Me.gpbxFilters.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents FinPowerNamesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FinPowerNamesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.finPowerNamesTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents FinPowerNamesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents FinPowerNamesDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents lblDisplayName As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblSuburb As System.Windows.Forms.Label
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtbxDisplayNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents txtbxLastNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents txtbxFirstNameFilter As System.Windows.Forms.TextBox
    Friend WithEvents txtbxSuburbFilter As System.Windows.Forms.TextBox
    Friend WithEvents txtbxCityFilter As System.Windows.Forms.TextBox
    Friend WithEvents gpbxFilters As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LegalName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblLegalName As System.Windows.Forms.Label
    Friend WithEvents txtLegalNameFilter As System.Windows.Forms.TextBox
End Class
