﻿Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.Text
Imports System.Configuration

Public Class ValidateXML
    Private _LoanCode As String = ""
    Private _ClientId1 As String = ""
    Private _ClientId2 As String = ""
    Private isShowSelected As Boolean
    Private isDirty As Boolean = True
    Private prevkey As Integer = 0
    Private ClientFinPowerId As String
    Public ClientTrueTrackId As Integer
    Public EnquiryId As Integer
    Public EnquiryCode As String
    Public ApplicationCode As String
    Dim htfinPower As New Hashtable ' Stores Selected data with Key selected
    Public FinPowerOpObj As finPowerOps ' instance of class

    '=======================================================
    'Data selected is stored in HashTable htfinPower and finally saved to XML on COMMIT
    'htXML in finPowerOps class contains current list of Client Names in XML
    '=======================================================
    Public Property Loancode() As String
        Get
            Return _LoanCode
        End Get
        Set(ByVal value As String)
            _LoanCode = value
        End Set
    End Property


    Public Property ClientId1() As String
        Get
            Return _ClientId1
        End Get
        Set(ByVal value As String)
            _ClientId1 = value
        End Set
    End Property

    Public Property ClientId2() As String
        Get
            Return _ClientId2
        End Get
        Set(ByVal value As String)
            _ClientId2 = value
        End Set
    End Property

    Public Sub SetFormCaption(ByVal ClientSalutation As String)
        Dim ThisFormText As String = "Possible Client matches for importing into finPower"
        'Set form title
        'Me.Text = ClientSalutation & " - " & ThisFormText
        Me.Text = ThisFormText
    End Sub



    Private Sub ValidateXML_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Me.lblFormHeader.Width = Me.Width
        FormLoadSetting()
        FinPowerOpObj = New finPowerOps(EnquiryCode, ApplicationCode)
        'CheckClientNames reads data from XML and checks a match exists in finPowser and Builds the hashtable of Names for display 
        FinPowerOpObj.CheckClientNames()
        'Populate the ListNames
        populateClientName()
    End Sub


    Public Sub populateClientName()
        Dim dispName As String = ""
        Dim i As Integer = 0
        Dim kvp As KeyValuePair(Of String, String)
        Dim clientfinPow As ClientEntity
        For i = 1 To FinPowerOpObj.htXML.Count
            If FinPowerOpObj.htNames.Contains(i) Then
                clientfinPow = CType(FinPowerOpObj.htXML.Item(i), ClientEntity)

                dispName = (clientfinPow.FirstName & " " & clientfinPow.LastName).Trim
                If clientfinPow.Comment.ToUpper = "AKA" Then
                    dispName += " AKA"
                End If
                kvp = New KeyValuePair(Of String, String)(i.ToString, dispName)
                lstClientName.DisplayMember = "Value"
                lstClientName.ValueMember = "key"
                lstClientName.Items.Add(kvp)
            End If
        Next
    End Sub

    'Display Address and Contact Details
    Private Sub getfinPowerAddressAndContactDetails()

        If dgvNames.RowCount > 0 And dgvNames.SelectedRows.Count > 0 Then

            txtfinAddress.Text = dgvNames.SelectedRows(0).Cells(7).Value
            txtfinSuburb.Text = dgvNames.SelectedRows(0).Cells(8).Value
            txtfinCity.Text = dgvNames.SelectedRows(0).Cells(9).Value
            txtfinPostCode.Text = dgvNames.SelectedRows(0).Cells(10).Value
            'If dgvContactMethod.RowCount > 0 Then
            '    dgvContactMethod.Rows(0).Cells(0).Selected = True
            'End If
            Me.dgvContactMethod.DataSource = FinPowerOpObj.getContactMethods(dgvNames.SelectedRows(0).Cells(1).Value)
            If dgvContactMethod.RowCount > 0 And dgvContactMethod.ColumnCount > 1 Then
                dgvContactMethod.Columns(1).Width = (dgvContactMethod.Width / 2) - 12
                dgvContactMethod.Columns(2).Width = (dgvContactMethod.Width / 2) - 12
                dgvContactMethod.Columns(0).Width = 25

            End If
        End If

    End Sub


    Private Sub dgvNames_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNames.CellContentClick
        'restricts user from selecting multiple checkboxes
        Dim chkcell As DataGridViewCheckBoxCell
        Dim i As Integer
        'gpbxfinPowDetails.Enabled = False
        chkcell = dgvNames.Rows(dgvNames.CurrentRow.Index).Cells(0)

        'If chkcell.EditingCellFormattedValue = True Then
        '    gpbxfinPowDetails.Enabled = True
        'Else
        '    gpbxfinPowDetails.Enabled = False
        'End If
        If e.ColumnIndex = 0 Then
            For i = 0 To dgvNames.Rows.Count - 1
                chkcell = dgvNames.Rows(i).Cells(0)
                If chkcell.EditedFormattedValue = True Then
                    If Not i = e.RowIndex Then
                        dgvNames.Rows(i).Cells(0).Value = False
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub dgvNames_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvNames.SelectionChanged

        'Show the details of the selected item
        getfinPowerAddressAndContactDetails()

        Dim chkcell As DataGridViewCheckBoxCell
        chkcell = dgvNames.Rows(dgvNames.CurrentRow.Index).Cells(0)

        'If chkcell.EditingCellFormattedValue = True Then
        '    gpbxfinPowDetails.Enabled = True
        'Else
        '    gpbxfinPowDetails.Enabled = False
        'End If

    End Sub

    'Add changes into ClientEntity Object and adds into the HashTable  
    Private Function AddClientChanges(ByVal key As Integer) As Boolean
        Dim i As Integer
        Dim row As DataGridViewRow
        Dim chkcell As DataGridViewCheckBoxCell
        Dim clientfinPower As New ClientEntity
        Dim strAlertMessage As String = ""
        strAlertMessage = "Cannot have more than one match selected for the client. Do you want to overwrite your current selection?"

        'Alert user if the data already exists in the dataGrid
        If htfinPower.ContainsKey(key) Then
            If MessageBox.Show(strAlertMessage, "Alert", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Cancel Then
                Return False
            End If
        End If

        clientfinPower.key = key
        clientfinPower.Name_MC = lstClientName.Text
        'Store the current state into HashTable by adding changes into clientfinPower Object 
        For Each row In dgvNames.Rows
            chkcell = row.Cells(0)
            If chkcell.EditedFormattedValue = True Then
                clientfinPower.NameChecked = True
                clientfinPower.ClientId = row.Cells(1).Value
                ClientFinPowerId = clientfinPower.ClientId
                clientfinPower.Type = row.Cells(2).Value
                clientfinPower.FirstName = row.Cells(3).Value
                clientfinPower.MiddleNames = row.Cells(4).Value
                clientfinPower.LastName = row.Cells(5).Value
                clientfinPower.DateOfBirth = row.Cells(6).Value


                If chkAddress.Checked = True Then
                    clientfinPower.AddressChecked = True
                    clientfinPower.Address = txtfinAddress.Text
                    clientfinPower.City = txtfinSuburb.Text
                    clientfinPower.State = txtfinCity.Text
                    clientfinPower.PostCode = txtfinPostCode.Text
                End If
                For i = 0 To dgvContactMethod.RowCount - 1
                    If dgvContactMethod.Rows(i).Cells(0).Value = True Then
                        Select Case dgvContactMethod.Rows(i).Cells(1).Value
                            Case "Work"
                                clientfinPower.PhoneWorkChecked = True
                                clientfinPower.PhoneWork = dgvContactMethod.Rows(i).Cells(2).Value
                            Case "Phone"
                                clientfinPower.PhoneHomeChecked = True
                                clientfinPower.PhoneHome = dgvContactMethod.Rows(i).Cells(2).Value
                            Case "Mobile"
                                clientfinPower.PhoneMobileChecked = True
                                clientfinPower.PhoneMobile = dgvContactMethod.Rows(i).Cells(2).Value
                            Case "Email"
                                clientfinPower.EmailChecked = True
                                clientfinPower.Email = dgvContactMethod.Rows(i).Cells(2).Value

                        End Select
                    End If
                Next
                If htfinPower.ContainsKey(key) Then

                    htfinPower.Remove(key)
                End If

                htfinPower.Add(key, clientfinPower)
            End If
        Next
        Return True
    End Function

    Private Sub RefreshUpdatedGrid()

        'dgvUpdated.AutoGenerateColumns = False
        Me.dgvUpdated.DataSource = BuildUpdatedDataTableFromHash()
        SetDatagridColumns()
    End Sub

    Private Sub SetDatagridColumns()
        Dim i As Integer
        Dim column As DataGridViewColumn


        If dgvContactsXML.RowCount > 0 And dgvContactsXML.ColumnCount > 1 Then
            dgvContactsXML.Columns(0).Width = (dgvContactsXML.Width / 2)
            dgvContactsXML.Columns(1).Width = (dgvContactsXML.Width / 2)
        End If


        dgvNames.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvNames.MultiSelect = False

        Dim style As DataGridViewCellStyle = New DataGridViewCellStyle()

        style.Alignment = DataGridViewContentAlignment.MiddleCenter
        style.ForeColor = Color.Black
        style.BackColor = System.Drawing.SystemColors.Window
        style.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        dgvNames.ColumnHeadersVisible = True
        dgvNames.AutoGenerateColumns = False
        For Each column In dgvNames.Columns
            column.HeaderCell.Style = style
        Next
        If dgvNames.Columns.Count > 4 Then
            dgvNames.ColumnHeadersVisible = True
            dgvNames.Columns(1).HeaderText = "Client ID"
            dgvNames.Columns(2).HeaderText = "Type"
            dgvNames.Columns(3).HeaderText = "First Name"
            dgvNames.Columns(4).HeaderText = "Middle Names"
            dgvNames.Columns(5).HeaderText = "Last Name"
            dgvNames.Columns(6).HeaderText = "DOB"
            dgvNames.Columns(7).HeaderText = "Address"
            dgvNames.Columns(8).HeaderText = "Suburb"
            dgvNames.Columns(9).HeaderText = "City"
            dgvNames.Columns(10).HeaderText = "PostCode"
            dgvNames.Columns(1).Frozen = True
        Else
            dgvNames.ColumnHeadersVisible = False
        End If
        For i = 1 To dgvNames.Columns.Count - 1
            dgvNames.Columns(i).ReadOnly = True
        Next
        If dgvContactMethod.RowCount > 0 And dgvContactMethod.ColumnCount > 1 Then
            dgvContactMethod.Columns(1).Width = (dgvContactMethod.Width / 2) - 15
            dgvContactMethod.Columns(2).Width = (dgvContactMethod.Width / 2) - 15
            dgvContactMethod.Columns(0).Width = 25

        End If
        For Each column In dgvUpdated.Columns
            column.HeaderCell.Style = style
        Next
        If dgvUpdated.RowCount > 0 Then
            dgvUpdated.Columns(1).Width = 0
            dgvUpdated.Columns(1).Visible = False
            dgvUpdated.ColumnHeadersVisible = True
        End If

    End Sub

    Private Function BuildContactXMLDataTable(ByVal clientfinObj As ClientEntity) As DataTable
        Dim table As New DataTable
        ' Create columns in the DataTable.
        table.Columns.Add("Method", GetType(String))
        table.Columns.Add("Value", GetType(String))
        'Add Values in Table
        If Not clientfinObj.PhoneHome Is Nothing Then table.Rows.Add("Phone", clientfinObj.PhoneHome)
        If Not clientfinObj.PhoneMobile Is Nothing Then table.Rows.Add("Mobile", clientfinObj.PhoneMobile)
        If Not clientfinObj.PhoneWork Is Nothing Then table.Rows.Add("Work", clientfinObj.PhoneWork)
        If Not clientfinObj.Email Is Nothing Then table.Rows.Add("Email", clientfinObj.Email)

        Return table

    End Function


    Private Function BuildUpdatedDataTableFromHash() As DataTable
        Dim clientfinPow As ClientEntity
        Dim i As Integer
        Dim table As New DataTable
        Dim kvp As KeyValuePair(Of Integer, String)
        ' Create four typed columns in the DataTable.
        table.Columns.Add("key", GetType(String))
        table.Columns.Add("Name", GetType(String))
        table.Columns.Add("ClientID", GetType(String))
        table.Columns.Add("FirstName", GetType(String), "")
        table.Columns.Add("MiddleNames", GetType(String), "")
        table.Columns.Add("LastName", GetType(String), "")
        table.Columns.Add("DateofBirth", GetType(String), "")
        table.Columns.Add("Phone", GetType(String), "") 'PhoneHome
        table.Columns.Add("Mobile", GetType(String), "")
        table.Columns.Add("Work", GetType(String), "")
        table.Columns.Add("Email", GetType(String), "")
        table.Columns.Add("Address", GetType(String), "")
        table.Columns.Add("Suburb", GetType(String), "")
        table.Columns.Add("City", GetType(String), "")
        table.Columns.Add("Postcode", GetType(String), "")
        table.Columns.Add("Country", GetType(String), "")
        'kvp = lstClientName.Text

        For Each i In htfinPower.Keys
            clientfinPow = htfinPower(i)
            table.Rows.Add(i, clientfinPow.Name_MC, clientfinPow.ClientId, clientfinPow.FirstName, clientfinPow.MiddleNames, clientfinPow.LastName, clientfinPow.DateOfBirth, _
                           clientfinPow.PhoneHome, clientfinPow.PhoneMobile, clientfinPow.PhoneWork, clientfinPow.Email, _
                           clientfinPow.Address, clientfinPow.City, clientfinPow.State, clientfinPow.PostCode, clientfinPow.Country)
        Next

        Return table

    End Function


    Private Sub dgvUpdated_CellClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvUpdated.CellClick
        Dim cell As DataGridViewImageCell = Nothing
        Dim delkey As Integer
        Dim clientfinPower As New ClientEntity

        If e.ColumnIndex = 0 Then
            'Get Image Cell
            cell = CType(dgvUpdated.Rows(e.RowIndex).Cells(e.ColumnIndex), DataGridViewImageCell)
        End If

        If Not (cell Is Nothing) Then
            'Check if Image cell is clicked to remove the record
            delkey = dgvUpdated.Rows(e.RowIndex).Cells(1).Value
            If htfinPower.Contains(delkey) Then
                htfinPower.Remove(delkey)
                RefreshUpdatedGrid()

            End If
        Else
            'Refresh the form with the selected Client data
            LoadModifiedData()
        End If

    End Sub


    Private Sub dgvUpdated_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvUpdated.SelectionChanged
        'Refresh the form with the selected Client data
        LoadModifiedData()
    End Sub

    'Selects the Row in the UpdatedGrid 
    Public Sub SelectRowinUpdatedGrid()

        Dim i As Integer
        If htfinPower.Contains(prevkey) Then
            For i = 0 To dgvUpdated.Rows.Count - 1
                If dgvUpdated.Rows(i).Cells(1).Value = prevkey Then
                    dgvUpdated.Rows(i).Selected = True
                    LoadModifiedData(False)
                End If
            Next
        End If
    End Sub

    'Updates the form with the data selected in the Updated Grid
    Public Sub LoadModifiedData(Optional ByVal selected As Boolean = True)
        Dim tempClientId As String = ""
        Dim tempName As String = ""
        Dim tempKey As Integer = 0
        Dim i As Integer = 0

        Dim ClientfinPowerObj As ClientEntity

        If Not dgvUpdated.CurrentRow Is Nothing Then

            gpbxfinPowDetails.Enabled = True

            If Not IsDBNull(dgvUpdated.CurrentRow.Cells(3).Value) Then
                tempClientId = dgvUpdated.CurrentRow.Cells(3).Value
            End If
            If Not IsDBNull(dgvUpdated.CurrentRow.Cells(2).Value) Then
                tempName = dgvUpdated.CurrentRow.Cells(2).Value
            End If
            If Not IsDBNull(dgvUpdated.CurrentRow.Cells(1).Value) Then
                tempKey = dgvUpdated.CurrentRow.Cells(1).Value
            End If

            If htfinPower.Contains(tempKey) Then
                ClientfinPowerObj = CType(htfinPower.Item(tempKey), ClientEntity)
            End If

            If selected Then
                For i = 0 To lstClientName.Items.Count - 1
                    If lstClientName.Items(i).key = tempKey Then
                        lstClientName.SetSelected(i, True)
                    End If
                Next
            End If

            'Update Client Names Grid
            If Not tempName = "" Then
                For i = 0 To dgvNames.RowCount - 1
                    If dgvNames.Rows(i).Cells(1).Value = tempClientId Then
                        dgvNames.Rows(i).Cells(0).Value = True
                        dgvNames.Rows(i).Selected = True
                    Else
                        dgvNames.Rows(i).Cells(0).Value = False
                    End If
                Next
            End If
            'Address and ContactMethods selection
            If Not ClientfinPowerObj Is Nothing Then

                chkAddress.Checked = ClientfinPowerObj.AddressChecked

                For i = 0 To dgvContactMethod.RowCount - 1
                    Select Case (dgvContactMethod.Rows(i).Cells(1).Value).ToString.ToLower
                        Case "phone"

                            dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.PhoneHomeChecked

                        Case "mobile"
                            dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.PhoneMobileChecked

                        Case "work"

                            dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.PhoneWorkChecked
                        Case "email"

                            dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.EmailChecked

                    End Select
                Next
            End If
        End If
    End Sub

    'Updates the XML file and saves into the finPOWER Import Filder
    Private Function updateElement() As Boolean
        Dim OutputXMLPath As String
        Dim inputXmlPath As String
        Dim updatedXML As String = ""
        Dim doc As XmlDocument = New XmlDocument()
        Dim xdoc As New XmlDocument
        Dim ClientfinUpd As New ClientEntity
        Dim nodeListChild As XmlNodeList
        Dim xNodeJointName As XmlNode
        Dim xNodeChild As XmlNode

        Dim xNodeAddresses As XmlNode
        Dim xNodeContactMethods As XmlNode
        Dim xRootNodeList As XmlNodeList
        'Dim kvp As KeyValuePair(Of String, String)
        Dim j As Integer
        Dim ClientId As String = ""
        Dim Fname As String = ""
        Dim LName As String = ""
        Dim DateOfBirth As String = ""
        Dim Address As String = ""
        Dim City As String = ""
        Dim Suburb As String = ""
        Dim Country As String = ""
        Dim PostCode As String = ""
        Dim PhoneHome As String = ""
        Dim PhoneWork As String = ""
        Dim PhoneMobile As String = ""
        Dim eMail As String = ""

        Dim isAka As Boolean
        Dim i As Integer = 0
        Dim xNode As XmlNode
        Dim xNodeRoot As XmlNode
        Dim xmlAttr As XmlAttribute
        Dim dt As DataTable
        Dim Risk As Integer = 0
        Dim JnRisk As Integer = 0
        Dim clientType As String = ""

        'Get the absolute path of the XML file
        inputXmlPath = FinPowerOpObj.getEnquiryFilePath() & FinPowerOpObj.getXMLFileName()
        'Read XML
        Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)

        xdoc.LoadXml(xmlFile)
        'Get client Risk Assessment
        dt = FinPowerOpObj.getTrueTrackClientDetails(ClientTrueTrackId)
        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                Risk = Convert.ToInt32(dt.Rows(0).Item("AMLRisk").ToString) 'MAIN
                JnRisk = Convert.ToInt32(dt.Rows(0).Item("JnAMLRisk").ToString) ' Joint Name
            End If
        End If
        xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
        xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")
        xmlAttr = xNode.Attributes("LoanId")
        If Not xmlAttr Is Nothing Then
            Loancode = xmlAttr.Value
        End If

        '==============================================
        'Read Clients One by One
        '==============================================
        xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
        For Each xNodeJointName In xRootNodeList
            isAka = False
            clientType = ""
            ClientfinUpd = New ClientEntity
            i += 1
            If htfinPower.ContainsKey(i) Then
                ClientfinUpd = htfinPower(i)
            End If

            xNode = xNodeJointName.SelectSingleNode("Comment")
            If xNode IsNot Nothing Then
                If xNode.InnerText.ToUpper = "AKA" Then
                    'Check if AKA, the applicant data
                    isAka = True
                End If
            End If
            xNode = xNodeJointName.SelectSingleNode("Type")
            If xNode IsNot Nothing Then
                If xNode.InnerText.ToUpper = "MAIN" Then
                    ClientId1 = ClientfinUpd.ClientId
                    clientType = "MAIN"
                End If
                If xNode.InnerText.ToUpper = "JOINT NAME" Then
                    If Not isAka Then ClientId2 = ClientfinUpd.ClientId
                    clientType = "JOINT NAME"
                End If
            End If
            If Not ClientfinUpd.ClientId Is Nothing Then
                nodeListChild = xNodeJointName.SelectNodes("Client")

                For Each xNodeChild In nodeListChild
                    If ClientfinUpd.NameChecked Then
                        xmlAttr = xNodeChild.Attributes("ClientId")
                        If Not xmlAttr Is Nothing Then
                            xmlAttr.Value = ClientfinUpd.ClientId
                        End If

                        xNode = xNodeChild.SelectSingleNode("LastName")
                        If xNode IsNot Nothing Then
                            'xNode.RemoveAll()
                            xNode.InnerText = ClientfinUpd.LastName
                        End If
                        xNode = xNodeChild.SelectSingleNode("FirstName")
                        If xNode IsNot Nothing Then
                            'xNode.RemoveAll()
                            xNode.InnerText = ClientfinUpd.FirstName
                        End If
                        xNode = xNodeChild.SelectSingleNode("MiddleNames")
                        If xNode IsNot Nothing Then
                            'xNode.RemoveAll()
                            xNode.InnerText = ClientfinUpd.MiddleNames
                        End If
                        xNode = xNodeChild.SelectSingleNode("Type")
                        If xNode IsNot Nothing Then
                            'xNode.RemoveAll()
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("Title")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("Salutation")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("Gender")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("DateOfBirth")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("MaritalStatus")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("Occupation")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        xNode = xNodeChild.SelectSingleNode("BranchId")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                        If clientType = "JOINT NAME" Then
                            xNode = xNodeChild.SelectSingleNode("UserField5")
                            If xNode IsNot Nothing Then
                                xNode.InnerText = FinPowerOpObj.getCustomerRiskIndicator(JnRisk)
                            End If
                        End If

                        '==============================================
                        'Update UserFields
                        '==============================================
                        If clientType = "MAIN" Then
                            xNode = xNodeChild.SelectSingleNode("UserField5")
                            If xNode IsNot Nothing Then
                                xNode.InnerText = FinPowerOpObj.getCustomerRiskIndicator(Risk)
                            End If
                        End If

                    End If
                    'Address and Contact Details
                    If ClientfinUpd.AddressChecked Then
                        xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                        If xNodeAddresses IsNot Nothing Then
                            xNodeAddresses.ParentNode.RemoveChild(xNodeAddresses)
                            'xNode = xNodeAddresses.SelectSingleNode("Address[@Type='Physical']")
                            'If xNode IsNot Nothing Then
                            '    xNode.RemoveAll()
                            'End If
                        End If
                    End If
                    xNodeContactMethods = xNodeChild.SelectSingleNode("ContactMethods")
                    If ClientfinUpd.PhoneMobileChecked Then
                        xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Mobile']")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                    End If
                    If ClientfinUpd.PhoneHomeChecked Then
                        xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Phone']")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                    End If
                    If ClientfinUpd.PhoneWorkChecked Then
                        xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Work']")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                    End If
                    If ClientfinUpd.EmailChecked Then
                        xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Email']")
                        If xNode IsNot Nothing Then
                            xNode.ParentNode.RemoveChild(xNode)
                        End If
                    End If
                Next

            End If
        Next
        '==============================================
        'WRITE XML file to finPOWER Import folder
        '==============================================
        OutputXMLPath = FinPowerOpObj.getImportFilePath() & FinPowerOpObj.getXMLFileName()

        'Resources finPowerImportFolder
        System.IO.File.WriteAllText(OutputXMLPath, xdoc.InnerXml)

        '==============================================
        'Update Client Details in TRUE TRACK
        '==============================================
        FinPowerOpObj.UpdateLoanDetails(EnquiryId, Loancode, ClientId1, ClientId2)

        'Delete XML file from Enquiry Folder after successfull transfer of file to finPOWE Import folder
        Try
            System.IO.File.Delete(inputXmlPath)
        Catch ex As Exception
            MsgBox("Error while deleting file " & FinPowerOpObj.getXMLFileName() & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)
            DialogResult = DialogResult.None
        End Try

        Return True

    End Function



    Private Function formatPhone(ByVal strPhone As String, ByVal type As String) As String

        Dim tempPhone As String = ""
        If type = "Mobile" Then
            If strPhone.Length > 3 Then
                tempPhone = "(" & strPhone.Substring(0, 3) & ")"
            Else
                tempPhone = "(" & strPhone.Substring(0, strPhone.Length) & ")"
            End If
            If strPhone.Length > 3 Then
                If strPhone.Length < 7 Then
                    tempPhone += " " & strPhone.Substring(3, strPhone.Length - 3)
                Else
                    tempPhone += " " & strPhone.Substring(3, 2)
                End If
            End If
            If strPhone.Length > 6 Then
                If strPhone.Length < 10 Then
                    tempPhone += " " & strPhone.Substring(6, strPhone.Length - 6)
                Else
                    tempPhone += " " & strPhone.Substring(6, 4)
                End If
            End If
        ElseIf type = "Work" Or type = "Home" Then
            If strPhone.Length > 2 Then
                tempPhone = "(" & strPhone.Substring(0, 2) & ")"
            Else
                tempPhone = "(" & strPhone.Substring(0, strPhone.Length) & ")"
            End If
            If strPhone.Length > 2 Then
                If strPhone.Length < 6 Then
                    tempPhone += " " & strPhone.Substring(2, strPhone.Length - 2)
                Else
                    tempPhone += " " & strPhone.Substring(2, 3)
                End If
            End If
            If strPhone.Length > 5 Then
                If strPhone.Length < 10 Then
                    tempPhone += " " & strPhone.Substring(5, strPhone.Length - 5)
                Else
                    tempPhone += " " & strPhone.Substring(5, 4)
                End If
            End If
        End If


        Return tempPhone

    End Function


    Private Sub lstClientName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstClientName.SelectedIndexChanged
        RemoveHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged

        prevkey = lstClientName.SelectedItem.key
        'lblfinNameForXMName.Text = "for " & lstClientName.SelectedItem.value
        lblDatafinPOWER.Text = "Matches from finPOWER for " & lstClientName.SelectedItem.value
        'gpbxfinPower.Name = "Matches from finPOWER for " & lstClientName.SelectedItem.value 'does not work??
        'Get details from finPower
        getSelectedNameInfo()

        'Set Grid properties
        SelectRowinUpdatedGrid()

        AddHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged
    End Sub


    'Get Data from FinPower for the selected Name in the ListBox
    Public Sub getSelectedNameInfo()
        Dim i As Integer
        Dim clientFinPower1 As ClientEntity
        Dim myDataSet As New Data.DataSet

        chkAddress.Checked = False

        i = lstClientName.SelectedItem.key

        dgvContactMethod.DataSource = Nothing

        If FinPowerOpObj.htXML.ContainsKey(i) Then
            clientFinPower1 = CType(FinPowerOpObj.htXML.Item(i), ClientEntity)

            txtDOB.Text = String.Format("{0:dd/MM/yyyy}", clientFinPower1.DateOfBirth)
            txtFirstName.Text = clientFinPower1.FirstName
            txtLastName.Text = clientFinPower1.LastName
            txtAddress.Text = clientFinPower1.Address
            txtSuburb.Text = clientFinPower1.City
            txtCity.Text = clientFinPower1.State
            txtPostCode.Text = clientFinPower1.PostCode
            'Bind ContactMethods XML
            dgvContactsXML.DataSource = BuildContactXMLDataTable(clientFinPower1)
            'Get data from finPOWER
            myDataSet = FinPowerOpObj.GetfinPOWERClientInfo(clientFinPower1.FirstName, clientFinPower1.LastName, clientFinPower1.DateOfBirth)

            Me.dgvNames.DataSource = myDataSet.Tables(0)

            'Set Grid Properties
            SetDatagridColumns()
            'Select the row in the Modified data UpdatedGrid
            If Not isShowSelected Then
                For i = 0 To dgvUpdated.RowCount - 1
                    If dgvUpdated.Rows(i).Cells(1).Value = prevkey And dgvUpdated.Rows(i).Selected = False Then
                        dgvUpdated.Rows(i).Selected = True
                    Else
                        dgvUpdated.Rows(i).Selected = False
                    End If
                Next
            End If
        Else
            Me.dgvNames.DataSource = Nothing

        End If


    End Sub


    Private Sub btnCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommit.Click

        If dgvUpdated.RowCount = 0 Then

            If MsgBox("No finPOWER client records selected. Do you want to continue? ", MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.No Then
                DialogResult = Windows.Forms.DialogResult.None
                Exit Sub
            Else
                'Saves the changes to XML and updates LoanNum into DueDeligence
                If updateElement() Then
                    DialogResult = Windows.Forms.DialogResult.OK
                    Me.Close()
                End If
            End If
        Else
            'Saves the changes to XML and updates LoanNum into DueDeligence
            If updateElement() Then
                DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If


    End Sub


    Private Sub FormLoadSetting()
        Me.dgvUpdated.ColumnHeadersVisible = False
        Me.dgvNames.ColumnHeadersVisible = False
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Me.Close()

    End Sub
    'Adds the selections made into the temporary Updated Grid and Saves the ClientEntity Object in HashTable 
    Private Sub btnAddtoGrid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoGrid.Click
        RemoveHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged
        Dim icnt As Integer
        Dim jKount As Integer
        Dim tClientId As String = ""
        Dim chkcell As DataGridViewCheckBoxCell
        Dim row As DataGridViewRow
        For Each row In dgvNames.Rows
            chkcell = row.Cells(0)
            If chkcell.EditedFormattedValue = True Then
                icnt = 1
            End If
        Next
        If icnt = 0 Then
            MessageBox.Show("Please select a client to update", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        ElseIf icnt > 1 Then
            MessageBox.Show("Cannot select more than one client to update", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        'Add changes into ClientEntity Object and adds into the HashTable  
        If AddClientChanges(prevkey) Then
            RefreshUpdatedGrid()

            For icnt = 0 To dgvUpdated.RowCount - 1
                If dgvUpdated.Rows(icnt).Cells(1).Value = prevkey Then
                    dgvUpdated.Rows(icnt).Selected = True
                Else
                    dgvUpdated.Rows(icnt).Selected = False
                End If
            Next
        Else 'User Opts not to add in the UpdatedGrid if the modified Client is already exists in the UpdatedGrid
            For icnt = 0 To dgvUpdated.RowCount - 1
                If dgvUpdated.Rows(icnt).Selected = True Then
                    tClientId = dgvUpdated.Rows(icnt).Cells(3).Value

                    For jKount = 0 To dgvNames.Rows.Count - 1
                        chkcell = dgvNames.Rows(jKount).Cells(0)
                        If dgvNames.Rows(jKount).Cells(1).Value = tClientId Then
                            dgvNames.Rows(jKount).Cells(0).Value = True
                            dgvNames.Rows(jKount).Selected = True
                        Else
                            dgvNames.Rows(jKount).Cells(0).Value = False
                        End If
                    Next
                    Exit For
                End If
            Next
        End If
        AddHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged
    End Sub




   
End Class


'DTO class used in ValidateXML
'Used to store Data from FinPower as well as from MemberCentreXML
Public Class ClientEntity
    Public key As Integer
    Public ClientId As String
    Public FirstName_MC As String
    Public MiddleNames_MC As String
    Public LastName_MC As String
    Public Name_MC As String
    Public Type As String
    Public FirstName As String
    Public MiddleNames As String
    Public LastName As String
    Public DateOfBirth As String
    Public PhoneWork As String
    Public PhoneMobile As String
    Public PhoneHome As String

    Public Email As String
    Public Address As String
    Public City As String
    Public State As String
    Public Country As String
    Public PostCode As String
    Public Comment As String
    Public Sequence As Integer
    Public NameChecked As Boolean
    Public AddressChecked As Boolean
    Public EmailChecked As Boolean
    Public PhoneHomeChecked As Boolean
    Public PhoneWorkChecked As Boolean
    Public PhoneMobileChecked As Boolean

End Class