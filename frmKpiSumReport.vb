﻿Imports Microsoft.Office.Interop

Public Class FrmKpiSumReport
    Dim sumTable As DataTable
    Dim sumRetail As DataTable
    Dim sumApplications As DataTable
    Dim sumWithdrawn As DataTable
    Dim sumDeclined As DataTable
    Dim sumDrawdown As DataTable
    Dim sumMonthDrawdown As DataTable
    Dim sumPending As DataTable
    Dim thisPayoutStartMonth As Integer = 0
    Dim thisPayoutStartYear As Integer = 0
    Dim thisPayoutEndMonth As Integer = 0
    Dim thisPayoutEndYear As Integer = 0
    Dim startdate As Date
    Dim enddate As Date
    Dim datelimit As Date 'to reduce the dataset for calculating the number of pending Enquiries

    ''' <summary>
    ''' Get number of new Enquiries by date
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'EnquiryId, Classification AS ClassEnum, Description AS Classification, DealerId, CurrentStatus, 
        '[DateTime], ApplicationCode, LoanValue, ProcessDate, PayoutMonth, PrelimSource

        'Enum|Name                          |Description
        '0  |BranchPersonalLoansSecured     |Branch Personal Loans Secured
        '1  |BranchPersonalLoansUnSecured   |Branch Personal Loans Un-secured
        '2  |BranchBusinessLoans            |Branch Business Loans
        '3  |ConsumerGoodsLoans             |Consumer Goods Loans
        '4  |MotorVehicleLoans              |Motor Vehicle Loans 
        '5  |FinanceFacility               |Finance Facility
        '========================
        Dim enqSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        enqSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter()
        Dim enquirySumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        enquirySumTable = enqSumTableAdapter.GetDataByEnquiryDate(sDate, eDate)

        Return enquirySumTable
    End Function
    ''' <summary>
    ''' Get Applications data by Date
    ''' </summary>
    ''' <param name="sDate">Start Date</param>
    ''' <param name="eDate">End Date</param>
    ''' <returns></returns>
    ''' <remarks>Queries the StatusHistory (SH) table, finds SH.Status = 'Due Diligence' filtered by SH.StatusChangeDate</remarks>
    Public Function GetApplications(ByVal sDate As Date, ByVal eDate As Date) As DataTable

        Dim appSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        appSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter()
        Dim appSumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        appSumTable = appSumTableAdapter.GetDataByApplication(sDate, eDate)

        Return appSumTable
    End Function

    ''' <summary>
    ''' Get Withdrawn data by Date
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns></returns>
    ''' <remarks>Queries the StatusHistory (SH) table, finds SH.Status = 'Withdrawn' filtered by SH.StatusChangeDate</remarks>
    Public Function GetWithdrawn(ByVal sDate As Date, ByVal eDate As Date) As DataTable

        Dim withdrawnSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        withdrawnSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter()
        Dim withdrawnSumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        withdrawnSumTable = withdrawnSumTableAdapter.GetDataByWithdrawn(sDate, eDate)

        Return withdrawnSumTable
    End Function

    ''' <summary>
    ''' Get Declined data by Date
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns></returns>
    ''' <remarks>Queries the StatusHistory (SH) table, finds SH.Status = 'Declined' filtered by SH.StatusChangeDate</remarks>
    Public Function GetDeclined(ByVal sDate As Date, ByVal eDate As Date) As DataTable

        Dim declinedSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        declinedSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter()
        Dim declinedSumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        declinedSumTable = declinedSumTableAdapter.GetDataByDeclined(sDate, eDate)

        Return declinedSumTable
    End Function

    ''' <summary>
    ''' Get Drawdown Summary Data by Process Date
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDrawdownSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable

        Dim drawdownSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        drawdownSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter()
        Dim drawdownSumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        drawdownSumTable = drawdownSumTableAdapter.GetDataByProcessDate(sDate, eDate)

        Return drawdownSumTable
    End Function
    ''' <summary>
    ''' Get Drawdown Summary Data by Payout Month
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMonthDrawdownSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable

        Dim monthDrawdownSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        monthDrawdownSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter()
        Dim monthDrawdownSumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        monthDrawdownSumTable = monthDrawdownSumTableAdapter.GetDataByPayoutMonth(sDate, eDate)

        Return monthDrawdownSumTable
    End Function

    ''' <summary>
    ''' Get All Pending Enquiries as at end date
    ''' </summary>    
    ''' <param name="lDate">Limit date</param>
    ''' <param name="eDate">End date</param>
    ''' <returns>DataTable</returns>
    ''' <remarks>Limit date used to reduce the dataset for calculating the number of pending Enquiries, set to one year in past</remarks>
    Public Function GetPendingSummary(ByVal lDate As Date, ByVal eDate As Date) As DataTable

        Dim pendingSumTableAdapter As EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        pendingSumTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.SummaryReportTableAdapter
        Dim pendingSumTable As EnquiryWorkSheetDataSet.SummaryReportDataTable
        pendingSumTable = pendingSumTableAdapter.GetDataByPending(lDate, eDate)

        Return pendingSumTable
    End Function

    ''' <summary>
    ''' Get KPI Report Retailer Data
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetRetail(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        Dim retailTableAdapter As EnquiryWorkSheetDataSetTableAdapters.ActiveDealersSummaryReportTableAdapter
        retailTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.ActiveDealersSummaryReportTableAdapter
        Dim retailTable As EnquiryWorkSheetDataSet.ActiveDealersSummaryReportDataTable
        retailTable = retailTableAdapter.GetDataByEnquiryDate(sDate, eDate)

        Return retailTable
    End Function




    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If ResetValues() Then
            btnExcel.Visible = False
            btnReset.Visible = False
        End If

        If ApplicationAccessRights.isAdministrator Or ApplicationAccessRights.isSystemsManager Then
            pbReportInformation.Visible = True
        Else
            pbReportInformation.Visible = False
        End If


    End Sub

    Private Sub BtnRunReportClick(sender As Object, e As EventArgs) Handles btnRunReport.Click
        Cursor = Cursors.WaitCursor
        startdate = DateTime.Parse(dtpStartDate.Value.Date)
        enddate = DateTime.Parse(dtpEndDate.Value.Date.AddDays(1)) 'Add a day to the end date to get full day in the filter
        datelimit = DateTime.Parse(dtpEndDate.Value.Date.AddDays(-365))

        sumTable = GetSummary(startdate, enddate)
        sumRetail = GetRetail(startdate, enddate)
        sumApplications = GetApplications(startdate, enddate)
        sumWithdrawn = GetWithdrawn(startdate, enddate)
        sumDeclined = GetDeclined(startdate, enddate)
        sumDrawdown = GetDrawdownSummary(startdate, enddate)
        sumPending = GetPendingSummary(datelimit, enddate)

        ' Declare an object variable.
        Dim sumObject As Object
        '*** Retail Dealers ***
        dgvRetail.DataSource = sumRetail
        'set dgvRetail column widths
        Dim column As DataGridViewColumn
        column = dgvRetail.Columns(0)
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        column = dgvRetail.Columns(1)
        column.Width = 50
        '*** Advertising ***
        sumObject = sumTable.Compute("Count(EnquiryId)", "PrelimSource Like 'Advertising%'")
        lblAdRepliesValue.Text = sumObject.ToString
        '*** Client Retention ***
        sumObject = sumTable.Compute("Count(EnquiryId)", "PrelimSource Like '%Retention%'")
        lblClientsRelentValue.Text = sumObject.ToString
        '*** Enquiries ***
        'Enquiries PL
        sumObject = sumTable.Compute("Count(EnquiryId)", "ClassEnum = 0")
        lblEnquiriesPlValue.Text = sumObject.ToString
        'Enquiries CL
        sumObject = sumTable.Compute("Count(EnquiryId)", "ClassEnum = 3")
        lblEnquiriesClValue.Text = sumObject.ToString
        'Enquiries VL
        sumObject = sumTable.Compute("Count(EnquiryId)", "ClassEnum = 4")
        lblEnquiriesVlValue.Text = sumObject.ToString
        'Enquiries BL
        sumObject = sumTable.Compute("Count(EnquiryId)", "ClassEnum = 2")
        lblEnquiriesBlValue.Text = sumObject.ToString
        'Enquiries Unsecured
        sumObject = sumTable.Compute("Count(EnquiryId)", "ClassEnum = 1")
        lblEnquiriesUnsecuredValue.Text = sumObject.ToString
        'Enquiries Finance Facility
        sumObject = sumTable.Compute("Count(EnquiryId)", "ClassEnum = 5")
        lblEnquiriesFfValue.Text = sumObject.ToString
        'Enquiries Total
        Dim enquiryTotal As Integer = CInt(lblEnquiriesPlValue.Text) + CInt(lblEnquiriesClValue.Text) + CInt(lblEnquiriesVlValue.Text) + CInt(lblEnquiriesBlValue.Text) + CInt(lblEnquiriesUnsecuredValue.Text) + CInt(lblEnquiriesFfValue.Text)
        lblEnquiriesTotalValue.Text = enquiryTotal
        '*** Applications ***
        'Applications PL
        sumObject = sumApplications.Compute("Count(EnquiryId)", "ClassEnum = 0") ' AND (Not ApplicationCode Is Null AND LEN(ApplicationCode) > 0)")
        lblApplicationsPlValue.Text = sumObject.ToString
        'Applications CL
        sumObject = sumApplications.Compute("Count(EnquiryId)", "ClassEnum = 3") ' AND (Not ApplicationCode Is Null AND LEN(ApplicationCode) > 0)")
        lblApplicationsClValue.Text = sumObject.ToString
        'Applications VL
        sumObject = sumApplications.Compute("Count(EnquiryId)", "ClassEnum = 4") ' AND (Not ApplicationCode Is Null AND LEN(ApplicationCode) > 0)")
        lblApplicationsVlValue.Text = sumObject.ToString
        'Applications BL
        sumObject = sumApplications.Compute("Count(EnquiryId)", "ClassEnum = 2") ' AND (Not ApplicationCode Is Null AND LEN(ApplicationCode) > 0)")
        lblApplicationsBlValue.Text = sumObject.ToString
        'Applications Unsecured
        sumObject = sumApplications.Compute("Count(EnquiryId)", "ClassEnum = 1") ' AND (Not ApplicationCode Is Null AND LEN(ApplicationCode) > 0)")
        lblApplicationsUnsecuredValue.Text = sumObject.ToString
        'Applications Finance Facility
        sumObject = sumApplications.Compute("Count(EnquiryId)", "ClassEnum = 5") ' AND (Not ApplicationCode Is Null AND LEN(ApplicationCode) > 0)")
        lblApplicationsFfValue.Text = sumObject.ToString
        'Application Total
        Dim applicationTotal As Integer = CInt(lblApplicationsPlValue.Text) + CInt(lblApplicationsClValue.Text) + CInt(lblApplicationsVlValue.Text) + CInt(lblApplicationsBlValue.Text) + CInt(lblApplicationsUnsecuredValue.Text) + CInt(lblApplicationsFfValue.Text)
        lblApplicationsTotalValue.Text = applicationTotal
        '*** Withdrawn ***
        'Withdrawn PL
        sumObject = sumWithdrawn.Compute("Count(EnquiryId)", "ClassEnum = 0") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_WITHDRAWN & "'")
        lblWithdrawnPlValue.Text = sumObject.ToString
        'Withdrawn CL
        sumObject = sumWithdrawn.Compute("Count(EnquiryId)", "ClassEnum = 3") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_WITHDRAWN & "'")
        lblWithdrawnClValue.Text = sumObject.ToString
        'Withdrawn VL
        sumObject = sumWithdrawn.Compute("Count(EnquiryId)", "ClassEnum = 4") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_WITHDRAWN & "'")
        lblWithdrawnVlValue.Text = sumObject.ToString
        'Withdrawn BL
        sumObject = sumWithdrawn.Compute("Count(EnquiryId)", "ClassEnum = 2") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_WITHDRAWN & "'")
        lblWithdrawnBlValue.Text = sumObject.ToString
        'Withdrawn Unsecured
        sumObject = sumWithdrawn.Compute("Count(EnquiryId)", "ClassEnum = 1") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_WITHDRAWN & "'")
        lblWithdrawnUnsecuredValue.Text = sumObject.ToString
        'Withdrawn Finance Facility
        sumObject = sumWithdrawn.Compute("Count(EnquiryId)", "ClassEnum = 5") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_WITHDRAWN & "'")
        lblWithdrawnFfValue.Text = sumObject.ToString
        'Withdrawn total
        Dim withdrawnTotal As Integer = CInt(lblWithdrawnPlValue.Text) + CInt(lblWithdrawnClValue.Text) + CInt(lblWithdrawnVlValue.Text) + CInt(lblWithdrawnBlValue.Text) + CInt(lblWithdrawnUnsecuredValue.Text) + CInt(lblWithdrawnFfValue.Text)
        lblWithdrawnTotalValue.Text = withdrawnTotal
        '*** Declined ***
        'Declined PL
        sumObject = sumDeclined.Compute("Count(EnquiryId)", "ClassEnum = 0") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_DECLINED & "'")
        lblDeclinedPlValue.Text = sumObject.ToString
        'Declined CL
        sumObject = sumDeclined.Compute("Count(EnquiryId)", "ClassEnum = 3") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_DECLINED & "'")
        lblDeclinedClValue.Text = sumObject.ToString
        'Declined VL
        sumObject = sumDeclined.Compute("Count(EnquiryId)", "ClassEnum = 4") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_DECLINED & "'")
        lblDeclinedVlValue.Text = sumObject.ToString
        'Declined BL
        sumObject = sumDeclined.Compute("Count(EnquiryId)", "ClassEnum = 2") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_DECLINED & "'")
        lblDeclinedBlValue.Text = sumObject.ToString
        'Declined Unsecured
        sumObject = sumDeclined.Compute("Count(EnquiryId)", "ClassEnum = 1") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_DECLINED & "'")
        lblDeclinedUnsecuredValue.Text = sumObject.ToString
        'Declined Finance Facility
        sumObject = sumDeclined.Compute("Count(EnquiryId)", "ClassEnum = 5") ' AND CurrentStatus = '" & Constants.CONST_CURRENT_STATUS_DECLINED & "'")
        lblDeclinedFfValue.Text = sumObject.ToString
        'Declined total
        Dim declinedTotal As Integer = CInt(lblDeclinedPlValue.Text) + CInt(lblDeclinedClValue.Text) + CInt(lblDeclinedVlValue.Text) + CInt(lblDeclinedBlValue.Text) + CInt(lblDeclinedUnsecuredValue.Text) + CInt(lblDeclinedFfValue.Text)
        lblDeclinedTotalValue.Text = declinedTotal
        '*** Due-diligence (Pending) ***
        'Due-diligence PL
        sumObject = sumPending.Compute("Count(EnquiryId)", "ClassEnum = 0")
        lblDdPlValue.Text = sumObject.ToString
        'Due-diligence CL
        sumObject = sumPending.Compute("Count(EnquiryId)", "ClassEnum = 3")
        lblDdClValue.Text = sumObject.ToString
        'Due-diligence VL
        sumObject = sumPending.Compute("Count(EnquiryId)", "ClassEnum = 4")
        lblDdVlValue.Text = sumObject.ToString
        'Due-diligence BL
        sumObject = sumPending.Compute("Count(EnquiryId)", "ClassEnum = 2")
        lblDdBlValue.Text = sumObject.ToString
        'Due-diligence Unsecured
        sumObject = sumPending.Compute("Count(EnquiryId)", "ClassEnum = 1")
        lblDdUnsecuredValue.Text = sumObject.ToString
        'Due-diligence Finance Facility
        sumObject = sumPending.Compute("Count(EnquiryId)", "ClassEnum = 5")
        lblDdFfValue.Text = sumObject.ToString
        'Due-diligence total
        Dim pendingTotal As Integer = CInt(lblDdPlValue.Text) + CInt(lblDdClValue.Text) + CInt(lblDdVlValue.Text) + CInt(lblDdBlValue.Text) + CInt(lblDdUnsecuredValue.Text) + CInt(lblDdFfValue.Text)
        lblDdTotalValue.Text = pendingTotal
        '*** Drawdowns *** => CashPrice_Advance.Value + OtherAdvance.Value - Deposit.Value + Brokerage
        ' Create Drawdown column
        Dim drawdownColumn As DataColumn = New DataColumn
        With drawdownColumn
            .DataType = System.Type.GetType("System.Decimal")
            .ColumnName = "drawdown"
            .Expression = "CashPrice_Advance + OtherAdvance - Deposit + IsNull(0,Brokerage)"
        End With
        ' Add columns to DataTable
        With sumDrawdown.Columns
            .Add(drawdownColumn)
        End With
        'Drawdown PL
        sumObject = sumDrawdown.Compute("Sum(drawdown)", "ClassEnum = 0")
        If Not IsDBNull(sumObject) Then
            lblDrawdownPlValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
        Else
            lblDrawdownPlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        End If
        sumObject = sumDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 0")
        lblDrawdownPlNum.Text = sumObject.ToString
        'Drawdown CL
        sumObject = sumDrawdown.Compute("Sum(drawdown)", "ClassEnum = 3")
        If Not IsDBNull(sumObject) Then
            lblDrawdownClValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
        Else
            lblDrawdownClValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        End If
        sumObject = sumDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 3")
        lblDrawdownClNum.Text = sumObject.ToString
        'Drawdown VL
        sumObject = sumDrawdown.Compute("Sum(drawdown)", "ClassEnum = 4")
        If Not IsDBNull(sumObject) Then
            lblDrawdownVlValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
        Else
            lblDrawdownVlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        End If
        sumObject = sumDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 4")
        lblDrawdownVlNum.Text = sumObject.ToString
        'Drawdown BL
        sumObject = sumDrawdown.Compute("Sum(drawdown)", "ClassEnum = 2")
        If Not IsDBNull(sumObject) Then
            lblDrawdownBlValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
        Else
            lblDrawdownBlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        End If
        sumObject = sumDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 2")
        lblDrawdownBlNum.Text = sumObject.ToString
        'Drawdown Unsecured
        sumObject = sumDrawdown.Compute("Sum(drawdown)", "ClassEnum = 1")
        If Not IsDBNull(sumObject) Then
            lblDrawdownUnsecuredValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
        Else
            lblDrawdownUnsecuredValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        End If
        sumObject = sumDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 1")
        lblDrawdownUnsecuredNum.Text = sumObject.ToString
        'Drawdown totals
        Dim drawdownTotalNum As Integer = CInt(lblDrawdownPlNum.Text) + CInt(lblDrawdownClNum.Text) + CInt(lblDrawdownVlNum.Text) + CInt(lblDrawdownBlNum.Text) + CInt(lblDrawdownUnsecuredNum.Text)
        lblDrawdownTotalNum.Text = drawdownTotalNum
        Dim drawdownTotalValue As Decimal = CDec(lblDrawdownPlValue.Text) + CDec(lblDrawdownClValue.Text) + CDec(lblDrawdownVlValue.Text) + CDec(lblDrawdownBlValue.Text) + CDec(lblDrawdownUnsecuredValue.Text)
        lblDrawdownTotalValue.Text = FormatCurrency(drawdownTotalValue, , , TriState.True, TriState.True)
        '*** Drawdown by month *** => CashPrice_Advance.Value + OtherAdvance.Value - Deposit.Value + Brokerage
        If thisPayoutStartYear > 0 And thisPayoutStartMonth > 0 And thisPayoutEndYear > 0 And thisPayoutEndYear > 0 Then
            Dim startDate As New Date(CInt(thisPayoutStartYear), CInt(thisPayoutStartMonth), 1, 0, 0, 0)
            Dim endDate As New Date(CInt(thisPayoutEndYear), CInt(thisPayoutEndMonth), 1, 0, 0, 0)
            sumMonthDrawdown = GetMonthDrawdownSummary(startDate, endDate)
            ' Create Drawdown column
            Dim monthDrawdownColumn As DataColumn = New DataColumn
            With monthDrawdownColumn
                .DataType = System.Type.GetType("System.Decimal")
                .ColumnName = "drawdown"
                .Expression = "CashPrice_Advance + OtherAdvance - Deposit + IsNull(0,Brokerage)"
            End With
            ' Add columns to DataTable
            With sumMonthDrawdown.Columns
                .Add(monthDrawdownColumn)
            End With
            'Drawdown PL
            sumObject = sumMonthDrawdown.Compute("Sum(drawdown)", "ClassEnum = 0")
            If Not IsDBNull(sumObject) Then
                lblMonthPlValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
            Else
                lblMonthPlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            End If
            sumObject = sumMonthDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 0")
            lblMonthPlNum.Text = sumObject.ToString
            'Drawdown CL
            sumObject = sumMonthDrawdown.Compute("Sum(drawdown)", "ClassEnum = 3")
            If Not IsDBNull(sumObject) Then
                lblMonthClValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
            Else
                lblMonthClValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            End If
            sumObject = sumMonthDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 3")
            lblMonthClNum.Text = sumObject.ToString
            'Drawdown VL
            sumObject = sumMonthDrawdown.Compute("Sum(drawdown)", "ClassEnum = 4")
            If Not IsDBNull(sumObject) Then
                lblMonthVlValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
            Else
                lblMonthVlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            End If
            sumObject = sumMonthDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 4")
            lblMonthVlNum.Text = sumObject.ToString
            'Drawdown BL
            sumObject = sumMonthDrawdown.Compute("Sum(drawdown)", "ClassEnum = 2")
            If Not IsDBNull(sumObject) Then
                lblMonthBlValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
            Else
                lblMonthBlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            End If
            sumObject = sumMonthDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 2")
            lblMonthBlNum.Text = sumObject.ToString
            'Drawdown Unsecured
            sumObject = sumMonthDrawdown.Compute("Sum(drawdown)", "ClassEnum = 1")
            If Not IsDBNull(sumObject) Then
                lblMonthUnsecuredValue.Text = FormatCurrency(sumObject, , , TriState.True, TriState.True)
            Else
                lblMonthUnsecuredValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            End If
            sumObject = sumMonthDrawdown.Compute("Count(EnquiryId)", "ClassEnum = 1")
            lblMonthUnsecuredNum.Text = sumObject.ToString
            'Month Drawdown totals
            Dim monthTotalNum As Integer = CInt(lblMonthPlNum.Text) + CInt(lblMonthClNum.Text) + CInt(lblMonthVlNum.Text) + CInt(lblMonthBlNum.Text) + CInt(lblMonthUnsecuredNum.Text)
            lblMonthTotalNum.Text = monthTotalNum
            Dim monthTotalValue As Decimal = CDec(lblMonthPlValue.Text) + CDec(lblMonthClValue.Text) + CDec(lblMonthVlValue.Text) + CDec(lblMonthBlValue.Text) + CDec(lblMonthUnsecuredValue.Text)
            lblMonthTotalValue.Text = FormatCurrency(monthTotalValue, , , TriState.True, TriState.True)
        Else
            lblMonthPlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            lblMonthClValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            lblMonthVlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            lblMonthBlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            lblMonthUnsecuredValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
            lblMonthPlNum.Text = "0"
            lblMonthClNum.Text = "0"
            lblMonthVlNum.Text = "0"
            lblMonthBlNum.Text = "0"
            lblMonthUnsecuredNum.Text = "0"

        End If

        'See if can create a valid Excel object
        Dim myExcel As New Excel.Application
        If myExcel Is Nothing Then
            btnExcel.Visible = False
            MessageBox.Show("Excel is not properly installed!!")
        Else
            'Dispose of object
            myExcel = Nothing
            'activate the excel button
            btnExcel.Visible = True
        End If

        btnReset.Visible = True


        Cursor = Cursors.Default
    End Sub

    Private Sub CmbxStartMonthSelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxStartMonth.SelectedIndexChanged
        thisPayoutStartMonth = cmbxStartMonth.SelectedIndex + 1
    End Sub

    Private Sub CmbxStartYearSelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxStartYear.SelectedIndexChanged
        thisPayoutStartYear = cmbxStartYear.SelectedItem
    End Sub

    Private Sub CmbxEndMonthSelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxEndMonth.SelectedIndexChanged
        thisPayoutEndMonth = cmbxEndMonth.SelectedIndex + 1
    End Sub

    Private Sub CmbxEndYearSelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxEndYear.SelectedIndexChanged
        thisPayoutEndYear = cmbxEndYear.SelectedItem
    End Sub


    Private Sub BtnResetClick(sender As Object, e As EventArgs) Handles btnReset.Click
        If ResetValues() Then
            btnExcel.Visible = False
            btnReset.Visible = False
        End If

    End Sub

    Function ResetValues() As Boolean

        'Report Dates
        dtpStartDate.Value = Date.Now
        dtpEndDate.Value = Date.Now
        '*** Advertising ***
        lblAdRepliesValue.Text = "0"
        lblClientsRelentValue.Text = "0"
        '*** Enquiries ***
        lblEnquiriesPlValue.Text = "0"
        lblEnquiriesClValue.Text = "0"
        lblEnquiriesVlValue.Text = "0"
        lblEnquiriesBlValue.Text = "0"
        lblEnquiriesUnsecuredValue.Text = "0"
        lblEnquiriesFfValue.Text = "0"
        lblEnquiriesTotalValue.Text = "0"
        '*** Applications ***
        lblApplicationsPlValue.Text = "0"
        lblApplicationsClValue.Text = "0"
        lblApplicationsVlValue.Text = "0"
        lblApplicationsBlValue.Text = "0"
        lblApplicationsUnsecuredValue.Text = "0"
        lblApplicationsFfValue.Text = "0"
        lblApplicationsTotalValue.Text = 0
        '*** Withdrawn ***
        lblWithdrawnPlValue.Text = "0"
        lblWithdrawnClValue.Text = "0"
        lblWithdrawnVlValue.Text = "0"
        lblWithdrawnBlValue.Text = "0"
        lblWithdrawnUnsecuredValue.Text = "0"
        lblWithdrawnFfValue.Text = "0"
        lblWithdrawnTotalValue.Text = "0"
        '*** Declined ***
        lblDeclinedPlValue.Text = "0"
        lblDeclinedClValue.Text = "0"
        lblDeclinedVlValue.Text = "0"
        lblDeclinedBlValue.Text = "0"
        lblDeclinedUnsecuredValue.Text = "0"
        lblDeclinedFfValue.Text = "0"
        lblDeclinedTotalValue.Text = "0"
        '*** Due-diligence (Pending) ***
        lblDdPlValue.Text = "0"
        lblDdClValue.Text = "0"
        lblDdVlValue.Text = "0"
        lblDdBlValue.Text = "0"
        lblDdUnsecuredValue.Text = "0"
        lblDdFfValue.Text = "0"
        lblDdTotalValue.Text = "0"
        '*** Drawdowns *** 
        lblDrawdownPlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblDrawdownPlNum.Text = "0"
        lblDrawdownClValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblDrawdownClNum.Text = "0"
        lblDrawdownVlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblDrawdownVlNum.Text = "0"
        lblDrawdownBlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblDrawdownBlNum.Text = "0"
        lblDrawdownUnsecuredValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblDrawdownUnsecuredNum.Text = "0"
        lblDrawdownTotalNum.Text = "0"
        lblDrawdownTotalValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        '*** Drawdown by month ***
        lblMonthPlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblMonthClValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblMonthVlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblMonthBlValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblMonthPlNum.Text = "0"
        lblMonthClNum.Text = "0"
        lblMonthVlNum.Text = "0"
        lblMonthBlNum.Text = "0"
        lblMonthUnsecuredNum.Text = "0"
        lblMonthUnsecuredValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        lblMonthTotalNum.Text = "0"
        lblMonthTotalValue.Text = FormatCurrency(0, , , TriState.True, TriState.True)
        'Payout Month Date Range        
        cmbxStartMonth.SelectedIndex = -1
        cmbxStartYear.SelectedIndex = -1
        cmbxEndMonth.SelectedIndex = -1
        cmbxEndYear.SelectedIndex = -1
        thisPayoutStartMonth = 0
        thisPayoutStartYear = 0
        thisPayoutEndMonth = 0
        thisPayoutEndYear = 0
        'Retail Enquiries
        dgvRetail.DataSource = Nothing
        dgvRetail.Refresh()

        Return True
    End Function



    Private Sub BtnExcelClick(sender As Object, e As EventArgs) Handles btnExcel.Click
        'Create Excel object (container)
        Dim myExcel As New Excel.Application
        'Add a wookbook to container
        myExcel.Workbooks.Add()
        ''Open a workbook
        'myExcel.Workbooks.Open("c:\temp\test.xlsx")

        'Add values to a workbook
        'make a cell active and add a value to it
        myExcel.Range("A1").Activate()
        'myExcel.ActiveCell.FormulaR1C1 = "75" 'value is added as text because of .FormulaR1C1
        myExcel.ActiveCell.EntireRow.Font.Bold = True
        myExcel.Range("A1", "B1").Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = "Lending KPI Report"
        myExcel.Range("A2").Activate()
        myExcel.ActiveCell.Value = "Date range: " & startdate & " to " & DateTime.Parse(enddate.AddDays(-1))
        myExcel.ActiveCell.EntireColumn.AutoFit()
        'start data display
        '*** General ***
        myExcel.Range("A4").Activate()
        myExcel.Range("A4", "B4").Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = "General"
        myExcel.ActiveCell.EntireRow.Font.Bold = True
        myExcel.Range("A5").Activate()
        myExcel.ActiveCell.Value = "Advertisement Replies"
        myExcel.Range("B5").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblAdRepliesValue.Text
        myExcel.Range("A6").Activate()
        myExcel.ActiveCell.Value = "Prospected Clients Re-lent"
        myExcel.Range("B6").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblClientsRelentValue.Text
        '*** Week activities ***
        myExcel.Range("A8").Activate()
        myExcel.Range("A8", "B8").Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = "Activities"
        myExcel.ActiveCell.EntireRow.Font.Bold = True
        '*** Enquiries ***
        myExcel.Range("A9").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Enquiries PL"
        myExcel.Range("B9").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblEnquiriesPlValue.Text
        myExcel.Range("A10").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Enquiries CL"
        myExcel.Range("B10").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblEnquiriesClValue.Text
        myExcel.Range("A11").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Enquiries VL"
        myExcel.Range("B11").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblEnquiriesVlValue.Text
        myExcel.Range("A12").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Enquiries BL"
        myExcel.Range("B12").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblEnquiriesBlValue.Text
        myExcel.Range("A13").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Enquiries Unsecured"
        myExcel.Range("B13").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblEnquiriesUnsecuredValue.Text
        myExcel.Range("A14").Activate()
        myExcel.ActiveCell.Font.Italic = True
        myExcel.ActiveCell.Value = "Total Enquiries"
        myExcel.Range("B14").Activate()
        Dim enquiryTotal As Integer = CInt(lblEnquiriesPlValue.Text) + CInt(lblEnquiriesClValue.Text) + CInt(lblEnquiriesVlValue.Text) + CInt(lblEnquiriesBlValue.Text) + CInt(lblEnquiriesUnsecuredValue.Text)
        myExcel.ActiveCell.Value = enquiryTotal
        '*** Applications ***
        myExcel.Range("A15").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Applications PL"
        myExcel.Range("B15").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblApplicationsPlValue.Text
        myExcel.Range("A16").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Applications CL"
        myExcel.Range("B16").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblApplicationsClValue.Text
        myExcel.Range("A17").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Applications VL"
        myExcel.Range("B17").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblApplicationsVlValue.Text
        myExcel.Range("A18").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Applications BL"
        myExcel.Range("B18").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblApplicationsBlValue.Text
        myExcel.Range("A19").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Applications Unsecured"
        myExcel.Range("B19").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblApplicationsUnsecuredValue.Text
        myExcel.Range("A20").Activate()
        myExcel.ActiveCell.Font.Italic = True
        myExcel.ActiveCell.Value = "Total Applications"
        myExcel.Range("B20").Activate()
        Dim applicationTotal As Integer = CInt(lblApplicationsPlValue.Text) + CInt(lblApplicationsClValue.Text) + CInt(lblApplicationsVlValue.Text) + CInt(lblApplicationsBlValue.Text) + CInt(lblApplicationsUnsecuredValue.Text)
        myExcel.ActiveCell.Value = applicationTotal
        '*** Withdrawn ***
        myExcel.Range("A21").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Withdrawn PL"
        myExcel.Range("B21").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblWithdrawnPlValue.Text
        myExcel.Range("A22").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Withdrawn CL"
        myExcel.Range("B22").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblWithdrawnClValue.Text
        myExcel.Range("A23").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Withdrawn VL"
        myExcel.Range("B23").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblWithdrawnVlValue.Text
        myExcel.Range("A24").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Withdrawn BL"
        myExcel.Range("B24").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblWithdrawnBlValue.Text
        myExcel.Range("A25").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Withdrawn Unsecured"
        myExcel.Range("B25").Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblWithdrawnUnsecuredValue.Text
        myExcel.Range("A26").Activate()
        myExcel.ActiveCell.Font.Italic = True
        myExcel.ActiveCell.Value = "Total Withdrawn"
        myExcel.Range("B26").Activate()
        Dim withdrawnTotal As Integer = CInt(lblWithdrawnPlValue.Text) + CInt(lblWithdrawnClValue.Text) + CInt(lblWithdrawnVlValue.Text) + CInt(lblWithdrawnBlValue.Text) + CInt(lblWithdrawnUnsecuredValue.Text)
        myExcel.ActiveCell.Value = withdrawnTotal
        '*** Declined ***
        myExcel.Range("A27").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Declined PL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDeclinedPlValue.Text
        myExcel.Range("A28").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Declined CL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDeclinedClValue.Text
        myExcel.Range("A29").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Declined VL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDeclinedVlValue.Text
        myExcel.Range("A30").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Declined BL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDeclinedBlValue.Text
        myExcel.Range("A31").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Declined Unsecured"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDeclinedUnsecuredValue.Text
        myExcel.Range("A32").Activate()
        myExcel.ActiveCell.Font.Italic = True
        myExcel.ActiveCell.Value = "Total Declined"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        Dim declinedTotal As Integer = CInt(lblDeclinedPlValue.Text) + CInt(lblDeclinedClValue.Text) + CInt(lblDeclinedVlValue.Text) + CInt(lblDeclinedBlValue.Text) + CInt(lblDeclinedUnsecuredValue.Text)
        myExcel.ActiveCell.Value = declinedTotal
        '*** Still Pending ***
        myExcel.Range("A33").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Still Pending PL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDdPlValue.Text
        myExcel.Range("A34").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Still Pending CL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDdClValue.Text
        myExcel.Range("A35").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Still Pending VL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDdVlValue.Text
        myExcel.Range("A36").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Still Pending BL"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDdBlValue.Text
        myExcel.Range("A37").Activate()
        myExcel.ActiveCell.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        myExcel.ActiveCell.Value = "Still Pending Unsecured"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        myExcel.ActiveCell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gainsboro)
        myExcel.ActiveCell.Value = lblDdUnsecuredValue.Text
        myExcel.Range("A38").Activate()
        myExcel.ActiveCell.Font.Italic = True
        myExcel.ActiveCell.Value = "Total Still Pending"
        myExcel.ActiveCell.Offset(0, 1).Activate()
        Dim pendingTotal As Integer = CInt(lblDdPlValue.Text) + CInt(lblDdClValue.Text) + CInt(lblDdVlValue.Text) + CInt(lblDdBlValue.Text) + CInt(lblDdUnsecuredValue.Text)
        myExcel.ActiveCell.Value = pendingTotal
        'reset the active cell to top of page
        myExcel.Range("A1").Activate()
        'Set thin boarders
        myExcel.Range("A9", "B14").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)
        myExcel.Range("A15", "B20").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)
        myExcel.Range("A21", "B26").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)
        myExcel.Range("A27", "B32").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)
        myExcel.Range("A33", "B38").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)
        'Set thick boarders
        myExcel.Range("A4", "B6").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)
        myExcel.Range("A8", "B38").BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic)



        ''to fill in a whole row of information need to create a loop
        ''create a counter, activate the cell according to counter, enter value in active cell, advance the counter
        'Dim counter As Integer = 0
        'myExcel.Range("A2").Activate()
        'Do
        '    myExcel.ActiveCell.Value = counter
        '    myExcel.ActiveCell.Offset(0, 1).Activate()
        '    counter += 1
        'Loop Until counter = 10

        'make workbook visible
        myExcel.Visible = True

    End Sub

#Region "Information"

    Private Sub PbReportInformationClick(sender As Object, e As EventArgs) Handles pbReportInformation.Click
        'Dim msgText As String = "Reports are for Date period selected and applications, withdrawals, declines are all the week that they happened. We populate the Status history table via a database trigger when the Enquiry table is updated. The trigger checks to see if the CurrentStatus is changing, if it is, it logs the data in the StatusHistory table." & vbNewLine & vbNewLine & "Queries:" & vbNewLine & "**********************" & vbNewLine & _
        '    "Get number of new Enquiries by date:" & vbNewLine & "Uses [DateTime] field in the Enquiry table." & vbNewLine & vbNewLine & _
        '    "Get Applications data by Date:" & vbNewLine & "Queries the StatusHistory (SH) table, finds SH.Status = 'Due Diligence' filtered by SH.StatusChangeDate." & vbNewLine & vbNewLine & _
        '    "Get Withdrawn data by Date:" & vbNewLine & "Queries the StatusHistory (SH) table, finds SH.Status = 'Withdrawn' filtered by SH.StatusChangeDate." & vbNewLine & vbNewLine & _
        '    "Get Declined data by Date:" & vbNewLine & "Queries the StatusHistory (SH) table, finds SH.Status = 'Declined' filtered by SH.StatusChangeDate." & vbNewLine & vbNewLine & _
        '    "Get Drawdown Summary Data by Process Date:" & vbNewLine & "Queries the ProcessDate in Enquiry and ArchiveEnquiry tables." & vbNewLine & vbNewLine & _
        '    "Get Drawdown Summary Data by Payout Month:" & vbNewLine & "Queries the PayoutMonth in Enquiry and ArchiveEnquiry tables." & vbNewLine & vbNewLine & _
        '    "Get All Pending Enquiries as at end date:" & vbNewLine & "Does several nested SELECT statements on StatusHistory table to get number of Enquiries with Status of Follow-up or Due-diligence at the time of enddate. Limit date used to reduce the dataset for calculating the number of pending Enquiries, set to one year in past." & vbNewLine & vbNewLine & _
        '    "02/02/2017: Imported the current status for all Enquiries (including Archived Enquiries) into StatisHistory table along with the date that the current status was set. This sets the start for the history. Reports should be accurate from 03/02/2017." & vbNewLine

        'MessageBox.Show(msgText, "Importing customers", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Dim rtfMsgBoxFrm As New RtfMsgBox
        'rtfMsgBoxFrm.PassVariables("Documents\DocNameConvention.rtf", "Document Naming Conventions", MyEnums.MsgBoxMode.FilePath, 650)
        rtfMsgBoxFrm.PassVariables("Documents\ReportInfo.rtf", "Report Information", MyEnums.MsgBoxMode.FilePath, 650)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()


    End Sub

    Private Sub PbReportInformationMouseHover(sender As Object, e As EventArgs) Handles pbReportInformation.MouseHover
        pbReportInformation.Image = My.Resources.questionGsm
    End Sub

    Private Sub PbReportInformationMouseLeave(sender As Object, e As EventArgs) Handles pbReportInformation.MouseLeave
        pbReportInformation.Image = My.Resources.questionBsm
    End Sub

#End Region


   
   
End Class