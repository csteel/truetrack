﻿Imports System.IO
Imports System.Data.SqlClient

Public Class WkShtWizForm2 'Security Due Diligence
    Dim thisEnquiryId As Integer
    Dim thisEnquiryCode As String ' set Enquiry Code
    Dim thisEnquiryType As Integer
    ' set New Wizard Status 
    Dim thisWizardStatus As Integer
    Dim qrgStatus As Integer 'set QRG Status for colour
    Dim thisEnquiryManagerId As Integer 'set EnquiryManagerId
    Dim thisTypeOfMainCustomer As Integer ' set Type of Main Client
    Dim thisContactDisplayName As String
    Dim contactName As String
    Dim contactLastName As String
    Dim contactAddress As String
    Dim thisDealerId As String
    Dim thisDealerName As String
    Dim thisEnquiryResult As String = ""
    Dim progressStatus As Boolean = False
    Dim equityValue As Decimal = 0
    Dim formTitle As String = "Due-diligence Security" 'Me.Text
    'user settings
    Dim newLocX As Integer
    Dim newLocY As Integer
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private Sub CalculateTotalLending()
        Try
            'Calculate Total Lending
            Dim totalLending As Decimal
            For Each dr As DataRow In EnquiryWorkSheetDataSet.Tables("Security").Rows
                totalLending = totalLending + CDec(dr.Item("LendingValue"))
            Next
            'MsgBox("Total Lending = " + CStr(TotalLending))
            lblTotalLendValue.Text = totalLending.ToString("C")
            'Equity Value
            Dim thisLoanAmount As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("LoanValue"))
            Dim thisCurrentLoanAmt As Decimal = CDec(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("CurrentLoanAmt"))
            lblSecurityLoanAmountValue.Text = thisLoanAmount.ToString("C")
            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurntLoan.Visible = True
                lblCurrentLoanAmtCalc.Visible = True
                equityValue = totalLending - thisLoanAmount - thisCurrentLoanAmt
            Else
                lblCurntLoan.Visible = False
                lblCurrentLoanAmtCalc.Visible = False
                equityValue = totalLending - thisLoanAmount
            End If
            If equityValue < 0 Then
                lblEquityValue.ForeColor = Color.Red
            Else
                lblEquityValue.ForeColor = Color.Black
            End If
            'MsgBox("Equity Value = " + CStr(EquityValue))
            lblEquityValue.Text = equityValue.ToString("C")
        Catch ex As Exception
            Log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CalculateTotalAssestValue()
        Try
            'Calculate Total Assest Value
            Dim totalAssetValue As Decimal = 0
            For Each avdr As DataRow In EnquiryWorkSheetDataSet.Tables("Security").Rows
                totalAssetValue = totalAssetValue + CDec(avdr.Item("TodayValue"))
            Next
            lblTotalAssetValue.Text = totalAssetValue.ToString("C")
        Catch ex As Exception
            Log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    'Loads form with parameters from WkShtWizForm1
    Friend Sub PassVariable(ByVal loadEnquiryVar As Integer)
        'get user settings
        Me.ClientSize = New Size(My.Settings.PrelimFormSize)
        Me.Location = New Point(My.Settings.PrelimFormLocation)
        ' setup the reference variables
        thisEnquiryId = loadEnquiryVar
        'set the form tag with EnquiryId
        Me.Tag = thisEnquiryId
        Try
            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            thisEnquiryCode = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryCode")
            'Get Wizard Status
            thisWizardStatus = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("WizardStatus")

            'get EnquiryManagerId
            thisEnquiryManagerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryManagerId")
            'get type of Enquiry
            thisEnquiryType = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("EnquiryType") '10/05/2016 Christopher    
            'set EnquiryType label
            lblTypeLoanEnquiry.Text = MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
            'Load User table
            UsersTableAdapter1.FillByUserId(EnquiryWorkSheetDataSet.Users, thisEnquiryManagerId)
            'get UserName
            lblEnquiryManagerName.Text = EnquiryWorkSheetDataSet.Users.Rows(UsersBindingSource.Position()).Item("FullName")
            'get Dealer ID
            thisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
            'This line of code loads data into the 'EnquiryWorkSheetDataSet.Security' table.
            Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
            'Get Type of main Customer
            thisTypeOfMainCustomer = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("mainCustomerType")
            'set documents button visibility
            'Get String to WorkSheet Drive
            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
            Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
            'Check directory exists
            If Directory.Exists(folderPath) Then
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
            Else
                'get new image
                btnDocs.Image = AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
                'deactivate drag and drop
                Me.btnDocs.AllowDrop = False
            End If
            'Get Contact Display Name
            thisContactDisplayName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactDisplayName")
            'Set form title
            Me.Text = thisContactDisplayName & " - " & Me.Text
            If My.Settings.IsTest = True Then
                Me.Text = "Test Application | " & Me.Text
                Me.BackColor = Color.MistyRose
            End If
            '*************** Set LoanReason display options
            If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                lblCurrentLoan.Visible = True
                lblCurrentLoanAmt.Visible = True
            Else
                lblCurrentLoan.Visible = False
                lblCurrentLoanAmt.Visible = False
            End If
            '*************** end of Set LoanReason display options
            '************************* Cannotate Contact Name
            contactLastName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactLastName")
            contactName = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactTitle") & "  " &
            EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactFirstName") & "  " & contactLastName
            'set lblContactName
            lblContactName.Text = contactName
            '*************** Cannotate Contact address
            contactAddress = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactSuburb") & "  " & EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("ContactCity")
            lblContactAddress.Text = contactAddress
            '*************** Get Dealer name
            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & thisDealerId & "'"
            'MsgBox("Select Statement = " & selectStatement)
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim dealerTempDataAdapter As New SqlDataAdapter(selectCommand)
            Dim dealerTempDataSet As New DataSet
            Dim dealerTempDataTable As New DataTable
            Try
                'dumps results into datatable LoginDataTable
                dealerTempDataAdapter.Fill(dealerTempDataTable)
                'if no matching rows .....
                If dealerTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("No Dealer Name, please try again.")
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                    'if there is a matching row
                ElseIf dealerTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim dealerTempDataRow As DataRow = dealerTempDataTable.Rows(0)
                    thisDealerName = dealerTempDataRow.Item(0)
                    'clear the dataTable and the Connect information
                    dealerTempDataAdapter = Nothing
                    dealerTempDataTable.Clear()
                End If
                'close the connection
                If connection.State <> ConnectionState.Closed Then
                    connection.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try
            lblDealerName.Text = thisDealerName
            '*************** End of Get Dealer name

            '*****************************  get QRGList status
            Dim connectionString1 As String = My.Settings.EnquiryWorkSheetConnectionString
            Dim connection1 As New SqlConnection(connectionString1)
            Dim selectStatement1 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
            'MsgBox("Select Statement = " & selectStatement1)
            Dim selectCommand1 As New SqlCommand(selectStatement1, connection1)
            Dim qrgListTempDataAdapter As New SqlDataAdapter(selectCommand1)
            Dim qrgListTempDataSet As New DataSet
            Dim qrgListTempDataTable As New DataTable
            Try
                'dumps results into datatable LoginDataTable
                qrgListTempDataAdapter.Fill(qrgListTempDataTable)
                'if no matching rows .....
                If qrgListTempDataTable.Rows.Count = 0 Then
                    MessageBox.Show("ERROR: No QRG List, please try again.")
                    'clear the dataTable and the Connect information
                    qrgListTempDataAdapter = Nothing
                    qrgListTempDataTable.Clear()
                    'if there is a matching row
                ElseIf qrgListTempDataTable.Rows.Count = 1 Then
                    'get active value
                    Dim qrgListTempDataRow As DataRow = qrgListTempDataTable.Rows(0)
                    qrgStatus = qrgListTempDataRow.Item(0)
                    'MsgBox("QRG Status = " & QRGStatus)
                    'clear the dataTable and the Connect information
                    qrgListTempDataAdapter = Nothing
                    qrgListTempDataTable.Clear()
                End If
                'close the connection
                If connection1.State <> ConnectionState.Closed Then
                    connection1.Close()
                End If

            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

            'set QRGStatus button colour
            Select Case qrgStatus
                Case 0
                    btnQRGList.BackColor = Color.Transparent
                Case 1
                    btnQRGList.BackColor = Color.Tomato
                Case 2
                    btnQRGList.BackColor = Color.LightGreen
                Case Else
                    btnQRGList.BackColor = Color.Transparent
            End Select
            '************* End of QRG List
            '*************** Set permissions
            'MsgBox("LoggedinPermissionLevel = " & LoggedinPermissionLevel)
            Select Case LoggedinPermissionLevel
                Case Is = 1 'Readonly
                    UpdateAllToolStripMenuItem.Visible = False
                    btnSaveAndExit.Enabled = False
                    btnFinish.Enabled = False
                    btnDocs.Enabled = False
                    btnDeleteSecurity.Enabled = False
                    btnAddSecurity.Enabled = False
                    txtbxSecurityComments.IsReadOnly = True



                Case Is = 2 'Base level, Can take enquiries add comments to current enquiries.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    btnFinish.Enabled = False
                    btnDeleteSecurity.Enabled = False
                    btnAddSecurity.Enabled = False


                Case Is = 3 'Approver Level, Can see /use Application Declined and Application Withdrawn in the File menu. 
                    'Can see /use Archive Enquiry in File menu for their user status
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True
                Case Is = 4
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True

                Case Is = 5
                    'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True

                Case Is > 5 'Systems Manager Level, Open.
                    '--------------------------------------- 2
                    AddCommentToolStripMenuItem1.Visible = True
                    AddCommentToolStripMenuItem.Visible = True
                    EmailToolStripMenuItem.Visible = True
                    '--------------------------------------- 3
                    ApprovalFormToolStripMenuItem1.Visible = True
                    EditCandDToolStripMenuItem.Visible = True
                    ClientDetailsToolStripMenuItem.Visible = True
                    EndEToolStripMenuItem.Visible = True

                Case Else 'Readonly
                    UpdateAllToolStripMenuItem.Visible = False
                    btnSaveAndExit.Enabled = False
                    btnFinish.Enabled = False
                    btnDocs.Enabled = False
                    btnDeleteSecurity.Enabled = False
                    btnAddSecurity.Enabled = False
                    txtbxSecurityComments.IsReadOnly = True

            End Select
            '*************** end of Set permissions

            'set StatusStrip text
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Status: Ready."
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        CalculateTotalLending()
        CalculateTotalAssestValue()
        'change UseWaitCursor back to default (waitCursor running in form continuously for some unknown reason)
        Application.UseWaitCursor = False


    End Sub 'Load Form

    Private Sub FormRefresh()
        Call PassVariable(thisEnquiryId)

    End Sub

    'Private Sub SecurityBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.Validate()
    '        Me.SecurityBindingSource.EndEdit()
    '        Me.SecurityTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Security)
    '        Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, ThisEnquiryId)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    CalculateTotalLending()
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        SecurityBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            'check if data has changed
            Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
            Dim rowView2 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not bindSrc2.Current Is Nothing Then
                rowView2 = CType(bindSrc2.Current, DataRowView)
                'test if any fields have changed
                If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                    'Get user confirmation
                    Dim msg As String
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    msg = "Do you want to Save Changed Data?"   ' Define message.
                    style = MsgBoxStyle.DefaultButton2 Or
                       MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                    title = "Save Record?"   ' Define title.
                    ' Display message.
                    response = MsgBox(msg, style, title)
                    If response = MsgBoxResult.Yes Then   ' User choose Yes.
                        Try
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Saving changed data."
                            'save changes
                            Me.Validate()
                            bindSrc2.EndEdit()
                            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                            'update Status Strip
                            ToolStripStatusLabel1.ForeColor = Color.Black
                            ToolStripStatusLabel1.Text = "Changes saved successfully."
                        Catch ex As Exception
                            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                            MsgBox(ex.Message)
                        End Try
                    Else
                        ' User choose No. Do nothing
                    End If
                    'no changes to save
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                End If
                'no data entered - proceed
            End If

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

        'launch WkShtWizForm1 and pass Enquiry Id
        Dim wkShtWizFrm1 As New WkShtWizForm1
        Try
            wkShtWizFrm1.PassVariable(thisEnquiryId)
            wkShtWizFrm1.Show()
            'Close this Form
            Me.Close()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnAddSecurity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSecurity.Click
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Adding New Security Record."
            Dim addSecurityFrm As New AddSecurityForm
            addSecurityFrm.PassVariable(thisEnquiryId, thisEnquiryManagerId)
            'AddSecurityFrm.ShowDialog(Me)
            If (addSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Security Record added successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Adding of Security Record cancelled. Status: Ready."
            End If
            addSecurityFrm.Dispose()
            Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            CalculateTotalLending()
            CalculateTotalAssestValue()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub btnModSecurity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModSecurity.Click
    '    Try
    '        'test there is data rows
    '        If Not SecurityBindingSource.Current Is Nothing Then
    '            'Get Selected Row
    '            Dim SelectedRowView As Data.DataRowView
    '            Dim SelectedRow As EnquiryWorkSheetDataSet.SecurityRow
    '            SelectedRowView = CType(SecurityBindingSource.Current, System.Data.DataRowView)
    '            SelectedRow = CType(SelectedRowView.Row, EnquiryWorkSheetDataSet.SecurityRow)
    '            'Launch new AddSecurity Form
    '            'pass Security Id so will bring up selected security for modification
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Blue
    '            ToolStripStatusLabel1.Text = "Modifying Security Item."
    '            Dim AddSecurityFrm As New AddSecurityForm
    '            AddSecurityFrm.PassVariable(ThisEnquiryId, ThisEnquiryManagerId, SelectedRow.SecurityId)
    '            'AddSecurityFrm.ShowDialog(Me)
    '            If (AddSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '                'update Status Strip
    '                ToolStripStatusLabel1.ForeColor = Color.Black
    '                ToolStripStatusLabel1.Text = "Security Record successfully updated!"
    '                '    'Call the function you used to populate the data grid..
    '                '    MsgBox("AddSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
    '                '    Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, ThisEnquiryId)
    '            Else
    '                'update Status Strip
    '                ToolStripStatusLabel1.ForeColor = Color.Black
    '                ToolStripStatusLabel1.Text = "Security Record modification cancelled. Status: Ready."
    '            End If
    '            AddSecurityFrm.Dispose()
    '            Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, ThisEnquiryId)
    '            'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
    '            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
    '            CalculateTotalLending()
    '        Else
    '            'update Status Strip
    '            ToolStripStatusLabel1.ForeColor = Color.Red
    '            ToolStripStatusLabel1.Text = "Error: No Data Row selected! Please select a Data Row."
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub SecurityDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles SecurityDataGridView.DoubleClick
        Try
            'test there is data rows
            If Not SecurityBindingSource.Current Is Nothing Then
                'Get Selected Row
                Dim selectedRowView As Data.DataRowView
                Dim selectedRow As EnquiryWorkSheetDataSet.SecurityRow
                selectedRowView = CType(SecurityBindingSource.Current, System.Data.DataRowView)
                selectedRow = CType(selectedRowView.Row, EnquiryWorkSheetDataSet.SecurityRow)
                'Launch new AddSecurity Form
                'pass Security Id so will bring up selected security for modification
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Modifying Security Item."
                Dim addSecurityFrm As New AddSecurityForm
                addSecurityFrm.PassVariable(thisEnquiryId, thisEnquiryManagerId, selectedRow.SecurityId)
                'AddSecurityFrm.ShowDialog(Me)
                If (addSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Security Record successfully updated!"
                    '    'Call the function you used to populate the data grid..
                    '    MsgBox("AddSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK")
                    '    Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, ThisEnquiryId)
                Else
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Security Record modification cancelled. Status: Ready."
                End If
                addSecurityFrm.Dispose()
                Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
                Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
                CalculateTotalLending()
                CalculateTotalAssestValue()
            Else
                'MsgBox("No Data Row selected." + vbCrLf + "Please select a Data Row")
                Try
                    'Launch new AddSecurity Form
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Adding New Security Record."
                    Dim addSecurityFrm As New AddSecurityForm
                    addSecurityFrm.PassVariable(thisEnquiryId, thisEnquiryManagerId)
                    'AddSecurityFrm.ShowDialog(Me)
                    If (addSecurityFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Security Record added successfully!"
                    Else
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Adding of Security Record cancelled. Status: Ready."
                    End If
                    addSecurityFrm.Dispose()
                    Me.SecurityTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Security, thisEnquiryId)
                    Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
                    CalculateTotalLending()
                    CalculateTotalAssestValue()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub btnToolStripUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    FormRefresh()
    'End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking data."
        Application.DoEvents()
        Cursor.Current = Cursors.WaitCursor
        Try
            Dim progressStatus As Boolean = False
            Dim bindSrc2 As BindingSource = Me.EnquiryBindingSource
            Dim rowView2 As DataRowView
            'leave if bindingsource.current is nothing (no data)
            If Not bindSrc2.Current Is Nothing Then
                rowView2 = CType(bindSrc2.Current, DataRowView)
                'test if any fields have changed
                If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Saving changed data."
                    ''update WizardStatus
                    'Dim EnquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
                    'EnquiryRow = Me.EnquiryWorkSheetDataSet.Enquiry(0)
                    'EnquiryRow.BeginEdit()
                    'EnquiryRow.WizardStatus = 4
                    'save changes
                    Me.Validate()
                    bindSrc2.EndEdit()
                    Me.TableAdapterManager.EnquiryTableAdapter.Update(EnquiryWorkSheetDataSet.Enquiry)
                    'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                    progressStatus = True
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "Changes saved successfully."
                Else
                    'no changes to save - proceed
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Black
                    ToolStripStatusLabel1.Text = "No changes to save."
                    progressStatus = True
                End If
            Else
                'no current record in binding source - do not proceed
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No data entered, please enter data!"
                progressStatus = False
            End If

            If progressStatus = True Then
                'If ThisTypeOfClient = "Company" Then
                '    'launch CompanyStabilityForm and pass Enquiry Id
                '    Dim CompanyStabilityFrm As New CompanyStabilityForm
                '    CompanyStabilityFrm.PassVariable(ThisEnquiryId)
                '    CompanyStabilityFrm.Show()
                '    'Close this Form
                '    Me.Close()
                'Else
                '    'launch WkShtWizForm3 and pass Enquiry Id
                '    Dim WkShtWizFrm3 As New WkShtWizForm3
                '    WkShtWizFrm3.PassVariable(ThisEnquiryId)
                '    WkShtWizFrm3.Show()
                '    'Close this Form
                '    Me.Close()
                'End If
                ''launch WkShtWizForm3 and pass Enquiry Id
                'Dim WkShtWizFrm3 As New WkShtWizForm3
                'WkShtWizFrm3.PassVariable(ThisEnquiryId)
                'WkShtWizFrm3.Show()
                ''Close this Form
                'Me.Close()

                'update Status Strip
                ToolStripStatusLabel1.Text = "Launching next form."
                Application.DoEvents()

                Select Case thisTypeOfMainCustomer
                    Case MyEnums.mainCustomerType.Company, MyEnums.mainCustomerType.Trust, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
                        'launch CompanyFinancialForm
                        Dim companyFinancialFrm As New CompanyFinancialForm
                        companyFinancialFrm.PassVariable(thisEnquiryId)
                        companyFinancialFrm.Show()
                        'Close this Form
                        Me.Close()
                    Case Else
                        'launch WkShtWizForm4 (personal budget)
                        Dim wkShtWizFrm4 As New WkShtWizForm4
                        wkShtWizFrm4.PassVariable(thisEnquiryId)
                        wkShtWizFrm4.Show()
                        'Close this Form
                        Me.Close()
                End Select

            Else
                'MsgBox("No record to save, please enter information")
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = "No record to save, please enter information!"
            End If

            Cursor.Current = Cursors.Default

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnDeleteSecurity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSecurity.Click
        'Check a row is selected
        If SecurityDataGridView.Rows.Count > 0 Then
            'Get user confirmation
            Dim msg As String
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult
            msg = "Do you want to Delete Selected Record?"   ' Define message.
            style = MsgBoxStyle.DefaultButton2 Or _
               MsgBoxStyle.Question Or MsgBoxStyle.YesNo
            title = "Record Deletion"   ' Define title.
            ' Display message.
            response = MsgBox(msg, style, title)
            If response = MsgBoxResult.Yes Then   ' User choose Yes.
                Try
                    'Get Selected Row Index
                    Dim rowIndex As Integer = SecurityDataGridView.CurrentRow.Index
                    'mark row for deletion
                    SecurityDataGridView.Rows.RemoveAt(rowIndex)
                    'Commit the deletion and update dataset
                    SecurityTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Security)
                    'update Status Strip
                    ToolStripStatusLabel1.ForeColor = Color.Blue
                    ToolStripStatusLabel1.Text = "Security Record deleted successfully!"
                    'Recalculate Income and expenses
                    CalculateTotalLending()
                    CalculateTotalAssestValue()
                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try
            Else
                ' Perform some other action.
            End If
        Else
            'no row selected
            'MsgBox("No Row Selected")
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Red
            ToolStripStatusLabel1.Text = "Error: No Data Row selected! Please select a Data Row."
        End If


    End Sub

    Private Sub WkShtWizForm2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SuspendLayout()
        ''save user settings
        'My.Settings.PrelimFormSize = New Size(Me.ClientSize)
        'My.Settings.PrelimFormLocation = New Point(Me.Location)
        'My.Settings.Save()

        'check if data has changed
        Dim bindSrc1 As BindingSource = Me.EnquiryBindingSource
        Dim rowView1 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        If Not bindSrc1.Current Is Nothing Then
            rowView1 = CType(bindSrc1.Current, DataRowView)
            'test if any fields have changed
            If rowView1.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        bindSrc1.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If

        '******************** check user settings
        'check user location settings
        If Me.Location.X > Me.MaximumSize.Width Or Me.Location.X < 0 Then
            newLocX = 0
        Else
            newLocX = Me.Location.X
        End If
        If Me.Location.Y > Me.MaximumSize.Height Or Me.Location.Y < 0 Then
            newLocY = 0
        Else
            newLocY = Me.Location.Y
        End If
        'save user settings
        My.Settings.PrelimFormLocation = New Point(newLocX, newLocY)
        My.Settings.PrelimFormSize = Me.ClientSize
        My.Settings.Save()
        '******************** end of check user settings
    End Sub

    Private Sub WkShtWizForm2_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Dim formWidth As Integer = Me.Width
        Dim formHeight As Integer = Me.Height
        'Dim minFormWidth As Integer
        Dim minFormHeight As Integer = 610
        'MsgBox("formHeight = " & formHeight & vbCrLf & "formWidth = " & formWidth)
        'resize
        If formHeight > minFormHeight Then
            If formHeight >= 830 Then
                'lblTitleSecurity.Location = New Point(377 - (960 - formWidth) / 2, 5)
                gpbxListOfSecurities.Size = New Size(formWidth - 40, 315 - (1050 - formHeight) / 2)
                PanelList.Size = New Size(formWidth - 46, 299 - (1050 - formHeight) / 2)
                SecurityDataGridView.Size = New Size(formWidth - 46, 269 - (1050 - formHeight) / 2)
                PanelMiddle.Location = New Point(12, 417 - (1050 - formHeight) / 2)
                PanelMiddle.Size = New Size(formWidth - 40, 66)
                txtbxSecurityComments.Size = New Size(formWidth - 46, 476 - (1050 - formHeight) / 2)
                txtbxSecurityComments.Location = New Point(15, 481 - (1050 - formHeight) / 2)
            Else
                'lblTitleSecurity.Location = New Point(377 - (960 - formWidth) / 2, 5)
                gpbxListOfSecurities.Size = New Size(formWidth - 40, 205)
                SecurityDataGridView.Size = New Size(formWidth - 46, 159)
                PanelMiddle.Location = New Point(12, 307)
                PanelMiddle.Size = New Size(formWidth - 40, 66)
                txtbxSecurityComments.Location = New Point(15, 371)
                txtbxSecurityComments.Size = New Size(formWidth - 46, formHeight - 464)
            End If

        End If
    End Sub

    Private Sub btnSaveAndExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndExit.Click
        Try
            'save changes
            Cursor.Current = Cursors.WaitCursor
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
            'close form
            Me.Close()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Save and Exit: " + vbCrLf + ex.Message)
        End Try
    End Sub

    'Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
    '    'Check a row is selected
    '    If SecurityDataGridView.Rows.Count > 0 Then
    '        'Get user confirmation
    '        Dim msg As String
    '        Dim title As String
    '        Dim style As MsgBoxStyle
    '        Dim response As MsgBoxResult
    '        msg = "Do you want to Delete Selected Record?"   ' Define message.
    '        style = MsgBoxStyle.DefaultButton2 Or _
    '           MsgBoxStyle.Question Or MsgBoxStyle.YesNo
    '        title = "Record Deletion"   ' Define title.
    '        ' Display message.
    '        response = MsgBox(msg, style, title)
    '        If response = MsgBoxResult.Yes Then   ' User choose Yes.
    '            Try
    '                'Get Selected Row Index
    '                Dim rowIndex As Integer = SecurityDataGridView.CurrentRow.Index
    '                'mark row for deletion
    '                SecurityDataGridView.Rows.RemoveAt(rowIndex)
    '                'Commit the deletion and update dataset
    '                SecurityTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Security)
    '                'update Status Strip
    '                ToolStripStatusLabel1.ForeColor = Color.Blue
    '                ToolStripStatusLabel1.Text = "Security Record deleted successfully!"
    '                'Recalculate Income and expenses
    '                CalculateTotalLending()
    '            Catch ex As Exception
    '                MsgBox(ex.Message)
    '            End Try
    '        Else
    '            ' Perform some other action.
    '        End If
    '    Else
    '        'no row selected
    '        'update Status Strip
    '        ToolStripStatusLabel1.ForeColor = Color.Red
    '        ToolStripStatusLabel1.Text = "Error: No Data Row selected! Please select a Data Row."
    '    End If
    'End Sub

    '******************* Documents
    Private Sub btnDocs_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragDrop
        'Get String to WorkSheet Drive
        Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode & "\"
        Dim filePath As String
        Dim fileName As String
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                'get file paths of dropping files
                Dim filePaths() As String = e.Data.GetData(DataFormats.FileDrop)
                For Each fileLoc As String In filePaths
                    If File.Exists(fileLoc) Then
                        filePath = Path.GetFullPath(fileLoc)
                        fileName = Path.GetFileName(fileLoc)
                        'MsgBox("File path = " & filePath & vbCrLf & "Copy Path = " & folderPath & fileName)
                        'Now copy the file
                        My.Computer.FileSystem.CopyFile(filePath, folderPath & fileName)
                    End If
                Next
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Blue
                ToolStripStatusLabel1.Text = "Drag and drop successful.    Status: Ready"
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Drag and Drop caused an error:" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub btnDocs_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles btnDocs.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocs.Click
        Dim wkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = wkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Path to Shared Folder already exists!."
        Else
            Try
                'create directory
                IO.Directory.CreateDirectory(fname)
                'activate drag and drop
                Me.btnDocs.AllowDrop = True
                'refresh form
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "The directory was created successfully at {0}." + Directory.GetCreationTime(fname)
            Catch ex As System.IO.DirectoryNotFoundException
                ' Let the user know that the directory did not exist.
                ToolStripStatusLabel1.ForeColor = Color.Red
                ToolStripStatusLabel1.Text = ("Directory " + fname + " not found: " + ex.Message)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox(ex.Message)
            End Try

        End If
    End Sub
    '*************** End of Documents


    Private Sub btnQRGList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQRGList.Click
        Try
            'MsgBox("ThisQRGListId = " & ThisQRGListId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Quick reference Guide List."
            Dim qrgFrm As New QRGForm
            qrgFrm.PassVariable(thisEnquiryId, thisEnquiryCode, thisContactDisplayName)
            'AddSecurityFrm.ShowDialog(Me)
            If (qrgFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                '*****************************  get QRGList status
                Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                Dim connection2 As New SqlConnection(connectionString2)
                Dim selectStatement2 As String = "SELECT QRGStatus FROM dbo.QRGList WHERE EnquiryId = '" & thisEnquiryId & "'"
                'MsgBox("Select Statement = " & selectStatement)
                Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                Dim qrgListTempDataAdapter As New SqlDataAdapter(selectCommand2)
                Dim qrgListTempDataSet As New DataSet
                Dim qrgListTempDataTable As New DataTable
                Try
                    'dumps results into datatable LoginDataTable
                    qrgListTempDataAdapter.Fill(qrgListTempDataTable)
                    'if no matching rows .....
                    If qrgListTempDataTable.Rows.Count = 0 Then
                        MessageBox.Show("ERROR: No QRG List, please try again.")
                        'clear the dataTable and the Connect information
                        qrgListTempDataAdapter = Nothing
                        qrgListTempDataTable.Clear()
                        'if there is a matching row
                    ElseIf qrgListTempDataTable.Rows.Count = 1 Then
                        'get  QRGStatus value
                        Dim qrgListTempDataRow As DataRow = qrgListTempDataTable.Rows(0)
                        qrgStatus = qrgListTempDataRow.Item(0)
                        'MsgBox("QRG Status = " & QRGStatus)
                        'clear the dataTable and the Connect information
                        qrgListTempDataAdapter = Nothing
                        qrgListTempDataTable.Clear()
                    End If
                    'close the connection
                    If connection2.State <> ConnectionState.Closed Then
                        connection2.Close()
                    End If

                Catch ex As Exception
                    log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                    MsgBox(ex.Message)
                End Try

                'set QRGStatus button colour
                Select Case qrgStatus
                    Case 0
                        btnQRGList.BackColor = Color.Transparent
                    Case 1
                        btnQRGList.BackColor = Color.Tomato
                    Case 2
                        btnQRGList.BackColor = Color.LightGreen
                    Case Else
                        btnQRGList.BackColor = Color.Transparent
                End Select
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Quick reference Guide List updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Quick reference Guide List cancelled. Status: Ready."
            End If
            qrgFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnViewDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDocs.Click
        Dim wkshtStr As String = Switch.GetWorksheetSharedFolder
        Dim fname As String = wkshtStr & "\" & thisEnquiryCode
        'Determine whether the directory exists.
        If Not Directory.Exists(fname) Then
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Shared Folder does not exist!."
        Else
            Dim viewDocsFrm As New ViewDocsForm
            viewDocsFrm.PassVariable(thisEnquiryCode, thisContactDisplayName)
            viewDocsFrm.ShowDialog(Me)
            If viewDocsFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then
                viewDocsFrm.Dispose()
            Else
                viewDocsFrm.Dispose()
            End If
        End If
    End Sub

    '******************************* MenuStrip code
#Region "MenuStrip"

    Private Sub ClientDetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientDetailsToolStripMenuItem.Click
        Try
            Cursor.Current = Cursors.WaitCursor
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            'Me.ClientBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'AddSecurityFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                ''loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
                'Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
                ''get Dealer Id
                'ThisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
                ''*************** Get Dealer name
                'Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
                'Dim connection2 As New SqlConnection(connectionString2)
                'Dim selectStatement2 As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & ThisDealerId & "'"
                ''MsgBox("Select Statement = " & selectStatement1)
                'Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
                'Dim DealerTempDataAdapter As New SqlDataAdapter(selectCommand2)
                'Dim DealerTempDataSet As New DataSet
                'Dim DealerTempDataTable As New DataTable
                'Try
                '    'dumps results into datatable LoginDataTable
                '    DealerTempDataAdapter.Fill(DealerTempDataTable)
                '    'if no matching rows .....
                '    If DealerTempDataTable.Rows.Count = 0 Then
                '        MessageBox.Show("No Dealer Name, please try again.")
                '        'clear the dataTable and the Connect information
                '        DealerTempDataAdapter = Nothing
                '        DealerTempDataTable.Clear()
                '        'if there is a matching row
                '    ElseIf DealerTempDataTable.Rows.Count = 1 Then
                '        'get active value
                '        Dim DealerTempDataRow As DataRow = DealerTempDataTable.Rows(0)
                '        ThisDealerName = DealerTempDataRow.Item(0)
                '        'clear the dataTable and the Connect information
                '        DealerTempDataAdapter = Nothing
                '        DealerTempDataTable.Clear()
                '    End If
                '    'close the connection
                '    If connection2.State <> ConnectionState.Closed Then
                '        connection2.Close()
                '    End If

                'Catch ex As Exception
                '    MsgBox(ex.Message)
                'End Try
                ''MsgBox("Dealer Name = " & ThisDealerName)
                'lblDealerName.Text = ThisDealerName
                FormRefresh()

                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()
            'Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
            ''loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
            'Me.ClientTableAdapter.FillByClientId(Me.EnquiryWorkSheetDataSet.Client, ThisClientId)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()
    End Sub

    '************ Check Names
    Private Sub CheckFinPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckFinPowerToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."

        Try
            'Call DisplayFinPowerNamesForm
            Dim DisplayFinPowerNamesFrm As New DisplayFinPowerNamesForm
            DisplayFinPowerNamesFrm.PassVariable()
            DisplayFinPowerNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("SearchForLastNameLike ERROR:  " & vbCrLf & ex.Message)
        End Try


    End Sub


    Private Sub CheckEnquiriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiriesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Sub CheckEnquiryNamesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckEnquiryNamesToolStripMenuItem.Click
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Checking Names."
        Try
            'Call the Results form
            Dim DisplayAllNamesFrm As New DisplayAllNamesForm
            DisplayAllNamesFrm.PassVariable()
            DisplayAllNamesFrm.Show()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DisplayAllNamesForm ERROR:  " & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub DealersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DealersToolStripMenuItem.Click
        Try
            Dim DisplayDealersFrm As New DisplayDealersForm
            DisplayDealersFrm.PassVariable(thisDealerId)
            DisplayDealersFrm.ShowDialog()
            DisplayDealersFrm.Dispose()
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    '************ end of Check Names

    Private Sub UpdateAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateAllToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Validate()
            'Me.ClientBindingSource.EndEdit()
            Me.EnquiryBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        FormRefresh()
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        FormRefresh()
    End Sub

    Private Sub AddCommentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EditCandDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCandDToolStripMenuItem.Click
       
        ''save changes
        'Me.Validate()
        'Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        ''update Status Strip
        'ToolStripStatusLabel1.ForeColor = Color.Blue
        'ToolStripStatusLabel1.Text = "Editing Enquiry Record."
        'Dim EditDetailsFrm As New EditDetailsForm
        'EditDetailsFrm.PassVariable(ThisEnquiryId)
        ''AddSecurityFrm.ShowDialog(Me)
        'If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
        '    'loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
        '    Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
        '    'get Dealer Id
        '    ThisDealerId = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource.Position()).Item("DealerId")
        '    '*************** Get Dealer name
        '    Dim connectionString2 As String = My.Settings.EnquiryWorkSheetConnectionString
        '    Dim connection2 As New SqlConnection(connectionString2)
        '    Dim selectStatement2 As String = "SELECT Name FROM dbo.ActiveDealers WHERE DealerId = '" & ThisDealerId & "'"
        '    'MsgBox("Select Statement = " & selectStatement1)
        '    Dim selectCommand2 As New SqlCommand(selectStatement2, connection2)
        '    Dim DealerTempDataAdapter As New SqlDataAdapter(selectCommand2)
        '    Dim DealerTempDataSet As New DataSet
        '    Dim DealerTempDataTable As New DataTable
        '    Try
        '        'dumps results into datatable LoginDataTable
        '        DealerTempDataAdapter.Fill(DealerTempDataTable)
        '        'if no matching rows .....
        '        If DealerTempDataTable.Rows.Count = 0 Then
        '            MessageBox.Show("No Dealer Name, please try again.")
        '            'clear the dataTable and the Connect information
        '            DealerTempDataAdapter = Nothing
        '            DealerTempDataTable.Clear()
        '            'if there is a matching row
        '        ElseIf DealerTempDataTable.Rows.Count = 1 Then
        '            'get active value
        '            Dim DealerTempDataRow As DataRow = DealerTempDataTable.Rows(0)
        '            ThisDealerName = DealerTempDataRow.Item(0)
        '            'clear the dataTable and the Connect information
        '            DealerTempDataAdapter = Nothing
        '            DealerTempDataTable.Clear()
        '        End If
        '        'close the connection
        '        If connection2.State <> ConnectionState.Closed Then
        '            connection2.Close()
        '        End If

        '    Catch ex As Exception
        '        MsgBox(ex.Message)
        '    End Try
        '    'MsgBox("Dealer Name = " & ThisDealerName)
        '    lblDealerName.Text = ThisDealerName

        '    'update Status Strip
        '    ToolStripStatusLabel1.ForeColor = Color.Black
        '    ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
        'Else
        '    'update Status Strip
        '    ToolStripStatusLabel1.ForeColor = Color.Black
        '    ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
        'End If
        'EditDetailsFrm.Dispose()
        'Me.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
        ''loads data into the 'EnquiryWorkSheetDataSet.Enquiry' table for this Enquiry Id
        'Me.ClientTableAdapter.FillByClientId(Me.EnquiryWorkSheetDataSet.Client, ThisClientId)

        Cursor.Current = Cursors.WaitCursor
        Try
            'save changes
            Me.Validate()
            Me.EnquiryBindingSource.EndEdit()
            Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("ERROR on Saving data: " + vbCrLf + ex.Message)
        End Try
        Try
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Blue
            ToolStripStatusLabel1.Text = "Editing Enquiry Record."
            Dim EditDetailsFrm As New EditDetailsForm
            EditDetailsFrm.PassVariable(thisEnquiryId)
            'EditDetailsFrm.ShowDialog(Me)
            If (EditDetailsFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                FormRefresh()
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Enquiry Record updated successfully!"
            Else
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Updating of Enquiry Record cancelled. Status: Ready."
            End If
            EditDetailsFrm.Dispose()

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub AddCommentToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCommentToolStripMenuItem1.Click
        'Save current changed data
        Me.Validate()
        EnquiryBindingSource.EndEdit()
        Me.EnquiryTableAdapter.Update(Me.EnquiryWorkSheetDataSet.Enquiry)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Blue
        ToolStripStatusLabel1.Text = "Adding comment"
        'launch Add Comment Form
        Dim AddCommentFrm As New AddCommentForm()
        AddCommentFrm.PassVariable(thisEnquiryId)
        ' Show AddCommentFrm as a modal dialog and determine if DialogResult = OK.
        If AddCommentFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'Call the function you used to populate the data grid..
            Me.TableAdapterManager.EnquiryTableAdapter.FillByEnquiryId(Me.EnquiryWorkSheetDataSet.Enquiry, thisEnquiryId)
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment added."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Comment cancelled."
        End If
        'When Add Comment Form closed
        AddCommentFrm.Dispose()
    End Sub

    Private Sub EmailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        ' Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailFrm As New EmailForm
        EmailFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailFrm.Dispose()

    End Sub

    Private Sub ApprovalFormToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApprovalFormToolStripMenuItem1.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching Approval Decline Pending Sheet."
        Try
            System.Diagnostics.Process.Start(My.Settings.ApprovalDeclinePendingSheet)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error Launching Approval Decline Pending Sheet" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub EmailApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmailApprovalToolStripMenuItem.Click
        'save changes
        Cursor.Current = Cursors.WaitCursor
        Me.Validate()
        Me.EnquiryBindingSource.EndEdit()
        'Me.ClientBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
        Cursor.Current = Cursors.Default

        Dim EmailAppFrm As New EmailApprovalForm
        EmailAppFrm.PassVariable(thisEnquiryId, thisDealerId)
        'Show EmailFrm as a modal dialog and determine if DialogResult = OK.
        If EmailAppFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
            'refresh comment box
            FormRefresh()
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Email sent. Status: Ready."
        Else
            'update Status Strip
            ToolStripStatusLabel1.ForeColor = Color.Black
            ToolStripStatusLabel1.Text = "Sending of email cancelled. Status: Ready."
        End If
        EmailAppFrm.Dispose()
    End Sub

    Private Sub WWWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WWWToolStripMenuItem.Click
        Cursor.Current = Cursors.WaitCursor
        'launch internet browser for web application
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Launching web browser."
        System.Diagnostics.Process.Start(My.Settings.YFL_website)
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = Color.Black
        ToolStripStatusLabel1.Text = "Status: Ready."
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub DeclineToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeclineToolStripMenuItem.Click
        thisEnquiryResult = "Declined"
        progressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        'If Not EnquiryBindingSource.Current Is Nothing And Not ClientBindingSource.Current Is Nothing Then
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                DeclinedFrm.Dispose()

                progressStatus = True

            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Declining Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub WithdrawToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WithdrawToolStripMenuItem.Click
        thisEnquiryResult = "Withdrawn"
        progressStatus = False
        Cursor.Current = Cursors.WaitCursor
        'check if data has changed
        'Dim rowView1 As DataRowView
        Dim rowView2 As DataRowView
        'leave if bindingsource.current is nothing (no data)
        'If Not EnquiryBindingSource.Current Is Nothing And Not ClientBindingSource.Current Is Nothing Then
        If Not EnquiryBindingSource.Current Is Nothing Then
            'rowView1 = CType(ClientBindingSource.Current, DataRowView)
            rowView2 = CType(EnquiryBindingSource.Current, DataRowView)
            'test if any fields have changed
            'If rowView1.Row.HasVersion(DataRowVersion.Proposed) Or rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
            If rowView2.Row.HasVersion(DataRowVersion.Proposed) Then
                'Get user confirmation
                Dim msg As String
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                msg = "Do you want to Save Changed Data?"   ' Define message.
                style = MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question Or MsgBoxStyle.YesNo
                title = "Save Record?"   ' Define title.
                ' Display message.
                response = MsgBox(msg, style, title)
                If response = MsgBoxResult.Yes Then   ' User choose Yes.
                    Try
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Saving changed data."
                        'save changes
                        Me.Validate()
                        'ClientBindingSource.EndEdit()
                        EnquiryBindingSource.EndEdit()
                        Me.TableAdapterManager.UpdateAll(Me.EnquiryWorkSheetDataSet)
                        'update Status Strip
                        ToolStripStatusLabel1.ForeColor = Color.Black
                        ToolStripStatusLabel1.Text = "Changes saved successfully."
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    ' User choose No. Do nothing
                End If
                'no changes to save. Do nothing
            End If
            'no data entered - proceed
        End If
        Cursor.Current = Cursors.Default

        'now decline form
        Try
            Dim DeclinedFrm As New DeclinedForm
            DeclinedFrm.PassVariable(thisEnquiryId, thisContactDisplayName, thisEnquiryResult)
            DeclinedFrm.ShowDialog(Me)
            If DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.OK Then

                DeclinedFrm.Dispose()

                progressStatus = True

            ElseIf DeclinedFrm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                DeclinedFrm.Dispose()
                'User choose Cancel, do nothing
                progressStatus = False
                'update Status Strip
                ToolStripStatusLabel1.ForeColor = Color.Black
                ToolStripStatusLabel1.Text = "Withdrawing of Enquiry cancelled. Status: Ready."
            End If
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("DeclinedForm caused an error:" & vbCrLf & ex.Message)
        End Try

        'Progress Status
        If progressStatus = True Then
            Try
                Cursor.Current = Cursors.WaitCursor
                'launch  Standard Worksheet Form
                Dim AppFrm As New AppForm
                AppFrm.PassVariable(thisEnquiryId, False)
                AppFrm.Show()
                Cursor.Current = Cursors.Default
                'Close this Form
                Me.Close()
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MsgBox("AppForm caused an error:" & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub MCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MCToolStripMenuItem.Click
        Dim MembersCentreUtilObject As New WebUtil
        MembersCentreUtilObject.MCLink()
        'update Status Strip
        ToolStripStatusLabel1.ForeColor = MembersCentreUtilObject.GetLabelForeColor
        ToolStripStatusLabel1.Text = MembersCentreUtilObject.GetLabelText
    End Sub

#End Region
    '******************************* end of MenuStrip code

   
   
 
   
    
    
    
    
   
   
End Class