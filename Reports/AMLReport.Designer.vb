﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AMLReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AMLReport))
        Me.gpbxDSAmlRiskSum = New System.Windows.Forms.GroupBox()
        Me.lvDSAmlRiskSum = New System.Windows.Forms.ListView()
        Me.btnPdf = New System.Windows.Forms.Button()
        Me.gpbxCustPaidOut = New System.Windows.Forms.GroupBox()
        Me.lblPepBeneOwners = New System.Windows.Forms.Label()
        Me.lblPep = New System.Windows.Forms.Label()
        Me.lblTotalCust = New System.Windows.Forms.Label()
        Me.gpbxClassification = New System.Windows.Forms.GroupBox()
        Me.pnlClassificationSum = New System.Windows.Forms.Panel()
        Me.dgvClassificationSum = New System.Windows.Forms.DataGridView()
        Me.gpbxCustType = New System.Windows.Forms.GroupBox()
        Me.pnlCustType = New System.Windows.Forms.Panel()
        Me.dgvCustTypeSum = New System.Windows.Forms.DataGridView()
        Me.gpbxCustRiskSum = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvAmlRiskSummary = New System.Windows.Forms.DataGridView()
        Me.btnRunReport = New System.Windows.Forms.Button()
        Me.gpbxAcceptMethCust = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvAcceptMethCust = New System.Windows.Forms.DataGridView()
        Me.gpbxAcceptMethClassSum = New System.Windows.Forms.GroupBox()
        Me.lvAcceptMethClassSum = New System.Windows.Forms.ListView()
        Me.gpbxAcceptanceMethodSummary = New System.Windows.Forms.GroupBox()
        Me.pnlAcceptMethSum = New System.Windows.Forms.Panel()
        Me.dgvAcceptanceMethodSummary = New System.Windows.Forms.DataGridView()
        Me.gpbxDateSelect = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.gpbxPayMethod = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.dgvPayMethods = New System.Windows.Forms.DataGridView()
        Me.gpbxDSAmlRiskSum.SuspendLayout()
        Me.gpbxCustPaidOut.SuspendLayout()
        Me.gpbxClassification.SuspendLayout()
        Me.pnlClassificationSum.SuspendLayout()
        CType(Me.dgvClassificationSum, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxCustType.SuspendLayout()
        Me.pnlCustType.SuspendLayout()
        CType(Me.dgvCustTypeSum, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxCustRiskSum.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvAmlRiskSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxAcceptMethCust.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvAcceptMethCust, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxAcceptMethClassSum.SuspendLayout()
        Me.gpbxAcceptanceMethodSummary.SuspendLayout()
        Me.pnlAcceptMethSum.SuspendLayout()
        CType(Me.dgvAcceptanceMethodSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxDateSelect.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gpbxPayMethod.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvPayMethods, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gpbxDSAmlRiskSum
        '
        Me.gpbxDSAmlRiskSum.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxDSAmlRiskSum.Controls.Add(Me.lvDSAmlRiskSum)
        Me.gpbxDSAmlRiskSum.Location = New System.Drawing.Point(586, 112)
        Me.gpbxDSAmlRiskSum.Name = "gpbxDSAmlRiskSum"
        Me.gpbxDSAmlRiskSum.Size = New System.Drawing.Size(287, 377)
        Me.gpbxDSAmlRiskSum.TabIndex = 24
        Me.gpbxDSAmlRiskSum.TabStop = False
        Me.gpbxDSAmlRiskSum.Text = "Director/Shareholder AML Risk Summary"
        '
        'lvDSAmlRiskSum
        '
        Me.lvDSAmlRiskSum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDSAmlRiskSum.Location = New System.Drawing.Point(3, 16)
        Me.lvDSAmlRiskSum.Name = "lvDSAmlRiskSum"
        Me.lvDSAmlRiskSum.Size = New System.Drawing.Size(281, 358)
        Me.lvDSAmlRiskSum.TabIndex = 0
        Me.lvDSAmlRiskSum.UseCompatibleStateImageBehavior = False
        '
        'btnPdf
        '
        Me.btnPdf.Location = New System.Drawing.Point(529, 81)
        Me.btnPdf.Name = "btnPdf"
        Me.btnPdf.Size = New System.Drawing.Size(265, 23)
        Me.btnPdf.TabIndex = 23
        Me.btnPdf.Text = "Create PDF"
        Me.btnPdf.UseVisualStyleBackColor = True
        '
        'gpbxCustPaidOut
        '
        Me.gpbxCustPaidOut.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCustPaidOut.Controls.Add(Me.lblPepBeneOwners)
        Me.gpbxCustPaidOut.Controls.Add(Me.lblPep)
        Me.gpbxCustPaidOut.Controls.Add(Me.lblTotalCust)
        Me.gpbxCustPaidOut.Cursor = System.Windows.Forms.Cursors.Default
        Me.gpbxCustPaidOut.Location = New System.Drawing.Point(12, 112)
        Me.gpbxCustPaidOut.Name = "gpbxCustPaidOut"
        Me.gpbxCustPaidOut.Size = New System.Drawing.Size(275, 79)
        Me.gpbxCustPaidOut.TabIndex = 22
        Me.gpbxCustPaidOut.TabStop = False
        Me.gpbxCustPaidOut.Text = "Loans drawn down"
        '
        'lblPepBeneOwners
        '
        Me.lblPepBeneOwners.AutoSize = True
        Me.lblPepBeneOwners.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblPepBeneOwners.Location = New System.Drawing.Point(6, 58)
        Me.lblPepBeneOwners.Name = "lblPepBeneOwners"
        Me.lblPepBeneOwners.Size = New System.Drawing.Size(246, 13)
        Me.lblPepBeneOwners.TabIndex = 8
        Me.lblPepBeneOwners.Text = "Number of Politically Exposed Beneficial Owners = "
        '
        'lblPep
        '
        Me.lblPep.AutoSize = True
        Me.lblPep.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.lblPep.Location = New System.Drawing.Point(6, 36)
        Me.lblPep.Name = "lblPep"
        Me.lblPep.Size = New System.Drawing.Size(199, 13)
        Me.lblPep.TabIndex = 0
        Me.lblPep.Text = "Number of Politically Exposed Persons = "
        Me.lblPep.UseWaitCursor = True
        '
        'lblTotalCust
        '
        Me.lblTotalCust.AutoSize = True
        Me.lblTotalCust.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTotalCust.Location = New System.Drawing.Point(6, 16)
        Me.lblTotalCust.Name = "lblTotalCust"
        Me.lblTotalCust.Size = New System.Drawing.Size(142, 13)
        Me.lblTotalCust.TabIndex = 7
        Me.lblTotalCust.Text = "Total customers (paid out) is "
        '
        'gpbxClassification
        '
        Me.gpbxClassification.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxClassification.Controls.Add(Me.pnlClassificationSum)
        Me.gpbxClassification.Location = New System.Drawing.Point(267, 495)
        Me.gpbxClassification.Name = "gpbxClassification"
        Me.gpbxClassification.Size = New System.Drawing.Size(348, 143)
        Me.gpbxClassification.TabIndex = 21
        Me.gpbxClassification.TabStop = False
        Me.gpbxClassification.Text = "Loan Classification Summary"
        '
        'pnlClassificationSum
        '
        Me.pnlClassificationSum.Controls.Add(Me.dgvClassificationSum)
        Me.pnlClassificationSum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlClassificationSum.Location = New System.Drawing.Point(3, 16)
        Me.pnlClassificationSum.Name = "pnlClassificationSum"
        Me.pnlClassificationSum.Size = New System.Drawing.Size(342, 124)
        Me.pnlClassificationSum.TabIndex = 0
        '
        'dgvClassificationSum
        '
        Me.dgvClassificationSum.AllowUserToAddRows = False
        Me.dgvClassificationSum.AllowUserToDeleteRows = False
        Me.dgvClassificationSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClassificationSum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvClassificationSum.Location = New System.Drawing.Point(0, 0)
        Me.dgvClassificationSum.MultiSelect = False
        Me.dgvClassificationSum.Name = "dgvClassificationSum"
        Me.dgvClassificationSum.ReadOnly = True
        Me.dgvClassificationSum.RowHeadersVisible = False
        Me.dgvClassificationSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassificationSum.Size = New System.Drawing.Size(342, 124)
        Me.dgvClassificationSum.TabIndex = 0
        '
        'gpbxCustType
        '
        Me.gpbxCustType.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCustType.Controls.Add(Me.pnlCustType)
        Me.gpbxCustType.Location = New System.Drawing.Point(12, 495)
        Me.gpbxCustType.Name = "gpbxCustType"
        Me.gpbxCustType.Size = New System.Drawing.Size(252, 143)
        Me.gpbxCustType.TabIndex = 20
        Me.gpbxCustType.TabStop = False
        Me.gpbxCustType.Text = "Customer Type Summary"
        '
        'pnlCustType
        '
        Me.pnlCustType.Controls.Add(Me.dgvCustTypeSum)
        Me.pnlCustType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCustType.Location = New System.Drawing.Point(3, 16)
        Me.pnlCustType.Name = "pnlCustType"
        Me.pnlCustType.Size = New System.Drawing.Size(246, 124)
        Me.pnlCustType.TabIndex = 0
        '
        'dgvCustTypeSum
        '
        Me.dgvCustTypeSum.AllowUserToAddRows = False
        Me.dgvCustTypeSum.AllowUserToDeleteRows = False
        Me.dgvCustTypeSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCustTypeSum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCustTypeSum.Location = New System.Drawing.Point(0, 0)
        Me.dgvCustTypeSum.MultiSelect = False
        Me.dgvCustTypeSum.Name = "dgvCustTypeSum"
        Me.dgvCustTypeSum.ReadOnly = True
        Me.dgvCustTypeSum.RowHeadersVisible = False
        Me.dgvCustTypeSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCustTypeSum.Size = New System.Drawing.Size(246, 124)
        Me.dgvCustTypeSum.TabIndex = 0
        '
        'gpbxCustRiskSum
        '
        Me.gpbxCustRiskSum.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxCustRiskSum.Controls.Add(Me.Panel2)
        Me.gpbxCustRiskSum.Location = New System.Drawing.Point(12, 197)
        Me.gpbxCustRiskSum.Name = "gpbxCustRiskSum"
        Me.gpbxCustRiskSum.Size = New System.Drawing.Size(275, 143)
        Me.gpbxCustRiskSum.TabIndex = 19
        Me.gpbxCustRiskSum.TabStop = False
        Me.gpbxCustRiskSum.Text = "Customer AML Risk Summary"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvAmlRiskSummary)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 16)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(269, 124)
        Me.Panel2.TabIndex = 1
        '
        'dgvAmlRiskSummary
        '
        Me.dgvAmlRiskSummary.AllowUserToAddRows = False
        Me.dgvAmlRiskSummary.AllowUserToDeleteRows = False
        Me.dgvAmlRiskSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmlRiskSummary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAmlRiskSummary.Location = New System.Drawing.Point(0, 0)
        Me.dgvAmlRiskSummary.MultiSelect = False
        Me.dgvAmlRiskSummary.Name = "dgvAmlRiskSummary"
        Me.dgvAmlRiskSummary.ReadOnly = True
        Me.dgvAmlRiskSummary.RowHeadersVisible = False
        Me.dgvAmlRiskSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmlRiskSummary.Size = New System.Drawing.Size(269, 124)
        Me.dgvAmlRiskSummary.TabIndex = 0
        '
        'btnRunReport
        '
        Me.btnRunReport.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRunReport.BackColor = System.Drawing.Color.YellowGreen
        Me.btnRunReport.Location = New System.Drawing.Point(103, 81)
        Me.btnRunReport.Name = "btnRunReport"
        Me.btnRunReport.Size = New System.Drawing.Size(265, 23)
        Me.btnRunReport.TabIndex = 17
        Me.btnRunReport.Text = "Run report"
        Me.btnRunReport.UseVisualStyleBackColor = False
        '
        'gpbxAcceptMethCust
        '
        Me.gpbxAcceptMethCust.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxAcceptMethCust.Controls.Add(Me.Panel1)
        Me.gpbxAcceptMethCust.Location = New System.Drawing.Point(12, 644)
        Me.gpbxAcceptMethCust.Name = "gpbxAcceptMethCust"
        Me.gpbxAcceptMethCust.Size = New System.Drawing.Size(861, 358)
        Me.gpbxAcceptMethCust.TabIndex = 18
        Me.gpbxAcceptMethCust.TabStop = False
        Me.gpbxAcceptMethCust.Text = "Customers: Select line from summary above and display associated enquiries"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvAcceptMethCust)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(855, 339)
        Me.Panel1.TabIndex = 0
        '
        'dgvAcceptMethCust
        '
        Me.dgvAcceptMethCust.AllowUserToAddRows = False
        Me.dgvAcceptMethCust.AllowUserToDeleteRows = False
        Me.dgvAcceptMethCust.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAcceptMethCust.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAcceptMethCust.Location = New System.Drawing.Point(0, 0)
        Me.dgvAcceptMethCust.MultiSelect = False
        Me.dgvAcceptMethCust.Name = "dgvAcceptMethCust"
        Me.dgvAcceptMethCust.ReadOnly = True
        Me.dgvAcceptMethCust.RowHeadersVisible = False
        Me.dgvAcceptMethCust.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAcceptMethCust.Size = New System.Drawing.Size(855, 339)
        Me.dgvAcceptMethCust.TabIndex = 0
        '
        'gpbxAcceptMethClassSum
        '
        Me.gpbxAcceptMethClassSum.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxAcceptMethClassSum.Controls.Add(Me.lvAcceptMethClassSum)
        Me.gpbxAcceptMethClassSum.Location = New System.Drawing.Point(293, 112)
        Me.gpbxAcceptMethClassSum.Name = "gpbxAcceptMethClassSum"
        Me.gpbxAcceptMethClassSum.Size = New System.Drawing.Size(287, 377)
        Me.gpbxAcceptMethClassSum.TabIndex = 16
        Me.gpbxAcceptMethClassSum.TabStop = False
        Me.gpbxAcceptMethClassSum.Text = "Customer Acceptance Method Classification Summary"
        '
        'lvAcceptMethClassSum
        '
        Me.lvAcceptMethClassSum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAcceptMethClassSum.Location = New System.Drawing.Point(3, 16)
        Me.lvAcceptMethClassSum.Name = "lvAcceptMethClassSum"
        Me.lvAcceptMethClassSum.Size = New System.Drawing.Size(281, 358)
        Me.lvAcceptMethClassSum.TabIndex = 0
        Me.lvAcceptMethClassSum.UseCompatibleStateImageBehavior = False
        '
        'gpbxAcceptanceMethodSummary
        '
        Me.gpbxAcceptanceMethodSummary.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxAcceptanceMethodSummary.Controls.Add(Me.pnlAcceptMethSum)
        Me.gpbxAcceptanceMethodSummary.Location = New System.Drawing.Point(12, 346)
        Me.gpbxAcceptanceMethodSummary.Name = "gpbxAcceptanceMethodSummary"
        Me.gpbxAcceptanceMethodSummary.Size = New System.Drawing.Size(275, 143)
        Me.gpbxAcceptanceMethodSummary.TabIndex = 15
        Me.gpbxAcceptanceMethodSummary.TabStop = False
        Me.gpbxAcceptanceMethodSummary.Text = "Customer Acceptance Method Summary"
        '
        'pnlAcceptMethSum
        '
        Me.pnlAcceptMethSum.Controls.Add(Me.dgvAcceptanceMethodSummary)
        Me.pnlAcceptMethSum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAcceptMethSum.Location = New System.Drawing.Point(3, 16)
        Me.pnlAcceptMethSum.Name = "pnlAcceptMethSum"
        Me.pnlAcceptMethSum.Size = New System.Drawing.Size(269, 124)
        Me.pnlAcceptMethSum.TabIndex = 0
        '
        'dgvAcceptanceMethodSummary
        '
        Me.dgvAcceptanceMethodSummary.AllowUserToAddRows = False
        Me.dgvAcceptanceMethodSummary.AllowUserToDeleteRows = False
        Me.dgvAcceptanceMethodSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAcceptanceMethodSummary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAcceptanceMethodSummary.Location = New System.Drawing.Point(0, 0)
        Me.dgvAcceptanceMethodSummary.MultiSelect = False
        Me.dgvAcceptanceMethodSummary.Name = "dgvAcceptanceMethodSummary"
        Me.dgvAcceptanceMethodSummary.ReadOnly = True
        Me.dgvAcceptanceMethodSummary.RowHeadersVisible = False
        Me.dgvAcceptanceMethodSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAcceptanceMethodSummary.Size = New System.Drawing.Size(269, 124)
        Me.dgvAcceptanceMethodSummary.TabIndex = 0
        '
        'gpbxDateSelect
        '
        Me.gpbxDateSelect.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxDateSelect.Controls.Add(Me.TableLayoutPanel1)
        Me.gpbxDateSelect.Location = New System.Drawing.Point(12, 27)
        Me.gpbxDateSelect.Name = "gpbxDateSelect"
        Me.gpbxDateSelect.Size = New System.Drawing.Size(861, 48)
        Me.gpbxDateSelect.TabIndex = 14
        Me.gpbxDateSelect.TabStop = False
        Me.gpbxDateSelect.Text = "Select date range"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 6
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.32997!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33501!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33501!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 166.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpStartDate, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpEndDate, 3, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(855, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(36, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Start date"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.dtpStartDate.Location = New System.Drawing.Point(95, 4)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(199, 20)
        Me.dtpStartDate.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(338, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "End start"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.dtpEndDate.Location = New System.Drawing.Point(393, 4)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(199, 20)
        Me.dtpEndDate.TabIndex = 3
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(376, 11)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(103, 13)
        Me.lblTitle.TabIndex = 13
        Me.lblTitle.Text = "AML/CFT Report"
        '
        'gpbxPayMethod
        '
        Me.gpbxPayMethod.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gpbxPayMethod.Controls.Add(Me.Panel3)
        Me.gpbxPayMethod.Location = New System.Drawing.Point(621, 495)
        Me.gpbxPayMethod.Name = "gpbxPayMethod"
        Me.gpbxPayMethod.Size = New System.Drawing.Size(252, 143)
        Me.gpbxPayMethod.TabIndex = 25
        Me.gpbxPayMethod.TabStop = False
        Me.gpbxPayMethod.Text = "Loan Payment Methods"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvPayMethods)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 16)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(246, 124)
        Me.Panel3.TabIndex = 0
        '
        'dgvPayMethods
        '
        Me.dgvPayMethods.AllowUserToAddRows = False
        Me.dgvPayMethods.AllowUserToDeleteRows = False
        Me.dgvPayMethods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayMethods.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPayMethods.Location = New System.Drawing.Point(0, 0)
        Me.dgvPayMethods.MultiSelect = False
        Me.dgvPayMethods.Name = "dgvPayMethods"
        Me.dgvPayMethods.ReadOnly = True
        Me.dgvPayMethods.RowHeadersVisible = False
        Me.dgvPayMethods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayMethods.Size = New System.Drawing.Size(246, 124)
        Me.dgvPayMethods.TabIndex = 0
        '
        'AMLReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(885, 1012)
        Me.Controls.Add(Me.gpbxPayMethod)
        Me.Controls.Add(Me.gpbxDSAmlRiskSum)
        Me.Controls.Add(Me.btnPdf)
        Me.Controls.Add(Me.gpbxCustPaidOut)
        Me.Controls.Add(Me.gpbxClassification)
        Me.Controls.Add(Me.gpbxCustType)
        Me.Controls.Add(Me.gpbxCustRiskSum)
        Me.Controls.Add(Me.btnRunReport)
        Me.Controls.Add(Me.gpbxAcceptMethCust)
        Me.Controls.Add(Me.gpbxAcceptMethClassSum)
        Me.Controls.Add(Me.gpbxAcceptanceMethodSummary)
        Me.Controls.Add(Me.gpbxDateSelect)
        Me.Controls.Add(Me.lblTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(901, 755)
        Me.Name = "AMLReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "AMLReport"
        Me.gpbxDSAmlRiskSum.ResumeLayout(False)
        Me.gpbxCustPaidOut.ResumeLayout(False)
        Me.gpbxCustPaidOut.PerformLayout()
        Me.gpbxClassification.ResumeLayout(False)
        Me.pnlClassificationSum.ResumeLayout(False)
        CType(Me.dgvClassificationSum, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxCustType.ResumeLayout(False)
        Me.pnlCustType.ResumeLayout(False)
        CType(Me.dgvCustTypeSum, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxCustRiskSum.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvAmlRiskSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxAcceptMethCust.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvAcceptMethCust, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxAcceptMethClassSum.ResumeLayout(False)
        Me.gpbxAcceptanceMethodSummary.ResumeLayout(False)
        Me.pnlAcceptMethSum.ResumeLayout(False)
        CType(Me.dgvAcceptanceMethodSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxDateSelect.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.gpbxPayMethod.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.dgvPayMethods, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gpbxDSAmlRiskSum As System.Windows.Forms.GroupBox
    Friend WithEvents lvDSAmlRiskSum As System.Windows.Forms.ListView
    Friend WithEvents btnPdf As System.Windows.Forms.Button
    Friend WithEvents gpbxCustPaidOut As System.Windows.Forms.GroupBox
    Friend WithEvents lblPepBeneOwners As System.Windows.Forms.Label
    Friend WithEvents lblPep As System.Windows.Forms.Label
    Friend WithEvents lblTotalCust As System.Windows.Forms.Label
    Friend WithEvents gpbxClassification As System.Windows.Forms.GroupBox
    Friend WithEvents pnlClassificationSum As System.Windows.Forms.Panel
    Friend WithEvents dgvClassificationSum As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxCustType As System.Windows.Forms.GroupBox
    Friend WithEvents pnlCustType As System.Windows.Forms.Panel
    Friend WithEvents dgvCustTypeSum As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxCustRiskSum As System.Windows.Forms.GroupBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvAmlRiskSummary As System.Windows.Forms.DataGridView
    Friend WithEvents btnRunReport As System.Windows.Forms.Button
    Friend WithEvents gpbxAcceptMethCust As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvAcceptMethCust As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxAcceptMethClassSum As System.Windows.Forms.GroupBox
    Friend WithEvents lvAcceptMethClassSum As System.Windows.Forms.ListView
    Friend WithEvents gpbxAcceptanceMethodSummary As System.Windows.Forms.GroupBox
    Friend WithEvents pnlAcceptMethSum As System.Windows.Forms.Panel
    Friend WithEvents dgvAcceptanceMethodSummary As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxDateSelect As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents gpbxPayMethod As System.Windows.Forms.GroupBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgvPayMethods As System.Windows.Forms.DataGridView
End Class
