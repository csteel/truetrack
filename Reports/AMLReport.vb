﻿
Imports System.Data.SqlClient
Imports System.IO
'Imports MigraDoc.DocumentObjectModel
'Imports MigraDoc.Rendering
'Imports PdfSharp.Pdf

Public Class AMLReport

    Dim sumTable As DataTable 'Acceptance Method Summary
    Dim amlSumTable As DataTable 'AML Risk Summary
    Dim dtsbAmlSumTable As DataTable 'Directors & Shareholders AML Risk Summary
    Dim amcSumTable As DataTable 'Acceptance Method Classification Summary
    Dim custTypeSumTable As DataTable 'Customer Type Summary
    Dim cust As DataTable 'Customer
    Dim loanClassSumTable As DataTable 'Loan Classification Summary
    Dim payMethodSumTable As DataTable 'Loan payout payment method Summary
    Dim startdate As Date
    Dim enddate As Date
    Dim custTotal As Integer = 0 'Total customers (paid out)
    Dim pepTotal As Integer 'Number of Politically Exposed Persons
    Dim pepBeneficalOwnerSum As Integer 'Number of Politically Exposed Beneficial Owners
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)



    Private Sub AmlReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        InitializeAcceptMethClassSumListView()
        InitializeDSAmlRiskSumListView()

    End Sub

    ''' <summary>
    ''' Get number of each Acceptance Method by date
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetAcceptanceMethodSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'CAM.Description, COUNT(C.CustomerId) AS Total, CAM.Enum
        '========================
        Dim acceptMethSumTa As ReportDSTableAdapters.AcceptanceMethodSummaryTableAdapter
        acceptMethSumTa = New ReportDSTableAdapters.AcceptanceMethodSummaryTableAdapter
        Dim acceptMethSumDt As ReportDS.AcceptanceMethodSummaryDataTable
        acceptMethSumDt = acceptMethSumTa.GetData(sDate, eDate)
        Return acceptMethSumDt

    End Function

    ''' <summary>
    ''' Get number of each Classification type grouped by Acceptance Method
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetAcceptMethClassSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'CAM.Description, COUNT(C.CustomerId) AS Total, CAM.Enum
        '========================
        Dim acceptMethClassSumTa As ReportDSTableAdapters.AcceptanceMethodClassificationSummaryTableAdapter
        acceptMethClassSumTa = New ReportDSTableAdapters.AcceptanceMethodClassificationSummaryTableAdapter
        Dim acceptMethClassSumDt As ReportDS.AcceptanceMethodClassificationSummaryDataTable
        acceptMethClassSumDt = acceptMethClassSumTa.GetData(sDate, eDate)
        Return acceptMethClassSumDt

    End Function

    ''' <summary>
    ''' Get the AML Risk for Directors and Shareholders
    ''' </summary>
    ''' <param name="sDate"></param>
    ''' <param name="eDate"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetDTSBAmlRiskSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'Enum_AMLRisk.Description (Description), Enum_DTSB_Type.Description (DTSBType), COUNT(DTSB.Id) AS Total, Enum_AMLRisk.Enum (Enum)
        '========================
        Dim dTSBAmlRiskSumTa As ReportDSTableAdapters.DirectorShareholderAMLRiskSummaryTableAdapter
        dTSBAmlRiskSumTa = New ReportDSTableAdapters.DirectorShareholderAMLRiskSummaryTableAdapter
        Dim dTSBAmlRiskSumDt As ReportDS.DirectorShareholderAMLRiskSummaryDataTable
        dTSBAmlRiskSumDt = dTSBAmlRiskSumTa.GetData(sDate, eDate)
        Return dTSBAmlRiskSumDt

    End Function

    Public Function GetDtsbAmlRiskCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal amlRiskEnum As Integer, ByVal dtsbTypeEnum As Integer) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim dtsbAmlRiskCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        dtsbAmlRiskCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter

        Dim dtsbAmlRiskCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        dtsbAmlRiskCustDt = dtsbAmlRiskCustTa.GetDataByDtsbAmlRiskCustomer(sDate, eDate, amlRiskEnum, dtsbTypeEnum)
        Return dtsbAmlRiskCustDt

    End Function


    Public Function GetAcceptMethCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal acceptanceMethodEnum As Integer, ByVal ClassificationEnum As Integer) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim acceptMethCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        acceptMethCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter

        Dim acceptMethCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        acceptMethCustDt = acceptMethCustTa.GetData(sDate, eDate, acceptanceMethodEnum, ClassificationEnum)
        Return acceptMethCustDt

    End Function

    Public Function GetAcceptMethCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal acceptanceMethodEnum As Integer) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim acceptMethCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        acceptMethCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter

        Dim acceptMethCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        acceptMethCustDt = acceptMethCustTa.GetData(sDate, eDate, acceptanceMethodEnum, Nothing)
        Return acceptMethCustDt

    End Function

    Public Function GetPEPCustomer(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim pepCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        pepCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        Dim pepCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        pepCustDt = pepCustTa.GetDataByPEP(sDate, eDate)
        Return pepCustDt

    End Function

    Public Function GetBeneOwnerPEPCustomer(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim pepBeneOwnerCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        pepBeneOwnerCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        Dim pepBeneOwnerCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        pepBeneOwnerCustDt = pepBeneOwnerCustTa.GetDataByBeneOwnerIsaPEP(sDate, eDate)
        Return pepBeneOwnerCustDt

    End Function

    Public Function GetAmlRiskSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'AML.Description, COUNT(C.CustomerId) AS Total, AML.Enum
        '========================
        Dim amlRiskSumTa As ReportDSTableAdapters.AmlRiskSummaryTableAdapter
        amlRiskSumTa = New ReportDSTableAdapters.AmlRiskSummaryTableAdapter
        Dim amlRiskSumDt As ReportDS.AmlRiskSummaryDataTable
        amlRiskSumDt = amlRiskSumTa.GetData(sDate, eDate)
        Return amlRiskSumDt

    End Function

    Public Function GetAmlRiskCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal amlRiskEnum As Integer) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim amlCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        amlCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        Dim amlCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        amlCustDt = amlCustTa.GetDataByAmlRisk(sDate, eDate, amlRiskEnum)
        Return amlCustDt

    End Function

    Public Function GetCustTypeSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'CT.Description, COUNT(C.CustomerId) AS Total, CT.Enum
        '========================
        Dim custTypeSumTa As ReportDSTableAdapters.CustTypeSummaryTableAdapter
        custTypeSumTa = New ReportDSTableAdapters.CustTypeSummaryTableAdapter
        Dim custTypeSumDt As ReportDS.CustTypeSummaryDataTable
        custTypeSumDt = custTypeSumTa.GetData(sDate, eDate)
        Return custTypeSumDt

    End Function

    Public Function GetCustTypeCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal custTypeEnum As Integer) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim custTypeCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        custTypeCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        Dim custTypeCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        custTypeCustDt = custTypeCustTa.GetDataByCustType(sDate, eDate, custTypeEnum)
        Return custTypeCustDt

    End Function

    Public Function GetLoanClassificationSummary(ByVal sDate As Date, ByVal eDate As Date) As DataTable
        '========================
        'EC.Description, COUNT(E.EnquiryId) AS Total, EC.Enum
        '========================
        Dim loanClassSumTa As ReportDSTableAdapters.LoanClassificationSummaryTableAdapter
        loanClassSumTa = New ReportDSTableAdapters.LoanClassificationSummaryTableAdapter
        Dim loanClassSumDt As ReportDS.LoanClassificationSummaryDataTable
        loanClassSumDt = loanClassSumTa.GetData(sDate, eDate)
        Return loanClassSumDt

    End Function

    Public Function GetClassificationCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal classificationEnum As Integer) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim classCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        classCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        Dim classCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        classCustDt = classCustTa.GetDataByClassification(sDate, eDate, classificationEnum)
        Return classCustDt

    End Function

    Private Function GetPaymentMethodSummary(ByVal sDate As Date, ByVal eDate As Date)
        '========================
        'Payment methods: Domestic Transfer|Cheque|Foreign Funds Transfer
        '========================
        Dim payMethTa As ReportDSTableAdapters.spPaymentMethodSummaryTableAdapter
        payMethTa = New ReportDSTableAdapters.spPaymentMethodSummaryTableAdapter
        Dim payMethDt As ReportDS.spPaymentMethodSummaryDataTable
        payMethDt = payMethTa.GetData(sDate, eDate)
        '**************************************
        'Transpose the table
        Dim newPayMethDt As DataTable
        newPayMethDt = New DataTable("PayoutPaymentMethod")
        Dim payTypeCol As DataColumn = New DataColumn("Method")
        payTypeCol.DataType = System.Type.GetType("System.String")
        Dim totalCol As DataColumn = New DataColumn("Loans")
        totalCol.DataType = System.Type.GetType("System.Int32")
        newPayMethDt.Columns.Add(payTypeCol)
        newPayMethDt.Columns.Add(totalCol)

        For Each dr As DataRow In payMethDt.Rows
            Dim row As DataRow
            Dim count As Integer = 0
            For Each dc As DataColumn In payMethDt.Columns
                row = newPayMethDt.NewRow
                row.Item(payTypeCol) = dc.ColumnName
                row.Item(totalCol) = dr.Item(count)
                count = count + 1
                newPayMethDt.Rows.Add(row)
            Next
        Next

        Return newPayMethDt

    End Function

    Private Function GetPayMethCustomer(ByVal sDate As Date, ByVal eDate As Date, ByVal payMethod As String) As DataTable
        '========================
        'Customer.Salutation, EnquiryType.Description, Payout.PayoutCompleted, Enquiry.EnquiryCode, Enquiry.EnquiryId
        '========================
        Dim payMethCustTa As ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        payMethCustTa = New ReportDSTableAdapters.AcceptanceMethodCustomerTableAdapter
        Dim payMethCustDt As ReportDS.AcceptanceMethodCustomerDataTable
        payMethCustDt = payMethCustTa.GetDataByPayMethod(sDate, eDate, payMethod)
        Return payMethCustDt


    End Function

    Private Sub btnRunReport_Click(sender As Object, e As EventArgs) Handles btnRunReport.Click
        Cursor = Cursors.WaitCursor
        Dim column As DataGridViewColumn
        startdate = DateTime.Parse(dtpStartDate.Value.Date)
        enddate = DateTime.Parse(dtpEndDate.Value.Date.AddDays(1)) 'Add a day to the end date to get full day in the filter
        'Reset Customers datagridview
        dgvAcceptMethCust.DataSource = Nothing
        gpbxAcceptMethCust.Text = "Customers: Select a line from a summary above and display associated enquiries"
        gpbxAcceptMethCust.ForeColor = System.Drawing.Color.Red
        '========================
        'Total Customers
        '========================
        Dim myConnection As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)
        myConnection.Open()
        Dim myCommand As New SqlCommand("CustomerSum", myConnection)
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.AddWithValue("@startdate", startdate)
        myCommand.Parameters.AddWithValue("@enddate", enddate)
        custTotal = myCommand.ExecuteScalar()
        lblTotalCust.Text = "Total customers (paid out) is " & custTotal
        '========================
        'Polically Exposed Persons
        '========================
        Dim myCommand2 As New SqlCommand("PepCustomerSum", myConnection)
        myCommand2.CommandType = CommandType.StoredProcedure
        myCommand2.Parameters.AddWithValue("@startdate", startdate)
        myCommand2.Parameters.AddWithValue("@enddate", enddate)
        pepTotal = myCommand2.ExecuteScalar()
        lblPep.Text = "Num of Politically Exposed Persons = " & pepTotal
        '========================
        'Polically Exposed Persons for Beneficial Owners
        '========================
        Dim myCommand3 As New SqlCommand("PepBeneficalOwnerSum", myConnection)
        myCommand3.CommandType = CommandType.StoredProcedure
        myCommand3.Parameters.AddWithValue("@startdate", startdate)
        myCommand3.Parameters.AddWithValue("@enddate", enddate)
        pepBeneficalOwnerSum = myCommand3.ExecuteScalar()
        lblPepBeneOwners.Text = "Num of Politically Exposed Beneficial Owners = " & pepBeneficalOwnerSum

        myConnection.Close()
        '========================
        'AML Risk Summary
        '========================
        amlSumTable = GetAmlRiskSummary(startdate, enddate)
        dgvAmlRiskSummary.DataSource = amlSumTable
        dgvAmlRiskSummary.Columns(2).Visible = False
        '------------------------------------
        column = dgvAmlRiskSummary.Columns(0)
        column.MinimumWidth = 150
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '------------------------------------
        column = dgvAmlRiskSummary.Columns(1)
        column.Width = 40
        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        column.ToolTipText = "Number of Customers"
        '--- add totals row
        Dim totalAmlCust As Integer
        For i As Integer = 0 To dgvAmlRiskSummary.Rows.Count - 1
            totalAmlCust = totalAmlCust + dgvAmlRiskSummary.Rows(i).Cells(1).Value
        Next
        Dim myrow As DataRow = amlSumTable.NewRow
        myrow(0) = "Total"
        myrow(1) = totalAmlCust
        myrow(2) = 99 'to satisfy Enum column (cannot be null)
        amlSumTable.Rows.Add(myrow)
        '========================
        'Director & Shareholder AML Risk Summary
        '========================
        dtsbAmlSumTable = GetDTSBAmlRiskSummary(startdate, enddate)
        LoadDSAmlRiskSumList()
        GroupListView(lvDSAmlRiskSum, 0)
        '========================
        'AcceptanceMethodSummary
        '========================
        sumTable = GetAcceptanceMethodSummary(startdate, enddate)
        dgvAcceptanceMethodSummary.DataSource = sumTable
        dgvAcceptanceMethodSummary.Columns(2).Visible = False
        '------------------------------------
        column = dgvAcceptanceMethodSummary.Columns(0)
        column.MinimumWidth = 150
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '------------------------------------
        column = dgvAcceptanceMethodSummary.Columns(1)
        column.Width = 40
        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        column.ToolTipText = "Number of Customers"
        '--- add totals row
        Dim totalCust As Integer
        For i As Integer = 0 To dgvAcceptanceMethodSummary.Rows.Count - 1
            totalCust = totalCust + dgvAcceptanceMethodSummary.Rows(i).Cells(1).Value
        Next
        myrow = sumTable.NewRow
        myrow(0) = "Total"
        myrow(1) = totalCust
        myrow(2) = 99
        sumTable.Rows.Add(myrow)
        '========================
        'AcceptanceMethodClassificationSummary
        '========================        
        amcSumTable = GetAcceptMethClassSummary(startdate, enddate)
        LoadAcceptMethClassSumList()
        GroupListView(lvAcceptMethClassSum, 0)
        '========================
        'Customer Type Summary
        '========================     
        custTypeSumTable = GetCustTypeSummary(startdate, enddate)
        dgvCustTypeSum.DataSource = custTypeSumTable
        dgvCustTypeSum.Columns(2).Visible = False
        '------------------------------------
        column = dgvCustTypeSum.Columns(0)
        column.MinimumWidth = 150
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '------------------------------------
        column = dgvCustTypeSum.Columns(1)
        column.Width = 40
        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        column.ToolTipText = "Number of Customers"
        '--- add totals row
        Dim totalCustType As Integer
        For i As Integer = 0 To dgvCustTypeSum.Rows.Count - 1
            totalCustType = totalCustType + dgvCustTypeSum.Rows(i).Cells(1).Value
        Next
        myrow = custTypeSumTable.NewRow
        myrow(0) = "Total"
        myrow(1) = totalCustType
        myrow(2) = 99
        custTypeSumTable.Rows.Add(myrow)
        '========================
        'Loan Classification Summary
        '========================  
        loanClassSumTable = GetLoanClassificationSummary(startdate, enddate)
        dgvClassificationSum.DataSource = loanClassSumTable
        dgvClassificationSum.Columns(3).Visible = False
        '------------------------------------
        column = dgvClassificationSum.Columns(0)
        column.MinimumWidth = 150
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '------------------------------------
        column = dgvClassificationSum.Columns(1)
        column.Width = 50
        column.ToolTipText = "Number of Enquiries Paidout"
        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        '------------------------------------
        column = dgvClassificationSum.Columns(2)
        column.Width = 100
        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        column.DefaultCellStyle.Format = "N2"
        '--- add totals row
        Dim totalDrawdowns As Decimal
        Dim totalLoans As Integer
        For i As Integer = 0 To dgvClassificationSum.Rows.Count - 1
            totalDrawdowns = totalDrawdowns + dgvClassificationSum.Rows(i).Cells(2).Value
            totalLoans = totalLoans + dgvClassificationSum.Rows(i).Cells(1).Value
        Next
        myrow = loanClassSumTable.NewRow
        myrow(0) = "Totals"
        myrow(1) = totalLoans
        myrow(2) = totalDrawdowns
        myrow(3) = 99
        loanClassSumTable.Rows.Add(myrow)

        '========================
        'Payout Payment Type Summary
        '========================     
        payMethodSumTable = GetPaymentMethodSummary(startdate, enddate)
        dgvPayMethods.DataSource = payMethodSumTable
        'dgvPayMethods.Columns(2).Visible = False
        '------------------------------------
        column = dgvPayMethods.Columns(0)
        column.MinimumWidth = 150
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '------------------------------------
        column = dgvPayMethods.Columns(1)
        column.Width = 40
        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        column.ToolTipText = "Number of Loans"
        '--- add totals row
        Dim totalPayTypes As Integer
        For i As Integer = 0 To dgvPayMethods.Rows.Count - 1
            totalPayTypes = totalPayTypes + dgvPayMethods.Rows(i).Cells(1).Value
        Next
        myrow = payMethodSumTable.NewRow
        myrow(0) = "Total"
        myrow(1) = totalPayTypes
        'myrow(2) = 99
        payMethodSumTable.Rows.Add(myrow)

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Initialise AcceptMethClassSumListView
    ''' </summary>
    ''' <remarks>Set to details view and add a columns with width and alignment</remarks>
    Private Sub InitializeAcceptMethClassSumListView()
        'Set to details view.
        lvAcceptMethClassSum.View = View.Details
        lvAcceptMethClassSum.FullRowSelect = True
        lvAcceptMethClassSum.ShowItemToolTips = True
        'Add a column with width and alignment.
        lvAcceptMethClassSum.Columns.Add("CustAcceptanceMethod", 0, HorizontalAlignment.Left)
        lvAcceptMethClassSum.Columns.Add("Classification", 200, HorizontalAlignment.Left)
        lvAcceptMethClassSum.Columns.Add("Total", 50, HorizontalAlignment.Right)
        lvAcceptMethClassSum.Columns.Add("CAM_Enum", 0, HorizontalAlignment.Left)
        lvAcceptMethClassSum.Columns.Add("Cl_Enum", 0, HorizontalAlignment.Left)


    End Sub


    ''' <summary>
    ''' Initialise DSAmlRiskSumListView
    ''' </summary>
    ''' <remarks>Set to details view and add a columns with width and alignment</remarks>
    Private Sub InitializeDSAmlRiskSumListView()
        'Set to details view.
        lvDSAmlRiskSum.View = View.Details
        lvDSAmlRiskSum.FullRowSelect = True
        lvDSAmlRiskSum.ShowItemToolTips = True
        'Add a column with width and alignment.
        lvDSAmlRiskSum.Columns.Add("Description", 0, HorizontalAlignment.Left)
        lvDSAmlRiskSum.Columns.Add("DTSBType", 200, HorizontalAlignment.Left)
        lvDSAmlRiskSum.Columns.Add("Total", 50, HorizontalAlignment.Right)
        lvDSAmlRiskSum.Columns.Add("AML_Enum", 0, HorizontalAlignment.Left)
        lvDSAmlRiskSum.Columns.Add("EDT_Enum", 0, HorizontalAlignment.Left)

    End Sub

    ''' <summary>
    ''' Load lvAcceptMethClassSum
    ''' </summary>
    ''' <remarks>Get the datatable, clear the ListView control and define the list items and then add the list items to the ListView</remarks>
    Private Sub LoadAcceptMethClassSumList()
        ' Get the datatable
        Dim dtable As DataTable = amcSumTable
        ' Clear the ListView control
        lvAcceptMethClassSum.Items.Clear()
        ' Display items in the ListView control
        For i As Integer = 0 To dtable.Rows.Count - 1
            Dim drow As DataRow = dtable.Rows(i)
            ' Only row that have not been deleted
            If drow.RowState <> DataRowState.Deleted Then
                ' Define the list items
                Dim lvi As New ListViewItem(drow("CustAcceptanceMethod").ToString())
                lvi.SubItems.Add(drow("Classification").ToString())
                lvi.SubItems.Add(drow("Total").ToString())
                lvi.SubItems.Add(drow("CAM_Enum").ToString())
                lvi.SubItems.Add(drow("Cl_Enum").ToString())
                lvi.ToolTipText = "Shows the number of Customers for each Acceptance Method and Enquiry Classification"
                ' Add the list items to the ListView
                lvAcceptMethClassSum.Items.Add(lvi)

            End If
        Next
    End Sub

    ''' <summary>
    ''' Load lvDSAmlRiskSum
    ''' </summary>
    ''' <remarks>Get the datatable, clear the ListView control and define the list items and then add the list items to the ListView</remarks>
    Private Sub LoadDSAmlRiskSumList()
        ' Get the datatable
        Dim dtable As DataTable = dtsbAmlSumTable
        ' Clear the ListView control
        lvDSAmlRiskSum.Items.Clear()
        ' Display items in the ListView control
        For i As Integer = 0 To dtable.Rows.Count - 1
            Dim drow As DataRow = dtable.Rows(i)
            ' Only row that have not been deleted
            If drow.RowState <> DataRowState.Deleted Then
                ' Define the list items
                Dim lvi As New ListViewItem(drow("Description").ToString())
                lvi.SubItems.Add(drow("DTSBType").ToString())
                lvi.SubItems.Add(drow("Total").ToString())
                lvi.SubItems.Add(drow("AML_Enum").ToString())
                lvi.SubItems.Add(drow("EDT_Enum").ToString())
                lvi.ToolTipText = "Shows the number of Directors and Shareholders for each Acceptance Method"
                ' Add the list items to the ListView
                lvDSAmlRiskSum.Items.Add(lvi)

            End If
        Next
    End Sub

    ''' <summary>
    ''' Setup grouping for the ListView
    ''' </summary>
    ''' <param name="lstV"></param>
    ''' <param name="SubItemIndex"></param>
    ''' <remarks></remarks>
    Public Sub GroupListView(ByVal lstV As ListView, ByVal SubItemIndex As Int16)
        Dim flag As Boolean = True
        For Each l As ListViewItem In lstV.Items
            Dim strmyGroupname As String = l.SubItems(SubItemIndex).Text
            For Each lvg As ListViewGroup In lstV.Groups
                If lvg.Name = strmyGroupname Then
                    l.Group = lvg
                    flag = False
                End If
            Next

            If flag = True Then
                Dim lstGrp As New ListViewGroup(strmyGroupname, strmyGroupname)
                lstV.Groups.Add(lstGrp)
                l.Group = lstGrp
            End If

            flag = True

        Next
    End Sub

    ''' <summary>
    ''' Setup columns for dgvAcceptMethCust datagridview
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DgvCustSetColumns()
        Dim column As DataGridViewColumn
        dgvAcceptMethCust.Columns(5).Visible = False 'EnquiryId
        dgvAcceptMethCust.Columns(6).Visible = False 'Archived
        '------------------------------------
        column = dgvAcceptMethCust.Columns(0)
        column.Width = 75
        '------------------------------------
        column = dgvAcceptMethCust.Columns(1)
        column.Width = 100
        '------------------------------------
        column = dgvAcceptMethCust.Columns(2)
        column.MinimumWidth = 200
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        '------------------------------------
        column = dgvAcceptMethCust.Columns(3)
        column.Width = 150
        '------------------------------------
        column = dgvAcceptMethCust.Columns(4)
        column.Width = 120

    End Sub

    'Private Sub lvAcceptMethClassSum_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles lvAcceptMethClassSum.ItemMouseHover
    '    e.Item.ToolTipText = "Shows the number of Customers for each Enquiry Classification"
    'End Sub


    Private Sub lvAcceptMethClassSum_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles lvAcceptMethClassSum.ItemSelectionChanged
        If e.IsSelected Then
            Dim custAcceptanceMethod As Integer = CInt(e.Item.SubItems(3).Text)
            Dim classification As Integer = CInt(e.Item.SubItems(4).Text)
            dgvAcceptMethCust.DataSource = Nothing

            gpbxAcceptMethCust.Text = "Customers: " & e.Item.SubItems(0).Text & " - " & e.Item.SubItems(1).Text
            gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
            cust = GetAcceptMethCustomer(startdate, enddate, custAcceptanceMethod, classification)
            dgvAcceptMethCust.DataSource = cust
            DgvCustSetColumns()

        End If
    End Sub

    Private Sub lvDSAmlRiskSum_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles lvDSAmlRiskSum.ItemSelectionChanged
        If e.IsSelected Then
            Dim amlRisk As Integer = CInt(e.Item.SubItems(3).Text)
            Dim DTSBType As Integer = CInt(e.Item.SubItems(4).Text)
            dgvAcceptMethCust.DataSource = Nothing

            gpbxAcceptMethCust.Text = "Customers: " & e.Item.SubItems(0).Text & " - " & e.Item.SubItems(1).Text
            gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
            cust = GetDtsbAmlRiskCustomer(startdate, enddate, amlRisk, DTSBType)
            dgvAcceptMethCust.DataSource = cust
            DgvCustSetColumns()

        End If
    End Sub

    Private Sub dgvAcceptMethCust_DoubleClick(sender As Object, e As EventArgs) Handles dgvAcceptMethCust.DoubleClick
        Cursor.Current = Cursors.WaitCursor
        Dim wizardStatus As Integer = 0
        Dim selectedRow As ReportDS.AcceptanceMethodCustomerRow
        Dim selectedRowEnquiryId As Integer
        Dim archived As Boolean
        Dim OpenFormSwitch As Boolean = True
        'Get selected row
        selectedRow = TryCast(dgvAcceptMethCust.SelectedRows(0).DataBoundItem, DataRowView).Row
        If Not selectedRow Is Nothing Then
            selectedRowEnquiryId = selectedRow.EnquiryId
            archived = selectedRow.Archived
            Dim frm As Form
            For Each frm In Application.OpenForms
                If frm.Tag = selectedRowEnquiryId Then
                    Dim frmNewLocX As Integer
                    Dim frmNewLocY As Integer
                    'set OpenFormSwitch
                    OpenFormSwitch = False
                    'set form focus
                    frm.Focus()
                    If frm.Location.X < 0 Then
                        frmNewLocX = 0
                    Else
                        frmNewLocX = frm.Location.X
                    End If
                    If frm.Location.Y < 0 Then
                        frmNewLocY = 0
                    Else
                        frmNewLocY = frm.Location.Y
                    End If
                    frm.Location = New Point(frmNewLocX, frmNewLocY)
                    frm.WindowState = FormWindowState.Normal
                    frm.Activate()
                    frm.FlashWindow(FlashOptions.TIMERNOFG, True, 5)

                    Exit For
                End If
            Next

            If OpenFormSwitch = True Then
                If archived = True Then 'destructive archive(purged)
                    Dim ArchiveFrm As New ArchiveForm
                    Try
                        'set cursor to wait cursor
                        Cursor.Current = Cursors.WaitCursor
                        'launch ArchiveForm passing EnquiryCodeValue
                        ArchiveFrm.PassVariable(selectedRowEnquiryId)
                        ArchiveFrm.Show()
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try
                Else
                    Try
                        'set cursor to wait cursor
                        Cursor.Current = Cursors.WaitCursor
                        'Get Wizard Status for selected Enquiry
                        'Get Connection String Settings
                        Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
                        Dim connection As New SqlConnection(connectionString)
                        Dim selectStatement As String = "SELECT WizardStatus FROM Enquiry WHERE EnquiryId ='" & selectedRow.EnquiryId & "'"
                        Dim selectCommand As New SqlCommand(selectStatement, connection)
                        Dim EnquiryWizDataAdapter As New SqlDataAdapter(selectCommand)
                        Dim EnquiryWizDataSet As New DataSet
                        Dim EnquiryWizDataTable As New DataTable

                        'dumps results into datatable LoginDataTable
                        EnquiryWizDataAdapter.Fill(EnquiryWizDataTable)
                        'if no matching rows .....
                        If EnquiryWizDataTable.Rows.Count = 0 Then
                            ' clear the dataTable and the Connect information
                            EnquiryWizDataAdapter = Nothing
                            EnquiryWizDataTable.Clear()
                            'Open Standard Worksheet Form
                            wizardStatus = MyEnums.WizardStatus.AppForm

                            'if there is a matching row
                        ElseIf EnquiryWizDataTable.Rows.Count = 1 Then
                            'get WizardStatus value
                            Dim EnquiryWizDataRow As DataRow = EnquiryWizDataTable.Rows(0)

                            'assign wizardstatus to Form variable WizardStatus 
                            wizardStatus = EnquiryWizDataRow.Item(0)

                            ' clear the dataTable and the Connect information
                            EnquiryWizDataAdapter = Nothing
                            EnquiryWizDataTable.Clear()
                        End If
                        'reset cursor
                        Cursor.Current = Cursors.Default
                    Catch ex As Exception
                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                    End Try

                    'set cursor to wait cursor
                    Cursor.Current = Cursors.WaitCursor
                    'launch Application Form and pass Keys
                    Select Case wizardStatus
                        Case Is >= MyEnums.WizardStatus.AppForm
                            'launch  Standard Worksheet Form
                            Dim EnquiryForm As New AppForm
                            'MessageBox.Show("Open AppForm", "Call Form")
                            Try
                                EnquiryForm.PassVariable(selectedRow.EnquiryId)
                                EnquiryForm.Show()
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox(ex.Message)
                            End Try
                        Case Is = MyEnums.WizardStatus.Preliminary
                            'launch PreliminaryForm and pass Enquiry Id
                            Dim OldEnquiryForm As New PreliminaryForm
                            'MessageBox.Show("Open PreliminaryForm", "Call Form")
                            Try
                                OldEnquiryForm.PassVariable(selectedRow.EnquiryId)
                                OldEnquiryForm.Show()
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox(ex.Message)
                            End Try
                        Case Is = MyEnums.WizardStatus.FollowUp
                            'launch FollowUpForm and pass Enquiry Id
                            Dim FollowUpFrm1 As New FollowUpForm
                            'MessageBox.Show("Open FollowUpForm", "Call Form")
                            Try
                                FollowUpFrm1.PassVariable(selectedRow.EnquiryId)
                                FollowUpFrm1.Show()
                            Catch ex As Exception
                                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                MsgBox(ex.Message)
                            End Try


                    End Select

                End If






            End If


        Else
            MsgBox("Please select a row")
        End If


        Cursor.Current = Cursors.Default




    End Sub

    Private Sub dgvAcceptanceMethodSummary_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvAcceptanceMethodSummary.CellFormatting
        If e.ColumnIndex = 0 Then
            If e.Value = "Total" Then
                Me.dgvAcceptanceMethodSummary.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGray
            End If
        End If
    End Sub

    Private Sub dgvAcceptanceMethodSummary_Click(sender As Object, e As EventArgs) Handles dgvAcceptanceMethodSummary.Click
        Cursor.Current = Cursors.WaitCursor
        Dim selectedRow As ReportDS.AcceptanceMethodSummaryRow
        Dim custAcceptanceMethod As Integer
        'Get selected row
        selectedRow = TryCast(sender.SelectedRows(0).DataBoundItem, DataRowView).Row
        gpbxAcceptMethCust.Text = "Customers: " & gpbxAcceptanceMethodSummary.Text & " - " & selectedRow.Description
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        custAcceptanceMethod = selectedRow._Enum

        cust = GetAcceptMethCustomer(startdate, enddate, custAcceptanceMethod)
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()

        Cursor.Current = Cursors.Default
    End Sub



    Private Sub lblPep_Click(sender As Object, e As EventArgs) Handles lblPep.Click
        'GetPEPCustomer
        dgvAcceptMethCust.DataSource = Nothing
        cust = GetPEPCustomer(startdate, enddate)
        gpbxAcceptMethCust.Text = "Customers: Politically Exposed Persons"
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()

    End Sub

    Private Sub lblPepBeneOwners_Click(sender As Object, e As EventArgs) Handles lblPepBeneOwners.Click
        'GetPEPBeneOwnersCustomer
        dgvAcceptMethCust.DataSource = Nothing
        cust = GetBeneOwnerPEPCustomer(startdate, enddate)
        gpbxAcceptMethCust.Text = "Customers: Beneficial Owner is a Politically Exposed Person"
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()
    End Sub


    Private Sub lblPep_MouseHover(sender As Object, e As EventArgs) Handles lblPep.MouseHover
        lblPep.BackColor = SystemColors.Highlight
        Cursor.Current = Cursors.Hand
    End Sub

    Private Sub lblPep_MouseLeave(sender As Object, e As EventArgs) Handles lblPep.MouseLeave
        lblPep.BackColor = System.Drawing.Color.Empty
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub lblPepBeneOwners_MouseHover(sender As Object, e As EventArgs) Handles lblPepBeneOwners.MouseHover
        lblPepBeneOwners.BackColor = SystemColors.Highlight
        Cursor.Current = Cursors.Hand
    End Sub

    Private Sub lblPepBeneOwners_MouseLeave(sender As Object, e As EventArgs) Handles lblPepBeneOwners.MouseLeave
        lblPepBeneOwners.BackColor = System.Drawing.Color.Empty
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub dgvAmlRiskSummary_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvAmlRiskSummary.CellFormatting
        If e.ColumnIndex = 0 Then
            If e.Value = "Total" Then
                Me.dgvAmlRiskSummary.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGray
            End If
        End If
    End Sub


    Private Sub dgvAmlRiskSummary_Click(sender As Object, e As EventArgs) Handles dgvAmlRiskSummary.Click
        Cursor.Current = Cursors.WaitCursor
        Dim selectedRow As ReportDS.AmlRiskSummaryRow
        Dim amlRiskEnum As Integer
        'Get selected row
        selectedRow = TryCast(sender.SelectedRows(0).DataBoundItem, DataRowView).Row
        gpbxAcceptMethCust.Text = "Customers: " & gpbxCustRiskSum.Text & " - " & selectedRow.Description
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        amlRiskEnum = selectedRow._Enum

        cust = GetAmlRiskCustomer(startdate, enddate, amlRiskEnum)
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub dgvCustTypeSum_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCustTypeSum.CellFormatting
        If e.ColumnIndex = 0 Then
            If e.Value = "Total" Then
                Me.dgvCustTypeSum.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGray
            End If
        End If
    End Sub

    Private Sub dgvCustTypeSum_Click(sender As Object, e As EventArgs) Handles dgvCustTypeSum.Click
        Cursor.Current = Cursors.WaitCursor
        Dim selectedRow As ReportDS.CustTypeSummaryRow
        Dim custTypeEnum As Integer
        'Get selected row
        selectedRow = TryCast(sender.SelectedRows(0).DataBoundItem, DataRowView).Row
        custTypeEnum = selectedRow._Enum
        gpbxAcceptMethCust.Text = "Customers: " & gpbxCustType.Text & " - " & selectedRow.Description
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        cust = GetCustTypeCustomer(startdate, enddate, custTypeEnum)
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub dgvClassificationSum_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvClassificationSum.CellFormatting
        If e.ColumnIndex = 0 Then
            If e.Value = "Totals" Then
                Me.dgvClassificationSum.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGray
            End If
        End If

    End Sub

    Private Sub dgvClassificationSum_Click(sender As Object, e As EventArgs) Handles dgvClassificationSum.Click
        Cursor.Current = Cursors.WaitCursor
        Dim selectedRow As ReportDS.LoanClassificationSummaryRow
        Dim classEnum As Integer
        'Get selected row
        selectedRow = TryCast(sender.SelectedRows(0).DataBoundItem, DataRowView).Row
        classEnum = selectedRow._Enum
        gpbxAcceptMethCust.Text = "Customers: " & gpbxCustPaidOut.Text & " - " & selectedRow.Description
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        cust = GetClassificationCustomer(startdate, enddate, classEnum)
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub dgvPayMethods_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPayMethods.CellFormatting
        If e.ColumnIndex = 0 Then
            If e.Value = "Total" Then
                Me.dgvPayMethods.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGray
            End If
        End If
    End Sub

    Private Sub dgvPayMethods_Click(sender As Object, e As EventArgs) Handles dgvPayMethods.Click
        Cursor.Current = Cursors.WaitCursor
        Dim selectedRow As DataRow
        selectedRow = TryCast(sender.SelectedRows(0).DataBoundItem, DataRowView).Row
        Dim payMethod As String
        payMethod = selectedRow.Item(0)
        gpbxAcceptMethCust.Text = "Loans: " & gpbxPayMethod.Text & " - " & payMethod
        gpbxAcceptMethCust.ForeColor = SystemColors.ControlText
        cust = GetPayMethCustomer(startdate, enddate, payMethod)
        dgvAcceptMethCust.DataSource = cust
        DgvCustSetColumns()

        Cursor.Current = Cursors.Default

    End Sub


    Private Sub AMLReport_Layout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        gpbxAcceptMethCust.Size = New Size(861, Me.Height - 692)
    End Sub


    Private Sub btnPdf_Click(sender As Object, e As EventArgs) Handles btnPdf.Click
        If custTotal > 0 Then
            Try
                Dim doc As MigraDoc.DocumentObjectModel.Document = CreateDocument()
                '========================
                'rendering doc
                Dim docRend As MigraDoc.Rendering.PdfDocumentRenderer = New MigraDoc.Rendering.PdfDocumentRenderer(False)
                docRend.Document = doc
                docRend.RenderDocument()
                Dim filename As String = Path.Combine(Path.GetTempPath(), "AML-CT Report.pdf") 'Save file to user's Temp folder
                docRend.PdfDocument.Save(filename)

                'open file in default app
                Dim processInfo As System.Diagnostics.ProcessStartInfo = New System.Diagnostics.ProcessStartInfo
                processInfo.FileName = filename

                System.Diagnostics.Process.Start(processInfo)

            Catch ex As Exception
                log.Error("Error creating PDF: " & ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.ToString, "Error creating PDF", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        Else
            MessageBox.Show("No customers to report on!", "Error creating PDF", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

        End If

    End Sub

#Region "MigraDoc"
    ''' <summary>
    ''' Create MigrDoc Pdf document
    ''' </summary>
    ''' <returns>MigraDoc.DocumentObjectModel.Document</returns>
    ''' <remarks></remarks>
    Public Function CreateDocument() As MigraDoc.DocumentObjectModel.Document
        'Create a new MigraDoc document
        Dim document As MigraDoc.DocumentObjectModel.Document
        document = New MigraDoc.DocumentObjectModel.Document()
        document.Info.Title = "AML/CT Report"
        document.Info.Subject = "AML/CT Report: " & startdate & " to " & enddate
        document.Info.Author = "TrueTrack"

        DefineStyles(document)

        CreatePage(document)

        Return document

    End Function

#Region "Document Setup"
    ''' <summary>
    ''' Styles define how the text will look
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DefineStyles(ByRef document As MigraDoc.DocumentObjectModel.Document)
        ' Get the predefined style Normal.
        Dim style As MigraDoc.DocumentObjectModel.Style = document.Styles("Normal")
        ' Because all styles are derived from Normal, the next line changes the 
        ' font of the whole document. Or, more exactly, it changes the font of
        ' all styles and paragraphs that do not redefine the font.
        style.Font.Name = "Verdana"

        style = document.Styles(MigraDoc.DocumentObjectModel.StyleNames.Header)
        style.ParagraphFormat.AddTabStop("16cm", MigraDoc.DocumentObjectModel.TabAlignment.Right)

        style = document.Styles(MigraDoc.DocumentObjectModel.StyleNames.Footer)
        style.ParagraphFormat.AddTabStop("8cm", MigraDoc.DocumentObjectModel.TabAlignment.Center)

        ' Create a new style called Table based on style Normal
        style = document.Styles.AddStyle("Table", "Normal")
        style.Font.Name = "Verdana"
        'style.Font.Name = "Times New Roman"
        style.Font.Size = 9

        'Create a new style called Reference based on style Normal
        style = document.Styles.AddStyle("Reference", "Normal")
        style.ParagraphFormat.SpaceBefore = "5mm"
        style.ParagraphFormat.SpaceAfter = "5mm"
        style.ParagraphFormat.TabStops.AddTabStop("16cm", MigraDoc.DocumentObjectModel.TabAlignment.Right)
    End Sub

    ''' <summary>
    ''' Create the page with tables, header, footer
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CreatePage(ByRef document As MigraDoc.DocumentObjectModel.Document)
        'Each MigraDoc document needs at least one section.
        Dim row As MigraDoc.DocumentObjectModel.Tables.Row
        Dim cell As MigraDoc.DocumentObjectModel.Tables.Cell
        Dim sec As MigraDoc.DocumentObjectModel.Section = document.AddSection
        sec.AddParagraph("Yes Finance AML/CT Report").Format.Font.Bold = True
        sec.AddParagraph("Date range: " & startdate & " to " & enddate)
        sec.AddParagraph() 'insert blank paragraph
        sec.AddParagraph("Total customers (paid out) is " & custTotal)
        sec.AddParagraph("Number of Politically Exposed Persons = " & pepTotal)
        sec.AddParagraph("Number of Politically Exposed Beneficial Owners = " & pepBeneficalOwnerSum)
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'Loan Classification Summary
        '======================== 
        'define table
        Dim table As MigraDoc.DocumentObjectModel.Tables.Table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        Dim column As MigraDoc.DocumentObjectModel.Tables.Column
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(3))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 2
        cell.AddParagraph("Loan Classification Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("Classification")
        cell.Format.Font.Bold = False
        cell = row.Cells(1)
        cell.AddParagraph("Loans")
        cell.Format.Font.Bold = False
        cell = row.Cells(2)
        cell.AddParagraph("Drawdowns")
        cell.Format.Font.Bold = False

        'define rows of datatable
        For Each dr As DataRow In loanClassSumTable.Rows
            row = table.AddRow
            If dr("Description") = "Totals" Then
                row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.LightGray
            End If
            cell = row.Cells(0)
            cell.AddParagraph(dr("Description"))
            cell.Format.Font.Bold = False
            cell = row.Cells(1)
            cell.AddParagraph(dr("Total").ToString())
            cell.Format.Font.Bold = False
            cell = row.Cells(2)
            'cell.AddParagraph(dr("Drawdown").ToString("C2"))
            cell.AddParagraph(String.Format("{0:C2}", dr("Drawdown")))
            cell.Format.Font.Bold = False
        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'Customer Type Summary
        '========================
        'define table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 1
        cell.AddParagraph("Customer Type Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("Customer Type")
        cell.Format.Font.Bold = False

        cell = row.Cells(1)
        cell.AddParagraph("Customers")
        cell.Format.Font.Bold = False

        'define rows of datatable
        For Each dr As DataRow In custTypeSumTable.Rows
            row = table.AddRow
            If dr("Description") = "Total" Then
                row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.LightGray
            End If
            cell = row.Cells(0)
            cell.AddParagraph(dr("Description"))
            cell.Format.Font.Bold = False
            cell = row.Cells(1)
            cell.AddParagraph(dr("Total").ToString())
            cell.Format.Font.Bold = False
        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'Customer Acceptance Method Summary
        '========================
        'define table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 1
        cell.AddParagraph("Customer Acceptance Method Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("Acceptance Method")
        cell.Format.Font.Bold = False

        cell = row.Cells(1)
        cell.AddParagraph("Customers")
        cell.Format.Font.Bold = False

        'define rows of datatable
        For Each dr As DataRow In sumTable.Rows
            row = table.AddRow
            If dr("Description") = "Total" Then
                row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.LightGray
            End If
            cell = row.Cells(0)
            cell.AddParagraph(dr("Description"))
            cell.Format.Font.Bold = False
            cell = row.Cells(1)
            cell.AddParagraph(dr("Total").ToString())
            cell.Format.Font.Bold = False
        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'Acceptance Method Classification Summary
        '========================
        'define table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(3))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 2
        cell.AddParagraph("Acceptance Method Classification Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("Acceptance Method")
        cell.Format.Font.Bold = False

        cell = row.Cells(1)
        cell.AddParagraph("Classification")
        cell.Format.Font.Bold = False

        cell = row.Cells(2)
        cell.AddParagraph("Customers")
        cell.Format.Font.Bold = False

        'define groupby list
        Dim groupName As New List(Of String)()
        'define rows of datatable
        For Each dr As DataRow In amcSumTable.Rows
            If groupName.Contains(dr("CustAcceptanceMethod")) Then
                row = table.AddRow
                cell = row.Cells(0)
                cell.AddParagraph("")
                cell.Format.Font.Bold = False
                cell = row.Cells(1)
                cell.AddParagraph(dr("Classification"))
                cell.Format.Font.Bold = False
                cell = row.Cells(2)
                cell.AddParagraph(dr("Total").ToString())
                cell.Format.Font.Bold = False
            Else
                groupName.Add(dr("CustAcceptanceMethod"))
                'add group row
                row = table.AddRow
                cell = row.Cells(0)
                cell.MergeRight = 2
                cell.AddParagraph(dr("CustAcceptanceMethod"))
                cell.Format.Font.Bold = False
                cell.Format.Font.Italic = True
                'add data row
                row = table.AddRow
                cell = row.Cells(0)
                cell.AddParagraph("")
                cell.Format.Font.Bold = False
                cell = row.Cells(1)
                cell.AddParagraph(dr("Classification"))
                cell.Format.Font.Bold = False
                cell = row.Cells(2)
                cell.AddParagraph(dr("Total").ToString())
                cell.Format.Font.Bold = False
            End If
        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'Customer AML Risk Summary
        '========================
        'define table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 1
        cell.AddParagraph("Customer AML Risk Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("AML Risk")
        cell.Format.Font.Bold = False

        cell = row.Cells(1)
        cell.AddParagraph("Customers")
        cell.Format.Font.Bold = False
        'define rows of datatable
        For Each dr As DataRow In amlSumTable.Rows
            row = table.AddRow
            If dr("Description") = "Total" Then
                row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.LightGray
            End If
            cell = row.Cells(0)
            cell.AddParagraph(dr("Description"))
            cell.Format.Font.Bold = False
            cell = row.Cells(1)
            cell.AddParagraph(dr("Total").ToString())
            cell.Format.Font.Bold = False
        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'DTSB AML Risk Summary
        '========================
        'define table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(3))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 2
        cell.AddParagraph("Director/Shareholder AML Risk Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("AML Risk")
        cell.Format.Font.Bold = False

        cell = row.Cells(1)
        cell.AddParagraph("DTSB Type")
        cell.Format.Font.Bold = False

        cell = row.Cells(2)
        cell.AddParagraph("DTSBs")
        cell.Format.Font.Bold = False

        'define groupby list
        Dim groupNameAml As New List(Of String)()
        'define rows of datatable
        For Each dr As DataRow In dtsbAmlSumTable.Rows
            If groupNameAml.Contains(dr("Description")) Then
                row = table.AddRow
                cell = row.Cells(0)
                cell.AddParagraph("")
                cell.Format.Font.Bold = False
                cell = row.Cells(1)
                cell.AddParagraph(dr("DTSBtype"))
                cell.Format.Font.Bold = False
                cell = row.Cells(2)
                cell.AddParagraph(dr("Total").ToString())
                cell.Format.Font.Bold = False
            Else
                groupNameAml.Add(dr("Description"))
                'add group row
                row = table.AddRow
                cell = row.Cells(0)
                cell.MergeRight = 2
                cell.AddParagraph(dr("Description"))
                cell.Format.Font.Bold = False
                cell.Format.Font.Italic = True
                'add data row
                row = table.AddRow
                cell = row.Cells(0)
                cell.AddParagraph("")
                cell.Format.Font.Bold = False
                cell = row.Cells(1)
                cell.AddParagraph(dr("DTSBtype"))
                cell.Format.Font.Bold = False
                cell = row.Cells(2)
                cell.AddParagraph(dr("Total").ToString())
                cell.Format.Font.Bold = False
            End If

        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        'Pay Methods Summary
        '========================
        'define table
        table = New MigraDoc.DocumentObjectModel.Tables.Table
        table.Borders.Width = 0.1
        'define columns
        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(7))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left

        column = table.AddColumn(MigraDoc.DocumentObjectModel.Unit.FromCentimeter(5))
        column.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Right
        'add title to table and set borders to nothing
        row = table.AddRow
        cell = row.Cells(0)
        cell.MergeRight = 1
        cell.AddParagraph("Loan Payment Method Summary")
        cell.Borders.Width = 0
        cell.Format.Font.Bold = True
        'define header of table
        row = table.AddRow
        row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.PaleGoldenrod
        cell = row.Cells(0)
        cell.AddParagraph("Payment Method")
        cell.Format.Font.Bold = False

        cell = row.Cells(1)
        cell.AddParagraph("Loans")
        cell.Format.Font.Bold = False
        'define rows of datatable
        For Each dr As DataRow In payMethodSumTable.Rows
            row = table.AddRow
            If dr("Method") = "Total" Then
                row.Shading.Color = MigraDoc.DocumentObjectModel.Colors.LightGray
            End If
            cell = row.Cells(0)
            cell.AddParagraph(dr("Method"))
            cell.Format.Font.Bold = False
            cell = row.Cells(1)
            cell.AddParagraph(dr("Loans").ToString())
            cell.Format.Font.Bold = False
        Next dr
        'keep the table rows together
        table.Rows(0).KeepWith = table.Rows.Count - 1
        'add table to document
        document.LastSection.Add(table)
        '========================
        sec.AddParagraph() 'insert blank paragraph
        '========================
        sec.AddParagraph("Printed by " & LoggedinFullName & " on " & DateTime.Now.ToString)
        '========================

    End Sub




#End Region

#End Region


   
End Class
