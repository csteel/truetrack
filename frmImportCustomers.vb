﻿Public Class FrmImportCustomers

    Private enquiryId As Integer
    Private enquiryCode As String = String.Empty
    Private oacCustList As List(Of OacCustomer)
    Private ttCustList As List(Of TtCustomer)
    Private unmatchedTtCustomers As New List(Of TtCustomer)
    Private unmatchedOacCustomers As New List(Of OacCustomer)
    Private onlineApplication As OnlineApplication

    Public Sub New(ByVal enquiryId As Integer, ByVal enquiryCode As String, ByVal oacCustomersList As List(Of OacCustomer), ByVal ttCustomerList As List(Of TtCustomer))
        'This call is required by the designer.
        InitializeComponent()
        Me.enquiryId = enquiryId
        Me.enquiryCode = enquiryCode
        Me.oacCustList = oacCustomersList
        Me.ttCustList = ttCustomerList
    End Sub
    ''' <summary>
    ''' Create list of TtCustomers not matched to a OacCustomer and list of OacCustomers that were not matched to a TtCustomer
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Partnership has already been filtered out</remarks>
    Private Sub FrmImportCustomersLoad(sender As Object, e As EventArgs) Handles Me.Load
        'Fill UnmatchedOacCustomers (PersonActing and Partnership have already been filtered out)
        Me.unmatchedOacCustomers = oacCustList
        For Each ttCust As TtCustomer In ttCustList
            If Not ttCust.Type = MyEnums.mainCustomerType.Partnership Then
                If ttCust.XRefId = String.Empty Then
                    'If XRefId is null then add ttCustomer to UnmatchedTTCustomers list
                    unmatchedTtCustomers.Add(ttCust)
                Else
                    'Check XRefId is in UnmatchedOacCustomers
                    Dim exists As Boolean = False
                    For Each oacCust As OacCustomer In unmatchedOacCustomers
                        If CStr(oacCust.XRefId) = ttCust.XRefId Then
                            exists = True
                            'remove oacCust from UnmatchedOacCustomers
                            unmatchedOacCustomers.Remove(oacCust)
                            Exit For
                        End If
                    Next
                    If Not exists Then
                        unmatchedTtCustomers.Add(ttCust)
                    End If
                End If
            End If
        Next
        'So afterthis we should have a list of TtCustomers not matched to a OacCustomer
        'And a list of OacCustomers that were not matched to a TtCustomer
        'Count TtCustomers not matched, if count > 0
        'Display those not able to be matched and ask user to match (Partnership has already been filtered out)
        If unmatchedTtCustomers.Count > 0 Then
            Try
                'Load comboBox OACCustomers
                Dim comboSource As New Dictionary(Of Integer, String)()
                For Each oacCust As OacCustomer In unmatchedOacCustomers
                    Try
                        Select Case oacCust.Customertype
                            Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL, Constants.CONST_OAC_CUSTOMERTYPE_SOLETRADER
                                comboSource.Add(oacCust.XRefId, oacCust.FirstName & " " & oacCust.Lastname)
                            Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY, Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                                comboSource.Add(oacCust.XRefId, oacCust.BusinessName)
                            Case Else 'If Partnerships not filtered
                                comboSource.Add(oacCust.XRefId, oacCust.BusinessName)
                        End Select
                    Catch ex As Exception
                        MsgBox("FrmImportCustomersLoad caused an error" & vbNewLine & ex.Message.ToString)
                    End Try
                Next

                OACCustomers.DataSource = New BindingSource(comboSource, Nothing)
                OACCustomers.DisplayMember = "Value"
                OACCustomers.ValueMember = "Key"

                'Load datagridview dgvMatchCustomers
                For Each ttCust As TtCustomer In unmatchedTtCustomers
                    With ttCust
                        dgvMatchCustomers.Rows.Add(.CustomerId.ToString(), .Name, Nothing)
                    End With
                Next

                'set form focus
                Me.dgvMatchCustomers.Focus()
                Me.dgvMatchCustomers.CurrentCell = Me.dgvMatchCustomers.Rows(0).Cells(2)

            Catch ex As Exception
                MsgBox("Error importing OAC Customers" & vbNewLine & ex.Message, "Error")
                DialogResult = System.Windows.Forms.DialogResult.None
            End Try

        Else
            'All TtCustomers are matched or there was none
            'So any UnmatchedOacCustomers are used to create TrueTrack Customers (Partnership has already been filtered out)
            If unmatchedOacCustomers.Count > 0 Then
                'import OAC customers directly, no user interaction needed
                onlineApplication = New OnlineApplication
                If onlineApplication.InsertOacCustomers(enquiryId, unmatchedOacCustomers) Then
                    DialogResult = System.Windows.Forms.DialogResult.OK
                Else
                    MsgBox("Error importing OAC Customers", "Error")
                    DialogResult = System.Windows.Forms.DialogResult.None
                End If
            Else
                'MsgBox("No OAC customers and no TrueTrack customers to match")
                MsgBox("All customers from the OAC have already been imported")
                'DialogResult set to None so will not trigger Completed message
                DialogResult = System.Windows.Forms.DialogResult.Ignore
            End If
        End If



    End Sub



    Private Sub FrmImportCustomersLayout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        Dim custCount As Integer
        Dim dgvHeight As Integer
        custCount = dgvMatchCustomers.Rows.Count
        dgvHeight = custCount * 22 + 23
        dgvMatchCustomers.Size = New Size(543, dgvHeight)
        Me.Size = New Size(595, dgvMatchCustomers.Location.Y + dgvHeight + 100)
    End Sub

    Private Sub SetFormCaption()
        If Not enquiryCode = String.Empty Then
            Me.Text = enquiryCode & " | Import Customers from the OAC"
        Else
            Me.Text = "Import Customers from the OAC"
        End If

    End Sub



    ''' <summary>
    ''' Save matches
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub BtnNextClick(sender As Object, e As EventArgs) Handles btnNext.Click
        'Save matches: 
        'on each row get OacCustomer.XRefId and save to ttCustomer.XRefId
        'Remove current OacCustomer and TtCustomer from their respective lists
        'At end if any OacCustomers left in list then insert new Ttcustomers
        'At end if any ttCustomer left in list then raise error message and abort,
        'suggest removing TtCustomer form trueTrack.
        Dim ttCustId As Integer
        Dim oacCustId As String
        For Each row As DataGridViewRow In dgvMatchCustomers.Rows
            ttCustId = CInt(row.Cells(0).Value)
            oacCustId = CStr(DirectCast(row.Cells(2), DataGridViewComboBoxCell).Value) 'Value member
            If Not oacCustId Is Nothing And Not oacCustId = String.Empty Then
                'update TrueTrack customer with the XRefId                
                onlineApplication = New OnlineApplication
                onlineApplication.UpdateCustXRefId(ttCustId, oacCustId)
                'Remove OAC customer from UnmatchedOacCustomers list
                unmatchedOacCustomers.Remove(New OacCustomer() With {.XRefId = oacCustId})
                'Remove TT customer from UnmatchedTtCustomers List
                unmatchedTtCustomers.Remove(New TtCustomer() With {.CustomerId = ttCustId})
            End If
        Next
        'if any OacCustomers left in list then insert new Ttcustomers
        If unmatchedOacCustomers.Count > 0 Then
            'import OAC customers directly, no user interaction needed
            onlineApplication = New OnlineApplication
            If onlineApplication.InsertOacCustomers(enquiryId, unmatchedOacCustomers) Then
                DialogResult = System.Windows.Forms.DialogResult.OK
            Else
                MsgBox("Error importing OAC Customers", "Error")
                DialogResult = System.Windows.Forms.DialogResult.None
            End If
        End If
        'if any ttCustomer left in list then raise error message and abort
        If unmatchedTtCustomers.Count > 0 Then
            Dim msg As String = String.Empty
            For Each ttCust As TtCustomer In unmatchedTtCustomers
                msg = msg & ("There is an unmatched TrueTrack customer: " & ttCust.Name & vbCrLf)
            Next
            If unmatchedTtCustomers.Count > 1 Then
                MsgBox(msg & vbCrLf & "We suggest you remove these customers.", "Error in customer matching")
            Else
                MsgBox(msg & vbCrLf & "We suggest you remove this customer.", "Error in customer matching")
            End If
        End If
        'finish
        DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

   

    Private Sub dgvMatchCustomers_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvMatchCustomers.DataError
        MessageBox.Show("Error happened " & e.Context.ToString())

        If (e.Context = DataGridViewDataErrorContexts.Commit) Then
            MessageBox.Show("Commit error")
        End If
        If (e.Context = DataGridViewDataErrorContexts.CurrentCellChange) Then
            MessageBox.Show("Cell change")
        End If
        If (e.Context = DataGridViewDataErrorContexts.Parsing) Then
            MessageBox.Show("parsing error")
        End If
        If (e.Context = DataGridViewDataErrorContexts.LeaveControl) Then
            MessageBox.Show("leave control error")
        End If

        If (TypeOf (e.Exception) Is ConstraintException) Then
            Dim view As DataGridView = CType(sender, DataGridView)
            view.Rows(e.RowIndex).ErrorText = "an error"
            view.Rows(e.RowIndex).Cells(e.ColumnIndex).ErrorText = "an error"

            e.ThrowException = False
        End If
    End Sub
End Class