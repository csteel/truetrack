﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("AppWhShtB.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        '''</summary>
        Friend ReadOnly Property AppWkst() As System.Drawing.Icon
            Get
                Dim obj As Object = ResourceManager.GetObject("AppWkst", resourceCulture)
                Return CType(obj,System.Drawing.Icon)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property cancelImage() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("cancelImage", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property CheckNames() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("CheckNames", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property clients16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("clients16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property comment16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("comment16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to 
        '''========================================
        '''Yes Finance Email Loan Remittance Advice
        '''========================================
        '''{0}
        '''
        '''{1}
        '''
        '''Payout Summary and Advice
        '''*************************
        '''Loan Contract number: {2}
        '''Loan Name: {3}
        '''
        '''Cash Price                                           {4:C2}
        '''Additional Advances (Including Dealer Warranties)         {7:C2}
        '''Less Deposit                                               {5:C2}            {6:C2}
        '''
        '''Brokerage                                                 [rest of string was truncated]&quot;;.
        '''</summary>
        Friend ReadOnly Property DealerRemittance() As String
            Get
                Return ResourceManager.GetString("DealerRemittance", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property delete16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("delete16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property Docs24() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("Docs24", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property editdetails16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("editdetails16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property email16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("email16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to     &lt;table style=&apos;font-family:verdana;font-size:80%;&apos; width=&apos;{21}&apos; border=&apos;0&apos; cellspacing=&apos;1&apos; cellpadding=&apos;1&apos;&gt;
        '''        &lt;tr&gt;
        '''            &lt;td colspan=&apos;4&apos;&gt;========================================&lt;/td&gt;
        '''        &lt;/tr&gt;
        '''        &lt;tr&gt;
        '''            &lt;td colspan=&apos;4&apos;&gt;Yes Finance Email Loan Remittance Advice&lt;/td&gt;            
        '''        &lt;/tr&gt;
        '''        &lt;tr&gt;
        '''            &lt;td colspan=&apos;4&apos;&gt;========================================&lt;/td&gt;
        '''        &lt;/tr&gt;
        '''        &lt;tr&gt;
        '''            &lt;td width=&apos;50%&apos;&gt;{0}&lt;/td&gt;
        '''            &lt;td width=&apos;1 [rest of string was truncated]&quot;;.
        '''</summary>
        Friend ReadOnly Property EmailDealerRemittance() As String
            Get
                Return ResourceManager.GetString("EmailDealerRemittance", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F:\ImportXMLTrueTrack2Fin.
        '''</summary>
        Friend ReadOnly Property finPowerImportFolder_PROD() As String
            Get
                Return ResourceManager.GetString("finPowerImportFolder_PROD", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F:\Test_ImportXMLTrueTrack2Fin.
        '''</summary>
        Friend ReadOnly Property finPowerImportFolder_TEST() As String
            Get
                Return ResourceManager.GetString("finPowerImportFolder_TEST", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F:\finData Client Files\Client Files.
        '''</summary>
        Friend ReadOnly Property finPowerSharedFolder_PROD() As String
            Get
                Return ResourceManager.GetString("finPowerSharedFolder_PROD", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to F:\Test_finData Client Files\Client Files.
        '''</summary>
        Friend ReadOnly Property finPowerSharedFolder_TEST() As String
            Get
                Return ResourceManager.GetString("finPowerSharedFolder_TEST", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder_invoices_open16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder_invoices_open16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder_with_file_icon() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder_with_file_icon", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder_with_file_icon_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder_with_file_icon_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property MC16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("MC16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property NewEnquiry16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("NewEnquiry16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property OAC16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("OAC16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property paperclip16a() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("paperclip16a", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property questionBsm() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("questionBsm", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property questionGsm() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("questionGsm", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        '''</summary>
        Friend ReadOnly Property refresh() As System.Drawing.Icon
            Get
                Dim obj As Object = ResourceManager.GetObject("refresh", resourceCulture)
                Return CType(obj,System.Drawing.Icon)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property refresh1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("refresh1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property refresh16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("refresh16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property saveAll16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("saveAll16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property splash() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("splash", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        '''</summary>
        Friend ReadOnly Property TT() As System.Drawing.Icon
            Get
                Dim obj As Object = ResourceManager.GetObject("TT", resourceCulture)
                Return CType(obj,System.Drawing.Icon)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property undoImage() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("undoImage", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property undoImageNotActive() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("undoImageNotActive", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to W:\EnquiryFiles\Archives.
        '''</summary>
        Friend ReadOnly Property WorksheetArchiveFolder_PROD() As String
            Get
                Return ResourceManager.GetString("WorksheetArchiveFolder_PROD", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to W:\Test\EnquiryFiles\Archives.
        '''</summary>
        Friend ReadOnly Property WorksheetArchiveFolder_TEST() As String
            Get
                Return ResourceManager.GetString("WorksheetArchiveFolder_TEST", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to W:\EnquiryFiles.
        '''</summary>
        Friend ReadOnly Property WorksheetSharedFolder_PROD() As String
            Get
                Return ResourceManager.GetString("WorksheetSharedFolder_PROD", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to W:\Test\EnquiryFiles.
        '''</summary>
        Friend ReadOnly Property WorksheetSharedFolder_TEST() As String
            Get
                Return ResourceManager.GetString("WorksheetSharedFolder_TEST", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property www16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("www16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property YesLogo_sm() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("YesLogo_sm", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property YesLogo_sm2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("YesLogo_sm2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        '''</summary>
        Friend ReadOnly Property YFL() As System.Drawing.Icon
            Get
                Dim obj As Object = ResourceManager.GetObject("YFL", resourceCulture)
                Return CType(obj,System.Drawing.Icon)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        '''</summary>
        Friend ReadOnly Property YFL2() As System.Drawing.Icon
            Get
                Dim obj As Object = ResourceManager.GetObject("YFL2", resourceCulture)
                Return CType(obj,System.Drawing.Icon)
            End Get
        End Property
    End Module
End Namespace
