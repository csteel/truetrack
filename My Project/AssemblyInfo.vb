﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("TrueTrack")> 
<Assembly: AssemblyDescription("Enquiry and Application Due Diligence")> 
<Assembly: AssemblyCompany("Yes Finance Ltd")> 
<Assembly: AssemblyProduct("TrueTrack")> 
<Assembly: AssemblyCopyright("Copyright © Yes Finance Ltd 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6f396fd9-7ffc-4ce6-a6d4-89288f682fec")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.0")> 
<Assembly: AssemblyFileVersion("0.0.0.0")>

<Assembly: NeutralResourcesLanguageAttribute("en-NZ")>

<Assembly: log4net.Config.XmlConfigurator(ConfigFile:="log4net.config", Watch:=True)>