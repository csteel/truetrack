﻿Imports Microsoft.Reporting.WinForms

Public Class ReportStatusForm
    Dim ThisGroupBy As String
    Dim ThisOrderBy As String
    Dim ThisUser As String
    Dim ThisStatus As String

    Friend Sub ReportSelections_PassVariable(ByVal StatusVar As String, ByVal GroupBy As String, ByVal OrderBy As String, ByVal User As String)

        ThisStatus = StatusVar
        ThisGroupBy = GroupBy
        ThisOrderBy = OrderBy
        ThisUser = User
        'MsgBox("Startdate: " & Startdate.ToString & vbCrLf & "Enddate: " & EndDate & vbCrLf & "ApprovalStatus: " & ThisApprovalStatus)
        Try
            'load tableAdapter
            Me.CurrentEnquiryReportTableAdapter.Fill(EnquiryWorkSheetDataSet.CurrentEnquiryReport)
            'set report parameters
            'Title
            Dim p As New ReportParameter("parReportTitle", "Current Status Report")
            'Group By
            Dim q As New ReportParameter("parReportGroupBy", ThisGroupBy)
            'OrderBy
            Dim r As New ReportParameter("parReportOrderBy", ThisOrderBy)
            'set LoggedinName
            Dim t As New ReportParameter("parReportLoggedinName", LoggedinName)
            'set parReportUser
            Dim u As New ReportParameter("parReportUser", ThisUser)
            'set parReportStatus
            Dim v As New ReportParameter("parReportStatusFilter", ThisStatus)

            ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p, q, r, t, u, v})

            Me.ReportViewer1.RefreshReport()

        Catch e As System.Exception
            Dim inner As Exception = e.InnerException
            If Not (inner Is Nothing) Then
                MsgBox("Error launching ReportPayoutDateForm: " + vbCrLf + inner.Message)
                inner = inner.InnerException
            Else
                MsgBox("Error launching ReportPayoutDateForm: " + vbCrLf + e.Message)
            End If
        End Try

    End Sub


    Private Sub ReportStatusForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
    End Sub
End Class