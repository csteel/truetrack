﻿
Imports System.IO
Imports System.Data.SqlClient

Module FileOpsModule
    Function ReadTextFile(ByVal fileName As String, ByVal txtbxName As TextBox, ByVal errorLabel As Label)
        'read a text file from resources folder
        'txtbxName is textbox to display text
        'fileName is name of file to read, include file extension i.e. filename.txt
        'errorLabel is Label to send error message to 
        Dim fullPath As String
        errorLabel.Visible = False
        fullPath = Application.StartupPath & "\Resources\"
        'MsgBox(fullPath, , "Full Path to file folder")
        'MsgBox(fileName, , "File Name")
        Try
            'Open file, if not exist then create it
            Dim sFileName As String = fullPath & fileName
            'MsgBox(sFileName, , "File path")
            Dim myFileStream As New FileStream(sFileName, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read)
            ' create the stream reader
            Dim StreamToDisplay As New StreamReader(myFileStream)
            txtbxName.Text = StreamToDisplay.ReadToEnd
            StreamToDisplay.Close()
            myFileStream.Close()
            txtbxName.Select(0, 0)
        Catch ex As Exception
            MsgBox(ex.Message)
            errorLabel.Text = "ERROR:  " & ex.Message
            errorLabel.ForeColor = Color.Red
            errorLabel.Visible = True
        End Try
        Return Nothing
    End Function

    Function WriteTextFile(ByVal fileName As String, ByVal txtbxName As TextBox, ByVal errorLabel As Label)
        'write to a text file in resources folder
        'txtbxName is textbox with text to write
        'fileName is name of file to write to, include file extension i.e. filename.txt
        'errorLabel is Label to send error message to 
        Dim fullPath As String
        errorLabel.Visible = False
        fullPath = Application.StartupPath & "\Resources\"
        'MsgBox(fullPath, , "Full Path to file folder")
        'MsgBox(fileName, , "File Name")
        Try
            'Open file, if not exist then create it
            Dim sFileName As String = fullPath & fileName
            Dim myFileStream As New FileStream(sFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)
            'create the Stream Writer
            Dim StreamToWrite As New StreamWriter(myFileStream)
            StreamToWrite.Write(txtbxName.Text)
            'Flush before we close
            StreamToWrite.Flush()
            'Close everything
            StreamToWrite.Close()
            myFileStream.Close()
            MsgBox("Update Successful")
            txtbxName.Select(0, 0)
        Catch ex As Exception
            MsgBox(ex.Message)
            errorLabel.Text = "ERROR:  " & ex.Message
            errorLabel.ForeColor = Color.Red
            errorLabel.Visible = True
        End Try
        Return Nothing
    End Function

    Function ReadTextFile2ComboBox(ByVal fileName As String, ByVal cmbxName As ComboBox, ByVal errorLabel As Label)
        'read a text file from resources folder
        'txtbxName is textbox to display text
        'fileName is name of file to read, include file extension i.e. filename.txt
        'errorLabel is Label to send error message to 
        Dim fullPath As String
        errorLabel.Visible = False
        fullPath = Application.StartupPath & "\Resources\"
        'MsgBox(fullPath, , "Full Path to file folder")
        'MsgBox(fileName, , "File Name")
        Try
            'Open file, if not exist then create it
            Dim sFileName As String = fullPath & fileName
            Dim myFileStream As New FileStream(sFileName, FileMode.Open, FileAccess.Read, FileShare.Read)
            'create the stream reader
            Dim StreamToDisplay As New StreamReader(myFileStream)
            ' Read file until no more data exists . i.e. BLANK LINE found
            Do While StreamToDisplay.Peek <> -1
                Dim LineIn As String = StreamToDisplay.ReadLine()
                cmbxName.Items.Add(LineIn)
            Loop
            StreamToDisplay.Close()
            myFileStream.Close()
            cmbxName.Select(0, 0)
        Catch ex As Exception
            MsgBox(ex.Message)
            errorLabel.Text = "ERROR:  " & ex.Message
            errorLabel.ForeColor = Color.Red
            errorLabel.Visible = True
        End Try
        Return Nothing
    End Function

    

    Public Function CurrentAge(ByVal BirthDate As Date) As Integer
        'PURPOSE: Calculates a person's age
        'PARAMETERS: BirthDate: the person's birthdate, in date format
        'RETURNS: The person's age as of the current date.
        Dim CurrentDate As Date = DateTime.Now
        Dim Age As Integer

        Age = DateDiff(DateInterval.Year, BirthDate, CurrentDate)
        Return Age
    End Function
    ''' <summary>
    ''' Creates a log note and adds it to the Enquiry.[field name] field
    ''' </summary>
    ''' <param name="eId">EnquiryId to save note to</param>
    ''' <param name="name">the person's login name</param>
    ''' <param name="Text">text to add</param>
    ''' <param name="Field">the database Enquiry.[field name] to insert note into</param>
    ''' <returns>boolean</returns>
    ''' <remarks></remarks>
    Public Function CreateLogNote(ByVal eId As Integer, ByVal name As String, ByVal text As String, ByVal field As String) As Boolean
        'does not current work properly????

        Dim FieldComments As String
        Dim DateStr As String
        Dim NewCommentStr As String
        Dim CommentStr As String
        'Get Connection String Settings
        Try
            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT  " & field & " FROM Enquiry WHERE  EnquiryId = " & eId
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim CommentsDataAdapter As New SqlDataAdapter(selectCommand)
            Dim CommentsDataSet As New DataSet
            Dim CommentsDataTable As New DataTable
            'dumps results into datatable CommentsDataTable
            CommentsDataAdapter.Fill(CommentsDataTable)
            'if no matching rows .....
            If CommentsDataTable.Rows.Count = 0 Then
                'MessageBox.Show("CreateLogNote failed, no data returned from database.")
                'clear the dataTable and the Connect information
                CommentsDataAdapter = Nothing
                CommentsDataTable.Clear()
                connection.Dispose()
                Return False
                'if there is a matching row
            ElseIf CommentsDataTable.Rows.Count = 1 Then
                'get LoanComments value
                Dim CommentsDataRow As DataRow = CommentsDataTable.Rows(0)
                'assign LoanComments to local variable LoanComments
                FieldComments = CommentsDataRow.Item(0)
                ' clear the dataTable and the Connect information
                CommentsDataAdapter = Nothing
                CommentsDataTable.Clear()
                connection.Close()
                'Create Date string
                DateStr = "-- " & DateTime.Now.ToString("f")
                'Add Date string to comments
                CommentStr = DateStr & " | " & name & vbCrLf & text & vbCrLf & vbCrLf
                'check loan comments from dataset exist
                If Not FieldComments = "" Then
                    NewCommentStr = CommentStr & FieldComments
                Else
                    NewCommentStr = CommentStr
                End If
                'setup Update Query
                Dim updateStatement As String = "UPDATE Enquiry SET " & field & " = '" & NewCommentStr & "' WHERE  EnquiryId = " & eId
                'MsgBox(updateStatement)
                Dim updateCommand As New SqlCommand(updateStatement, connection)
                connection.Open()
                Dim count As Integer = updateCommand.ExecuteNonQuery
                connection.Dispose()
                If count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                MessageBox.Show("CreateLogNote failed, multiple data returned from database.")
                ' clear the dataTable and the Connect information
                CommentsDataAdapter = Nothing
                CommentsDataTable.Clear()
                connection.Dispose()
                Return False
            End If

        Catch ex As Exception
            MsgBox("ERROR with CreateLogNote Function: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Archives Enquiry: Inserts info into Archive table then updates Archive table with copy of TrueTrackWorksheet.txt
    ''' </summary>
    ''' <param name="EnquiryIdVar"></param>
    ''' <param name="EnquiryPrintVar"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>This function only available from UI if "CurrentStatus" = EnquiryEnded or Declined or Withdrawn or Exported.</remarks>
    Public Function Archive_Enquiry(ByVal enquiryIdVar As Integer, ByVal enquiryPrintVar As String) As Boolean
        '####################
        '25/08/2017
        'Changed archive process to just activate a flag in Enquiry table, no further processing needed.
        '09/10/2017 Created new Archive Function in Util class [Util.NewArchiveEnquiry], this procedure is now redundant and can be removed.
        '#################### New archive code
        Dim thisCurrentStatusSetDate As Date
        Dim thisCurrentStatus As String
        'check enquiry exists in Enquiry table

        Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
        enquiryDt = enquiryTa.GetDataByEnquiryId(enquiryIdVar)
        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        If enquiryDt.Rows.Count = 0 Then
            MessageBox.Show("Enquiry does not exist", "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        ElseIf enquiryDt.Rows.Count > 0 Then
            enquiryRow = enquiryDt.Rows(0)
            thisCurrentStatusSetDate = enquiryRow.CurrentStatusSetDate
            thisCurrentStatus = enquiryRow.CurrentStatus
            'test CurrentStatusSetDate is 10 days old Or Exported
            Dim dD As Long = DateDiff(DateInterval.Day, thisCurrentStatusSetDate, Date.Now)
            'If dD > 9 Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then 'Exported
            If dD > 9 Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Then
                'If dD > 9 Then 'Use when not auto archiving exported enquiries
                'OK to carryon
                enquiryRow = enquiryDt.Rows(0)
                enquiryRow.IsArchived = True
                enquiryTa.Update(enquiryDt)
                Return True
            Else
                'Not enough time since last Status set to Archive
                MessageBox.Show("Enquiry Status is not 10 days old yet." & vbCrLf & "Try again later", "Archiving has been cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
            End If

        End If

        '#################### End New archive code


        'This function only available from UI if "CurrentStatus" = EnquiryEnded or Declined or Withdrawn or Exported.
        'PURPOSE: Archives Enquiry: Inserts info into Archive table then updates Archive table with copy of TrueTrackWorksheet.......txt
        'deletes info from various related enquiry tables        '
        'Searches Enquiry folder for pattern matches to 'Application' case insensitive, if finds files then
        'creates new folder in EnquiryFiles > Archives using EnquiryCode then copies the located files to this new directory
        'then deletes original folder
        'PARAMETERS:    EnquiryId, table reference 
        '               EnquiryPrint, the printout of the Enquiry notes to be inserted into the Archive table
        'RETURNS: a boolean on completion; True on success, False on failure
        'Test parameters
        'MsgBox("EnquiryId = " & EnquiryIdVar)
        'MsgBox("Enquiry Print:" & vbCrLf & EnquiryPrintVar)
        ''create local variables
        'Dim thisEnquiryCode As String
        'Dim thisCurrentStatusSetDate As Date
        'Dim thisCurrentStatus As String
        ''check enquiry exists in Enquiry table
        'Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
        'Dim connection As New SqlConnection(connectionString)
        'Dim selectStatement As String = "SELECT EnquiryCode, CurrentStatusSetDate, CurrentStatus FROM Enquiry WHERE  EnquiryId = " & enquiryIdVar
        'Dim selectCommand As New SqlCommand(selectStatement, connection)
        'Dim thisEnquiryDataAdapter As New SqlDataAdapter(selectCommand)
        ''Dim thisEnquiryDataSet As New DataSet
        'Dim thisEnquiryDataTable As New DataTable
        'Try
        '    'dumps results into datatable CommentsDataTable
        '    thisEnquiryDataAdapter.Fill(thisEnquiryDataTable)
        'Catch ex As Exception
        '    MsgBox("check enquiry exists in Enquiry table caused an error:" & vbCrLf & ex.Message)
        'End Try
        ''if no matching rows .....
        'If thisEnquiryDataTable.Rows.Count = 0 Then
        '    MessageBox.Show("Enquiry does not exist", "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    'clear the dataTable and the Connect information
        '    thisEnquiryDataAdapter = Nothing
        '    thisEnquiryDataTable.Clear()
        '    connection.Dispose()
        '    Return False
        '    'if there is a matching row
        'ElseIf thisEnquiryDataTable.Rows.Count = 1 Then
        '    'get EnquiryCode value
        '    Dim thisEnquiryDataRow As DataRow = thisEnquiryDataTable.Rows(0)
        '    thisEnquiryCode = thisEnquiryDataRow.Item(0)
        '    thisCurrentStatusSetDate = thisEnquiryDataRow.Item(1)
        '    thisCurrentStatus = thisEnquiryDataRow.Item(2)
        '    'clear the dataTable and the Connect information
        '    thisEnquiryDataAdapter = Nothing
        '    thisEnquiryDataTable.Clear()
        '    connection.Dispose()

        ''test CurrentStatusSetDate is 10 days old Or Exported
        'Dim dD As Long = DateDiff(DateInterval.Day, thisCurrentStatusSetDate, Date.Now)
        'If dD > 9 Or thisCurrentStatus = "Exported" Then
        '    'If dD > 9 Then 'Use when not auto archiving exported enquiries
        '    'OK to carryon

        ''create stored procedure call
        'Try
        '    Dim sqlconnection1 As New SqlConnection(connectionString)
        '    Dim cmd As New SqlCommand("ArchiveThisEnquiry", sqlconnection1)
        '    Dim returnValue As Integer
        '    cmd.CommandType = CommandType.StoredProcedure
        '    Dim myParmEnquiryId As SqlParameter = cmd.Parameters.Add("@EnquiryId", SqlDbType.Int)
        '    myParmEnquiryId.Value = enquiryIdVar
        '    Dim myParmEnquiryPrint As SqlParameter = cmd.Parameters.Add("@EnquirySummary", SqlDbType.VarChar)
        '    myParmEnquiryPrint.Value = enquiryPrintVar
        '    sqlconnection1.Open()
        '    returnValue = Convert.ToInt16(cmd.ExecuteScalar())
        '    sqlconnection1.Close()
        '    'compose return imformation
        '    Select Case returnValue
        '        Case Is = 0
        '            'Stored Procedure completed successfully
        '            'now search for Enquiry folder, if exists check for Application files, if exist make new folder under Archives 
        '            'and copy Application files across then delete original folder along with other files if exist.
        '            'Get String to WorkSheet Drive
        '            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
        '            Dim folderPath As String = worksheetDrive & "\" & thisEnquiryCode
        '            'Check directory exists
        '            If Directory.Exists(folderPath) Then
        '                'check for application files
        '                'create arrayList
        '                Dim moveFilesList As New ArrayList()
        '                Try
        '                    ''set match pattern
        '                    'Dim sPattern As String = "\AAPP_" '"application" ' Startwith APP_
        '                    'Dim sPattern2 As String = "\APW_" '(\A)Starts with PW_
        '                    'Dim sPattern3 As String = "\AID_" '(\A)Starts with ID_
        '                    'Dim sPattern4 As String = "\AClient" '(\A)Starts with Client

        '                    'create match pattern string array
        '                    Dim patterns(3) As String
        '                    patterns(0) = "\AAPP" '(\A)Starts APP
        '                    patterns(1) = "\APW" '(\A)Starts with PW
        '                    patterns(2) = "\AID" '(\A)Starts with ID
        '                    patterns(3) = "\AClient" '(\A)Starts with Client

        '                    'Create place to store files
        '                    Dim aFiles() As String 'array type string
        '                    aFiles = Directory.GetFiles(folderPath)
        '                    'loop through aFiles
        '                    For Each sfile As String In aFiles
        '                        'assign str array values
        '                        Dim finfo As New FileInfo(sfile)
        '                        If finfo.Exists Then
        '                            ''check pattern for Application (case insensitive)
        '                            'If System.Text.RegularExpressions.Regex.IsMatch(finfo.Name, sPattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase) Then
        '                            '    'we have a match list filename in arrayList
        '                            '    moveFilesList.Add(finfo.Name)
        '                            '    'check pattern for Application (case insensitive)
        '                            'ElseIf System.Text.RegularExpressions.Regex.IsMatch(finfo.Name, sPattern2, System.Text.RegularExpressions.RegexOptions.IgnoreCase) Then
        '                            '    'we have a match list filename in arrayList
        '                            '    moveFilesList.Add(finfo.Name)
        '                            'ElseIf System.Text.RegularExpressions.Regex.IsMatch(finfo.Name, sPattern3, System.Text.RegularExpressions.RegexOptions.IgnoreCase) Then
        '                            '    'we have a match list filename in arrayList
        '                            '    moveFilesList.Add(finfo.Name)
        '                            'ElseIf System.Text.RegularExpressions.Regex.IsMatch(finfo.Name, sPattern4, System.Text.RegularExpressions.RegexOptions.IgnoreCase) Then
        '                            '    'we have a match list filename in arrayList
        '                            '    moveFilesList.Add(finfo.Name)
        '                            'End If

        '                            'check patterns (case insensitive)
        '                            For Each pattern As String In patterns
        '                                If System.Text.RegularExpressions.Regex.IsMatch(finfo.Name, pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase) Then
        '                                    'we have a match list filename in arrayList
        '                                    moveFilesList.Add(finfo.Name)
        '                                    Exit For
        '                                End If
        '                            Next

        '                        End If
        '                    Next sfile
        '                Catch ex As Exception
        '                    MsgBox("Error listing files: " & vbCrLf & ex.Message)
        '                End Try
        '                'check if MoveFileList has items
        '                If moveFilesList.Count > 0 Then
        '                    'list files
        '                    Dim strFinal As String = ""
        '                    For Each arrayStr As String In moveFilesList
        '                        strFinal = strFinal & vbCrLf & arrayStr
        '                    Next
        '                    MessageBox.Show(strFinal, "Files to be moved", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '                    'create archive folder
        '                    'Get String to WorkSheet Archive Folder
        '                    Dim archiveFolder As String = Switch.GetWorksheetArchiveFolder
        '                    Dim aname As String = archiveFolder & "\" & thisEnquiryCode
        '                    'Determine whether the directory exists.
        '                    If Directory.Exists(aname) = False Then
        '                        Try
        '                            'create(Directory)
        '                            IO.Directory.CreateDirectory(aname)
        '                        Catch ex As DirectoryNotFoundException
        '                            ' Let the user know that the directory did not exist.
        '                            MessageBox.Show("Directory " + aname + " not found: " & vbCrLf & ex.Message, "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                        Catch ex As Exception
        '                            MessageBox.Show("Creating directory caused an error:" & vbCrLf & ex.Message, "Archive error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                        End Try
        '                    End If
        '                    If Directory.Exists(aname) = True Then
        '                        'loop through MoveFilesList and move files
        '                        Try
        '                            For Each fileStr As String In moveFilesList
        '                                My.Computer.FileSystem.MoveFile(folderPath & "\" & fileStr, aname & "\" & fileStr)
        '                            Next
        '                        Catch ex As FileNotFoundException
        '                            MessageBox.Show("The source file is not valid or does not exist: " & vbCrLf & ex.Message, "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                        Catch ex As Exception
        '                            MessageBox.Show("Moving file caused an error:" & vbCrLf & ex.Message, "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                        End Try
        '                        'delete original folder
        '                        If Directory.Exists(folderPath) = True Then
        '                            Try
        '                                Directory.Delete(folderPath, True)
        '                            Catch ex As Exception
        '                                MessageBox.Show("Deleting the Original folder caused an error:" & vbCrLf & ex.Message, "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                            End Try
        '                        End If
        '                        'archiving completed
        '                        'TODO: SetApplicationStatus in OAC
        '                    Else
        '                        MessageBox.Show("Directory " + aname + " not found" & vbCrLf & "Moving of Application files can not proceed", "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                    End If
        '                Else
        '                    'if no files in MoveFileList then delete original folder
        '                    If Directory.Exists(folderPath) = True Then
        '                        Try
        '                            Directory.Delete(folderPath, True)
        '                        Catch ex As Exception
        '                            MessageBox.Show("Deleting the Original folder caused an error:" & vbCrLf & ex.Message, "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                        End Try
        '                    End If
        '                End If
        '                MessageBox.Show("Archiving completed", "Archive Notice", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '                Return True
        '            Else
        '                MessageBox.Show("Source Directory: " & folderPath & vbCrLf & "Directory probably does not exist." & vbCrLf & _
        '                                "If you expected to Archive Application files from this folder" & vbCrLf & "then see your administrator." & _
        '                                vbCrLf & "Archiving completed.", _
        '                                "Directory not found", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '                Return True
        '            End If
        '        Case Is = 1
        '            MsgBox("Creating Archive failed")
        '            Return False
        '        Case Is = 2
        '            MsgBox("Archive already exists")
        '            Return False
        '        Case Is = 3
        '            MsgBox("Archive record not found")
        '            Return False
        '        Case Is = 4
        '            MsgBox("QRGListID exits in Enquiry table")
        '            Return False
        '        Case Else
        '            MsgBox("Result of archiving unknown")
        '            Return False
        '    End Select
        'Catch ex As Exception
        '    MsgBox("create stored procedure call caused an error:" & vbCrLf & ex.Message)
        'End Try
        'Else
        '    'Not enough time since last Status set to Archive
        '    MessageBox.Show("Enquiry Status is not 10 days old yet." & vbCrLf & "Try again later", "Archiving has been cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Return False
        'End If



    End Function



    ''' <summary>
    ''' Delete relevant table records from database and if exists delete Enquiry Folder on shared drive.
    ''' </summary>
    ''' <param name="EnquiryCodeVar">EnquiryCode</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Utilises Stored procedure in database.</remarks>
    Public Function Delete_Enquiry(ByVal enquiryCodeVar As String) As Boolean
        Dim ThisEnquiryId As Integer
        Try
            'check enquiry exists in Enquiry table
            Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString()
            Dim connection As New SqlConnection(connectionString)
            Dim selectStatement As String = "SELECT EnquiryId FROM Enquiry WHERE  EnquiryCode = '" & enquiryCodeVar & "'"
            Dim selectCommand As New SqlCommand(selectStatement, connection)
            Dim ThisEnquiryDataAdapter As New SqlDataAdapter(selectCommand)
            Dim ThisEnquiryDataSet As New DataSet
            Dim ThisEnquiryDataTable As New DataTable
            Try
                'dumps results into datatable DataTable
                ThisEnquiryDataAdapter.Fill(ThisEnquiryDataTable)
            Catch ex As Exception
                MsgBox("Checking Enquiry exists in Enquiry table caused an error:" & vbCrLf & ex.Message)
                Return False
            End Try
            'if no matching rows .....
            If ThisEnquiryDataTable.Rows.Count = 0 Then
                MsgBox("Enquiry does not exist.")
                ' clear the dataTable and the Connect information
                ThisEnquiryDataAdapter = Nothing
                ThisEnquiryDataTable.Clear()
                connection.Dispose()
                Return False
                'if there is a matching row
            ElseIf ThisEnquiryDataTable.Rows.Count = 1 Then
                'get EnquiryCode value
                Dim ThisEnquiryDataRow As DataRow = ThisEnquiryDataTable.Rows(0)
                ThisEnquiryId = ThisEnquiryDataRow.Item(0)
                ' clear the dataTable and the Connect information
                ThisEnquiryDataAdapter = Nothing
                ThisEnquiryDataTable.Clear()
                connection.Dispose()
                'create stored procedure call
                Try
                    Dim sqlconnection1 As New SqlConnection(connectionString)
                    sqlconnection1.Open()
                    Dim cmd As New SqlCommand("DeleteThisEnquiry", sqlconnection1)
                    'Dim returnValue As Integer
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim myParmEnquiryId As SqlParameter = cmd.Parameters.Add("@EnquiryId", SqlDbType.Int)
                    myParmEnquiryId.Value = ThisEnquiryId

                    'Create a SqlParameter object to hold the output parameter value
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    'IMPORTANT - must set Direction as ReturnValue
                    retValParam.Direction = ParameterDirection.ReturnValue
                    'Finally, add the parameter to the Command's Parameters collection
                    cmd.Parameters.Add(retValParam)
                    'Call the sproc...
                    Dim reader As SqlDataReader = cmd.ExecuteReader()
                    'Now you can grab the output parameter's value...
                    Dim returnValue As Integer = Convert.ToInt32(retValParam.Value)
                    sqlconnection1.Close()

                    'compare return information
                    Select Case returnValue
                        Case Is = 0
                            'Stored Procedure completed successfully
                            'now search for Enquiry folder, if exists then delete original folder along with other files if exist.
                            'Get String to WorkSheet Drive
                            Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
                            Dim folderPath As String = worksheetDrive & "\" & enquiryCodeVar
                            'Check directory exists
                            If Directory.Exists(folderPath) Then
                                Try
                                    Directory.Delete(folderPath, True)
                                Catch ex As Exception
                                    MsgBox("Deleting the Enquiry Folder caused an error:" & vbCrLf & ex.Message)
                                End Try
                            End If
                            Return True
                        Case Is = 1
                            MessageBox.Show("Deleting Enquiry failed", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Return False
                        Case Is = 2
                            MessageBox.Show("Enquiry does not exist", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return False
                        Case Else
                            MessageBox.Show("Unknown Error", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return False
                    End Select
                Catch ex As Exception
                    MsgBox("Deleting this Enquiry caused an error:" & vbCrLf & ex.Message)
                    Return False
                End Try
            End If
        Catch ex As Exception
            MsgBox("Function Delete_Enquiry caused an error:" & vbCrLf & ex.Message)
            Return False
        End Try

    End Function

    ''' <summary>
    ''' check file exists in target folder, if it does then rename and check again
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <param name="fileName"></param>
    ''' <param name="fileExt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateFileName(ByVal filePath As String, ByVal fileName As String, Optional ByVal fileExt As String = "") As String
        'check file exists in target folder
        'if it does then rename and check again
        Try
            If File.Exists(filePath & "\" & fileName) Then
                'rename
                Dim strRename As String = ""
                'get name less extension
                If Not fileExt = "" Then
                    strRename = fileName.Replace(fileExt, "")
                Else
                    strRename = fileName
                End If
                'Add suffix to strRename and add extension back on
                Dim i As Integer
                For i = 1 To 100
                    If Not File.Exists(filePath & "\" & strRename & "_" & i & fileExt) Then
                        strRename = strRename & "_" & i & fileExt
                        'move to the finPower Client Folder 
                        Return strRename
                        Exit For
                    End If
                Next
            Else
                'move to the finPower Client Folder 
                Return fileName
            End If

        Catch ex As Exception
            MessageBox.Show("Function ValidateFileName has an error" & vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
       
    End Function


    Public Function IsProcessOpen(ByVal name As String) As Boolean
        'PURPOSE: 'check (by name) if a windows process is running
        'PARAMETERS: name: the name of the process
        'RETURNS: Boolean.
        For Each clsProcess As Process In Process.GetProcesses
            If clsProcess.ProcessName.Contains(name) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Function UnArchive_Enquiry(ByVal enquiryIdVar As Integer) As Boolean
        '25/08/2017
        'deactivate a flag in Enquiry table
        Try
            'check enquiry exists in Enquiry table
            Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
            enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
            Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
            enquiryDt = enquiryTa.GetDataByEnquiryId(enquiryIdVar)
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            If enquiryDt.Rows.Count = 0 Then
                MessageBox.Show("Enquiry does not exist", "Un-Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            ElseIf enquiryDt.Rows.Count > 0 Then
                enquiryRow = enquiryDt.Rows(0)
                Dim isArchived As Boolean = If(IsDBNull(enquiryRow.Item("IsArchived")), False, enquiryRow.Item("IsArchived"))
                'check enquiry has been archived (IsArchived)
                If isArchived = True Then
                    enquiryRow.IsArchived = False
                    enquiryTa.Update(enquiryDt)
                    Return True
                Else
                    MessageBox.Show("Enquiry is not archived", "Un-Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                End If
            End If

        Catch ex As Exception
            'Not enough time since last Status set to Archive
            MessageBox.Show("Error in UnArchive_Enquiry:" & vbCrLf & ex.Message, "Error in function", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try



    End Function


End Module
