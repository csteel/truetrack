﻿Public Class StatusReportSelectionsForm
   
    Dim GroupBy As String
    Dim OrderBy As String
    Dim User As String = ""
    Dim Status As String = ""


    Private Sub StatusReportSelectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'loads data into the 'EnquiryWorkSheetDataSet.Users' table. 
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)
        'set default values
        Me.cmbxGroupBy.SelectedIndex = 0
        Me.cmbxOrderBy.SelectedIndex = 0


    End Sub

    Private Sub ckbxUsers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxUsers.CheckedChanged
        If ckbxUsers.CheckState = CheckState.Checked Then
            lstbxUsers.Enabled = True
        Else
            lstbxUsers.Enabled = False
        End If
    End Sub

    Private Sub ckbxStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxStatus.CheckedChanged
        If ckbxStatus.CheckState = CheckState.Checked Then
            lstbxStatus.Enabled = True
        Else
            lstbxStatus.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Cursor.Current = Cursors.WaitCursor
        'get values
        'get status if selected
        If lstbxStatus.Enabled = True Then
            Status = lstbxStatus.SelectedItem.ToString
        Else
            Status = ""
        End If
        'get users if selected
        If lstbxUsers.Enabled = True Then
            User = lstbxUsers.SelectedValue.ToString
            'MsgBox("Selected User = " + User)
        Else
            User = ""
        End If
        'get Group By
        GroupBy = cmbxGroupBy.SelectedItem.ToString
        'MsgBox("GroupBy = " & GroupBy)
        'get Order by
        OrderBy = cmbxOrderBy.SelectedItem.ToString
        Try
            'launch ReportStatusForm and pass Enquiry parameters
            Dim ReportStatusFrm As New ReportStatusForm
            ReportStatusFrm.ReportSelections_PassVariable(Status, GroupBy, OrderBy, User)
            ReportStatusFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportStatusForm: " + vbCrLf + ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

 
   
End Class