﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmKpiSumReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmKpiSumReport))
        Me.lblPageTitle = New System.Windows.Forms.Label()
        Me.gpbxDates = New System.Windows.Forms.GroupBox()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblStartdate = New System.Windows.Forms.Label()
        Me.btnRunReport = New System.Windows.Forms.Button()
        Me.gpbxRetail = New System.Windows.Forms.GroupBox()
        Me.dgvRetail = New System.Windows.Forms.DataGridView()
        Me.gpbxGeneral = New System.Windows.Forms.GroupBox()
        Me.lblClientsRelentValue = New System.Windows.Forms.Label()
        Me.lblAdRepliesValue = New System.Windows.Forms.Label()
        Me.lblClientsRelent = New System.Windows.Forms.Label()
        Me.lblAdReplies = New System.Windows.Forms.Label()
        Me.gpbxEnquiries = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblEnquiriesCL = New System.Windows.Forms.Label()
        Me.lblEnquiriesTotalValue = New System.Windows.Forms.Label()
        Me.lblEnquiriesFfValue = New System.Windows.Forms.Label()
        Me.lblTotalEnquiries = New System.Windows.Forms.Label()
        Me.lblEnquiriesClValue = New System.Windows.Forms.Label()
        Me.lblEnquiriesFf = New System.Windows.Forms.Label()
        Me.lblEnquiriesVL = New System.Windows.Forms.Label()
        Me.lblEnquiriesPL = New System.Windows.Forms.Label()
        Me.lblEnquiriesPlValue = New System.Windows.Forms.Label()
        Me.lblEnquiriesUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblEnquiriesVlValue = New System.Windows.Forms.Label()
        Me.lblEnquiriesUnsecured = New System.Windows.Forms.Label()
        Me.lblEnquiriesBL = New System.Windows.Forms.Label()
        Me.lblEnquiriesBlValue = New System.Windows.Forms.Label()
        Me.gpbxApplications = New System.Windows.Forms.GroupBox()
        Me.lblApplicationsFfValue = New System.Windows.Forms.Label()
        Me.lblApplicationsFf = New System.Windows.Forms.Label()
        Me.lblApplicationsTotalValue = New System.Windows.Forms.Label()
        Me.lblTotalApplications = New System.Windows.Forms.Label()
        Me.lblApplicationsUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblApplicationsUnsecured = New System.Windows.Forms.Label()
        Me.lblApplicationsBlValue = New System.Windows.Forms.Label()
        Me.lblApplicationsBL = New System.Windows.Forms.Label()
        Me.lblApplicationsVlValue = New System.Windows.Forms.Label()
        Me.lblApplicationsVL = New System.Windows.Forms.Label()
        Me.lblApplicationsClValue = New System.Windows.Forms.Label()
        Me.lblApplicationsCL = New System.Windows.Forms.Label()
        Me.lblApplicationsPlValue = New System.Windows.Forms.Label()
        Me.lblApplicationsPL = New System.Windows.Forms.Label()
        Me.gpbxWithdrawn = New System.Windows.Forms.GroupBox()
        Me.lblWithdrawnFfValue = New System.Windows.Forms.Label()
        Me.lblWithdrawnFf = New System.Windows.Forms.Label()
        Me.lblWithdrawnTotalValue = New System.Windows.Forms.Label()
        Me.lblTotalWithdrawn = New System.Windows.Forms.Label()
        Me.lblWithdrawnUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblWithdrawnUnsecured = New System.Windows.Forms.Label()
        Me.lblWithdrawnBlValue = New System.Windows.Forms.Label()
        Me.lblWithdrawnBL = New System.Windows.Forms.Label()
        Me.lblWithdrawnVlValue = New System.Windows.Forms.Label()
        Me.lblWithdrawnVL = New System.Windows.Forms.Label()
        Me.lblWithdrawnClValue = New System.Windows.Forms.Label()
        Me.lblWithdrawnCL = New System.Windows.Forms.Label()
        Me.lblWithdrawnPlValue = New System.Windows.Forms.Label()
        Me.lblWithdrawnPL = New System.Windows.Forms.Label()
        Me.gpbxDeclined = New System.Windows.Forms.GroupBox()
        Me.lblDeclinedFfValue = New System.Windows.Forms.Label()
        Me.lblDeclinedFf = New System.Windows.Forms.Label()
        Me.lblDeclinedTotalValue = New System.Windows.Forms.Label()
        Me.lblTotalDeclined = New System.Windows.Forms.Label()
        Me.lblDeclinedUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblDeclinedUnsecured = New System.Windows.Forms.Label()
        Me.lblDeclinedBlValue = New System.Windows.Forms.Label()
        Me.lblDeclinedBL = New System.Windows.Forms.Label()
        Me.lblDeclinedVlValue = New System.Windows.Forms.Label()
        Me.lblDeclinedVL = New System.Windows.Forms.Label()
        Me.lblDeclinedClValue = New System.Windows.Forms.Label()
        Me.lblDeclinedCL = New System.Windows.Forms.Label()
        Me.lblDeclinedPlValue = New System.Windows.Forms.Label()
        Me.lblDeclinedPL = New System.Windows.Forms.Label()
        Me.gpbxDd = New System.Windows.Forms.GroupBox()
        Me.lblDdFfValue = New System.Windows.Forms.Label()
        Me.lblDdFf = New System.Windows.Forms.Label()
        Me.lblDdTotalValue = New System.Windows.Forms.Label()
        Me.lblDdTotal = New System.Windows.Forms.Label()
        Me.lblDdUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblDdUnsecured = New System.Windows.Forms.Label()
        Me.lblDdBlValue = New System.Windows.Forms.Label()
        Me.lblDdBL = New System.Windows.Forms.Label()
        Me.lblDdVlValue = New System.Windows.Forms.Label()
        Me.lblDdVL = New System.Windows.Forms.Label()
        Me.lblDdClValue = New System.Windows.Forms.Label()
        Me.lblDdCL = New System.Windows.Forms.Label()
        Me.lblDdPlValue = New System.Windows.Forms.Label()
        Me.lblDdPL = New System.Windows.Forms.Label()
        Me.gpbxDrawdown = New System.Windows.Forms.GroupBox()
        Me.lblDrawdownTotalValue = New System.Windows.Forms.Label()
        Me.lblDrawdownTotalNum = New System.Windows.Forms.Label()
        Me.lblDrawdownTotal = New System.Windows.Forms.Label()
        Me.lblDrawdownUnsecuredNum = New System.Windows.Forms.Label()
        Me.lblDrawdownBlNum = New System.Windows.Forms.Label()
        Me.lblDrawdownVlNum = New System.Windows.Forms.Label()
        Me.lblDrawdownClNum = New System.Windows.Forms.Label()
        Me.lblDrawdownPlNum = New System.Windows.Forms.Label()
        Me.lblDrawdownUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblDrawdownUnsecured = New System.Windows.Forms.Label()
        Me.lblDrawdownBlValue = New System.Windows.Forms.Label()
        Me.lblDrawdownBL = New System.Windows.Forms.Label()
        Me.lblDrawdownVlValue = New System.Windows.Forms.Label()
        Me.lblDrawdownVL = New System.Windows.Forms.Label()
        Me.lblDrawdownClValue = New System.Windows.Forms.Label()
        Me.lblDrawdownCL = New System.Windows.Forms.Label()
        Me.lblDrawdownPlValue = New System.Windows.Forms.Label()
        Me.lblDrawdownPL = New System.Windows.Forms.Label()
        Me.gpbxdateRange = New System.Windows.Forms.GroupBox()
        Me.cmbxEndYear = New System.Windows.Forms.ComboBox()
        Me.lblEndYear = New System.Windows.Forms.Label()
        Me.cmbxEndMonth = New System.Windows.Forms.ComboBox()
        Me.cmbxStartYear = New System.Windows.Forms.ComboBox()
        Me.lblStartYear = New System.Windows.Forms.Label()
        Me.cmbxStartMonth = New System.Windows.Forms.ComboBox()
        Me.lblEndMonth = New System.Windows.Forms.Label()
        Me.lblStartMonth = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblMonthTotalNum = New System.Windows.Forms.Label()
        Me.lblMonthTotalValue = New System.Windows.Forms.Label()
        Me.lblMonthTotal = New System.Windows.Forms.Label()
        Me.lblMonthUnsecuredNum = New System.Windows.Forms.Label()
        Me.lblMonthBlNum = New System.Windows.Forms.Label()
        Me.lblMonthVlNum = New System.Windows.Forms.Label()
        Me.lblMonthClNum = New System.Windows.Forms.Label()
        Me.lblMonthPlNum = New System.Windows.Forms.Label()
        Me.lblMonthUnsecuredValue = New System.Windows.Forms.Label()
        Me.lblMonthUnsecured = New System.Windows.Forms.Label()
        Me.lblMonthBlValue = New System.Windows.Forms.Label()
        Me.lblMonthBl = New System.Windows.Forms.Label()
        Me.lblMonthVlValue = New System.Windows.Forms.Label()
        Me.lblMonthVl = New System.Windows.Forms.Label()
        Me.lblMonthClValue = New System.Windows.Forms.Label()
        Me.lblMonthCl = New System.Windows.Forms.Label()
        Me.lblMonthPlValue = New System.Windows.Forms.Label()
        Me.lblMonthPl = New System.Windows.Forms.Label()
        Me.btnExcel = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.pbReportInformation = New System.Windows.Forms.PictureBox()
        Me.gpbxDates.SuspendLayout()
        Me.gpbxRetail.SuspendLayout()
        CType(Me.dgvRetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxGeneral.SuspendLayout()
        Me.gpbxEnquiries.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gpbxApplications.SuspendLayout()
        Me.gpbxWithdrawn.SuspendLayout()
        Me.gpbxDeclined.SuspendLayout()
        Me.gpbxDd.SuspendLayout()
        Me.gpbxDrawdown.SuspendLayout()
        Me.gpbxdateRange.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.pbReportInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPageTitle
        '
        Me.lblPageTitle.AutoSize = True
        Me.lblPageTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPageTitle.Location = New System.Drawing.Point(231, 6)
        Me.lblPageTitle.Name = "lblPageTitle"
        Me.lblPageTitle.Size = New System.Drawing.Size(123, 13)
        Me.lblPageTitle.TabIndex = 0
        Me.lblPageTitle.Text = "KPI Summary Report"
        '
        'gpbxDates
        '
        Me.gpbxDates.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxDates.Controls.Add(Me.dtpEndDate)
        Me.gpbxDates.Controls.Add(Me.lblEndDate)
        Me.gpbxDates.Controls.Add(Me.dtpStartDate)
        Me.gpbxDates.Controls.Add(Me.lblStartdate)
        Me.gpbxDates.Location = New System.Drawing.Point(12, 24)
        Me.gpbxDates.Name = "gpbxDates"
        Me.gpbxDates.Size = New System.Drawing.Size(578, 47)
        Me.gpbxDates.TabIndex = 1
        Me.gpbxDates.TabStop = False
        Me.gpbxDates.Text = "Select dates"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Location = New System.Drawing.Point(340, 17)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(200, 20)
        Me.dtpEndDate.TabIndex = 3
        '
        'lblEndDate
        '
        Me.lblEndDate.AutoSize = True
        Me.lblEndDate.Location = New System.Drawing.Point(284, 20)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(50, 13)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End date"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Location = New System.Drawing.Point(66, 17)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(200, 20)
        Me.dtpStartDate.TabIndex = 1
        '
        'lblStartdate
        '
        Me.lblStartdate.AutoSize = True
        Me.lblStartdate.Location = New System.Drawing.Point(7, 20)
        Me.lblStartdate.Name = "lblStartdate"
        Me.lblStartdate.Size = New System.Drawing.Size(53, 13)
        Me.lblStartdate.TabIndex = 0
        Me.lblStartdate.Text = "Start date"
        '
        'btnRunReport
        '
        Me.btnRunReport.BackColor = System.Drawing.Color.YellowGreen
        Me.btnRunReport.Location = New System.Drawing.Point(151, 159)
        Me.btnRunReport.Name = "btnRunReport"
        Me.btnRunReport.Size = New System.Drawing.Size(302, 23)
        Me.btnRunReport.TabIndex = 2
        Me.btnRunReport.Text = "Run Report"
        Me.btnRunReport.UseVisualStyleBackColor = False
        '
        'gpbxRetail
        '
        Me.gpbxRetail.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxRetail.Controls.Add(Me.dgvRetail)
        Me.gpbxRetail.Location = New System.Drawing.Point(12, 188)
        Me.gpbxRetail.Name = "gpbxRetail"
        Me.gpbxRetail.Size = New System.Drawing.Size(302, 227)
        Me.gpbxRetail.TabIndex = 3
        Me.gpbxRetail.TabStop = False
        Me.gpbxRetail.Text = "Retail Enquiries"
        '
        'dgvRetail
        '
        Me.dgvRetail.AllowUserToAddRows = False
        Me.dgvRetail.AllowUserToDeleteRows = False
        Me.dgvRetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRetail.Location = New System.Drawing.Point(7, 20)
        Me.dgvRetail.Name = "dgvRetail"
        Me.dgvRetail.ReadOnly = True
        Me.dgvRetail.RowHeadersWidth = 10
        Me.dgvRetail.Size = New System.Drawing.Size(289, 201)
        Me.dgvRetail.TabIndex = 0
        '
        'gpbxGeneral
        '
        Me.gpbxGeneral.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxGeneral.Controls.Add(Me.lblClientsRelentValue)
        Me.gpbxGeneral.Controls.Add(Me.lblAdRepliesValue)
        Me.gpbxGeneral.Controls.Add(Me.lblClientsRelent)
        Me.gpbxGeneral.Controls.Add(Me.lblAdReplies)
        Me.gpbxGeneral.Location = New System.Drawing.Point(321, 188)
        Me.gpbxGeneral.Name = "gpbxGeneral"
        Me.gpbxGeneral.Size = New System.Drawing.Size(269, 67)
        Me.gpbxGeneral.TabIndex = 4
        Me.gpbxGeneral.TabStop = False
        Me.gpbxGeneral.Text = "General"
        '
        'lblClientsRelentValue
        '
        Me.lblClientsRelentValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblClientsRelentValue.AutoSize = True
        Me.lblClientsRelentValue.Location = New System.Drawing.Point(192, 37)
        Me.lblClientsRelentValue.Name = "lblClientsRelentValue"
        Me.lblClientsRelentValue.Size = New System.Drawing.Size(16, 13)
        Me.lblClientsRelentValue.TabIndex = 3
        Me.lblClientsRelentValue.Text = "..."
        '
        'lblAdRepliesValue
        '
        Me.lblAdRepliesValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAdRepliesValue.AutoSize = True
        Me.lblAdRepliesValue.Location = New System.Drawing.Point(192, 20)
        Me.lblAdRepliesValue.Name = "lblAdRepliesValue"
        Me.lblAdRepliesValue.Size = New System.Drawing.Size(16, 13)
        Me.lblAdRepliesValue.TabIndex = 2
        Me.lblAdRepliesValue.Text = "..."
        '
        'lblClientsRelent
        '
        Me.lblClientsRelent.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblClientsRelent.AutoSize = True
        Me.lblClientsRelent.Location = New System.Drawing.Point(48, 37)
        Me.lblClientsRelent.Name = "lblClientsRelent"
        Me.lblClientsRelent.Size = New System.Drawing.Size(125, 13)
        Me.lblClientsRelent.TabIndex = 1
        Me.lblClientsRelent.Text = "Prospective clients relent"
        '
        'lblAdReplies
        '
        Me.lblAdReplies.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAdReplies.AutoSize = True
        Me.lblAdReplies.Location = New System.Drawing.Point(72, 20)
        Me.lblAdReplies.Name = "lblAdReplies"
        Me.lblAdReplies.Size = New System.Drawing.Size(101, 13)
        Me.lblAdReplies.TabIndex = 0
        Me.lblAdReplies.Text = "Advertisment replies"
        '
        'gpbxEnquiries
        '
        Me.gpbxEnquiries.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxEnquiries.Controls.Add(Me.TableLayoutPanel1)
        Me.gpbxEnquiries.Location = New System.Drawing.Point(321, 285)
        Me.gpbxEnquiries.Name = "gpbxEnquiries"
        Me.gpbxEnquiries.Size = New System.Drawing.Size(270, 130)
        Me.gpbxEnquiries.TabIndex = 5
        Me.gpbxEnquiries.TabStop = False
        Me.gpbxEnquiries.Text = "Enquiries"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesCL, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesTotalValue, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesFfValue, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.lblTotalEnquiries, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesClValue, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesFf, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesVL, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesPL, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesPlValue, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesUnsecuredValue, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesVlValue, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesUnsecured, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesBL, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblEnquiriesBlValue, 1, 3)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 14)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(226, 113)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'lblEnquiriesCL
        '
        Me.lblEnquiriesCL.AutoSize = True
        Me.lblEnquiriesCL.Location = New System.Drawing.Point(3, 16)
        Me.lblEnquiriesCL.Name = "lblEnquiriesCL"
        Me.lblEnquiriesCL.Size = New System.Drawing.Size(81, 13)
        Me.lblEnquiriesCL.TabIndex = 2
        Me.lblEnquiriesCL.Text = "Consumer Loan"
        '
        'lblEnquiriesTotalValue
        '
        Me.lblEnquiriesTotalValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesTotalValue.AutoSize = True
        Me.lblEnquiriesTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnquiriesTotalValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesTotalValue.Location = New System.Drawing.Point(152, 96)
        Me.lblEnquiriesTotalValue.Name = "lblEnquiriesTotalValue"
        Me.lblEnquiriesTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblEnquiriesTotalValue.TabIndex = 11
        Me.lblEnquiriesTotalValue.Text = "Total value"
        '
        'lblEnquiriesFfValue
        '
        Me.lblEnquiriesFfValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesFfValue.AutoSize = True
        Me.lblEnquiriesFfValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesFfValue.Location = New System.Drawing.Point(175, 80)
        Me.lblEnquiriesFfValue.Name = "lblEnquiriesFfValue"
        Me.lblEnquiriesFfValue.Size = New System.Drawing.Size(48, 13)
        Me.lblEnquiriesFfValue.TabIndex = 13
        Me.lblEnquiriesFfValue.Text = "FF value"
        '
        'lblTotalEnquiries
        '
        Me.lblTotalEnquiries.AutoSize = True
        Me.lblTotalEnquiries.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalEnquiries.Location = New System.Drawing.Point(3, 96)
        Me.lblTotalEnquiries.Name = "lblTotalEnquiries"
        Me.lblTotalEnquiries.Size = New System.Drawing.Size(92, 13)
        Me.lblTotalEnquiries.TabIndex = 10
        Me.lblTotalEnquiries.Text = "Total Enquiries"
        '
        'lblEnquiriesClValue
        '
        Me.lblEnquiriesClValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesClValue.AutoSize = True
        Me.lblEnquiriesClValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesClValue.Location = New System.Drawing.Point(174, 16)
        Me.lblEnquiriesClValue.Name = "lblEnquiriesClValue"
        Me.lblEnquiriesClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblEnquiriesClValue.TabIndex = 3
        Me.lblEnquiriesClValue.Text = "CL value"
        '
        'lblEnquiriesFf
        '
        Me.lblEnquiriesFf.AutoSize = True
        Me.lblEnquiriesFf.Location = New System.Drawing.Point(3, 80)
        Me.lblEnquiriesFf.Name = "lblEnquiriesFf"
        Me.lblEnquiriesFf.Size = New System.Drawing.Size(80, 13)
        Me.lblEnquiriesFf.TabIndex = 12
        Me.lblEnquiriesFf.Text = "Finance Facility"
        '
        'lblEnquiriesVL
        '
        Me.lblEnquiriesVL.AutoSize = True
        Me.lblEnquiriesVL.Location = New System.Drawing.Point(3, 32)
        Me.lblEnquiriesVL.Name = "lblEnquiriesVL"
        Me.lblEnquiriesVL.Size = New System.Drawing.Size(69, 13)
        Me.lblEnquiriesVL.TabIndex = 4
        Me.lblEnquiriesVL.Text = "Vehicle Loan"
        '
        'lblEnquiriesPL
        '
        Me.lblEnquiriesPL.AutoSize = True
        Me.lblEnquiriesPL.Location = New System.Drawing.Point(3, 0)
        Me.lblEnquiriesPL.Name = "lblEnquiriesPL"
        Me.lblEnquiriesPL.Size = New System.Drawing.Size(75, 13)
        Me.lblEnquiriesPL.TabIndex = 0
        Me.lblEnquiriesPL.Text = "Personal Loan"
        '
        'lblEnquiriesPlValue
        '
        Me.lblEnquiriesPlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesPlValue.AutoSize = True
        Me.lblEnquiriesPlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesPlValue.Location = New System.Drawing.Point(174, 0)
        Me.lblEnquiriesPlValue.Name = "lblEnquiriesPlValue"
        Me.lblEnquiriesPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblEnquiriesPlValue.TabIndex = 1
        Me.lblEnquiriesPlValue.Text = "PL value"
        '
        'lblEnquiriesUnsecuredValue
        '
        Me.lblEnquiriesUnsecuredValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesUnsecuredValue.AutoSize = True
        Me.lblEnquiriesUnsecuredValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesUnsecuredValue.Location = New System.Drawing.Point(135, 64)
        Me.lblEnquiriesUnsecuredValue.Name = "lblEnquiriesUnsecuredValue"
        Me.lblEnquiriesUnsecuredValue.Size = New System.Drawing.Size(88, 13)
        Me.lblEnquiriesUnsecuredValue.TabIndex = 9
        Me.lblEnquiriesUnsecuredValue.Text = "Unsecured value"
        '
        'lblEnquiriesVlValue
        '
        Me.lblEnquiriesVlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesVlValue.AutoSize = True
        Me.lblEnquiriesVlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesVlValue.Location = New System.Drawing.Point(174, 32)
        Me.lblEnquiriesVlValue.Name = "lblEnquiriesVlValue"
        Me.lblEnquiriesVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblEnquiriesVlValue.TabIndex = 5
        Me.lblEnquiriesVlValue.Text = "VL value"
        '
        'lblEnquiriesUnsecured
        '
        Me.lblEnquiriesUnsecured.AutoSize = True
        Me.lblEnquiriesUnsecured.Location = New System.Drawing.Point(3, 64)
        Me.lblEnquiriesUnsecured.Name = "lblEnquiriesUnsecured"
        Me.lblEnquiriesUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblEnquiriesUnsecured.TabIndex = 8
        Me.lblEnquiriesUnsecured.Text = "Unsecured Loan"
        '
        'lblEnquiriesBL
        '
        Me.lblEnquiriesBL.AutoSize = True
        Me.lblEnquiriesBL.Location = New System.Drawing.Point(3, 48)
        Me.lblEnquiriesBL.Name = "lblEnquiriesBL"
        Me.lblEnquiriesBL.Size = New System.Drawing.Size(76, 13)
        Me.lblEnquiriesBL.TabIndex = 6
        Me.lblEnquiriesBL.Text = "Business Loan"
        '
        'lblEnquiriesBlValue
        '
        Me.lblEnquiriesBlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEnquiriesBlValue.AutoSize = True
        Me.lblEnquiriesBlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEnquiriesBlValue.Location = New System.Drawing.Point(174, 48)
        Me.lblEnquiriesBlValue.Name = "lblEnquiriesBlValue"
        Me.lblEnquiriesBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblEnquiriesBlValue.TabIndex = 7
        Me.lblEnquiriesBlValue.Text = "BL value"
        '
        'gpbxApplications
        '
        Me.gpbxApplications.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsFfValue)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsFf)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsTotalValue)
        Me.gpbxApplications.Controls.Add(Me.lblTotalApplications)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsUnsecuredValue)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsUnsecured)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsBlValue)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsBL)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsVlValue)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsVL)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsClValue)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsCL)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsPlValue)
        Me.gpbxApplications.Controls.Add(Me.lblApplicationsPL)
        Me.gpbxApplications.Location = New System.Drawing.Point(12, 421)
        Me.gpbxApplications.Name = "gpbxApplications"
        Me.gpbxApplications.Size = New System.Drawing.Size(270, 130)
        Me.gpbxApplications.TabIndex = 7
        Me.gpbxApplications.TabStop = False
        Me.gpbxApplications.Text = "Applications"
        '
        'lblApplicationsFfValue
        '
        Me.lblApplicationsFfValue.AutoSize = True
        Me.lblApplicationsFfValue.Location = New System.Drawing.Point(126, 96)
        Me.lblApplicationsFfValue.Name = "lblApplicationsFfValue"
        Me.lblApplicationsFfValue.Size = New System.Drawing.Size(48, 13)
        Me.lblApplicationsFfValue.TabIndex = 15
        Me.lblApplicationsFfValue.Text = "FF value"
        '
        'lblApplicationsFf
        '
        Me.lblApplicationsFf.AutoSize = True
        Me.lblApplicationsFf.Location = New System.Drawing.Point(21, 96)
        Me.lblApplicationsFf.Name = "lblApplicationsFf"
        Me.lblApplicationsFf.Size = New System.Drawing.Size(80, 13)
        Me.lblApplicationsFf.TabIndex = 14
        Me.lblApplicationsFf.Text = "Finance Facility"
        Me.lblApplicationsFf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblApplicationsTotalValue
        '
        Me.lblApplicationsTotalValue.AutoSize = True
        Me.lblApplicationsTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationsTotalValue.Location = New System.Drawing.Point(127, 112)
        Me.lblApplicationsTotalValue.Name = "lblApplicationsTotalValue"
        Me.lblApplicationsTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblApplicationsTotalValue.TabIndex = 13
        Me.lblApplicationsTotalValue.Text = "Total value"
        '
        'lblTotalApplications
        '
        Me.lblTotalApplications.AutoSize = True
        Me.lblTotalApplications.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalApplications.Location = New System.Drawing.Point(1, 111)
        Me.lblTotalApplications.Name = "lblTotalApplications"
        Me.lblTotalApplications.Size = New System.Drawing.Size(109, 13)
        Me.lblTotalApplications.TabIndex = 12
        Me.lblTotalApplications.Text = "Total Applications"
        Me.lblTotalApplications.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblApplicationsUnsecuredValue
        '
        Me.lblApplicationsUnsecuredValue.AutoSize = True
        Me.lblApplicationsUnsecuredValue.Location = New System.Drawing.Point(127, 81)
        Me.lblApplicationsUnsecuredValue.Name = "lblApplicationsUnsecuredValue"
        Me.lblApplicationsUnsecuredValue.Size = New System.Drawing.Size(88, 13)
        Me.lblApplicationsUnsecuredValue.TabIndex = 9
        Me.lblApplicationsUnsecuredValue.Text = "Unsecured value"
        '
        'lblApplicationsUnsecured
        '
        Me.lblApplicationsUnsecured.AutoSize = True
        Me.lblApplicationsUnsecured.Location = New System.Drawing.Point(22, 81)
        Me.lblApplicationsUnsecured.Name = "lblApplicationsUnsecured"
        Me.lblApplicationsUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblApplicationsUnsecured.TabIndex = 8
        Me.lblApplicationsUnsecured.Text = "Unsecured Loan"
        Me.lblApplicationsUnsecured.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblApplicationsBlValue
        '
        Me.lblApplicationsBlValue.AutoSize = True
        Me.lblApplicationsBlValue.Location = New System.Drawing.Point(127, 66)
        Me.lblApplicationsBlValue.Name = "lblApplicationsBlValue"
        Me.lblApplicationsBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblApplicationsBlValue.TabIndex = 7
        Me.lblApplicationsBlValue.Text = "BL value"
        '
        'lblApplicationsBL
        '
        Me.lblApplicationsBL.AutoSize = True
        Me.lblApplicationsBL.Location = New System.Drawing.Point(32, 66)
        Me.lblApplicationsBL.Name = "lblApplicationsBL"
        Me.lblApplicationsBL.Size = New System.Drawing.Size(76, 13)
        Me.lblApplicationsBL.TabIndex = 6
        Me.lblApplicationsBL.Text = "Business Loan"
        Me.lblApplicationsBL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblApplicationsVlValue
        '
        Me.lblApplicationsVlValue.AutoSize = True
        Me.lblApplicationsVlValue.Location = New System.Drawing.Point(127, 51)
        Me.lblApplicationsVlValue.Name = "lblApplicationsVlValue"
        Me.lblApplicationsVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblApplicationsVlValue.TabIndex = 5
        Me.lblApplicationsVlValue.Text = "VL value"
        '
        'lblApplicationsVL
        '
        Me.lblApplicationsVL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblApplicationsVL.AutoSize = True
        Me.lblApplicationsVL.Location = New System.Drawing.Point(39, 51)
        Me.lblApplicationsVL.Name = "lblApplicationsVL"
        Me.lblApplicationsVL.Size = New System.Drawing.Size(69, 13)
        Me.lblApplicationsVL.TabIndex = 4
        Me.lblApplicationsVL.Text = "Vehicle Loan"
        Me.lblApplicationsVL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblApplicationsClValue
        '
        Me.lblApplicationsClValue.AutoSize = True
        Me.lblApplicationsClValue.Location = New System.Drawing.Point(127, 38)
        Me.lblApplicationsClValue.Name = "lblApplicationsClValue"
        Me.lblApplicationsClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblApplicationsClValue.TabIndex = 3
        Me.lblApplicationsClValue.Text = "CL value"
        '
        'lblApplicationsCL
        '
        Me.lblApplicationsCL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblApplicationsCL.AutoSize = True
        Me.lblApplicationsCL.Location = New System.Drawing.Point(27, 38)
        Me.lblApplicationsCL.Name = "lblApplicationsCL"
        Me.lblApplicationsCL.Size = New System.Drawing.Size(81, 13)
        Me.lblApplicationsCL.TabIndex = 2
        Me.lblApplicationsCL.Text = "Consumer Loan"
        Me.lblApplicationsCL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblApplicationsPlValue
        '
        Me.lblApplicationsPlValue.AutoSize = True
        Me.lblApplicationsPlValue.Location = New System.Drawing.Point(127, 25)
        Me.lblApplicationsPlValue.Name = "lblApplicationsPlValue"
        Me.lblApplicationsPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblApplicationsPlValue.TabIndex = 1
        Me.lblApplicationsPlValue.Text = "PL value"
        '
        'lblApplicationsPL
        '
        Me.lblApplicationsPL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblApplicationsPL.AutoSize = True
        Me.lblApplicationsPL.Location = New System.Drawing.Point(33, 25)
        Me.lblApplicationsPL.Name = "lblApplicationsPL"
        Me.lblApplicationsPL.Size = New System.Drawing.Size(75, 13)
        Me.lblApplicationsPL.TabIndex = 0
        Me.lblApplicationsPL.Text = "Personal Loan"
        Me.lblApplicationsPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbxWithdrawn
        '
        Me.gpbxWithdrawn.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnFfValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnFf)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnTotalValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblTotalWithdrawn)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnUnsecuredValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnUnsecured)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnBlValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnBL)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnVlValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnVL)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnClValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnCL)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnPlValue)
        Me.gpbxWithdrawn.Controls.Add(Me.lblWithdrawnPL)
        Me.gpbxWithdrawn.Location = New System.Drawing.Point(12, 557)
        Me.gpbxWithdrawn.Name = "gpbxWithdrawn"
        Me.gpbxWithdrawn.Size = New System.Drawing.Size(270, 130)
        Me.gpbxWithdrawn.TabIndex = 8
        Me.gpbxWithdrawn.TabStop = False
        Me.gpbxWithdrawn.Text = "Withdrawn"
        '
        'lblWithdrawnFfValue
        '
        Me.lblWithdrawnFfValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnFfValue.AutoSize = True
        Me.lblWithdrawnFfValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnFfValue.Location = New System.Drawing.Point(141, 96)
        Me.lblWithdrawnFfValue.Name = "lblWithdrawnFfValue"
        Me.lblWithdrawnFfValue.Size = New System.Drawing.Size(48, 13)
        Me.lblWithdrawnFfValue.TabIndex = 15
        Me.lblWithdrawnFfValue.Text = "FF value"
        '
        'lblWithdrawnFf
        '
        Me.lblWithdrawnFf.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnFf.AutoSize = True
        Me.lblWithdrawnFf.Location = New System.Drawing.Point(19, 96)
        Me.lblWithdrawnFf.Name = "lblWithdrawnFf"
        Me.lblWithdrawnFf.Size = New System.Drawing.Size(80, 13)
        Me.lblWithdrawnFf.TabIndex = 14
        Me.lblWithdrawnFf.Text = "Finance Facility"
        '
        'lblWithdrawnTotalValue
        '
        Me.lblWithdrawnTotalValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnTotalValue.AutoSize = True
        Me.lblWithdrawnTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWithdrawnTotalValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnTotalValue.Location = New System.Drawing.Point(141, 112)
        Me.lblWithdrawnTotalValue.Name = "lblWithdrawnTotalValue"
        Me.lblWithdrawnTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblWithdrawnTotalValue.TabIndex = 13
        Me.lblWithdrawnTotalValue.Text = "Total value"
        '
        'lblTotalWithdrawn
        '
        Me.lblTotalWithdrawn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotalWithdrawn.AutoSize = True
        Me.lblTotalWithdrawn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalWithdrawn.Location = New System.Drawing.Point(6, 111)
        Me.lblTotalWithdrawn.Name = "lblTotalWithdrawn"
        Me.lblTotalWithdrawn.Size = New System.Drawing.Size(100, 13)
        Me.lblTotalWithdrawn.TabIndex = 12
        Me.lblTotalWithdrawn.Text = "Total Withdrawn"
        '
        'lblWithdrawnUnsecuredValue
        '
        Me.lblWithdrawnUnsecuredValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnUnsecuredValue.AutoSize = True
        Me.lblWithdrawnUnsecuredValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnUnsecuredValue.Location = New System.Drawing.Point(141, 81)
        Me.lblWithdrawnUnsecuredValue.Name = "lblWithdrawnUnsecuredValue"
        Me.lblWithdrawnUnsecuredValue.Size = New System.Drawing.Size(88, 13)
        Me.lblWithdrawnUnsecuredValue.TabIndex = 9
        Me.lblWithdrawnUnsecuredValue.Text = "Unsecured value"
        '
        'lblWithdrawnUnsecured
        '
        Me.lblWithdrawnUnsecured.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnUnsecured.AutoSize = True
        Me.lblWithdrawnUnsecured.Location = New System.Drawing.Point(19, 81)
        Me.lblWithdrawnUnsecured.Name = "lblWithdrawnUnsecured"
        Me.lblWithdrawnUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblWithdrawnUnsecured.TabIndex = 8
        Me.lblWithdrawnUnsecured.Text = "Unsecured Loan"
        '
        'lblWithdrawnBlValue
        '
        Me.lblWithdrawnBlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnBlValue.AutoSize = True
        Me.lblWithdrawnBlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnBlValue.Location = New System.Drawing.Point(141, 66)
        Me.lblWithdrawnBlValue.Name = "lblWithdrawnBlValue"
        Me.lblWithdrawnBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblWithdrawnBlValue.TabIndex = 7
        Me.lblWithdrawnBlValue.Text = "BL value"
        '
        'lblWithdrawnBL
        '
        Me.lblWithdrawnBL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnBL.AutoSize = True
        Me.lblWithdrawnBL.Location = New System.Drawing.Point(29, 66)
        Me.lblWithdrawnBL.Name = "lblWithdrawnBL"
        Me.lblWithdrawnBL.Size = New System.Drawing.Size(76, 13)
        Me.lblWithdrawnBL.TabIndex = 6
        Me.lblWithdrawnBL.Text = "Business Loan"
        '
        'lblWithdrawnVlValue
        '
        Me.lblWithdrawnVlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnVlValue.AutoSize = True
        Me.lblWithdrawnVlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnVlValue.Location = New System.Drawing.Point(141, 51)
        Me.lblWithdrawnVlValue.Name = "lblWithdrawnVlValue"
        Me.lblWithdrawnVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblWithdrawnVlValue.TabIndex = 5
        Me.lblWithdrawnVlValue.Text = "VL value"
        '
        'lblWithdrawnVL
        '
        Me.lblWithdrawnVL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnVL.AutoSize = True
        Me.lblWithdrawnVL.Location = New System.Drawing.Point(36, 51)
        Me.lblWithdrawnVL.Name = "lblWithdrawnVL"
        Me.lblWithdrawnVL.Size = New System.Drawing.Size(69, 13)
        Me.lblWithdrawnVL.TabIndex = 4
        Me.lblWithdrawnVL.Text = "Vehicle Loan"
        Me.lblWithdrawnVL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWithdrawnClValue
        '
        Me.lblWithdrawnClValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnClValue.AutoSize = True
        Me.lblWithdrawnClValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnClValue.Location = New System.Drawing.Point(141, 38)
        Me.lblWithdrawnClValue.Name = "lblWithdrawnClValue"
        Me.lblWithdrawnClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblWithdrawnClValue.TabIndex = 3
        Me.lblWithdrawnClValue.Text = "CL value"
        '
        'lblWithdrawnCL
        '
        Me.lblWithdrawnCL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnCL.AutoSize = True
        Me.lblWithdrawnCL.Location = New System.Drawing.Point(24, 38)
        Me.lblWithdrawnCL.Name = "lblWithdrawnCL"
        Me.lblWithdrawnCL.Size = New System.Drawing.Size(81, 13)
        Me.lblWithdrawnCL.TabIndex = 2
        Me.lblWithdrawnCL.Text = "Consumer Loan"
        Me.lblWithdrawnCL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWithdrawnPlValue
        '
        Me.lblWithdrawnPlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnPlValue.AutoSize = True
        Me.lblWithdrawnPlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblWithdrawnPlValue.Location = New System.Drawing.Point(141, 25)
        Me.lblWithdrawnPlValue.Name = "lblWithdrawnPlValue"
        Me.lblWithdrawnPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblWithdrawnPlValue.TabIndex = 1
        Me.lblWithdrawnPlValue.Text = "PL value"
        '
        'lblWithdrawnPL
        '
        Me.lblWithdrawnPL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWithdrawnPL.AutoSize = True
        Me.lblWithdrawnPL.Location = New System.Drawing.Point(30, 25)
        Me.lblWithdrawnPL.Name = "lblWithdrawnPL"
        Me.lblWithdrawnPL.Size = New System.Drawing.Size(75, 13)
        Me.lblWithdrawnPL.TabIndex = 0
        Me.lblWithdrawnPL.Text = "Personal Loan"
        Me.lblWithdrawnPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbxDeclined
        '
        Me.gpbxDeclined.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedFfValue)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedFf)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedTotalValue)
        Me.gpbxDeclined.Controls.Add(Me.lblTotalDeclined)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedUnsecuredValue)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedUnsecured)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedBlValue)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedBL)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedVlValue)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedVL)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedClValue)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedCL)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedPlValue)
        Me.gpbxDeclined.Controls.Add(Me.lblDeclinedPL)
        Me.gpbxDeclined.Location = New System.Drawing.Point(321, 557)
        Me.gpbxDeclined.Name = "gpbxDeclined"
        Me.gpbxDeclined.Size = New System.Drawing.Size(270, 130)
        Me.gpbxDeclined.TabIndex = 9
        Me.gpbxDeclined.TabStop = False
        Me.gpbxDeclined.Text = "Declined"
        '
        'lblDeclinedFfValue
        '
        Me.lblDeclinedFfValue.AutoSize = True
        Me.lblDeclinedFfValue.Location = New System.Drawing.Point(126, 96)
        Me.lblDeclinedFfValue.Name = "lblDeclinedFfValue"
        Me.lblDeclinedFfValue.Size = New System.Drawing.Size(48, 13)
        Me.lblDeclinedFfValue.TabIndex = 17
        Me.lblDeclinedFfValue.Text = "FF value"
        '
        'lblDeclinedFf
        '
        Me.lblDeclinedFf.AutoSize = True
        Me.lblDeclinedFf.Location = New System.Drawing.Point(21, 96)
        Me.lblDeclinedFf.Name = "lblDeclinedFf"
        Me.lblDeclinedFf.Size = New System.Drawing.Size(80, 13)
        Me.lblDeclinedFf.TabIndex = 16
        Me.lblDeclinedFf.Text = "Finance Facility"
        Me.lblDeclinedFf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeclinedTotalValue
        '
        Me.lblDeclinedTotalValue.AutoSize = True
        Me.lblDeclinedTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeclinedTotalValue.Location = New System.Drawing.Point(127, 112)
        Me.lblDeclinedTotalValue.Name = "lblDeclinedTotalValue"
        Me.lblDeclinedTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblDeclinedTotalValue.TabIndex = 15
        Me.lblDeclinedTotalValue.Text = "Total value"
        '
        'lblTotalDeclined
        '
        Me.lblTotalDeclined.AutoSize = True
        Me.lblTotalDeclined.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalDeclined.Location = New System.Drawing.Point(19, 111)
        Me.lblTotalDeclined.Name = "lblTotalDeclined"
        Me.lblTotalDeclined.Size = New System.Drawing.Size(90, 13)
        Me.lblTotalDeclined.TabIndex = 14
        Me.lblTotalDeclined.Text = "Total Declined"
        Me.lblTotalDeclined.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeclinedUnsecuredValue
        '
        Me.lblDeclinedUnsecuredValue.AutoSize = True
        Me.lblDeclinedUnsecuredValue.Location = New System.Drawing.Point(127, 81)
        Me.lblDeclinedUnsecuredValue.Name = "lblDeclinedUnsecuredValue"
        Me.lblDeclinedUnsecuredValue.Size = New System.Drawing.Size(88, 13)
        Me.lblDeclinedUnsecuredValue.TabIndex = 9
        Me.lblDeclinedUnsecuredValue.Text = "Unsecured value"
        '
        'lblDeclinedUnsecured
        '
        Me.lblDeclinedUnsecured.AutoSize = True
        Me.lblDeclinedUnsecured.Location = New System.Drawing.Point(22, 81)
        Me.lblDeclinedUnsecured.Name = "lblDeclinedUnsecured"
        Me.lblDeclinedUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblDeclinedUnsecured.TabIndex = 8
        Me.lblDeclinedUnsecured.Text = "Unsecured Loan"
        Me.lblDeclinedUnsecured.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeclinedBlValue
        '
        Me.lblDeclinedBlValue.AutoSize = True
        Me.lblDeclinedBlValue.Location = New System.Drawing.Point(127, 66)
        Me.lblDeclinedBlValue.Name = "lblDeclinedBlValue"
        Me.lblDeclinedBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDeclinedBlValue.TabIndex = 7
        Me.lblDeclinedBlValue.Text = "BL value"
        '
        'lblDeclinedBL
        '
        Me.lblDeclinedBL.AutoSize = True
        Me.lblDeclinedBL.Location = New System.Drawing.Point(32, 66)
        Me.lblDeclinedBL.Name = "lblDeclinedBL"
        Me.lblDeclinedBL.Size = New System.Drawing.Size(76, 13)
        Me.lblDeclinedBL.TabIndex = 6
        Me.lblDeclinedBL.Text = "Business Loan"
        Me.lblDeclinedBL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeclinedVlValue
        '
        Me.lblDeclinedVlValue.AutoSize = True
        Me.lblDeclinedVlValue.Location = New System.Drawing.Point(127, 51)
        Me.lblDeclinedVlValue.Name = "lblDeclinedVlValue"
        Me.lblDeclinedVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDeclinedVlValue.TabIndex = 5
        Me.lblDeclinedVlValue.Text = "VL value"
        '
        'lblDeclinedVL
        '
        Me.lblDeclinedVL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDeclinedVL.AutoSize = True
        Me.lblDeclinedVL.Location = New System.Drawing.Point(39, 51)
        Me.lblDeclinedVL.Name = "lblDeclinedVL"
        Me.lblDeclinedVL.Size = New System.Drawing.Size(69, 13)
        Me.lblDeclinedVL.TabIndex = 4
        Me.lblDeclinedVL.Text = "Vehicle Loan"
        Me.lblDeclinedVL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeclinedClValue
        '
        Me.lblDeclinedClValue.AutoSize = True
        Me.lblDeclinedClValue.Location = New System.Drawing.Point(127, 38)
        Me.lblDeclinedClValue.Name = "lblDeclinedClValue"
        Me.lblDeclinedClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDeclinedClValue.TabIndex = 3
        Me.lblDeclinedClValue.Text = "CL value"
        '
        'lblDeclinedCL
        '
        Me.lblDeclinedCL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDeclinedCL.AutoSize = True
        Me.lblDeclinedCL.Location = New System.Drawing.Point(27, 38)
        Me.lblDeclinedCL.Name = "lblDeclinedCL"
        Me.lblDeclinedCL.Size = New System.Drawing.Size(81, 13)
        Me.lblDeclinedCL.TabIndex = 2
        Me.lblDeclinedCL.Text = "Consumer Loan"
        Me.lblDeclinedCL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDeclinedPlValue
        '
        Me.lblDeclinedPlValue.AutoSize = True
        Me.lblDeclinedPlValue.Location = New System.Drawing.Point(127, 25)
        Me.lblDeclinedPlValue.Name = "lblDeclinedPlValue"
        Me.lblDeclinedPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDeclinedPlValue.TabIndex = 1
        Me.lblDeclinedPlValue.Text = "PL value"
        '
        'lblDeclinedPL
        '
        Me.lblDeclinedPL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDeclinedPL.AutoSize = True
        Me.lblDeclinedPL.Location = New System.Drawing.Point(33, 25)
        Me.lblDeclinedPL.Name = "lblDeclinedPL"
        Me.lblDeclinedPL.Size = New System.Drawing.Size(75, 13)
        Me.lblDeclinedPL.TabIndex = 0
        Me.lblDeclinedPL.Text = "Personal Loan"
        Me.lblDeclinedPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbxDd
        '
        Me.gpbxDd.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxDd.Controls.Add(Me.lblDdFfValue)
        Me.gpbxDd.Controls.Add(Me.lblDdFf)
        Me.gpbxDd.Controls.Add(Me.lblDdTotalValue)
        Me.gpbxDd.Controls.Add(Me.lblDdTotal)
        Me.gpbxDd.Controls.Add(Me.lblDdUnsecuredValue)
        Me.gpbxDd.Controls.Add(Me.lblDdUnsecured)
        Me.gpbxDd.Controls.Add(Me.lblDdBlValue)
        Me.gpbxDd.Controls.Add(Me.lblDdBL)
        Me.gpbxDd.Controls.Add(Me.lblDdVlValue)
        Me.gpbxDd.Controls.Add(Me.lblDdVL)
        Me.gpbxDd.Controls.Add(Me.lblDdClValue)
        Me.gpbxDd.Controls.Add(Me.lblDdCL)
        Me.gpbxDd.Controls.Add(Me.lblDdPlValue)
        Me.gpbxDd.Controls.Add(Me.lblDdPL)
        Me.gpbxDd.Location = New System.Drawing.Point(321, 421)
        Me.gpbxDd.Name = "gpbxDd"
        Me.gpbxDd.Size = New System.Drawing.Size(270, 130)
        Me.gpbxDd.TabIndex = 10
        Me.gpbxDd.TabStop = False
        Me.gpbxDd.Text = "Pending"
        '
        'lblDdFfValue
        '
        Me.lblDdFfValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdFfValue.AutoSize = True
        Me.lblDdFfValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdFfValue.Location = New System.Drawing.Point(142, 96)
        Me.lblDdFfValue.Name = "lblDdFfValue"
        Me.lblDdFfValue.Size = New System.Drawing.Size(48, 13)
        Me.lblDdFfValue.TabIndex = 17
        Me.lblDdFfValue.Text = "FF value"
        '
        'lblDdFf
        '
        Me.lblDdFf.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdFf.AutoSize = True
        Me.lblDdFf.Location = New System.Drawing.Point(24, 96)
        Me.lblDdFf.Name = "lblDdFf"
        Me.lblDdFf.Size = New System.Drawing.Size(80, 13)
        Me.lblDdFf.TabIndex = 16
        Me.lblDdFf.Text = "Finance Facility"
        '
        'lblDdTotalValue
        '
        Me.lblDdTotalValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdTotalValue.AutoSize = True
        Me.lblDdTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDdTotalValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdTotalValue.Location = New System.Drawing.Point(141, 112)
        Me.lblDdTotalValue.Name = "lblDdTotalValue"
        Me.lblDdTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblDdTotalValue.TabIndex = 15
        Me.lblDdTotalValue.Text = "Total value"
        '
        'lblDdTotal
        '
        Me.lblDdTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdTotal.AutoSize = True
        Me.lblDdTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDdTotal.Location = New System.Drawing.Point(19, 111)
        Me.lblDdTotal.Name = "lblDdTotal"
        Me.lblDdTotal.Size = New System.Drawing.Size(86, 13)
        Me.lblDdTotal.TabIndex = 14
        Me.lblDdTotal.Text = "Total Pending"
        '
        'lblDdUnsecuredValue
        '
        Me.lblDdUnsecuredValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdUnsecuredValue.AutoSize = True
        Me.lblDdUnsecuredValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdUnsecuredValue.Location = New System.Drawing.Point(141, 81)
        Me.lblDdUnsecuredValue.Name = "lblDdUnsecuredValue"
        Me.lblDdUnsecuredValue.Size = New System.Drawing.Size(88, 13)
        Me.lblDdUnsecuredValue.TabIndex = 9
        Me.lblDdUnsecuredValue.Text = "Unsecured value"
        '
        'lblDdUnsecured
        '
        Me.lblDdUnsecured.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdUnsecured.AutoSize = True
        Me.lblDdUnsecured.Location = New System.Drawing.Point(19, 81)
        Me.lblDdUnsecured.Name = "lblDdUnsecured"
        Me.lblDdUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblDdUnsecured.TabIndex = 8
        Me.lblDdUnsecured.Text = "Unsecured Loan"
        '
        'lblDdBlValue
        '
        Me.lblDdBlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdBlValue.AutoSize = True
        Me.lblDdBlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdBlValue.Location = New System.Drawing.Point(141, 66)
        Me.lblDdBlValue.Name = "lblDdBlValue"
        Me.lblDdBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDdBlValue.TabIndex = 7
        Me.lblDdBlValue.Text = "BL value"
        '
        'lblDdBL
        '
        Me.lblDdBL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdBL.AutoSize = True
        Me.lblDdBL.Location = New System.Drawing.Point(29, 66)
        Me.lblDdBL.Name = "lblDdBL"
        Me.lblDdBL.Size = New System.Drawing.Size(76, 13)
        Me.lblDdBL.TabIndex = 6
        Me.lblDdBL.Text = "Business Loan"
        '
        'lblDdVlValue
        '
        Me.lblDdVlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdVlValue.AutoSize = True
        Me.lblDdVlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdVlValue.Location = New System.Drawing.Point(141, 51)
        Me.lblDdVlValue.Name = "lblDdVlValue"
        Me.lblDdVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDdVlValue.TabIndex = 5
        Me.lblDdVlValue.Text = "VL value"
        '
        'lblDdVL
        '
        Me.lblDdVL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdVL.AutoSize = True
        Me.lblDdVL.Location = New System.Drawing.Point(36, 51)
        Me.lblDdVL.Name = "lblDdVL"
        Me.lblDdVL.Size = New System.Drawing.Size(69, 13)
        Me.lblDdVL.TabIndex = 4
        Me.lblDdVL.Text = "Vehicle Loan"
        Me.lblDdVL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDdClValue
        '
        Me.lblDdClValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdClValue.AutoSize = True
        Me.lblDdClValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdClValue.Location = New System.Drawing.Point(141, 38)
        Me.lblDdClValue.Name = "lblDdClValue"
        Me.lblDdClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDdClValue.TabIndex = 3
        Me.lblDdClValue.Text = "CL value"
        '
        'lblDdCL
        '
        Me.lblDdCL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdCL.AutoSize = True
        Me.lblDdCL.Location = New System.Drawing.Point(24, 38)
        Me.lblDdCL.Name = "lblDdCL"
        Me.lblDdCL.Size = New System.Drawing.Size(81, 13)
        Me.lblDdCL.TabIndex = 2
        Me.lblDdCL.Text = "Consumer Loan"
        Me.lblDdCL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDdPlValue
        '
        Me.lblDdPlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdPlValue.AutoSize = True
        Me.lblDdPlValue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDdPlValue.Location = New System.Drawing.Point(141, 25)
        Me.lblDdPlValue.Name = "lblDdPlValue"
        Me.lblDdPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDdPlValue.TabIndex = 1
        Me.lblDdPlValue.Text = "PL value"
        '
        'lblDdPL
        '
        Me.lblDdPL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDdPL.AutoSize = True
        Me.lblDdPL.Location = New System.Drawing.Point(30, 25)
        Me.lblDdPL.Name = "lblDdPL"
        Me.lblDdPL.Size = New System.Drawing.Size(75, 13)
        Me.lblDdPL.TabIndex = 0
        Me.lblDdPL.Text = "Personal Loan"
        Me.lblDdPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbxDrawdown
        '
        Me.gpbxDrawdown.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownTotalValue)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownTotalNum)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownTotal)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownUnsecuredNum)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownBlNum)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownVlNum)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownClNum)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownPlNum)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownUnsecuredValue)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownUnsecured)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownBlValue)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownBL)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownVlValue)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownVL)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownClValue)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownCL)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownPlValue)
        Me.gpbxDrawdown.Controls.Add(Me.lblDrawdownPL)
        Me.gpbxDrawdown.Location = New System.Drawing.Point(12, 693)
        Me.gpbxDrawdown.Name = "gpbxDrawdown"
        Me.gpbxDrawdown.Size = New System.Drawing.Size(270, 118)
        Me.gpbxDrawdown.TabIndex = 11
        Me.gpbxDrawdown.TabStop = False
        Me.gpbxDrawdown.Text = "Draw-downs"
        '
        'lblDrawdownTotalValue
        '
        Me.lblDrawdownTotalValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownTotalValue.AutoSize = True
        Me.lblDrawdownTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrawdownTotalValue.Location = New System.Drawing.Point(181, 96)
        Me.lblDrawdownTotalValue.Name = "lblDrawdownTotalValue"
        Me.lblDrawdownTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblDrawdownTotalValue.TabIndex = 17
        Me.lblDrawdownTotalValue.Text = "Total value"
        '
        'lblDrawdownTotalNum
        '
        Me.lblDrawdownTotalNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownTotalNum.AutoSize = True
        Me.lblDrawdownTotalNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrawdownTotalNum.Location = New System.Drawing.Point(121, 96)
        Me.lblDrawdownTotalNum.Name = "lblDrawdownTotalNum"
        Me.lblDrawdownTotalNum.Size = New System.Drawing.Size(71, 13)
        Me.lblDrawdownTotalNum.TabIndex = 16
        Me.lblDrawdownTotalNum.Text = "Total value"
        '
        'lblDrawdownTotal
        '
        Me.lblDrawdownTotal.AutoSize = True
        Me.lblDrawdownTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrawdownTotal.Location = New System.Drawing.Point(5, 95)
        Me.lblDrawdownTotal.Name = "lblDrawdownTotal"
        Me.lblDrawdownTotal.Size = New System.Drawing.Size(105, 13)
        Me.lblDrawdownTotal.TabIndex = 15
        Me.lblDrawdownTotal.Text = "Total Drawdowns"
        Me.lblDrawdownTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDrawdownUnsecuredNum
        '
        Me.lblDrawdownUnsecuredNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownUnsecuredNum.AutoSize = True
        Me.lblDrawdownUnsecuredNum.Location = New System.Drawing.Point(121, 81)
        Me.lblDrawdownUnsecuredNum.Name = "lblDrawdownUnsecuredNum"
        Me.lblDrawdownUnsecuredNum.Size = New System.Drawing.Size(61, 13)
        Me.lblDrawdownUnsecuredNum.TabIndex = 14
        Me.lblDrawdownUnsecuredNum.Text = "Unsec num"
        '
        'lblDrawdownBlNum
        '
        Me.lblDrawdownBlNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownBlNum.AutoSize = True
        Me.lblDrawdownBlNum.Location = New System.Drawing.Point(121, 66)
        Me.lblDrawdownBlNum.Name = "lblDrawdownBlNum"
        Me.lblDrawdownBlNum.Size = New System.Drawing.Size(43, 13)
        Me.lblDrawdownBlNum.TabIndex = 13
        Me.lblDrawdownBlNum.Text = "BL num"
        '
        'lblDrawdownVlNum
        '
        Me.lblDrawdownVlNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownVlNum.AutoSize = True
        Me.lblDrawdownVlNum.Location = New System.Drawing.Point(121, 51)
        Me.lblDrawdownVlNum.Name = "lblDrawdownVlNum"
        Me.lblDrawdownVlNum.Size = New System.Drawing.Size(43, 13)
        Me.lblDrawdownVlNum.TabIndex = 12
        Me.lblDrawdownVlNum.Text = "VL num"
        '
        'lblDrawdownClNum
        '
        Me.lblDrawdownClNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownClNum.AutoSize = True
        Me.lblDrawdownClNum.Location = New System.Drawing.Point(121, 38)
        Me.lblDrawdownClNum.Name = "lblDrawdownClNum"
        Me.lblDrawdownClNum.Size = New System.Drawing.Size(43, 13)
        Me.lblDrawdownClNum.TabIndex = 11
        Me.lblDrawdownClNum.Text = "CL num"
        '
        'lblDrawdownPlNum
        '
        Me.lblDrawdownPlNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownPlNum.AutoSize = True
        Me.lblDrawdownPlNum.Location = New System.Drawing.Point(121, 25)
        Me.lblDrawdownPlNum.Name = "lblDrawdownPlNum"
        Me.lblDrawdownPlNum.Size = New System.Drawing.Size(43, 13)
        Me.lblDrawdownPlNum.TabIndex = 10
        Me.lblDrawdownPlNum.Text = "PL num"
        '
        'lblDrawdownUnsecuredValue
        '
        Me.lblDrawdownUnsecuredValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownUnsecuredValue.AutoSize = True
        Me.lblDrawdownUnsecuredValue.Location = New System.Drawing.Point(181, 81)
        Me.lblDrawdownUnsecuredValue.Name = "lblDrawdownUnsecuredValue"
        Me.lblDrawdownUnsecuredValue.Size = New System.Drawing.Size(67, 13)
        Me.lblDrawdownUnsecuredValue.TabIndex = 9
        Me.lblDrawdownUnsecuredValue.Text = "Unsec value"
        '
        'lblDrawdownUnsecured
        '
        Me.lblDrawdownUnsecured.AutoSize = True
        Me.lblDrawdownUnsecured.Location = New System.Drawing.Point(21, 81)
        Me.lblDrawdownUnsecured.Name = "lblDrawdownUnsecured"
        Me.lblDrawdownUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblDrawdownUnsecured.TabIndex = 8
        Me.lblDrawdownUnsecured.Text = "Unsecured Loan"
        Me.lblDrawdownUnsecured.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDrawdownBlValue
        '
        Me.lblDrawdownBlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownBlValue.AutoSize = True
        Me.lblDrawdownBlValue.Location = New System.Drawing.Point(181, 66)
        Me.lblDrawdownBlValue.Name = "lblDrawdownBlValue"
        Me.lblDrawdownBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDrawdownBlValue.TabIndex = 7
        Me.lblDrawdownBlValue.Text = "BL value"
        '
        'lblDrawdownBL
        '
        Me.lblDrawdownBL.AutoSize = True
        Me.lblDrawdownBL.Location = New System.Drawing.Point(31, 66)
        Me.lblDrawdownBL.Name = "lblDrawdownBL"
        Me.lblDrawdownBL.Size = New System.Drawing.Size(76, 13)
        Me.lblDrawdownBL.TabIndex = 6
        Me.lblDrawdownBL.Text = "Business Loan"
        Me.lblDrawdownBL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDrawdownVlValue
        '
        Me.lblDrawdownVlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownVlValue.AutoSize = True
        Me.lblDrawdownVlValue.Location = New System.Drawing.Point(181, 51)
        Me.lblDrawdownVlValue.Name = "lblDrawdownVlValue"
        Me.lblDrawdownVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDrawdownVlValue.TabIndex = 5
        Me.lblDrawdownVlValue.Text = "VL value"
        '
        'lblDrawdownVL
        '
        Me.lblDrawdownVL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownVL.AutoSize = True
        Me.lblDrawdownVL.Location = New System.Drawing.Point(38, 51)
        Me.lblDrawdownVL.Name = "lblDrawdownVL"
        Me.lblDrawdownVL.Size = New System.Drawing.Size(69, 13)
        Me.lblDrawdownVL.TabIndex = 4
        Me.lblDrawdownVL.Text = "Vehicle Loan"
        Me.lblDrawdownVL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDrawdownClValue
        '
        Me.lblDrawdownClValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownClValue.AutoSize = True
        Me.lblDrawdownClValue.Location = New System.Drawing.Point(181, 38)
        Me.lblDrawdownClValue.Name = "lblDrawdownClValue"
        Me.lblDrawdownClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDrawdownClValue.TabIndex = 3
        Me.lblDrawdownClValue.Text = "CL value"
        '
        'lblDrawdownCL
        '
        Me.lblDrawdownCL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownCL.AutoSize = True
        Me.lblDrawdownCL.Location = New System.Drawing.Point(26, 38)
        Me.lblDrawdownCL.Name = "lblDrawdownCL"
        Me.lblDrawdownCL.Size = New System.Drawing.Size(81, 13)
        Me.lblDrawdownCL.TabIndex = 2
        Me.lblDrawdownCL.Text = "Consumer Loan"
        Me.lblDrawdownCL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDrawdownPlValue
        '
        Me.lblDrawdownPlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownPlValue.AutoSize = True
        Me.lblDrawdownPlValue.Location = New System.Drawing.Point(181, 25)
        Me.lblDrawdownPlValue.Name = "lblDrawdownPlValue"
        Me.lblDrawdownPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblDrawdownPlValue.TabIndex = 1
        Me.lblDrawdownPlValue.Text = "PL value"
        '
        'lblDrawdownPL
        '
        Me.lblDrawdownPL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDrawdownPL.AutoSize = True
        Me.lblDrawdownPL.Location = New System.Drawing.Point(32, 25)
        Me.lblDrawdownPL.Name = "lblDrawdownPL"
        Me.lblDrawdownPL.Size = New System.Drawing.Size(75, 13)
        Me.lblDrawdownPL.TabIndex = 0
        Me.lblDrawdownPL.Text = "Personal Loan"
        Me.lblDrawdownPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpbxdateRange
        '
        Me.gpbxdateRange.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxdateRange.Controls.Add(Me.cmbxEndYear)
        Me.gpbxdateRange.Controls.Add(Me.lblEndYear)
        Me.gpbxdateRange.Controls.Add(Me.cmbxEndMonth)
        Me.gpbxdateRange.Controls.Add(Me.cmbxStartYear)
        Me.gpbxdateRange.Controls.Add(Me.lblStartYear)
        Me.gpbxdateRange.Controls.Add(Me.cmbxStartMonth)
        Me.gpbxdateRange.Controls.Add(Me.lblEndMonth)
        Me.gpbxdateRange.Controls.Add(Me.lblStartMonth)
        Me.gpbxdateRange.Location = New System.Drawing.Point(12, 73)
        Me.gpbxdateRange.Name = "gpbxdateRange"
        Me.gpbxdateRange.Size = New System.Drawing.Size(578, 80)
        Me.gpbxdateRange.TabIndex = 21
        Me.gpbxdateRange.TabStop = False
        Me.gpbxdateRange.Text = "Payout Month Range"
        '
        'cmbxEndYear
        '
        Me.cmbxEndYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxEndYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxEndYear.FormattingEnabled = True
        Me.cmbxEndYear.Items.AddRange(New Object() {"2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2030"})
        Me.cmbxEndYear.Location = New System.Drawing.Point(440, 46)
        Me.cmbxEndYear.Name = "cmbxEndYear"
        Me.cmbxEndYear.Size = New System.Drawing.Size(100, 21)
        Me.cmbxEndYear.TabIndex = 12
        '
        'lblEndYear
        '
        Me.lblEndYear.AutoSize = True
        Me.lblEndYear.Location = New System.Drawing.Point(370, 49)
        Me.lblEndYear.Name = "lblEndYear"
        Me.lblEndYear.Size = New System.Drawing.Size(51, 13)
        Me.lblEndYear.TabIndex = 11
        Me.lblEndYear.Text = "End Year"
        '
        'cmbxEndMonth
        '
        Me.cmbxEndMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxEndMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxEndMonth.FormattingEnabled = True
        Me.cmbxEndMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmbxEndMonth.Location = New System.Drawing.Point(440, 18)
        Me.cmbxEndMonth.Name = "cmbxEndMonth"
        Me.cmbxEndMonth.Size = New System.Drawing.Size(100, 21)
        Me.cmbxEndMonth.TabIndex = 10
        '
        'cmbxStartYear
        '
        Me.cmbxStartYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxStartYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxStartYear.FormattingEnabled = True
        Me.cmbxStartYear.Items.AddRange(New Object() {"2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2030"})
        Me.cmbxStartYear.Location = New System.Drawing.Point(97, 46)
        Me.cmbxStartYear.Name = "cmbxStartYear"
        Me.cmbxStartYear.Size = New System.Drawing.Size(100, 21)
        Me.cmbxStartYear.TabIndex = 9
        '
        'lblStartYear
        '
        Me.lblStartYear.AutoSize = True
        Me.lblStartYear.Location = New System.Drawing.Point(7, 49)
        Me.lblStartYear.Name = "lblStartYear"
        Me.lblStartYear.Size = New System.Drawing.Size(54, 13)
        Me.lblStartYear.TabIndex = 8
        Me.lblStartYear.Text = "Start Year"
        '
        'cmbxStartMonth
        '
        Me.cmbxStartMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxStartMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxStartMonth.FormattingEnabled = True
        Me.cmbxStartMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmbxStartMonth.Location = New System.Drawing.Point(97, 18)
        Me.cmbxStartMonth.Name = "cmbxStartMonth"
        Me.cmbxStartMonth.Size = New System.Drawing.Size(100, 21)
        Me.cmbxStartMonth.TabIndex = 7
        '
        'lblEndMonth
        '
        Me.lblEndMonth.AutoSize = True
        Me.lblEndMonth.Location = New System.Drawing.Point(370, 21)
        Me.lblEndMonth.Name = "lblEndMonth"
        Me.lblEndMonth.Size = New System.Drawing.Size(59, 13)
        Me.lblEndMonth.TabIndex = 2
        Me.lblEndMonth.Text = "End Month"
        '
        'lblStartMonth
        '
        Me.lblStartMonth.AutoSize = True
        Me.lblStartMonth.Location = New System.Drawing.Point(7, 21)
        Me.lblStartMonth.Name = "lblStartMonth"
        Me.lblStartMonth.Size = New System.Drawing.Size(62, 13)
        Me.lblStartMonth.TabIndex = 0
        Me.lblStartMonth.Text = "Start Month"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox1.Controls.Add(Me.lblMonthTotalNum)
        Me.GroupBox1.Controls.Add(Me.lblMonthTotalValue)
        Me.GroupBox1.Controls.Add(Me.lblMonthTotal)
        Me.GroupBox1.Controls.Add(Me.lblMonthUnsecuredNum)
        Me.GroupBox1.Controls.Add(Me.lblMonthBlNum)
        Me.GroupBox1.Controls.Add(Me.lblMonthVlNum)
        Me.GroupBox1.Controls.Add(Me.lblMonthClNum)
        Me.GroupBox1.Controls.Add(Me.lblMonthPlNum)
        Me.GroupBox1.Controls.Add(Me.lblMonthUnsecuredValue)
        Me.GroupBox1.Controls.Add(Me.lblMonthUnsecured)
        Me.GroupBox1.Controls.Add(Me.lblMonthBlValue)
        Me.GroupBox1.Controls.Add(Me.lblMonthBl)
        Me.GroupBox1.Controls.Add(Me.lblMonthVlValue)
        Me.GroupBox1.Controls.Add(Me.lblMonthVl)
        Me.GroupBox1.Controls.Add(Me.lblMonthClValue)
        Me.GroupBox1.Controls.Add(Me.lblMonthCl)
        Me.GroupBox1.Controls.Add(Me.lblMonthPlValue)
        Me.GroupBox1.Controls.Add(Me.lblMonthPl)
        Me.GroupBox1.Location = New System.Drawing.Point(321, 693)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(270, 118)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Draw-downs by Payout Month"
        '
        'lblMonthTotalNum
        '
        Me.lblMonthTotalNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthTotalNum.AutoSize = True
        Me.lblMonthTotalNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonthTotalNum.Location = New System.Drawing.Point(120, 95)
        Me.lblMonthTotalNum.Name = "lblMonthTotalNum"
        Me.lblMonthTotalNum.Size = New System.Drawing.Size(71, 13)
        Me.lblMonthTotalNum.TabIndex = 17
        Me.lblMonthTotalNum.Text = "Total value"
        '
        'lblMonthTotalValue
        '
        Me.lblMonthTotalValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthTotalValue.AutoSize = True
        Me.lblMonthTotalValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonthTotalValue.Location = New System.Drawing.Point(180, 95)
        Me.lblMonthTotalValue.Name = "lblMonthTotalValue"
        Me.lblMonthTotalValue.Size = New System.Drawing.Size(71, 13)
        Me.lblMonthTotalValue.TabIndex = 16
        Me.lblMonthTotalValue.Text = "Total value"
        '
        'lblMonthTotal
        '
        Me.lblMonthTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthTotal.AutoSize = True
        Me.lblMonthTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonthTotal.Location = New System.Drawing.Point(1, 95)
        Me.lblMonthTotal.Name = "lblMonthTotal"
        Me.lblMonthTotal.Size = New System.Drawing.Size(105, 13)
        Me.lblMonthTotal.TabIndex = 15
        Me.lblMonthTotal.Text = "Total Drawdowns"
        '
        'lblMonthUnsecuredNum
        '
        Me.lblMonthUnsecuredNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthUnsecuredNum.AutoSize = True
        Me.lblMonthUnsecuredNum.Location = New System.Drawing.Point(120, 81)
        Me.lblMonthUnsecuredNum.Name = "lblMonthUnsecuredNum"
        Me.lblMonthUnsecuredNum.Size = New System.Drawing.Size(61, 13)
        Me.lblMonthUnsecuredNum.TabIndex = 14
        Me.lblMonthUnsecuredNum.Text = "Unsec num"
        '
        'lblMonthBlNum
        '
        Me.lblMonthBlNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthBlNum.AutoSize = True
        Me.lblMonthBlNum.Location = New System.Drawing.Point(120, 66)
        Me.lblMonthBlNum.Name = "lblMonthBlNum"
        Me.lblMonthBlNum.Size = New System.Drawing.Size(43, 13)
        Me.lblMonthBlNum.TabIndex = 13
        Me.lblMonthBlNum.Text = "BL num"
        '
        'lblMonthVlNum
        '
        Me.lblMonthVlNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthVlNum.AutoSize = True
        Me.lblMonthVlNum.Location = New System.Drawing.Point(120, 51)
        Me.lblMonthVlNum.Name = "lblMonthVlNum"
        Me.lblMonthVlNum.Size = New System.Drawing.Size(43, 13)
        Me.lblMonthVlNum.TabIndex = 12
        Me.lblMonthVlNum.Text = "VL num"
        '
        'lblMonthClNum
        '
        Me.lblMonthClNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthClNum.AutoSize = True
        Me.lblMonthClNum.Location = New System.Drawing.Point(120, 38)
        Me.lblMonthClNum.Name = "lblMonthClNum"
        Me.lblMonthClNum.Size = New System.Drawing.Size(43, 13)
        Me.lblMonthClNum.TabIndex = 11
        Me.lblMonthClNum.Text = "CL num"
        '
        'lblMonthPlNum
        '
        Me.lblMonthPlNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthPlNum.AutoSize = True
        Me.lblMonthPlNum.Location = New System.Drawing.Point(120, 25)
        Me.lblMonthPlNum.Name = "lblMonthPlNum"
        Me.lblMonthPlNum.Size = New System.Drawing.Size(43, 13)
        Me.lblMonthPlNum.TabIndex = 10
        Me.lblMonthPlNum.Text = "PL num"
        '
        'lblMonthUnsecuredValue
        '
        Me.lblMonthUnsecuredValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthUnsecuredValue.AutoSize = True
        Me.lblMonthUnsecuredValue.Location = New System.Drawing.Point(180, 81)
        Me.lblMonthUnsecuredValue.Name = "lblMonthUnsecuredValue"
        Me.lblMonthUnsecuredValue.Size = New System.Drawing.Size(67, 13)
        Me.lblMonthUnsecuredValue.TabIndex = 9
        Me.lblMonthUnsecuredValue.Text = "Unsec value"
        '
        'lblMonthUnsecured
        '
        Me.lblMonthUnsecured.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthUnsecured.AutoSize = True
        Me.lblMonthUnsecured.Location = New System.Drawing.Point(18, 81)
        Me.lblMonthUnsecured.Name = "lblMonthUnsecured"
        Me.lblMonthUnsecured.Size = New System.Drawing.Size(86, 13)
        Me.lblMonthUnsecured.TabIndex = 8
        Me.lblMonthUnsecured.Text = "Unsecured Loan"
        '
        'lblMonthBlValue
        '
        Me.lblMonthBlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthBlValue.AutoSize = True
        Me.lblMonthBlValue.Location = New System.Drawing.Point(180, 66)
        Me.lblMonthBlValue.Name = "lblMonthBlValue"
        Me.lblMonthBlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblMonthBlValue.TabIndex = 7
        Me.lblMonthBlValue.Text = "BL value"
        '
        'lblMonthBl
        '
        Me.lblMonthBl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthBl.AutoSize = True
        Me.lblMonthBl.Location = New System.Drawing.Point(28, 66)
        Me.lblMonthBl.Name = "lblMonthBl"
        Me.lblMonthBl.Size = New System.Drawing.Size(76, 13)
        Me.lblMonthBl.TabIndex = 6
        Me.lblMonthBl.Text = "Business Loan"
        '
        'lblMonthVlValue
        '
        Me.lblMonthVlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthVlValue.AutoSize = True
        Me.lblMonthVlValue.Location = New System.Drawing.Point(180, 51)
        Me.lblMonthVlValue.Name = "lblMonthVlValue"
        Me.lblMonthVlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblMonthVlValue.TabIndex = 5
        Me.lblMonthVlValue.Text = "VL value"
        '
        'lblMonthVl
        '
        Me.lblMonthVl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthVl.AutoSize = True
        Me.lblMonthVl.Location = New System.Drawing.Point(35, 51)
        Me.lblMonthVl.Name = "lblMonthVl"
        Me.lblMonthVl.Size = New System.Drawing.Size(69, 13)
        Me.lblMonthVl.TabIndex = 4
        Me.lblMonthVl.Text = "Vehicle Loan"
        Me.lblMonthVl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMonthClValue
        '
        Me.lblMonthClValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthClValue.AutoSize = True
        Me.lblMonthClValue.Location = New System.Drawing.Point(180, 38)
        Me.lblMonthClValue.Name = "lblMonthClValue"
        Me.lblMonthClValue.Size = New System.Drawing.Size(49, 13)
        Me.lblMonthClValue.TabIndex = 3
        Me.lblMonthClValue.Text = "CL value"
        '
        'lblMonthCl
        '
        Me.lblMonthCl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthCl.AutoSize = True
        Me.lblMonthCl.Location = New System.Drawing.Point(23, 38)
        Me.lblMonthCl.Name = "lblMonthCl"
        Me.lblMonthCl.Size = New System.Drawing.Size(81, 13)
        Me.lblMonthCl.TabIndex = 2
        Me.lblMonthCl.Text = "Consumer Loan"
        Me.lblMonthCl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMonthPlValue
        '
        Me.lblMonthPlValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthPlValue.AutoSize = True
        Me.lblMonthPlValue.Location = New System.Drawing.Point(180, 25)
        Me.lblMonthPlValue.Name = "lblMonthPlValue"
        Me.lblMonthPlValue.Size = New System.Drawing.Size(49, 13)
        Me.lblMonthPlValue.TabIndex = 1
        Me.lblMonthPlValue.Text = "PL value"
        '
        'lblMonthPl
        '
        Me.lblMonthPl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthPl.AutoSize = True
        Me.lblMonthPl.Location = New System.Drawing.Point(29, 25)
        Me.lblMonthPl.Name = "lblMonthPl"
        Me.lblMonthPl.Size = New System.Drawing.Size(75, 13)
        Me.lblMonthPl.TabIndex = 0
        Me.lblMonthPl.Text = "Personal Loan"
        Me.lblMonthPl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnExcel
        '
        Me.btnExcel.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnExcel.Location = New System.Drawing.Point(470, 159)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(121, 23)
        Me.btnExcel.TabIndex = 23
        Me.btnExcel.Text = "Produce Excel"
        Me.btnExcel.UseVisualStyleBackColor = False
        Me.btnExcel.Visible = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.MistyRose
        Me.btnReset.Location = New System.Drawing.Point(12, 159)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(121, 23)
        Me.btnReset.TabIndex = 24
        Me.btnReset.Text = "Reset"
        Me.btnReset.UseVisualStyleBackColor = False
        Me.btnReset.Visible = False
        '
        'pbReportInformation
        '
        Me.pbReportInformation.BackColor = System.Drawing.SystemColors.Control
        Me.pbReportInformation.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbReportInformation.Location = New System.Drawing.Point(360, 4)
        Me.pbReportInformation.Name = "pbReportInformation"
        Me.pbReportInformation.Size = New System.Drawing.Size(18, 18)
        Me.pbReportInformation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbReportInformation.TabIndex = 257
        Me.pbReportInformation.TabStop = False
        Me.pbReportInformation.Visible = False
        '
        'FrmKpiSumReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(603, 823)
        Me.Controls.Add(Me.pbReportInformation)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExcel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gpbxdateRange)
        Me.Controls.Add(Me.gpbxDrawdown)
        Me.Controls.Add(Me.gpbxDd)
        Me.Controls.Add(Me.gpbxDeclined)
        Me.Controls.Add(Me.gpbxWithdrawn)
        Me.Controls.Add(Me.gpbxApplications)
        Me.Controls.Add(Me.gpbxEnquiries)
        Me.Controls.Add(Me.gpbxGeneral)
        Me.Controls.Add(Me.gpbxRetail)
        Me.Controls.Add(Me.btnRunReport)
        Me.Controls.Add(Me.gpbxDates)
        Me.Controls.Add(Me.lblPageTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(701, 1050)
        Me.Name = "FrmKpiSumReport"
        Me.Text = "KPI Summary Report"
        Me.gpbxDates.ResumeLayout(False)
        Me.gpbxDates.PerformLayout()
        Me.gpbxRetail.ResumeLayout(False)
        CType(Me.dgvRetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxGeneral.ResumeLayout(False)
        Me.gpbxGeneral.PerformLayout()
        Me.gpbxEnquiries.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.gpbxApplications.ResumeLayout(False)
        Me.gpbxApplications.PerformLayout()
        Me.gpbxWithdrawn.ResumeLayout(False)
        Me.gpbxWithdrawn.PerformLayout()
        Me.gpbxDeclined.ResumeLayout(False)
        Me.gpbxDeclined.PerformLayout()
        Me.gpbxDd.ResumeLayout(False)
        Me.gpbxDd.PerformLayout()
        Me.gpbxDrawdown.ResumeLayout(False)
        Me.gpbxDrawdown.PerformLayout()
        Me.gpbxdateRange.ResumeLayout(False)
        Me.gpbxdateRange.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.pbReportInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPageTitle As System.Windows.Forms.Label
    Friend WithEvents gpbxDates As System.Windows.Forms.GroupBox
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartdate As System.Windows.Forms.Label
    Friend WithEvents btnRunReport As System.Windows.Forms.Button
    Friend WithEvents gpbxRetail As System.Windows.Forms.GroupBox
    Friend WithEvents dgvRetail As System.Windows.Forms.DataGridView
    Friend WithEvents gpbxGeneral As System.Windows.Forms.GroupBox
    Friend WithEvents lblClientsRelentValue As System.Windows.Forms.Label
    Friend WithEvents lblAdRepliesValue As System.Windows.Forms.Label
    Friend WithEvents lblClientsRelent As System.Windows.Forms.Label
    Friend WithEvents lblAdReplies As System.Windows.Forms.Label
    Friend WithEvents gpbxEnquiries As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiriesUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesBlValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesBL As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesVlValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesVL As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesClValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesCL As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesPlValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesPL As System.Windows.Forms.Label
    Friend WithEvents gpbxApplications As System.Windows.Forms.GroupBox
    Friend WithEvents lblApplicationsUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsBlValue As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsBL As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsVlValue As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsVL As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsClValue As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsCL As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsPlValue As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsPL As System.Windows.Forms.Label
    Friend WithEvents gpbxWithdrawn As System.Windows.Forms.GroupBox
    Friend WithEvents lblWithdrawnUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnBlValue As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnBL As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnVlValue As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnVL As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnClValue As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnCL As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnPlValue As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnPL As System.Windows.Forms.Label
    Friend WithEvents gpbxDeclined As System.Windows.Forms.GroupBox
    Friend WithEvents lblDeclinedUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedBlValue As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedBL As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedVlValue As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedVL As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedClValue As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedCL As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedPlValue As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedPL As System.Windows.Forms.Label
    Friend WithEvents gpbxDd As System.Windows.Forms.GroupBox
    Friend WithEvents lblDdUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblDdUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblDdBlValue As System.Windows.Forms.Label
    Friend WithEvents lblDdBL As System.Windows.Forms.Label
    Friend WithEvents lblDdVlValue As System.Windows.Forms.Label
    Friend WithEvents lblDdVL As System.Windows.Forms.Label
    Friend WithEvents lblDdClValue As System.Windows.Forms.Label
    Friend WithEvents lblDdCL As System.Windows.Forms.Label
    Friend WithEvents lblDdPlValue As System.Windows.Forms.Label
    Friend WithEvents lblDdPL As System.Windows.Forms.Label
    Friend WithEvents gpbxDrawdown As System.Windows.Forms.GroupBox
    Friend WithEvents lblDrawdownUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownBlValue As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownBL As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownVlValue As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownVL As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownClValue As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownCL As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownPlValue As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownPL As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownUnsecuredNum As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownBlNum As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownVlNum As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownClNum As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownPlNum As System.Windows.Forms.Label
    Friend WithEvents gpbxdateRange As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxEndYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblEndYear As System.Windows.Forms.Label
    Friend WithEvents cmbxEndMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblStartYear As System.Windows.Forms.Label
    Friend WithEvents cmbxStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblEndMonth As System.Windows.Forms.Label
    Friend WithEvents lblStartMonth As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblMonthUnsecuredNum As System.Windows.Forms.Label
    Friend WithEvents lblMonthBlNum As System.Windows.Forms.Label
    Friend WithEvents lblMonthVlNum As System.Windows.Forms.Label
    Friend WithEvents lblMonthClNum As System.Windows.Forms.Label
    Friend WithEvents lblMonthPlNum As System.Windows.Forms.Label
    Friend WithEvents lblMonthUnsecuredValue As System.Windows.Forms.Label
    Friend WithEvents lblMonthUnsecured As System.Windows.Forms.Label
    Friend WithEvents lblMonthBlValue As System.Windows.Forms.Label
    Friend WithEvents lblMonthBl As System.Windows.Forms.Label
    Friend WithEvents lblMonthVlValue As System.Windows.Forms.Label
    Friend WithEvents lblMonthVl As System.Windows.Forms.Label
    Friend WithEvents lblMonthClValue As System.Windows.Forms.Label
    Friend WithEvents lblMonthCl As System.Windows.Forms.Label
    Friend WithEvents lblMonthPlValue As System.Windows.Forms.Label
    Friend WithEvents lblMonthPl As System.Windows.Forms.Label
    Friend WithEvents btnExcel As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents lblEnquiriesTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalEnquiries As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalApplications As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalWithdrawn As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalDeclined As System.Windows.Forms.Label
    Friend WithEvents lblDdTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblDdTotal As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownTotalNum As System.Windows.Forms.Label
    Friend WithEvents lblDrawdownTotal As System.Windows.Forms.Label
    Friend WithEvents lblMonthTotalNum As System.Windows.Forms.Label
    Friend WithEvents lblMonthTotalValue As System.Windows.Forms.Label
    Friend WithEvents lblMonthTotal As System.Windows.Forms.Label
    Friend WithEvents pbReportInformation As System.Windows.Forms.PictureBox
    Friend WithEvents lblEnquiriesFfValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiriesFf As System.Windows.Forms.Label
    Friend WithEvents lblDdFfValue As System.Windows.Forms.Label
    Friend WithEvents lblDdFf As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsFfValue As System.Windows.Forms.Label
    Friend WithEvents lblApplicationsFf As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnFfValue As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawnFf As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedFfValue As System.Windows.Forms.Label
    Friend WithEvents lblDeclinedFf As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
End Class
