﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WkShtWizForm1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WkShtWizForm1))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblLoanProgress = New System.Windows.Forms.Label()
        Me.txtbxAppCode = New System.Windows.Forms.Label()
        Me.txtbxAppCodeValue = New System.Windows.Forms.TextBox()
        Me.lblError = New System.Windows.Forms.Label()
        Me.btnAddComment = New System.Windows.Forms.Button()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pbNC = New System.Windows.Forms.PictureBox()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnQRGList = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.btnGenAppCode = New System.Windows.Forms.Button()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblContactNumber = New System.Windows.Forms.Label()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblContactName = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblContactAddress = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tlpApplication = New System.Windows.Forms.TableLayoutPanel()
        Me.cmbxMainCustType = New System.Windows.Forms.ComboBox()
        Me.lblMainCustType = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEnquiryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeclinedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WithdrawnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsReceivedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DealersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MCMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocsReceivedImg = New System.Windows.Forms.ToolStripMenuItem()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.lblCurrentLoan = New System.Windows.Forms.Label()
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.dgvEnquiryComments = New System.Windows.Forms.DataGridView()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateCreatedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryCommentBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.PrelimReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeOfLoanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LoanTypesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.ApplicationCodeGeneratorTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ApplicationCodeGeneratorTableAdapter()
        Me.DueDiligenceTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DueDiligenceTableAdapter()
        Me.EnquiryCommentTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter()
        Me.LoanTypesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanTypesTableAdapter()
        Me.TypeOfLoanTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfLoanTableAdapter()
        Me.LoanTypesBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ApplicationCodeGeneratorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrelimResultsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrelimResultsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter()
        Me.PrelimReasonsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DueDiligenceBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusStripMessage.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.pbNC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxStatus.SuspendLayout()
        Me.tlpApplication.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.gpbxManager.SuspendLayout()
        CType(Me.dgvEnquiryComments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryCommentBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeOfLoanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoanTypesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoanTypesBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ApplicationCodeGeneratorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DueDiligenceBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnFinish
        '
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(617, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.TabIndex = 23
        Me.btnFinish.Text = "Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(536, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 22
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Enabled = False
        Me.btnBack.Location = New System.Drawing.Point(455, 3)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 21
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(374, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 20
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblLoanProgress
        '
        Me.lblLoanProgress.AutoSize = True
        Me.lblLoanProgress.Location = New System.Drawing.Point(26, 148)
        Me.lblLoanProgress.Name = "lblLoanProgress"
        Me.lblLoanProgress.Size = New System.Drawing.Size(355, 13)
        Me.lblLoanProgress.TabIndex = 16
        Me.lblLoanProgress.Text = "Progress Status and Comments (What are waiting for, please be accurate)"
        '
        'txtbxAppCode
        '
        Me.txtbxAppCode.AutoSize = True
        Me.txtbxAppCode.Dock = System.Windows.Forms.DockStyle.Left
        Me.txtbxAppCode.Location = New System.Drawing.Point(3, 0)
        Me.txtbxAppCode.Name = "txtbxAppCode"
        Me.txtbxAppCode.Size = New System.Drawing.Size(87, 30)
        Me.txtbxAppCode.TabIndex = 17
        Me.txtbxAppCode.Text = "Application Code"
        Me.txtbxAppCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtbxAppCodeValue
        '
        Me.txtbxAppCodeValue.Location = New System.Drawing.Point(106, 3)
        Me.txtbxAppCodeValue.MaxLength = 14
        Me.txtbxAppCodeValue.Name = "txtbxAppCodeValue"
        Me.txtbxAppCodeValue.Size = New System.Drawing.Size(120, 20)
        Me.txtbxAppCodeValue.TabIndex = 1
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Location = New System.Drawing.Point(13, 6)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 27
        Me.lblError.Visible = False
        '
        'btnAddComment
        '
        Me.btnAddComment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddComment.Location = New System.Drawing.Point(832, 143)
        Me.btnAddComment.Name = "btnAddComment"
        Me.btnAddComment.Size = New System.Drawing.Size(100, 23)
        Me.btnAddComment.TabIndex = 11
        Me.btnAddComment.Text = "Add Comment"
        Me.btnAddComment.UseVisualStyleBackColor = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 990)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStripMessage.Size = New System.Drawing.Size(944, 22)
        Me.StatusStripMessage.TabIndex = 51
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.pbNC)
        Me.Panel1.Controls.Add(Me.btnViewDocs)
        Me.Panel1.Controls.Add(Me.btnQRGList)
        Me.Panel1.Controls.Add(Me.btnDocs)
        Me.Panel1.Controls.Add(Me.btnSaveAndExit)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnBack)
        Me.Panel1.Controls.Add(Me.btnNext)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 960)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 30)
        Me.Panel1.TabIndex = 57
        '
        'pbNC
        '
        Me.pbNC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbNC.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbNC.Location = New System.Drawing.Point(66, 6)
        Me.pbNC.Name = "pbNC"
        Me.pbNC.Size = New System.Drawing.Size(18, 18)
        Me.pbNC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbNC.TabIndex = 28
        Me.pbNC.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbNC, "Click to see explanation of politically exposed person")
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(34, 3)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 27
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnQRGList
        '
        Me.btnQRGList.Location = New System.Drawing.Point(106, 3)
        Me.btnQRGList.Name = "btnQRGList"
        Me.btnQRGList.Size = New System.Drawing.Size(75, 23)
        Me.btnQRGList.TabIndex = 26
        Me.btnQRGList.Text = "QRGuide"
        Me.btnQRGList.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(3, 3)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 25
        Me.ToolTip1.SetToolTip(Me.btnDocs, "You can 'Drag and Drop' documents from Windows Explorer or your Desktop.")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(275, 3)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 24
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'btnGenAppCode
        '
        Me.btnGenAppCode.Location = New System.Drawing.Point(232, 3)
        Me.btnGenAppCode.Name = "btnGenAppCode"
        Me.btnGenAppCode.Size = New System.Drawing.Size(104, 23)
        Me.btnGenAppCode.TabIndex = 58
        Me.btnGenAppCode.Text = "Generate Code"
        Me.btnGenAppCode.UseVisualStyleBackColor = True
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(59, 84)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(0, 13)
        Me.lblDealerName.TabIndex = 75
        '
        'lblContactNumber
        '
        Me.lblContactNumber.AutoSize = True
        Me.lblContactNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNumber.Location = New System.Drawing.Point(594, 62)
        Me.lblContactNumber.Name = "lblContactNumber"
        Me.lblContactNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblContactNumber.TabIndex = 72
        Me.lblContactNumber.Text = "Number"
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 71
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(299, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(190, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 67
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(492, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 65
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 84)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 64
        Me.lblDealer.Text = "Dealer:"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(187, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 63
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(155, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 62
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(92, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 61
        Me.lblEnquiryNumberValue.Text = "0000"
        Me.ToolTip1.SetToolTip(Me.lblEnquiryNumberValue, "Double click mouse to copy text")
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 60
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(842, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 76
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContactName
        '
        Me.lblContactName.AutoSize = True
        Me.lblContactName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactName.Location = New System.Drawing.Point(15, 62)
        Me.lblContactName.Name = "lblContactName"
        Me.lblContactName.Size = New System.Drawing.Size(75, 13)
        Me.lblContactName.TabIndex = 79
        Me.lblContactName.Text = "Client Name"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 81
        Me.lblAddress.Text = "Address:"
        '
        'lblContactAddress
        '
        Me.lblContactAddress.AutoSize = True
        Me.lblContactAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblContactAddress.Name = "lblContactAddress"
        Me.lblContactAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblContactAddress.TabIndex = 82
        Me.lblContactAddress.Text = "ClientAddress"
        '
        'tlpApplication
        '
        Me.tlpApplication.ColumnCount = 6
        Me.tlpApplication.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.tlpApplication.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpApplication.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118.0!))
        Me.tlpApplication.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpApplication.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126.0!))
        Me.tlpApplication.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpApplication.Controls.Add(Me.txtbxAppCode, 0, 0)
        Me.tlpApplication.Controls.Add(Me.txtbxAppCodeValue, 1, 0)
        Me.tlpApplication.Controls.Add(Me.btnGenAppCode, 2, 0)
        Me.tlpApplication.Controls.Add(Me.cmbxMainCustType, 4, 0)
        Me.tlpApplication.Controls.Add(Me.lblMainCustType, 3, 0)
        Me.tlpApplication.Location = New System.Drawing.Point(12, 101)
        Me.tlpApplication.Name = "tlpApplication"
        Me.tlpApplication.RowCount = 1
        Me.tlpApplication.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.tlpApplication.Size = New System.Drawing.Size(920, 30)
        Me.tlpApplication.TabIndex = 84
        '
        'cmbxMainCustType
        '
        Me.cmbxMainCustType.FormattingEnabled = True
        Me.cmbxMainCustType.Location = New System.Drawing.Point(797, 3)
        Me.cmbxMainCustType.Name = "cmbxMainCustType"
        Me.cmbxMainCustType.Size = New System.Drawing.Size(120, 21)
        Me.cmbxMainCustType.TabIndex = 59
        '
        'lblMainCustType
        '
        Me.lblMainCustType.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblMainCustType.AutoSize = True
        Me.lblMainCustType.Location = New System.Drawing.Point(682, 8)
        Me.lblMainCustType.Name = "lblMainCustType"
        Me.lblMainCustType.Size = New System.Drawing.Size(109, 13)
        Me.lblMainCustType.TabIndex = 60
        Me.lblMainCustType.Text = "(main) Customer Type"
        Me.lblMainCustType.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.WWWToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem, Me.MCMenuItem, Me.DocsReceivedImg})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 246
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEnquiryToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Enabled = False
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEnquiryToolStripMenuItem
        '
        Me.EndEnquiryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeclinedToolStripMenuItem, Me.WithdrawnToolStripMenuItem})
        Me.EndEnquiryToolStripMenuItem.Name = "EndEnquiryToolStripMenuItem"
        Me.EndEnquiryToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEnquiryToolStripMenuItem.Text = "End Enquiry"
        Me.EndEnquiryToolStripMenuItem.Visible = False
        '
        'DeclinedToolStripMenuItem
        '
        Me.DeclinedToolStripMenuItem.Name = "DeclinedToolStripMenuItem"
        Me.DeclinedToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.DeclinedToolStripMenuItem.Text = "Declined"
        '
        'WithdrawnToolStripMenuItem
        '
        Me.WithdrawnToolStripMenuItem.Name = "WithdrawnToolStripMenuItem"
        Me.WithdrawnToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.WithdrawnToolStripMenuItem.Text = "Withdrawn"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem, Me.DocumentsReceivedToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'DocumentsReceivedToolStripMenuItem
        '
        Me.DocumentsReceivedToolStripMenuItem.Name = "DocumentsReceivedToolStripMenuItem"
        Me.DocumentsReceivedToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.DocumentsReceivedToolStripMenuItem.Text = "Documents received"
        Me.DocumentsReceivedToolStripMenuItem.Visible = False
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem, Me.DealersToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "Enquiries Names"
        '
        'DealersToolStripMenuItem
        '
        Me.DealersToolStripMenuItem.Name = "DealersToolStripMenuItem"
        Me.DealersToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.DealersToolStripMenuItem.Text = "Dealers"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1, Me.EmailApprovalToolStripMenuItem})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(154, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'EmailApprovalToolStripMenuItem
        '
        Me.EmailApprovalToolStripMenuItem.Name = "EmailApprovalToolStripMenuItem"
        Me.EmailApprovalToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EmailApprovalToolStripMenuItem.Text = "Email Approval"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "YFL Website"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Website - Ctrl+Alt+W"
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search Enquiry Names - Ctrl+Alt+M"
        '
        'MCMenuItem
        '
        Me.MCMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.MCMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.OAC16
        Me.MCMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MCMenuItem.Name = "MCMenuItem"
        Me.MCMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.MCMenuItem.Text = "MembersCentreLink"
        Me.MCMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.MCMenuItem.ToolTipText = "OAC Link"
        '
        'DocsReceivedImg
        '
        Me.DocsReceivedImg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.DocsReceivedImg.Image = Global.AppWhShtB.My.Resources.Resources.Docs24
        Me.DocsReceivedImg.Name = "DocsReceivedImg"
        Me.DocsReceivedImg.Size = New System.Drawing.Size(28, 20)
        Me.DocsReceivedImg.Text = "Docs received"
        Me.DocsReceivedImg.ToolTipText = "Documents received"
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(740, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 247
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'lblCurrentLoan
        '
        Me.lblCurrentLoan.AutoSize = True
        Me.lblCurrentLoan.Location = New System.Drawing.Point(567, 39)
        Me.lblCurrentLoan.Name = "lblCurrentLoan"
        Me.lblCurrentLoan.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrentLoan.TabIndex = 248
        Me.lblCurrentLoan.Text = "Current Loan"
        Me.lblCurrentLoan.Visible = False
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(639, 39)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(28, 13)
        Me.lblCurrentLoanAmt.TabIndex = 249
        Me.lblCurrentLoanAmt.Text = "0.00"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'dgvEnquiryComments
        '
        Me.dgvEnquiryComments.AllowUserToAddRows = False
        Me.dgvEnquiryComments.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEnquiryComments.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEnquiryComments.AutoGenerateColumns = False
        Me.dgvEnquiryComments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Me.dgvEnquiryComments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEnquiryComments.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.EnquiryIdDataGridViewTextBoxColumn, Me.DateCreatedDataGridViewTextBoxColumn, Me.UserIdDataGridViewTextBoxColumn, Me.TextDataGridViewTextBoxColumn})
        Me.dgvEnquiryComments.DataSource = Me.EnquiryCommentBS
        Me.dgvEnquiryComments.Location = New System.Drawing.Point(12, 172)
        Me.dgvEnquiryComments.MultiSelect = False
        Me.dgvEnquiryComments.Name = "dgvEnquiryComments"
        Me.dgvEnquiryComments.ReadOnly = True
        Me.dgvEnquiryComments.RowHeadersWidth = 10
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEnquiryComments.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvEnquiryComments.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgvEnquiryComments.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEnquiryComments.RowTemplate.Height = 10
        Me.dgvEnquiryComments.RowTemplate.ReadOnly = True
        Me.dgvEnquiryComments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnquiryComments.Size = New System.Drawing.Size(920, 782)
        Me.dgvEnquiryComments.TabIndex = 272
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'EnquiryIdDataGridViewTextBoxColumn
        '
        Me.EnquiryIdDataGridViewTextBoxColumn.DataPropertyName = "EnquiryId"
        Me.EnquiryIdDataGridViewTextBoxColumn.HeaderText = "EnquiryId"
        Me.EnquiryIdDataGridViewTextBoxColumn.Name = "EnquiryIdDataGridViewTextBoxColumn"
        Me.EnquiryIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.EnquiryIdDataGridViewTextBoxColumn.Visible = False
        '
        'DateCreatedDataGridViewTextBoxColumn
        '
        Me.DateCreatedDataGridViewTextBoxColumn.DataPropertyName = "DateCreated"
        DataGridViewCellStyle2.Format = "g"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DateCreatedDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.DateCreatedDataGridViewTextBoxColumn.HeaderText = "Date"
        Me.DateCreatedDataGridViewTextBoxColumn.Name = "DateCreatedDataGridViewTextBoxColumn"
        Me.DateCreatedDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateCreatedDataGridViewTextBoxColumn.Width = 120
        '
        'UserIdDataGridViewTextBoxColumn
        '
        Me.UserIdDataGridViewTextBoxColumn.DataPropertyName = "UserId"
        Me.UserIdDataGridViewTextBoxColumn.HeaderText = "User"
        Me.UserIdDataGridViewTextBoxColumn.Name = "UserIdDataGridViewTextBoxColumn"
        Me.UserIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.UserIdDataGridViewTextBoxColumn.Width = 75
        '
        'TextDataGridViewTextBoxColumn
        '
        Me.TextDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TextDataGridViewTextBoxColumn.DataPropertyName = "Text"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TextDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.TextDataGridViewTextBoxColumn.HeaderText = "Comment"
        Me.TextDataGridViewTextBoxColumn.Name = "TextDataGridViewTextBoxColumn"
        Me.TextDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EnquiryCommentBS
        '
        Me.EnquiryCommentBS.DataMember = "EnquiryComment"
        Me.EnquiryCommentBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'PrelimReasonsBindingSource
        '
        Me.PrelimReasonsBindingSource.DataMember = "PrelimReasons"
        Me.PrelimReasonsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'TypeOfLoanBindingSource
        '
        Me.TypeOfLoanBindingSource.DataMember = "TypeOfLoan"
        Me.TypeOfLoanBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'LoanTypesBindingSource
        '
        Me.LoanTypesBindingSource.DataMember = "LoanTypes"
        Me.LoanTypesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Me.ApplicationCodeGeneratorTableAdapter
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Me.DueDiligenceTableAdapter
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Me.EnquiryCommentTableAdapter
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Me.LoanTypesTableAdapter
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Me.TypeOfLoanTableAdapter
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'ApplicationCodeGeneratorTableAdapter
        '
        Me.ApplicationCodeGeneratorTableAdapter.ClearBeforeFill = True
        '
        'DueDiligenceTableAdapter
        '
        Me.DueDiligenceTableAdapter.ClearBeforeFill = True
        '
        'EnquiryCommentTableAdapter
        '
        Me.EnquiryCommentTableAdapter.ClearBeforeFill = True
        '
        'LoanTypesTableAdapter
        '
        Me.LoanTypesTableAdapter.ClearBeforeFill = True
        '
        'TypeOfLoanTableAdapter
        '
        Me.TypeOfLoanTableAdapter.ClearBeforeFill = True
        '
        'LoanTypesBindingSource1
        '
        Me.LoanTypesBindingSource1.DataMember = "LoanTypes"
        Me.LoanTypesBindingSource1.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'ApplicationCodeGeneratorBindingSource
        '
        Me.ApplicationCodeGeneratorBindingSource.DataMember = "ApplicationCodeGenerator"
        Me.ApplicationCodeGeneratorBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'PrelimResultsBindingSource
        '
        Me.PrelimResultsBindingSource.DataMember = "PrelimResults"
        Me.PrelimResultsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'PrelimResultsTableAdapter
        '
        Me.PrelimResultsTableAdapter.ClearBeforeFill = True
        '
        'PrelimReasonsTableAdapter
        '
        Me.PrelimReasonsTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DueDiligenceBindingSource
        '
        Me.DueDiligenceBindingSource.DataMember = "DueDiligence"
        Me.DueDiligenceBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Id"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Id"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.HeaderText = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "DateCreated"
        DataGridViewCellStyle5.Format = "f"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn3.HeaderText = "DateCreated"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "UserId"
        Me.DataGridViewTextBoxColumn4.HeaderText = "User"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 75
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Text"
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn5.HeaderText = "Text"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'WkShtWizForm1
        '
        Me.AcceptButton = Me.btnNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.PrelimFormSize
        Me.Controls.Add(Me.dgvEnquiryComments)
        Me.Controls.Add(Me.lblCurrentLoanAmt)
        Me.Controls.Add(Me.lblCurrentLoan)
        Me.Controls.Add(Me.gpbxManager)
        Me.Controls.Add(Me.gpbxStatus)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.tlpApplication)
        Me.Controls.Add(Me.lblContactAddress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.lblContactName)
        Me.Controls.Add(Me.lblDealerName)
        Me.Controls.Add(Me.lblContactNumber)
        Me.Controls.Add(Me.lblContact)
        Me.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.Controls.Add(Me.lblLoanAmount)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblDateAndTimeValue)
        Me.Controls.Add(Me.lblDateAndTime)
        Me.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Controls.Add(Me.lblEnquiryCode)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnAddComment)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.lblLoanProgress)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "PrelimFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "PrelimFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.PrelimFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "WkShtWizForm1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Due Diligence Main"
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.pbNC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        Me.tlpApplication.ResumeLayout(False)
        Me.tlpApplication.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        CType(Me.dgvEnquiryComments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryCommentBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeOfLoanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoanTypesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoanTypesBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ApplicationCodeGeneratorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DueDiligenceBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents LoanTypesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanTypesTableAdapter
    Friend WithEvents LoanTypesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lblLoanProgress As System.Windows.Forms.Label
    Friend WithEvents LoanTypesBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents TypeOfLoanBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TypeOfLoanTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TypeOfLoanTableAdapter
    Friend WithEvents txtbxAppCode As System.Windows.Forms.Label
    Friend WithEvents txtbxAppCodeValue As System.Windows.Forms.TextBox
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents btnAddComment As System.Windows.Forms.Button
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnGenAppCode As System.Windows.Forms.Button
    Friend WithEvents ApplicationCodeGeneratorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ApplicationCodeGeneratorTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ApplicationCodeGeneratorTableAdapter
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblContactNumber As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents PrelimResultsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimResultsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents lblContactName As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblContactAddress As System.Windows.Forms.Label
    Friend WithEvents PrelimReasonsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimReasonsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnQRGList As System.Windows.Forms.Button
    Friend WithEvents tlpApplication As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEnquiryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeclinedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WithdrawnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblCurrentLoan As System.Windows.Forms.Label
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents DueDiligenceTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DueDiligenceTableAdapter
    Friend WithEvents DueDiligenceBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmailApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DealersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MCMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmbxMainCustType As System.Windows.Forms.ComboBox
    Friend WithEvents lblMainCustType As System.Windows.Forms.Label
    Friend WithEvents dgvEnquiryComments As System.Windows.Forms.DataGridView
    Friend WithEvents EnquiryCommentBS As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryCommentTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnquiryIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateCreatedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents DocumentsReceivedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocsReceivedImg As ToolStripMenuItem
    Friend WithEvents pbNC As System.Windows.Forms.PictureBox
End Class
