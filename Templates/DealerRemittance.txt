﻿
========================================
Yes Finance Email Loan Remittance Advice
========================================
{0}

{1}

Payout Summary and Advice
*************************
Loan Contract number: {2}
Loan Name: {3}

Cash Price                                           {4:C2}
Additional Advances (Including Dealer Warranties)         {7:C2}
Less Trade-in                                              {24:C2}
Less Deposit                                               {5:C2}            {6:C2}

Brokerage                                                  {20:C2}
Protecta Product Margin                             {8:C2}
Interest Commission                                     {9:C2}       {10:C2}      {11:C2}

Less
    Loan Retention ({22:N2}%)                                       {12:C2}
    Commission Retention ({23:N2}%)                    {13:C2}       {14:C2}
    
Payouts:
    {15} Bank Account                                 {16:C2}
    {15} Commission Account                        {21:C2}
    {17} Retention Account                          {18:C2}          {19:C2}
        