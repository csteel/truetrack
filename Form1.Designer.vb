﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteEnquiryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetSizeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinPowerNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrueTrackNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DealersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MCApplicationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnquiryReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnquiriesDateRangeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivityReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DDApprovalsbyDateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayoutsCompletedbyDateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayoutsCompletedbyMonthToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CurrentStatusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarketingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmMarketingEnquiriesDateRange = New System.Windows.Forms.ToolStripMenuItem()
        Me.KPISummaryReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTxtbxVersion = New System.Windows.Forms.ToolStripTextBox()
        Me.AdministrationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NetworkToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SystemInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActiveClientEnquiriesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ActiveClientEnquiriesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnRefresh = New System.Windows.Forms.ToolStripButton()
        Me.btnSelectAllAgents = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.CheckEnquiryNamesToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MCToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.NewEnquiryToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ActiveClientEnquiriesDataGridView = New System.Windows.Forms.DataGridView()
        Me.Folder = New System.Windows.Forms.DataGridViewImageColumn()
        Me.EnquiryId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactDisplayName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CurrentStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.EnquiryManagerId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblLoggedInUserValue = New System.Windows.Forms.Label()
        Me.ActiveClientEnquiriesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveClientEnquiriesTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.cmbxBranch = New System.Windows.Forms.ComboBox()
        Me.BranchesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblSelectBranch = New System.Windows.Forms.Label()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.BranchesTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BranchesTableAdapter()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblSelectAgent = New System.Windows.Forms.Label()
        Me.cmbxAgentName = New System.Windows.Forms.ComboBox()
        Me.AMLCTReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.ActiveClientEnquiriesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ActiveClientEnquiriesBindingNavigator.SuspendLayout()
        CType(Me.ActiveClientEnquiriesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActiveClientEnquiriesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BranchesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.SearchToolStripMenuItem, Me.ReportsToolStripMenuItem, Me.ToolsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(12, 20)
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteEnquiryToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'DeleteEnquiryToolStripMenuItem
        '
        Me.DeleteEnquiryToolStripMenuItem.Name = "DeleteEnquiryToolStripMenuItem"
        Me.DeleteEnquiryToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.DeleteEnquiryToolStripMenuItem.Text = "Delete Enquiry"
        Me.DeleteEnquiryToolStripMenuItem.Visible = False
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ResetSizeToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'ResetSizeToolStripMenuItem
        '
        Me.ResetSizeToolStripMenuItem.Name = "ResetSizeToolStripMenuItem"
        Me.ResetSizeToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.ResetSizeToolStripMenuItem.Text = "Reset Size"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FinPowerNamesToolStripMenuItem, Me.TrueTrackNamesToolStripMenuItem, Me.DealersToolStripMenuItem, Me.MCApplicationsToolStripMenuItem})
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.SearchToolStripMenuItem.Text = "Search"
        '
        'FinPowerNamesToolStripMenuItem
        '
        Me.FinPowerNamesToolStripMenuItem.Name = "FinPowerNamesToolStripMenuItem"
        Me.FinPowerNamesToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.FinPowerNamesToolStripMenuItem.Text = "FinPower Names"
        '
        'TrueTrackNamesToolStripMenuItem
        '
        Me.TrueTrackNamesToolStripMenuItem.Name = "TrueTrackNamesToolStripMenuItem"
        Me.TrueTrackNamesToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.TrueTrackNamesToolStripMenuItem.Text = "TrueTrack Names"
        '
        'DealersToolStripMenuItem
        '
        Me.DealersToolStripMenuItem.Name = "DealersToolStripMenuItem"
        Me.DealersToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.DealersToolStripMenuItem.Text = "Dealers"
        '
        'MCApplicationsToolStripMenuItem
        '
        Me.MCApplicationsToolStripMenuItem.Name = "MCApplicationsToolStripMenuItem"
        Me.MCApplicationsToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.MCApplicationsToolStripMenuItem.Text = "Application Archive Search"
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EnquiryReportToolStripMenuItem, Me.ActivityReportsToolStripMenuItem, Me.StatusReportsToolStripMenuItem, Me.MarketingReportToolStripMenuItem, Me.KPISummaryReportToolStripMenuItem, Me.AMLCTReportToolStripMenuItem})
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'EnquiryReportToolStripMenuItem
        '
        Me.EnquiryReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EnquiriesDateRangeToolStripMenuItem})
        Me.EnquiryReportToolStripMenuItem.Name = "EnquiryReportToolStripMenuItem"
        Me.EnquiryReportToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.EnquiryReportToolStripMenuItem.Text = "Enquiry Reports"
        '
        'EnquiriesDateRangeToolStripMenuItem
        '
        Me.EnquiriesDateRangeToolStripMenuItem.Name = "EnquiriesDateRangeToolStripMenuItem"
        Me.EnquiriesDateRangeToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.EnquiriesDateRangeToolStripMenuItem.Text = "Enquiries (Date Range)"
        '
        'ActivityReportsToolStripMenuItem
        '
        Me.ActivityReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DDApprovalsbyDateToolStripMenuItem, Me.PayoutsCompletedbyDateToolStripMenuItem, Me.PayoutsCompletedbyMonthToolStripMenuItem})
        Me.ActivityReportsToolStripMenuItem.Name = "ActivityReportsToolStripMenuItem"
        Me.ActivityReportsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ActivityReportsToolStripMenuItem.Text = "Activity Reports"
        '
        'DDApprovalsbyDateToolStripMenuItem
        '
        Me.DDApprovalsbyDateToolStripMenuItem.Name = "DDApprovalsbyDateToolStripMenuItem"
        Me.DDApprovalsbyDateToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.DDApprovalsbyDateToolStripMenuItem.Text = "Approvals (Date Range)"
        '
        'PayoutsCompletedbyDateToolStripMenuItem
        '
        Me.PayoutsCompletedbyDateToolStripMenuItem.Name = "PayoutsCompletedbyDateToolStripMenuItem"
        Me.PayoutsCompletedbyDateToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.PayoutsCompletedbyDateToolStripMenuItem.Text = "Payouts (Date Range)"
        '
        'PayoutsCompletedbyMonthToolStripMenuItem
        '
        Me.PayoutsCompletedbyMonthToolStripMenuItem.Name = "PayoutsCompletedbyMonthToolStripMenuItem"
        Me.PayoutsCompletedbyMonthToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.PayoutsCompletedbyMonthToolStripMenuItem.Text = "Payouts (Month Range)"
        '
        'StatusReportsToolStripMenuItem
        '
        Me.StatusReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CurrentStatusToolStripMenuItem})
        Me.StatusReportsToolStripMenuItem.Name = "StatusReportsToolStripMenuItem"
        Me.StatusReportsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.StatusReportsToolStripMenuItem.Text = "Status Reports"
        '
        'CurrentStatusToolStripMenuItem
        '
        Me.CurrentStatusToolStripMenuItem.Name = "CurrentStatusToolStripMenuItem"
        Me.CurrentStatusToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.CurrentStatusToolStripMenuItem.Text = "Current Status"
        '
        'MarketingReportToolStripMenuItem
        '
        Me.MarketingReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsmMarketingEnquiriesDateRange})
        Me.MarketingReportToolStripMenuItem.Name = "MarketingReportToolStripMenuItem"
        Me.MarketingReportToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.MarketingReportToolStripMenuItem.Text = "Marketing Report"
        '
        'TsmMarketingEnquiriesDateRange
        '
        Me.TsmMarketingEnquiriesDateRange.Name = "TsmMarketingEnquiriesDateRange"
        Me.TsmMarketingEnquiriesDateRange.Size = New System.Drawing.Size(193, 22)
        Me.TsmMarketingEnquiriesDateRange.Text = "Enquiries (Date Range)"
        '
        'KPISummaryReportToolStripMenuItem
        '
        Me.KPISummaryReportToolStripMenuItem.Name = "KPISummaryReportToolStripMenuItem"
        Me.KPISummaryReportToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.KPISummaryReportToolStripMenuItem.Text = "KPI Summary report"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTxtbxVersion, Me.AdministrationToolStripMenuItem, Me.NetworkToolsToolStripMenuItem, Me.SystemInformationToolStripMenuItem, Me.ViewSettingsToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        Me.ToolsToolStripMenuItem.Visible = False
        '
        'ToolStripTxtbxVersion
        '
        Me.ToolStripTxtbxVersion.Name = "ToolStripTxtbxVersion"
        Me.ToolStripTxtbxVersion.Size = New System.Drawing.Size(100, 23)
        Me.ToolStripTxtbxVersion.Text = "Version"
        '
        'AdministrationToolStripMenuItem
        '
        Me.AdministrationToolStripMenuItem.Name = "AdministrationToolStripMenuItem"
        Me.AdministrationToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.AdministrationToolStripMenuItem.Text = "Administration"
        Me.AdministrationToolStripMenuItem.Visible = False
        '
        'NetworkToolsToolStripMenuItem
        '
        Me.NetworkToolsToolStripMenuItem.Name = "NetworkToolsToolStripMenuItem"
        Me.NetworkToolsToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.NetworkToolsToolStripMenuItem.Text = "Network Tools"
        Me.NetworkToolsToolStripMenuItem.Visible = False
        '
        'SystemInformationToolStripMenuItem
        '
        Me.SystemInformationToolStripMenuItem.Name = "SystemInformationToolStripMenuItem"
        Me.SystemInformationToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.SystemInformationToolStripMenuItem.Text = "System Information"
        Me.SystemInformationToolStripMenuItem.Visible = False
        '
        'ViewSettingsToolStripMenuItem
        '
        Me.ViewSettingsToolStripMenuItem.Name = "ViewSettingsToolStripMenuItem"
        Me.ViewSettingsToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ViewSettingsToolStripMenuItem.Text = "View Settings"
        Me.ViewSettingsToolStripMenuItem.Visible = False
        '
        'ActiveClientEnquiriesBindingNavigator
        '
        Me.ActiveClientEnquiriesBindingNavigator.AddNewItem = Nothing
        Me.ActiveClientEnquiriesBindingNavigator.BindingSource = Me.ActiveClientEnquiriesBindingSource
        Me.ActiveClientEnquiriesBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ActiveClientEnquiriesBindingNavigator.DeleteItem = Nothing
        Me.ActiveClientEnquiriesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btnRefresh, Me.btnSelectAllAgents, Me.ToolStripSeparator1, Me.CheckEnquiryNamesToolStripButton, Me.ToolStripSeparator2, Me.MCToolStripButton, Me.ToolStripSeparator3, Me.NewEnquiryToolStripButton})
        Me.ActiveClientEnquiriesBindingNavigator.Location = New System.Drawing.Point(0, 24)
        Me.ActiveClientEnquiriesBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ActiveClientEnquiriesBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ActiveClientEnquiriesBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ActiveClientEnquiriesBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ActiveClientEnquiriesBindingNavigator.Name = "ActiveClientEnquiriesBindingNavigator"
        Me.ActiveClientEnquiriesBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ActiveClientEnquiriesBindingNavigator.Size = New System.Drawing.Size(944, 25)
        Me.ActiveClientEnquiriesBindingNavigator.TabIndex = 5
        Me.ActiveClientEnquiriesBindingNavigator.Text = "BindingNavigator1"
        '
        'ActiveClientEnquiriesBindingSource
        '
        Me.ActiveClientEnquiriesBindingSource.DataMember = "ActiveClientEnquiries"
        Me.ActiveClientEnquiriesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnRefresh
        '
        Me.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRefresh.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.btnRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(23, 22)
        Me.btnRefresh.Text = "Refresh"
        '
        'btnSelectAllAgents
        '
        Me.btnSelectAllAgents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSelectAllAgents.Image = Global.AppWhShtB.My.Resources.Resources.clients16
        Me.btnSelectAllAgents.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSelectAllAgents.Name = "btnSelectAllAgents"
        Me.btnSelectAllAgents.Size = New System.Drawing.Size(23, 22)
        Me.btnSelectAllAgents.Text = "All Agents"
        Me.btnSelectAllAgents.ToolTipText = "Select All Agents"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'CheckEnquiryNamesToolStripButton
        '
        Me.CheckEnquiryNamesToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripButton.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CheckEnquiryNamesToolStripButton.Name = "CheckEnquiryNamesToolStripButton"
        Me.CheckEnquiryNamesToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.CheckEnquiryNamesToolStripButton.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripButton.ToolTipText = "Search TrueTrack Names"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'MCToolStripButton
        '
        Me.MCToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.MCToolStripButton.Image = Global.AppWhShtB.My.Resources.Resources.OAC16
        Me.MCToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MCToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.MCToolStripButton.Name = "MCToolStripButton"
        Me.MCToolStripButton.Size = New System.Drawing.Size(33, 22)
        Me.MCToolStripButton.Text = "MembersCentre"
        Me.MCToolStripButton.ToolTipText = "OAC Link"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'NewEnquiryToolStripButton
        '
        Me.NewEnquiryToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.NewEnquiryToolStripButton.Image = Global.AppWhShtB.My.Resources.Resources.NewEnquiry16
        Me.NewEnquiryToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NewEnquiryToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewEnquiryToolStripButton.Name = "NewEnquiryToolStripButton"
        Me.NewEnquiryToolStripButton.Padding = New System.Windows.Forms.Padding(0, 0, 25, 0)
        Me.NewEnquiryToolStripButton.Size = New System.Drawing.Size(125, 22)
        Me.NewEnquiryToolStripButton.Text = "New Enquiry"
        Me.NewEnquiryToolStripButton.ToolTipText = "New Enquiry"
        '
        'ActiveClientEnquiriesDataGridView
        '
        Me.ActiveClientEnquiriesDataGridView.AllowUserToAddRows = False
        Me.ActiveClientEnquiriesDataGridView.AllowUserToDeleteRows = False
        Me.ActiveClientEnquiriesDataGridView.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        Me.ActiveClientEnquiriesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.ActiveClientEnquiriesDataGridView.AutoGenerateColumns = False
        Me.ActiveClientEnquiriesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ActiveClientEnquiriesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Folder, Me.EnquiryId, Me.EnquiryCode, Me.DataGridViewTextBoxColumn2, Me.ContactDisplayName, Me.CurrentStatus, Me.DataGridViewTextBoxColumn4, Me.UserName, Me.DataGridViewTextBoxColumn5, Me.DataGridViewCheckBoxColumn1, Me.EnquiryManagerId})
        Me.ActiveClientEnquiriesDataGridView.DataSource = Me.ActiveClientEnquiriesBindingSource
        Me.ActiveClientEnquiriesDataGridView.Location = New System.Drawing.Point(0, 79)
        Me.ActiveClientEnquiriesDataGridView.MultiSelect = False
        Me.ActiveClientEnquiriesDataGridView.Name = "ActiveClientEnquiriesDataGridView"
        Me.ActiveClientEnquiriesDataGridView.ReadOnly = True
        Me.ActiveClientEnquiriesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ActiveClientEnquiriesDataGridView.Size = New System.Drawing.Size(944, 910)
        Me.ActiveClientEnquiriesDataGridView.TabIndex = 3
        '
        'Folder
        '
        Me.Folder.HeaderText = ""
        Me.Folder.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.Folder.Name = "Folder"
        Me.Folder.ReadOnly = True
        Me.Folder.Width = 30
        '
        'EnquiryId
        '
        Me.EnquiryId.DataPropertyName = "EnquiryId"
        Me.EnquiryId.HeaderText = "EnquiryId"
        Me.EnquiryId.Name = "EnquiryId"
        Me.EnquiryId.ReadOnly = True
        Me.EnquiryId.Visible = False
        '
        'EnquiryCode
        '
        Me.EnquiryCode.DataPropertyName = "EnquiryCode"
        Me.EnquiryCode.HeaderText = "Enquiry"
        Me.EnquiryCode.Name = "EnquiryCode"
        Me.EnquiryCode.ReadOnly = True
        Me.EnquiryCode.Width = 60
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "ApplicationCode"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Application"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'ContactDisplayName
        '
        Me.ContactDisplayName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ContactDisplayName.DataPropertyName = "ContactDisplayName"
        Me.ContactDisplayName.HeaderText = "ContactDisplayName"
        Me.ContactDisplayName.Name = "ContactDisplayName"
        Me.ContactDisplayName.ReadOnly = True
        '
        'CurrentStatus
        '
        Me.CurrentStatus.DataPropertyName = "CurrentStatus"
        Me.CurrentStatus.HeaderText = "Status"
        Me.CurrentStatus.Name = "CurrentStatus"
        Me.CurrentStatus.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "DateTime"
        Me.DataGridViewTextBoxColumn4.HeaderText = "DateTime"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 75
        '
        'UserName
        '
        Me.UserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.UserName.DataPropertyName = "UserName"
        Me.UserName.HeaderText = "Manager"
        Me.UserName.Name = "UserName"
        Me.UserName.ReadOnly = True
        Me.UserName.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "BranchId"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Branch"
        Me.DataGridViewTextBoxColumn5.MinimumWidth = 50
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "ActiveStatus"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "ActiveStatus"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        Me.DataGridViewCheckBoxColumn1.Visible = False
        '
        'EnquiryManagerId
        '
        Me.EnquiryManagerId.DataPropertyName = "EnquiryManagerId"
        Me.EnquiryManagerId.HeaderText = "EnquiryManagerId"
        Me.EnquiryManagerId.Name = "EnquiryManagerId"
        Me.EnquiryManagerId.ReadOnly = True
        Me.EnquiryManagerId.Visible = False
        '
        'lblLoggedInUserValue
        '
        Me.lblLoggedInUserValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLoggedInUserValue.AutoSize = True
        Me.lblLoggedInUserValue.BackColor = System.Drawing.Color.Snow
        Me.lblLoggedInUserValue.Location = New System.Drawing.Point(687, 5)
        Me.lblLoggedInUserValue.Name = "lblLoggedInUserValue"
        Me.lblLoggedInUserValue.Size = New System.Drawing.Size(29, 13)
        Me.lblLoggedInUserValue.TabIndex = 4
        Me.lblLoggedInUserValue.Text = "User"
        '
        'ActiveClientEnquiriesTableAdapter
        '
        Me.ActiveClientEnquiriesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'cmbxBranch
        '
        Me.cmbxBranch.DataSource = Me.BranchesBindingSource
        Me.cmbxBranch.DisplayMember = "FullName"
        Me.cmbxBranch.FormattingEnabled = True
        Me.cmbxBranch.Location = New System.Drawing.Point(361, 52)
        Me.cmbxBranch.Name = "cmbxBranch"
        Me.cmbxBranch.Size = New System.Drawing.Size(186, 21)
        Me.cmbxBranch.TabIndex = 14
        Me.cmbxBranch.ValueMember = "BranchId"
        '
        'BranchesBindingSource
        '
        Me.BranchesBindingSource.DataMember = "Branches"
        Me.BranchesBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblSelectBranch
        '
        Me.lblSelectBranch.AutoSize = True
        Me.lblSelectBranch.Location = New System.Drawing.Point(267, 55)
        Me.lblSelectBranch.Name = "lblSelectBranch"
        Me.lblSelectBranch.Size = New System.Drawing.Size(88, 13)
        Me.lblSelectBranch.TabIndex = 13
        Me.lblSelectBranch.Text = "Select by Branch"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'BranchesTableAdapter
        '
        Me.BranchesTableAdapter.ClearBeforeFill = True
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 990)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(944, 22)
        Me.StatusStripMessage.TabIndex = 15
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'lblSelectAgent
        '
        Me.lblSelectAgent.AutoSize = True
        Me.lblSelectAgent.Location = New System.Drawing.Point(13, 55)
        Me.lblSelectAgent.Name = "lblSelectAgent"
        Me.lblSelectAgent.Size = New System.Drawing.Size(82, 13)
        Me.lblSelectAgent.TabIndex = 17
        Me.lblSelectAgent.Text = "Select Manager"
        '
        'cmbxAgentName
        '
        Me.cmbxAgentName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxAgentName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxAgentName.DataSource = Me.UsersBindingSource
        Me.cmbxAgentName.DisplayMember = "UserName"
        Me.cmbxAgentName.FormattingEnabled = True
        Me.cmbxAgentName.Location = New System.Drawing.Point(99, 52)
        Me.cmbxAgentName.Name = "cmbxAgentName"
        Me.cmbxAgentName.Size = New System.Drawing.Size(157, 21)
        Me.cmbxAgentName.TabIndex = 16
        Me.cmbxAgentName.ValueMember = "UserId"
        '
        'AMLCTReportToolStripMenuItem
        '
        Me.AMLCTReportToolStripMenuItem.Name = "AMLCTReportToolStripMenuItem"
        Me.AMLCTReportToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AMLCTReportToolStripMenuItem.Text = "AML/CT report"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.Form1Size
        Me.Controls.Add(Me.cmbxBranch)
        Me.Controls.Add(Me.lblSelectBranch)
        Me.Controls.Add(Me.ActiveClientEnquiriesDataGridView)
        Me.Controls.Add(Me.lblSelectAgent)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.cmbxAgentName)
        Me.Controls.Add(Me.lblLoggedInUserValue)
        Me.Controls.Add(Me.ActiveClientEnquiriesBindingNavigator)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "Form1Location", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "Form1Size", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.Form1Location
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "TrueTrack List"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.ActiveClientEnquiriesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ActiveClientEnquiriesBindingNavigator.ResumeLayout(False)
        Me.ActiveClientEnquiriesBindingNavigator.PerformLayout()
        CType(Me.ActiveClientEnquiriesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActiveClientEnquiriesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BranchesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministrationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents ActiveClientEnquiriesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActiveClientEnquiriesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveClientEnquiriesTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ActiveClientEnquiriesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ActiveClientEnquiriesDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents lblLoggedInUserValue As System.Windows.Forms.Label
    Friend WithEvents btnRefresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmbxBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblSelectBranch As System.Windows.Forms.Label
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents BranchesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BranchesTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.BranchesTableAdapter
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NetworkToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnquiryReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSelectAllAgents As System.Windows.Forms.ToolStripButton
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SystemInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetSizeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripTxtbxVersion As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents NewEnquiryToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CheckEnquiryNamesToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DeleteEnquiryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FinPowerNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrueTrackNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblSelectAgent As System.Windows.Forms.Label
    Friend WithEvents cmbxAgentName As System.Windows.Forms.ComboBox
    Friend WithEvents ActivityReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DDApprovalsbyDateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayoutsCompletedbyDateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayoutsCompletedbyMonthToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CurrentStatusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnquiriesDateRangeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DealersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MCToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MCApplicationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MarketingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsmMarketingEnquiriesDateRange As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Folder As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents EnquiryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnquiryCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactDisplayName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CurrentStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents EnquiryManagerId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KPISummaryReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AMLCTReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
