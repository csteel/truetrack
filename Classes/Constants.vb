﻿Public Class Constants

    'CURRENT_STATUS
    Public Const CONST_CURRENT_STATUS_PRELIMINARY As String = "Preliminary"
    Public Const CONST_CURRENT_STATUS_COMPLETED As String = "Completed"
    'Public Const CONST_CURRENT_STATUS_SAVED As String = "Saved"
    Public Const CONST_CURRENT_STATUS_WITHDRAWN As String = "Withdrawn"
    Public Const CONST_CURRENT_STATUS_ENQUIRY_ENDED As String = "Enquiry Ended"
    Public Const CONST_CURRENT_STATUS_DECLINED As String = "Declined"
    Public Const CONST_CURRENT_STATUS_FOLLOW_UP As String = "Follow-up"
    Public Const CONST_CURRENT_STATUS_DUE_DILIGENCE As String = "Due Diligence"
    Public Const CONST_CURRENT_STATUS_CAD_APPROVED As String = "CAD Approved"
    Public Const CONST_CURRENT_STATUS_AUDIT As String = "Audit"
    Public Const CONST_CURRENT_STATUS_AUDIT_FAILED As String = "Audit Failed"
    Public Const CONST_CURRENT_STATUS_PAYOUT As String = "Payout"
    Public Const CONST_CURRENT_STATUS_EXPORTED As String = "Exported"

    'CUSTOMERTYPE
    'Customer.CustomerType from the OAC: Partnership,Company,Trust,Sole trader,Individual
    Public Const CONST_OAC_CUSTOMERTYPE_INDIVIDUAL As String = "Individual"
    Public Const CONST_OAC_CUSTOMERTYPE_COMPANY As String = "Company"
    Public Const CONST_OAC_CUSTOMERTYPE_PARTNERSHIP As String = "Partnership"
    Public Const CONST_OAC_CUSTOMERTYPE_TRUST As String = "Trust"
    Public Const CONST_OAC_CUSTOMERTYPE_SOLETRADER As String = "Sole Trader"

    'CustomerApplication.Type from the OAC
    Public Const CONST_OAC_CUSTAPPTYPE_MAIN As String = "main"
    Public Const CONST_OAC_CUSTAPPTYPE_COMAIN As String = "coMain"
    Public Const CONST_OAC_CUSTAPPTYPE_GUARANTOR As String = "guarantor"
    Public Const CONST_OAC_CUSTAPPTYPE_PERSONACTING As String = "personActing"

    'LOAN TYPE
    Public Const CONST_PRELIMREASON_PERSONAL_LOAN_NEW As String = "Personal Loan (New)"
    Public Const CONST_PRELIMREASON_PERSONAL_LOAN_REFINANCE As String = "Personal Loan (Refinance)"
    Public Const CONST_PRELIMREASON_PERSONAL_LOAN_VARIATION As String = "Personal Loan (Variation)"
    Public Const CONST_PRELIMREASON_CONSUMER_RETAIL As String = "Consumer Retail"
    Public Const CONST_PRELIMREASON_BUSINESS_LOAN As String = "Business Loan"
    Public Const CONST_PRELIMREASON_BUSINESS_LOAN_VARIATION As String = "Business Loan (Variation)"
    Public Const CONST_PRELIMREASON_CONSUMER_VEHICLE As String = "Consumer Vehicle"
    Public Const CONST_PRELIMREASON_SPLIT_LOAN_DRAWDOWN As String = "Split Loan Drawdown"

    Public Const CONST_LOANREASON_PERSONAL_LOAN_NEW As String = "Personal Loan (New)"
    Public Const CONST_LOANREASON_PERSONAL_LOAN_REFINANCE As String = "Personal Loan (Refinance)"
    Public Const CONST_LOANREASON_PERSONAL_LOAN_VARIATION As String = "Personal Loan (Variation)"
    Public Const CONST_LOANREASON_CONSUMER_RETAIL As String = "Consumer Retail"
    Public Const CONST_LOANREASON_BUSINESS_LOAN As String = "Business Loan"
    Public Const CONST_LOANREASON_BUSINESS_LOAN_VARIATION As String = "Business Loan (Variation)"
    Public Const CONST_LOANREASON_CONSUMER_VEHICLE As String = "Consumer Vehicle"
    Public Const CONST_LOANREASON_SPLIT_LOAN_DRAWDOWN As String = "Split Loan Drawdown"


    'LOG TYPE
    Public Const CONST_LATENCY_POINTS_LOG_TYPE_CLIENT As String = "Client"
    Public Const CONST_LATENCY_POINTS_LOG_TYPE_LOAN As String = "Loan"

    'SECURITY TYPES
    Public Const CONST_SEC_BOAT As String = "BT"
    Public Const CONST_SEC_COMMERCIALMACHINERY As String = "CM"
    Public Const CONST_SEC_LIVESTOCK As String = "LS"
    Public Const CONST_SEC_MOTORVEHICLE As String = "MV"
    Public Const CONST_SEC_HOUSEHOLD As String = "HS"
    Public Const CONST_SEC_MISCNONSERIAL As String = "MN"
    Public Const CONST_SEC_MISCSERIAL As String = "MS"
    Public Const CONST_SEC_NEGOTIABLEINSTRUM As String = "NI"

    'DEALER CONTACT METHODS
    Public Const CONST_CM_EMAIL As String = "Email"
    Public Const CONST_CM_FACSIMILE As String = "Facsimile"
    Public Const CONST_CM_MOBILE As String = "Mobile"
    Public Const CONST_CM_PHONE As String = "Phone"

    

End Class
