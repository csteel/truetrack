﻿'version no:2.3.0.1(13.05)
Public Class ApplicationAccessRights

    Public Shared Function isAdministrator() As Boolean
        'Administrator Level, Can see the tool menu. Can use the multiple Archive wizard in the Tool menu.
        If LoggedinPermissionLevel = MyEnums.RolePermissions.Administrator Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function isSystemsManager() As Boolean
        'Systems Manager Level, Open.
        If LoggedinPermissionLevel = MyEnums.RolePermissions.SystemsAdministrator Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Can by pass document checks for All TrueTrack security level 4 and above 
    ''' ie. manager and administrator 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function canbyPassApplicationDocumentChecks() As Boolean
        '2.3.0.1(13.05)
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Manager Then
            Return True
        Else
            Return False
        End If


    End Function

    Public Shared Function canViewManualLoanCodeBtn() As Boolean
        '2.5.0
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Administrator Then
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Can change the Customer's name in customer due-diligence
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks>Used in SetCustomerTypePanel</remarks>
    Public Shared Function CanUpdateCustomerName() As Boolean
        '3.1.0
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Administrator Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Can edit comments
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Shared Function CanEditComments() As Boolean
        If LoggedinPermissionLevel >= MyEnums.RolePermissions.Manager Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
