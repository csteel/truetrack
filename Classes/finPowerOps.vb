﻿Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.Text
Imports System.Configuration

''' <summary>
''' Functions and Subs for operations involving finpower
''' </summary>
''' <remarks></remarks>
Public Class finPowerOps : Inherits ApplicationInterchange
    Public ClientNames As New Dictionary(Of Integer, String) 'Clients with match in finPower: ClientXMLInfo.key and dispName
    Public EnquiryCode As String
    Public ApplicationCode As String
    Private _LoanCode As String
    Private _ClientId As String
    Public ClientXmlInfo As New ClientInfo
    Public ErrorMessage As String
    Public EnquiryId As Integer
    Private xmlClients As New Dictionary(Of Integer, finPowerOps.CustomerInfo) 'list of xmlClientInfo.Clients (Name, CustomerId)
    Private trueTrackCustomers As New Dictionary(Of Integer, finPowerOps.CustomerInfo) 'list of ttCustomerInfo.Customers (XRefId, Name, CustomerId)
    Private enquiryType As Integer
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Property LoanCode() As String
        Get
            If _LoanCode Is Nothing Then
                Return String.Empty
            Else
                Return _LoanCode
            End If
        End Get
        Set(ByVal value As String)
            _LoanCode = value
        End Set
    End Property

    Public Property ClientId() As String
        Get
            Return _ClientId
        End Get
        Set(ByVal value As String)
            _ClientId = value
        End Set
    End Property


    Public Sub New(ByVal enquiry_Code As String, ByVal application_Code As String)
        EnquiryCode = enquiry_Code
        ApplicationCode = application_Code
    End Sub

    Public Sub New(ByVal enquiry_Code As String, ByVal application_Code As String, ByVal enquiry_Id As Integer)
        EnquiryCode = enquiry_Code
        ApplicationCode = application_Code
        EnquiryId = enquiry_Id
    End Sub

    Public Sub New(ByVal enquiry_Code As String, ByVal application_Code As String, ByVal enquiry_Id As Integer, ByVal enquiry_Type As Integer)
        EnquiryCode = enquiry_Code
        ApplicationCode = application_Code
        EnquiryId = enquiry_Id
        enquiryType = enquiry_Type
    End Sub

    ''' <summary>
    ''' Checks for any match between XML and finPOWER and adds to ClientNames
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks>Fills ClientXmlInfo and adds matching (with finPower clients) customer's display name to ClientNames dictionary. Starts adding at i = 1.</remarks>
    Public Function CheckClientNames() As Boolean

        Dim dispName As String = ""
        Dim i As Integer = 0
        Dim retFlag As Boolean
        'Dim htXML As New Hashtable 'Data from the XML for Import
        'Build hashTable from the XML

        ClientXmlInfo = GetXmlClientInfo() 'Key (1) starts at first client in XML

        'htXML = ClientXmlInfo.htClient

        If ClientXmlInfo Is Nothing OrElse ClientXmlInfo.Status = False Then Return False

        Dim clientfinPow As ClientMappingDetails

        For i = 1 To ClientXmlInfo.Clients.Count
            'for all xml clients construct dispName
            If ClientXmlInfo.Clients.ContainsKey(i) Then
                clientfinPow = CType(ClientXmlInfo.Clients.Item(i), ClientMappingDetails)

                Select Case clientfinPow.CustomerType
                    Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                        dispName = (clientfinPow.FirstName & " " & clientfinPow.LastName).Trim
                        'If clientfinPow.Comment.ToUpper = "AKA" Then
                        '    dispName += " (AKA)"
                        'End If
                    Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                        dispName = clientfinPow.LegalName
                    Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                        dispName = clientfinPow.CompanyName
                    Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                        dispName = clientfinPow.CompanyName
                End Select

                If CheckClientExistsinfinPOWER(clientfinPow) Then
                    'if xml client in finPower then add ClientXMLInfo.key and dispName to ClientNames dictionary
                    ClientNames.Add(i, dispName)
                End If
            End If
        Next
        If ClientNames.Count > 0 Then
            retFlag = True
        Else
            'No clientfinPow match in finPower. 
            'If EnquiryType is a Finance Facility then as the ClientId is created by the OAC then add ClientId to ttCustomer (main) where XRefId = ...
            'Beyond this point the XML documents is moved to finPower Import folder

            If enquiryType = MyEnums.EnquiryType.FinanceFacility Then
                Dim ttDt As DataTable
                ttDt = GetEnquiryCustomers(EnquiryId)

                For i = 1 To ClientXmlInfo.Clients.Count
                    If ClientXmlInfo.Clients.ContainsKey(i) Then
                        clientfinPow = CType(ClientXmlInfo.Clients.Item(i), ClientMappingDetails)
                        'clientfinPow.Type is not in XML for FinanceFacility so will always be Nothing, so can not test if main customer
                        If Not String.IsNullOrEmpty(clientfinPow.ClientId) Then
                            If Not ttDt Is Nothing Then
                                If ttDt.Rows.Count > 0 Then
                                    For Each row As DataRow In ttDt.Rows
                                        If row.Item("XREfId") = clientfinPow.XREfId Then
                                            Try
                                                UpdateCustClientId(CInt(row.Item("CustomerId")), clientfinPow.ClientId, clientfinPow.ClientTypeId)
                                            Catch ex As Exception
                                                log.Error("Error in CheckClientNames():" & ex.Message & " : " & ex.TargetSite.ToString)
                                                MsgBox("Error in CheckClientNames()")
                                            End Try
                                        End If
                                    Next row
                                End If
                            End If
                        End If

                    End If
                Next
            End If

        End If
        Return retFlag

    End Function

    ''' <summary>
    ''' Get Client data from the XML file (key starts at 1)
    ''' </summary>
    ''' <returns>ClientInfo</returns>
    ''' <remarks>Reads XML client elements one by one, adds to hashtable (Collection). Returns hashtable Collection of ClientEntity</remarks>
    Public Function GetXmlClientInfo() As finPowerOps.ClientInfo
        Dim clientDetailXml As New finPowerOps.ClientInfo
        'Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim xdoc As New XmlDocument
        Dim clientMap As New ClientMappingDetails
        Dim nodeListChild As XmlNodeList
        Dim nodeListAddress As XmlNodeList
        Dim xNodeJointName As XmlNode
        Dim xNodeChild As XmlNode
        Dim xNodeAddress As XmlNode
        Dim xNodeAddresses As XmlNode
        Dim xNodeContactMethods As XmlNode
        Dim xRootNodeList As XmlNodeList
        Dim inputXmlPath As String
        Dim xmlAttr As XmlAttribute
        Dim xNode As XmlNode
        Dim xNodeRoot As XmlNode
        Dim i As Integer = 0
        Dim j As Integer = 0
        'Dim tempString As String = ""
        'Dim clientContactMethods As New Dictionary(Of Integer, ContactMethods)

        Try
            '=====================================================
            'Get Client data from the XML file
            '=====================================================
            inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & ApplicationInterchange.GetXMLFileName(ApplicationCode)
            '=====================================================
            'TODO:For XML testing
            'inputXmlPath = "C:\Enquiry\testClient.xml"
            '=====================================================

            clientDetailXml.Status = False

            'if file does not exists
            If System.IO.File.Exists(inputXmlPath) Then

                Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)
                xdoc.LoadXml(xmlFile)
                xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
                'Need to branch here if EnquiryType is FinanceFacility as this EnquiryType has no loan
                If Not xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan") Is Nothing Then
                    xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")
                    If xNode IsNot Nothing Then
                        'Get loan code from XML
                        xmlAttr = xNode.Attributes("LoanId")
                        If Not xmlAttr Is Nothing Then
                            LoanCode = xmlAttr.Value
                        End If
                        'Clientfin.Type = xNode.InnerText
                    End If

                    '=====================================================
                    'Reads client elements one by one
                    '=====================================================
                    xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
                    '"/finPOWER/Loans/Loan/JointNames/JointName/Client/ContactMethods"
                    For Each xNodeJointName In xRootNodeList
                        clientMap = New ClientMappingDetails
                        i += 1
                        clientMap.Sequence = i
                        xNode = xNodeJointName.SelectSingleNode("Type")
                        If xNode IsNot Nothing Then
                            clientMap.Type = xNode.InnerText 'main, co-main, guarantor
                        End If
                        xNode = xNodeJointName.SelectSingleNode("Comment")
                        If xNode IsNot Nothing Then
                            clientMap.Comment = xNode.InnerText
                        End If
                        nodeListChild = xNodeJointName.SelectNodes("Client")

                        'this loop could be common to both branches
                        For Each xNodeChild In nodeListChild
                            xNode = xNodeChild.SelectSingleNode("LastName")
                            If xNode IsNot Nothing Then
                                clientMap.LastName = xNode.InnerText
                            End If
                            xNode = xNodeChild.SelectSingleNode("MiddleNames")
                            If xNode IsNot Nothing Then
                                clientMap.MiddleNames = xNode.InnerText
                            End If
                            xNode = xNodeChild.SelectSingleNode("FirstName")
                            If xNode IsNot Nothing Then
                                clientMap.FirstName = xNode.InnerText
                            End If
                            xNode = xNodeChild.SelectSingleNode("DateOfBirth")
                            If xNode IsNot Nothing Then
                                clientMap.DateOfBirth = If(xNode.InnerText = "", "", DateTime.Parse(xNode.InnerText).ToString("dd/MM/yyyy"))
                            End If
                            xNode = xNodeChild.SelectSingleNode("Type")
                            If xNode IsNot Nothing Then
                                clientMap.CustomerType = xNode.InnerText
                            End If
                            'added 09/09/2016
                            xNode = xNodeChild.SelectSingleNode("XRefId")
                            If xNode IsNot Nothing Then
                                clientMap.XREfId = xNode.InnerText
                            End If

                            xNode = xNodeChild.SelectSingleNode("ManagerId")
                            If xNode IsNot Nothing Then
                                clientMap.ManagerId = xNode.InnerText
                            End If
                            xNode = xNodeChild.SelectSingleNode("CompanyName")
                            If xNode IsNot Nothing Then
                                clientMap.CompanyName = xNode.InnerText
                            End If
                            xNode = xNodeChild.SelectSingleNode("LegalName")
                            If xNode IsNot Nothing Then
                                clientMap.LegalName = xNode.InnerText
                            End If
                            xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                            If xNodeAddresses IsNot Nothing Then
                                nodeListAddress = xNodeAddresses.SelectNodes("Address[@Type='Physical']")
                                For Each xNodeAddress In nodeListAddress
                                    If xNodeAddress IsNot Nothing Then
                                        xNode = xNodeAddress.SelectSingleNode("Address")
                                        If xNode IsNot Nothing Then
                                            clientMap.AddressPhysical = xNode.InnerText
                                        End If
                                        xNode = xNodeAddress.SelectSingleNode("City")
                                        If xNode IsNot Nothing Then
                                            clientMap.CityPhysical = xNode.InnerText
                                        End If
                                        xNode = xNodeAddress.SelectSingleNode("State")
                                        If xNode IsNot Nothing Then
                                            clientMap.StatePhysical = xNode.InnerText
                                        End If
                                        xNode = xNodeAddress.SelectSingleNode("Postcode")
                                        If xNode IsNot Nothing Then
                                            clientMap.PostCodePhysical = xNode.InnerText
                                        End If
                                    End If
                                Next
                            End If
                            xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                            If xNodeAddresses IsNot Nothing Then
                                nodeListAddress = xNodeAddresses.SelectNodes("Address[@Type='Postal']")
                                For Each xNodeAddress In nodeListAddress
                                    If xNodeAddress IsNot Nothing Then
                                        xNode = xNodeAddress.SelectSingleNode("Address")
                                        If xNode IsNot Nothing Then
                                            clientMap.AddressPostal = xNode.InnerText
                                        End If
                                        xNode = xNodeAddress.SelectSingleNode("City")
                                        If xNode IsNot Nothing Then
                                            clientMap.CityPostal = xNode.InnerText
                                        End If
                                        xNode = xNodeAddress.SelectSingleNode("State")
                                        If xNode IsNot Nothing Then
                                            clientMap.StatePostal = xNode.InnerText
                                        End If
                                        xNode = xNodeAddress.SelectSingleNode("Postcode")
                                        If xNode IsNot Nothing Then
                                            clientMap.PostCodePostal = xNode.InnerText
                                        End If
                                    End If
                                Next
                            End If
                            xNodeContactMethods = xNodeChild.SelectSingleNode("ContactMethods")
                            If xNodeAddresses IsNot Nothing Then
                                j = 0
                                clientMap.ContactMethodsOACXml = New List(Of ContactMethods)
                                'nodeListAddress = xNodeAddresses.SelectNodes("Address[@Type='Postal']")
                                For Each xNode In xNodeContactMethods
                                    Dim attr As XmlAttribute
                                    attr = xNode.Attributes("Method")
                                    If (attr IsNot Nothing) Then
                                        j += 1
                                        clientMap.ContactMethodsOACXml.Add(New ContactMethods(attr.Value, xNode.InnerText))
                                    End If
                                Next
                            End If
                            'xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Mobile']")
                            'If xNode IsNot Nothing Then
                            '    Clientfin.PhoneMobile = xNode.InnerText
                            'End If
                            'xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Phone']") '21/01/2014 changed 'Home' to 'Phone'
                            'If xNode IsNot Nothing Then
                            '    Clientfin.PhoneHome = xNode.InnerText
                            'End If
                            'xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Work']")
                            'If xNode IsNot Nothing Then
                            '    Clientfin.PhoneWork = xNode.InnerText
                            'End If
                            'xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Email']")
                            'If xNode IsNot Nothing Then
                            '    Clientfin.Email = xNode.InnerText
                            'End If
                        Next
                        '=====================================================
                        'temp store of all client data from XML; adds to hashtable (Collection)
                        '=====================================================
                        'get the display name
                        Select Case clientMap.CustomerType
                            Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                                clientMap.ApplicantName = (clientMap.FirstName & " " & clientMap.LastName).Trim
                            Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                                clientMap.ApplicantName = clientMap.LegalName
                            Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                                clientMap.ApplicantName = clientMap.CompanyName
                            Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                                clientMap.ApplicantName = clientMap.CompanyName
                        End Select

                        clientDetailXml.Clients.Add(i, clientMap)
                    Next

                    clientDetailXml.Status = True

                ElseIf Not xNodeRoot.SelectSingleNode("/finPOWER/Clients/Client") Is Nothing Then
                    xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Clients/Client")
                    '=====================================================
                    'Reads client elements one by one
                    '=====================================================

                    For Each xNodeChild In xRootNodeList
                        clientMap = New ClientMappingDetails
                        i += 1
                        clientMap.Sequence = i

                        'Get client code from XML
                        xmlAttr = xNodeChild.Attributes("ClientId")
                        If Not xmlAttr Is Nothing Then
                            clientMap.ClientId = xmlAttr.Value
                        End If

                        xNode = xNodeChild.SelectSingleNode("LastName")
                        If xNode IsNot Nothing Then
                            clientMap.LastName = xNode.InnerText
                        End If
                        xNode = xNodeChild.SelectSingleNode("MiddleNames")
                        If xNode IsNot Nothing Then
                            clientMap.MiddleNames = xNode.InnerText
                        End If
                        xNode = xNodeChild.SelectSingleNode("FirstName")
                        If xNode IsNot Nothing Then
                            clientMap.FirstName = xNode.InnerText
                        End If
                        xNode = xNodeChild.SelectSingleNode("DateOfBirth")
                        If xNode IsNot Nothing Then
                            clientMap.DateOfBirth = If(xNode.InnerText = "", "", DateTime.Parse(xNode.InnerText).ToString("dd/MM/yyyy"))
                        End If
                        xNode = xNodeChild.SelectSingleNode("Type")
                        If xNode IsNot Nothing Then
                            clientMap.CustomerType = xNode.InnerText
                        End If
                        'added 09/09/2016
                        xNode = xNodeChild.SelectSingleNode("XRefId")
                        If xNode IsNot Nothing Then
                            clientMap.XREfId = xNode.InnerText
                        End If
                        'added 10/02/2017
                        xNode = xNodeChild.SelectSingleNode("ClientTypeId")
                        If xNode IsNot Nothing Then
                            clientMap.ClientTypeId = xNode.InnerText
                        End If

                        xNode = xNodeChild.SelectSingleNode("ManagerId")
                        If xNode IsNot Nothing Then
                            clientMap.ManagerId = xNode.InnerText
                        End If
                        xNode = xNodeChild.SelectSingleNode("CompanyName")
                        If xNode IsNot Nothing Then
                            clientMap.CompanyName = xNode.InnerText
                        End If
                        xNode = xNodeChild.SelectSingleNode("LegalName")
                        If xNode IsNot Nothing Then
                            clientMap.LegalName = xNode.InnerText
                        End If
                        xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                        If xNodeAddresses IsNot Nothing Then
                            nodeListAddress = xNodeAddresses.SelectNodes("Address[@Type='Physical']")
                            For Each xNodeAddress In nodeListAddress
                                If xNodeAddress IsNot Nothing Then
                                    xNode = xNodeAddress.SelectSingleNode("Address")
                                    If xNode IsNot Nothing Then
                                        clientMap.AddressPhysical = xNode.InnerText
                                    End If
                                    xNode = xNodeAddress.SelectSingleNode("City")
                                    If xNode IsNot Nothing Then
                                        clientMap.CityPhysical = xNode.InnerText
                                    End If
                                    xNode = xNodeAddress.SelectSingleNode("State")
                                    If xNode IsNot Nothing Then
                                        clientMap.StatePhysical = xNode.InnerText
                                    End If
                                    xNode = xNodeAddress.SelectSingleNode("Postcode")
                                    If xNode IsNot Nothing Then
                                        clientMap.PostCodePhysical = xNode.InnerText
                                    End If
                                End If
                            Next
                        End If
                        xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                        If xNodeAddresses IsNot Nothing Then
                            nodeListAddress = xNodeAddresses.SelectNodes("Address[@Type='Postal']")
                            For Each xNodeAddress In nodeListAddress
                                If xNodeAddress IsNot Nothing Then
                                    xNode = xNodeAddress.SelectSingleNode("Address")
                                    If xNode IsNot Nothing Then
                                        clientMap.AddressPostal = xNode.InnerText
                                    End If
                                    xNode = xNodeAddress.SelectSingleNode("City")
                                    If xNode IsNot Nothing Then
                                        clientMap.CityPostal = xNode.InnerText
                                    End If
                                    xNode = xNodeAddress.SelectSingleNode("State")
                                    If xNode IsNot Nothing Then
                                        clientMap.StatePostal = xNode.InnerText
                                    End If
                                    xNode = xNodeAddress.SelectSingleNode("Postcode")
                                    If xNode IsNot Nothing Then
                                        clientMap.PostCodePostal = xNode.InnerText
                                    End If
                                End If
                            Next
                        End If
                        xNodeContactMethods = xNodeChild.SelectSingleNode("ContactMethods")
                        If xNodeAddresses IsNot Nothing Then
                            j = 0
                            clientMap.ContactMethodsOACXml = New List(Of ContactMethods)
                            'nodeListAddress = xNodeAddresses.SelectNodes("Address[@Type='Postal']")
                            For Each xNode In xNodeContactMethods
                                Dim attr As XmlAttribute
                                attr = xNode.Attributes("Method")
                                If (attr IsNot Nothing) Then
                                    j += 1
                                    clientMap.ContactMethodsOACXml.Add(New ContactMethods(attr.Value, xNode.InnerText))
                                End If
                            Next
                        End If


                        '=====================================================
                        'temp store of all client data from XML; adds to hashtable (Collection)
                        '=====================================================
                        'get the display name
                        Select Case clientMap.CustomerType
                            Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                                clientMap.ApplicantName = (clientMap.FirstName & " " & clientMap.LastName).Trim
                            Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                                clientMap.ApplicantName = clientMap.LegalName
                            Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                                clientMap.ApplicantName = clientMap.CompanyName
                            Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                                clientMap.ApplicantName = clientMap.CompanyName
                        End Select

                        clientDetailXml.Clients.Add(i, clientMap)

                    Next

                    clientDetailXml.Status = True

                End If





            End If ' folder or Xml file does not exist
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            clientDetailXml.Status = False
            clientDetailXml.ErrorMessage = ex.Message
        End Try


        Return clientDetailXml

    End Function


    ''' <summary>
    ''' Checks name against Clients in finpower
    ''' </summary>
    ''' <param name="ClientTransformObj"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Returns true if atleast one match exists in finPower</remarks>
    Public Function CheckClientExistsinfinPOWER(ByVal ClientTransformObj As ClientMappingDetails) As Boolean
        Dim NumChars As Integer = 3
        Dim connectionstring As String
        Dim sFName As String = ""
        Dim sLName As String = ""
        Dim iCount As Integer

        Dim sbrSQL As New StringBuilder

        Dim sFirstName As String = If(ClientTransformObj.FirstName Is Nothing, "", ClientTransformObj.FirstName)
        Dim sLastName As String = If(ClientTransformObj.LastName Is Nothing, "", ClientTransformObj.LastName)
        Dim DateOfBirth As String = ClientTransformObj.DateOfBirth
        Dim sLegalName As String = If(ClientTransformObj.LegalName Is Nothing, "", ClientTransformObj.LegalName)
        Dim sCompanyName As String = If(ClientTransformObj.CompanyName Is Nothing, "", ClientTransformObj.CompanyName)

        If Not sFirstName = "" Then
            If sFirstName.Length > 3 Then
                sFirstName = sFirstName.Substring(0, 3)
            End If
        End If

        If Not sLastName = "" Then
            NumChars = If(sLastName.IndexOf("'") > -1, 4, 3)
            If sLastName.Length > NumChars Then
                sLastName = sLastName.Substring(0, NumChars)
            End If
        End If

        If Not sLegalName = "" Then
            NumChars = If(sLegalName.IndexOf("'") > -1, 4, 3)
            If sLegalName.Length > NumChars Then
                sLegalName = sLegalName.Substring(0, NumChars)
            End If
        End If

        If Not sCompanyName = "" Then
            NumChars = If(sCompanyName.IndexOf("'") > -1, 4, 3)
            If sCompanyName.Length > NumChars Then
                sCompanyName = sCompanyName.Substring(0, NumChars)
            End If
        End If



        'strSelect = "SELECT ClientId, Type , FirstName, MiddleNames, LastName, DateofBirth, PLAddress, PLCity, PLState, PLPostCode  From Client " & _
        '        " Where LastName LIKE '" & sLName & "%' AND FirstName LIKE '" & sFName & "%' AND  DateofBirth='" & DateTime.Parse(DateOfBirth).ToString("yyyy/MM/dd") & "'"

        sLastName = sLastName.Replace("'", "' + char(39) + '")
        sLegalName = sLegalName.Replace("'", "' + char(39) + '")
        sCompanyName = sCompanyName.Replace("'", "' + char(39) + '")


        Select Case ClientTransformObj.CustomerType
            Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                sbrSQL.Append("SELECT Count(Type) ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL)
                sbrSQL.Append("' AND LastName LIKE '" & sLastName & "%' AND  DateofBirth='" & DateTime.Parse(DateOfBirth).ToString("yyyy/MM/dd") & "'")
            Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                sbrSQL.Append("SELECT Count(Type)  ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_COMPANY)
                sbrSQL.Append("' AND LegalName LIKE '" & sLegalName & "%' ")
            Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                sbrSQL.Append("SELECT Count(Type)  ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_TRUST)
                sbrSQL.Append("' AND Name LIKE '" & sCompanyName & "%' ")
            Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                sbrSQL.Append("SELECT Count(Type)  ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP)
                sbrSQL.Append("' AND Name LIKE '" & sCompanyName & "%' ")
        End Select


        'If DateOfBirth Is Nothing Then
        '    ErrorMessage = "Client Date of Birth should be defined"
        '    Return False
        'End If

        'If Not sFirstName = "" Then
        '    If sFirstName.Length > 3 Then
        '        sFName = sFirstName.Substring(0, 3)
        '    Else
        '        sFName = sFirstName
        '    End If
        'End If

        'If sLastName.IndexOf("'") > -1 Then NumChars = 4

        'If Not sLastName = "" Then
        '    If sLastName.Length > NumChars Then
        '        sLName = sLastName.Substring(0, NumChars)
        '    Else
        '        sLName = sLastName
        '    End If
        'End If

        'sLName = sLName.Replace("'", "' + char(39) + '")

        'strSelect = "SELECT COUNT(ClientId) From Client " & _
        '        " WHERE LastName LIKE '" & sLName & "%' AND  DateofBirth='" & DateTime.Parse(DateOfBirth).ToString("yyyy/MM/dd") & "'"
        '  " Where LastName LIKE '" & sLName & "%' AND FirstName LIKE '" & sFName & "%' AND  DateofBirth='" & DateOfBirth & "'"

        connectionstring = GetfinPowerConnectionString()

        Using mycon As New SqlConnection(connectionstring)

            mycon.ConnectionString = connectionstring
            mycon.Open()
            Dim myCommand As New SqlCommand(sbrSQL.ToString, mycon)

            iCount = myCommand.ExecuteScalar()
            'Returns true if atleast one match exists in finPower

        End Using


        Return iCount > 0

    End Function

    ''' <summary>
    ''' Get client data from FinPOWER
    ''' </summary>
    ''' <param name="ClientTransformObj"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks>ClientId, Type, Name, FirstName, MiddleNames, LastName, DateofBirth, PLAddress, PLCity, PLState, PLPostCode, Address, City, State, PostCode, ManagerId</remarks>
    Public Function GetfinPOWERClientInfo(ByVal ClientTransformObj As ClientMappingDetails) As DataSet
        Dim NumChars As Integer = 3
        Dim connectionstring As String
        Dim myAdapter As New SqlDataAdapter
        Dim myDataSet As New Data.DataSet
        Dim sbrSQL As New StringBuilder

        If ClientTransformObj Is Nothing Then Return myDataSet

        Dim sFirstName As String = If(ClientTransformObj.FirstName Is Nothing, "", ClientTransformObj.FirstName)
        Dim sLastName As String = If(ClientTransformObj.LastName Is Nothing, "", ClientTransformObj.LastName)
        Dim DateOfBirth As String = ClientTransformObj.DateOfBirth
        Dim sLegalName As String = If(ClientTransformObj.LegalName Is Nothing, "", ClientTransformObj.LegalName)
        Dim sCompanyName As String = If(ClientTransformObj.CompanyName Is Nothing, "", ClientTransformObj.CompanyName)

        If Not sFirstName = "" Then
            If sFirstName.Length > 3 Then
                sFirstName = sFirstName.Substring(0, 3)
            End If
        End If

        If Not sLastName = "" Then
            NumChars = If(sLastName.IndexOf("'") > -1, 4, 3)
            If sLastName.Length > NumChars Then
                sLastName = sLastName.Substring(0, NumChars)
            End If
        End If

        If Not sLegalName = "" Then
            NumChars = If(sLegalName.IndexOf("'") > -1, 4, 3)
            If sLegalName.Length > NumChars Then
                sLegalName = sLegalName.Substring(0, NumChars)
            End If
        End If

        If Not sCompanyName = "" Then
            NumChars = If(sCompanyName.IndexOf("'") > -1, 4, 3)
            If sCompanyName.Length > NumChars Then
                sCompanyName = sCompanyName.Substring(0, NumChars)
            End If
        End If



        'strSelect = "SELECT ClientId, Type , FirstName, MiddleNames, LastName, DateofBirth, PLAddress, PLCity, PLState, PLPostCode  From Client " & _
        '        " Where LastName LIKE '" & sLName & "%' AND FirstName LIKE '" & sFName & "%' AND  DateofBirth='" & DateTime.Parse(DateOfBirth).ToString("yyyy/MM/dd") & "'"

        sLastName = sLastName.Replace("'", "' + char(39) + '")
        sLegalName = sLegalName.Replace("'", "' + char(39) + '")
        sCompanyName = sCompanyName.Replace("'", "' + char(39) + '")

        'POSTAL ADDRESS:  Address, Suburb, City, Postcode
        'PERSON ACTION  :   PAddress, PASuburb, PACity, PAPostcode
        'PERSON LENDING (Physical) :   :  PLAddress, PLSuburb, PLCity, PLPostcode
        Select Case ClientTransformObj.CustomerType
            Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                sbrSQL.Append("SELECT ClientId, Type, LegalName, FirstName, MiddleNames, LastName, DateofBirth,  PLAddress, PLCity, PLState, PLPostCode, Address, City, State, PostCode, ManagerId ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL)
                sbrSQL.Append("' AND LastName LIKE '" & sLastName & "%' AND  DateofBirth='" & DateTime.Parse(DateOfBirth).ToString("yyyy/MM/dd") & "'")
            Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                sbrSQL.Append("SELECT ClientId, Type, LegalName, FirstName, MiddleNames, LastName, DateofBirth , PLAddress, PLCity, PLState, PLPostCode, Address, City, State, PostCode, ManagerId ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_COMPANY)
                sbrSQL.Append("' AND LegalName LIKE '" & sLegalName & "%' ")
            Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                sbrSQL.Append("SELECT ClientId, Type, Name, FirstName, MiddleNames, LastName, DateofBirth, PLAddress, PLCity, PLState, PLPostCode, Address, City, State, PostCode, ManagerId ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_TRUST)
                sbrSQL.Append("' AND Name LIKE '" & sCompanyName & "%' ")
            Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                sbrSQL.Append("SELECT ClientId, Type, Name, FirstName, MiddleNames, LastName, DateofBirth, PLAddress, PLCity, PLState, PLPostCode, Address, City, State, PostCode, ManagerId ")
                sbrSQL.Append(" FROM Client ")
                sbrSQL.Append(" WHERE Type='")
                sbrSQL.Append(Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP)
                sbrSQL.Append("' AND Name LIKE '" & sCompanyName & "%' ")
        End Select

        '  " Where LastName LIKE '" & sLName & "%' AND FirstName LIKE '" & sFName & "%' AND  DateofBirth='" & DateOfBirth & "'"

        connectionstring = GetfinPowerConnectionString()

        Using mycon As New SqlConnection(connectionstring)

            mycon.ConnectionString = connectionstring
            mycon.Open()
            Dim myCommand As New SqlCommand(sbrSQL.ToString, mycon)
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myDataSet)


        End Using

        Return myDataSet

    End Function



    Public Function GetfinPowerConnectionString() As String
        Dim connectionstring As String

        'connectionstring = "data source=YESSQL01\YESSQLSVR08;Initial Catalog=Test_finPOWER_YesFinance;Integrated Security=True;"
        connectionstring = My.Settings.finPOWERconnectionstring

        Return connectionstring

    End Function

    Public Function GetTrueTrackConnectionString() As String
        Dim connectionstring As String

        connectionstring = My.Settings.EnquiryWorkSheetConnectionString

        Return connectionstring

    End Function


    'Public Function GetTrueTrackClientDetails(ByVal enquiryId As Integer) As DataTable
    '    'TODO:XML: modify GetTrueTrackClientDetails for new customer table
    '    'Get GetTrueTrackClientDetails for the Enquiry, no longer single customer
    '    'and return multiple customers

    '    Dim myAdapter As New SqlDataAdapter
    '    Dim myDataSet As New Data.DataSet
    '    Dim sbrSelect As New StringBuilder

    '    'sbrSelect.Append("SELECT ISNULL(AR.RiskDesc,'') AMLRiskDesc, ISNULL(JR.RiskDesc,'') JnAMLRiskDesc ")
    '    'sbrSelect.Append(" FROM Client C ")
    '    'sbrSelect.Append(" INNER JOIN List_AMLRisk AR ON AR.RiskValue =C.AMLRisk ")
    '    'sbrSelect.Append(" LEFT JOIN List_AMLRisk JR ON JR.RiskValue =C.JNAMLRisk ")
    '    'sbrSelect.Append(" WHERE C.ClientId =@ClientId ")

    '    sbrSelect.Append("SELECT dbo.CustomerEnquiry.CustomerId, ISNULL(AR.RiskDesc,'') AMLRiskDesc, ISNULL(C.XRefId,'') CustomerXRefId ")
    '    sbrSelect.Append("FROM dbo.Customer C ")
    '    sbrSelect.Append("INNER JOIN dbo.CustomerEnquiry ON C.CustomerId = dbo.CustomerEnquiry.CustomerId ")
    '    sbrSelect.Append("INNER JOIN List_AMLRisk AR ON AR.RiskValue = C.AMLRisk ")
    '    sbrSelect.Append("WHERE (dbo.CustomerEnquiry.EnquiryId = @EnquiryId)")

    '    Using mycon As New SqlConnection(GetTrueTrackConnectionString())

    '        mycon.Open()
    '        Dim myCommand As New SqlCommand(sbrSelect.ToString, mycon)
    '        myCommand.Parameters.AddWithValue("@EnquiryId ", enquiryId)
    '        myAdapter.SelectCommand = myCommand
    '        myAdapter.Fill(myDataSet)

    '    End Using

    '    Return myDataSet.Tables(0)
    'End Function 'retired 29/09/2016

    ''' <summary>
    ''' Get Customers for an Enquiry: Customer Id_number, Name, Type, CustomerType, CustomerId, EnquiryId, ClientTypeId, ClientNum, XRefId
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks>DataTable of Enquiry Customers using EnquiryCustomersTableAdapter</remarks>
    Public Function GetEnquiryCustomers(ByVal enquiryId As Integer) As DataTable
        '========================
        'Added 13/09/2016
        '========================
        Dim enqCustTableAdapter As EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
        enqCustTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter()
        Dim enquiryCustomersTable As EnquiryWorkSheetDataSet.EnquiryCustomersDataTable
        enquiryCustomersTable = enqCustTableAdapter.GetDataByEnquiryId(enquiryId)

        Return enquiryCustomersTable
    End Function
    ''' <summary>
    ''' Get TrueTrack EnquiryCustomer data
    ''' </summary>
    ''' <returns>ttCustomerInfo</returns>
    ''' <remarks>Reads customer datatable row, adds to (Collection). Returns Collection of EnqCustomer</remarks>
    Public Function GetTTCustomerInfo(ByVal enquiryId As Integer) As finPowerOps.ttCustomerInfo
        Dim customersDetails As New finPowerOps.ttCustomerInfo
        Dim customerMap As EnqCustomer
        'Get Customer data from database through datatable
        Dim dt As DataTable
        Dim i As Integer = 0
        Try
            dt = GetEnquiryCustomers(enquiryId)
            'load into EnqCustomer
            For Each row As DataRow In dt.Rows
                i += 1
                customerMap = New EnqCustomer
                customerMap.IdNumber = If(row.Item("Id_number") Is DBNull.Value, 0, row.Item("Id_number"))
                customerMap.Name = If(row.Item("Name") Is DBNull.Value, "", row.Item("Name"))
                customerMap.Type = If(row.Item("Type") Is DBNull.Value, 0, row.Item("Type"))
                customerMap.CustomerType = If(row.Item("CustomerType") Is DBNull.Value, 0, row.Item("CustomerType"))
                customerMap.CustomerId = If(row.Item("CustomerId") Is DBNull.Value, "", row.Item("CustomerId"))
                customerMap.EnquiryId = If(row.Item("EnquiryId") Is DBNull.Value, 0, row.Item("EnquiryId"))
                customerMap.ClientTypeId = If(row.Item("ClientTypeId") Is DBNull.Value, "", row.Item("ClientTypeId"))
                customerMap.ClientNum = If(row.Item("ClientNum") Is DBNull.Value, "", row.Item("ClientNum"))
                customerMap.XRefId = If(row.Item("XRefId") Is DBNull.Value, "", row.Item("XRefId"))
                customerMap.DriverLicenceType = If(row.Item("DriverLicenceType") Is DBNull.Value, -1, row.Item("DriverLicenceType"))
                customersDetails.Customers.Add(i, customerMap)
            Next row
            customersDetails.Status = True

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            customersDetails.Status = False
            customersDetails.ErrorMessage = ex.Message & vbNewLine & ex.TargetSite.ToString
        End Try

        Return customersDetails

    End Function

    ''' <summary>
    ''' Select value to use in Monitor node of Loan XML
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Private Function SelectMonitorValue(ByVal enquiryId As Integer) As String
        'Get TrueTrack Customers' details
        Dim ttCustomerInfo As New finPowerOps.ttCustomerInfo
        Dim i As Integer
        Dim k As Integer = 0
        Dim custWithDL As New Dictionary(Of Integer, Integer)
        Try
            'Get TrueTrack EnquiryCustomer info
            ttCustomerInfo = GetTTCustomerInfo(enquiryId)
            If ttCustomerInfo IsNot Nothing Then
                If Not String.IsNullOrWhiteSpace(ttCustomerInfo.ErrorMessage) Then
                    MessageBox.Show(ttCustomerInfo.ErrorMessage, "Xml parsing error", MessageBoxButtons.OK)
                    Return String.Empty
                End If
            End If
            'Get Customer DriverLicenceType and add to dictionary
            For i = 1 To ttCustomerInfo.Customers.Count
                If ttCustomerInfo.Customers(i).DriverLicenceType > -1 Then
                    k += 1
                    custWithDL.Add(k, ttCustomerInfo.Customers(i).DriverLicenceType)
                End If
            Next
            'Find the most needed DriverLicenceType, can only use one in the XML Loan Monitor node
            If custWithDL.Count > 0 Then
                If custWithDL.ContainsValue(MyEnums.DriverLicenceType.Learner) Then
                    Return "DL_L"
                ElseIf custWithDL.ContainsValue(MyEnums.DriverLicenceType.Restricted) Then
                    Return "DL_R"
                ElseIf custWithDL.ContainsValue(MyEnums.DriverLicenceType.Full) Then
                    Return "DL_F"
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If

        Catch ex As Exception
            log.Error("SelectMonitorValue error:" & ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.Message, "SelectMonitorValue error", MessageBoxButtons.OK)
            Return String.Empty
        End Try

    End Function




    'XML: The DueDilgence table no longer holds the ClientId or ClientNum, these are now held in the Customer table
    'need to write new code to update ClientId, ClientNum and use this just to update LoanId
    ''' <summary>
    ''' Update LoanCode in Enquiry
    ''' </summary>
    ''' <param name="EnquiryId"></param>
    ''' <param name="LoanId"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Inserts or Updates DueDiligence table with LoanNum</remarks>
    Public Function UpdateLoanDetails(ByVal enquiryId As Integer, ByVal loanId As String) As Boolean
        'connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings("AppWhShtB.My.MySettings.finPowerConnectionString").ToString()
        Using mycon As New SqlConnection(GetTrueTrackConnectionString())
            mycon.Open()

            Dim myCommand As New SqlCommand("InsertNewDueDiligence", mycon)

            myCommand.Parameters.AddWithValue("@EnquiryId ", enquiryId)
            myCommand.Parameters.AddWithValue("@LoanNum ", loanId)

            myCommand.CommandType = CommandType.StoredProcedure
            myCommand.ExecuteNonQuery()


        End Using

        Return True


    End Function

    ''' <summary>
    ''' Update ClientId in Customer table
    ''' </summary>
    ''' <param name="customerId"></param>
    ''' <param name="clientId"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Updates Customer table with clientId</remarks>
    Public Function UpdateCustClientId(ByVal customerId As Integer, ByVal clientId As String, ByVal clientTypeId As String) As Boolean
        Dim param As SqlParameter
        Dim clientNum As String = ""
        Dim thisClientTypeId As String = ""
        Dim tempClientType As String = ""

        Using mycon As New SqlConnection(GetTrueTrackConnectionString())
            mycon.Open()

            Dim myCommand As New SqlCommand("InsertClientId", mycon)
            If clientId Is Nothing Or clientId = "" Then
                clientNum = "" : thisClientTypeId = ""
            Else
                If clientId.Length > 1 Then
                    'tempClientType = clientId.Substring(0, 1)
                    'Check for ClientType as valid Character
                    'If (Strings.Asc(tempClientType) > 64 And Strings.Asc(tempClientType) < 91) Or (Strings.Asc(tempClientType) > 96 And Strings.Asc(tempClientType) < 123) Then
                    '    clientNum = clientId.Substring(1)
                    '    clientTypeId = tempClientType
                    'Else
                    '    clientNum = clientId
                    '    clientTypeId = ""
                    'End If
                    clientNum = clientId
                    thisClientTypeId = clientTypeId
                End If
            End If

            myCommand.Parameters.AddWithValue("@CustomerId ", customerId)
            param = New SqlParameter("@ClientNum", SqlDbType.VarChar)
            param.Value = clientNum
            myCommand.Parameters.Add(param)
            param = New SqlParameter("@ClientTypeId", SqlDbType.VarChar)
            param.Value = clientTypeId
            myCommand.Parameters.Add(param)

            myCommand.CommandType = CommandType.StoredProcedure
            myCommand.ExecuteNonQuery()


        End Using

        Return True

    End Function

    ''' <summary>
    ''' Update XRefId in Customer table
    ''' </summary>
    ''' <param name="customerId"></param>
    ''' <param name="xRefId"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Updates Customer table with XRefId</remarks>
    Public Function UpdateCustXRefId(ByVal customerId As Integer, ByVal xRefId As String) As Boolean
        Dim param As SqlParameter

        Using mycon As New SqlConnection(GetTrueTrackConnectionString())
            mycon.Open()

            Dim myCommand As New SqlCommand("InsertXRefId", mycon)
            If xRefId Is Nothing Then
                xRefId = ""
            End If

            myCommand.Parameters.AddWithValue("@CustomerId ", customerId)
            param = New SqlParameter("@XRefId", SqlDbType.VarChar)
            param.Value = xRefId
            myCommand.Parameters.Add(param)

            myCommand.CommandType = CommandType.StoredProcedure
            myCommand.ExecuteNonQuery()


        End Using

        Return True

    End Function

    ''' <summary>
    ''' Get Contact methods for Client in finPower
    ''' </summary>
    ''' <param name="ClientId"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetContactMethods(ByVal ClientId As String) As DataTable
        Dim connectionstring As String
        Dim myAdapter As New SqlDataAdapter
        Dim myDataSet As New Data.DataSet
        Dim strSelect As String


        strSelect = "SELECT ClientContactMethodId , ContactMethod, Value From ClientContactMethod " & _
                " Where ClientId=@clientId " '  AND ContactMethod IN ('Work','Mobile','Phone','Email')"


        'connectionstring = "data source=YESSQL01\YESSQLSVR08;Initial Catalog=Test_finPOWER_YesFinance;Integrated Security=True;"
        connectionstring = GetfinPowerConnectionString()

        'connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings("AppWhShtB.My.MySettings.finPowerConnectionString").ToString()
        Using mycon As New SqlConnection(connectionstring)

            'ClientId = "C10013"

            mycon.ConnectionString = connectionstring
            mycon.Open()
            Dim myCommand As New SqlCommand(strSelect, mycon)
            myCommand.Parameters.AddWithValue("@clientId ", ClientId)
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myDataSet)


        End Using

        Return myDataSet.Tables(0)

    End Function





    Public Function MoveXMLFileToImportFolder() As Boolean
        'Dim OutputXMLPath As String = getImportFilePath() & GetXMLFileName()
        'Resources finPowerImportFolder
        Dim outputXmlPath As String = GetfinPOWERImportFilePath() & ApplicationInterchange.GetXMLFileName(ApplicationCode)
        Dim inputXmlPath As String
        If System.IO.File.Exists(outputXmlPath) Then
            ErrorMessage = "Cannot move file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & " File Exists in finPOWER importfolder."
            Return False
        End If
        'Get the absolute path of the XML file
        inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & ApplicationInterchange.GetXMLFileName(ApplicationCode)
        '=====================================================
        'TODO:For XML testing
        'inputXmlPath = "C:\Enquiry\testClient.xml"
        '=====================================================
        '==============================================
        'Check XML file exists in Enquiry Folder
        '==============================================
        If System.IO.File.Exists(inputXmlPath) Then

            '==============================================
            'WRITE XML file to finPOWER Import folder
            '==============================================
            Try
                System.IO.File.Move(inputXmlPath, outputXmlPath)

            Catch ex As Exception
                ErrorMessage = "Error while moving file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & " " & ex.Message
                Return False
            End Try

            Try
                System.IO.File.Delete(inputXmlPath)
            Catch ex As Exception
                log.Error("Error while deleting file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & ": " & ex.Message & " : " & ex.TargetSite.ToString)
                ErrorMessage = "Error while deleting file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & " " & ex.Message
                Return False
            End Try

            Return True
        End If
        Return False

    End Function



    ''' <summary>
    ''' Updates the OAC XML file and saves into the finPOWER Import Folder
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function UpdateXMLFileMappingData(finPowerClients As Dictionary(Of Integer, ClientMappingDetails), enquiryId As Integer) As Boolean
        Dim outputXmlPath As String
        Dim inputXmlPath As String
        Dim xdoc As New XmlDocument
        Dim clientfinUpd As New ClientMappingDetails
        Dim nodeListChild As XmlNodeList
        Dim xNodeJointName As XmlNode
        Dim xNodeAddresses As XmlNode
        Dim xNodeChild As XmlNode
        Dim xNodeContactMethods As XmlNode
        Dim xRootNodeList As XmlNodeList
        Dim newChildElem As XmlElement
        Dim i As Integer = 0
        Dim xNode As XmlNode
        Dim xNodeRoot As XmlNode
        Dim xmlAttr As XmlAttribute
        Dim contactMethod As ContactMethods = Nothing
        Dim nodeListContactMethodChild As XmlNode
        Dim nodeListContactMethod As XmlNodeList
        Dim ie As IEnumerator = Nothing
        Dim contactMethodlist As List(Of ContactMethods)  'Used to keep MembersCentre and after that finPower contact method
        Dim status As Boolean = False
        Dim ttDt As DataTable

        'Get the absolute path of the XML file
        inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & ApplicationInterchange.GetXMLFileName(ApplicationCode)
        '=====================================================
        'TODO:For XML testing
        'inputXmlPath = "C:\Enquiry\testClient.xml"
        '=====================================================
        Try
            'Read XML
            Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)
            xdoc.LoadXml(xmlFile)

            ttDt = GetEnquiryCustomers(enquiryId)

            xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
            'Need to branch here if EnquiryType is FinanceFacility as this EnquiryType has no loan
            If Not xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan") Is Nothing Then
                xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")
                xmlAttr = xNode.Attributes("LoanId")
                If Not xmlAttr Is Nothing Then
                    LoanCode = xmlAttr.Value
                End If
                '==============================================
                'Read JointName element One by One
                '==============================================
                xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
                For Each xNodeJointName In xRootNodeList
                    clientfinUpd = New ClientMappingDetails
                    i += 1 'iterate through finPowerClients(selected XML customers) 'finPowerClients As Dictionary(Of Integer, ClientMappingDetails)
                    If finPowerClients.ContainsKey(i) Then
                        clientfinUpd = finPowerClients.Item(i)
                        'Add ClientId if XREfIds match
                        If Not ttDt Is Nothing Then
                            If ttDt.Rows.Count > 0 Then
                                For Each row As DataRow In ttDt.Rows
                                    If row.Item("XREfId") = clientfinUpd.XREfId Then
                                        Try
                                            UpdateCustClientId(CInt(row.Item("CustomerId")), clientfinUpd.ClientId, clientfinUpd.ClientTypeId)
                                        Catch ex As Exception
                                            log.Error("Error in UpdateCustClientId:" & ex.Message & " : " & ex.TargetSite.ToString)
                                            MsgBox("Error in UpdateCustClientId")
                                        End Try
                                    End If
                                Next row
                            End If
                        End If
                    End If


                    If Not clientfinUpd.ClientId Is Nothing Then
                        nodeListChild = xNodeJointName.SelectNodes("Client")

                        For Each xNodeChild In nodeListChild
                            If clientfinUpd.NameChecked Then
                                'If NameChecked then using existing finPower Client and so some nodes are to be removed from XML, and some nodes to be updated
                                'Check attribute ClientId in Node Client
                                xmlAttr = xNodeChild.Attributes("ClientId")
                                If Not xmlAttr Is Nothing Then
                                    'Add ClientId to XML
                                    xmlAttr.Value = clientfinUpd.ClientId
                                Else
                                    'If attribute ClientId is missing then add an attribute
                                    xmlAttr = xdoc.CreateAttribute("ClientId")
                                    xmlAttr.Value = clientfinUpd.ClientId
                                    xNodeChild.Attributes.Append(xmlAttr)
                                End If

                                xNode = xNodeChild.SelectSingleNode("LastName")
                                If xNode IsNot Nothing Then
                                    xNode.InnerText = clientfinUpd.LastName
                                End If
                                xNode = xNodeChild.SelectSingleNode("FirstName")
                                If xNode IsNot Nothing Then
                                    xNode.InnerText = clientfinUpd.FirstName
                                End If
                                xNode = xNodeChild.SelectSingleNode("MiddleNames")
                                If xNode IsNot Nothing Then
                                    xNode.InnerText = clientfinUpd.MiddleNames
                                End If
                                'xNode = xNodeChild.SelectSingleNode("Type")
                                'If xNode IsNot Nothing Then
                                '    'xNode.RemoveAll()
                                '    xNode.ParentNode.RemoveChild(xNode)
                                'End If
                                xNode = xNodeChild.SelectSingleNode("Title")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                xNode = xNodeChild.SelectSingleNode("Salutation")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                xNode = xNodeChild.SelectSingleNode("Gender")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                xNode = xNodeChild.SelectSingleNode("DateOfBirth")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                xNode = xNodeChild.SelectSingleNode("MaritalStatus")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                xNode = xNodeChild.SelectSingleNode("Occupation")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                xNode = xNodeChild.SelectSingleNode("BranchId")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                                If clientfinUpd.ManagerIdChecked Then
                                    xNode = xNodeChild.SelectSingleNode("ManagerId")
                                    If xNode IsNot Nothing Then
                                        xNode.ParentNode.RemoveChild(xNode)
                                    End If
                                End If

                                'AMLRisk is not longer being transferred to finPower
                                'AMLRisk reports will be run in TrueTrack

                            End If
                            xNode = xNodeChild.SelectSingleNode("UserField0")
                            If xNode IsNot Nothing Then
                                If xNode.InnerText.ToUpper = "" Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                            xNode = xNodeChild.SelectSingleNode("UserField1")
                            If xNode IsNot Nothing Then
                                If xNode.InnerText.ToUpper = "" Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                            xNode = xNodeChild.SelectSingleNode("UserField2")
                            If xNode IsNot Nothing Then
                                If xNode.InnerText.ToUpper = "" Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                            xNode = xNodeChild.SelectSingleNode("UserField3")
                            If xNode IsNot Nothing Then
                                If xNode.InnerText.ToUpper = "" Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                            xNode = xNodeChild.SelectSingleNode("UserField4")
                            If xNode IsNot Nothing Then
                                If xNode.InnerText.ToUpper = "" Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                            xNode = xNodeChild.SelectSingleNode("UserField5")
                            If xNode IsNot Nothing Then
                                If xNode.InnerText.ToUpper = "" Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                            'Address and Contact Details
                            '============================================================================
                            '  ADDRESS
                            '============================================================================
                            xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                            'Check physical address is selected
                            If clientfinUpd.AddressPhysicalChecked Then
                                If xNodeAddresses IsNot Nothing Then
                                    'Remove physical address Node of OAC if finPOWER physical address is selected
                                    xNode = xNodeAddresses.SelectSingleNode("Address[@Type='Physical']")
                                    If xNode IsNot Nothing Then
                                        xNode.RemoveAll()
                                        'ask the node's parent to remove the node
                                        xNode.ParentNode.RemoveChild(xNode)
                                    End If
                                End If
                            End If
                            'Check postal address is selected
                            If clientfinUpd.AddressPostalChecked Then
                                If xNodeAddresses IsNot Nothing Then
                                    'Remove postal address Node of OAC if finPOWER postal address is selected
                                    xNode = xNodeAddresses.SelectSingleNode("Address[@Type='Postal']")
                                    If xNode IsNot Nothing Then
                                        xNode.RemoveAll()
                                        'ask the node's parent to remove the node
                                        xNode.ParentNode.RemoveChild(xNode)
                                    End If
                                End If
                            End If



                            '============================================================================
                            '  CONTACT METHODS
                            '============================================================================
                            'NOTES: adds the contact method from finPower if missing in the XML to avoid deleting from finPOWER
                            'If user selects to retain finPOWER contact Method value will copy value from finPOWER to XML Node

                            xNodeContactMethods = xNodeChild.SelectSingleNode("ContactMethods")
                            nodeListContactMethod = xNodeContactMethods.SelectNodes("ContactMethod")

                            For Each nodeListContactMethodChild In nodeListContactMethod
                                'Deletes all ContactMethods and recreates list of contact Methods
                                If nodeListContactMethodChild IsNot Nothing Then
                                    nodeListContactMethodChild.ParentNode.RemoveChild(nodeListContactMethodChild)
                                End If
                            Next

                            '=============================================================================
                            'BUILD Contact Method elements in XML
                            'First create Selected Contact Methods of MembersCentre 
                            'Second create selected contact methods from finPOWER
                            '=============================================================================
                            contactMethodlist = clientfinUpd.ContactMethodsOACXml
                            If contactMethodlist IsNot Nothing Then

                                ie = contactMethodlist.GetEnumerator
                                While ie.MoveNext
                                    contactMethod = TryCast(ie.Current, ContactMethods)
                                    If contactMethod IsNot Nothing Then
                                        newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
                                        newChildElem.InnerText = contactMethod.MethodValue
                                        xmlAttr = xdoc.CreateAttribute("Method")
                                        xmlAttr.InnerText = contactMethod.MethodName
                                        newChildElem.Attributes.Append(xmlAttr)
                                        xNodeContactMethods.AppendChild(newChildElem)
                                        '========================================================================
                                        'Mobile (SMS)
                                        '========================================================================
                                        'Adds an addtional element if the Contact Method is Mobile
                                        'Only for ContactMethod= Mobile from MembersCentre 
                                        'there is always Mobile (SMS) in finPOWER for all Mobile elements
                                        If contactMethod.MethodName = "Mobile" Then
                                            'Mobile (SMS)
                                            newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
                                            newChildElem.InnerText = contactMethod.MethodValue
                                            xmlAttr = xdoc.CreateAttribute("Method")
                                            xmlAttr.InnerText = "Mobile (SMS)"
                                            newChildElem.Attributes.Append(xmlAttr)
                                            xNodeContactMethods.AppendChild(newChildElem)
                                        End If
                                    End If
                                End While


                            End If

                            contactMethodlist = clientfinUpd.ContactMethodsFinPower
                            If contactMethodlist IsNot Nothing Then

                                ie = contactMethodlist.GetEnumerator
                                While ie.MoveNext
                                    contactMethod = TryCast(ie.Current, ContactMethods)
                                    If contactMethod IsNot Nothing Then
                                        newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
                                        newChildElem.InnerText = contactMethod.MethodValue
                                        xmlAttr = xdoc.CreateAttribute("Method")
                                        xmlAttr.InnerText = contactMethod.MethodName
                                        newChildElem.Attributes.Append(xmlAttr)
                                        xNodeContactMethods.AppendChild(newChildElem)
                                    End If
                                End While

                            End If

                        Next

                    End If
                Next

            ElseIf Not xNodeRoot.SelectSingleNode("/finPOWER/Clients/Client") Is Nothing Then

                clientfinUpd = New ClientMappingDetails
                i += 1 'iterate through finPowerClients(selected XML customers) 'finPowerClients As Dictionary(Of Integer, ClientMappingDetails)
                If finPowerClients.ContainsKey(i) Then
                    clientfinUpd = CType(finPowerClients.Item(i), ClientMappingDetails)
                    'Add ClientId if XREfIds match
                    If Not ttDt Is Nothing Then
                        If ttDt.Rows.Count > 0 Then
                            For Each row As DataRow In ttDt.Rows
                                If row.Item("XREfId") = clientfinUpd.XREfId Then
                                    Try
                                        UpdateCustClientId(CInt(row.Item("CustomerId")), clientfinUpd.ClientId, clientfinUpd.ClientTypeId)
                                    Catch ex As Exception
                                        log.Error("Error in UpdateCustClientId:" & ex.Message & " : " & ex.TargetSite.ToString)
                                        MsgBox("Error in UpdateCustClientId")
                                    End Try
                                End If
                            Next row
                        End If
                    End If
                End If


                If Not clientfinUpd.ClientId Is Nothing Then
                    xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Clients/Client")
                    '=====================================================
                    'Reads client elements one by one
                    '=====================================================
                    For Each xNodeChild In xRootNodeList
                        If clientfinUpd.NameChecked Then
                            'If NameChecked then using existing finPower Client and so some nodes are to be removed from XML, and some nodes to be updated
                            'Check attribute ClientId in Node Client
                            xmlAttr = xNodeChild.Attributes("ClientId")
                            If Not xmlAttr Is Nothing Then
                                'Add ClientId to XML
                                xmlAttr.Value = clientfinUpd.ClientId
                            Else
                                'If attribute ClientId is missing then add an attribute
                                xmlAttr = xdoc.CreateAttribute("ClientId")
                                xmlAttr.Value = clientfinUpd.ClientId
                                xNodeChild.Attributes.Append(xmlAttr)
                            End If

                            xNode = xNodeChild.SelectSingleNode("LastName")
                            If xNode IsNot Nothing Then
                                xNode.InnerText = clientfinUpd.LastName
                            End If
                            xNode = xNodeChild.SelectSingleNode("FirstName")
                            If xNode IsNot Nothing Then
                                xNode.InnerText = clientfinUpd.FirstName
                            End If
                            xNode = xNodeChild.SelectSingleNode("MiddleNames")
                            If xNode IsNot Nothing Then
                                xNode.InnerText = clientfinUpd.MiddleNames
                            End If
                            'xNode = xNodeChild.SelectSingleNode("Type")
                            'If xNode IsNot Nothing Then
                            '    'xNode.RemoveAll()
                            '    xNode.ParentNode.RemoveChild(xNode)
                            'End If
                            xNode = xNodeChild.SelectSingleNode("Title")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            xNode = xNodeChild.SelectSingleNode("Salutation")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            xNode = xNodeChild.SelectSingleNode("Gender")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            xNode = xNodeChild.SelectSingleNode("DateOfBirth")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            xNode = xNodeChild.SelectSingleNode("MaritalStatus")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            xNode = xNodeChild.SelectSingleNode("Occupation")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            xNode = xNodeChild.SelectSingleNode("BranchId")
                            If xNode IsNot Nothing Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                            If clientfinUpd.ManagerIdChecked Then
                                xNode = xNodeChild.SelectSingleNode("ManagerId")
                                If xNode IsNot Nothing Then
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If

                            'AMLRisk is not longer being transferred to finPower
                            'AMLRisk reports will be run in TrueTrack

                        End If
                        xNode = xNodeChild.SelectSingleNode("UserField0")
                        If xNode IsNot Nothing Then
                            If xNode.InnerText.ToUpper = "" Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                        End If
                        xNode = xNodeChild.SelectSingleNode("UserField1")
                        If xNode IsNot Nothing Then
                            If xNode.InnerText.ToUpper = "" Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                        End If
                        xNode = xNodeChild.SelectSingleNode("UserField2")
                        If xNode IsNot Nothing Then
                            If xNode.InnerText.ToUpper = "" Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                        End If
                        xNode = xNodeChild.SelectSingleNode("UserField3")
                        If xNode IsNot Nothing Then
                            If xNode.InnerText.ToUpper = "" Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                        End If
                        xNode = xNodeChild.SelectSingleNode("UserField4")
                        If xNode IsNot Nothing Then
                            If xNode.InnerText.ToUpper = "" Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                        End If
                        xNode = xNodeChild.SelectSingleNode("UserField5")
                        If xNode IsNot Nothing Then
                            If xNode.InnerText.ToUpper = "" Then
                                xNode.ParentNode.RemoveChild(xNode)
                            End If
                        End If
                        'Address and Contact Details
                        '============================================================================
                        '  ADDRESS
                        '============================================================================
                        xNodeAddresses = xNodeChild.SelectSingleNode("Addresses")
                        'Check physical address is selected
                        If clientfinUpd.AddressPhysicalChecked Then
                            If xNodeAddresses IsNot Nothing Then
                                'Remove physical address Node of OAC if finPOWER physical address is selected
                                xNode = xNodeAddresses.SelectSingleNode("Address[@Type='Physical']")
                                If xNode IsNot Nothing Then
                                    xNode.RemoveAll()
                                    'ask the node's parent to remove the node
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                        End If
                        'Check postal address is selected
                        If clientfinUpd.AddressPostalChecked Then
                            If xNodeAddresses IsNot Nothing Then
                                'Remove postal address Node of OAC if finPOWER postal address is selected
                                xNode = xNodeAddresses.SelectSingleNode("Address[@Type='Postal']")
                                If xNode IsNot Nothing Then
                                    xNode.RemoveAll()
                                    'ask the node's parent to remove the node
                                    xNode.ParentNode.RemoveChild(xNode)
                                End If
                            End If
                        End If



                        '============================================================================
                        '  CONTACT METHODS
                        '============================================================================
                        'NOTES: adds the contact method from finPower if missing in the XML to avoid deleting from finPOWER
                        'If user selects to retain finPOWER contact Method value will copy value from finPOWER to XML Node

                        xNodeContactMethods = xNodeChild.SelectSingleNode("ContactMethods")
                        nodeListContactMethod = xNodeContactMethods.SelectNodes("ContactMethod")

                        For Each nodeListContactMethodChild In nodeListContactMethod
                            'Deletes all ContactMethods and recreates list of contact Methods
                            If nodeListContactMethodChild IsNot Nothing Then
                                nodeListContactMethodChild.ParentNode.RemoveChild(nodeListContactMethodChild)
                            End If
                        Next

                        '=============================================================================
                        'BUILD Contact Method elements in XML
                        'First create Selected Contact Methods of MembersCentre 
                        'Second create selected contact methods from finPOWER
                        '=============================================================================
                        contactMethodlist = clientfinUpd.ContactMethodsOACXml
                        If contactMethodlist IsNot Nothing Then

                            ie = contactMethodlist.GetEnumerator
                            While ie.MoveNext
                                contactMethod = TryCast(ie.Current, ContactMethods)
                                If contactMethod IsNot Nothing Then
                                    newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
                                    newChildElem.InnerText = contactMethod.MethodValue
                                    xmlAttr = xdoc.CreateAttribute("Method")
                                    xmlAttr.InnerText = contactMethod.MethodName
                                    newChildElem.Attributes.Append(xmlAttr)
                                    xNodeContactMethods.AppendChild(newChildElem)
                                    '========================================================================
                                    'Mobile (SMS)
                                    '========================================================================
                                    'Adds an addtional element if the Contact Method is Mobile
                                    'Only for ContactMethod= Mobile from MembersCentre 
                                    'there is always Mobile (SMS) in finPOWER for all Mobile elements
                                    If contactMethod.MethodName = "Mobile" Then
                                        'Mobile (SMS)
                                        newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
                                        newChildElem.InnerText = contactMethod.MethodValue
                                        xmlAttr = xdoc.CreateAttribute("Method")
                                        xmlAttr.InnerText = "Mobile (SMS)"
                                        newChildElem.Attributes.Append(xmlAttr)
                                        xNodeContactMethods.AppendChild(newChildElem)
                                    End If
                                End If
                            End While


                        End If

                        contactMethodlist = clientfinUpd.ContactMethodsFinPower
                        If contactMethodlist IsNot Nothing Then

                            ie = contactMethodlist.GetEnumerator
                            While ie.MoveNext
                                contactMethod = TryCast(ie.Current, ContactMethods)
                                If contactMethod IsNot Nothing Then
                                    newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
                                    newChildElem.InnerText = contactMethod.MethodValue
                                    xmlAttr = xdoc.CreateAttribute("Method")
                                    xmlAttr.InnerText = contactMethod.MethodName
                                    newChildElem.Attributes.Append(xmlAttr)
                                    xNodeContactMethods.AppendChild(newChildElem)
                                End If
                            End While

                        End If

                    Next
                End If

            End If

            'Resources finPowerImportFolder
            outputXmlPath = GetfinPOWERImportFilePath() & ApplicationInterchange.GetXMLFileName(ApplicationCode)

            'WRITE XML file to finPOWER Import folder
            System.IO.File.WriteAllText(outputXmlPath, xdoc.InnerXml)

            status = True

        Catch ex As Exception
            log.Error("Error while importing file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & ": " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error while importing file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)
        End Try
        'Delete XML file from Enquiry Folder after successfull transfer of file to finPOWE Import folder
        Try
            System.IO.File.Delete(inputXmlPath)
        Catch ex As Exception
            log.Error("Error while deleting file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & ": " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error while deleting file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)

        End Try

        Return status


    End Function


    ''' <summary>
    ''' Updates the OAC XML file with Monitor Value
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks>Depends on ApplicationCode in finPowersOps</remarks>
    Public Function UpdateXmlMonitor() As Boolean
        Dim inputXmlPath As String
        Dim xdoc As New XmlDocument
        Dim xNode As XmlNode
        Dim xNodeRoot As XmlNode
        Dim monitorString As String

        'Get the absolute path of the XML file
        inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & ApplicationInterchange.GetXMLFileName(ApplicationCode)
        '=====================================================
        'TODO:For XML testing
        'inputXmlPath = "C:\Enquiry\testClient.xml"
        '=====================================================
        Try
            'Read XML
            Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)
            xdoc.LoadXml(xmlFile)
            xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
            'if EnquiryType is FinanceFacility there is no loan
            If Not xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan") Is Nothing Then
                xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")
                'Add monitor for Customer Driver's Licence
                If xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/Monitor") Is Nothing Then
                    monitorString = SelectMonitorValue(EnquiryId)
                    If monitorString.Length > 0 Then
                        'Create a new node.
                        Dim elem As XmlElement = xdoc.CreateElement("Monitor")
                        elem.InnerText = monitorString
                        'xNode from above is still valid
                        xNode.AppendChild(elem)
                    End If
                Else
                    xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/Monitor")
                    xNode.InnerText = SelectMonitorValue(EnquiryId)
                End If
                'WRITE XML file to finPOWER Import folder
                System.IO.File.WriteAllText(inputXmlPath, xdoc.InnerXml)
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            log.Error("Error while updating file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & ": " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error while updating file " & ApplicationInterchange.GetXMLFileName(ApplicationCode) & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)
            Return False
        End Try

    End Function


    ' ''' <summary>
    ' ''' Moves XML file if no client match is found
    ' ''' </summary>
    ' ''' <param name="enquiryId"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Public Function UpdateAndMoveXMLFileNoMatch(ByVal enquiryId As Integer) As Boolean
    '    'TODO:XML: UpdateAndMoveXMLFileNoMatch, is this used??
    '    Dim outputXmlPath As String
    '    Dim inputXmlPath As String
    '    'Dim updatedXml As String = ""
    '    Dim doc As XmlDocument = New XmlDocument()
    '    Dim xdoc As New XmlDocument
    '    Dim ClientfinUpd As New ClientMappingDetails
    '    Dim nodeListChild As XmlNodeList
    '    Dim xNodeJointName As XmlNode
    '    Dim xNodeLog As XmlNode = Nothing
    '    Dim xNodeChild As XmlNode
    '    Dim xRootNodeList As XmlNodeList
    '    Dim xNode As XmlNode
    '    Dim xNodeRoot As XmlNode
    '    Dim xmlAttr As XmlAttribute
    '    'Dim dt As DataTable
    '    Dim RiskDesc As String = ""
    '    Dim JnRiskDesc As String = ""
    '    Dim clientType As String = ""
    '    Dim xNodeContactMethods As XmlNode
    '    Dim xNode2 As XmlNode
    '    Dim newChildElem As XmlElement
    '    'Dim CustomerTypeMain As String = String.Empty
    '    Dim CustomerType As String = String.Empty

    '    'Get the absolute path of the XML file
    '    inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & GetXMLFileName(ApplicationCode)
    '    'Read XML
    '    Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)

    '    xdoc.LoadXml(xmlFile)
    '    'Get client Risk Assessment
    '    'modified 09/09/2016 to get AMLRisk and XRefId
    '    'dt = GetTrueTrackClientDetails(enquiryId)
    '    'If Not dt Is Nothing Then
    '    '    If dt.Rows.Count > 0 Then
    '    '        RiskDesc = dt.Rows(0).Item("AMLRiskDesc").ToString 'MAIN
    '    '        JnRiskDesc = dt.Rows(0).Item("JnAMLRiskDesc").ToString ' Joint Name

    '    '        RiskDesc = If(RiskDesc = "Select one", "", RiskDesc)
    '    '        JnRiskDesc = If(JnRiskDesc = "Select one", "", JnRiskDesc)
    '    '    End If
    '    'End If

    '    'TtCustomers = New ttCustomerInfo '29/09/2016
    '    'TtCustomers = GetTTCustomerInfo(enquiryId)


    '    xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
    '    xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")
    '    xmlAttr = xNode.Attributes("LoanId")
    '    If Not xmlAttr Is Nothing Then
    '        LoanCode = xmlAttr.Value
    '    End If

    '    '==============================================
    '    'Read Clients One by One
    '    '==============================================
    '    xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
    '    For Each xNodeJointName In xRootNodeList
    '        'TODO:XML: match up ClientfinUpd.XRefId with dt(GetTrueTrackClientDetails datatable) Customer.XRefId
    '        'add get the Customer.AMLRisk > AMLRiskDesc
    '        'then assign the AMLRiskDesc

    '        xNode = xNodeJointName.SelectSingleNode("Type")
    '        If xNode IsNot Nothing Then
    '            If xNode.InnerText.ToUpper = "MAIN" Then
    '                clientType = "MAIN"

    '            End If
    '            If xNode.InnerText.ToUpper = "JOINT NAME" Then
    '                clientType = "JOINT NAME"
    '            End If
    '        End If
    '        nodeListChild = xNodeJointName.SelectNodes("Client")

    '        For Each xNodeChild In nodeListChild
    '            'If clientType = "MAIN" Then
    '            xNode = xNodeJointName.SelectSingleNode("Type")
    '            If xNode IsNot Nothing Then
    '                CustomerType = xNode.InnerText.ToUpper
    '            End If
    '            'End If
    '            'TODO:XML: modify for multiple customers
    '            'If CustomerTypeMain = "INDIVIDUAL" Then
    '            '    If clientType = "JOINT NAME" Then
    '            '        xNode = xNodeChild.SelectSingleNode("UserField5")
    '            '        If xNode IsNot Nothing Then
    '            '            xNode.InnerText = JnRiskDesc
    '            '        End If
    '            '    End If
    '            'End If
    '            '==============================================
    '            'Update UserFields
    '            '==============================================
    '            'TODO:XML: modify for multiple customers
    '            'If clientType = "MAIN" Then
    '            '    xNode = xNodeChild.SelectSingleNode("UserField5")
    '            '    If xNode IsNot Nothing Then
    '            '        xNode.InnerText = RiskDesc
    '            '    End If
    '            'End If
    '            If CustomerType = Constants.CONST_CUSTOMERTYPE_INDIVIDUAL Then
    '                xNode = xNodeChild.SelectSingleNode("UserField5")
    '                If xNode IsNot Nothing Then
    '                    xNode.InnerText = RiskDesc
    '                End If
    '            End If

    '            xNode = xNodeChild.SelectSingleNode("UserField0")
    '            If xNode IsNot Nothing Then
    '                If xNode.InnerText.ToUpper = "" Then
    '                    xNode.ParentNode.RemoveChild(xNode)
    '                End If
    '            End If
    '            xNode = xNodeChild.SelectSingleNode("UserField1")
    '            If xNode IsNot Nothing Then
    '                If xNode.InnerText.ToUpper = "" Then
    '                    xNode.ParentNode.RemoveChild(xNode)
    '                End If
    '            End If
    '            xNode = xNodeChild.SelectSingleNode("UserField2")
    '            If xNode IsNot Nothing Then
    '                If xNode.InnerText.ToUpper = "" Then
    '                    xNode.ParentNode.RemoveChild(xNode)
    '                End If
    '            End If
    '            xNode = xNodeChild.SelectSingleNode("UserField3")
    '            If xNode IsNot Nothing Then
    '                If xNode.InnerText.ToUpper = "" Then
    '                    xNode.ParentNode.RemoveChild(xNode)
    '                End If
    '            End If
    '            xNode = xNodeChild.SelectSingleNode("UserField4")
    '            If xNode IsNot Nothing Then
    '                If xNode.InnerText.ToUpper = "" Then
    '                    xNode.ParentNode.RemoveChild(xNode)
    '                End If
    '            End If
    '            xNode = xNodeChild.SelectSingleNode("UserField5")
    '            If xNode IsNot Nothing Then
    '                If xNode.InnerText.ToUpper = "" Then
    '                    xNode.ParentNode.RemoveChild(xNode)
    '                End If
    '            End If

    '            'added "Mobile (SMS)" 07/01/2014. Adds SMS if mobile number exists
    '            xNodeContactMethods = xNodeChild.SelectSingleNode("ContactMethods")
    '            xNode = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Mobile']")
    '            xNode2 = xNodeContactMethods.SelectSingleNode("ContactMethod[@Method='Mobile (SMS)']")
    '            If xNode2 IsNot Nothing Then
    '                If xNode IsNot Nothing Then
    '                    If Not xNode.InnerText.ToUpper = "" Then
    '                        xNode2.InnerText = xNode.InnerText.ToString
    '                    End If
    '                End If
    '            Else
    '                If xNode IsNot Nothing Then
    '                    If Not xNode.InnerText.ToUpper = "" Then
    '                        newChildElem = xdoc.CreateNode("element", "ContactMethod", "")
    '                        newChildElem.InnerText = xNode.InnerText.ToString
    '                        xmlAttr = xdoc.CreateAttribute("Method")
    '                        xmlAttr.InnerText = "Mobile (SMS)"
    '                        newChildElem.Attributes.Append(xmlAttr)
    '                        xNodeContactMethods.AppendChild(newChildElem)
    '                    End If
    '                End If

    '            End If

    '        Next

    '    Next
    '    '==============================================
    '    'WRITE XML file to finPOWER Import folder
    '    '==============================================
    '    outputXmlPath = GetfinPOWERImportFilePath() & GetXMLFileName(ApplicationCode)

    '    'Resources finPowerImportFolder
    '    System.IO.File.WriteAllText(outputXmlPath, xdoc.InnerXml)

    '    'Delete XML file from Enquiry Folder after successfull transfer of file to finPOWER Import folder
    '    Try
    '        System.IO.File.Delete(inputXmlPath)
    '    Catch ex As Exception
    '        MsgBox("Error while deleting file " & GetXMLFileName(EnquiryCode) & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)
    '    End Try

    '    Return True

    'End Function

    Public Function GetCustomerRiskIndicator(ByVal RiskValue As String) As String

        Dim RiskConvertor As String = ""

        Select Case RiskValue
            Case "1" : RiskConvertor = "Low"
            Case "2" : RiskConvertor = "Medium"
            Case "3" : RiskConvertor = "High"

        End Select

        Return RiskConvertor

    End Function

    ''' <summary>
    ''' load existing XML Loan and Client LatencyPoints (logs)
    ''' </summary>
    ''' <returns>Hashtable</returns>
    ''' <remarks>load Loan LatencyPoints into htXmlLogs at key '0'. Load Client alLatencyPoints into htXmlLogs at key of ikey. This grabs all the Logs wether or not they are really LatencyPoints</remarks>
    Public Function ReadClientLogs() As Hashtable
        Dim alLatencyPoints As ArrayList
        Dim htXmlLogs As New Hashtable
        Dim inputXmlPath As String
        Dim xdoc As New XmlDocument
        Dim xNodeJointName As XmlNode
        Dim xNodeLog As XmlNode
        Dim xJointNameNodeList As XmlNodeList
        Dim xRootNodeList As XmlNodeList
        Dim xNodeChild As XmlNode
        Dim xLogNodeList As XmlNodeList
        Dim iKey As Integer = 0
        Dim xNode As XmlNode
        Dim xNodeRoot As XmlNode
        Dim latencyPointsDtoObj As LatencyPoint

        'Get the absolute path of the XML file
        inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & GetXMLFileName(ApplicationCode)
        '=====================================================
        'TODO:For XML testing
        'inputXmlPath = "C:\Enquiry\testClient.xml"
        '=====================================================
        'Read XML
        Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)

        xdoc.LoadXml(xmlFile)

        xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
        'Need to branch here if EnquiryType is FinanceFacility as this EnquiryType has no loan
        If Not xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan") Is Nothing Then
            xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")

            xLogNodeList = xNode.SelectNodes("Logs/Log")

            alLatencyPoints = New ArrayList
            '==============================================
            'Read Loan
            '==============================================
            'load (Loan) Log notes into alLatencyPoints
            If Not xLogNodeList Is Nothing Then

                For Each xNodeLog In xLogNodeList
                    latencyPointsDtoObj = New LatencyPoint
                    latencyPointsDtoObj.LogType = Constants.CONST_LATENCY_POINTS_LOG_TYPE_LOAN
                    xNode = xNodeLog.SelectSingleNode("Subject")
                    If Not xNode Is Nothing Then
                        latencyPointsDtoObj.Subject = xNode.InnerText
                    End If
                    xNode = xNodeLog.SelectSingleNode("Notes")
                    If Not xNode Is Nothing Then
                        latencyPointsDtoObj.Notes = xNode.InnerText
                    End If
                    xNode = xNodeLog.SelectSingleNode("ActionDate")
                    If Not xNode Is Nothing Then
                        latencyPointsDtoObj.ActionDate = xNode.InnerText
                    End If
                    alLatencyPoints.Add(latencyPointsDtoObj)
                Next
            End If
            'load (Loan) LatencyPoints into htXmlLogs at key '0'
            htXmlLogs.Add(0, alLatencyPoints) ' Only one Loan in a data file

            '==============================================
            'Read Clients One by One
            '==============================================
            xJointNameNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
            'load Client Log notes into alLatencyPoints
            'Load each alLatencyPoints into htXmlLogs at key of ikey
            For Each xNodeJointName In xJointNameNodeList
                iKey = iKey + 1
                alLatencyPoints = New ArrayList
                xLogNodeList = xNodeJointName.SelectNodes("Client/Logs/Log")

                If Not xLogNodeList Is Nothing Then
                    For Each xNodeLog In xLogNodeList
                        latencyPointsDtoObj = New LatencyPoint
                        latencyPointsDtoObj.LogType = Constants.CONST_LATENCY_POINTS_LOG_TYPE_CLIENT
                        xNode = xNodeLog.SelectSingleNode("Subject")
                        If Not xNode Is Nothing Then
                            latencyPointsDtoObj.Subject = xNode.InnerText
                        End If
                        xNode = xNodeLog.SelectSingleNode("Notes")
                        If Not xNode Is Nothing Then
                            latencyPointsDtoObj.Notes = xNode.InnerText
                        End If
                        xNode = xNodeLog.SelectSingleNode("ActionDate")
                        If Not xNode Is Nothing Then
                            latencyPointsDtoObj.ActionDate = xNode.InnerText
                        End If
                        alLatencyPoints.Add(latencyPointsDtoObj)
                    Next
                End If
                htXmlLogs.Add(iKey, alLatencyPoints)
            Next

        ElseIf Not xNodeRoot.SelectSingleNode("/finPOWER/Clients/Client") Is Nothing Then
            xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Clients/Client")
            '=====================================================
            'Reads client elements one by one
            '=====================================================

            For Each xNodeChild In xRootNodeList
                iKey = iKey + 1
                alLatencyPoints = New ArrayList
                xLogNodeList = xNodeChild.SelectNodes("Client/Logs/Log")
                If Not xLogNodeList Is Nothing Then
                    For Each xNodeLog In xLogNodeList
                        latencyPointsDtoObj = New LatencyPoint
                        latencyPointsDtoObj.LogType = Constants.CONST_LATENCY_POINTS_LOG_TYPE_CLIENT
                        xNode = xNodeLog.SelectSingleNode("Subject")
                        If Not xNode Is Nothing Then
                            latencyPointsDtoObj.Subject = xNode.InnerText
                        End If
                        xNode = xNodeLog.SelectSingleNode("Notes")
                        If Not xNode Is Nothing Then
                            latencyPointsDtoObj.Notes = xNode.InnerText
                        End If
                        xNode = xNodeLog.SelectSingleNode("ActionDate")
                        If Not xNode Is Nothing Then
                            latencyPointsDtoObj.ActionDate = xNode.InnerText
                        End If
                        alLatencyPoints.Add(latencyPointsDtoObj)
                    Next
                End If
                htXmlLogs.Add(iKey, alLatencyPoints)
            Next

        End If

        Return htXmlLogs

    End Function


    ''' <summary>
    ''' Updates the XML file with param and saves it
    ''' </summary>
    ''' <param name="htClientLatencyPoints"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>In the Hashtable htClientLatencyPoints Key:0 contains Loan LatencyPoints values. The arraylist alLatencyPointsDefaultList contains default latencyPoints list from LatencyPoints table</remarks>
    Public Function UpdateLatencyPointsIntoXMLFile(ByVal htClientLatencyPoints As Hashtable, ByVal alLatencyPointsDefaultList As ArrayList) As Boolean
        Dim subject As String = ""
        Dim Notes As String = ""
        Dim LogType As String = ""
        Dim OutputXMLPath As String
        Dim inputXmlPath As String
        Dim updatedXML As String = ""
        Dim xdoc As New XmlDocument
        Dim xNodeJointName As XmlNode
        Dim xNodeLogs As XmlNode
        Dim xNodeChild As XmlNode
        Dim xRootNodeList As XmlNodeList
        Dim xLogNodeList As XmlNodeList
        Dim i As Integer = 0
        Dim iKey As Integer = 0
        Dim xNode As XmlNode
        Dim xNodeLoan As XmlNode
        Dim xNodeRoot As XmlNode
        Dim xNodeLog As XmlNode
        Dim clientType As String = ""
        Dim newElem As XmlNode
        Dim newChildElem As XmlNode
        Dim dateCreated As String = ""
        Dim managerId As String = ""
        Dim LatencyPointsDTOObj As LatencyPoint
        Dim doc As XmlDocument
        Dim alLatencyPoints As ArrayList
        Dim alDefaultLatencyPoints As ArrayList = Nothing

        'param alLatencyPointsDefaultList contains arraylist of default LatencyPoints
        alDefaultLatencyPoints = alLatencyPointsDefaultList

        doc = New XmlDocument()
        'Get the absolute path of the XML file
        inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & GetXMLFileName(ApplicationCode)
        '=====================================================
        'TODO:For XML testing
        'inputXmlPath = "C:\Enquiry\testClient.xml"
        '=====================================================
        'Read XML
        Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)

        xdoc.LoadXml(xmlFile)

        xNodeRoot = xdoc.SelectSingleNode("/finPOWER")
        'Need to branch here if EnquiryType is FinanceFacility as this EnquiryType has no loan
        If Not xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan") Is Nothing Then
            xNodeLoan = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")

            xNodeLogs = xNodeLoan.SelectSingleNode("Logs")
            'Why this?
            If xNodeLogs IsNot Nothing Then
                xNodeChild = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/Logs/Log/Date")

                If xNodeChild IsNot Nothing Then
                    dateCreated = xNodeChild.InnerText
                End If
                xNodeChild = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/Logs/Log/ManagerId")

                If xNodeChild IsNot Nothing Then
                    managerId = xNodeChild.InnerText
                End If
            End If
            'End of Why this?

            'NOTE: Loan Latency Points will always in the first Grid (iKey = 0)
            alLatencyPoints = CType(htClientLatencyPoints.Item(iKey), ArrayList)
            xLogNodeList = xNodeLoan.SelectNodes("Logs/Log")
            If Not xLogNodeList Is Nothing And Not alDefaultLatencyPoints Is Nothing Then
                'iterate through Loan/Logs
                'we are wanting to remove Logs that are Latency Points
                For Each xNodeLog In xLogNodeList
                    xNode = xNodeLog.SelectSingleNode("Notes")
                    If Not xNode Is Nothing Then
                        Notes = xNode.InnerText
                    End If
                    xNode = xNodeLog.SelectSingleNode("Subject")
                    If Not xNode Is Nothing Then
                        subject = xNode.InnerText
                        For i = 0 To alDefaultLatencyPoints.Count - 1
                            LatencyPointsDTOObj = TryCast(alDefaultLatencyPoints(i), LatencyPoint)
                            If Not LatencyPointsDTOObj Is Nothing Then
                                If LatencyPointsDTOObj.LogType = "Loan" Then
                                    If LatencyPointsDTOObj.Subject = subject And LatencyPointsDTOObj.LogType = Constants.CONST_LATENCY_POINTS_LOG_TYPE_LOAN Then
                                        'If have a match then remove
                                        xNodeLog.ParentNode.RemoveChild(xNodeLog)
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
            End If

            'Loan Latency Points in Logs
            For i = 0 To alLatencyPoints.Count - 1
                LatencyPointsDTOObj = TryCast(alLatencyPoints(i), LatencyPoint)
                If LatencyPointsDTOObj IsNot Nothing Then
                    If LatencyPointsDTOObj.LogType = "Loan" Then
                        If xNodeLogs Is Nothing Then
                            newElem = xdoc.CreateNode("element", "Logs", "")
                            xNodeLoan.AppendChild(newElem)
                            xNodeLogs = xNodeLoan.SelectSingleNode("Logs")
                        End If
                        If xNodeLogs IsNot Nothing Then
                            newElem = xdoc.CreateNode("element", "Log", "")

                            newChildElem = xdoc.CreateNode("element", "Date", "")
                            newChildElem.InnerText = dateCreated
                            newElem.AppendChild(newChildElem)

                            newChildElem = xdoc.CreateNode("element", "Subject", "")
                            newChildElem.InnerText = LatencyPointsDTOObj.Subject
                            newElem.AppendChild(newChildElem)

                            newChildElem = xdoc.CreateNode("element", "Notes", "")
                            newChildElem.InnerText = LatencyPointsDTOObj.Notes
                            newElem.AppendChild(newChildElem)

                            newChildElem = xdoc.CreateNode("element", "ManagerId", "")
                            newChildElem.InnerText = managerId
                            newElem.AppendChild(newChildElem)

                            newChildElem = xdoc.CreateNode("element", "ActionDate", "")
                            newChildElem.InnerText = LatencyPointsDTOObj.ActionDate
                            newElem.AppendChild(newChildElem)
                            xNodeLogs.AppendChild(newElem)
                        End If
                    End If 'LogType = "Loan"
                End If
            Next


            '==============================================
            'Read Clients One by One
            '==============================================
            xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
            '==============================================
            'Removing Latency Points Logs if Exits from XML file. Adds afresh complete Latency Points
            'iterate through JointName/Client/Logs/
            'we are wanting to remove Logs that are Latency Points
            'We are using the same order as was used in setting up the datagridviews so clients should be automatically in correct sequence
            '==============================================
            For Each xNodeJointName In xRootNodeList
                xLogNodeList = xNodeJointName.SelectNodes("Client/Logs/Log")
                If Not xLogNodeList Is Nothing And Not alDefaultLatencyPoints Is Nothing Then
                    For Each xNodeLog In xLogNodeList
                        xNode = xNodeLog.SelectSingleNode("Notes")
                        If Not xNode Is Nothing Then
                            Notes = xNode.InnerText
                        End If
                        xNode = xNodeLog.SelectSingleNode("Subject")
                        If Not xNode Is Nothing Then
                            subject = xNode.InnerText
                            For i = 0 To alDefaultLatencyPoints.Count - 1
                                LatencyPointsDTOObj = TryCast(alDefaultLatencyPoints(i), LatencyPoint)
                                If Not LatencyPointsDTOObj Is Nothing Then
                                    If LatencyPointsDTOObj.LogType = "Client" Then
                                        If LatencyPointsDTOObj.Subject = subject Then
                                            xNodeLog.ParentNode.RemoveChild(xNodeLog)
                                            Exit For
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
                iKey = iKey + 1
                xNode = xNodeJointName.SelectSingleNode("Type")
                If xNode IsNot Nothing Then
                    'If xNode.InnerText.ToUpper = "MAIN" Then
                    'NOTE: Client Latency Points will always after the Loan LPs, so start at ikey = 1
                    alLatencyPoints = CType(htClientLatencyPoints.Item(iKey), ArrayList)

                    If Not alLatencyPoints Is Nothing Then
                        xNode = xNodeJointName.SelectSingleNode("Client")
                        If xNode IsNot Nothing Then
                            xNodeLogs = xNode.SelectSingleNode("Logs")

                            'Client Latency Points in Logs
                            For i = 0 To alLatencyPoints.Count - 1
                                LatencyPointsDTOObj = TryCast(alLatencyPoints(i), LatencyPoint)
                                If LatencyPointsDTOObj IsNot Nothing Then
                                    If LatencyPointsDTOObj.LogType = "Client" Then
                                        If xNodeLogs Is Nothing Then
                                            newElem = xdoc.CreateNode("element", "Logs", "")
                                            xNode.AppendChild(newElem)
                                            xNodeLogs = xNode.SelectSingleNode("Logs")
                                        End If
                                        If xNodeLogs IsNot Nothing Then
                                            newElem = xdoc.CreateNode("element", "Log", "")

                                            newChildElem = xdoc.CreateNode("element", "Date", "")
                                            newChildElem.InnerText = dateCreated
                                            newElem.AppendChild(newChildElem)

                                            newChildElem = xdoc.CreateNode("element", "Subject", "")
                                            newChildElem.InnerText = LatencyPointsDTOObj.Subject
                                            newElem.AppendChild(newChildElem)

                                            newChildElem = xdoc.CreateNode("element", "Notes", "")
                                            newChildElem.InnerText = LatencyPointsDTOObj.Notes
                                            newElem.AppendChild(newChildElem)

                                            newChildElem = xdoc.CreateNode("element", "ManagerId", "")
                                            newChildElem.InnerText = managerId
                                            newElem.AppendChild(newChildElem)

                                            newChildElem = xdoc.CreateNode("element", "ActionDate", "")
                                            newChildElem.InnerText = LatencyPointsDTOObj.ActionDate
                                            newElem.AppendChild(newChildElem)
                                            xNodeLogs.AppendChild(newElem)
                                        End If
                                    End If
                                End If
                            Next 'alLatencyPoints.Count

                        End If 'xNode
                    End If 'Not alLatencyPoints Is Nothing
                    'End If 'xNode.InnerText.ToUpper = "MAIN"
                End If 'xNode IsNot Nothing

            Next 'xRootNodeList

        ElseIf Not xNodeRoot.SelectSingleNode("/finPOWER/Clients/Client") Is Nothing Then
            xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Clients/Client")
            '=====================================================
            'Reads client elements one by one
            '=====================================================
            'Removing Latency Points Logs if Exits from XML file. Adds afresh complete Latency Points
            'iterate through JointName/Client/Logs/
            'we are wanting to remove Logs that are Latency Points
            'We are using the same order as was used in setting up the datagridviews so clients should be automatically in correct sequence
            '==============================================
            For Each xNodeChild In xRootNodeList
                xLogNodeList = xNodeChild.SelectNodes("Logs/Log")
                If Not xLogNodeList Is Nothing And Not alDefaultLatencyPoints Is Nothing Then
                    For Each xNodeLog In xLogNodeList
                        xNode = xNodeLog.SelectSingleNode("Notes")
                        If Not xNode Is Nothing Then
                            Notes = xNode.InnerText
                        End If
                        xNode = xNodeLog.SelectSingleNode("Subject")
                        If Not xNode Is Nothing Then
                            subject = xNode.InnerText
                            For i = 0 To alDefaultLatencyPoints.Count - 1
                                LatencyPointsDTOObj = TryCast(alDefaultLatencyPoints(i), LatencyPoint)
                                If Not LatencyPointsDTOObj Is Nothing Then
                                    If LatencyPointsDTOObj.LogType = "Client" Then
                                        If LatencyPointsDTOObj.Subject = subject Then
                                            xNodeLog.ParentNode.RemoveChild(xNodeLog)
                                            Exit For
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
                iKey = iKey + 1
                alLatencyPoints = CType(htClientLatencyPoints.Item(iKey), ArrayList)
                If Not alLatencyPoints Is Nothing Then
                    xNodeLogs = xNodeChild.SelectSingleNode("Logs")
                    'Client Latency Points in Logs
                    For i = 0 To alLatencyPoints.Count - 1
                        LatencyPointsDTOObj = TryCast(alLatencyPoints(i), LatencyPoint)
                        If LatencyPointsDTOObj IsNot Nothing Then
                            If LatencyPointsDTOObj.LogType = "Client" Then
                                If xNodeLogs Is Nothing Then
                                    newElem = xdoc.CreateNode("element", "Logs", "")
                                    xNodeChild.AppendChild(newElem)
                                    xNodeLogs = xNodeChild.SelectSingleNode("Logs")
                                End If
                                If xNodeLogs IsNot Nothing Then
                                    newElem = xdoc.CreateNode("element", "Log", "")

                                    newChildElem = xdoc.CreateNode("element", "Date", "")
                                    newChildElem.InnerText = dateCreated
                                    newElem.AppendChild(newChildElem)

                                    newChildElem = xdoc.CreateNode("element", "Subject", "")
                                    newChildElem.InnerText = LatencyPointsDTOObj.Subject
                                    newElem.AppendChild(newChildElem)

                                    newChildElem = xdoc.CreateNode("element", "Notes", "")
                                    newChildElem.InnerText = LatencyPointsDTOObj.Notes
                                    newElem.AppendChild(newChildElem)

                                    newChildElem = xdoc.CreateNode("element", "ManagerId", "")
                                    newChildElem.InnerText = managerId
                                    newElem.AppendChild(newChildElem)

                                    newChildElem = xdoc.CreateNode("element", "ActionDate", "")
                                    newChildElem.InnerText = LatencyPointsDTOObj.ActionDate
                                    newElem.AppendChild(newChildElem)
                                    xNodeLogs.AppendChild(newElem)
                                End If
                            End If
                        End If
                    Next 'alLatencyPoints.Count

                End If

            Next


        End If

        

        '==============================================
        'WRITE XML file to finPOWER Import folder
        '==============================================
        OutputXMLPath = ApplicationInterchange.GetWorkSheetFolder(EnquiryCode) & GetXMLFileName(ApplicationCode)
        '=====================================================
        'TODO:For XML testing
        'OutputXMLPath = "C:\Enquiry\testClient.xml"
        '=====================================================
        Try
            'Resources finPowerImportFolder
            System.IO.File.WriteAllText(OutputXMLPath, xdoc.InnerXml)
        Catch ex As Exception
            log.Error("Error while writing Latency points " & GetXMLFileName(EnquiryCode) & ": " & ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox("Error while writing Latency points " & GetXMLFileName(EnquiryCode) & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)
        End Try

        Return True

    End Function





    ''Updates the XML file and saves for further processing
    'Public Function updateLatencyPointsIntoXMLFile(ByVal alLatencyPoints As ArrayList) As Boolean
    '    Dim OutputXMLPath As String
    '    Dim inputXmlPath As String
    '    Dim updatedXML As String = ""

    '    Dim xdoc As New XmlDocument

    '    Dim xNodeJointName As XmlNode
    '    Dim xNodeLogs As XmlNode
    '    Dim xNodeChild As XmlNode
    '    Dim xRootNodeList As XmlNodeList
    '    Dim j As Integer
    '    Dim i As Integer = 0
    '    Dim xNode As XmlNode
    '    Dim xNodeRoot As XmlNode
    '    Dim clientType As String = ""
    '    Dim newElem As XmlNode
    '    Dim newChildElem As XmlNode
    '    Dim dateCreated As String = ""
    '    Dim managerId As String = ""
    '    Dim LatencyPointsDTOObj As LatencyPointsDTO


    '    Dim doc As XmlDocument = New XmlDocument()
    '    'Get the absolute path of the XML file
    '    inputXmlPath = getEnquiryFilePath() & getXMLFileName()
    '    'Read XML
    '    Dim xmlFile As String = System.IO.File.ReadAllText(inputXmlPath)

    '    xdoc.LoadXml(xmlFile)

    '    xNodeRoot = xdoc.SelectSingleNode("/finPOWER")

    '    xNode = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan")
    '    xNodeLogs = xNode.SelectSingleNode("Logs")

    '    If xNodeLogs IsNot Nothing Then
    '        xNodeChild = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/Logs/Log/Date")

    '        If xNodeChild IsNot Nothing Then
    '            dateCreated = xNodeChild.InnerText
    '        End If
    '        xNodeChild = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/Logs/Log/ManagerId")

    '        If xNodeChild IsNot Nothing Then
    '            managerId = xNodeChild.InnerText
    '        End If
    '    End If

    '    'Loan Latency Points in Logs
    '    For i = 0 To alLatencyPoints.Count - 1
    '        LatencyPointsDTOObj = TryCast(alLatencyPoints(i), LatencyPointsDTO)
    '        If LatencyPointsDTOObj IsNot Nothing Then
    '            If LatencyPointsDTOObj.LogType = "Loan" Then
    '                If xNodeLogs Is Nothing Then
    '                    newElem = xdoc.CreateNode("element", "Logs", "")
    '                    xNode.AppendChild(newElem)
    '                    xNodeLogs = xNode.SelectSingleNode("Logs")

    '                End If
    '                If xNodeLogs IsNot Nothing Then
    '                    newElem = xdoc.CreateNode("element", "Log", "")

    '                    newChildElem = xdoc.CreateNode("element", "Date", "")
    '                    newChildElem.InnerText = dateCreated
    '                    newElem.AppendChild(newChildElem)

    '                    newChildElem = xdoc.CreateNode("element", "Subject", "")
    '                    newChildElem.InnerText = LatencyPointsDTOObj.Subject
    '                    newElem.AppendChild(newChildElem)

    '                    newChildElem = xdoc.CreateNode("element", "Notes", "")
    '                    newChildElem.InnerText = LatencyPointsDTOObj.Notes
    '                    newElem.AppendChild(newChildElem)

    '                    newChildElem = xdoc.CreateNode("element", "ManagerId", "")
    '                    newChildElem.InnerText = managerId
    '                    newElem.AppendChild(newChildElem)

    '                    newChildElem = xdoc.CreateNode("element", "ActionDate", "")
    '                    newChildElem.InnerText = LatencyPointsDTOObj.ActionDate
    '                    newElem.AppendChild(newChildElem)
    '                    xNodeLogs.AppendChild(newElem)
    '                End If
    '            End If 'LogType = "Loan"
    '        End If
    '    Next


    '    '==============================================
    '    'Read Clients One by One
    '    '==============================================
    '    xRootNodeList = xNodeRoot.SelectNodes("/finPOWER/Loans/Loan/JointNames/JointName")
    '    For Each xNodeJointName In xRootNodeList

    '        xNode = xNodeJointName.SelectSingleNode("Type")
    '        If xNode IsNot Nothing Then
    '            If xNode.InnerText.ToUpper = "MAIN" Then

    '                xNode = xNodeJointName.SelectSingleNode("Client")
    '                If xNode IsNot Nothing Then
    '                    xNodeLogs = xNodeRoot.SelectSingleNode("/finPOWER/Loans/Loan/JointNames/JointName/Client/Logs")

    '                    'Client Latency Points in Logs
    '                    For i = 0 To alLatencyPoints.Count - 1
    '                        LatencyPointsDTOObj = TryCast(alLatencyPoints(i), LatencyPointsDTO)
    '                        If LatencyPointsDTOObj IsNot Nothing Then
    '                            If LatencyPointsDTOObj.LogType = "Client" Then
    '                                If xNodeLogs Is Nothing Then
    '                                    newElem = xdoc.CreateNode("element", "Logs", "")
    '                                    xNode.AppendChild(newElem)
    '                                    xNodeLogs = xNode.SelectSingleNode("Logs")
    '                                End If
    '                                If xNodeLogs IsNot Nothing Then
    '                                    newElem = xdoc.CreateNode("element", "Log", "")

    '                                    newChildElem = xdoc.CreateNode("element", "Date", "")
    '                                    newChildElem.InnerText = dateCreated
    '                                    newElem.AppendChild(newChildElem)

    '                                    newChildElem = xdoc.CreateNode("element", "Subject", "")
    '                                    newChildElem.InnerText = LatencyPointsDTOObj.Subject
    '                                    newElem.AppendChild(newChildElem)

    '                                    newChildElem = xdoc.CreateNode("element", "Notes", "")
    '                                    newChildElem.InnerText = LatencyPointsDTOObj.Notes
    '                                    newElem.AppendChild(newChildElem)

    '                                    newChildElem = xdoc.CreateNode("element", "ManagerId", "")
    '                                    newChildElem.InnerText = managerId
    '                                    newElem.AppendChild(newChildElem)

    '                                    newChildElem = xdoc.CreateNode("element", "ActionDate", "")
    '                                    newChildElem.InnerText = LatencyPointsDTOObj.ActionDate
    '                                    newElem.AppendChild(newChildElem)
    '                                    xNodeLogs.AppendChild(newElem)
    '                                End If
    '                            End If
    '                        End If
    '                    Next

    '                End If
    '            End If

    '        End If

    '    Next

    '    '==============================================
    '    'WRITE XML file to finPOWER Import folder
    '    '==============================================
    '    OutputXMLPath = getEnquiryFilePath() & getXMLFileName()
    '    Try
    '        'Resources finPowerImportFolder
    '        System.IO.File.WriteAllText(OutputXMLPath, xdoc.InnerXml)
    '    Catch ex As Exception
    '        MsgBox("Error while writing Latency points " & getXMLFileName() & Environment.NewLine & ex.Message, MsgBoxStyle.OkOnly)
    '    End Try

    '    Return True

    'End Function
    '*****************************************************************************************

    ''' <summary>
    ''' Contains Clients (Dictionary(Of Integer, ClientMappingDetails)), Status, Error Message
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ClientInfo
        Public Clients As New Dictionary(Of Integer, ClientMappingDetails)
        Public Status As Boolean
        Public ErrorMessage As String
    End Class

    ''' <summary>
    ''' Object to store TrueTrack EnquiryCustomer details: IdNumber As Integer (CustomerEnqury.Id_number), Name  As String (display name), Type As Integer Enum of CustomerEnquiryType (main, co-main etc), CustomerType As Integer Enum of CustomerType (individual etc), CustomerId As Integer (TrueTrack CustomerId), EnquiryId As Integer (TrueTrack EnquiryId), ClientTypeId As String (finPower Client Type (C)), ClientNum As String (finPower Client number), XRefId As String (OAC CustomerId)
    ''' </summary>
    ''' <remarks>Same members as EnquiryCustomer DB view</remarks>
    Public Class EnqCustomer
        Public IdNumber As Integer 'CustomerEnqury.Id_number
        Public Name As String 'display name
        Public Type As Integer 'Enum of CustomerEnquiryType (main, co-main etc)
        Public CustomerType As Integer 'Enum of CustomerType (individual etc)
        Public CustomerId As Integer 'TrueTrack CustomerId
        Public EnquiryId As Integer 'TrueTrack EnquiryId
        Public ClientTypeId As String 'finPower Client Type (C)
        Public ClientNum As String 'finPower Client number
        Public XRefId As String 'OAC CustomerId
        Public DriverLicenceType As Integer 'TT Customer Driver's Licence Type using Public Enum DriverLicenceType

    End Class

    ''' <summary>
    ''' Store TrueTrack EnquiryCustomers: Customers Dictionary, Status, ErrorMessage
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ttCustomerInfo
        Public Customers As New Dictionary(Of Integer, EnqCustomer)
        Public Status As Boolean
        Public ErrorMessage As String
    End Class



    ''' <summary>
    '''  Object to store Customer details after matching: Name As String, CustomerId As Integer, XRefId As String (OAC CustomerId)
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CustomerInfo
        Public Name As String 'display name
        Public CustomerId As Integer 'CustomerId
        Public XRefId As String 'OAC CustomerId

    End Class

    ''' <summary>
    ''' Store Latency Points: Id, LogType, Subject, Notes, ActionDate
    ''' </summary>
    ''' <remarks></remarks>
    Public Class LatencyPoint
        Public LatencyPointsId As Integer
        Public LogType As String
        Public Subject As String
        Public Notes As String
        Public ActionDate As String

    End Class




End Class








