﻿Imports System
Imports System.ComponentModel
Imports System.ComponentModel.Design.Serialization
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Forms.Integration
Imports System.Windows.Forms.Design

<Designer(GetType(ControlDesigner))>
Class SpellBox
    Inherits ElementHost

    'TextOptions.TextFormattingMode="Display"

    Public Sub New()
        box = New TextBox()
        MyBase.Child = box
        AddHandler box.TextChanged, AddressOf box_TextChanged
        box.Language = Markup.XmlLanguage.GetLanguage("en-GB")
        box.SpellCheck.IsEnabled = True
        box.VerticalScrollBarVisibility = ScrollBarVisibility.Auto
        box.Background = System.Windows.SystemColors.WindowBrush
        box.Foreground = System.Windows.SystemColors.WindowTextBrush

        Me.Size = New System.Drawing.Size(100, 20)
        ''custom dictionaries
        'Dim dictionaries As IList = SpellCheck.GetCustomDictionaries(box)
        '' customwords.lex is included as a content file
        'dictionaries.Add(New Uri("pack://application:,,,/CustomDictionaries/customwords.lex"))
    End Sub

    Private Sub box_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        OnTextChanged(EventArgs.Empty)
    End Sub

    Public Sub Copy()
        box.Copy()
    End Sub

    Public Sub Cut()
        box.Cut()
    End Sub

    Public Sub Paste()
        box.Paste()
    End Sub

    Public Sub Undo()
        box.Undo()
    End Sub

    'Public Sub ClearUndo()
    '    box.ClearUndo()
    'End Sub

    Public Property IsReadOnly() As Boolean
        Get
            Return box.IsReadOnly
        End Get
        Set(ByVal value As Boolean)
            box.IsReadOnly = value
        End Set
    End Property

    Public Property MaxLength As Integer
        Get
            Return box.MaxLength
        End Get
        Set(ByVal value As Integer)
            box.MaxLength = value
        End Set
    End Property

    Public Overrides Property Text() As String
        Get
            Return box.Text
        End Get
        Set(ByVal value As String)
            box.Text = value
        End Set
    End Property


    Public Property SelectedText() As String
        Get
            Return box.SelectedText
        End Get
        Set(ByVal value As String)
            box.SelectedText = value
        End Set
    End Property


    Public Property SelectionLength() As Integer
        Get
            Return box.SelectionLength
        End Get
        Set(ByVal value As Integer)
            box.SelectionLength = value
        End Set
    End Property


    Public Property SelectionStart() As Integer
        Get
            Return box.SelectionStart
        End Get
        Set(ByVal value As Integer)
            box.SelectionStart = value
        End Set
    End Property


    Public ReadOnly Property CanUndo() As Boolean
        Get
            Return box.CanUndo
        End Get
    End Property

    <DefaultValue(False)>
        Public Property IsUndoEnabled() As Boolean
        Get
            Return box.IsUndoEnabled
        End Get
        Set(ByVal value As Boolean)
            box.IsUndoEnabled = value
        End Set
    End Property

    <DefaultValue(False)>
    Public Property MultiLine() As Boolean
        Get
            Return box.AcceptsReturn
        End Get
        Set(ByVal value As Boolean)
            box.AcceptsReturn = value
        End Set
    End Property

    <DefaultValue(False)>
    Public Property WordWrap() As Boolean
        Get
            Return box.TextWrapping <> TextWrapping.NoWrap
        End Get
        Set(ByVal value As Boolean)
            If value Then
                box.TextWrapping = TextWrapping.Wrap
            Else
                box.TextWrapping = TextWrapping.NoWrap
            End If
        End Set
    End Property

    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property Child() As System.Windows.UIElement
        Get
            Return MyBase.Child
        End Get
        Set(ByVal value As System.Windows.UIElement)
            '' Do nothing to solve a problem with the serializer !!
        End Set
    End Property

    Private box As TextBox

End Class