﻿Imports System.Text
Imports System.Collections.ObjectModel
Imports System.IO 'for file system operations
 
Public Class Util
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    ''' <summary>
    ''' Encode specified string to Base64.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function EncodeTo64(toEncode As String) As String

        Dim toEncodeAsBytes() As Byte = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode)
        Dim returnValue As String = System.Convert.ToBase64String(toEncodeAsBytes)
        Return returnValue

    End Function

    ''' <summary>
    ''' Set all controls in Control Collection to ReadOnly/Disable
    ''' </summary>
    ''' <param name="controlCollection"></param>
    ''' <remarks>e.g. SetReadonlyControls(groupBox1.Controls)</remarks>
    Shared Sub SetReadonlyControls(controlCollection As Control.ControlCollection)
        'Donot seem to be able to use a dataRepeater in this 
        If controlCollection Is Nothing Then
            Return
        End If

        For Each ctrl As Control In controlCollection
            If ctrl.[GetType]().Name = "TextBox" Then
                Dim tb As TextBox = TryCast(ctrl, TextBox)
                tb.[ReadOnly] = True
            ElseIf ctrl.[GetType]().Name = "ComboBox" Then
                Dim cb As ComboBox = TryCast(ctrl, ComboBox)
                cb.[Enabled] = False
            ElseIf ctrl.[GetType]().Name = "CheckBox" Then
                Dim cbx As CheckBox = TryCast(ctrl, CheckBox)
                cbx.[Enabled] = False
            ElseIf ctrl.[GetType]().Name = "RadioButton" Then
                Dim rb As RadioButton = TryCast(ctrl, RadioButton)
                rb.[Enabled] = False
            ElseIf ctrl.[GetType]().Name = "Button" Then
                Dim b As Button = TryCast(ctrl, Button)
                b.[Enabled] = False
            ElseIf ctrl.[GetType]().Name = "TableLayoutPanel" Then
                ' If the control has children, 
                ' recursively call this function
                If ctrl.HasChildren Then
                    SetReadonlyControls(CType(ctrl, TableLayoutPanel).Controls)
                End If
            ElseIf ctrl.[GetType]().Name = "GroupBox" Then
                ' If the control has children, 
                ' recursively call this function
                If ctrl.HasChildren Then
                    SetReadonlyControls(CType(ctrl, GroupBox).Controls)
                End If
            Else
                'ctrl.[Enabled] = False
            End If

        Next

    End Sub
    ''' <summary>
    ''' Set DataRepeater textbox to ReadOnly
    ''' </summary>
    ''' <param name="dr"></param>
    ''' <remarks></remarks>
    Shared Sub SetDrReadOnly(dr As Microsoft.VisualBasic.PowerPacks.DataRepeater)

        If dr.ItemTemplate.Controls.Count > 0 Then
            For Each ctrl As Control In dr.ItemTemplate.Controls
                If ctrl.[GetType]().Name = "TextBox" Then
                    Dim tb As TextBox = TryCast(ctrl, TextBox)
                    tb.[ReadOnly] = True
                End If
            Next
        End If

    End Sub
    ''' <summary>
    ''' Set BackColor of all controls in Control Collection (option to include a button)
    ''' </summary>
    ''' <param name="controlCollection">ControlCollection</param>
    ''' <param name="newBackColor">Color</param>
    ''' <remarks>Use KnownColor to set to a SystemColors colour: e.g. newBackColor = Color.FromKnownColor(KnownColor.Window)</remarks>
    Shared Sub SetControlsBackColor(controlCollection As Control.ControlCollection, ByVal newBackColor As Color, Optional ByVal includeButton As Boolean = False)
        'Donot seem to be able to use a dataRepeater in this 
        If controlCollection Is Nothing Then
            Return
        End If

        For Each ctrl As Control In controlCollection
            If ctrl.[GetType]().Name = "TextBox" Then
                Dim tb As TextBox = TryCast(ctrl, TextBox)
                tb.BackColor = newBackColor
            ElseIf ctrl.[GetType]().Name = "ComboBox" Then
                Dim cb As ComboBox = TryCast(ctrl, ComboBox)
                cb.BackColor = newBackColor
            ElseIf ctrl.[GetType]().Name = "CheckBox" Then
                Dim cbx As CheckBox = TryCast(ctrl, CheckBox)
                cbx.BackColor = newBackColor
            ElseIf ctrl.[GetType]().Name = "RadioButton" Then
                Dim rb As RadioButton = TryCast(ctrl, RadioButton)
                rb.BackColor = newBackColor
            ElseIf ctrl.[GetType]().Name = "Button" Then
                If includeButton = True Then
                    Dim b As Button = TryCast(ctrl, Button)
                    b.BackColor = newBackColor
                End If
            ElseIf ctrl.[GetType]().Name = "TableLayoutPanel" Then
                ' If the control has children, 
                ' recursively call this function
                If ctrl.HasChildren Then
                    SetControlsBackColor(CType(ctrl, TableLayoutPanel).Controls, newBackColor, includeButton)
                End If
            ElseIf ctrl.[GetType]().Name = "GroupBox" Then
                ' If the control has children, 
                ' recursively call this function
                If ctrl.HasChildren Then
                    SetControlsBackColor(CType(ctrl, GroupBox).Controls, newBackColor, includeButton)
                End If
            Else
                'ctrl.[Enabled] = False
            End If

        Next

    End Sub

    ''' <summary>
    ''' Reset BackColor of all controls in Control Collection back to default value (option to include a button)
    ''' </summary>
    ''' <param name="controlCollection"></param>
    ''' <remarks></remarks>
    Shared Sub ReSetControlsBackColor(controlCollection As Control.ControlCollection, Optional ByVal includeButton As Boolean = False)
        'Donot seem to be able to use a dataRepeater in this 
        If controlCollection Is Nothing Then
            Return
        End If

        For Each ctrl As Control In controlCollection
            If ctrl.[GetType]().Name = "TextBox" Then
                Dim tb As TextBox = TryCast(ctrl, TextBox)
                tb.BackColor = Color.Empty
            ElseIf ctrl.[GetType]().Name = "ComboBox" Then
                Dim cb As ComboBox = TryCast(ctrl, ComboBox)
                cb.BackColor = Color.Empty
            ElseIf ctrl.[GetType]().Name = "CheckBox" Then
                Dim cbx As CheckBox = TryCast(ctrl, CheckBox)
                cbx.BackColor = Color.Empty
            ElseIf ctrl.[GetType]().Name = "RadioButton" Then
                Dim rb As RadioButton = TryCast(ctrl, RadioButton)
                rb.BackColor = Color.Empty
            ElseIf ctrl.[GetType]().Name = "Button" Then
                If includeButton = True Then
                    Dim b As Button = TryCast(ctrl, Button)
                    b.BackColor = Color.Empty
                End If
            ElseIf ctrl.[GetType]().Name = "TableLayoutPanel" Then
                ' If the control has children, 
                ' recursively call this function
                If ctrl.HasChildren Then
                    ReSetControlsBackColor(CType(ctrl, TableLayoutPanel).Controls, includeButton)
                End If
            ElseIf ctrl.[GetType]().Name = "GroupBox" Then
                ' If the control has children, 
                ' recursively call this function
                If ctrl.HasChildren Then
                    ReSetControlsBackColor(CType(ctrl, GroupBox).Controls, includeButton)
                End If
            Else
                'ctrl.[Enabled] = False
            End If

        Next

    End Sub


    ''' <summary>
    ''' Get the parent Panel control 
    ''' </summary>
    ''' <param name="parent"></param>
    ''' <returns></returns>
    ''' <remarks>Used in controlling the mouse wheel scroll</remarks>
    Public Shared Function GetParentPanel(ByVal parent As Control) As Control
        Dim parentControl As Control = TryCast(parent, Control)
        '------------------------------------------------------------------------
        'Specific to a control means if you want to find only for certain control
        If TypeOf parentControl Is Panel Then   'myControl is of UserControl
            Return parentControl
        End If
        '------------------------------------------------------------------------
        If parentControl.Parent Is Nothing Then
            Return parentControl
        End If
        If parent IsNot Nothing Then
            Return GetParentPanel(parent.Parent)
        End If
        Return Nothing
    End Function


    ''' <summary>
    ''' Object to store Field validation messages and error: Message, FieldError
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FieldMessage
        Public Message As String
        Public FieldError As String
    End Class

    ''' <summary>
    ''' Object to store String and Boolean result: StringOut, Result
    ''' </summary>
    ''' <remarks></remarks>
    Public Class StringOut
        Public StringOut As String
        Public Result As Boolean
    End Class


    ''' <summary>
    ''' Contructs a signature using company info
    ''' </summary>
    ''' <param name="makeHtml"></param>
    ''' <returns>String</returns>
    ''' <remarks>If String.IsNullOrEmpty then line is not added to signature</remarks>
    Public Shared Function GetSignature(Optional ByVal makeHtml As Boolean = False) As String
        Dim signatureBuilder As New System.Text.StringBuilder
        Dim thisAppsettings As New AppSettings
        signatureBuilder.Append(LoggedinFullName)
        If Not String.IsNullOrEmpty(thisAppsettings.SystemBusinessName) Then
            If makeHtml Then
                signatureBuilder.Append("<br/>" & thisAppsettings.SystemBusinessName)
            Else
                signatureBuilder.Append(vbCrLf & thisAppsettings.SystemBusinessName)
            End If
        End If
        If Not String.IsNullOrEmpty(thisAppsettings.SystemBusinessPhone) Then
            If makeHtml Then
                signatureBuilder.Append("<br/>" & "Phone: " & thisAppsettings.SystemBusinessPhone)
            Else
                signatureBuilder.Append(vbCrLf & "Phone: " & thisAppsettings.SystemBusinessPhone)
            End If
        End If
        If Not String.IsNullOrEmpty(thisAppsettings.SystemBusinessFax) Then
            If makeHtml Then
                signatureBuilder.Append("<br/>" & "Fax: " & thisAppsettings.SystemBusinessFax)
            Else
                signatureBuilder.Append(vbCrLf & "Fax: " & thisAppsettings.SystemBusinessFax)
            End If
        End If
        If Not String.IsNullOrEmpty(thisAppsettings.SystemBusinessWebsite) Then
            If makeHtml Then
                signatureBuilder.Append("<br/>" & "Website: " & thisAppsettings.SystemBusinessWebsite)
            Else
                signatureBuilder.Append(vbCrLf & "Website: " & thisAppsettings.SystemBusinessWebsite)
            End If
        End If

        Return signatureBuilder.ToString

    End Function

    ''' <summary>
    ''' Insert a comment into EnquiryComment table
    ''' </summary>
    ''' <param name="thisEnquiryId"></param>
    ''' <param name="name"></param>
    ''' <param name="commentText"></param>
    ''' <returns>row Id</returns>
    ''' <remarks></remarks>
    Public Shared Function SetComment(ByVal thisEnquiryId As Integer, ByVal name As String, ByVal commentText As String) As Integer

        Dim commentId As Integer
        Dim enquiryCommentTa As New EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter
        Try
            enquiryCommentTa.InsertEnquiryComment(thisEnquiryId, name, DateTime.Now, commentText, commentId)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.Message, "Error in SetComment function", MessageBoxButtons.OK, MessageBoxIcon.Error)
            commentId = 0
        End Try

        Return commentId

    End Function

    Public Shared Function UpdateComment(ByVal commentText As String, ByVal commentId As Integer) As Boolean
        Dim enquiryCommentTa As New EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter
        Try
            enquiryCommentTa.UpdateEnquiryComment(commentId, commentText)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.Message, "Error in UpdateComment function", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Get all the Enquiry Comments
    ''' </summary>
    ''' <param name="thisEnquiryId"></param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function GetEnquiryComments(ByVal thisEnquiryId As Integer) As String
        Dim strDateCreated As String
        Dim enquiryCommentTa As New EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter
        Dim enquiryCommentDt As New EnquiryWorkSheetDataSet.EnquiryCommentDataTable
        enquiryCommentTa.FillByEnquiryId(enquiryCommentDt, thisEnquiryId)
        'start to string building
        Dim builder As New StringBuilder
        For Each row As DataRow In enquiryCommentDt.Rows
            '-- Thursday, 13 July 2017 11:46 a.m. | kstrange
            'Status changed to Due Diligence

            strDateCreated = CDate(row("DateCreated")).ToString("dddd, dd MMMM yyyy hh:mm tt")
            builder.Append("-- " & strDateCreated & " | " & row("UserId") & vbNewLine & row("Text")).AppendLine()
            builder.AppendLine()
        Next row

        Return builder.ToString

    End Function




    Public Shared Function ValidateDueDiligence(ByVal thisEnquiryId As Integer) As StringOut
        Dim thisTypeOfMainCustomer As Integer ' set Type of Main Client
        Dim docsReceived As Boolean = False
        Dim thisEnquiryType As Integer
        Dim checkFieldsMessage As String = ""
        Dim checkWarningMessage As String = ""
        Dim thisMinLoanValueSecurityInsured As Decimal
        Dim resultOut As New StringOut
        resultOut.StringOut = String.Empty
        resultOut.Result = False

        'Get appSettings
        Try
            Dim thisAppsettings As New AppSettings
            thisMinLoanValueSecurityInsured = thisAppsettings.MinLoanValueSecurityInsured
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message & vbNewLine & ex.TargetSite.ToString)
        End Try

        Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        Try
            enquiryDt = enquiryTa.GetDataByEnquiryId(thisEnquiryId)
            If enquiryDt.Rows.Count = 0 Then
                'MessageBox.Show("Enquiry does not exist", "Validate Due-diligence Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                resultOut.StringOut = "Enquiry does not exist"
                resultOut.Result = False
            ElseIf enquiryDt.Rows.Count > 0 Then
                enquiryRow = enquiryDt.Rows(0)
                'assign values
                thisTypeOfMainCustomer = enquiryRow.mainCustomerType
                thisEnquiryType = enquiryRow.EnquiryType
                docsReceived = If(IsDBNull(enquiryRow.Item("DocsReceived")), False, enquiryRow.Item("DocsReceived"))
                If docsReceived = True Then
                    '#######################
                    'Check Security
                    '#######################
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        Dim securityTa As EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
                        securityTa = New EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
                        Dim securityDt As EnquiryWorkSheetDataSet.SecurityDataTable
                        securityDt = securityTa.GetDataByEnquiryId(thisEnquiryId)
                        If securityDt.Rows.Count > 0 Then
                            'Check that certain security types have a value and insurance
                            For Each securityRow As DataRow In securityDt.Rows
                                'SecurityValued
                                Select Case Trim(securityRow.Item("Type"))
                                    Case Constants.CONST_SEC_BOAT, Constants.CONST_SEC_COMMERCIALMACHINERY, Constants.CONST_SEC_HOUSEHOLD, Constants.CONST_SEC_MISCNONSERIAL, Constants.CONST_SEC_MISCSERIAL, Constants.CONST_SEC_LIVESTOCK, Constants.CONST_SEC_MOTORVEHICLE
                                        If securityRow.Item("SecurityValued") = False Then
                                            checkFieldsMessage = checkFieldsMessage & "Security item must be valued" & vbCrLf
                                        End If
                                    Case Else

                                End Select
                                'SecurityInsured
                                Select Case Trim(securityRow.Item("Type"))
                                    Case Constants.CONST_SEC_BOAT, Constants.CONST_SEC_COMMERCIALMACHINERY, Constants.CONST_SEC_LIVESTOCK, Constants.CONST_SEC_MOTORVEHICLE
                                        If enquiryRow.LoanValue > thisMinLoanValueSecurityInsured Then
                                            If securityRow.Item("SecurityInsured") = False Then
                                                'checkFieldsMessage = checkFieldsMessage & "Security item must be insured" & vbCrLf
                                                checkWarningMessage = checkWarningMessage & "Security item " & securityRow.Item("Description") & " is not insured" & vbCrLf
                                            End If
                                        End If
                                    Case Else

                                End Select
                            Next
                        Else
                            checkFieldsMessage = checkFieldsMessage & "A security item must be added!" & vbCrLf
                        End If 'Security
                    End If 'FinanceFacility
                    '#######################
                    'Check Budget, Financial questions
                    '#######################
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        Select Case thisTypeOfMainCustomer
                            Case MyEnums.mainCustomerType.Individual
                                If enquiryRow.BudgetQuestion1 = 0 Or enquiryRow.BudgetQuestion2 = 0 Or enquiryRow.BudgetQuestion3 = 0 Or enquiryRow.BudgetQuestion4 = 0 Or enquiryRow.BudgetQuestion5 = 0 Or enquiryRow.BudgetQuestion6 = 0 Or enquiryRow.BudgetQuestion7 = 0 Or enquiryRow.BudgetQuestion8 = 0 Or enquiryRow.BudgetQuestion9 = 0 Or enquiryRow.BudgetQuestion10 = 0 Or enquiryRow.BudgetQuestion11 = 0 Or enquiryRow.BudgetQuestion12 = 0 Then
                                    checkFieldsMessage = checkFieldsMessage + "Some Budget Questions still need to be answered!" & vbCrLf
                                End If

                            Case MyEnums.mainCustomerType.Company, MyEnums.mainCustomerType.Trust, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
                                'Count how establishing income
                                Dim countIncome As Integer = 0
                                If enquiryRow.CompanyIncomePrevious = True Then
                                    countIncome = countIncome + 1
                                End If
                                If enquiryRow.CompanyIncomeYTD = True Then
                                    countIncome = countIncome + 1
                                End If
                                If enquiryRow.CompanyIncomeGST = True Then
                                    countIncome = countIncome + 1
                                End If
                                If enquiryRow.CompanyIncomeForcast = True Then
                                    countIncome = countIncome + 1
                                End If
                                If countIncome < 1 Then
                                    checkFieldsMessage = checkFieldsMessage + "Need to have established Business Income in at least one way" & vbCrLf
                                End If
                                If enquiryRow.CompanyIncomeNotes Is DBNull.Value Or enquiryRow.CompanyExpensesNotes Is DBNull.Value Then
                                    checkFieldsMessage = checkFieldsMessage + "Some Business Income and Expenses Questions still need to be answered!" & vbCrLf
                                ElseIf Not enquiryRow.CompanyIncomeNotes.Length > 0 Or Not enquiryRow.CompanyExpensesNotes.Length > 0 Then
                                    checkFieldsMessage = checkFieldsMessage + "Some Business Income and Expenses  Questions still need to be answered!" & vbCrLf
                                End If

                                If thisTypeOfMainCustomer = MyEnums.mainCustomerType.Company Or thisTypeOfMainCustomer = MyEnums.mainCustomerType.Trust Then
                                    'Business Salaries
                                    If enquiryRow.CompanySalariesNotes Is DBNull.Value Then
                                        checkFieldsMessage = checkFieldsMessage + "Business Salaries need to be commented on!" & vbCrLf
                                    ElseIf Not enquiryRow.CompanySalariesNotes.Length > 0 Then
                                        checkFieldsMessage = checkFieldsMessage + "Business Salaries need to be commented on!" & vbCrLf
                                    End If
                                    If enquiryRow.CompanySalariesSatis = False Then
                                        checkFieldsMessage = checkFieldsMessage + "Business Salaries still need to be confirmed satisfactory!" & vbCrLf
                                    End If
                                    'Business Financial position
                                    If enquiryRow.CompanyDebtNotes Is DBNull.Value Then
                                        checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position Questions still need to be answered!" & vbCrLf
                                    ElseIf Not enquiryRow.CompanyDebtNotes.Length > 0 Then
                                        checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position  Questions still need to be answered!" & vbCrLf
                                    End If
                                    If enquiryRow.CompanyAssetRatio = False Then
                                        checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position questions still need to be confirmed!" & vbCrLf
                                    End If
                                End If

                                'Business Earnings
                                If enquiryRow.CompanyEarningsNotes Is DBNull.Value Then
                                    checkFieldsMessage = checkFieldsMessage + "Business Earnings need to be commented on!" & vbCrLf
                                ElseIf Not enquiryRow.CompanyEarningsNotes.Length > 0 Then
                                    checkFieldsMessage = checkFieldsMessage + "Business Earnings need to be commented on!" & vbCrLf
                                End If
                                If enquiryRow.CompanyProfitSatis = False Then
                                    checkFieldsMessage = checkFieldsMessage + "Business Profit still needs to be confirmed satisfactory!" & vbCrLf
                                End If
                                'Business Financial position
                                If enquiryRow.CompanyAcctsPayNotes Is DBNull.Value Or enquiryRow.CompanyTaxesNotes Is DBNull.Value Then
                                    checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position Questions still need to be answered!" & vbCrLf
                                ElseIf Not enquiryRow.CompanyAcctsPayNotes.Length > 0 Or Not enquiryRow.CompanyTaxesNotes.Length > 0 Then
                                    checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position  Questions still need to be answered!" & vbCrLf
                                End If
                                If enquiryRow.CompanyAcctsPaySatis = False Then
                                    checkFieldsMessage = checkFieldsMessage + "Some Business Financial Position questions still need to be confirmed!" & vbCrLf
                                End If

                        End Select
                    End If
                    '#######################
                    'Check customer
                    '#######################
                    Dim custType As Integer
                    Dim custId As Integer
                    Dim custName As String
                    'Check Customers
                    Dim enquiryCustomersTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
                    enquiryCustomersTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
                    Dim enquiryCustomersDt As EnquiryWorkSheetDataSet.EnquiryCustomersDataTable
                    enquiryCustomersDt = enquiryCustomersTa.GetDataByEnquiryId(thisEnquiryId)

                    Dim customerTa As EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter
                    customerTa = New EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter
                    Dim customerDt As EnquiryWorkSheetDataSet.CustomerDataTable

                    If enquiryCustomersDt.Rows.Count > 0 Then
                        'iterate through EnquiryCustomers to load a customer
                        For Each row As DataRow In enquiryCustomersDt.Rows
                            custId = row.Item("CustomerId")
                            custName = row.Item("Name")
                            'load customer
                            customerDt = customerTa.GetDataByCustomerId(custId)
                            Dim custRow As EnquiryWorkSheetDataSet.CustomerRow
                            custRow = customerDt.Rows(0)
                            custType = custRow.CustomerType
                            Dim checkCustomerMessage As String = ""
                            Select Case custType
                                Case MyEnums.CustomerType.Individual, MyEnums.CustomerType.SoleTrader
                                    Try
                                        'Customer
                                        If custRow.IsAcceptanceMethodNull Then
                                            checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
                                        ElseIf custRow.AcceptanceMethod < 0 Then
                                            checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
                                        End If
                                        'Check Identity Assessment
                                        If custRow.IsIdTypeNull Or custRow.IdType = -1 Then
                                            checkCustomerMessage = checkCustomerMessage & "An ID type must be selected!" & vbCrLf
                                        ElseIf custRow.IdType = MyEnums.IdType.NZDriversLicence Then
                                            If custRow.IsDriverLicenceTypeNull Or custRow.DriverLicenceType = -1 Then
                                                checkCustomerMessage = checkCustomerMessage & "An NZ Driver's Licence type must be selected!" & vbCrLf
                                            End If
                                        End If
                                        If custRow.IsIDCurrentNull() Or custRow.IsIDPersonNull() Or custRow.IsResStatusAcceptNull() Or custRow.IsIDLegitimateNull() Or custRow.IsAMLRiskNull() Or custRow.IsIsaPEPNull() Then
                                            checkCustomerMessage = checkCustomerMessage & "A Identity Assessment must be added!" & vbCrLf
                                        ElseIf Not custRow.IDPerson.Length > 0 Or Not custRow.ResStatusAccept.Length > 0 Or Not custRow.IDLegitimate.Length > 0 Or custRow.AMLRisk = 0 Then
                                            checkCustomerMessage = checkCustomerMessage & "A Identity Assessment must be added!" & vbCrLf
                                        ElseIf custRow.IDCurrent = False Then
                                            checkCustomerMessage = checkCustomerMessage & "Client's ID documents must be current!" & vbCrLf
                                        End If
                                        'check tenancy questions
                                        If custRow.TenancyLength Is DBNull.Value Or custRow.TenancySatisfactory Is DBNull.Value Or custRow.TenancyType Is DBNull.Value Or custRow.TenancyEnvironment Is DBNull.Value Or custRow.TenancyEstablish Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage & "Some Tenancy Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.TenancyLength.Length > 0 Or Not custRow.TenancySatisfactory.Length > 0 Or Not custRow.TenancyType.Length > 0 Or Not custRow.TenancyEnvironment.Length > 0 Or Not custRow.TenancyEstablish.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage & "Some Tenancy Questions still need to be answered!" & vbCrLf
                                        End If
                                        If custType = MyEnums.CustomerType.Individual Then
                                            'if self employed or if beneficiary or if EmployerNotRung
                                            If custRow.SelfEmployed = True Or custRow.Beneficiary = True Or custRow.EmployerNotRung = True Then
                                                If custRow.StabilityComments Is DBNull.Value Then
                                                    checkCustomerMessage = checkCustomerMessage & "Need to fill in Stability Comments" & vbCrLf
                                                ElseIf Not custRow.StabilityComments.Length > 0 Then
                                                    checkCustomerMessage = checkCustomerMessage & "Need to fill in Stability Comments" & vbCrLf
                                                End If
                                            Else
                                                Try
                                                    If IsDBNull(custRow.EmployerRangDate) Then
                                                        checkCustomerMessage = checkCustomerMessage & "The EmployerRangDate needs to be answered!" & vbCrLf
                                                    ElseIf Not custRow.EmployerRangDate > DateTime.MinValue Then
                                                        checkCustomerMessage = checkCustomerMessage & "The EmployerRangDate needs to be answered!" & vbCrLf
                                                    End If
                                                Catch ex As Exception
                                                    log.Error("ValidateDueDiligence: If self employed or if beneficiary or if EmployerNotRung: Line 560: " & ex.Message & " : " & ex.TargetSite.ToString)
                                                    checkCustomerMessage = checkCustomerMessage & "The EmployerRangDate needs to be answered!" & vbCrLf
                                                End Try
                                                'check stability questions 'EmployerRangDate
                                                If custRow.EmployerName Is DBNull.Value Or custRow.EmployerSpoketoName Is DBNull.Value Or custRow.StabilityQ1 Is DBNull.Value Or custRow.StabilityQ2 Is DBNull.Value Or custRow.StabilityQ3 Is DBNull.Value Or custRow.StabilityQ4 Is DBNull.Value Or custRow.StabilityQ5 Is DBNull.Value Or custRow.StabilityQ6 Is DBNull.Value Or custRow.StabilityQ7 Is DBNull.Value Or custRow.StabilityQ8 Is DBNull.Value Or custRow.StabilityQ9 Is DBNull.Value Then
                                                    checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
                                                ElseIf Not custRow.EmployerName.Length > 0 Or Not custRow.EmployerSpoketoName.Length > 0 Or Not custRow.StabilityQ1.Length > 0 Or Not custRow.StabilityQ2.Length > 0 Or Not custRow.StabilityQ3.Length > 0 Or Not custRow.StabilityQ4.Length > 0 Or Not custRow.StabilityQ5.Length > 0 Or Not custRow.StabilityQ6.Length > 0 Or Not custRow.StabilityQ7.Length > 0 Or Not custRow.StabilityQ8.Length > 0 Or Not custRow.StabilityQ9.Length > 0 Then
                                                    checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
                                                End If
                                            End If
                                        ElseIf custType = MyEnums.CustomerType.SoleTrader Then
                                            'check stability questions
                                            If custRow.StabilityQ1 Is DBNull.Value Or custRow.StabilityQ2 Is DBNull.Value Or custRow.StabilityQ3 Is DBNull.Value Or custRow.StabilityQ4 Is DBNull.Value Or custRow.StabilityQ6 Is DBNull.Value Or custRow.StabilityQ7 Is DBNull.Value Or custRow.StabilityQ9 Is DBNull.Value Then
                                                checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
                                            ElseIf Not custRow.StabilityQ1.Length > 0 Or Not custRow.StabilityQ2.Length > 0 Or Not custRow.StabilityQ3.Length > 0 Or Not custRow.StabilityQ4.Length > 0 Or Not custRow.StabilityQ6.Length > 0 Or Not custRow.StabilityQ7.Length > 0 Or Not custRow.StabilityQ9.Length > 0 Then
                                                checkCustomerMessage = checkCustomerMessage & "Some Stability Questions still need to be answered!" & vbCrLf
                                            End If
                                        End If
                                        'Credit
                                        If custRow.CreditComments Is DBNull.Value Or custRow.CreditExplanation Is DBNull.Value Or custRow.CreditOther Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Credit Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.CreditComments.Length > 0 Or Not custRow.CreditExplanation.Length > 0 Or Not custRow.CreditOther.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Credit Questions still need to be answered!" & vbCrLf
                                        End If
                                    Catch ex As Exception
                                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                                    End Try


                                Case MyEnums.CustomerType.Company
                                    Try
                                        'Customer
                                        If custRow.IsAcceptanceMethodNull Then
                                            checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
                                        ElseIf custRow.AcceptanceMethod < 0 Then
                                            checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
                                        End If
                                        'Business information
                                        If custRow.BusinessRegistered = False Or custRow.BusinessReturnsFiled = False Or custRow.BusinessPaidUpCapital = False Or custRow.BusinessDirectors = False Or custRow.BusinessShareholders = False Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be confirmed!" & vbCrLf
                                        End If
                                        If custRow.BusinessDirectorNotes Is DBNull.Value Or custRow.BusinessShareholderNotes Is DBNull.Value Or custRow.BusinessHistoryNotes Is DBNull.Value Or custRow.BusinessAddressNotes Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.BusinessDirectorNotes.Length > 0 Or Not custRow.BusinessShareholderNotes.Length > 0 Or Not custRow.BusinessHistoryNotes.Length > 0 Or Not custRow.BusinessAddressNotes.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
                                        End If
                                        'Tenancy
                                        If custRow.IsTenancyTypeNull Or custRow.TenancyLength Is DBNull.Value Or custRow.TenancySatisfactory Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.TenancyType.Length > 0 Or Not custRow.TenancyLength.Length > 0 Or Not custRow.TenancySatisfactory.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
                                        End If
                                        'Credit
                                        If custRow.CreditComments Is DBNull.Value Or custRow.CreditExplanation Is DBNull.Value Or custRow.CreditOther Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.CreditComments.Length > 0 Or Not custRow.CreditExplanation.Length > 0 Or Not custRow.CreditOther.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
                                        End If
                                    Catch ex As Exception
                                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                                    End Try

                                    'Shareholders
                                    Dim DtsbTa As EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                    DtsbTa = New EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                    Dim DtsbDt As EnquiryWorkSheetDataSet.DTSBDataTable
                                    DtsbDt = DtsbTa.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.ShareHolder))
                                    For Each dtsbRow As DataRow In DtsbDt.Rows
                                        If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "A shareholder is missing a name!" & vbCrLf
                                        ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "A shareholder is missing a name!" & vbCrLf
                                        End If
                                        If dtsbRow.Item("IdVerified") = False Or dtsbRow.Item("AddressVerified") = False Then
                                            checkCustomerMessage = checkCustomerMessage + "A shareholder's Id or Address has not been verified!" & vbCrLf
                                        End If
                                        Dim dob As Date = dtsbRow.Item("DateOfBirth")
                                        If dob.AddYears(18) > Now.Date Then
                                            checkCustomerMessage = checkCustomerMessage + "A shareholder's Date of Birth is not valid!" & vbCrLf
                                        End If
                                        If dtsbRow.Item("AMLRisk") = MyEnums.AMLRisk.SelectOne Then
                                            checkCustomerMessage = checkCustomerMessage + "A shareholders's AML/CFT Risk needs to be assessed!" & vbCrLf
                                        End If
                                    Next dtsbRow
                                    'Directors
                                    Dim Dtsb01Ta As EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                    Dtsb01Ta = New EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                    Dim Dtsb01Dt As EnquiryWorkSheetDataSet.DTSB01DataTable
                                    Dtsb01Dt = Dtsb01Ta.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Director))
                                    For Each dtsbRow As DataRow In Dtsb01Dt.Rows
                                        If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "A director is missing a name!" & vbCrLf
                                        ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "A director is missing a name!" & vbCrLf
                                        End If
                                        If dtsbRow.Item("IdVerified") = 0 Or dtsbRow.Item("AddressVerified") = 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "A director's Id or Address has not been verified!" & vbCrLf
                                        End If
                                        Dim dob As Date = dtsbRow.Item("DateOfBirth")
                                        If dob.AddYears(18) > Now.Date Then
                                            checkCustomerMessage = checkCustomerMessage + "A director's Date of Birth is not valid!" & vbCrLf
                                        End If
                                        If dtsbRow.Item("AMLRisk") = MyEnums.AMLRisk.SelectOne Then
                                            checkCustomerMessage = checkCustomerMessage + "A director's AML/CFT Risk needs to be assessed!" & vbCrLf
                                        End If
                                    Next dtsbRow
                                    'Trade References
                                    Dim TradeReferencesTa As EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                    TradeReferencesTa = New EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                    Dim TradeReferencesDt As EnquiryWorkSheetDataSet.TradeReferencesDataTable
                                    TradeReferencesDt = TradeReferencesTa.GetDataByCustomerId(New System.Nullable(Of Integer)(custId))
                                    For Each trRow As DataRow In TradeReferencesDt.Rows
                                        If trRow.Item("BusinessName") Is DBNull.Value Or trRow.Item("Account") Is DBNull.Value Or trRow.Item("Spend") Is DBNull.Value Or trRow.Item("Pay") Is DBNull.Value Or trRow.Item("Comments") Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
                                        ElseIf Not trRow.Item("BusinessName").length > 0 Or Not trRow.Item("Account").length > 0 Or Not trRow.Item("Spend").length > 0 Or Not trRow.Item("Pay").length > 0 Or Not trRow.Item("Comments").length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
                                        End If
                                    Next trRow

                                Case MyEnums.CustomerType.Trust
                                    Try
                                        'Customer
                                        If custRow.IsAcceptanceMethodNull Then
                                            checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
                                        ElseIf custRow.AcceptanceMethod < 0 Then
                                            checkCustomerMessage = checkCustomerMessage & "An acceptance method must be chosen!" & vbCrLf
                                        End If
                                        'Business information
                                        If custRow.BusinessReturnsFiled = False Or custRow.BusinessPaidUpCapital = False Or custRow.BusinessDirectors = False Or custRow.BusinessShareholders = False Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be confirmed!" & vbCrLf
                                        End If
                                        If custRow.BusinessDirectorNotes Is DBNull.Value Or custRow.BusinessShareholderNotes Is DBNull.Value Or custRow.BusinessHistoryNotes Is DBNull.Value Or custRow.BusinessAddressNotes Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.BusinessDirectorNotes.Length > 0 Or Not custRow.BusinessShareholderNotes.Length > 0 Or Not custRow.BusinessHistoryNotes.Length > 0 Or Not custRow.BusinessAddressNotes.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Information Questions still need to be answered!" & vbCrLf
                                        End If
                                        'Tenancy
                                        If custRow.IsTenancyTypeNull Or custRow.TenancyLength Is DBNull.Value Or custRow.TenancySatisfactory Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.TenancyType.Length > 0 Or Not custRow.TenancyLength.Length > 0 Or Not custRow.TenancySatisfactory.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Tenancy Questions still need to be answered!" & vbCrLf
                                        End If
                                        'Credit
                                        If custRow.CreditComments Is DBNull.Value Or custRow.CreditExplanation Is DBNull.Value Or custRow.CreditOther Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
                                        ElseIf Not custRow.CreditComments.Length > 0 Or Not custRow.CreditExplanation.Length > 0 Or Not custRow.CreditOther.Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Business Credit Questions still need to be answered!" & vbCrLf
                                        End If
                                    Catch ex As Exception
                                        log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
                                    End Try

                                    'Beneficaries
                                    Dim DtsbTa As EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                    DtsbTa = New EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                    Dim DtsbDt As EnquiryWorkSheetDataSet.DTSBDataTable
                                    DtsbDt = DtsbTa.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Beneficiary))
                                    For Each dtsbRow As DataRow In DtsbDt.Rows
                                        If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "A beneficary is missing a name!" & vbCrLf
                                        ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "A beneficary is missing a name!" & vbCrLf
                                        End If
                                        If dtsbRow.Item("IdVerified") = False Or dtsbRow.Item("AddressVerified") = False Then
                                            checkCustomerMessage = checkCustomerMessage + "A beneficary's Id or Address has not been verified!" & vbCrLf
                                        End If
                                        Dim dob As Date = dtsbRow.Item("DateOfBirth")
                                        If dob.AddYears(18) > Now.Date Then
                                            checkCustomerMessage = checkCustomerMessage + "A beneficary's Date of Birth is not valid!" & vbCrLf
                                        End If
                                    Next dtsbRow
                                    'Trustees
                                    Dim Dtsb01Ta As EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                    Dtsb01Ta = New EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                    Dim Dtsb01Dt As EnquiryWorkSheetDataSet.DTSB01DataTable
                                    Dtsb01Dt = Dtsb01Ta.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Trustee))
                                    For Each dtsbRow As DataRow In Dtsb01Dt.Rows
                                        If dtsbRow.Item("FirstName") Is DBNull.Value Or dtsbRow.Item("LastName") Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "A trustee is missing a name!" & vbCrLf
                                        ElseIf Not dtsbRow.Item("FirstName").Length > 0 Or Not dtsbRow.Item("LastName").Length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "A trustee is missing a name!" & vbCrLf
                                        End If
                                        If dtsbRow.Item("IdVerified") = False Or dtsbRow.Item("AddressVerified") = False Then
                                            checkCustomerMessage = checkCustomerMessage + "A trustee's Id or Address has not been verified!" & vbCrLf
                                        End If
                                        Dim dob As Date = dtsbRow.Item("DateOfBirth")
                                        If dob.AddYears(18) > Now.Date Then
                                            checkCustomerMessage = checkCustomerMessage + "A trustee's Date of Birth is not valid!" & vbCrLf
                                        End If
                                    Next dtsbRow
                                    'Trade References
                                    Dim TradeReferencesTa As EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                    TradeReferencesTa = New EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                    Dim TradeReferencesDt As EnquiryWorkSheetDataSet.TradeReferencesDataTable
                                    TradeReferencesDt = TradeReferencesTa.GetDataByCustomerId(New System.Nullable(Of Integer)(custId))
                                    For Each trRow As DataRow In TradeReferencesDt.Rows
                                        If trRow.Item("BusinessName") Is DBNull.Value Or trRow.Item("Account") Is DBNull.Value Or trRow.Item("Spend") Is DBNull.Value Or trRow.Item("Pay") Is DBNull.Value Or trRow.Item("Comments") Is DBNull.Value Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
                                        ElseIf Not trRow.Item("BusinessName").length > 0 Or Not trRow.Item("Account").length > 0 Or Not trRow.Item("Spend").length > 0 Or Not trRow.Item("Pay").length > 0 Or Not trRow.Item("Comments").length > 0 Then
                                            checkCustomerMessage = checkCustomerMessage + "Some Trade Reference information is missing!" & vbCrLf
                                        End If
                                    Next trRow

                                Case Else
                                    checkCustomerMessage = checkCustomerMessage + "Could not validate customers!" & vbCrLf

                            End Select

                            If Not checkCustomerMessage = "" Then
                                checkFieldsMessage = checkFieldsMessage + "--Error validating " & custName & vbNewLine & checkCustomerMessage
                            End If

                        Next row
                    Else
                        checkFieldsMessage = checkFieldsMessage + "A customer needs to be created!" & vbCrLf
                    End If

                    '#######################
                    'Display warnings
                    '#######################
                    If Not checkWarningMessage = "" Then
                        Dim result As MsgBoxResult
                        checkWarningMessage = checkWarningMessage & vbCrLf & "Do you wish to continue?" & vbCrLf
                        result = MsgBox(checkWarningMessage, MsgBoxStyle.OkCancel, "Warning message")
                        If result = MsgBoxResult.Cancel Then
                            resultOut.StringOut = ""
                            resultOut.Result = False
                            Return resultOut
                        End If

                    End If
                    '#######################
                    'checkFieldsMessages
                    '#######################
                    If Not checkFieldsMessage = "" Then
                        resultOut.StringOut = checkFieldsMessage
                        resultOut.Result = False
                    Else
                        resultOut.StringOut = ""
                        resultOut.Result = True
                    End If

                Else 'docsReceived False
                    'MessageBox.Show("Documents have not been received.", "Loan documents", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    resultOut.StringOut = "Documents have not been received."
                    resultOut.Result = False
                End If 'docsReceived

            End If 'Enquiry rows

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Validate Due-diligence Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try



        Return resultOut

    End Function

    ''' <summary>
    ''' Produces string of all the enquiry
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <param name="printPrintedBy"></param>
    ''' <returns>Util.StringOut</returns>
    ''' <remarks></remarks>
    Public Shared Function GetPrintStringAll(ByVal enquiryId As Integer, Optional ByVal printPrintedBy As Boolean = True) As Util.StringOut
        Dim ourPrintString As Util.StringOut
        ourPrintString = New Util.StringOut
        ourPrintString.Result = False
        'Loan
        Dim thisEnquiryCode As String
        Dim thisEnquiryType As Integer
        Dim thisTypeOfMainCustomer As Integer ' set Type of Main Client
        Dim thisLoanAmount As Decimal = 0
        Dim thisCurrentLoanAmt As Decimal = 0
        Dim thisCurrentStatus As String = String.Empty
        'Securities
        Dim totalAssetValue As Decimal = 0
        Dim totalLending As Decimal = 0
        Dim equityValue As Decimal = 0
        'Budget
        Dim incomeTotal As Decimal = 0 'set Total Income
        Dim expenseTotal As Decimal = 0 'set Total Expenses
        Dim budgetDependTotal As Decimal = 0 'set Budget Dependency
        Dim budgetMargin As Decimal = 0
        'Customer
        Dim custCount As Integer = 0

        Try
            Dim constructedString As String = ""
            Dim auditPayBookSentSwitch As Boolean = False
            'Get Row in Enquiry dataset
            Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
            enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
            Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
            enquiryDt = enquiryTa.GetDataByEnquiryId(enquiryId)
            Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
            If enquiryDt.Rows.Count > 0 Then
                enquiryRow = enquiryDt.Rows(0)
                'setup enquiry wide variables
                thisEnquiryType = enquiryRow.EnquiryType
                If Not IsDBNull(enquiryRow.Item("mainCustomerType")) Then
                    thisTypeOfMainCustomer = enquiryRow.Item("mainCustomerType")
                End If
                thisCurrentStatus = enquiryRow.Item("CurrentStatus")
                thisEnquiryCode = enquiryRow.Item("EnquiryCode")
                'Construct header using main string
                constructedString = vbCrLf & "Enquiry number: " & enquiryRow.Item("EnquiryCode") & _
                "                            Application number: " & enquiryRow.Item("ApplicationCode") & vbCrLf & enquiryRow.Item("ContactDisplayName") & vbCrLf & vbCrLf & _
                "Date: " & CStr(enquiryRow.Item("DateTime")) & "                    Current Status: " & enquiryRow.Item("CurrentStatus") & vbCrLf
                constructedString = constructedString & "Main client Type: " & MyEnums.GetDescription(DirectCast(thisTypeOfMainCustomer, MyEnums.mainCustomerType)) & vbCrLf
                constructedString = constructedString & "Reason for Loan: " & MyEnums.GetDescription(DirectCast(thisEnquiryType, MyEnums.EnquiryType))
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    constructedString = constructedString & "                       Amount of Loan: " & CStr(Format(enquiryRow.Item("LoanValue"), "C")) & vbCrLf
                    If thisEnquiryType = MyEnums.EnquiryType.PersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.PersonalLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.BusinessLoanRefinance Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanVariation Or thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalLoanRefinance Then
                        constructedString = constructedString & "Current Loan value: " & CStr(Format(enquiryRow.Item("CurrentLoanAmt"), "C")) & vbCrLf
                    End If
                    If Not IsDBNull(enquiryRow.Item("LoanPurpose")) Then
                        constructedString = constructedString & "Loan purpose: " & enquiryRow.Item("LoanPurpose") & vbCrLf
                    End If
                Else
                    constructedString = constructedString & vbCrLf
                End If
                constructedString = constructedString & "Contact name: " & enquiryRow.Item("ContactTitle") & " " & enquiryRow.Item("ContactFirstName") & " " & enquiryRow.Item("ContactLastName") & vbCrLf
                constructedString = constructedString & "Suburb: " & enquiryRow.Item("ContactSuburb") & "          City: " & enquiryRow.Item("ContactCity") & vbCrLf
                constructedString = constructedString & "Contact number: " & enquiryRow.Item("ContactPhone") & vbCrLf
                constructedString = constructedString & "Contact email: " & enquiryRow.Item("ContactEmail") & vbCrLf

                '********** Loan Comments *************************

                constructedString = constructedString & vbCrLf & "*****  LOAN COMMENTS: Progress and Status  *************************" & vbCrLf & Util.GetEnquiryComments(enquiryId) & vbCrLf

                '********** End of Loan Comments ******************
                '********** Security ******************************
                'If ThisLoanReason = Split Loan Drawdown or FinanceFacility, then NO Security info exits
                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    Dim securityString As String
                    securityString = vbCrLf & "*****  SECURITY  ************************************************************" & _
                    vbCrLf & "Insurance and Security Comments: " & vbCrLf & enquiryRow.Item("SecurityComments") & "Security Items: " & vbCrLf
                    'get Security Items      
                    Dim securitySubString As String
                    'Get Row in Security datatable
                    Dim securityTa As EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
                    securityTa = New EnquiryWorkSheetDataSetTableAdapters.SecurityTableAdapter
                    Dim securityDt As EnquiryWorkSheetDataSet.SecurityDataTable
                    securityDt = securityTa.GetDataByEnquiryId(enquiryId)
                    Dim securityItemRow As EnquiryWorkSheetDataSet.SecurityRow
                    For Each securityItemRow In securityDt
                        'contruct string
                        'totals
                        totalAssetValue = totalAssetValue + CDec(If(IsDBNull(securityItemRow.Item("TodayValue")), 0, securityItemRow.Item("TodayValue")))
                        totalLending = totalLending + CDec(If(IsDBNull(securityItemRow.Item("LendingValue")), 0, securityItemRow.Item("LendingValue")))
                        securitySubString = "Description: " & securityItemRow.Item("Description") & vbCrLf & "Type: " & securityItemRow.Item("Type") & _
                        "     Valuer: " & securityItemRow.Item("TodayValuer") & "    Value: " & CStr(Format(securityItemRow.Item("TodayValue"), "C")) & vbCrLf & _
                        "Lending: " & securityItemRow.Item("LendingPercentage") & "%                  Lending Value: " & CStr(Format(securityItemRow.Item("LendingValue"), "C")) & _
                        vbCrLf & "Valuation Approved? "
                        If securityItemRow.Item("SecurityValued") = False Then
                            securitySubString = securitySubString & "No" & vbCrLf
                        Else
                            securitySubString = securitySubString & "Yes" & vbCrLf
                        End If
                        If Not IsDBNull(securityItemRow.Item("Comments")) Then
                            If securityItemRow.Item("Comments").length > 0 Then
                                'If Not String.IsNullOrEmpty(SecurityItemRow.Item("Comments")) Then
                                'If SecurityItemRow.Item("Comments") <> "" Then
                                securitySubString = securitySubString & securityItemRow.Item("Comments") & vbCrLf
                            End If
                        End If
                        securitySubString = securitySubString & "Security Insured? "
                        If securityItemRow.Item("SecurityInsured") = False Then
                            securitySubString = securitySubString & "No" & vbCrLf
                        Else
                            securitySubString = securitySubString & "Yes" & vbCrLf
                        End If
                        If Not IsDBNull(securityItemRow.Item("InsuranceComments")) Then
                            If securityItemRow.Item("InsuranceComments").length > 0 Then
                                'If Not String.IsNullOrEmpty(SecurityItemRow.Item("InsuranceComments")) Then
                                'If SecurityItemRow.Item("InsuranceComments") <> "" Then
                                securitySubString = securitySubString & securityItemRow.Item("InsuranceComments") & vbCrLf
                            End If
                        End If
                        'Add to SecurityString
                        securityString = securityString & securitySubString & vbCrLf
                    Next
                    thisLoanAmount = CDec(If(IsDBNull(enquiryRow.Item("LoanValue")), 0, enquiryRow.Item("LoanValue")))
                    thisCurrentLoanAmt = CDec(If(IsDBNull(enquiryRow.Item("CurrentLoanAmt")), 0, enquiryRow.Item("CurrentLoanAmt")))
                    equityValue = totalLending - thisLoanAmount - thisCurrentLoanAmt
                    securityString = securityString & "Total Assest Value:      " & CStr(Format(totalAssetValue, "C")) & vbCrLf
                    securityString = securityString & "Total Lending Available: " & CStr(Format(totalLending, "C")) & "                    Lending Equity: " & CStr(Format(equityValue, "C")) & vbCrLf
                    'add security string to main string
                    If securityString.Length > 0 Then
                        constructedString = constructedString & securityString
                    End If
                End If

                '********** End of Security **************
                '**********Budget ********************************
                'If ThisLoanReason = Split Loan Drawdown or FinanceFacility, then NO Budget info exits
                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    Dim budgetString As String = String.Empty
                    Select Case thisTypeOfMainCustomer
                        Case MyEnums.mainCustomerType.Company, MyEnums.mainCustomerType.Trust, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
                            budgetString = vbCrLf & "*****  FINANCIALS **************************************************" & vbCrLf
                            'Financials
                            'Income and Expenses
                            Dim incomeExpensesString As String = "--------------------  Income and Expenses:" & vbCrLf
                            incomeExpensesString = "Income was established by previous years financials? "
                            If enquiryRow.Item("CompanyIncomePrevious") = True Then
                                incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
                            Else
                                incomeExpensesString = incomeExpensesString & "No" & vbCrLf
                            End If
                            incomeExpensesString = incomeExpensesString & "Income was established by year to date financials? "
                            If enquiryRow.Item("CompanyIncomeYTD") = True Then
                                incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
                            Else
                                incomeExpensesString = incomeExpensesString & "No" & vbCrLf
                            End If
                            incomeExpensesString = incomeExpensesString & "Income was established by GST returns? "
                            If enquiryRow.Item("CompanyIncomeGST") = True Then
                                incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
                            Else
                                incomeExpensesString = incomeExpensesString & "No" & vbCrLf
                            End If
                            incomeExpensesString = incomeExpensesString & "Income was established by income forcast? "
                            If enquiryRow.Item("CompanyIncomeForcast") = True Then
                                incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
                            Else
                                incomeExpensesString = incomeExpensesString & "No" & vbCrLf
                            End If
                            If Not IsDBNull(enquiryRow.Item("CompanyIncomeNotes")) Then
                                If enquiryRow.Item("CompanyIncomeNotes").length > 0 Then
                                    incomeExpensesString = incomeExpensesString & "Past, present and future income is satisfactory because " & enquiryRow.Item("CompanyIncomeNotes") & vbCrLf
                                End If
                            End If
                            If Not IsDBNull(enquiryRow.Item("CompanyExpensesNotes")) Then
                                If enquiryRow.Item("CompanyExpensesNotes").length > 0 Then
                                    incomeExpensesString = incomeExpensesString & "Past, present and future expenses are satisfactory because " & enquiryRow.Item("CompanyExpensesNotes") & vbCrLf
                                End If
                            End If
                            incomeExpensesString = incomeExpensesString & "Income and expenses are satisfactory? "
                            If enquiryRow.Item("CompanyIncomeExpensesSatis") = True Then
                                incomeExpensesString = incomeExpensesString & "Yes" & vbCrLf
                            Else
                                incomeExpensesString = incomeExpensesString & "No" & vbCrLf
                            End If
                            budgetString = budgetString & incomeExpensesString
                            'Salaries (Partnership and SoleTrader donot have salaries section)
                            If Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership And Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
                                Dim salariesString As String = "--------------------  Salaries:" & vbCrLf
                                If Not IsDBNull(enquiryRow.Item("CompanySalariesNotes")) Then
                                    If enquiryRow.Item("CompanySalariesNotes").length > 0 Then
                                        Select Case thisTypeOfMainCustomer
                                            Case MyEnums.mainCustomerType.Company
                                                salariesString = salariesString & "Salaries to directors, shareholders and owners are satisfactory because " & enquiryRow.Item("CompanySalariesNotes") & vbCrLf
                                            Case MyEnums.mainCustomerType.Trust
                                                salariesString = salariesString & "Salaries to trustees, beneficiaries are satisfactory because " & enquiryRow.Item("CompanySalariesNotes") & vbCrLf
                                                'Case MyEnums.mainCustomerType.Partnership
                                                '    salariesString = salariesString & "Salaries to partners are satisfactory because " & enquiryRow.Item("CompanySalariesNotes") & vbCrLf
                                        End Select
                                    End If
                                End If
                                salariesString = salariesString & "Salaries are satisfactory? "
                                If enquiryRow.Item("CompanySalariesSatis") = True Then
                                    salariesString = salariesString & "Yes" & vbCrLf
                                Else
                                    salariesString = salariesString & "No" & vbCrLf
                                End If
                                budgetString = budgetString & salariesString
                            End If
                            'Earnings before interest and taxes
                            Dim earningsString As String = "--------------------  Earnings before interest and taxes:" & vbCrLf
                            If Not IsDBNull(enquiryRow.Item("CompanyEarningsNotes")) Then
                                If enquiryRow.Item("CompanyEarningsNotes").length > 0 Then
                                    Select Case thisTypeOfMainCustomer
                                        Case MyEnums.mainCustomerType.Company
                                            earningsString = earningsString & "The company's performamce and ability to service YFL loan is satisfactory because " & enquiryRow.Item("CompanyEarningsNotes") & vbCrLf
                                        Case MyEnums.mainCustomerType.Trust
                                            earningsString = earningsString & "The trust's performamce and ability to service YFL loan is satisfactory because " & enquiryRow.Item("CompanyEarningsNotes") & vbCrLf
                                        Case MyEnums.mainCustomerType.Partnership
                                            earningsString = earningsString & "The partnerships's performamce and ability to service YFL loan is satisfactory because " & enquiryRow.Item("CompanyEarningsNotes") & vbCrLf
                                    End Select

                                End If
                            End If
                            earningsString = earningsString & "Profit is satisfactory? "
                            If enquiryRow.Item("CompanyProfitSatis") = True Then
                                earningsString = earningsString & "Yes" & vbCrLf
                            Else
                                earningsString = earningsString & "No" & vbCrLf
                            End If
                            budgetString = budgetString & earningsString
                            'Financial Position
                            Dim positionString As String = "--------------------  Financial position:" & vbCrLf
                            If Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership And Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
                                positionString = positionString & "Asset to liabilities ratio is satisfactory? "
                                If enquiryRow.Item("CompanyAssetRatio") = True Then
                                    positionString = positionString & "Yes" & vbCrLf
                                Else
                                    positionString = positionString & "No" & vbCrLf
                                End If
                                If Not IsDBNull(enquiryRow.Item("CompanyDebtNotes")) Then
                                    If enquiryRow.Item("CompanyDebtNotes").length > 0 Then
                                        positionString = positionString & "The debt and asset to liabilities ratio is satisfactory because " & enquiryRow.Item("CompanyDebtNotes") & vbCrLf
                                    End If
                                End If
                            End If
                            positionString = positionString & "Accounts payable are satisfactory and identified? "
                            If enquiryRow.Item("CompanyAcctsPaySatis") = True Then
                                positionString = positionString & "Yes" & vbCrLf
                            Else
                                positionString = positionString & "No" & vbCrLf
                            End If
                            If Not IsDBNull(enquiryRow.Item("CompanyAcctsPayNotes")) Then
                                If enquiryRow.Item("CompanyAcctsPayNotes").length > 0 Then
                                    positionString = positionString & "The accounts payable are satisfactory because " & enquiryRow.Item("CompanyAcctsPayNotes") & vbCrLf
                                End If
                            End If
                            positionString = positionString & "GST returns and payments are up to date? "
                            If enquiryRow.Item("CompanyGSTUTD") = True Then
                                positionString = positionString & "Yes" & vbCrLf
                            Else
                                positionString = positionString & "No" & vbCrLf
                            End If
                            If Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership And Not thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
                                positionString = positionString & "PAYE returns and payments are up to date? "
                                If enquiryRow.Item("CompanyPayeUTD") = True Then
                                    positionString = positionString & "Yes" & vbCrLf
                                Else
                                    positionString = positionString & "No" & vbCrLf
                                End If
                            End If
                            positionString = positionString & "Income Tax returns and payments are up to date? "
                            If enquiryRow.Item("CompanyITUTD") = True Then
                                positionString = positionString & "Yes" & vbCrLf
                            Else
                                positionString = positionString & "No" & vbCrLf
                            End If
                            If Not IsDBNull(enquiryRow.Item("CompanyTaxesNotes")) Then
                                If enquiryRow.Item("CompanyTaxesNotes").length > 0 Then
                                    positionString = positionString & "The taxes are satisfactory because " & enquiryRow.Item("CompanyTaxesNotes") & vbCrLf
                                End If
                            End If
                            budgetString = budgetString & positionString
                            'end of Company Financials

                        Case MyEnums.mainCustomerType.Individual, MyEnums.mainCustomerType.Partnership, MyEnums.mainCustomerType.SoleTrader
                            budgetString = vbCrLf & "*****  BUDGET (per month)  *************************************************" & vbCrLf
                            If thisTypeOfMainCustomer = MyEnums.mainCustomerType.Partnership Then
                                budgetString = "*****  Personal Incomes are to be combined to see if the Budget is sufficent for the partners to live on." & vbCrLf
                            End If
                            If thisTypeOfMainCustomer = MyEnums.mainCustomerType.SoleTrader Then
                                budgetString = "*****  Personal Income is derived from the income of the Sole Trader's business activities." & vbCrLf
                            End If
                            budgetString = budgetString & "Stated Net Income: " & CStr(Format(enquiryRow.Item("NetIncomeStated"), "C")) & _
                           "                         Stated Expenses: " & CStr(Format(enquiryRow.Item("ExpensesStated"), "C")) & vbCrLf & vbCrLf
                            'add to total of income and expenses
                            incomeTotal = incomeTotal + If(IsDBNull(CDec(enquiryRow.Item("NetIncomeStated"))), 0, CDec(enquiryRow.Item("NetIncomeStated")))
                            expenseTotal = expenseTotal + If(IsDBNull(CDec(enquiryRow.Item("ExpensesStated"))), 0, CDec(enquiryRow.Item("ExpensesStated")))
                            'check questions
                            'contruct BudgetQuestionString
                            Dim budgetQuestionString As String
                            budgetQuestionString = "Is Partner's pay relevant? "
                            If enquiryRow.Item("BudgetQuestion1") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No                       "
                            ElseIf enquiryRow.Item("BudgetQuestion1") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes                      "
                            Else
                                budgetQuestionString = budgetQuestionString & "                         "
                            End If
                            budgetQuestionString = budgetQuestionString & "Any other income? "
                            If enquiryRow.Item("BudgetQuestion2") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No" & vbCrLf
                            ElseIf enquiryRow.Item("BudgetQuestion2") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
                            Else
                                budgetQuestionString = budgetQuestionString & vbCrLf
                            End If

                            budgetQuestionString = budgetQuestionString & "Are you paying child support? "
                            If enquiryRow.Item("BudgetQuestion3") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No               "
                            ElseIf enquiryRow.Item("BudgetQuestion3") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes              "
                            Else
                                budgetQuestionString = budgetQuestionString & "                 "
                            End If
                            budgetQuestionString = budgetQuestionString & "Are you paying any fines? "
                            If enquiryRow.Item("BudgetQuestion4") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No" & vbCrLf
                            ElseIf enquiryRow.Item("BudgetQuestion4") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
                            Else
                                budgetQuestionString = budgetQuestionString & vbCrLf
                            End If

                            budgetQuestionString = budgetQuestionString & "Are you paying any other loans? "
                            If enquiryRow.Item("BudgetQuestion5") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No              "
                            ElseIf enquiryRow.Item("BudgetQuestion5") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes             "
                            Else
                                budgetQuestionString = budgetQuestionString & "                "
                            End If
                            budgetQuestionString = budgetQuestionString & "Is you partner paying any loans? "
                            If enquiryRow.Item("BudgetQuestion6") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No" & vbCrLf
                            ElseIf enquiryRow.Item("BudgetQuestion6") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
                            Else
                                budgetQuestionString = budgetQuestionString & vbCrLf
                            End If

                            budgetQuestionString = budgetQuestionString & "Any hire purchase payments? "
                            If enquiryRow.Item("BudgetQuestion7") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No                 "
                            ElseIf enquiryRow.Item("BudgetQuestion7") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes                "
                            Else
                                budgetQuestionString = budgetQuestionString & "                   "
                            End If
                            budgetQuestionString = budgetQuestionString & "Any Credit Card payments? "
                            If enquiryRow.Item("BudgetQuestion8") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No" & vbCrLf
                            ElseIf enquiryRow.Item("BudgetQuestion8") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
                            Else
                                budgetQuestionString = budgetQuestionString & vbCrLf
                            End If

                            budgetQuestionString = budgetQuestionString & "Any insurance payments? "
                            If enquiryRow.Item("BudgetQuestion9") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No                        "
                            ElseIf enquiryRow.Item("BudgetQuestion9") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes                       "
                            Else
                                budgetQuestionString = budgetQuestionString & "                          "
                            End If
                            budgetQuestionString = budgetQuestionString & "Any other expenses? "
                            If enquiryRow.Item("BudgetQuestion10") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No" & vbCrLf
                            ElseIf enquiryRow.Item("BudgetQuestion10") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
                            Else
                                budgetQuestionString = budgetQuestionString & vbCrLf
                            End If

                            budgetQuestionString = budgetQuestionString & "Board/rent/mortgage? "
                            If enquiryRow.Item("BudgetQuestion11") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No                               "
                            ElseIf enquiryRow.Item("BudgetQuestion11") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes                              "
                            Else
                                budgetQuestionString = budgetQuestionString & "                                 "
                            End If
                            budgetQuestionString = budgetQuestionString & "This YFL Loan? "
                            If enquiryRow.Item("BudgetQuestion12") = 1 Then
                                budgetQuestionString = budgetQuestionString & "No" & vbCrLf
                            ElseIf enquiryRow.Item("BudgetQuestion12") = 2 Then
                                budgetQuestionString = budgetQuestionString & "Yes" & vbCrLf
                            Else
                                budgetQuestionString = budgetQuestionString & vbCrLf
                            End If
                            'Add to BudgetString
                            budgetString = budgetString & budgetQuestionString & vbCrLf
                            'get Budget items   
                            Dim budgetSubString As String = ""
                            'Get Row in Security datatable
                            Dim budgetTa As EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter
                            budgetTa = New EnquiryWorkSheetDataSetTableAdapters.BudgetTableAdapter
                            Dim budgetDt As EnquiryWorkSheetDataSet.BudgetDataTable
                            budgetDt = budgetTa.GetDataByEnquiryId(enquiryId)
                            Dim budgetItemRow As EnquiryWorkSheetDataSet.BudgetRow
                            For Each budgetItemRow In budgetDt
                                'contruct SubString
                                If Not IsDBNull(budgetItemRow.Item("Description")) Then
                                    If budgetItemRow.Item("Description").length > 0 Then
                                        budgetSubString = budgetSubString & "Description: " & budgetItemRow.Item("Description") & vbCrLf
                                    End If
                                End If
                                If budgetItemRow.Item("ExpenseValue") > 0 Then
                                    'add to total of expenses
                                    expenseTotal = expenseTotal + CDec(budgetItemRow.Item("ExpenseValue"))
                                    budgetSubString = budgetSubString & "Expense: " & CStr(Format(budgetItemRow.Item("ExpenseValue"), "C")) & vbCrLf
                                ElseIf budgetItemRow.Item("IncomeValue") > 0 Then
                                    'add to total of income
                                    incomeTotal = incomeTotal + CDec(budgetItemRow.Item("IncomeValue"))
                                    budgetSubString = budgetSubString & "Income: " & CStr(Format(budgetItemRow.Item("IncomeValue"), "C")) & vbCrLf
                                End If
                                If Not IsDBNull(budgetItemRow.Item("Comments")) Then
                                    If budgetItemRow.Item("Comments").length > 0 Then
                                        budgetSubString = budgetSubString & "Comments: " & budgetItemRow.Item("Comments") & vbCrLf
                                    End If
                                End If
                                budgetSubString = budgetSubString & vbCrLf
                            Next
                            'Add to BudgetString
                            budgetString = budgetString & budgetSubString & vbCrLf
                            budgetString = budgetString & "Total Income: " & CStr(Format(incomeTotal, "C")) & "                                   TotalExpenses: " & CStr(Format(expenseTotal, "C")) & vbCrLf
                            'Dependency
                            budgetDependTotal = CDec(If(IsDBNull(enquiryRow.BudgetDependencyTotal), 0, enquiryRow.BudgetDependencyTotal))
                            budgetMargin = incomeTotal - expenseTotal - budgetDependTotal
                            budgetString = budgetString & "Dependency: " & CStr(Format(enquiryRow.Item("BudgetDependencyClient"), "C")) & _
                            "          No: of Dependants: " & enquiryRow.Item("BudgetDependencyChildren") & "          Total Dependency: " & _
                            CStr(Format(enquiryRow.Item("BudgetDependencyTotal"), "C")) & vbCrLf & vbCrLf & "MARGIN = " & CStr(Format(budgetMargin, "C")) & vbCrLf
                            'Added BudgetExplanation 12/11/2012
                            If Not IsDBNull(enquiryRow.Item("BudgetExplanation")) Then
                                If enquiryRow.Item("BudgetExplanation").length > 0 Then
                                    budgetString = budgetString & "Budget Comments and Explanations: " & enquiryRow.Item("BudgetExplanation") & vbCrLf
                                End If
                            End If
                    End Select
                    If budgetString.Length > 0 Then
                        constructedString = constructedString & budgetString
                    End If

                Else
                    'ThisEnquiryType = Split Loan Drawdown   
                End If
                '******* End of Budget ***********
                '********** Customer Info ******************
                Dim clientString As String = String.Empty
                'Check customer
                Dim custType As Integer
                Dim custId As Integer
                Dim custEnquiryType As Integer
                Dim thisCustString As String
                'get EnquiryCustomers
                Dim enquiryCustTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
                enquiryCustTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
                Dim enquiryCustDt As EnquiryWorkSheetDataSet.EnquiryCustomersDataTable
                enquiryCustDt = enquiryCustTa.GetDataByEnquiryId(enquiryId)
                Dim enquiryCustRow As EnquiryWorkSheetDataSet.EnquiryCustomersRow
                custCount = enquiryCustDt.Count
                'customers
                Dim custTa As EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter
                custTa = New EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter
                Dim custDt As EnquiryWorkSheetDataSet.CustomerDataTable
                Dim custRow As EnquiryWorkSheetDataSet.CustomerRow
                If custCount > 0 Then
                    'iterate through EnquiryCustomers to load a customer
                    For Each enquiryCustRow In enquiryCustDt
                        'For Each row As DataRow In EnquiryWorkSheetDataSet.EnquiryCustomers.Rows
                        thisCustString = vbCrLf & "*****  CUSTOMER INFORMATION  *****************************************" & vbCrLf
                        custId = enquiryCustRow.Item("CustomerId")
                        custEnquiryType = enquiryCustRow.Item("Type")
                        'load customer
                        custDt = custTa.GetDataByCustomerId(custId)
                        custRow = custDt.Rows(0)
                        custType = custRow.Item("CustomerType")
                        Select Case custType
                            '==============================
                            'Individual
                            '==============================
                            Case MyEnums.CustomerType.Individual, MyEnums.CustomerType.SoleTrader
                                thisCustString = thisCustString & "Type: Individual  -  " & MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType)) & vbCrLf
                                thisCustString = thisCustString & "Customer name: " & If(IsDBNull(custRow.Item("FirstName")), "", custRow.Item("FirstName")) & " " & If(IsDBNull(custRow.Item("MiddleNames")), "", custRow.Item("MiddleNames")) & " " & If(IsDBNull(custRow.Item("LastName")), "", custRow.Item("LastName")) & vbCrLf
                                If Not IsDBNull(custRow.Item("ClientNum")) Then
                                    If custRow.ClientNum.Length > 0 Then
                                        thisCustString = thisCustString & "FinPower: Client No - Client_" & custRow.Item("ClientNum") & vbCrLf 'custRow.Item("ClientTypeId") & custRow.Item("ClientNum") & vbCrLf
                                    Else
                                        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                            If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                                                Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
                                            ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility And custEnquiryType = MyEnums.CustomerEnquiryType.Main Then
                                                Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
                                            Else
                                                thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                            End If
                                        Else
                                            thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                        End If
                                    End If
                                Else
                                    If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                        If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                                            Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
                                        ElseIf thisEnquiryType = MyEnums.EnquiryType.FinanceFacility And custEnquiryType = MyEnums.CustomerEnquiryType.Main Then
                                            Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
                                        Else
                                            thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                        End If
                                    Else
                                        thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                    End If
                                End If
                                'If EnquiryType = Split Loan Drawdown, then NO Stability info exits
                                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                                    '********** Identity Risk Assessment
                                    Dim identityString As String = "--------------------   Identity Risk Assessment" & vbCrLf
                                    If Not IsDBNull(custRow.Item("IdType")) Then
                                        If custRow.Item("IdType") > -1 Then
                                            'MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType))
                                            identityString = identityString & "The ID type is " & MyEnums.GetDescription(DirectCast(CInt(custRow.Item("IdType")), MyEnums.IdType)) & vbCrLf
                                            If custRow.Item("IdType") = MyEnums.IdType.NZDriversLicence Then
                                                If Not IsDBNull(custRow.Item("DriverLicenceType")) Then
                                                    If custRow.Item("DriverLicenceType") > -1 Then
                                                        identityString = identityString & "The Driver's Licence type is " & MyEnums.GetDescription(DirectCast(CInt(custRow.Item("DriverLicenceType")), MyEnums.DriverLicenceType)) & vbCrLf
                                                    Else
                                                        identityString = identityString & "The Driver's Licence type is unknown!" & vbCrLf
                                                    End If
                                                Else
                                                    identityString = identityString & "The Driver's Licence type is unknown!" & vbCrLf
                                                End If
                                            End If
                                        Else
                                            identityString = identityString & "The ID type is unknown!" & vbCrLf
                                        End If
                                    Else
                                        identityString = identityString & "The ID type is unknown!" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("IDCurrent")) Then
                                        If custRow.Item("IDCurrent") = True Then
                                            identityString = identityString & "The ID documents are current" & vbCrLf
                                        Else
                                            identityString = identityString & "The ID documents are NOT current!" & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("IDPerson")) Then
                                        If custRow.Item("IDPerson").length > 0 Then
                                            identityString = identityString & "I am satisfied the the ID is this person because " & custRow.Item("IDPerson") & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("ResStatusAccept")) Then
                                        If custRow.Item("ResStatusAccept").length > 0 Then
                                            identityString = identityString & "Their residency status is acceptable because " & custRow.Item("ResStatusAccept") & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("IDLegitimate")) Then
                                        If custRow.Item("IDLegitimate").length > 0 Then
                                            identityString = identityString & "Taking into account the age of the ID and Client I am satisfied it is legitimate because " & custRow.Item("IDLegitimate") & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("AMLRisk")) Then
                                        If custRow.Item("AMLRisk") > 0 Then
                                            identityString = identityString & "Based on the above my AML/CFT risk assessment of this client is " & MyEnums.GetDescription(DirectCast(CInt(custRow.Item("AMLRisk")), MyEnums.AMLRisk)) & vbCrLf
                                        End If
                                    End If

                                    thisCustString = thisCustString & identityString

                                    '********** Stability
                                    Dim stabilityString As String
                                    stabilityString = "*****  STABILITY  *****" & vbCrLf
                                    'tenancy
                                    stabilityString = stabilityString & "--------------------  Tenancy:" & vbCrLf
                                    If Not IsDBNull(custRow.Item("TenancyLength")) Then
                                        If custRow.Item("TenancyLength").Length > 0 Then
                                            stabilityString = stabilityString & "Length of current tenancy is " & custRow.Item("TenancyLength")
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("TenancyType")) Then
                                        If custRow.Item("TenancyType").Length > 0 Then
                                            stabilityString = stabilityString & "                    The tenancy type is " & custRow.Item("TenancyType") & vbCrLf
                                        End If
                                    Else
                                        stabilityString = stabilityString & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("TenancySatisfactory")) Then
                                        If custRow.Item("TenancySatisfactory").Length > 0 Then
                                            stabilityString = stabilityString & "This is satisfactory because " & custRow.Item("TenancySatisfactory") & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("TenancyEnvironment")) Then
                                        If custRow.Item("TenancyEnvironment").Length > 0 Then
                                            stabilityString = stabilityString & "Is the tenancy environment likely to change over the term of the loan? " & custRow.Item("TenancyEnvironment") & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("TenancyEstablish")) Then
                                        If custRow.Item("TenancyEstablish").Length > 0 Then
                                            stabilityString = stabilityString & "Established tenancy by " & custRow.Item("TenancyEstablish") & vbCrLf
                                        End If
                                    End If
                                    'employment
                                    If custType = MyEnums.CustomerType.SoleTrader Then
                                        stabilityString = stabilityString & "--------------------   Trading activity:" & vbCrLf
                                        stabilityString = stabilityString & "How long has the business been trading? " & custRow.StabilityQ1 & vbCrLf & _
                                                    "Is this sufficient? " & custRow.StabilityQ2 & vbCrLf & _
                                                    "Do they have financials? Do we have copies? " & custRow.StabilityQ3 & vbCrLf & _
                                                    "Is this the main source of income? " & custRow.StabilityQ9 & vbCrLf & _
                                                    "What sort of business is it? " & custRow.StabilityQ4 & vbCrLf & _
                                                    "Is it a satificatory business to lend to? " & custRow.StabilityQ6 & vbCrLf & _
                                                    "Is it registered with the IRD? " & custRow.StabilityQ7 & vbCrLf
                                    ElseIf custType = MyEnums.CustomerType.Individual Then
                                        stabilityString = stabilityString & "--------------------   Employment:" & vbCrLf
                                        'check if self employed
                                        If custRow.SelfEmployed = False And custRow.Beneficiary = False And custRow.EmployerNotRung = False Then
                                            If Not IsDBNull(custRow.Item("EmployerName")) Then
                                                If custRow.EmployerName.Length > 0 Then
                                                    stabilityString = stabilityString & "Rang Employer " & custRow.EmployerName & " and spoke with " & _
                                                    custRow.EmployerSpoketoName & " on " & If(custRow.EmployerRangDate = Nothing, "unknown", custRow.EmployerRangDate) & vbCrLf
                                                    stabilityString = stabilityString & "Does Client work for your company? " & custRow.StabilityQ1 & vbCrLf & _
                                                    "Are they fulltime permanent or other? " & custRow.StabilityQ2 & vbCrLf & _
                                                    "What does your employee do? " & custRow.StabilityQ3 & vbCrLf & _
                                                    "Are they PAYE employee, Contractor, Invoice or other? " & custRow.StabilityQ9 & vbCrLf & _
                                                    "How long have they been there? " & custRow.StabilityQ4 & vbCrLf & _
                                                    "Confirm pay of $" & custRow.StabilityQ5 & "? " & custRow.StabilityQ6 & vbCrLf & _
                                                    "Is their job safe? " & custRow.StabilityQ7 & vbCrLf & _
                                                    "Are they a good worker? " & custRow.StabilityQ8 & vbCrLf
                                                End If
                                            End If
                                        Else
                                            If custRow.SelfEmployed = True Then
                                                stabilityString = stabilityString & "Client is self-employed" & vbCrLf
                                            End If
                                            If custRow.Beneficiary = True Then
                                                stabilityString = stabilityString & "Client is a beneficiary" & vbCrLf
                                            End If
                                            If custRow.EmployerNotRung = True Then
                                                stabilityString = stabilityString & "Employer not rung, must be a Top-up Enquiry" & vbCrLf
                                            End If
                                        End If
                                    End If

                                    'work and other comments
                                    If Not IsDBNull(custRow.Item("StabilityComments")) Then
                                        If custRow.Item("StabilityComments").length > 0 Then
                                            stabilityString = stabilityString & "Comments: " & custRow.Item("StabilityComments") & vbCrLf
                                        End If
                                    End If

                                    thisCustString = thisCustString & stabilityString

                                    '********** Credit
                                    Dim creditString As String
                                    creditString = "*****  CREDIT  *****" & vbCrLf
                                    If Not IsDBNull(custRow.Item("CreditComments")) Then
                                        If custRow.CreditComments.Length > 0 Then
                                            creditString = creditString & "Comments on Credit History - work notes: " & custRow.CreditComments & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("CreditExplanation")) Then
                                        If custRow.CreditExplanation.Length > 0 Then
                                            creditString = creditString & "Defaults / Collection explanations: " & custRow.CreditExplanation & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("CreditOther")) Then
                                        If custRow.CreditOther.Length > 0 Then
                                            creditString = creditString & "PPSR and other checks: " & custRow.CreditOther & vbCrLf
                                        End If
                                    End If
                                    creditString = creditString & "Is Credit History sufficient? "
                                    If custRow.CreditQuestion1 = False Then
                                        creditString = creditString & "                                       No" & vbCrLf
                                    ElseIf custRow.CreditQuestion1 = True Then
                                        creditString = creditString & "                                       Yes" & vbCrLf
                                    End If
                                    creditString = creditString & "Are all enquiries expected and accounted for? "
                                    If custRow.CreditQuestion2 = False Then
                                        creditString = creditString & "     No" & vbCrLf
                                    ElseIf custRow.CreditQuestion2 = True Then
                                        creditString = creditString & "     Yes" & vbCrLf
                                    End If
                                    creditString = creditString & "Are there Gaps in the Credit History? "
                                    If custRow.CreditQuestion3 = False Then
                                        creditString = creditString & "                     No" & vbCrLf
                                    ElseIf custRow.CreditQuestion3 = True Then
                                        creditString = creditString & "                     Yes" & vbCrLf
                                    End If

                                    thisCustString = thisCustString & creditString & vbCrLf

                                End If

                                '==============================
                                'Company
                                '==============================
                            Case MyEnums.CustomerType.Company
                                thisCustString = thisCustString & "Type: Company  -  " & MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType)) & vbCrLf
                                thisCustString = thisCustString & "Company name: " & If(IsDBNull(custRow.Item("LegalName")), "", custRow.Item("LegalName")) & "      Company number: " & If(IsDBNull(custRow.Item("CompanyNumber")), "", custRow.Item("CompanyNumber")) & vbCrLf
                                If Not IsDBNull(custRow.Item("TradingName")) Then
                                    thisCustString = thisCustString & "Trading name: " & custRow.TradingName & vbCrLf
                                End If
                                If Not IsDBNull(custRow.Item("ClientNum")) Then
                                    If custRow.ClientNum.Length > 0 Then
                                        thisCustString = thisCustString & "FinPower: Client No - Client_" & custRow.ClientNum & vbCrLf 'custRow.ClientTypeId & custRow.ClientNum & vbCrLf
                                    Else
                                        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                            Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
                                        Else
                                            thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                        End If
                                    End If
                                Else
                                    If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                        Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
                                    Else
                                        thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                    End If
                                End If
                                'If EnquiryType = Split Loan Drawdown, then NO Stability info exits
                                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                                    '********** Shareholders
                                    Try
                                        Dim dtsbTa As EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                        dtsbTa = New EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                        Dim dtsbDt As EnquiryWorkSheetDataSet.DTSBDataTable
                                        Dim dtsbRow As EnquiryWorkSheetDataSet.DTSBRow
                                        dtsbDt = dtsbTa.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.ShareHolder))
                                        If dtsbDt.Rows.Count > 0 Then
                                            Dim thisDtsbString As String = String.Empty
                                            thisDtsbString = thisDtsbString & "*****  Shareholders:" & vbCrLf
                                            For Each dtsbRow In dtsbDt
                                                Dim idVerified As String = String.Empty
                                                Dim addressVerified As String = String.Empty
                                                If Not IsDBNull(dtsbRow.Item("IdVerified")) Then
                                                    If dtsbRow.Item("IdVerified") = True Then
                                                        idVerified = "Yes"
                                                    Else
                                                        idVerified = "No"
                                                    End If
                                                End If
                                                If Not IsDBNull(dtsbRow.Item("AddressVerified")) Then
                                                    If dtsbRow.Item("AddressVerified") = True Then
                                                        addressVerified = "Yes"
                                                    Else
                                                        addressVerified = "No"
                                                    End If
                                                End If
                                                thisDtsbString = thisDtsbString & "   " & dtsbRow.Item("FirstName") & "" & dtsbRow.Item("LastName") & "    Born: " & dtsbRow.Item("DateOfBirth") & vbCrLf
                                                thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
                                            Next
                                            thisCustString = thisCustString & thisDtsbString
                                        End If
                                    Catch ex As System.Exception
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Shareholders")
                                    End Try

                                    thisCustString = thisCustString & vbCrLf
                                    If custRow.BusinessShareIdentified = True Then
                                        thisCustString = thisCustString & "Company's shareholders have been fully identified" & vbCrLf
                                    Else
                                        thisCustString = thisCustString & "Company's shareholders have NOT been fully identified" & vbCrLf
                                    End If
                                    If custRow.BusinessShareStructure = True Then
                                        thisCustString = thisCustString & "Company's shareholding structure has been fully identified" & vbCrLf
                                    Else
                                        thisCustString = thisCustString & "Company's shareholding structure has NOT been fully identified" & vbCrLf
                                    End If
                                    If custRow.BusinessShareholders = True Then
                                        thisCustString = thisCustString & "Company's shareholders are satisfactory" & vbCrLf
                                    Else
                                        thisCustString = thisCustString & "Company's shareholders are NOT satisfactory, or not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessShareholderNotes")) Then
                                        If custRow.BusinessShareholderNotes.Length > 0 Then
                                            thisCustString = thisCustString & custRow.Item("BusinessShareholderNotes") & vbCrLf
                                        End If
                                    End If

                                    '********** Directors
                                    Try
                                        Dim dtsb01Ta As EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                        dtsb01Ta = New EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                        Dim dtsb01Dt As EnquiryWorkSheetDataSet.DTSB01DataTable
                                        Dim dtsb01Row As EnquiryWorkSheetDataSet.DTSB01Row
                                        dtsb01Dt = dtsb01Ta.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Director))
                                        If dtsb01Dt.Rows.Count > 0 Then
                                            Dim thisDtsbString As String = String.Empty
                                            thisDtsbString = thisDtsbString & "*****  Directors:" & vbCrLf
                                            For Each dtsb01Row In dtsb01Dt
                                                Dim idVerified As String = String.Empty
                                                Dim addressVerified As String = String.Empty
                                                If Not IsDBNull(dtsb01Row.Item("IdVerified")) Then
                                                    If dtsb01Row.Item("IdVerified") = True Then
                                                        idVerified = "Yes"
                                                    Else
                                                        idVerified = "No"
                                                    End If
                                                End If
                                                If Not IsDBNull(dtsb01Row.Item("AddressVerified")) Then
                                                    If dtsb01Row.Item("AddressVerified") = True Then
                                                        addressVerified = "Yes"
                                                    Else
                                                        addressVerified = "No"
                                                    End If
                                                End If
                                                thisDtsbString = thisDtsbString & "   " & dtsb01Row.Item("FirstName") & "" & dtsb01Row.Item("LastName") & "    Born: " & dtsb01Row.Item("DateOfBirth") & vbCrLf
                                                thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
                                            Next
                                            thisCustString = thisCustString & thisDtsbString
                                        End If
                                    Catch ex As System.Exception
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Directors")
                                    End Try

                                    thisCustString = thisCustString & vbCrLf
                                    If Not IsDBNull(custRow.Item("BusinessDirectors")) Then
                                        If custRow.BusinessDirectors = True Then
                                            thisCustString = thisCustString & "Company's directors are satisfactory" & vbCrLf
                                        Else
                                            thisCustString = thisCustString & "Company's directors are NOT satisfactory, or not yet checked" & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessDirectorNotes")) Then
                                        If custRow.BusinessDirectorNotes.Length > 0 Then
                                            thisCustString = thisCustString & custRow.Item("BusinessDirectorNotes") & vbCrLf
                                        End If
                                    End If
                                    '********** Trade References
                                    Try
                                        Dim tradeRefTa As EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                        tradeRefTa = New EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                        Dim tradeRefDt As EnquiryWorkSheetDataSet.TradeReferencesDataTable
                                        Dim trRow As EnquiryWorkSheetDataSet.TradeReferencesRow
                                        tradeRefDt = tradeRefTa.GetDataByCustomerId(New System.Nullable(Of Integer)(custId))
                                        If tradeRefDt.Rows.Count > 0 Then
                                            Dim thisTrString As String = String.Empty
                                            thisTrString = thisTrString & "*****  TradeReferences:" & vbCrLf
                                            For Each trRow In tradeRefDt
                                                thisTrString = thisTrString & "   " & trRow.Item("BusinessName") & vbCrLf
                                                thisTrString = thisTrString & "   Account: " & trRow.Item("Account") & vbCrLf
                                                thisTrString = thisTrString & "   Spend: " & trRow.Item("Spend") & vbCrLf
                                                thisTrString = thisTrString & "   Pay: " & trRow.Item("Pay") & vbCrLf
                                                thisTrString = thisTrString & "   Comments: " & trRow.Item("Comments") & vbCrLf
                                            Next
                                            thisCustString = thisCustString & thisTrString
                                        End If
                                    Catch ex As Exception
                                        log.Error("Trade References: " & ex.Message & " : " & ex.TargetSite.ToString)
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Trade References")
                                    End Try
                                    '********** Stability
                                    Dim stabilityString As String
                                    stabilityString = vbCrLf & "*****  STABILITY  *****" & vbCrLf
                                    Dim thisCompanyInfo As String = "--------------------  Company Information:" & vbCrLf
                                    If Not IsDBNull(custRow.Item("BusinessRegistered")) Then
                                        If custRow.BusinessRegistered = True Then
                                            thisCompanyInfo = thisCompanyInfo & "Company is registered" & vbCrLf
                                        Else
                                            thisCompanyInfo = thisCompanyInfo & "Company is NOT registered" & vbCrLf
                                        End If
                                    Else
                                        thisCompanyInfo = thisCompanyInfo & "Company registration not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("CoExtractTrustDeed")) Then
                                        If custRow.CoExtractTrustDeed = True Then
                                            thisCompanyInfo = thisCompanyInfo & "Company Extract is saved" & vbCrLf
                                        Else
                                            thisCompanyInfo = thisCompanyInfo & "Company Extract is NOT saved" & vbCrLf
                                        End If
                                    Else
                                        thisCompanyInfo = thisCompanyInfo & "Company Extract is NOT saved" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessReturnsFiled")) Then
                                        If custRow.BusinessReturnsFiled = True Then
                                            thisCompanyInfo = thisCompanyInfo & "Company has filed returns" & vbCrLf
                                        Else
                                            thisCompanyInfo = thisCompanyInfo & "Company has NOT filed returns" & vbCrLf
                                        End If
                                    Else
                                        thisCompanyInfo = thisCompanyInfo & "Company filed returns not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessPaidUpCapital")) Then
                                        If custRow.BusinessPaidUpCapital = True Then
                                            thisCompanyInfo = thisCompanyInfo & "Company's paid-up capital is sufficient" & vbCrLf
                                        Else
                                            thisCompanyInfo = thisCompanyInfo & "Company's paid-up capital is NOT sufficient" & vbCrLf
                                        End If
                                    Else
                                        thisCompanyInfo = thisCompanyInfo & "Company's paid-up capital is not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessHistorySatisfactory")) Then
                                        If custRow.BusinessHistorySatisfactory = True Then
                                            thisCompanyInfo = thisCompanyInfo & "The Company's previous history is satisfactory" & vbCrLf
                                        Else
                                            thisCompanyInfo = thisCompanyInfo & "The Company's previous history is NOT satisfactory" & vbCrLf
                                        End If
                                    Else
                                        thisCompanyInfo = thisCompanyInfo & "The Company's previous history is not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessHistoryNotes")) Then
                                        If custRow.BusinessHistoryNotes.Length > 0 Then
                                            thisCompanyInfo = thisCompanyInfo & custRow.Item("BusinessHistoryNotes") & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessAddressSatisfactory")) Then
                                        If custRow.BusinessAddressSatisfactory = True Then
                                            thisCompanyInfo = thisCompanyInfo & "The Company's addresses are satisfactory" & vbCrLf
                                        Else
                                            thisCompanyInfo = thisCompanyInfo & "The Company's addresses are NOT satisfactory" & vbCrLf
                                        End If
                                    Else
                                        thisCompanyInfo = thisCompanyInfo & "The Company's addresses are not yet checked" & vbCrLf
                                    End If

                                    If Not IsDBNull(custRow.Item("BusinessAddressNotes")) Then
                                        If custRow.BusinessAddressNotes.Length > 0 Then
                                            thisCompanyInfo = thisCompanyInfo & custRow.BusinessAddressNotes & vbCrLf
                                        End If
                                    End If
                                    stabilityString = stabilityString & thisCompanyInfo
                                    'Company Tenancy
                                    Dim thisCompanyTenancy As String = "--------------------  Company Tenancy:" & vbCrLf
                                    If Not IsDBNull(custRow.Item("TenancyType")) Then
                                        If custRow.TenancyType.Length > 0 Then
                                            thisCompanyTenancy = thisCompanyTenancy & "The company tenancy is " & custRow.TenancyType & " for " & custRow.TenancyLength & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("TenancySatisfactory")) Then
                                        If custRow.TenancySatisfactory.Length > 0 Then
                                            thisCompanyTenancy = thisCompanyTenancy & "This is satisfactory for the term of the loan because " & custRow.TenancySatisfactory & vbCrLf
                                        End If
                                    End If
                                    stabilityString = stabilityString & thisCompanyTenancy
                                    thisCustString = thisCustString & stabilityString & vbCrLf
                                    'Company Credit
                                    Dim creditString As String
                                    creditString = vbCrLf & "*****  CREDIT  *****" & vbCrLf
                                    If Not IsDBNull(custRow.Item("CreditComments")) Then
                                        If custRow.CreditComments.Length > 0 Then
                                            creditString = creditString & "Comments on Credit History - work notes: " & custRow.CreditComments & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("CreditExplanation")) Then
                                        If custRow.CreditExplanation.Length > 0 Then
                                            creditString = creditString & "Defaults / Collection explanations: " & custRow.CreditExplanation & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("CreditOther")) Then
                                        If custRow.CreditOther.Length > 0 Then
                                            creditString = creditString & "PPSR and other checks: " & custRow.CreditOther & vbCrLf
                                        End If
                                    End If
                                    creditString = creditString & "Is Credit History sufficient? "
                                    If custRow.CreditQuestion1 = False Then
                                        creditString = creditString & "                                       No" & vbCrLf
                                    ElseIf custRow.CreditQuestion1 = True Then
                                        creditString = creditString & "                                       Yes" & vbCrLf
                                    End If
                                    creditString = creditString & "Are all enquiries expected and accounted for? "
                                    If custRow.CreditQuestion2 = False Then
                                        creditString = creditString & "     No" & vbCrLf
                                    ElseIf custRow.CreditQuestion2 = True Then
                                        creditString = creditString & "     Yes" & vbCrLf
                                    End If
                                    creditString = creditString & "Are there Gaps in the Credit History? "
                                    If custRow.CreditQuestion3 = False Then
                                        creditString = creditString & "                     No" & vbCrLf
                                    ElseIf custRow.CreditQuestion3 = True Then
                                        creditString = creditString & "                     Yes" & vbCrLf
                                    End If

                                    thisCustString = thisCustString & creditString & vbCrLf

                                    'Company Other Comments
                                    If Not IsDBNull(custRow.Item("BusinessOtherNotes")) Then
                                        If custRow.Item("BusinessOtherNotes").length > 0 Then
                                            thisCustString = thisCustString & "Other notes: " & custRow.Item("BusinessOtherNotes") & vbCrLf
                                        End If
                                    End If

                                End If 'End:  If Not ThisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not ThisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not ThisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown 

                                '==============================
                                'Trust
                                '==============================
                            Case MyEnums.CustomerType.Trust
                                thisCustString = thisCustString & "Type: Trust  -  " & MyEnums.GetDescription(DirectCast(custEnquiryType, MyEnums.CustomerEnquiryType)) & vbCrLf
                                If Not IsDBNull(custRow.Item("TradingName")) Then
                                    thisCustString = thisCustString & "Trust name: " & custRow.TradingName & vbCrLf
                                End If
                                If Not IsDBNull(custRow.Item("ClientNum")) Then
                                    If custRow.ClientNum.Length > 0 Then
                                        thisCustString = thisCustString & "FinPower: Client No - Client_" & custRow.ClientNum & vbCrLf 'custRow.ClientTypeId & custRow.ClientNum & vbCrLf
                                    Else
                                        If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                            Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is empty.")
                                        Else
                                            thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                        End If
                                    End If
                                Else
                                    If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                        Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Customer: ClientNum is null.")
                                    Else
                                        thisCustString = thisCustString & "FinPower: Client No - unknown" & vbCrLf
                                    End If
                                End If
                                'If EnquiryType = Split Loan Drawdown, then NO Stability info exits
                                If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                                    '********** Beneficaries
                                    Try
                                        Dim dtsbTa As EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                        dtsbTa = New EnquiryWorkSheetDataSetTableAdapters.DTSBTableAdapter
                                        Dim dtsbDt As EnquiryWorkSheetDataSet.DTSBDataTable
                                        Dim dtsbRow As EnquiryWorkSheetDataSet.DTSBRow
                                        dtsbDt = dtsbTa.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Beneficiary))
                                        If dtsbDt.Rows.Count > 0 Then
                                            Dim thisDtsbString As String = String.Empty
                                            thisDtsbString = thisDtsbString & "*****  Beneficaries:" & vbCrLf
                                            For Each dtsbRow In dtsbDt.Rows
                                                Dim idVerified As String = String.Empty
                                                Dim addressVerified As String = String.Empty
                                                If Not IsDBNull(dtsbRow.Item("IdVerified")) Then
                                                    If dtsbRow.Item("IdVerified") = True Then
                                                        idVerified = "Yes"
                                                    Else
                                                        idVerified = "No"
                                                    End If
                                                End If
                                                If Not IsDBNull(dtsbRow.Item("AddressVerified")) Then
                                                    If dtsbRow.Item("AddressVerified") = True Then
                                                        addressVerified = "Yes"
                                                    Else
                                                        addressVerified = "No"
                                                    End If
                                                End If
                                                thisDtsbString = thisDtsbString & "   " & dtsbRow.Item("FirstName") & "" & dtsbRow.Item("LastName") & "    Born: " & dtsbRow.Item("DateOfBirth") & vbCrLf
                                                thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
                                            Next
                                            thisCustString = thisCustString & thisDtsbString
                                        End If
                                    Catch ex As System.Exception
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Beneficaries")
                                    End Try
                                    '********** Trustees
                                    Try
                                        Dim dtsb01Ta As EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                        dtsb01Ta = New EnquiryWorkSheetDataSetTableAdapters.DTSB01TableAdapter
                                        Dim dtsb01Dt As EnquiryWorkSheetDataSet.DTSB01DataTable
                                        Dim dtsb01Row As EnquiryWorkSheetDataSet.DTSB01Row
                                        dtsb01Dt = dtsb01Ta.GetData(New System.Nullable(Of Integer)(custId), New System.Nullable(Of Integer)(MyEnums.DtsbType.Trustee))
                                        If dtsb01Dt.Rows.Count > 0 Then
                                            Dim thisDtsbString As String = String.Empty
                                            thisDtsbString = thisDtsbString & "*****  Trustees:" & vbCrLf
                                            For Each dtsb01Row In dtsb01Dt
                                                Dim idVerified As String = String.Empty
                                                Dim addressVerified As String = String.Empty
                                                If Not IsDBNull(dtsb01Row.Item("IdVerified")) Then
                                                    If dtsb01Row.Item("IdVerified") = True Then
                                                        idVerified = "Yes"
                                                    Else
                                                        idVerified = "No"
                                                    End If
                                                End If
                                                If Not IsDBNull(dtsb01Row.Item("AddressVerified")) Then
                                                    If dtsb01Row.Item("AddressVerified") = True Then
                                                        addressVerified = "Yes"
                                                    Else
                                                        addressVerified = "No"
                                                    End If
                                                End If
                                                thisDtsbString = thisDtsbString & "   " & dtsb01Row.Item("FirstName") & "" & dtsb01Row.Item("LastName") & "    Born: " & dtsb01Row.Item("DateOfBirth") & vbCrLf
                                                thisDtsbString = thisDtsbString & "   IdVerified: " & idVerified & "   AddressVerified: " & addressVerified & vbCrLf
                                            Next
                                            thisCustString = thisCustString & thisDtsbString
                                        End If
                                    Catch ex As System.Exception
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Trustees")
                                    End Try
                                    '********** Trade References
                                    Try
                                        Dim tradeRefTa As EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                        tradeRefTa = New EnquiryWorkSheetDataSetTableAdapters.TradeReferencesTableAdapter
                                        Dim tradeRefDt As EnquiryWorkSheetDataSet.TradeReferencesDataTable
                                        Dim trRow As EnquiryWorkSheetDataSet.TradeReferencesRow
                                        tradeRefDt = tradeRefTa.GetDataByCustomerId(New System.Nullable(Of Integer)(custId))
                                        If tradeRefDt.Rows.Count > 0 Then
                                            Dim thisTrString As String = String.Empty
                                            thisTrString = thisTrString & "*****  TradeReferences:" & vbCrLf
                                            For Each trRow In tradeRefDt
                                                thisTrString = thisTrString & "   " & trRow.Item("BusinessName") & vbCrLf
                                                thisTrString = thisTrString & "   Account: " & trRow.Item("Account") & vbCrLf
                                                thisTrString = thisTrString & "   Spend: " & trRow.Item("Spend") & vbCrLf
                                                thisTrString = thisTrString & "   Pay: " & trRow.Item("Pay") & vbCrLf
                                                thisTrString = thisTrString & "   Comments: " & trRow.Item("Comments") & vbCrLf
                                            Next
                                            thisCustString = thisCustString & thisTrString
                                        End If
                                    Catch ex As Exception
                                        log.Error("Trade References: " & ex.Message & " : " & ex.TargetSite.ToString)
                                        System.Windows.Forms.MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString, "Trade References")
                                    End Try
                                    '********** Stability
                                    Dim stabilityString As String
                                    stabilityString = vbCrLf & "*****  STABILITY  *****" & vbCrLf
                                    Dim thisTrustInfo As String = "--------------------  Trust Information:" & vbCrLf
                                    'If custRow.BusinessRegistered = True Then
                                    '    thisCompanyInfo = thisCompanyInfo & "Company is registered" & vbCrLf
                                    'Else
                                    '    thisCompanyInfo = thisCompanyInfo & "Company is NOT registered or not yet checked" & vbCrLf
                                    'End If
                                    If custRow.CoExtractTrustDeed = True Then
                                        thisTrustInfo = thisTrustInfo & "Trust Deed is saved" & vbCrLf
                                    Else
                                        thisTrustInfo = thisTrustInfo & "Trust Deed is NOT saved" & vbCrLf
                                    End If
                                    If custRow.BusinessReturnsFiled = True Then
                                        thisTrustInfo = thisTrustInfo & "Trust has files returns" & vbCrLf
                                    Else
                                        thisTrustInfo = thisTrustInfo & "Trust has NOT filed returns, or not yet checked" & vbCrLf
                                    End If
                                    If custRow.BusinessPaidUpCapital = True Then
                                        thisTrustInfo = thisTrustInfo & "Trust's paid-up capital is sufficient" & vbCrLf
                                    Else
                                        thisTrustInfo = thisTrustInfo & "Trust's paid-up capital is NOT sufficient, or not yet checked" & vbCrLf
                                    End If
                                    If custRow.BusinessHistorySatisfactory = True Then
                                        thisTrustInfo = thisTrustInfo & "The Trust's previous history is satisfactory" & vbCrLf
                                    Else
                                        thisTrustInfo = thisTrustInfo & "The Trust's previous history is NOT satisfactory, or not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessHistoryNotes")) Then
                                        If custRow.BusinessHistoryNotes.Length > 0 Then
                                            thisTrustInfo = thisTrustInfo & custRow.Item("BusinessHistoryNotes") & vbCrLf
                                        End If
                                    End If
                                    If custRow.BusinessAddressSatisfactory = True Then
                                        thisTrustInfo = thisTrustInfo & "The Trust's addresses are satisfactory" & vbCrLf
                                    Else
                                        thisTrustInfo = thisTrustInfo & "The Trust's addresses are NOT satisfactory, or not yet checked" & vbCrLf
                                    End If
                                    If Not IsDBNull(custRow.Item("BusinessAddressNotes")) Then
                                        If custRow.BusinessAddressNotes.Length > 0 Then
                                            thisTrustInfo = thisTrustInfo & custRow.BusinessAddressNotes & vbCrLf
                                        End If
                                    End If
                                    stabilityString = stabilityString & thisTrustInfo
                                    'Company Tenancy
                                    Dim thisTrustTenancy As String = "--------------------  Trust Tenancy:" & vbCrLf
                                    If Not IsDBNull(custRow.Item("TenancyType")) Then
                                        If custRow.TenancyType.Length > 0 Then
                                            thisTrustTenancy = thisTrustTenancy & "The trust tenancy is " & custRow.TenancyType & " for " & custRow.TenancyLength & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("TenancySatisfactory")) Then
                                        If custRow.TenancySatisfactory.Length > 0 Then
                                            thisTrustTenancy = thisTrustTenancy & "This is satisfactory for the term of the loan because " & custRow.TenancySatisfactory & vbCrLf
                                        End If
                                    End If
                                    stabilityString = stabilityString & thisTrustTenancy
                                    thisCustString = thisCustString & stabilityString & vbCrLf

                                    'Company Credit
                                    Dim creditString As String
                                    creditString = vbCrLf & "*****  CREDIT  *****" & vbCrLf
                                    If Not IsDBNull(custRow.Item("CreditComments")) Then
                                        If custRow.CreditComments.Length > 0 Then
                                            creditString = creditString & "Comments on Credit History - work notes: " & custRow.CreditComments & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("CreditExplanation")) Then
                                        If custRow.CreditExplanation.Length > 0 Then
                                            creditString = creditString & "Defaults / Collection explanations: " & custRow.CreditExplanation & vbCrLf
                                        End If
                                    End If
                                    If Not IsDBNull(custRow.Item("CreditOther")) Then
                                        If custRow.CreditOther.Length > 0 Then
                                            creditString = creditString & "PPSR and other checks: " & custRow.CreditOther & vbCrLf
                                        End If
                                    End If
                                    creditString = creditString & "Is Credit History sufficient? "
                                    If custRow.CreditQuestion1 = False Then
                                        creditString = creditString & "                                       No" & vbCrLf
                                    ElseIf custRow.CreditQuestion1 = True Then
                                        creditString = creditString & "                                       Yes" & vbCrLf
                                    End If
                                    creditString = creditString & "Are all enquiries expected and accounted for? "
                                    If custRow.CreditQuestion2 = False Then
                                        creditString = creditString & "     No" & vbCrLf
                                    ElseIf custRow.CreditQuestion2 = True Then
                                        creditString = creditString & "     Yes" & vbCrLf
                                    End If
                                    creditString = creditString & "Are there Gaps in the Credit History? "
                                    If custRow.CreditQuestion3 = False Then
                                        creditString = creditString & "                     No" & vbCrLf
                                    ElseIf custRow.CreditQuestion3 = True Then
                                        creditString = creditString & "                     Yes" & vbCrLf
                                    End If

                                    thisCustString = thisCustString & creditString & vbCrLf

                                    'Company Other Comments
                                    If Not IsDBNull(custRow.Item("BusinessOtherNotes")) Then
                                        If custRow.Item("BusinessOtherNotes").length > 0 Then
                                            thisCustString = thisCustString & "Other notes: " & custRow.Item("BusinessOtherNotes") & vbCrLf
                                        End If
                                    End If

                                End If


                            Case Else

                        End Select

                        clientString = clientString & thisCustString & vbCrLf
                    Next

                End If

                If clientString.Length > 0 Then
                    constructedString = constructedString & clientString
                End If
                '********** End of Client Info ************ 

                '********** Audit ********************
                Dim auditString As String = ""
                auditString = vbCrLf & "*****  AUDIT INFORMATION  **********************************************" & vbCrLf
                Dim auditTa As EnquiryWorkSheetDataSetTableAdapters.DueDiligenceTableAdapter
                auditTa = New EnquiryWorkSheetDataSetTableAdapters.DueDiligenceTableAdapter
                Dim auditDt As EnquiryWorkSheetDataSet.DueDiligenceDataTable
                auditDt = auditTa.GetDataByEnquiryId(enquiryId)
                Dim auditRow As EnquiryWorkSheetDataSet.DueDiligenceRow
                If auditDt.Rows.Count > 0 Then
                    'Get Row in DueDiligence dataset
                    auditRow = auditDt.Rows(0)
                    'Loan Breakdown & Dealer Commission
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        'Loan Breakdown
                        auditString = auditString & "--------------------  Loan Breakdown:" & vbCrLf
                        If Not IsDBNull(auditRow.Item("LoanNum")) Then
                            auditString = auditString & "Loan number: " & auditRow.Item("LoanNum") & vbCrLf
                        Else
                            If thisCurrentStatus = Constants.CONST_CURRENT_STATUS_PAYOUT Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_EXPORTED Then
                                Throw New System.Exception("ConstructPrintStringAll: Error has occurred in Audit: Loan number is null.")
                            Else
                                auditString = auditString & "Loan number: Unknown" & vbCrLf
                            End If
                        End If
                        Dim totalDrawdown As Decimal = If(IsDBNull(auditRow.Item("CashPrice_Advance")), 0, auditRow.Item("CashPrice_Advance")) + If(IsDBNull(auditRow.Item("OtherAdvance")), 0, auditRow.Item("OtherAdvance")) - If(IsDBNull(auditRow.Item("Deposit")), 0, auditRow.Item("Deposit")) + If(IsDBNull(auditRow.Item("Brokerage")), 0, auditRow.Item("Brokerage"))
                        auditString = auditString & "Cash price/Advance: " & Format(If(IsDBNull(auditRow.Item("CashPrice_Advance")), 0, auditRow.Item("CashPrice_Advance")), "C") & "  |  Other advanced: " & Format(If(IsDBNull(auditRow.Item("OtherAdvance")), 0, auditRow.Item("OtherAdvance")), "C") & vbCrLf & "Deposit: " & Format(If(IsDBNull(auditRow.Item("Deposit")), 0, auditRow.Item("Deposit")), "C") & "  |  Brokerage: " & Format(If(IsDBNull(auditRow.Item("Brokerage")), 0, auditRow.Item("Brokerage")), "C") & vbCrLf & "Loan retention: " & If(IsDBNull(auditRow.Item("LoanRetention")), 0, auditRow.Item("LoanRetention")) & "%" & vbCrLf & "Total drawdown: " & Format(totalDrawdown, "C") & vbCrLf
                        auditString = auditString & "Refinance: " & Format(If(IsDBNull(auditRow.Item("Refinance")), 0, auditRow.Item("Refinance")), "C") & "  |  YFL fees: " & Format(If(IsDBNull(auditRow.Item("YFLFees")), 0, auditRow.Item("YFLFees")), "C") & "  |  YFL insurances: " & Format(If(IsDBNull(auditRow.Item("YFLInsurances")), 0, auditRow.Item("YFLInsurances")), "C") & vbCrLf
                        'AmountFinanced = CashPriceAdvance + OtherAdvance - Deposit + Refinance + YFLFees + YFLInsurances  
                        Dim amountFinanced As Decimal = If(IsDBNull(auditRow.Item("CashPrice_Advance")), 0, auditRow.Item("CashPrice_Advance")) + If(IsDBNull(auditRow.Item("OtherAdvance")), 0, auditRow.Item("OtherAdvance")) - If(IsDBNull(auditRow.Item("Deposit")), 0, auditRow.Item("Deposit")) + If(IsDBNull(auditRow.Item("Brokerage")), 0, auditRow.Item("Brokerage")) + If(IsDBNull(auditRow.Item("Refinance")), 0, auditRow.Item("Refinance")) + If(IsDBNull(auditRow.Item("YFLFees")), 0, auditRow.Item("YFLFees")) + If(IsDBNull(auditRow.Item("YFLInsurances")), 0, auditRow.Item("YFLInsurances"))
                        auditString = auditString & "Amount financed: " & Format(amountFinanced, "C") & vbCrLf
                        If If(IsDBNull(auditRow.Item("LoanFiguresChecked")), False, auditRow.Item("LoanFiguresChecked")) = True Then
                            auditString = auditString & "Loan breakdown figures have been checked." & vbCrLf
                        Else
                            auditString = auditString & "Loan breakdown figures have NOT been checked!" & vbCrLf
                        End If
                        'Dealer Commission
                        auditString = auditString & "--------------------  Dealer Commission:" & vbCrLf
                        Dim totalCommission As Decimal = If(IsDBNull(auditRow.Item("ProductCommission")), 0, auditRow.Item("ProductCommission")) + If(IsDBNull(auditRow.Item("InterestCommission")), 0, auditRow.Item("InterestCommission"))
                        auditString = auditString & "Product commission: " & Format(If(IsDBNull(auditRow.Item("ProductCommission")), 0, auditRow.Item("ProductCommission")), "C") & "  |  Interest commission: " & Format(If(IsDBNull(auditRow.Item("InterestCommission")), 0, auditRow.Item("InterestCommission")), "C") & vbCrLf & "Commission retention: " & If(IsDBNull(auditRow.Item("CommRetention")), 0, auditRow.Item("CommRetention")) & "%" & vbCrLf & "Total commission: " & Format(totalCommission, "C") & vbCrLf
                        If If(IsDBNull(auditRow.Item("CommFiguresChecked")), False, auditRow.Item("CommFiguresChecked")) = True Then
                            auditString = auditString & "Dealer commission figures have been checked." & vbCrLf
                        Else
                            auditString = auditString & "Dealer commission figures have NOT been checked!" & vbCrLf
                        End If
                    End If
                    'documents
                    auditString = auditString & "--------------------  Documents:" & vbCrLf
                    If If(IsDBNull(auditRow.Item("Originals")), False, auditRow.Item("Originals")) = True Then
                        auditString = auditString & "All pages of original contract are clear and legible - Yes" & vbCrLf
                    Else
                        auditString = auditString & "All pages of original contract are clear and legible - No" & vbCrLf
                    End If
                    If If(IsDBNull(auditRow.Item("Signed")), False, auditRow.Item("Signed")) = True Then
                        auditString = auditString & "All pages are signed or initialled correctly where appropiate - Yes" & vbCrLf
                    Else
                        auditString = auditString & "All pages are signed or initialled correctly where appropiate - No" & vbCrLf
                    End If

                    If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                        If If(IsDBNull(auditRow.Item("Application")), False, auditRow.Item("Application")) = True Then
                            auditString = auditString & "Received original application; completed and signed correctly - Yes" & vbCrLf
                        Else
                            auditString = auditString & "Received original application; completed and signed correctly - No" & vbCrLf
                        End If
                    End If
                    If If(IsDBNull(auditRow.Item("SpecialConditions")), False, auditRow.Item("SpecialConditions")) = True Then
                        auditString = auditString & "All special conditions are met - Yes" & vbCrLf
                    Else
                        auditString = auditString & "All special conditions are met - No" & vbCrLf
                    End If

                    If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                        If If(IsDBNull(auditRow.Item("Documents")), False, auditRow.Item("Documents")) = True Then
                            auditString = auditString & "All supporting documents are clear and legible - Yes" & vbCrLf
                        Else
                            auditString = auditString & "All supporting documents are clear and legible - No" & vbCrLf
                        End If
                    End If
                    'processing
                    auditString = auditString & "--------------------  Processing:" & vbCrLf
                    If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                        If If(IsDBNull(auditRow.Item("DDinPlace")), False, auditRow.Item("DDinPlace")) = True Then
                            auditString = auditString & "DD is already inplace (Variation)" & vbCrLf
                        End If
                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                            auditString = auditString & "DD / AP Form Sheet number - " & If(IsDBNull(auditRow.Item("APDDShtNum")), String.Empty, auditRow.Item("APDDShtNum")) & vbCrLf
                            If If(IsDBNull(auditRow.Item("BanckAcc")), False, auditRow.Item("BanckAcc")) = True Then
                                auditString = auditString & "Bank Acc No in finPower checked - Yes"
                            Else
                                auditString = auditString & "Bank Acc No in finPower checked - No "
                            End If
                            If If(IsDBNull(auditRow.Item("PayBookSent")), False, auditRow.Item("PayBookSent")) = True Then
                                auditString = auditString & "                           Payment book given to client - Yes" & vbCrLf
                                'for use in payout - final
                                auditPayBookSentSwitch = True
                            Else
                                auditString = auditString & "                          Payment book given to client - No" & vbCrLf
                            End If
                            If auditRow.APDDClientFiled = True Then
                                auditString = auditString & "Client has setup themselves and the Agreement Form has been filed." & vbCrLf
                            End If
                            auditString = auditString & "Payment method has been checked in finPower? "
                            If If(IsDBNull(auditRow.Item("PayMethod")), False, auditRow.Item("PayMethod")) = True Then
                                auditString = auditString & "Yes" & vbCrLf
                            Else
                                auditString = auditString & "No" & vbCrLf
                            End If
                            auditString = auditString & "Wage deduction has been confirmed? "
                            If If(IsDBNull(auditRow.Item("WageDeduction")), False, auditRow.Item("WageDeduction")) = True Then
                                auditString = auditString & "Yes" & vbCrLf
                            Else
                                auditString = auditString & "No" & vbCrLf
                            End If
                            'Documents
                            auditString = auditString & "Documents scanned and/or saved: "
                            auditString = auditString & "IDs - "
                            If If(IsDBNull(auditRow.Item("IDsaved")), False, auditRow.Item("IDsaved")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            auditString = auditString & "                   DD / AP - "
                            If If(IsDBNull(auditRow.Item("DDAPsaved")), False, auditRow.Item("DDAPsaved")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            auditString = auditString & "                   Application - "
                            If If(IsDBNull(auditRow.Item("AppSaved")), False, auditRow.Item("AppSaved")) = True Then
                                auditString = auditString & "Yes" & vbCrLf
                            Else
                                auditString = auditString & "No " & vbCrLf
                            End If
                            auditString = auditString & "                                    Proof of address - "
                            If If(IsDBNull(auditRow.Item("ProofAddress")), False, auditRow.Item("ProofAddress")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            auditString = auditString & "                   Proof of income - "
                            If If(IsDBNull(auditRow.Item("ProofIncome")), False, auditRow.Item("ProofIncome")) = True Then
                                auditString = auditString & "Yes" & vbCrLf
                            Else
                                auditString = auditString & "No " & vbCrLf
                            End If
                        End If

                        'finPower  
                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                            auditString = auditString & "All fields are filled in and correct: Client - "
                            If If(IsDBNull(auditRow.Item("finClient")), False, auditRow.Item("finClient")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            auditString = auditString & "     Loan - "
                            If If(IsDBNull(auditRow.Item("finLoan")), False, auditRow.Item("finLoan")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            auditString = auditString & "     Security - "
                            If If(IsDBNull(auditRow.Item("finSecurity")), False, auditRow.Item("finSecurity")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            auditString = auditString & "     Graded - "
                            If If(IsDBNull(auditRow.Item("finGrading")), False, auditRow.Item("finGrading")) = True Then
                                auditString = auditString & "Yes" & vbCrLf
                            Else
                                auditString = auditString & "No " & vbCrLf
                            End If
                            'PPSR registered
                            auditString = auditString & "PPSR registered: "
                            If If(IsDBNull(auditRow.Item("PPSRNew")), False, auditRow.Item("PPSRNew")) = True Then
                                auditString = auditString & "New"
                            ElseIf If(IsDBNull(auditRow.Item("PPSRAmended")), False, auditRow.Item("PPSRAmended")) = True Then
                                auditString = auditString & "Amended"
                            Else
                                auditString = auditString & "Not registered"
                            End If
                            If If(IsDBNull(auditRow.Item("PPSRFiled")), False, auditRow.Item("PPSRFiled")) = True Then
                                auditString = auditString & "               F/S PIN - printed and saved" & vbCrLf
                            Else
                                auditString = auditString & "               F/S PIN - not processed" & vbCrLf
                            End If
                            auditString = auditString & "Financial Statement has been checked and is correct:" & vbCrLf & "Debtor - "
                            If If(IsDBNull(auditRow.Item("FSDebtor")), False, auditRow.Item("FSDebtor")) = True Then
                                auditString = auditString & "Yes"
                            Else
                                auditString = auditString & "No "
                            End If
                            If If(IsDBNull(auditRow.Item("FSType")), False, auditRow.Item("FSType")) = True Then
                                auditString = auditString & "               Collateral Type - Yes"
                            Else
                                auditString = auditString & "               Collateral Type - No "
                            End If
                            If If(IsDBNull(auditRow.Item("FSDesc")), False, auditRow.Item("FSDesc")) = True Then
                                auditString = auditString & "               Description - Yes" & vbCrLf
                            Else
                                auditString = auditString & "               Description - No " & vbCrLf
                            End If
                            'Protecta
                            auditString = auditString & "Protecta: "
                            If If(IsDBNull(auditRow.Item("NoProtectaPolicies")), False, auditRow.Item("NoProtectaPolicies")) = True Then
                                auditString = auditString & "There are no Protecta policies" & vbCrLf
                            Else
                                Dim lineBreakVar As Boolean = False
                                If If(IsDBNull(auditRow.Item("InsuranceIdentified")), False, auditRow.Item("InsuranceIdentified")) = True Then
                                    auditString = auditString & "Insurance has been identified in OAC Insurances Report"
                                Else
                                    auditString = auditString & "Insurance has NOT been identified in OAC Insurances Report" & vbCrLf
                                End If
                                auditString = auditString & "  entered for - "
                                If If(IsDBNull(auditRow.Item("CPI")), False, auditRow.Item("CPI")) = True Then
                                    auditString = auditString & "CPI,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("PPIfull")), False, auditRow.Item("PPIfull")) = True Then
                                    auditString = auditString & "PPI full,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("PPIDO")), False, auditRow.Item("PPIDO")) = True Then
                                    auditString = auditString & "PPI Death only,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("GAP1")), False, auditRow.Item("GAP1")) = True Then
                                    auditString = auditString & "GAP option1,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("GAP2")), False, auditRow.Item("GAP2")) = True Then
                                    auditString = auditString & "GAP option2,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("MechWarr")), False, auditRow.Item("MechWarr")) = True Then
                                    auditString = auditString & "Mech Warranty,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("InsuranceV")), False, auditRow.Item("InsuranceV")) = True Then
                                    auditString = auditString & "Vehicle insurance,  "
                                    lineBreakVar = True
                                End If
                                If If(IsDBNull(auditRow.Item("InsuranceC")), False, auditRow.Item("InsuranceC")) = True Then
                                    auditString = auditString & "Chattels insurance,  "
                                    lineBreakVar = True
                                End If
                                If lineBreakVar = True Then
                                    auditString = auditString & vbCrLf
                                End If
                                If Not IsDBNull(auditRow.Item("SpecialNotes")) Then
                                    If auditRow.Item("SpecialNotes").Length > 0 Then
                                        auditString = auditString & " Special notes: " & auditRow.Item("SpecialNotes") & vbCrLf
                                    End If
                                End If

                            End If
                            'insurances
                            auditString = auditString & "Insurances: "
                            If If(IsDBNull(auditRow.Item("SecurityInsured")), False, auditRow.Item("SecurityInsured")) = True Then
                                auditString = auditString & "Security insured and YFL interest noted" & vbCrLf
                            ElseIf If(IsDBNull(auditRow.Item("NoInsuranceRequired")), False, auditRow.Item("NoInsuranceRequired")) = True Then
                                auditString = auditString & "No insurance is required" & vbCrLf
                            End If
                        End If
                        'disbursements
                        auditString = auditString & "--------------------  Disbursements:" & vbCrLf
                        If If(IsDBNull(auditRow.Item("DisburseAmt1")), 0, auditRow.Item("DisburseAmt1")) > 0 Then
                            auditString = auditString & auditRow.Item("DisburseName1") & ",  " & auditRow.Item("DisburseAcc1") & ",  " & auditRow.Item("DisburseRef1") & ",  " & Format(auditRow.Item("DisburseAmt1"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(auditRow.Item("DisburseAmt2")), 0, auditRow.Item("DisburseAmt2")) > 0 Then
                            auditString = auditString & auditRow.Item("DisburseName2") & ",  " & auditRow.Item("DisburseAcc2") & ",  " & auditRow.Item("DisburseRef2") & ",  " & Format(auditRow.Item("DisburseAmt2"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(auditRow.Item("DisburseAmt3")), 0, auditRow.Item("DisburseAmt3")) > 0 Then
                            auditString = auditString & auditRow.Item("DisburseName3") & ",  " & auditRow.Item("DisburseAcc3") & ",  " & auditRow.Item("DisburseRef3") & ",  " & Format(auditRow.Item("DisburseAmt3"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(auditRow.Item("DisburseAmt4")), 0, auditRow.Item("DisburseAmt4")) > 0 Then
                            auditString = auditString & auditRow.Item("DisburseName4") & ",  " & auditRow.Item("DisburseAcc4") & ",  " & auditRow.Item("DisburseRef4") & ",  " & Format(auditRow.Item("DisburseAmt4"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(auditRow.Item("DisburseAmt5")), 0, auditRow.Item("DisburseAmt5")) > 0 Then
                            auditString = auditString & auditRow.Item("DisburseName5") & ",  " & auditRow.Item("DisburseAcc5") & ",  " & auditRow.Item("DisburseRef5") & ",  " & Format(auditRow.Item("DisburseAmt5"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(auditRow.Item("DisburseAmt6")), 0, auditRow.Item("DisburseAmt6")) > 0 Then
                            auditString = auditString & auditRow.Item("DisburseName6") & ",  " & auditRow.Item("DisburseAcc6") & ",  " & auditRow.Item("DisburseRef6") & ",  " & Format(auditRow.Item("DisburseAmt6"), "C") & vbCrLf
                        End If
                        auditString = auditString & "Total amount financed =  " & Format(If(IsDBNull(auditRow.Item("DisburseAmtTotal")), 0, auditRow.Item("DisburseAmtTotal")), "C") & vbCrLf
                        auditString = auditString & "--------------------  Final:" & vbCrLf
                        'Final
                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                            If If(IsDBNull(auditRow.Item("finPowerOpen")), False, auditRow.Item("finPowerOpen")) = True Then
                                auditString = auditString & "FinPower Loan has been opened." & vbCrLf
                                If If(IsDBNull(auditRow.Item("WelcomeLetter")), False, auditRow.Item("WelcomeLetter")) = True Then
                                    auditString = auditString & "Welcome letter has been sent."
                                Else
                                    auditString = auditString & "Welcome letter has NOT been sent."
                                End If
                                If If(IsDBNull(auditRow.Item("ProtectaBook")), False, auditRow.Item("ProtectaBook")) = True Then
                                    auditString = auditString & "                    Protecta books have been sent." & vbCrLf
                                Else
                                    auditString = auditString & "                    Protecta books have NOT been sent." & vbCrLf
                                End If
                            Else
                                auditString = auditString & "FinPower Loan has NOT been opened." & vbCrLf
                            End If
                        Else
                            If If(IsDBNull(auditRow.Item("finPowerWithdrawal")), False, auditRow.Item("finPowerWithdrawal")) = True Then
                                auditString = auditString & "FinPower withdrawal has been completed." & vbCrLf
                            Else
                                auditString = auditString & "FinPower withdrawal has NOT been completed." & vbCrLf
                            End If
                        End If
                    Else 'thisEnquiryType = MyEnums.EnquiryType.FinanceFacility
                        'Documents
                        auditString = auditString & "Documents scanned and/or saved: "
                        auditString = auditString & "IDs - "
                        If If(IsDBNull(auditRow.Item("IDsaved")), False, auditRow.Item("IDsaved")) = True Then
                            auditString = auditString & "Yes"
                        Else
                            auditString = auditString & "No "
                        End If
                        auditString = auditString & "                   Evaluation sheet - "
                        If If(IsDBNull(auditRow.Item("DDAPsaved")), False, auditRow.Item("DDAPsaved")) = True Then
                            auditString = auditString & "Yes"
                        Else
                            auditString = auditString & "No "
                        End If
                        auditString = auditString & "                   Dealer Agreement - "
                        If If(IsDBNull(auditRow.Item("AppSaved")), False, auditRow.Item("AppSaved")) = True Then
                            auditString = auditString & "Yes" & vbCrLf
                        Else
                            auditString = auditString & "No " & vbCrLf
                        End If
                        auditString = auditString & "                                    Deposit slip - "
                        If If(IsDBNull(auditRow.Item("ProofAddress")), False, auditRow.Item("ProofAddress")) = True Then
                            auditString = auditString & "Yes"
                        Else
                            auditString = auditString & "No "
                        End If
                        auditString = auditString & "                   Proof of income - "
                        If If(IsDBNull(auditRow.Item("ProofIncome")), False, auditRow.Item("ProofIncome")) = True Then
                            auditString = auditString & "Yes" & vbCrLf
                        Else
                            auditString = auditString & "No " & vbCrLf
                        End If
                        'insurances
                        auditString = auditString & "Insurances: "
                        If If(IsDBNull(auditRow.Item("SecurityInsured")), False, auditRow.Item("SecurityInsured")) = True Then
                            auditString = auditString & "Indemnity insurance and YFL interest noted" & vbCrLf
                        ElseIf If(IsDBNull(auditRow.Item("NoInsuranceRequired")), False, auditRow.Item("NoInsuranceRequired")) = True Then
                            auditString = auditString & "No insurance is required" & vbCrLf
                        End If
                        'Final
                        If If(IsDBNull(auditRow.Item("finPowerOpen")), False, auditRow.Item("finPowerOpen")) = True Then
                            auditString = auditString & "Dealer has been setup in FinPower." & vbCrLf
                        Else
                            auditString = auditString & "Dealer has NOT been setup in FinPower." & vbCrLf
                        End If
                        If If(IsDBNull(auditRow.Item("WelcomeLetter")), False, auditRow.Item("WelcomeLetter")) = True Then
                            auditString = auditString & "Details have been sent to the Accountant."
                        Else
                            auditString = auditString & "Details have NOT been sent to the Accountant."
                        End If

                    End If
                Else
                    'DueDiligenceBindingSource.Current Is Nothing 
                    auditString = auditString & "There is no Audit information." & vbCrLf
                End If
                'add to constructedString
                If auditString.Length > 0 Then
                    constructedString = constructedString & auditString
                End If
                '******* End of Audit ****************
                '********** Payout *******************
                If Not thisEnquiryType = MyEnums.EnquiryType.FinanceFacility Then
                    Dim payoutString As String = ""
                    payoutString = vbCrLf & "*****  PAYOUT INFORMATION  *******************************************" & vbCrLf
                    Dim payoutTa As EnquiryWorkSheetDataSetTableAdapters.PayoutTableAdapter
                    payoutTa = New EnquiryWorkSheetDataSetTableAdapters.PayoutTableAdapter
                    Dim payoutDt As EnquiryWorkSheetDataSet.PayoutDataTable
                    payoutDt = payoutTa.GetDataByEnquiryId(enquiryId)
                    Dim payoutRow As EnquiryWorkSheetDataSet.PayoutRow
                    If payoutDt.Rows.Count > 0 Then
                        'Get Row in Payout dataset
                        payoutRow = payoutDt.Rows(0)
                        'processing
                        payoutString = payoutString & "--------------------  Processing:" & vbCrLf
                        If If(IsDBNull(payoutRow.Item("TraderLoansLoaded")), False, payoutRow.Item("TraderLoansLoaded")) = True Then
                            payoutString = payoutString & "Loan figures in Audit are correct." & vbCrLf
                        Else
                            payoutString = payoutString & "Loan figures in Audit have not been checked." & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("VerificationStatement")), False, payoutRow.Item("VerificationStatement")) = True Then
                            payoutString = payoutString & "The PPSR Verification Statement has been viewed and is correct." & vbCrLf
                        Else
                            payoutString = payoutString & "The PPSR Verification Statement has NOT been viewed." & vbCrLf
                        End If
                        payoutString = payoutString & "Payout month is " & If(IsDBNull(payoutRow.Item("LoanInMonthOf")), String.Empty, payoutRow.Item("LoanInMonthOf")) & "  " & If(IsDBNull(payoutRow.Item("LoanInYearOf")), String.Empty, payoutRow.Item("LoanInYearOf")) & vbCrLf
                        'payout
                        payoutString = payoutString & "--------------------  Payout:" & vbCrLf
                        If If(IsDBNull(payoutRow.Item("PayAmt1")), 0, payoutRow.Item("PayAmt1")) > 0 Then
                            payoutString = payoutString & payoutRow.Item("PayeeName1") & ",  " & payoutRow.Item("PayeeRef1") & ",  " & Format(payoutRow.Item("PayAmt1"), "C") & ",  " & payoutRow.Item("Cheque1") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("PayAmt2")), 0, payoutRow.Item("PayAmt2")) > 0 Then
                            payoutString = payoutString & payoutRow.Item("PayeeName2") & ",  " & payoutRow.Item("PayeeRef2") & ",  " & Format(payoutRow.Item("PayAmt2"), "C") & ",  " & payoutRow.Item("Cheque2") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("PayAmt3")), 0, payoutRow.Item("PayAmt3")) > 0 Then
                            payoutString = payoutString & payoutRow.Item("PayeeName3") & ",  " & payoutRow.Item("PayeeRef3") & ",  " & Format(payoutRow.Item("PayAmt3"), "C") & ",  " & payoutRow.Item("Cheque3") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("PayAmt4")), 0, payoutRow.Item("PayAmt4")) > 0 Then
                            payoutString = payoutString & payoutRow.Item("PayeeName4") & ",  " & payoutRow.Item("PayeeRef4") & ",  " & Format(payoutRow.Item("PayAmt4"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("PayAmt5")), 0, payoutRow.Item("PayAmt5")) > 0 Then
                            payoutString = payoutString & payoutRow.Item("PayeeName5") & ",  " & payoutRow.Item("PayeeRef5") & ",  " & Format(payoutRow.Item("PayAmt5"), "C") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("PayAmt6")), 0, payoutRow.Item("PayAmt6")) > 0 Then
                            payoutString = payoutString & payoutRow.Item("PayeeName6") & ",  " & payoutRow.Item("PayeeRef6") & ",  " & Format(payoutRow.Item("PayAmt6"), "C") & vbCrLf
                        End If
                        'Dealer commission
                        If If(IsDBNull(payoutRow.Item("CommissionAmt")), 0, payoutRow.Item("CommissionAmt")) > 0 Then
                            payoutString = payoutString & "Dealer commission,  " & ",  " & payoutRow.Item("CommissionRef") & ",  " & Format(payoutRow.Item("CommissionAmt"), "C") & vbCrLf
                        End If
                        'Interest holdback (for Interest free deal)
                        If If(IsDBNull(payoutRow.Item("HoldbackAmt")), 0, payoutRow.Item("HoldbackAmt")) > 0 Then
                            payoutString = payoutString & "Holdback Interest,  " & payoutRow.Item("HoldbackRef") & ",  " & payoutRow.Item("HoldbackAmt") & vbCrLf
                        End If
                        'Loan/Commission retention
                        If If(IsDBNull(payoutRow.Item("RetentionAmt")), 0, payoutRow.Item("RetentionAmt")) > 0 Then
                            payoutString = payoutString & "Loan and Commission Retention,  " & payoutRow.Item("RetentionRef") & ",  " & payoutRow.Item("RetentionAmt") & vbCrLf
                        End If
                        Dim totalPaidOut As Decimal = _
                            If(IsDBNull(payoutRow.Item("PayAmt1")), 0, payoutRow.Item("PayAmt1")) + _
                            If(IsDBNull(payoutRow.Item("PayAmt2")), 0, payoutRow.Item("PayAmt2")) + _
                            If(IsDBNull(payoutRow.Item("PayAmt3")), 0, payoutRow.Item("PayAmt3")) + _
                            If(IsDBNull(payoutRow.Item("PayAmt4")), 0, payoutRow.Item("PayAmt4")) + _
                            If(IsDBNull(payoutRow.Item("PayAmt5")), 0, payoutRow.Item("PayAmt5")) + _
                            If(IsDBNull(payoutRow.Item("PayAmt6")), 0, payoutRow.Item("PayAmt6")) + _
                            If(IsDBNull(payoutRow.Item("CommissionAmt")), 0, payoutRow.Item("CommissionAmt")) + _
                            If(IsDBNull(payoutRow.Item("HoldbackAmt")), 0, payoutRow.Item("HoldbackAmt")) + _
                            If(IsDBNull(payoutRow.Item("RetentionAmt")), 0, payoutRow.Item("RetentionAmt"))
                        payoutString = payoutString & "Total paid out: " & Format(totalPaidOut, "C")
                        If If(IsDBNull(payoutRow.Item("TotalCheque")), False, payoutRow.Item("TotalCheque")) = True Then
                            payoutString = payoutString & "                    Total paid by cheque" & vbCrLf
                        Else
                            payoutString = payoutString & "                    ANZ Batch Number:  " & payoutRow.Item("BatchNum") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("RefinanceAmt1")), 0, payoutRow.Item("RefinanceAmt1")) > 0 Then
                            payoutString = payoutString & "Refinanced Ref:  " & payoutRow.Item("RefinanceRef1") & ",  " & payoutRow.Item("RefinanceAmt1") & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("RefinanceAmt2")), 0, payoutRow.Item("RefinanceAmt2")) > 0 Then
                            payoutString = payoutString & "Refinanced Ref:  " & payoutRow.Item("RefinanceRef2") & ",  " & payoutRow.Item("RefinanceAmt2") & vbCrLf
                        End If
                        Dim totalFinAmt As Decimal = If(IsDBNull(payoutRow.Item("RefinanceAmt1")), 0, payoutRow.Item("RefinanceAmt1")) + If(IsDBNull(payoutRow.Item("RefinanceAmt2")), 0, payoutRow.Item("RefinanceAmt2")) + totalPaidOut
                        payoutString = payoutString & "Total financed amount:  " & Format(totalFinAmt, "C") & vbCrLf
                        If payoutRow.Item("BankProcessed") = True Then
                            payoutString = payoutString & "ANZ processed and/or all cheques issued: Yes                    Date:  " & payoutRow.Item("ProcessDate") & vbCrLf
                        Else
                            payoutString = payoutString & "ANZ processed and/or all cheques issued: No " & vbCrLf
                        End If
                        If payoutRow.Item("CashBook") = True Then
                            payoutString = payoutString & "Cheques and/or Batch entered into Cash Book: Yes" & vbCrLf
                        Else
                            payoutString = payoutString & "Cheques and/or Batch entered into Cash Book: No " & vbCrLf
                        End If
                        If payoutRow.Item("PayAdvice") = True Then
                            payoutString = payoutString & "Pay advice faxed / emailed to Dealer: Yes                    Date:  " & payoutRow.Item("AdviceDate") & vbCrLf
                        Else
                            payoutString = payoutString & "Pay advice faxed / emailed to Dealer: No " & vbCrLf
                        End If
                        If If(IsDBNull(payoutRow.Item("RetentionAmt")), 0, payoutRow.Item("RetentionAmt")) > 0 Then
                            If payoutRow.Item("RetentionEntered") = True Then
                                payoutString = payoutString & "Retention entered into Dealer trust account: Yes" & vbCrLf
                            Else
                                payoutString = payoutString & "Retention entered into Dealer trust account: No " & vbCrLf
                            End If
                        End If

                        'If Not ThisLoanReason = Constants.CONST_PRELIMREASON_SPLIT_LOAN_DRAWDOWN Then
                        If Not thisEnquiryType = MyEnums.EnquiryType.PersonalSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.BusinessSplitLoanDrawdown And Not thisEnquiryType = MyEnums.EnquiryType.UnsecuredPersonalSplitLoanDrawdown Then
                            'refinanced accounts
                            payoutString = payoutString & "--------------------  Refinanced accounts:" & vbCrLf
                            If If(IsDBNull(payoutRow.Item("RefinanceAmt1")), 0, payoutRow.Item("RefinanceAmt1")) > 0 Or If(IsDBNull(payoutRow.Item("RefinanceAmt2")), 0, payoutRow.Item("RefinanceAmt2")) > 0 Then
                                If payoutRow.Item("ClosedFinPower") = True Then
                                    payoutString = payoutString & "Account/s have been closed in finPower" & vbCrLf
                                Else
                                    payoutString = payoutString & "Account/s have been NOT closed in finPower" & vbCrLf
                                End If
                                If payoutRow.Item("PPSRDischarged") = True Or payoutRow.Item("PPSRAmended") = True Then
                                    If payoutRow.Item("PPSRDischarged") = True Then
                                        payoutString = payoutString & "PPSR has been discharged." & vbCrLf
                                    End If
                                    If payoutRow.Item("PPSRAmended") = True Then
                                        payoutString = payoutString & "PPSR has been amended." & vbCrLf
                                    End If
                                Else
                                    payoutString = payoutString & "PPSR has not been processed." & vbCrLf
                                End If
                                If payoutRow.Item("ProtectaClaimBack") = True Then
                                    payoutString = payoutString & "Protecta Claim Back has been processed.          Sheet number - " & payoutRow.Item("ProtectaShtNum") & vbCrLf
                                Else
                                    payoutString = payoutString & "Protecta Claim Back has NOT been processed." & vbCrLf
                                End If
                                If payoutRow.Item("CommRecall") = True Then
                                    payoutString = payoutString & "Commission Recall has been processed.          Sheet number - " & payoutRow.Item("CommShtNum") & vbCrLf
                                Else
                                    payoutString = payoutString & "Commission Recall has NOT been processed." & vbCrLf
                                End If
                            Else
                                payoutString = payoutString & "No Refinance information" & vbCrLf
                            End If
                            'Final
                            payoutString = payoutString & "--------------------  Final:" & vbCrLf
                            If auditPayBookSentSwitch = False Then
                                If payoutRow.Item("DDSafe") = True Then
                                    payoutString = payoutString & "DD filed in safe."
                                Else
                                    payoutString = payoutString & "DD NOT filed in safe."
                                End If
                            End If
                            If payoutRow.Item("ClosedFinPower") = True Then
                                If payoutRow.Item("Archived") = True Then
                                    payoutString = payoutString & "Accounts closed have been filed in the Archives" & vbCrLf
                                Else
                                    payoutString = payoutString & "Accounts closed have NOT been filed in the Archives" & vbCrLf
                                End If
                            End If
                        End If

                    Else 'PayoutBindingSource.Current Is Nothing 
                        payoutString = payoutString & "There is no Payout information." & vbCrLf
                    End If
                    'add to constructedString
                    If payoutString.Length > 0 Then
                        constructedString = constructedString & payoutString
                    End If
                End If

                '******* End of Payout ***************
                '******** Document Info **************
                'get list of documents in workfolder
                'Get String to WorkSheet Drive
                Dim worksheetDrive As String = Switch.GetWorksheetSharedFolder
                Dim fileString As String = String.Empty
                fileString = GetDocumentsList(worksheetDrive, thisEnquiryCode)
                'add to constructedString
                If fileString.Length > 0 Then
                    constructedString = constructedString & vbCrLf & fileString
                End If

                '******End of Document Info ************
                '*********** Printing Info *************
                If printPrintedBy = True Then
                    Dim printInfoString As String
                    printInfoString = vbCrLf & vbCrLf & vbCrLf & "*****  Printed by: " & LoggedinName & vbCrLf & DateTime.Now.ToString
                    'add to constructedString
                    If printInfoString.Length > 0 Then
                        constructedString = constructedString & printInfoString
                    End If
                End If

                '******** End of Printing Info *********
                'MsgBox(constructedString)
                '**************** Return value **************
                ourPrintString.StringOut = constructedString
                ourPrintString.Result = True

            End If 'enquiryDt.Rows.Count

        Catch ex As Exception
            'Display error message
            log.Error("Function ConstructPrintStringAll: " & ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show("Function ConstructPrintStringAll caused an error:" & vbCrLf & ex.Message)
            ourPrintString.Result = False
        End Try

        Return ourPrintString




    End Function


    ''' <summary>
    ''' get list of documents in workfolder
    ''' </summary>
    ''' <param name="thisWorksheetDrive"></param>
    ''' <param name="ourEnquiryCode"></param>
    ''' <returns></returns>
    ''' <remarks>Call this before the file list is empty</remarks>
    Public Shared Function GetDocumentsList(ByVal thisWorksheetDrive As String, ByVal ourEnquiryCode As String, Optional withHeader As Boolean = True) As String
        Dim filePath As String = thisWorksheetDrive & "\" & ourEnquiryCode
        Dim folderExists As Boolean
        folderExists = My.Computer.FileSystem.DirectoryExists(filePath)
        Dim fileString As String = String.Empty
        If folderExists = True Then
            If withHeader = True Then
                fileString = vbCrLf & "*****  DOCUMENTS  ********************************************************" & vbCrLf
            End If
            Dim files As ReadOnlyCollection(Of String)
            files = My.Computer.FileSystem.GetFiles(filePath, FileIO.SearchOption.SearchAllSubDirectories, "*.*")

            For Each fileItem As String In files
                'Compare file path with fileItem and add diff to str
                Dim str As String = ""
                'Dim strExt As String = ""
                Dim finfo As New FileInfo(fileItem)
                Dim txt1(filePath.Split("\").Length) As String
                Dim txt2(fileItem.Split("\").Length) As String
                txt1 = filePath.Split("\")
                txt2 = fileItem.Split("\")
                Dim diff1 As String = ""
                For Each diff As String In txt2
                    If Array.IndexOf(txt1, diff.ToString) = -1 Then 'Searches for the specified object and returns the index of its first occurrence in a one-dimensional array.
                        diff1 += "\" & diff.ToString
                    End If
                Next
                If String.IsNullOrEmpty(diff1) Then
                    str = finfo.Name & finfo.Extension
                Else
                    str = diff1.Substring(1) 'remove leading"\"
                End If
                fileString = fileString & str & vbCrLf
            Next

            Return fileString

        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Archives Enquiry
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <returns></returns>
    ''' <remarks>Checks on days to archive in AppSettings.</remarks>
    Public Shared Async Function NewArchiveEnquiry(ByVal enquiryId As Integer) As Threading.Tasks.Task(Of StringOut)
        Dim archiveDays As Integer
        Dim thisAppsettings As New AppSettings
        archiveDays = CInt(thisAppsettings.DaysBeforeArchive)
        Dim thisCurrentStatusSetDate As Date
        Dim thisCurrentStatus As String
        Dim thisApplicationCode As String
        Dim progressMsg As String = String.Empty
        Dim result As New StringOut
        result.StringOut = String.Empty
        result.Result = False
        'check enquiry exists in Enquiry table
        Dim enquiryTa As EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        enquiryTa = New EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
        Dim enquiryDt As EnquiryWorkSheetDataSet.EnquiryDataTable
        enquiryDt = enquiryTa.GetDataByEnquiryId(enquiryId)
        Dim enquiryRow As EnquiryWorkSheetDataSet.EnquiryRow
        If enquiryDt.Rows.Count = 0 Then
            'no rows
            'MessageBox.Show("Enquiry does not exist", "Archive Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            result.StringOut = "Archiving error! Enquiry does not exist."
            result.Result = False
            Return result
        ElseIf enquiryDt.Rows.Count > 0 Then
            enquiryRow = enquiryDt.Rows(0)
            thisCurrentStatusSetDate = enquiryRow.CurrentStatusSetDate
            thisCurrentStatus = enquiryRow.CurrentStatus
            thisApplicationCode = enquiryRow.ApplicationCode
            'test CurrentStatusSetDate is x days old Or Completed
            Dim dD As Long = DateDiff(DateInterval.Day, thisCurrentStatusSetDate, Date.Now)
            If dD >= archiveDays Or thisCurrentStatus = Constants.CONST_CURRENT_STATUS_COMPLETED Then
                'OK to carryon, get user confirmation
                Dim msg As String
                Dim title As String
                Dim response As MsgBoxResult
                msg = "Do you want to Archive this Enquiry?"
                title = "Archive Enquiry?"
                response = MessageBox.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                If response = MsgBoxResult.Yes Then   'User choose Yes.
                    Dim commentId As Integer
                    Try
                        'Perform operations
                        'construct print string
                        Dim enquiryPrint As String
                        Dim enquiryPrintOut As Util.StringOut = Util.GetPrintStringAll(enquiryId)

                        If enquiryPrintOut.Result Then
                            enquiryPrint = enquiryPrintOut.StringOut
                        Else
                            result.StringOut = "Error creating Print String" & vbCrLf & "Aborting Archive process!"
                            result.Result = False
                            Return result
                        End If
                        'Check is possible to Change Remote Status
                        Dim systemCanChangeRemoteStatus As Boolean
                        systemCanChangeRemoteStatus = thisAppsettings.SystemCanChangeRemoteStatus
                        If systemCanChangeRemoteStatus = True Then
                            'update OAC
                            Dim onlineApplicationService As New OnlineApplication
                            Dim retTTResult As New TrackResult
                            Dim actionType As Integer = OACWebService.ApplicationActionType.Archive
                            retTTResult = Await onlineApplicationService.SetStatus(thisApplicationCode, actionType, String.Empty)
                            If retTTResult.Status = False Then 'error condition
                                'Add comment
                                commentId = Util.SetComment(enquiryId, "System", retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage)
                                'set dialogbox
                                msg = retTTResult.StatusMessage & vbCrLf & retTTResult.ErrorMessage & vbCrLf & vbCrLf & "Remote (OAC) Archiving has failed, do you wish to continue with TrueTrack archiving?" & vbCrLf & vbCrLf & "If you do not know what to do, select No."
                                Dim diagResult As Integer = MessageBox.Show(msg, "Remote (OAC) Archiving Error", MessageBoxButtons.YesNo)
                                If diagResult = DialogResult.No Then
                                    commentId = Util.SetComment(enquiryId, LoggedinName, "Remote (OAC) Archiving error. Archiving aborted!")
                                    result.StringOut = "Remote (OAC) Archiving error. Archiving aborted!"
                                    result.Result = False
                                    Return result
                                ElseIf diagResult = DialogResult.Yes Then
                                    'set local archived
                                    Try
                                        enquiryRow.IsArchived = True
                                        enquiryTa.Update(enquiryDt)
                                        commentId = Util.SetComment(enquiryId, LoggedinName, "Remote (OAC) Archiving error. TrueTrack archiving completed.")
                                        result.StringOut = "Remote (OAC) Archiving error. TrueTrack archiving completed."
                                        result.Result = True
                                        Return result
                                    Catch ex As Exception
                                        log.Error("Remote (OAC) Archiving error. TrueTrack archiving error: " & ex.Message & " : " & ex.TargetSite.ToString)
                                        commentId = Util.SetComment(enquiryId, LoggedinName, "Remote (OAC) Archiving error. TrueTrack archiving error!")
                                        result.StringOut = "Archiving TrueTrack" & vbCrLf & ex.Message
                                        result.Result = False
                                        Return result
                                    End Try
                                End If
                            Else 'Remote (OAC) Archiving completed OK
                                'set local archived
                                Try
                                    enquiryRow.IsArchived = True
                                    enquiryTa.Update(enquiryDt)
                                    commentId = Util.SetComment(enquiryId, LoggedinName, "Remote (OAC) Archiving completed. TrueTrack archiving completed.")
                                    result.StringOut = "Remote (OAC) Archiving completed. TrueTrack archiving completed"
                                    result.Result = True
                                    Return result
                                Catch ex As Exception
                                    log.Error("Remote (OAC) Archiving completed. TrueTrack archiving error: " & ex.Message & " : " & ex.TargetSite.ToString)
                                    commentId = Util.SetComment(enquiryId, LoggedinName, "Remote (OAC) Archiving completed. TrueTrack archiving error!")
                                    result.StringOut = "Archiving TrueTrack" & vbCrLf & ex.Message
                                    result.Result = False
                                    Return result
                                End Try
                            End If
                        Else
                            'no remote archiving allowed, set local archived
                            Try
                                enquiryRow.IsArchived = True
                                enquiryTa.Update(enquiryDt)
                                commentId = Util.SetComment(enquiryId, LoggedinName, "TrueTrack archiving completed.")
                                result.StringOut = "TrueTrack archiving completed."
                                result.Result = True
                                Return result
                            Catch ex As Exception
                                log.Error("TrueTrack archiving error: " & ex.Message & " : " & ex.TargetSite.ToString)
                                commentId = Util.SetComment(enquiryId, LoggedinName, "TrueTrack archiving error!")
                                result.StringOut = "Archiving TrueTrack" & vbCrLf & ex.Message
                                result.Result = False
                                Return result
                            End Try
                        End If

                    Catch ex As Exception
                        log.Error("Archiving TrueTrack: " & ex.Message & " : " & ex.TargetSite.ToString)
                        MsgBox(ex.Message)
                        result.StringOut = "Archiving TrueTrack" & vbCrLf & ex.Message
                        result.Result = False
                        Return result
                    End Try

                Else
                    ' User choose No.
                    result.StringOut = "Archiving TrueTrack Cancelled"
                    result.Result = False
                    Return result
                End If
            Else
                'Not enough time since last Status set to Archive
                result.StringOut = "Enquiry Status is not " & archiveDays.ToString & " days old yet." & vbCrLf & "Try again later"
                result.Result = False
                Return result
            End If

        End If
        result.StringOut = "Something went wrong with the archiving!"
        result.Result = False
        Return result

    End Function

End Class
