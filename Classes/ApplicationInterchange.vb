﻿Imports System.IO

Public Class ApplicationInterchange
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    ''' <summary>
    ''' Returns Xml file name as XML_{ApplicationCode}.xml
    ''' </summary>
    ''' <param name="applicationCode">Application code of the Enquiry</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetXMLFileName(applicationCode As String) As String
        Return "XML_" & applicationCode & ".xml"

    End Function

    Public Shared Function CheckXMLFileExists(enquiryCode As String, applicationCode As String) As Boolean
        Dim inputXmlPath As String
        'Get the absolute path of the XML file
        inputXmlPath = ApplicationInterchange.GetWorkSheetFolder(enquiryCode) & GetXMLFileName(applicationCode)
        If System.IO.File.Exists(inputXmlPath) Then
            Return True
        End If
        Return False
    End Function


    ''' <summary>
    ''' Checks Online Application Centre web service is up and running
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>
    '''</remarks>
    Public Function PingOnlineApplicationService() As TrackResult
        Dim _retResult As TrackResult
        Dim _url As String

        'OAC Web service url from settings
        If My.Settings.IsTest = True Then
            _url = My.Settings.MembersCentreWebService_TEST
        Else
            _url = My.Settings.MembersCentreWebService_PROD
        End If

        _retResult = PingWebUrl(_url)
        _retResult.StatusMessage = "Web service not available"
        Return _retResult
    End Function


    ''' <summary>
    ''' Checks a website is up and running
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>
    '''Source: Jack Eagles1
    '''Website: http://www.dreamincode.net/forums/topic/137663-check-if-url-exists/
    '''</remarks>
    Public Function PingWebUrl(url As String) As TrackResult
        Dim _retResult As New TrackResult
        _retResult.Status = False
        _retResult.ResultIntegerValue = 0 ' Ping result
        Dim req As System.Net.WebRequest
        Dim _url As New System.Uri(url)
        'make a call to web service
        req = System.Net.WebRequest.Create(_url)
        Dim resp As System.Net.WebResponse
        Try
            resp = req.GetResponse()
            _retResult.Status = If(DirectCast(resp, System.Net.HttpWebResponse).StatusCode = 200, True, False)
            resp.Close()
            req = Nothing
            _retResult.StatusMessage = "Webservice found"
            _retResult.ResultIntegerValue = 1
        Catch ex As Exception
            req = Nothing
            _retResult.Status = False
            _retResult.StatusMessage = "Webservice not found"
            _retResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try
        Return _retResult
    End Function



    ''' <summary>
    ''' pings the remote server
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PingRemoteServer(serverName As String) As TrackResult
        Dim _retResult As New TrackResult
        Dim _pingResult As Integer = 0

        _retResult.Status = False
        _retResult.ResultIntegerValue = 0 ' Ping result


        Try

            If My.Computer.Network.Ping(serverName) Then
                _retResult.Status = True
                _retResult.ErrorMessage = "Status: Server pinged successfully."
                _retResult.ResultIntegerValue = 1

            Else
                _retResult.Status = False
                _retResult.ErrorMessage = "Status: Ping request timed out."
                _retResult.ResultIntegerValue = 2
            End If
        Catch ex As Net.NetworkInformation.PingException
            _retResult.ResultIntegerValue = -1
            _retResult.Status = False
            _retResult.ErrorMessage = "PingException: URL was not valid." & vbCrLf & ex.Message
            log.Error("PingException: URL was not valid." & ex.Message & " : " & ex.TargetSite.ToString)
        Catch ex As InvalidOperationException
            _retResult.ResultIntegerValue = -1
            _retResult.Status = False
            _retResult.ErrorMessage = "InvalidOperationException: No network connection is available." & vbCrLf & ex.Message
            log.Error("InvalidOperationException: No network connection is available." & ex.Message & " : " & ex.TargetSite.ToString)
        Catch ex As Exception
            _retResult.ResultIntegerValue = -1
            _retResult.Status = False
            _retResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try
        If _retResult.ResultIntegerValue = 0 Then
            _retResult.Status = False
            _retResult.ErrorMessage = "Error in Ping.  Status:  Ready"
        End If

        Return _retResult


    End Function

    ''' <summary>
    ''' Returns the WorkSheet shared folder
    ''' </summary>
    ''' <param name="enquiryCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWorkSheetFolder(enquiryCode As String) As String

        Dim enquiryFolder As String
        enquiryFolder = Switch.GetWorksheetSharedFolder & ""

        If enquiryFolder.Length > 0 Then

            If Not enquiryFolder.Substring(enquiryFolder.Length - 1, 1) = "\" Then
                enquiryFolder += "\"
            End If
        End If

        '==========================================================================================DEV BEGIN
        'returns a local folder for testing as configured in the appsettings
        enquiryFolder = ApplicationInterchange.DevelopmentFolder(enquiryFolder)
        '==========================================================================================DEV END

        Return enquiryFolder & enquiryCode & "\"

    End Function

    Public Shared Function CheckEnquiryFolderExists(enquiryCode As String) As TrackResult
        Dim TTResult As New TrackResult
        Dim enquiryFolder As String

        Try
            enquiryFolder = ApplicationInterchange.GetWorkSheetFolder(enquiryCode)
            If Not Directory.Exists(enquiryFolder) Then
                Directory.CreateDirectory(enquiryFolder)
            End If
            TTResult.ResultStringValue = enquiryFolder
            TTResult.Status = True
        Catch ex As Exception
            TTResult.Status = False
            TTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return TTResult

    End Function

    ''' <summary>
    ''' Checks folder exists and creates folder if not exists
    ''' </summary>
    ''' <param name="folderName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckFolderExists(folderName) As TrackResult
        Dim TTResult As New TrackResult
        Try
            If (Not System.IO.Directory.Exists(folderName)) Then
                System.IO.Directory.CreateDirectory(folderName)
            End If
            TTResult.Status = True
        Catch ex As Exception
            TTResult.Status = False
            TTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try


        Return TTResult

    End Function


    Public Function GetfinPOWERImportFilePath() As String
        Dim _importFileFolder As String
        _importFileFolder = Switch.GetFinPowerImportFolder & ""
        If _importFileFolder.Length > 0 Then
            If Not _importFileFolder.Substring(_importFileFolder.Length - 1, 1) = "\" Then
                _importFileFolder += "\"
            End If
        End If

        '=======================================================================================DEV BEGIN
        'returns a local folder for testing as configured in the appsettings
        _importFileFolder = ApplicationInterchange.DevelopmentFolder(_importFileFolder)
        '=======================================================================================DEV END

        Return _importFileFolder

    End Function

    ''' <summary>
    ''' Development folder for testing purpose
    ''' </summary>
    ''' <param name="enquiryFolder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DevelopmentFolder(enquiryFolder As String) As String

        '==========================================================================================DEV BEGIN
        If My.Settings.IsDevelopment And My.Settings.IsTest Then ' DEVELOPMENT TEST CODE comment code
            'Function DevelopmentFolder: Comment the line below ' Added for testing
            enquiryFolder = My.Settings.DevEnquiryFolder & ""
        End If
        '==========================================================================================DEV END

        Return enquiryFolder
    End Function


    ''' <summary>
    ''' Saves string to file
    ''' </summary>
    ''' <param name="fileContent"></param>
    ''' <param name="filename"></param>''' 
    ''' <param name="folderPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SaveFile(fileContent As String, filename As String, folderPath As String) As TrackResult
        Dim _retResult As New TrackResult
        Dim _CheckFolderResult As New TrackResult
        _retResult.Status = False
        _retResult.ErrorMessage = ""

        Dim saveFileTo As String = folderPath & filename
        'Check for valid fileName and path
        If Not (String.IsNullOrWhiteSpace(filename) Or String.IsNullOrWhiteSpace(folderPath)) Then
            'Check the last character of folder and append \
            If folderPath.Length > 0 Then
                If Not folderPath.Substring(folderPath.Length - 1, 1) = "\" Then
                    folderPath += "\"
                End If
            End If
            Try
                _CheckFolderResult = CheckFolderExists(folderPath)
                If _CheckFolderResult IsNot Nothing And _CheckFolderResult.Status = True Then
                    saveFileTo = folderPath & filename
                    'Creates a new file, writes the specified string to the file, and then closes the file. 
                    'If the target file already exists, it is overwritten.
                    System.IO.File.WriteAllText(saveFileTo, fileContent)
                End If
                _retResult.Status = True
                _retResult.ErrorMessage = ""
            Catch ex As Exception
                _retResult.Status = False
                _retResult.ErrorMessage = ex.Message
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            End Try
        Else
            _retResult.Status = False
            _retResult.ErrorMessage = "Incorrect file path"
        End If

        Return _retResult

    End Function


    Public Function ReadFile(filename As String, folderPath As String) As TrackResult
        Dim retResult As New TrackResult
        retResult.Status = False
        retResult.ErrorMessage = ""

        Dim readFilePath As String = folderPath & filename
        'Check for valid fileName and path
        If Not (String.IsNullOrWhiteSpace(filename) Or String.IsNullOrWhiteSpace(folderPath)) Then
            'Check the last character of folder and append \
            If folderPath.Length > 0 Then
                If Not folderPath.Substring(folderPath.Length - 1, 1) = "\" Then
                    folderPath += "\"
                End If
            End If
            Try
                readFilePath = folderPath & filename
                Dim objReader As New System.IO.StreamReader(readFilePath)
                'read the file
                retResult.ResultStringValue = objReader.ReadToEnd
                retResult.Status = True
                retResult.ErrorMessage = ""
            Catch ex As Exception
                retResult.Status = False
                retResult.ErrorMessage = ex.Message
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            End Try
        Else
            retResult.Status = False
            retResult.ErrorMessage = "Incorrect file path"
        End If

        Return retResult

    End Function


End Class
