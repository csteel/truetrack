﻿Imports System.Data.SqlClient

''' <summary>
''' Class interfaces with OAC Web service
''' </summary>
''' <remarks></remarks>
Public Class OnlineApplication : Inherits ApplicationInterchange
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    ''' <summary>
    ''' Get Loan Code from OAC Web Service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLoanCode(applicationCode As String) As TrackResult
        Dim _OACServiceClient As OACWebService.DataServiceClient
        Dim _loanCodeResponse As OACWebService.LoanCodeResponse
        Dim _TTResult As New TrackResult
        Dim _statusDefaultErrorMessage As String = "Error while generating Loan code."
        _TTResult.Status = False
        Try
            _OACServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If

            _loanCodeResponse = _OACServiceClient.GetLoanCode(AuthToken, applicationCode)
            If _loanCodeResponse IsNot Nothing Then
                'LoanCode
                If _loanCodeResponse.LoanCode > 0 Then _TTResult.ResultStringValue = _loanCodeResponse.LoanCode
                If Not String.IsNullOrWhiteSpace(_TTResult.ResultStringValue) Then
                    'set status
                    _TTResult.Status = True
                End If
                'get error message and show as alert
                _TTResult.ErrorMessage = _loanCodeResponse.ErrorMsg
            End If
        Catch Endpointex As System.ServiceModel.EndpointNotFoundException
            _TTResult.StatusMessage = _statusDefaultErrorMessage
            _TTResult.ErrorMessage = Endpointex.Message
        Catch ex As Exception
            _TTResult.StatusMessage = _statusDefaultErrorMessage
            _TTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return _TTResult

    End Function

    ''' <summary>
    ''' Check Application Code
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Function CheckApplicationCode(applicationCode As String) As TrackResult
        Dim _OACServiceClient As OACWebService.DataServiceClient
        Dim _applicationCodeResponse As OACWebService.ApplicationCodeResponse
        Dim _TTResult As New TrackResult
        _TTResult.Status = False
        Try
            _OACServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            _applicationCodeResponse = _OACServiceClient.CheckApplicationCode(AuthToken, applicationCode)
            If _applicationCodeResponse IsNot Nothing Then
                _TTResult.ResultBooleanValue = _applicationCodeResponse.Exists
                If String.IsNullOrWhiteSpace(_applicationCodeResponse.ServiceError) Then
                    _TTResult.Status = True
                End If
                _TTResult.ErrorMessage = _applicationCodeResponse.ServiceError
            End If
        Catch Endpointex As System.ServiceModel.EndpointNotFoundException
            _TTResult.ErrorMessage = Endpointex.Message
        Catch ex As Exception
            _TTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return _TTResult

    End Function

    ''' <summary>
    ''' Makes call to OAC web service to get application HTML file from OAC
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Function getApplicationFile(applicationCode As String) As TrackResult

        Dim _OACServiceClient As OACWebService.DataServiceClient
        Dim _applicationExportResponse As OACWebService.ApplicationExportResponse
        Dim _TTResult As New TrackResult
        _TTResult.Status = False
        Try
            _OACServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If

            'get Html template
            _applicationExportResponse = _OACServiceClient.GetHtmlExport(AuthToken, applicationCode)
            'if response from OAC Service
            If _applicationExportResponse IsNot Nothing Then
                _TTResult.ResultStringValue = _applicationExportResponse.Data
                If _applicationExportResponse.Exists = True And String.IsNullOrWhiteSpace(_applicationExportResponse.ServiceError) Then
                    _TTResult.Status = True
                End If
                If _applicationExportResponse.Exists = False Then
                    _TTResult.ErrorMessage = "Application does not exits in OAC"
                Else
                    _TTResult.ErrorMessage = _applicationExportResponse.ServiceError
                End If
            End If


        Catch ex As Exception
            _TTResult.ErrorMessage = ex.Message
            _TTResult.StatusMessage = "Error while retrieving application file"
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return _TTResult

    End Function

    ''' <summary>
    ''' Get Xml data from OAC
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Function GetXmlFile(applicationCode As String) As TrackResult
        Dim _OACServiceClient As OACWebService.DataServiceClient
        Dim _applicationXmlResponse As OACWebService.ApplicationExportResponse
        Dim _TTResult As New TrackResult
        _TTResult.Status = "False"
        Try
            _OACServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                _OACServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If

            'get Xml template
            _applicationXmlResponse = _OACServiceClient.GetXmlExport(AuthToken, applicationCode)
            'if response from OAC Service
            If _applicationXmlResponse IsNot Nothing Then
                _TTResult.ResultStringValue = _applicationXmlResponse.Data
                If _applicationXmlResponse.Exists = True And String.IsNullOrWhiteSpace(_applicationXmlResponse.ServiceError) Then
                    _TTResult.Status = "True"
                End If
                If _applicationXmlResponse.Exists = False Then
                    If _applicationXmlResponse.Error = True And Not String.IsNullOrWhiteSpace(_applicationXmlResponse.ErrorMsg) Then
                        _TTResult.ErrorMessage = _applicationXmlResponse.ErrorMsg
                    Else
                        _TTResult.ErrorMessage = "Application does not exits or not executed in OAC"
                    End If

                Else
                    _TTResult.ErrorMessage = _applicationXmlResponse.ServiceError
                End If
            End If

        Catch ex As Exception
            _TTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return _TTResult

    End Function

    ''' <summary>
    ''' Set the Status in the OAC to Documents Executed
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Function SetStatusDocsExecuted(applicationCode As String) As TrackResult
        Dim oacServiceClient As OACWebService.DataServiceClient
        Dim applicationStatusResponse As OACWebService.BaseResponse
        Dim tTResult As New TrackResult
        tTResult.Status = False 'set to error condition
        Try
            oacServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            applicationStatusResponse = oacServiceClient.SetDocumentsExecuted(AuthToken, applicationCode, LoggedinName)
            'Process applicationStatusResponse
            If applicationStatusResponse IsNot Nothing Then
                tTResult.ResultBooleanValue = False
                If String.IsNullOrWhiteSpace(applicationStatusResponse.ServiceError) Then
                    'no service error
                    If String.IsNullOrWhiteSpace(applicationStatusResponse.TokenError) Then
                        'no Token error
                        If applicationStatusResponse.Error Then
                            'there is a process error  (ErrorCode = 3)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                            'so get error msg
                            tTResult.ErrorMessage = applicationStatusResponse.ErrorMsg
                            'set status message
                            tTResult.StatusMessage = "OAC process error"
                        Else
                            'no errors (ErrorCode = 0)
                            tTResult.Status = True
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.None
                            tTResult.ResultBooleanValue = True 'must be the case as this is a base response
                        End If
                    Else
                        'is a Token error  (ErrorCode = 2)
                        tTResult.ErrorCode = MyEnums.ResponseErrorCode.TokenError
                        tTResult.StatusMessage = "OAC Token error"
                        tTResult.ErrorMessage = applicationStatusResponse.TokenError
                    End If
                Else
                    'is a service error (ErrorCode = 1)
                    tTResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                    tTResult.StatusMessage = "OAC service error"
                    tTResult.ErrorMessage = applicationStatusResponse.ServiceError
                End If
            Else
                'there is no applicationStatusResponse
                tTResult.StatusMessage = "No response recieved from Web Service"
                tTResult.ErrorMessage = "No response recieved from Web Service"
            End If
        Catch endpointex As System.ServiceModel.EndpointNotFoundException
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = endpointex.Message
        Catch ex As Exception
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return tTResult

    End Function

    ''' <summary>
    ''' Reset the Status in the OAC to WaitingForDocs
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks>OACWebService method which has the signature; public BaseResponse ResetToWaitingForDocuments(string token, string applicationCode, string userName). It has validation such that; Usual stuff like token, user and application code validation, If it is already in Waiting for Documents – it does nothing and returns success, If it is not in a status that is Waiting For Documents or later – then it returns an error. If all OK it switches the application to Waiting For Documents and adds an entry to the application log for a “Reset”</remarks>
    Public Async Function SetStatusWaitingForDocs(applicationCode As String) As Threading.Tasks.Task(Of TrackResult)
        Dim oacServiceClient As OACWebService.DataServiceClient
        Dim applicationStatusResponse As OACWebService.BaseResponse
        Dim tTResult As New TrackResult
        tTResult.Status = False 'set to error condition
        Try
            oacServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            applicationStatusResponse = Await oacServiceClient.ResetToWaitingForDocumentsAsync(AuthToken, applicationCode, LoggedinName)
            'Process applicationStatusResponse
            If applicationStatusResponse IsNot Nothing Then
                tTResult.ResultBooleanValue = False
                If String.IsNullOrWhiteSpace(applicationStatusResponse.ServiceError) Then
                    'no service error
                    If String.IsNullOrWhiteSpace(applicationStatusResponse.TokenError) Then
                        'no Token error
                        If applicationStatusResponse.Error Then
                            'there is a process error  (ErrorCode = 3)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                            'so get error msg
                            tTResult.ErrorMessage = applicationStatusResponse.ErrorMsg
                            'set status message
                            tTResult.StatusMessage = "OAC process error"
                        Else
                            'no errors (ErrorCode = 0)
                            tTResult.Status = True
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.None
                            tTResult.ResultBooleanValue = True 'must be the case as this is a base response
                        End If
                    Else
                        'is a Token error  (ErrorCode = 2)
                        tTResult.ErrorCode = MyEnums.ResponseErrorCode.TokenError
                        tTResult.StatusMessage = "OAC Token error"
                        tTResult.ErrorMessage = applicationStatusResponse.TokenError
                    End If
                Else
                    'is a service error (ErrorCode = 1)
                    tTResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                    tTResult.StatusMessage = "OAC service error"
                    tTResult.ErrorMessage = applicationStatusResponse.ServiceError
                End If
            Else
                'there is no applicationStatusResponse
                tTResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                tTResult.StatusMessage = "No response recieved from Web Service"
                tTResult.ErrorMessage = "No response recieved from Web Service"
            End If
        Catch endpointex As System.ServiceModel.EndpointNotFoundException
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = endpointex.Message
        Catch ex As Exception
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return tTResult

    End Function

    ''' <summary>
    ''' Get list of Customer details from OAC
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <param name="filterPartnership"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks>Option to filter out Partnerships</remarks>
    Public Function GetCustomerList(applicationCode As String, Optional ByVal filterPartnership As Boolean = False) As OacCustomersResponse
        Dim oacServiceClient As OACWebService.DataServiceClient
        Dim applicationCustListResponse As OACWebService.ApplicationCustomerListResponse
        'Dim applicationCustResponse As OACWebService.ApplicationCustomerResponse
        Dim tTResult As New OacCustomersResponse
        tTResult.Exists = False 'set to error condition
        Try
            oacServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            applicationCustListResponse = oacServiceClient.GetCustomerList(AuthToken, applicationCode)
            'Process applicationStatusResponse
            If applicationCustListResponse IsNot Nothing Then
                If applicationCustListResponse.Exists Then
                    Dim custList As New List(Of OacCustomer)
                    For Each cust As OACWebService.ApplicationCustomerResponse In applicationCustListResponse.Customer_List
                        Dim appCust As New OacCustomer
                        'filter out persons acting, not used in TrueTrack
                        If Not cust.Type = Constants.CONST_OAC_CUSTAPPTYPE_PERSONACTING Then
                            'filter out partnerships
                            If filterPartnership = True Then
                                If Not cust.CustomerType = Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP Then
                                    appCust.XRefId = cust.CustomerId
                                    appCust.Customertype = cust.CustomerType 'individual, company, trust, sole trader(for main only).
                                    appCust.Title = cust.Title
                                    appCust.FirstName = cust.FirstName
                                    appCust.MiddleName = cust.MiddleNames
                                    appCust.Lastname = cust.LastName
                                    appCust.BusinessName = cust.BusinessName
                                    appCust.CompanyNum = cust.CompanyNumber
                                    appCust.Type = cust.Type 'main, guarantor, co-main, personActing
                                    custList.Add(appCust)
                                End If
                            Else
                                appCust.XRefId = cust.CustomerId
                                appCust.Customertype = cust.CustomerType 'individual, company, trust, partnership(for main only), sole trader(for main only).
                                appCust.Title = cust.Title
                                appCust.FirstName = cust.FirstName
                                appCust.MiddleName = cust.MiddleNames
                                appCust.Lastname = cust.LastName
                                appCust.BusinessName = cust.BusinessName
                                appCust.CompanyNum = cust.CompanyNumber
                                appCust.Type = cust.Type 'main, guarantor, co-main, personActing
                                custList.Add(appCust)
                            End If
                        End If
                    Next
                    tTResult.CustomerList = custList
                    tTResult.ApplicationCode = applicationCustListResponse.ApplicationCode
                    tTResult.Exists = True
                Else
                    tTResult.StatusMessage = "OAC service"
                    tTResult.ErrorMessage = "No OAC customers found"
                End If
            End If
        Catch endpointex As System.ServiceModel.EndpointNotFoundException
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = endpointex.Message
        Catch ex As Exception
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return tTResult

    End Function

    ''' <summary>
    ''' Get the Status of OAC Application
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>OacAppStatusResponse</returns>
    ''' <remarks></remarks>
    Public Async Function GetOACApplicationStatus(ByVal applicationCode As String) As Threading.Tasks.Task(Of OacAppStatusResponse)
        Dim oacServiceClient As OACWebService.DataServiceClient
        Dim applicationStatusResponse As OACWebService.ApplicationStatusResponse
        Dim tTResult As New OacAppStatusResponse
        tTResult.Exists = False 'set to error condition
        Try
            oacServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            applicationStatusResponse = Await oacServiceClient.GetApplicationStatusAsync(AuthToken, applicationCode)
            'Process applicationStatusResponse
            If applicationStatusResponse IsNot Nothing Then
                tTResult.Exists = True


                If String.IsNullOrWhiteSpace(applicationStatusResponse.ServiceError) Then
                    'no service error
                    If String.IsNullOrWhiteSpace(applicationStatusResponse.TokenError) Then
                        'no Token error
                        If applicationStatusResponse.Error Then
                            'there is a process error  (ErrorCode = 3)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                            'so get error msg
                            tTResult.ErrorMessage = applicationStatusResponse.ErrorMsg
                            'set status message
                            tTResult.StatusMessage = "OAC process error"
                        Else
                            'no errors (ErrorCode = 0)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.None
                            'do stuff here
                            tTResult.ApplicationStatus = applicationStatusResponse.ApplicationStatus
                            tTResult.IsArchived = applicationStatusResponse.IsArchived
                        End If
                    Else
                        'is a Token error  (ErrorCode = 2)
                        tTResult.ErrorCode = MyEnums.ResponseErrorCode.TokenError
                        tTResult.StatusMessage = "OAC Token error"
                        tTResult.ErrorMessage = applicationStatusResponse.TokenError
                    End If
                Else
                    'is a service error (ErrorCode = 1)
                    tTResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                    tTResult.StatusMessage = "OAC service error"
                    tTResult.ErrorMessage = applicationStatusResponse.ServiceError
                End If
            Else
                tTResult.Exists = False
                tTResult.StatusMessage = "OAC Response is nothing"
                tTResult.ErrorMessage = "No Application Status found"
            End If

        Catch endpointex As System.ServiceModel.EndpointNotFoundException
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = endpointex.Message
        Catch ex As Exception
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return tTResult

    End Function

    ''' <summary>
    ''' Compare TrueTrack Status with OAC Status
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Async Function CompareStatus(ByVal applicationCode As String, ByVal currentStatus As String, ByVal isArchived As Boolean) As Threading.Tasks.Task(Of TrackResult)
        'Check is possible to GetLoanSummary in AppSettings
        Dim thisTrackResult As New TrackResult
        Dim systemCanGetLoanSummary As Boolean
        Dim thisAppsettings As New AppSettings
        Dim oacStatus As OACWebService.ApplicationStatusType
        Dim errorMsg As String = "TrueTrack and the OAC are out of sync," & vbNewLine & "please take steps to synchronise them." & vbCrLf & "The OAC is in status '{0}'"
        systemCanGetLoanSummary = thisAppsettings.SystemCanGetLoanSummary
        If systemCanGetLoanSummary = True Then
            'Get values form the OAC
            Dim retStatusResult As New OacAppStatusResponse
            'Dim OnlineApplicationService As New OnlineApplication
            Try
                retStatusResult = Await GetOACApplicationStatus(applicationCode)
            Catch ex As Exception
                log.Error(ex.Message & " : " & ex.TargetSite.ToString)
                MessageBox.Show(ex.Message, "Get OAC application status", MessageBoxButtons.OK, MessageBoxIcon.Error)
                retStatusResult.Exists = False
            End Try

            If retStatusResult.Exists = False Then
                'MessageBox.Show(retStatusResult.ErrorMessage, retStatusResult.StatusMessage)
                thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                thisTrackResult.StatusMessage = retStatusResult.StatusMessage
                thisTrackResult.ErrorMessage = retStatusResult.ErrorMessage
            Else
                If retStatusResult.ErrorCode > 0 Then
                    'error returned
                    'MessageBox.Show(retStatusResult.ErrorMessage, retStatusResult.StatusMessage)
                    thisTrackResult.ErrorCode = retStatusResult.ErrorCode
                    thisTrackResult.StatusMessage = retStatusResult.StatusMessage
                    thisTrackResult.ErrorMessage = retStatusResult.ErrorMessage
                Else
                    'no error returned, do stuff
                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                    'check if archived
                    If retStatusResult.IsArchived = True Then
                        If isArchived = True Then
                            thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                        Else
                            thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                            thisTrackResult.ErrorMessage = "TrueTrack and the OAC are out of sync," & vbNewLine & "please take steps to synchronise them." & vbCrLf & "The OAC application is archived!"
                        End If
                    ElseIf isArchived = True Then
                        thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                        thisTrackResult.ErrorMessage = "TrueTrack and the OAC are out of sync," & vbNewLine & "please take steps to synchronise them." & vbCrLf & "The OAC application is NOT archived!"
                    Else
                        'compare Statuses
                        oacStatus = retStatusResult.ApplicationStatus
                        Select Case currentStatus
                            Case Constants.CONST_CURRENT_STATUS_DUE_DILIGENCE
                                If oacStatus = OACWebService.ApplicationStatusType.Incomplete Or oacStatus = OACWebService.ApplicationStatusType.Submitted Or oacStatus = OACWebService.ApplicationStatusType.Saved Or oacStatus = OACWebService.ApplicationStatusType.Approved Or oacStatus = OACWebService.ApplicationStatusType.Referred Or oacStatus = OACWebService.ApplicationStatusType.Assessing Or oacStatus = OACWebService.ApplicationStatusType.WaitingForDocs Or oacStatus = OACWebService.ApplicationStatusType.DueDiligence Or oacStatus = OACWebService.ApplicationStatusType.OnHold Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If

                            Case Constants.CONST_CURRENT_STATUS_CAD_APPROVED
                                If oacStatus = OACWebService.ApplicationStatusType.DueDiligence Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If

                            Case Constants.CONST_CURRENT_STATUS_AUDIT
                                If oacStatus = OACWebService.ApplicationStatusType.Audit Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If

                            Case Constants.CONST_CURRENT_STATUS_PAYOUT
                                If oacStatus = OACWebService.ApplicationStatusType.Payout Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If
                            Case Constants.CONST_CURRENT_STATUS_COMPLETED
                                If oacStatus = OACWebService.ApplicationStatusType.Completed Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If
                            Case Constants.CONST_CURRENT_STATUS_DECLINED
                                If oacStatus = OACWebService.ApplicationStatusType.Declined Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If

                            Case Constants.CONST_CURRENT_STATUS_WITHDRAWN
                                If oacStatus = OACWebService.ApplicationStatusType.Withdrawn Then
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                                Else
                                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                    thisTrackResult.ErrorMessage = String.Format(errorMsg, oacStatus)
                                End If

                            Case Else
                                thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                        End Select

                        If thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None Then
                            If My.Settings.IsTest = True Then
                                thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                                thisTrackResult.ErrorMessage = String.Format("The OAC is in status '{0}'", oacStatus)
                            End If
                        End If

                    End If
                End If
            End If
        Else
            thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None

        End If
        Return thisTrackResult

    End Function

    ''' <summary>
    ''' Get OAC application status
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Async Function GetOacStatus(ByVal applicationCode As String) As Threading.Tasks.Task(Of TrackResult)
        'Check is possible to GetLoanSummary in AppSettings
        Dim thisTrackResult As New TrackResult
        Dim thisAppsettings As New AppSettings
        Dim oacStatus As String = String.Empty
        'Get values form the OAC
        Dim retStatusResult As New OacAppStatusResponse
        Try
            retStatusResult = Await GetOACApplicationStatus(applicationCode)
        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show(ex.Message, "Get OAC application status", MessageBoxButtons.OK, MessageBoxIcon.Error)
            retStatusResult.Exists = False
        End Try

        If retStatusResult.Exists = False Then
            thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
            thisTrackResult.StatusMessage = retStatusResult.StatusMessage
            thisTrackResult.ErrorMessage = retStatusResult.ErrorMessage
        Else
            If retStatusResult.ErrorCode > 0 Then
                'error returned
                thisTrackResult.ErrorCode = retStatusResult.ErrorCode
                thisTrackResult.StatusMessage = retStatusResult.StatusMessage
                thisTrackResult.ErrorMessage = retStatusResult.ErrorMessage
            Else
                'no error returned, do stuff
                thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.None
                'check if archived
                If retStatusResult.IsArchived = True Then
                    thisTrackResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                    thisTrackResult.StatusMessage = "OAC Archived check failed!"
                    thisTrackResult.ErrorMessage = "The OAC application is archived!"
                    thisTrackResult.ResultIntegerValue = retStatusResult.ApplicationStatus
                Else
                    'Return the OAC Status
                    thisTrackResult.ResultIntegerValue = retStatusResult.ApplicationStatus

                End If
            End If
        End If

        Return thisTrackResult




    End Function

    ''' <summary>
    ''' Generate authentication token used to validate client request(s).
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property AuthToken() As String
        Get
            Dim shard1 As String = (DateTime.Now.Day * DateTime.Now.Month * 89) + (97 * DateTime.Now.Day)
            Dim shard2 As Integer = (DateTime.Now.Day * DateTime.Now.Month * 139) - (101 * DateTime.Now.Day)
            Dim shard3 As Integer = (DateTime.Now.Day * DateTime.Now.Month * 193) + (103 * DateTime.Now.Day)
            Dim shard4 As Integer = (DateTime.Now.Day * DateTime.Now.Month * 251) - (107 * DateTime.Now.Day)
            Dim shard5 As Integer = (DateTime.Now.Day * DateTime.Now.Month * 311) + (109 * DateTime.Now.Day)
            Dim key As String = String.Join(":", shard1, shard2, shard3, shard4, shard5)
            Return Util.EncodeTo64(key)
        End Get

    End Property

    ''' <summary>
    ''' Validate authentication token.
    ''' </summary>
    ''' <param name="token">Token to validate</param>
    ''' <returns>Flag to indicate whether token is valid.</returns>
    ''' <remarks></remarks>
    Private Function ValidateToken(token As String) As Boolean
        Return (AuthToken = token)
    End Function

    ''' <summary>
    ''' Get Customers for an Enquiry: Customer.Id_number, Name, Type, CustomerType, CustomerId, EnquiryId, ClientTypeId, ClientNum, XRefId
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <returns>DataTable</returns>
    ''' <remarks>DataTable of Enquiry Customers using EnquiryCustomersTableAdapter</remarks>
    Public Function GetEnquiryCustomers(ByVal enquiryId As Integer) As DataTable
        '========================
        'Added 13/09/2016
        '========================
        Dim enqCustTableAdapter As EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter
        enqCustTableAdapter = New EnquiryWorkSheetDataSetTableAdapters.EnquiryCustomersTableAdapter()
        Dim enquiryCustomersTable As EnquiryWorkSheetDataSet.EnquiryCustomersDataTable
        enquiryCustomersTable = enqCustTableAdapter.GetDataByEnquiryId(enquiryId)

        Return enquiryCustomersTable
    End Function

    ''' <summary>
    ''' Get TrueTrack EnquiryCustomer data
    ''' </summary>
    ''' <returns>tTResult</returns>
    ''' <remarks>Reads customer datatable row, adds to (Collection). Returns Collection of OacCustomer</remarks>
    Public Function TTCustomerInfo(ByVal enquiryId As Integer) As TtCustomersResponse
        Dim tTResult As New TtCustomersResponse
        tTResult.Exists = False
        Dim dt As DataTable
        Try
            dt = GetEnquiryCustomers(enquiryId)
            Dim custList As New List(Of TtCustomer)
            'load into appCust
            For Each row As DataRow In dt.Rows
                Dim appCust As New TtCustomer
                appCust.CustomerId = If(row.Item("CustomerId") Is DBNull.Value, "", row.Item("CustomerId"))
                appCust.XRefId = If(row.Item("XRefId") Is DBNull.Value, "", row.Item("XRefId"))
                appCust.Customertype = If(row.Item("CustomerType") Is DBNull.Value, 0, row.Item("CustomerType")) 'individual, company, trust, partnership(for main only),sole trader(for main only).
                appCust.Name = If(row.Item("Name") Is DBNull.Value, "", row.Item("Name"))
                'appCust.CompanyNum = ?
                appCust.Type = If(row.Item("Type") Is DBNull.Value, 0, row.Item("Type")) 'main, guarantor, co-main
                custList.Add(appCust)
            Next
            tTResult.CustomerList = custList
            tTResult.Exists = True

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            tTResult.StatusMessage = "Error retrieving customers"
            tTResult.ErrorMessage = ex.Message
        End Try

        Return tTResult
    End Function
    ''' <summary>
    ''' FillByNewCustomer on New CustomerTableAdapter
    ''' </summary>
    ''' <param name="enquiryId"></param>
    ''' <param name="oacCustList"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>For each OacCustomer OacCustomerList: FillByNewCustomer on New CustomerTableAdapter then update CustomerTableAdapter</remarks>
    Public Function InsertOacCustomers(ByVal enquiryId As Integer, oacCustList As List(Of OacCustomer)) As Boolean
        Dim customerEnquiryType As Integer
        Dim customerTableAdapter As New EnquiryWorkSheetDataSetTableAdapters.CustomerTableAdapter
        Dim customerDt As New EnquiryWorkSheetDataSet.CustomerDataTable
        'Dim customerBindingSource As New BindingSource
        'customerBindingSource.DataSource = customerDt
        Dim customerRow As EnquiryWorkSheetDataSet.CustomerRow
        'Dim loadedCustomerId As Integer
        Try
            For Each oacCust As OacCustomer In oacCustList
                Select Case oacCust.Type 'TrueTrack does not use PersonActing
                    Case "main", "Main"
                        customerEnquiryType = MyEnums.CustomerEnquiryType.Main
                    Case "co-main", "Co-main", "comain", "coMain", "Comain", "CoMain", "Joint", "joint"
                        customerEnquiryType = MyEnums.CustomerEnquiryType.CoMain
                    Case "guarantor", "Guarantor"
                        customerEnquiryType = MyEnums.CustomerEnquiryType.Guarantor
                End Select

                Select Case oacCust.Customertype
                    Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL 'MyEnums.CustomerType.Individual
                        customerTableAdapter.FillByNewCustomer(customerDt, enquiryId, MyEnums.CustomerType.Individual, customerEnquiryType)
                        'loadedCustomerId = customerDt.Rows(customerBindingSource.Position()).Item("CustomerId")
                        customerRow = customerDt(0)
                        customerRow.BeginEdit()
                        customerRow.Title = oacCust.Title
                        customerRow.FirstName = oacCust.FirstName
                        customerRow.MiddleNames = oacCust.MiddleName
                        customerRow.LastName = oacCust.Lastname
                        customerRow.Salutation = oacCust.Title & " " & oacCust.FirstName.Substring(0, 1) & " " & oacCust.Lastname
                        customerRow.XRefId = CStr(oacCust.XRefId)
                        customerRow.EndEdit()
                        customerTableAdapter.Update(customerDt)
                    Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY 'MyEnums.CustomerType.Company
                        customerTableAdapter.FillByNewCustomer(customerDt, enquiryId, MyEnums.CustomerType.Company, customerEnquiryType)
                        'loadedCustomerId = customerDt.Rows(customerBindingSource.Position()).Item("CustomerId")
                        customerRow = customerDt(0)
                        customerRow.BeginEdit()
                        customerRow.LegalName = oacCust.BusinessName
                        customerRow.XRefId = CStr(oacCust.XRefId)
                        customerRow.EndEdit()
                        customerTableAdapter.Update(customerDt)
                    Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST 'MyEnums.CustomerType.Trust
                        customerTableAdapter.FillByNewCustomer(customerDt, enquiryId, MyEnums.CustomerType.Trust, customerEnquiryType)
                        'loadedCustomerId = customerDt.Rows(customerBindingSource.Position()).Item("CustomerId")
                        customerRow = customerDt(0)
                        customerRow.BeginEdit()
                        customerRow.TradingName = oacCust.BusinessName
                        customerRow.XRefId = CStr(oacCust.XRefId)
                        customerRow.EndEdit()
                        customerTableAdapter.Update(customerDt)
                    Case Constants.CONST_OAC_CUSTOMERTYPE_SOLETRADER
                        customerTableAdapter.FillByNewCustomer(customerDt, enquiryId, MyEnums.CustomerType.SoleTrader, customerEnquiryType)
                        'loadedCustomerId = customerDt.Rows(customerBindingSource.Position()).Item("CustomerId")
                        customerRow = customerDt(0)
                        customerRow.BeginEdit()
                        customerRow.Title = oacCust.Title
                        customerRow.FirstName = oacCust.FirstName
                        customerRow.MiddleNames = oacCust.MiddleName
                        customerRow.LastName = oacCust.Lastname
                        customerRow.Salutation = oacCust.Title & " " & oacCust.FirstName.Substring(0, 1) & " " & oacCust.Lastname
                        customerRow.XRefId = CStr(oacCust.XRefId)
                        customerRow.EndEdit()
                        customerTableAdapter.Update(customerDt)
                    Case Else

                End Select
                customerDt.Clear()
            Next

            Return True

        Catch ex As Exception
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
            MsgBox(ex.Message)
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Update XRefId in Customer table
    ''' </summary>
    ''' <param name="customerId"></param>
    ''' <param name="xRefId"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Updates Customer table with XRefId</remarks>
    Public Function UpdateCustXRefId(ByVal customerId As Integer, ByVal xRefId As String) As Boolean
        Dim param As SqlParameter

        Using mycon As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)
            mycon.Open()

            Dim myCommand As New SqlCommand("InsertXRefId", mycon)
            If xRefId Is Nothing Then
                xRefId = ""
            End If

            myCommand.Parameters.AddWithValue("@CustomerId ", customerId)
            param = New SqlParameter("@XRefId", SqlDbType.VarChar)
            param.Value = xRefId
            myCommand.Parameters.Add(param)

            myCommand.CommandType = CommandType.StoredProcedure
            myCommand.ExecuteNonQuery()


        End Using

        Return True

    End Function


    ''' <summary>
    ''' Gets the Loan Summary figures for the Audit from OAC webservice
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <returns>OacLoanSummaryResponse</returns>
    ''' <remarks>Uses OACWebService.DataServiceClient.GetLoanSummary</remarks>
    Public Function GetLoanSummary(ByVal applicationCode As String) As OacLoanSummaryResponse
        Dim oacServiceClient As OACWebService.DataServiceClient
        Dim loanSummaryResponse As OACWebService.LoanSummaryResponse
        Dim tTResult As New OacLoanSummaryResponse
        tTResult.Exists = False 'set to error condition
        Try
            oacServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            loanSummaryResponse = oacServiceClient.GetLoanSummary(AuthToken, applicationCode)
            'Process loanSummaryResponse
            If loanSummaryResponse IsNot Nothing Then
                tTResult.Exists = True
                If String.IsNullOrWhiteSpace(loanSummaryResponse.ServiceError) Then
                    'no service error
                    If String.IsNullOrWhiteSpace(loanSummaryResponse.TokenError) Then
                        'no Token error
                        If loanSummaryResponse.Error Then
                            'there is a process error  (ErrorCode = 3)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                            'so get error msg
                            tTResult.ErrorMessage = loanSummaryResponse.ErrorMsg
                            'set status message
                            tTResult.StatusMessage = "OAC process error"
                        Else
                            'no errors (ErrorCode = 0)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.None
                            'do stuff here
                            tTResult.CashPrice = loanSummaryResponse.CashPrice
                            tTResult.OtherFundsAdvanced = loanSummaryResponse.OtherFundsAdvanced
                            tTResult.CashDeposit = loanSummaryResponse.CashDeposit
                            tTResult.Brokerage = loanSummaryResponse.Brokerage
                            tTResult.TradeIn = loanSummaryResponse.TradeInAllowed
                            tTResult.LoanRetentionRate = loanSummaryResponse.LoanRetentionRate
                            tTResult.YFLFees = loanSummaryResponse.TotalFees
                            tTResult.YFLInsurances = loanSummaryResponse.TotalInsurance
                            If loanSummaryResponse.IsMasterDealer = True Then
                                tTResult.ProductMargin = 0
                                tTResult.InterestCommission = 0
                                tTResult.CommissionRetentionRate = 0
                            Else
                                tTResult.ProductMargin = loanSummaryResponse.ProductMargin
                                tTResult.InterestCommission = loanSummaryResponse.DealerInterestCommission
                                tTResult.CommissionRetentionRate = loanSummaryResponse.CommisionRetentionRate
                            End If

                        End If
                    Else
                        'is a Token error  (ErrorCode = 2)
                        tTResult.ErrorCode = MyEnums.ResponseErrorCode.TokenError
                        tTResult.StatusMessage = "OAC Token error"
                        tTResult.ErrorMessage = loanSummaryResponse.TokenError
                    End If
                Else
                    'is a service error (ErrorCode = 1)
                    tTResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                    tTResult.StatusMessage = "OAC service error"
                    tTResult.ErrorMessage = loanSummaryResponse.ServiceError
                End If
            Else
                tTResult.Exists = False
                tTResult.StatusMessage = "OAC Response is nothing"
                tTResult.ErrorMessage = "No Loan Summary found"
            End If

        Catch endpointex As System.ServiceModel.EndpointNotFoundException
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = endpointex.Message
        Catch ex As Exception
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)
        End Try

        Return tTResult
    End Function

    ''' <summary>
    ''' Set the OAC Status
    ''' </summary>
    ''' <param name="applicationCode"></param>
    ''' <param name="actionType"></param>
    ''' <param name="note"></param>
    ''' <returns>TrackResult</returns>
    ''' <remarks></remarks>
    Public Async Function SetStatus(ByVal applicationCode As String, ByVal actionType As OACWebService.ApplicationActionType, ByVal note As String) As Threading.Tasks.Task(Of TrackResult)

        Dim oacServiceClient As OACWebService.DataServiceClient
        Dim applicationStatusResponse As OACWebService.BaseResponse
        Dim tTResult As New TrackResult
        tTResult.Status = False 'set to error condition
        Try
            oacServiceClient = New OACWebService.DataServiceClient
            If My.Settings.IsTest = True Then
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_TEST)
            Else
                oacServiceClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(My.Settings.MembersCentreWebService_PROD)
            End If
            applicationStatusResponse = Await oacServiceClient.SetApplicationStatusAsync(AuthToken, applicationCode, LoggedinName, actionType, note)
            'Process applicationStatusResponse
            If applicationStatusResponse IsNot Nothing Then
                tTResult.ResultBooleanValue = False
                If String.IsNullOrWhiteSpace(applicationStatusResponse.ServiceError) Then
                    'no service error
                    If String.IsNullOrWhiteSpace(applicationStatusResponse.TokenError) Then
                        'no Token error
                        If applicationStatusResponse.Error Then
                            'there is a process error  (ErrorCode = 3)
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.ProcessError
                            'so get error msg
                            tTResult.ErrorMessage = "There has been an error in the OAC." & vbNewLine & applicationStatusResponse.ErrorMsg & vbNewLine & "This process has been aborted. Please contact your administrator."
                            'set status message
                            tTResult.StatusMessage = "OAC process error"
                        Else
                            'no errors (ErrorCode = 0)
                            tTResult.Status = True
                            tTResult.ErrorCode = MyEnums.ResponseErrorCode.None
                            tTResult.ResultBooleanValue = True 'must be the case as this is a base response
                        End If
                    Else
                        'is a Token error  (ErrorCode = 2)
                        tTResult.ErrorCode = MyEnums.ResponseErrorCode.TokenError
                        tTResult.StatusMessage = "OAC Token error"
                        tTResult.ErrorMessage = applicationStatusResponse.TokenError
                    End If
                Else
                    'is a service error (ErrorCode = 1)
                    tTResult.ErrorCode = MyEnums.ResponseErrorCode.ServiceError
                    tTResult.StatusMessage = "OAC service error"
                    tTResult.ErrorMessage = applicationStatusResponse.ServiceError
                End If
            Else
                'there is no applicationStatusResponse
                tTResult.StatusMessage = "No response recieved from Web Service"
                tTResult.ErrorMessage = "No response recieved from Web Service"
            End If

        Catch endpointex As System.ServiceModel.EndpointNotFoundException
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = endpointex.Message
        Catch ex As Exception
            tTResult.StatusMessage = "OAC service Error"
            tTResult.ErrorMessage = ex.Message
            log.Error(ex.Message & " : " & ex.TargetSite.ToString)

        End Try

        Return tTResult

    End Function

End Class


Public Class TrackResult
    Private _ResultStringValue As String
    Private _ResultBooleanValue As Boolean
    Private _ResultDecimalValue As Decimal
    Private _ResultIntegerValue As Integer
    Private _ErrorCode As Integer
    Private _ErrorMessage As String
    Private _Status As Boolean
    Private _StatusMessage As String

    Public Sub New()
        StatusMessage = String.Empty
        ErrorMessage = String.Empty
        ResultStringValue = String.Empty
    End Sub


    Public Property StatusMessage() As String
        Get
            Return _StatusMessage
        End Get
        Set(value As String)
            _StatusMessage = value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(value As String)
            _Status = value
        End Set
    End Property


    Public Property ResultBooleanValue() As Boolean
        Get
            Return _ResultBooleanValue
        End Get
        Set(value As Boolean)
            _ResultBooleanValue = value
        End Set
    End Property

    Public Property ResultDecimalValue() As Decimal
        Get
            Return _ResultDecimalValue
        End Get
        Set(value As Decimal)
            _ResultDecimalValue = value
        End Set
    End Property

    Public Property ResultIntegerValue() As Integer
        Get
            Return _ResultIntegerValue
        End Get
        Set(value As Integer)
            _ResultIntegerValue = value
        End Set
    End Property

    Public Property ResultStringValue() As String
        Get
            Return _ResultStringValue
        End Get
        Set(value As String)
            _ResultStringValue = value
        End Set
    End Property

    Public Property ErrorCode() As Integer
        Get
            Return _ErrorCode
        End Get
        Set(value As Integer)
            _ErrorCode = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return _ErrorMessage
        End Get
        Set(value As String)
            _ErrorMessage = value
        End Set
    End Property
End Class

''' <summary>
''' Object for OAC Customer
''' </summary>
''' <remarks>Overloads Function Equals</remarks>
Public Class OacCustomer
    Implements IEquatable(Of OacCustomer)

    Private mXRefId As Integer
    Public Property XRefId() As Integer
        Get
            Return mXRefId
        End Get
        Set(ByVal value As Integer)
            mXRefId = value
        End Set
    End Property

    Private mCustomerType As String
    Public Property Customertype() As String
        Get
            Return mCustomerType
        End Get
        Set(ByVal value As String)
            mCustomerType = value
        End Set
    End Property

    Private mTitle As String
    Public Property Title() As String
        Get
            Return mTitle
        End Get
        Set(ByVal value As String)
            mTitle = value
        End Set
    End Property

    Private mFirstName As String
    Public Property FirstName() As String
        Get
            Return mFirstName
        End Get
        Set(ByVal value As String)
            mFirstName = value
        End Set
    End Property

    Private mMiddleName As String
    Public Property MiddleName() As String
        Get
            Return mMiddleName
        End Get
        Set(ByVal value As String)
            mMiddleName = value
        End Set
    End Property

    Private mLastName As String
    Public Property Lastname() As String
        Get
            Return mLastName
        End Get
        Set(ByVal value As String)
            mLastName = value
        End Set
    End Property

    Private mBusinessName As String
    Public Property BusinessName() As String
        Get
            Return mBusinessName
        End Get
        Set(ByVal value As String)
            mBusinessName = value
        End Set
    End Property


    Private mCompanyNum As String
    Public Property CompanyNum() As String
        Get
            Return mCompanyNum
        End Get
        Set(ByVal value As String)
            mCompanyNum = value
        End Set
    End Property


    Private mType As String
    Public Property Type() As String
        Get
            Return mType
        End Get
        Set(ByVal value As String)
            mType = value
        End Set
    End Property

    Public Overloads Function Equals(other As OacCustomer) As Boolean Implements IEquatable(Of OacCustomer).Equals
        If other Is Nothing Then
            Return False
        End If
        Return (Me.XRefId.Equals(other.XRefId))
    End Function


End Class

''' <summary>
''' Object to contain list of OacCustomer
''' </summary>
''' <remarks></remarks>
Public Class OacCustomersResponse

    Private mCustomerList As List(Of OacCustomer)
    Public Property CustomerList() As List(Of OacCustomer)
        Get
            Return mCustomerList
        End Get
        Set(ByVal value As List(Of OacCustomer))
            mCustomerList = value
        End Set
    End Property

    Private mExists As Boolean
    Public Property Exists() As Boolean
        Get
            Return mExists
        End Get
        Set(ByVal value As Boolean)
            mExists = value
        End Set
    End Property

    Private mApplicationCode As String
    Public Property ApplicationCode() As String
        Get
            Return mApplicationCode
        End Get
        Set(ByVal value As String)
            mApplicationCode = value
        End Set
    End Property

    Private mErrorMessage As String
    Public Property ErrorMessage() As String
        Get
            Return mErrorMessage
        End Get
        Set(ByVal value As String)
            mErrorMessage = value
        End Set
    End Property

    Private mStatusMessage As String
    Public Property StatusMessage() As String
        Get
            Return mStatusMessage
        End Get
        Set(ByVal value As String)
            mStatusMessage = value
        End Set
    End Property

End Class

''' <summary>
''' Object for TrueTrack Customer
''' </summary>
''' <remarks>Overloads Function Equals</remarks>
Public Class TtCustomer
    Implements IEquatable(Of TtCustomer)

    Private mCustomerId As Integer
    Public Property CustomerId() As Integer
        Get
            Return mCustomerId
        End Get
        Set(ByVal value As Integer)
            mCustomerId = value
        End Set
    End Property


    Private mXRefId As String
    Public Property XRefId() As String
        Get
            Return mXRefId
        End Get
        Set(ByVal value As String)
            mXRefId = value
        End Set
    End Property

    Private mCustomerType As Integer
    Public Property Customertype() As Integer
        Get
            Return mCustomerType
        End Get
        Set(ByVal value As Integer)
            mCustomerType = value
        End Set
    End Property

    Private mName As String
    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property


    Private mCompanyNum As String
    Public Property CompanyNum() As String
        Get
            Return mCompanyNum
        End Get
        Set(ByVal value As String)
            mCompanyNum = value
        End Set
    End Property


    Private mType As Integer
    Public Property Type() As Integer
        Get
            Return mType
        End Get
        Set(ByVal value As Integer)
            mType = value
        End Set
    End Property

    Public Overloads Function Equals(other As TtCustomer) As Boolean Implements IEquatable(Of TtCustomer).Equals
        If other Is Nothing Then
            Return False
        End If
        Return (Me.CustomerId.Equals(other.CustomerId))
    End Function

End Class

''' <summary>
''' Object to contain List of TtCustomer
''' </summary>
''' <remarks></remarks>
Public Class TtCustomersResponse

    Private mCustomerList As List(Of TtCustomer)
    Public Property CustomerList() As List(Of TtCustomer)
        Get
            Return mCustomerList
        End Get
        Set(ByVal value As List(Of TtCustomer))
            mCustomerList = value
        End Set
    End Property

    Private mExists As Boolean
    Public Property Exists() As Boolean
        Get
            Return mExists
        End Get
        Set(ByVal value As Boolean)
            mExists = value
        End Set
    End Property

    Private mErrorMessage As String
    Public Property ErrorMessage() As String
        Get
            Return mErrorMessage
        End Get
        Set(ByVal value As String)
            mErrorMessage = value
        End Set
    End Property

    Private mStatusMessage As String
    Public Property StatusMessage() As String
        Get
            Return mStatusMessage
        End Get
        Set(ByVal value As String)
            mStatusMessage = value
        End Set
    End Property

End Class

''' <summary>
''' Object to contain Loan Summary
''' </summary>
''' <remarks></remarks>
Public Class OacLoanSummaryResponse

    Private mCashPrice As Decimal
    Public Property CashPrice() As Decimal
        Get
            Return mCashPrice
        End Get
        Set(ByVal value As Decimal)
            mCashPrice = value
        End Set
    End Property

    Private mOtherFundsAdvanced As Decimal
    Public Property OtherFundsAdvanced() As Decimal
        Get
            Return mOtherFundsAdvanced
        End Get
        Set(ByVal value As Decimal)
            mOtherFundsAdvanced = value
        End Set
    End Property

    Private mCashDeposit As Decimal
    Public Property CashDeposit() As Decimal
        Get
            Return mCashDeposit
        End Get
        Set(ByVal value As Decimal)
            mCashDeposit = value
        End Set
    End Property

    Private mBrokerage As Decimal
    Public Property Brokerage() As Decimal
        Get
            Return mBrokerage
        End Get
        Set(ByVal value As Decimal)
            mBrokerage = value
        End Set
    End Property


    Private mTradeIn As Decimal
    Public Property TradeIn() As Decimal
        Get
            Return mTradeIn
        End Get
        Set(ByVal value As Decimal)
            mTradeIn = value
        End Set
    End Property

    Private mLoanRetentionRate As Decimal
    Public Property LoanRetentionRate() As Decimal
        Get
            Return mLoanRetentionRate
        End Get
        Set(ByVal value As Decimal)
            mLoanRetentionRate = value
        End Set
    End Property

    Private mYFLFees As Decimal
    Public Property YFLFees() As Decimal
        Get
            Return mYFLFees
        End Get
        Set(ByVal value As Decimal)
            mYFLFees = value
        End Set
    End Property

    Private mYFLInsurances As Decimal
    Public Property YFLInsurances() As Decimal
        Get
            Return mYFLInsurances
        End Get
        Set(ByVal value As Decimal)
            mYFLInsurances = value
        End Set
    End Property

    Private mProductMargin As Decimal
    Public Property ProductMargin() As Decimal
        Get
            Return mProductMargin
        End Get
        Set(ByVal value As Decimal)
            mProductMargin = value
        End Set
    End Property

    Private mInterestCommission As Decimal
    Public Property InterestCommission() As Decimal
        Get
            Return mInterestCommission
        End Get
        Set(ByVal value As Decimal)
            mInterestCommission = value
        End Set
    End Property

    Private mCommissionRetentionRate As Decimal
    Public Property CommissionRetentionRate() As Decimal
        Get
            Return mCommissionRetentionRate
        End Get
        Set(ByVal value As Decimal)
            mCommissionRetentionRate = value
        End Set
    End Property

    Private mExists As Boolean
    Public Property Exists() As Boolean
        Get
            Return mExists
        End Get
        Set(ByVal value As Boolean)
            mExists = value
        End Set
    End Property

    Private mErrorMessage As String
    Public Property ErrorMessage() As String
        Get
            Return mErrorMessage
        End Get
        Set(ByVal value As String)
            mErrorMessage = value
        End Set
    End Property

    Private mStatusMessage As String
    Public Property StatusMessage() As String
        Get
            Return mStatusMessage
        End Get
        Set(ByVal value As String)
            mStatusMessage = value
        End Set
    End Property

    Private mErrorCode As Integer

    Public Property ErrorCode() As Integer
        Get
            Return mErrorCode
        End Get
        Set(value As Integer)
            mErrorCode = value
        End Set
    End Property


End Class


Public Class OacAppStatusResponse
    Private _ApplicationCode As String
    Private _ApplicationStatus As Integer
    Private _Error As Boolean
    Private _ErrorCode As Integer
    Private _ErrorMessage As String
    Private _IsArchived As Boolean
    Private _ServiceError As String
    Private _TokenError As String
    '================================
    Private _StatusMessage As String
    Private _Exists As Boolean

    Public Sub New()
        StatusMessage = String.Empty
        ErrorMessage = String.Empty
        ApplicationStatus = 0
    End Sub


    Public Property StatusMessage() As String
        Get
            Return _StatusMessage
        End Get
        Set(value As String)
            _StatusMessage = value
        End Set
    End Property
    Public Property ApplicationStatus() As Integer
        Get
            Return _ApplicationStatus
        End Get
        Set(value As Integer)
            _ApplicationStatus = value
        End Set
    End Property


    Public Property ApplicationCode() As String
        Get
            Return _ApplicationCode
        End Get
        Set(value As String)
            _ApplicationCode = value
        End Set
    End Property

    Public Property Exists() As Boolean
        Get
            Return _Exists
        End Get
        Set(value As Boolean)
            _Exists = value
        End Set
    End Property

    Public Property [Error]() As Boolean
        Get
            Return _Error
        End Get
        Set(value As Boolean)
            _Error = value
        End Set
    End Property

    Public Property ErrorCode() As Integer
        Get
            Return _ErrorCode
        End Get
        Set(value As Integer)
            _ErrorCode = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return _ErrorMessage
        End Get
        Set(value As String)
            _ErrorMessage = value
        End Set
    End Property

    Public Property IsArchived() As Boolean
        Get
            Return _IsArchived
        End Get
        Set(value As Boolean)
            _IsArchived = value
        End Set
    End Property

    Public Property ServiceError() As String
        Get
            Return _ServiceError
        End Get
        Set(value As String)
            _ServiceError = value
        End Set
    End Property

    Public Property TokenError() As String
        Get
            Return _TokenError
        End Get
        Set(value As String)
            _TokenError = value
        End Set
    End Property

End Class