﻿Imports System.Data.SqlClient
''' <summary>
''' AppSettings from database
''' </summary>
''' <remarks></remarks>
Public Class AppSettings

    Sub New()
        'Get info from database
        'Dim dt As DataTable = GetSettingData()
        Dim dt As DataTable = GetTableData()

        'Interate through datatable and populate list (Decimal, Text, Boolean)
        For Each row As DataRow In dt.Rows
            If row.Item("Name") = "DependSingle" Then
                mDependSingle = If(IsDBNull(row.Item("Decimal")), 0, row.Item("Decimal"))
            ElseIf row.Item("Name") = "DependCouple" Then
                mDependCouple = If(IsDBNull(row.Item("Decimal")), 0, row.Item("Decimal"))
            ElseIf row.Item("Name") = "DependChild" Then
                mDependChild = If(IsDBNull(row.Item("Decimal")), 0, row.Item("Decimal"))
            ElseIf row.Item("Name") = "SystemCanImportCustomers" Then
                mSystemCanImportCustomers = If(IsDBNull(row.Item("Boolean")), False, row.Item("Boolean"))
            ElseIf row.Item("Name") = "SystemCanGetLoanSummary" Then
                mSystemCanGetLoanSummary = If(IsDBNull(row.Item("Boolean")), False, row.Item("Boolean"))
            ElseIf row.Item("Name") = "SystemCanChangeRemoteStatus" Then
                mSystemCanChangeRemoteStatus = If(IsDBNull(row.Item("Boolean")), False, row.Item("Boolean"))
            ElseIf row.Item("Name") = "minLoanValueSecurityInsured" Then
                mMinLoanValueSecurityInsured = If(IsDBNull(row.Item("Decimal")), 0, row.Item("Decimal"))
            ElseIf row.Item("Name") = "SystemBusinessName" Then
                mSystemBusinessName = If(IsDBNull(row.Item("Text")), String.Empty, row.Item("Text"))
            ElseIf row.Item("Name") = "SystemBusinessPhone" Then
                mSystemBusinessPhone = If(IsDBNull(row.Item("Text")), String.Empty, row.Item("Text"))
            ElseIf row.Item("Name") = "SystemBusinessFax" Then
                mSystemBusinessFax = If(IsDBNull(row.Item("Text")), String.Empty, row.Item("Text"))
            ElseIf row.Item("Name") = "SystemBusinessWebsite" Then
                mSystemBusinessWebsite = If(IsDBNull(row.Item("Text")), String.Empty, row.Item("Text"))
            ElseIf row.Item("Name") = "DaysBeforeArchive" Then
                mDaysBeforeArchive = If(IsDBNull(row.Item("Decimal")), 0, row.Item("Decimal"))
            End If

        Next
    End Sub

    Private mSystemBusinessWebsite As String
    Public ReadOnly Property SystemBusinessWebsite() As String
        Get
            Return mSystemBusinessWebsite
        End Get
        'Set(ByVal value As String)
        '    mSystemBusinessWebsite = value
        'End Set
    End Property

    Private mSystemBusinessFax As String
    Public ReadOnly Property SystemBusinessFax() As String
        Get
            Return mSystemBusinessFax
        End Get
        'Set(ByVal value As String)
        '    mSystemBusinessFax = value
        'End Set
    End Property

    Private mSystemBusinessPhone As String
    Public ReadOnly Property SystemBusinessPhone() As String
        Get
            Return mSystemBusinessPhone
        End Get
        'Set(ByVal value As String)
        '    mSystemBusinessPhone = value
        'End Set
    End Property


    Private mSystemBusinessName As String
    Public ReadOnly Property SystemBusinessName() As String
        Get
            Return mSystemBusinessName
        End Get
        'Set(ByVal value As String)
        '    mSystemBusinessName = value
        'End Set
    End Property


    Private mDependSingle As Decimal
    Public ReadOnly Property DependSingle() As Decimal
        Get
            Return mDependSingle
        End Get
        'Set(ByVal value As Decimal)
        '    _DependSingle = value
        'End Set
    End Property


    Private mDependCouple As Decimal
    Public ReadOnly Property DependCouple() As Decimal
        Get
            Return mDependCouple
        End Get
        'Set(ByVal value As String)
        '    mDependCouple = value
        'End Set
    End Property

    Private mDependChild As Decimal
    Public ReadOnly Property DependChild() As Decimal
        Get
            Return mDependChild
        End Get
        'Set(ByVal value As String)
        '    mDependChild = value
        'End Set
    End Property

    Private mSystemCanImportCustomers As Boolean
    Public ReadOnly Property SystemCanImportCustomers() As Boolean
        Get
            Return mSystemCanImportCustomers
        End Get
        'Set(ByVal value As Boolean)
        '    mSystemCanImportCustomers = value
        'End Set
    End Property

    Private mSystemCanGetLoanSummary As Boolean
    Public ReadOnly Property SystemCanGetLoanSummary() As Boolean
        Get
            Return mSystemCanGetLoanSummary
        End Get

    End Property


    Private mSystemCanChangeRemoteStatus As Boolean
    Public ReadOnly Property SystemCanChangeRemoteStatus() As Boolean
        Get
            Return mSystemCanChangeRemoteStatus
        End Get

    End Property

    Private mMinLoanValueSecurityInsured As Decimal
    Public ReadOnly Property MinLoanValueSecurityInsured() As Decimal
        Get
            Return mMinLoanValueSecurityInsured
        End Get
        'Set(ByVal value As Decimal)
        '    mMinLoanValueSecurityInsured = value
        'End Set
    End Property

    Private mDaysBeforeArchive As Decimal
    Public ReadOnly Property DaysBeforeArchive() As Decimal
        Get
            Return mDaysBeforeArchive
        End Get
        'Set(ByVal value As Decimal)
        '    _DependSingle = value
        'End Set
    End Property


    ''' <summary>
    ''' Get Setting Info from table adapter
    ''' </summary>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function GetTableData() As DataTable
        Dim appSettingsTableAdapter1 As New EnquiryWorkSheetDataSetTableAdapters.AppSettingsTableAdapter
        appSettingsTableAdapter1 = New EnquiryWorkSheetDataSetTableAdapters.AppSettingsTableAdapter
        Dim newAppSettingsTable As EnquiryWorkSheetDataSet.AppSettingsDataTable
        newAppSettingsTable = appSettingsTableAdapter1.GetData()
        Return newAppSettingsTable
    End Function



    ' ''' <summary>
    ' ''' Get Setting info from database
    ' ''' </summary>
    ' ''' <returns>DataTable</returns>
    ' ''' <remarks></remarks>
    'Private Function GetSettingData() As DataTable
    '    'create a datatable and load it with Latency points columns
    '    Dim ds As DataSet
    '    Dim da As SqlDataAdapter
    '    Dim dt As New DataTable
    '    Dim connection As New SqlConnection(My.Settings.EnquiryWorkSheetConnectionString)
    '    Dim sCommand As SqlClient.SqlCommand

    '    Try
    '        Using connection
    '            sCommand = New SqlCommand("Select Id,Name,Decimal,Text,Comments From AppSettings Order by Id")
    '            sCommand.Connection = connection
    '            da = New SqlDataAdapter(sCommand)
    '            ds = New DataSet
    '            da.Fill(ds)
    '            dt = ds.Tables(0)
    '        End Using
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message & vbNewLine & ex.TargetSite.ToString)
    '    Finally
    '    End Try

    '    Return dt
    'End Function

    ' ''' <summary>
    ' ''' Object to store Setting information
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Class Setting
    '    Public SettingId As Integer
    '    Public SettingName As String
    '    Public SettingDecimal As Decimal
    '    Public SettingText As String
    '    Public Comments As String
    'End Class

End Class
