﻿Imports System.Reflection
Imports System.ComponentModel

Public Class MyEnums


    Public Shared Function ToList(type As Type) As IList
        If type Is Nothing Then
            Throw New ArgumentNullException("type")
        End If

        Dim list As New ArrayList()
        Dim enumValues As Array = [Enum].GetValues(type)

        For Each value As [Enum] In enumValues
            list.Add(New KeyValuePair(Of Integer, String)(Convert.ToInt32(value), GetDescription(value)))
        Next

        Return list
    End Function



    Shared Function GetDescription(ByVal enumConstant As [Enum]) As String
        Dim fi As FieldInfo = enumConstant.GetType().GetField(enumConstant.ToString())
        Dim attr() As DescriptionAttribute = DirectCast(
            fi.GetCustomAttributes(GetType(DescriptionAttribute), False), 
               DescriptionAttribute())
        If attr.Length > 0 Then
            Return attr(0).Description
        Else
            Return enumConstant.ToString()
        End If
    End Function

    Shared Function GetDescriptions(type As Type) As IEnumerable(Of String)
        If type Is Nothing Then
            Throw New ArgumentNullException("type")
        End If

        Dim descs = New List(Of String)()
        Dim enumValues As Array = [Enum].GetValues(type)
        For Each value As [Enum] In enumValues
            descs.Add(GetDescription(value))
        Next

        Return descs
    End Function


    Public Enum EnquiryType
        <Description("Personal Loan (New)")> PersonalLoanNew = 0
        <Description("Personal Loan (Refinance)")> PersonalLoanRefinance
        <Description("Personal Loan (Variation)")> PersonalLoanVariation
        <Description("Personal Split Loan Drawdown")> PersonalSplitLoanDrawdown
        <Description("Consumer Retail")> ConsumerRetail
        <Description("Consumer Vehicle")> ConsumerVehicle
        <Description("Business Loan (New)")> BusinessLoanNew
        <Description("Business Loan (Refinance)")> BusinessLoanRefinance
        <Description("Business Loan (Variation)")> BusinessLoanVariation
        <Description("Business Split Loan Drawdown")> BusinessSplitLoanDrawdown
        <Description("Unsecured Personal Loan (New)")> UnsecuredPersonalLoanNew
        <Description("Unsecured Personal Loan (Refinance)")> UnsecuredPersonalLoanRefinance
        <Description("Unsecured Personal Loan (Variation)")> UnsecuredPersonalLoanVariation
        <Description("Unsecured Personal Split Loan Drawdown")> UnsecuredPersonalSplitLoanDrawdown
        <Description("Finance Facility")> FinanceFacility
    End Enum

    Public Enum CustomerType
        <Description("Individual")> Individual = 0
        <Description("Company")> Company
        <Description("Trust")> Trust
        <Description("Sole trader")> SoleTrader
    End Enum

    Public Enum CustomerEnquiryType
        <Description("main")> Main = 0
        <Description("co-main")> CoMain
        <Description("guarantor")> Guarantor
    End Enum

    Public Enum mainCustomerType
        <Description("Individual")> Individual = 0
        <Description("Company")> Company
        <Description("Trust")> Trust
        <Description("Sole trader")> SoleTrader
        <Description("Partnership")> Partnership
    End Enum

    Public Enum AMLRisk
        <Description("Select one")> SelectOne = 0
        <Description("Low")> Low
        <Description("Medium")> Medium
        <Description("High")> High
    End Enum

    Public Enum DtsbType
        <Description("Director")> Director = 0
        <Description("Trustee")> Trustee = 1
        <Description("ShareHolder")> ShareHolder = 2
        <Description("Beneficiary")> Beneficiary = 3
    End Enum

    Public Enum CustAcceptanceMethod
        <Description("Face to face")> FaceToFace = 0
        <Description("Non face to face")> NonFaceToFace
        <Description("Domestic intermediaries")> DomesticIntermeds
        <Description("Overseas intermediaries")> OverseasIntermeds
    End Enum

    Public Enum RolePermissions
        <Description("Read only")> [Readonly] = 1
        <Description("Can take new enquiries up to Follow-up stage and add comments to current enquiries")> StdUser
        <Description("StdUser + can decline,withdraw and archive their own enquiries")> Approver
        <Description("Approver + can archive other users' enquiries and reset the status")> Manager
        <Description("Manager + can use tools")> Administrator
        <Description("No restrictions")> SystemsAdministrator
    End Enum

    Public Enum TransferFileType
        <Description("Loan")> Loan = 0
        <Description("Client")> Client
    End Enum


    Public Enum ResponseErrorCode
        <Description("No error")> None
        <Description("Service error")> ServiceError
        <Description("Token error")> TokenError
        <Description("Process error")> ProcessError
    End Enum

    Public Enum ApprovalStatus
        <Description("Nothing")> [Nothing] = 0
        <Description("Declined")> Declined = 1
        <Description("Approved")> Approved = 2
    End Enum
    ''' <summary>
    ''' Set form mode
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum Mode
        <Description("ReadOnly")> [ReadOnly] = 0
        <Description("Edit")> [Edit] = 1
    End Enum

    'Public Enum OacApplicationActionType
    '    <Description("Edit")> Edit = 1
    '    <Description("Edit Manager")> EditManager = 2
    '    <Description("Submit")> Submit = 3
    '    <Description("Approve")> Approve = 4
    '    <Description("Decline")> Decline = 5
    '    <Description("Withdraw")> Withdraw = 6
    '    <Description("Resubmit")> Resubmit = 7
    '    <Description("Reactivate")> Reactivate = 8
    '    <Description("Produce Documents")> ProduceDocuments = 9
    '    <Description("Print Documents")> PrintDocuments = 10
    '    <Description("Execute Documents")> ExecuteDocuments = 11
    '    <Description("Reset")> Reset = 12
    '    <Description("Archive")> Archive = 13
    '    <Description("Unarchive")> Unarchive = 14
    '    <Description("Save")> Save = 15
    '    <Description("Save & Exit")> SaveAndExit = 16
    '    <Description("Next Step")> NextStep = 17
    '    <Description("Quote")> Quote = 18
    '    <Description("Documents Confirmed Printed")> DocumentsConfirmedPrinted = 19
    '    <Description("Digital Sign Documents")> DigitalSignDocuments = 20
    'End Enum

    Public Enum WizardStatus
        <Description("Preliminary")> Preliminary = 0
        <Description("Follow-up")> FollowUp
        <Description("Wizard Form 1")> WizForm1
        <Description("Wizard Form 2")> WizForm2
        <Description("Wizard Form 3")> WizForm3
        <Description("Wizard Form 4")> WizForm4
        <Description("Wizard Form 5")> WizForm5
        <Description("AppForm")> AppForm
    End Enum

    Public Enum IdType
        '<Description("Provide later")> ProvideLater = 0
        <Description("Birth certificate")> BirthCertificate = 0 'was 1
        <Description("18+ card")> Card18Plus 'was 2
        <Description("NZ drivers licence")> NZDriversLicence 'was 3
        <Description("NZ passport")> NZPassport 'was 4
        <Description("Overseas passport")> OverseasPassport 'was 5

    End Enum

    Public Enum DriverLicenceType
        <Description("Restricted")> Restricted = 0
        <Description("Learner")> Learner
        <Description("Full")> Full
    End Enum

    ''' <summary>
    ''' Used in RtfMsgBox
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum MsgBoxMode
        <Description("File path")> FilePath = 0
        <Description("Display text")> DisplayText
    End Enum
    ''' <summary>
    ''' Used in RtfMsgBox
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum MsgBoxFormat
        <Description("Plain text")> PlainText = 0
        <Description("RTF text")> RtfText
    End Enum

End Class
