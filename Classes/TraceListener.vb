﻿Imports Microsoft.Exchange.WebServices.Data
Imports System
Imports System.Text

Namespace EWSTrace

    Public Class TraceListener
        Implements ITraceListener

        Public Sub Trace(ByVal traceType As String, ByVal traceMessage As String) Implements ITraceListener.Trace
            CreateXMLTextFile(traceType + " --- " + traceMessage.ToString())
        End Sub

        Private Sub CreateXMLTextFile(ByVal traceContent As String)
            'Get the path of the application to create log files at
            Dim strPath As String = System.AppDomain.CurrentDomain.BaseDirectory
            strPath = strPath + "EWSLog.txt"
           

            Dim fS As System.IO.FileStream
            If System.IO.File.Exists(strPath) = False Then
                fS = System.IO.File.Create(strPath)
            Else
                fS = System.IO.File.OpenWrite(strPath)
            End If
            fS.Close()

            ' Create an instance of StreamWriter to write text to a file.
            Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(strPath)
            sw.WriteLine(System.DateTime.Now.ToString() + " : " + traceContent)
            sw.Close()
            fS = Nothing
            sw = Nothing
        End Sub

    End Class

End Namespace
