﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Runtime.InteropServices

Public Class WebUtil
    'Create variable used to call the log4net methods
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    'Set variable
    Private _LabelForeColor As Color = Color.Black
    Private _LabelText As String = ""

    'set properties
    Public ReadOnly Property GetLabelForeColor() As Color
        Get
            Return _LabelForeColor
        End Get
    End Property

    Public ReadOnly Property GetLabelText() As String
        Get
            Return _LabelText
        End Get
    End Property

    'Brings the thread that created the specified window into the foreground and activates the window. 
    'Keyboard input is directed to the window, and various visual cues are changed for the user. 
    'The system assigns a slightly higher priority to the thread that created the foreground window than it does to other threads. 
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function SetForegroundWindow(ByVal hwnd As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function ShowWindow(ByVal hwnd As IntPtr, ByVal cmdshow As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    <DllImport("user32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)> _
    Public Shared Function SetWindowPos(ByVal hwnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As UInteger) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function


    Public Shared Function UserId() As String
        'Dim user As String = loggedinUser()
        'If user.IndexOf("\") > -1 Then
        '    user = user.Split("\")(1)
        'End If
        Return LoggedinName
    End Function

    Public Shared Function PassKey(ByVal userId As String) As String
        Dim pass As String
        Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
        'Dim connection As New SqlConnection(connectionString)
        Dim selectStatement As String = "SELECT Users.Password FROM Users WHERE UserName ='" & userId & "'"

        Using connection As New SqlConnection(connectionString)
            connection.Open()
            Dim myCommand As New SqlCommand(selectStatement, connection)
            pass = myCommand.ExecuteScalar()
        End Using

        Return pass
    End Function

    Function CallMC(uri As String) As Boolean

        Try
            Process.Start(uri)
        Catch ex As Exception
            log.Error("Error in OAC Link: " & ex.Message & " : " & ex.TargetSite.ToString)
            MessageBox.Show("Error in OAC Link" & vbCrLf & ex.Message, "OAC Link Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


        'Try
        '    Dim user As String = MembersCentreUtil.UserId
        '    Dim pass As String = MembersCentreUtil.PassKey(user)

        '    OpenMembersCentre(user, pass)
        '    Return 1
        'Catch ex As Exception
        '    MessageBox.Show("Error in Members Centre Link" & vbCrLf & ex.Message, "MembersCentre Link Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Return 0

        'End Try

    End Function


    Public Sub OpenMembersCentre(ByVal user As String, ByVal pass As String)
        'Dim MyBrowser As New WebBrowser

        'Dim authHdr As String = "Authorization:" & System.Text.Encoding.UTF8.GetString(System.Text.Encoding.ASCII.GetBytes(user & ":" & pass)) '& vbCr & vbLf
        'Dim Url As String = My.Settings.WebApplicationPage_PROD

        'MyBrowser.Navigate(Url, Nothing, Nothing, authHdr)

    End Sub

    Public Sub MCLink()
        Dim navUrl As String = ""
        If My.Settings.IsTest = True Then
            navUrl = My.Settings.WebApplicationPage_TEST
        Else
            navUrl = My.Settings.WebApplicationPage_PROD
        End If

        'Check if iexplore is running
        If IsProcessOpen("iexplore") Then 'IE is now open!
            'MessageBox.Show("IE is now open!")

            Dim shellWindows As New SHDocVw.ShellWindows()
            Dim filename As String
            Dim sdOpen As Boolean = False
            Dim urlParts As String() = Nothing
            For Each ie As SHDocVw.InternetExplorer In shellWindows
                filename = Path.GetFileNameWithoutExtension(ie.FullName).ToLower()
                If filename.Equals("iexplore") Then

                    If ie.LocationURL.ToString().IndexOf(navUrl.Substring(navUrl.IndexOf("//") + 2)) > -1 Then
                        urlParts = (ie.LocationURL.ToString()).Split("/"c)

                        'Dim strPart As String = ""
                        'Dim part As String = ""
                        'For Each part In urlParts
                        '    strPart = strPart & part & vbCrLf
                        'Next
                        'MessageBox.Show("url parts:" & vbCrLf & strPart)
                        If urlParts.GetUpperBound(0) > 2 Then
                            Dim website As String = urlParts(2)
                            If website = navUrl.Substring(navUrl.IndexOf("//") + 2) Then 'Members Centre is open!
                                sdOpen = True

                                'SetForegroundWindow(ie.HWND)
                                ShowWindow(ie.HWND, 1)
                                SetWindowPos(ie.HWND, 0, 960, 0, 960, 1040, &H40)
                                Exit Sub
                            End If
                        End If
                    End If


                End If
            Next

            If sdOpen Then 'Members Centre is open!
                'MessageBox.Show("Members Centre is open!")
                'update Status Strip
                _LabelForeColor = Color.Black
                _LabelText = "Members Centre is open!"

            Else 'IE is now open but Members Centre is not open!
                'MessageBox.Show("Members Centre is not open!")

                'SetForegroundWindow(ie.HWND) 'Members Centre is not open!
                'ie.Navigate(navUrl, "0x800")
                'ie.Visible = True
                'ie.Top = True

                If CallMC(navUrl) Then
                    'update Status Strip
                    _LabelForeColor = Color.Black
                    _LabelText = "Launching web browser."
                Else
                    'update Status Strip
                    _LabelForeColor = Color.Red
                    _LabelText = "Problem Launching web browser"
                End If
            End If


        Else 'IE is now closed!
            'MessageBox.Show("IE is now closed!")

            If CallMC(navUrl) Then
                'update Status Strip
                _LabelForeColor = Color.Black
                _LabelText = "Launching web browser."
            Else
                'update Status Strip
                _LabelForeColor = Color.Red
                _LabelText = "Problem Launching web browser"
            End If
        End If

    End Sub

    ''' <summary>
    ''' Ping the Web Service Server
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Shared Function WebServiceAvailable() As Boolean
        Dim success As Boolean = False
        Dim serverAddress As String = String.Empty
        Dim strArr1() As String
        strArr1 = Switch.GetWebServiceServer.Split("//")
        serverAddress = strArr1(2)
        Try
            If My.Computer.Network.Ping(serverAddress, 1000) Then
                success = True
            Else
                success = False
            End If
            Return success
        Catch ex As Exception
            log.Error("Function WebServiceAvailable(): " & ex.Message & " : " & ex.TargetSite.ToString)
            Return False
        End Try


    End Function



End Class
