﻿''' <summary>
''' Automatically gets the TEST or PROD values
''' </summary>
''' <remarks>Tests against My.Settings.IsTest</remarks>
Public Class Switch

    ''' <summary>
    ''' Get the finPower folder for importing XML
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFinPowerImportFolder() As String
        Dim finPowerImportFolder As String = String.Empty
        If My.Settings.IsTest = True Then
            finPowerImportFolder = My.Resources.finPowerImportFolder_TEST
        Else
            finPowerImportFolder = My.Resources.finPowerImportFolder_PROD
        End If
        Return finPowerImportFolder

    End Function

    ''' <summary>
    ''' Get the finPower Client and Loan Folders Root Path
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFinPowerSharedFolder() As String
        Dim finPowerSharedFolder As String = String.Empty
        If My.Settings.IsTest = True Then
            finPowerSharedFolder = My.Resources.finPowerSharedFolder_TEST
        Else
            finPowerSharedFolder = My.Resources.finPowerSharedFolder_PROD
        End If
        Return finPowerSharedFolder

    End Function

    ''' <summary>
    ''' Get the Enquiry Archive Folders Root Path
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function GetWorksheetArchiveFolder() As String
        Dim worksheetArchiveFolder As String = String.Empty
        If My.Settings.IsDevelopment = True Then
            worksheetArchiveFolder = My.Settings.DevEnquiryFolder & "\Archives"
        Else
            If My.Settings.IsTest = True Then
                worksheetArchiveFolder = My.Resources.WorksheetArchiveFolder_TEST
            Else
                worksheetArchiveFolder = My.Resources.WorksheetArchiveFolder_PROD
            End If
        End If
        
        Return worksheetArchiveFolder

    End Function

    ''' <summary>
    ''' Get the Enquiry Folders Root Path
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function GetWorksheetSharedFolder() As String
        Dim worksheetSharedFolder As String = String.Empty
        If My.Settings.IsDevelopment = True Then
            worksheetSharedFolder = My.Settings.DevEnquiryFolder
        Else
            If My.Settings.IsTest = True Then
                worksheetSharedFolder = My.Resources.WorksheetSharedFolder_TEST
            Else
                worksheetSharedFolder = My.Resources.WorksheetSharedFolder_PROD
            End If
        End If

        Return worksheetSharedFolder

    End Function

    ''' <summary>
    ''' Get the current Data WebService uri
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks>Tests against Settings.IsTest</remarks>
    Public Shared Function GetWebServiceServer() As String
        Dim webServiceServer As String = String.Empty
        If My.Settings.IsTest = True Then
            webServiceServer = My.Settings.MembersCentreWebService_TEST
        Else
            webServiceServer = My.Settings.MembersCentreWebService_PROD
        End If
        Return webServiceServer
    End Function



End Class
