﻿Imports System.Globalization

Public Class PayoutReportSelectionsForm
    Dim StartDate As Date
    Dim EndDate As Date
    Dim GroupBy As String
    Dim OrderBy As String
    Dim ArchiveStatus As String
    Dim User As String = ""

    Private Sub PayoutReportSelectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'loads data into the 'EnquiryWorkSheetDataSet.Users' table. 
        Me.UsersTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Users)
        'set default values
        Me.cmbxGroupBy.SelectedIndex = 0
        Me.cmbxOrderBy.SelectedIndex = 0


    End Sub

    Private Sub ckbxUsers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbxUsers.CheckedChanged
        If ckbxUsers.CheckState = CheckState.Checked Then
            lstbxUsers.Enabled = True
        Else
            lstbxUsers.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Cursor.Current = Cursors.WaitCursor
        'get values
        StartDate = DateTimePicker1.Value.ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        EndDate = DateTimePicker2.Value.AddDays(1).ToString("d", CultureInfo.CreateSpecificCulture("en-NZ"))
        'get users if selected
        If lstbxUsers.Enabled = True Then
            User = lstbxUsers.SelectedValue.ToString
            'MsgBox("Selected User = " + User)
        Else
            User = ""
        End If
        'get Group By
        GroupBy = cmbxGroupBy.SelectedItem.ToString
        'get Order by
        OrderBy = cmbxOrderBy.SelectedItem.ToString
        Try
            'launch ReportPayoutDateForm and pass Enquiry parameters
            Dim ReportPayoutDateFrm As New ReportPayoutDateForm
            ReportPayoutDateFrm.ReportSelections_PassVariable(StartDate, EndDate, GroupBy, OrderBy, User)
            ReportPayoutDateFrm.Show()
        Catch ex As Exception
            MsgBox("Error launching ReportPayoutDateForm: " + vbCrLf + ex.Message)
        End Try
        Cursor.Current = Cursors.Default

    End Sub

End Class