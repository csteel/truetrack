﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditDetailsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditDetailsForm))
        Me.cmbxSuburb = New System.Windows.Forms.ComboBox()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.SuburbListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtbxAmount = New System.Windows.Forms.TextBox()
        Me.lblSuburb = New System.Windows.Forms.Label()
        Me.lblTown = New System.Windows.Forms.Label()
        Me.lblBorrow = New System.Windows.Forms.Label()
        Me.cmbxEnquiryType = New System.Windows.Forms.ComboBox()
        Me.PrelimReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblReason = New System.Windows.Forms.Label()
        Me.cmbxTown = New System.Windows.Forms.ComboBox()
        Me.NZTownsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblPrelimArea = New System.Windows.Forms.Label()
        Me.txtbxPrelimContact = New System.Windows.Forms.TextBox()
        Me.lblPrelimContact = New System.Windows.Forms.Label()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.cmbxTitle = New System.Windows.Forms.ComboBox()
        Me.txtbxLastName = New System.Windows.Forms.TextBox()
        Me.txtbxFirstName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblFormTitle = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblDealerId = New System.Windows.Forms.Label()
        Me.btnChangeDealer = New System.Windows.Forms.Button()
        Me.lblEnquireManager = New System.Windows.Forms.Label()
        Me.lblEnquiryManagerValue = New System.Windows.Forms.Label()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblEnquiryManagerId = New System.Windows.Forms.Label()
        Me.btnChangeManager = New System.Windows.Forms.Button()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.ActiveDealersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveDealersTableAdapter()
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.NZTownsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.NZTownsTableAdapter()
        Me.PrelimReasonsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter()
        Me.SuburbListTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SuburbListTableAdapter()
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.ActiveDealersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtbxSalutation = New System.Windows.Forms.TextBox()
        Me.lblUsed2b = New System.Windows.Forms.Label()
        Me.lblCurrentSalutation = New System.Windows.Forms.Label()
        Me.btnResetDisplayName = New System.Windows.Forms.Button()
        Me.lblCurrentLoanAmt = New System.Windows.Forms.Label()
        Me.txtbxCurrentLoanAmt = New System.Windows.Forms.TextBox()
        Me.lblTopupNote = New System.Windows.Forms.Label()
        Me.LoanPurposeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LoanPurposeTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanPurposeTableAdapter()
        Me.cmbxLoanPurpose = New System.Windows.Forms.ComboBox()
        Me.lblLoanPurpose = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuburbListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NZTownsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStripMessage.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActiveDealersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoanPurposeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbxSuburb
        '
        Me.cmbxSuburb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxSuburb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxSuburb.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactSuburb", True))
        Me.cmbxSuburb.DataSource = Me.SuburbListBindingSource
        Me.cmbxSuburb.DisplayMember = "Suburb"
        Me.cmbxSuburb.FormattingEnabled = True
        Me.cmbxSuburb.Location = New System.Drawing.Point(227, 179)
        Me.cmbxSuburb.Name = "cmbxSuburb"
        Me.cmbxSuburb.Size = New System.Drawing.Size(196, 21)
        Me.cmbxSuburb.TabIndex = 8
        Me.cmbxSuburb.ValueMember = "Suburb"
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SuburbListBindingSource
        '
        Me.SuburbListBindingSource.DataMember = "SuburbList"
        Me.SuburbListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'txtbxAmount
        '
        Me.txtbxAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.txtbxAmount.Location = New System.Drawing.Point(460, 206)
        Me.txtbxAmount.Name = "txtbxAmount"
        Me.txtbxAmount.Size = New System.Drawing.Size(183, 20)
        Me.txtbxAmount.TabIndex = 11
        '
        'lblSuburb
        '
        Me.lblSuburb.AutoSize = True
        Me.lblSuburb.Location = New System.Drawing.Point(180, 182)
        Me.lblSuburb.Name = "lblSuburb"
        Me.lblSuburb.Size = New System.Drawing.Size(41, 13)
        Me.lblSuburb.TabIndex = 70
        Me.lblSuburb.Text = "Suburb"
        '
        'lblTown
        '
        Me.lblTown.AutoSize = True
        Me.lblTown.Location = New System.Drawing.Point(169, 151)
        Me.lblTown.Name = "lblTown"
        Me.lblTown.Size = New System.Drawing.Size(56, 13)
        Me.lblTown.TabIndex = 69
        Me.lblTown.Text = "Town/City"
        '
        'lblBorrow
        '
        Me.lblBorrow.AutoSize = True
        Me.lblBorrow.Location = New System.Drawing.Point(384, 209)
        Me.lblBorrow.Name = "lblBorrow"
        Me.lblBorrow.Size = New System.Drawing.Size(70, 13)
        Me.lblBorrow.TabIndex = 68
        Me.lblBorrow.Text = "Loan Amount"
        '
        'cmbxEnquiryType
        '
        Me.cmbxEnquiryType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxEnquiryType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxEnquiryType.FormattingEnabled = True
        Me.cmbxEnquiryType.Location = New System.Drawing.Point(156, 206)
        Me.cmbxEnquiryType.Name = "cmbxEnquiryType"
        Me.cmbxEnquiryType.Size = New System.Drawing.Size(184, 21)
        Me.cmbxEnquiryType.TabIndex = 10
        '
        'PrelimReasonsBindingSource
        '
        Me.PrelimReasonsBindingSource.DataMember = "PrelimReasons"
        Me.PrelimReasonsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblReason
        '
        Me.lblReason.AutoSize = True
        Me.lblReason.Location = New System.Drawing.Point(30, 209)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(114, 13)
        Me.lblReason.TabIndex = 67
        Me.lblReason.Text = "Type of Loan Enquiry?"
        '
        'cmbxTown
        '
        Me.cmbxTown.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTown.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTown.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactCity", True))
        Me.cmbxTown.DataSource = Me.NZTownsBindingSource
        Me.cmbxTown.DisplayMember = "Towns"
        Me.cmbxTown.FormattingEnabled = True
        Me.cmbxTown.Location = New System.Drawing.Point(227, 148)
        Me.cmbxTown.Name = "cmbxTown"
        Me.cmbxTown.Size = New System.Drawing.Size(196, 21)
        Me.cmbxTown.TabIndex = 7
        Me.cmbxTown.ValueMember = "Towns"
        '
        'NZTownsBindingSource
        '
        Me.NZTownsBindingSource.DataMember = "NZTowns"
        Me.NZTownsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblPrelimArea
        '
        Me.lblPrelimArea.AutoSize = True
        Me.lblPrelimArea.Location = New System.Drawing.Point(30, 149)
        Me.lblPrelimArea.Name = "lblPrelimArea"
        Me.lblPrelimArea.Size = New System.Drawing.Size(128, 13)
        Me.lblPrelimArea.TabIndex = 66
        Me.lblPrelimArea.Text = "What area do you live in?"
        '
        'txtbxPrelimContact
        '
        Me.txtbxPrelimContact.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.txtbxPrelimContact.Location = New System.Drawing.Point(156, 70)
        Me.txtbxPrelimContact.MaxLength = 20
        Me.txtbxPrelimContact.Name = "txtbxPrelimContact"
        Me.txtbxPrelimContact.Size = New System.Drawing.Size(133, 20)
        Me.txtbxPrelimContact.TabIndex = 5
        '
        'lblPrelimContact
        '
        Me.lblPrelimContact.AutoSize = True
        Me.lblPrelimContact.Location = New System.Drawing.Point(30, 73)
        Me.lblPrelimContact.Name = "lblPrelimContact"
        Me.lblPrelimContact.Size = New System.Drawing.Size(115, 13)
        Me.lblPrelimContact.TabIndex = 65
        Me.lblPrelimContact.Text = "Contact phone number"
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Location = New System.Drawing.Point(384, 42)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(58, 13)
        Me.lblLastName.TabIndex = 53
        Me.lblLastName.Text = "Last Name"
        '
        'cmbxTitle
        '
        Me.cmbxTitle.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxTitle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxTitle.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactTitle", True))
        Me.cmbxTitle.FormattingEnabled = True
        Me.cmbxTitle.Items.AddRange(New Object() {"Mr", "Mrs", "Ms", "Miss", "Dr"})
        Me.cmbxTitle.Location = New System.Drawing.Point(156, 39)
        Me.cmbxTitle.Name = "cmbxTitle"
        Me.cmbxTitle.Size = New System.Drawing.Size(60, 21)
        Me.cmbxTitle.TabIndex = 2
        '
        'txtbxLastName
        '
        Me.txtbxLastName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactLastName", True))
        Me.txtbxLastName.Location = New System.Drawing.Point(448, 39)
        Me.txtbxLastName.MaxLength = 40
        Me.txtbxLastName.Name = "txtbxLastName"
        Me.txtbxLastName.Size = New System.Drawing.Size(195, 20)
        Me.txtbxLastName.TabIndex = 4
        '
        'txtbxFirstName
        '
        Me.txtbxFirstName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactFirstName", True))
        Me.txtbxFirstName.Location = New System.Drawing.Point(223, 39)
        Me.txtbxFirstName.MaxLength = 40
        Me.txtbxFirstName.Name = "txtbxFirstName"
        Me.txtbxFirstName.Size = New System.Drawing.Size(148, 20)
        Me.txtbxFirstName.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(30, 42)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(73, 13)
        Me.lblName.TabIndex = 50
        Me.lblName.Text = "Contact name"
        '
        'lblFormTitle
        '
        Me.lblFormTitle.AutoSize = True
        Me.lblFormTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormTitle.Location = New System.Drawing.Point(251, 9)
        Me.lblFormTitle.Name = "lblFormTitle"
        Me.lblFormTitle.Size = New System.Drawing.Size(203, 20)
        Me.lblFormTitle.TabIndex = 75
        Me.lblFormTitle.Text = "Edit Enquiry Information"
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(33, 359)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 16
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.Location = New System.Drawing.Point(599, 359)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 17
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 390)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(704, 22)
        Me.StatusStripMessage.TabIndex = 78
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(80, 17)
        Me.ToolStripStatusLabel1.Text = "Status:  Ready"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 291)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 13)
        Me.Label1.TabIndex = 99
        Me.Label1.Text = "Current Dealer is:"
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(153, 291)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(67, 13)
        Me.lblDealerName.TabIndex = 100
        Me.lblDealerName.Text = "Dealer name"
        '
        'lblDealerId
        '
        Me.lblDealerId.AutoSize = True
        Me.lblDealerId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DealerId", True))
        Me.lblDealerId.Location = New System.Drawing.Point(384, 291)
        Me.lblDealerId.Name = "lblDealerId"
        Me.lblDealerId.Size = New System.Drawing.Size(50, 13)
        Me.lblDealerId.TabIndex = 102
        Me.lblDealerId.Text = "Dealer Id"
        '
        'btnChangeDealer
        '
        Me.btnChangeDealer.Location = New System.Drawing.Point(541, 286)
        Me.btnChangeDealer.Name = "btnChangeDealer"
        Me.btnChangeDealer.Size = New System.Drawing.Size(102, 23)
        Me.btnChangeDealer.TabIndex = 14
        Me.btnChangeDealer.Text = "Change Dealer"
        Me.btnChangeDealer.UseVisualStyleBackColor = True
        '
        'lblEnquireManager
        '
        Me.lblEnquireManager.AutoSize = True
        Me.lblEnquireManager.Location = New System.Drawing.Point(30, 323)
        Me.lblEnquireManager.Name = "lblEnquireManager"
        Me.lblEnquireManager.Size = New System.Drawing.Size(137, 13)
        Me.lblEnquireManager.TabIndex = 104
        Me.lblEnquireManager.Text = "Current Enquiry Manager is:"
        '
        'lblEnquiryManagerValue
        '
        Me.lblEnquiryManagerValue.AutoSize = True
        Me.lblEnquiryManagerValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "FullName", True))
        Me.lblEnquiryManagerValue.Location = New System.Drawing.Point(174, 323)
        Me.lblEnquiryManagerValue.Name = "lblEnquiryManagerValue"
        Me.lblEnquiryManagerValue.Size = New System.Drawing.Size(115, 13)
        Me.lblEnquiryManagerValue.TabIndex = 105
        Me.lblEnquiryManagerValue.Text = "Enquiry manager name"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'lblEnquiryManagerId
        '
        Me.lblEnquiryManagerId.AutoSize = True
        Me.lblEnquiryManagerId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryManagerId", True))
        Me.lblEnquiryManagerId.Location = New System.Drawing.Point(384, 323)
        Me.lblEnquiryManagerId.Name = "lblEnquiryManagerId"
        Me.lblEnquiryManagerId.Size = New System.Drawing.Size(99, 13)
        Me.lblEnquiryManagerId.TabIndex = 106
        Me.lblEnquiryManagerId.Text = "Enquiry Manager Id"
        Me.lblEnquiryManagerId.Visible = False
        '
        'btnChangeManager
        '
        Me.btnChangeManager.Location = New System.Drawing.Point(541, 318)
        Me.btnChangeManager.Name = "btnChangeManager"
        Me.btnChangeManager.Size = New System.Drawing.Size(102, 23)
        Me.btnChangeManager.TabIndex = 15
        Me.btnChangeManager.Text = "Change Manager"
        Me.btnChangeManager.UseVisualStyleBackColor = True
        Me.btnChangeManager.Visible = False
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Me.ActiveDealersTableAdapter
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        'Me.TableAdapterManager.ClientLogTableAdapter = Nothing
        'Me.TableAdapterManager.ClientTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Me.NZTownsTableAdapter
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Me.PrelimReasonsTableAdapter
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Me.SuburbListTableAdapter
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter
        '
        'ActiveDealersTableAdapter
        '
        Me.ActiveDealersTableAdapter.ClearBeforeFill = True
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'NZTownsTableAdapter
        '
        Me.NZTownsTableAdapter.ClearBeforeFill = True
        '
        'PrelimReasonsTableAdapter
        '
        Me.PrelimReasonsTableAdapter.ClearBeforeFill = True
        '
        'SuburbListTableAdapter
        '
        Me.SuburbListTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'ActiveDealersBindingSource
        '
        Me.ActiveDealersBindingSource.DataMember = "ActiveDealers"
        Me.ActiveDealersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(30, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 108
        Me.Label2.Text = "Display Name"
        '
        'txtbxSalutation
        '
        Me.txtbxSalutation.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactDisplayName", True))
        Me.txtbxSalutation.Location = New System.Drawing.Point(156, 101)
        Me.txtbxSalutation.Name = "txtbxSalutation"
        Me.txtbxSalutation.Size = New System.Drawing.Size(267, 20)
        Me.txtbxSalutation.TabIndex = 6
        '
        'lblUsed2b
        '
        Me.lblUsed2b.AutoSize = True
        Me.lblUsed2b.Location = New System.Drawing.Point(153, 124)
        Me.lblUsed2b.Name = "lblUsed2b"
        Me.lblUsed2b.Size = New System.Drawing.Size(57, 13)
        Me.lblUsed2b.TabIndex = 110
        Me.lblUsed2b.Text = "used to be"
        Me.lblUsed2b.Visible = False
        '
        'lblCurrentSalutation
        '
        Me.lblCurrentSalutation.AutoSize = True
        Me.lblCurrentSalutation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentSalutation.Location = New System.Drawing.Point(216, 124)
        Me.lblCurrentSalutation.Name = "lblCurrentSalutation"
        Me.lblCurrentSalutation.Size = New System.Drawing.Size(0, 13)
        Me.lblCurrentSalutation.TabIndex = 111
        '
        'btnResetDisplayName
        '
        Me.btnResetDisplayName.Location = New System.Drawing.Point(429, 99)
        Me.btnResetDisplayName.Name = "btnResetDisplayName"
        Me.btnResetDisplayName.Size = New System.Drawing.Size(121, 23)
        Me.btnResetDisplayName.TabIndex = 112
        Me.btnResetDisplayName.Text = "Reset Display Name"
        Me.btnResetDisplayName.UseVisualStyleBackColor = True
        Me.btnResetDisplayName.Visible = False
        '
        'lblCurrentLoanAmt
        '
        Me.lblCurrentLoanAmt.AutoSize = True
        Me.lblCurrentLoanAmt.Location = New System.Drawing.Point(30, 265)
        Me.lblCurrentLoanAmt.Name = "lblCurrentLoanAmt"
        Me.lblCurrentLoanAmt.Size = New System.Drawing.Size(107, 13)
        Me.lblCurrentLoanAmt.TabIndex = 113
        Me.lblCurrentLoanAmt.Text = "Current Loan Amount"
        Me.lblCurrentLoanAmt.Visible = False
        '
        'txtbxCurrentLoanAmt
        '
        Me.txtbxCurrentLoanAmt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentLoanAmt", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.txtbxCurrentLoanAmt.Location = New System.Drawing.Point(156, 262)
        Me.txtbxCurrentLoanAmt.Name = "txtbxCurrentLoanAmt"
        Me.txtbxCurrentLoanAmt.Size = New System.Drawing.Size(184, 20)
        Me.txtbxCurrentLoanAmt.TabIndex = 13
        Me.txtbxCurrentLoanAmt.Visible = False
        '
        'lblTopupNote
        '
        Me.lblTopupNote.AutoSize = True
        Me.lblTopupNote.Location = New System.Drawing.Point(344, 265)
        Me.lblTopupNote.Name = "lblTopupNote"
        Me.lblTopupNote.Size = New System.Drawing.Size(170, 13)
        Me.lblTopupNote.TabIndex = 115
        Me.lblTopupNote.Text = "(The loan amount before Variation)"
        Me.lblTopupNote.Visible = False
        '
        'LoanPurposeBindingSource
        '
        Me.LoanPurposeBindingSource.DataMember = "LoanPurpose"
        Me.LoanPurposeBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'LoanPurposeTableAdapter
        '
        Me.LoanPurposeTableAdapter.ClearBeforeFill = True
        '
        'cmbxLoanPurpose
        '
        Me.cmbxLoanPurpose.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanPurpose", True))
        Me.cmbxLoanPurpose.DataSource = Me.LoanPurposeBindingSource
        Me.cmbxLoanPurpose.DisplayMember = "LoanPurpose"
        Me.cmbxLoanPurpose.FormattingEnabled = True
        Me.cmbxLoanPurpose.Location = New System.Drawing.Point(156, 233)
        Me.cmbxLoanPurpose.Name = "cmbxLoanPurpose"
        Me.cmbxLoanPurpose.Size = New System.Drawing.Size(184, 21)
        Me.cmbxLoanPurpose.TabIndex = 12
        Me.cmbxLoanPurpose.ValueMember = "LoanPurpose"
        '
        'lblLoanPurpose
        '
        Me.lblLoanPurpose.AutoSize = True
        Me.lblLoanPurpose.Location = New System.Drawing.Point(30, 236)
        Me.lblLoanPurpose.Name = "lblLoanPurpose"
        Me.lblLoanPurpose.Size = New System.Drawing.Size(79, 13)
        Me.lblLoanPurpose.TabIndex = 116
        Me.lblLoanPurpose.Text = "Loan Purpose?"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(339, 73)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(32, 13)
        Me.lblEmail.TabIndex = 117
        Me.lblEmail.Text = "Email"
        '
        'txtEmail
        '
        Me.txtEmail.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactEmail", True))
        Me.txtEmail.Location = New System.Drawing.Point(377, 70)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(266, 20)
        Me.txtEmail.TabIndex = 118
        '
        'EditDetailsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(704, 412)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.lblLoanPurpose)
        Me.Controls.Add(Me.cmbxLoanPurpose)
        Me.Controls.Add(Me.lblTopupNote)
        Me.Controls.Add(Me.txtbxCurrentLoanAmt)
        Me.Controls.Add(Me.lblCurrentLoanAmt)
        Me.Controls.Add(Me.btnResetDisplayName)
        Me.Controls.Add(Me.lblCurrentSalutation)
        Me.Controls.Add(Me.lblUsed2b)
        Me.Controls.Add(Me.txtbxSalutation)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnChangeManager)
        Me.Controls.Add(Me.lblEnquiryManagerId)
        Me.Controls.Add(Me.lblEnquiryManagerValue)
        Me.Controls.Add(Me.lblEnquireManager)
        Me.Controls.Add(Me.btnChangeDealer)
        Me.Controls.Add(Me.lblDealerId)
        Me.Controls.Add(Me.lblDealerName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.lblFormTitle)
        Me.Controls.Add(Me.cmbxSuburb)
        Me.Controls.Add(Me.txtbxAmount)
        Me.Controls.Add(Me.lblSuburb)
        Me.Controls.Add(Me.lblTown)
        Me.Controls.Add(Me.lblBorrow)
        Me.Controls.Add(Me.cmbxEnquiryType)
        Me.Controls.Add(Me.lblReason)
        Me.Controls.Add(Me.cmbxTown)
        Me.Controls.Add(Me.lblPrelimArea)
        Me.Controls.Add(Me.txtbxPrelimContact)
        Me.Controls.Add(Me.lblPrelimContact)
        Me.Controls.Add(Me.lblLastName)
        Me.Controls.Add(Me.cmbxTitle)
        Me.Controls.Add(Me.txtbxLastName)
        Me.Controls.Add(Me.txtbxFirstName)
        Me.Controls.Add(Me.lblName)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(720, 450)
        Me.MinimumSize = New System.Drawing.Size(720, 450)
        Me.Name = "EditDetailsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Enquiry Details Form"
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuburbListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NZTownsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActiveDealersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoanPurposeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbxSuburb As System.Windows.Forms.ComboBox
    Friend WithEvents txtbxAmount As System.Windows.Forms.TextBox
    Friend WithEvents lblSuburb As System.Windows.Forms.Label
    Friend WithEvents lblTown As System.Windows.Forms.Label
    Friend WithEvents lblBorrow As System.Windows.Forms.Label
    Friend WithEvents cmbxEnquiryType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cmbxTown As System.Windows.Forms.ComboBox
    Friend WithEvents lblPrelimArea As System.Windows.Forms.Label
    Friend WithEvents txtbxPrelimContact As System.Windows.Forms.TextBox
    Friend WithEvents lblPrelimContact As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents cmbxTitle As System.Windows.Forms.ComboBox
    Friend WithEvents txtbxLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtbxFirstName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblFormTitle As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NZTownsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.NZTownsTableAdapter
    Friend WithEvents NZTownsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimReasonsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter
    Friend WithEvents PrelimReasonsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents SuburbListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SuburbListTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.SuburbListTableAdapter
    Friend WithEvents ActiveDealersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActiveDealersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveDealersTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblDealerId As System.Windows.Forms.Label
    Friend WithEvents btnChangeDealer As System.Windows.Forms.Button
    Friend WithEvents lblEnquireManager As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryManagerValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryManagerId As System.Windows.Forms.Label
    Friend WithEvents btnChangeManager As System.Windows.Forms.Button
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtbxSalutation As System.Windows.Forms.TextBox
    Friend WithEvents lblUsed2b As System.Windows.Forms.Label
    Friend WithEvents lblCurrentSalutation As System.Windows.Forms.Label
    Friend WithEvents btnResetDisplayName As System.Windows.Forms.Button
    Friend WithEvents lblCurrentLoanAmt As System.Windows.Forms.Label
    Friend WithEvents txtbxCurrentLoanAmt As System.Windows.Forms.TextBox
    Friend WithEvents lblTopupNote As System.Windows.Forms.Label
    Friend WithEvents LoanPurposeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LoanPurposeTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.LoanPurposeTableAdapter
    Friend WithEvents cmbxLoanPurpose As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanPurpose As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
End Class
