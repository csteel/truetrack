﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportMarketingForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReportMarketingForm))
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.CurrentAndArchiveEnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.CurrentAndArchiveEnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CurrentAndArchiveEnquiryTableAdapter()
        CType(Me.CurrentAndArchiveEnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "EnquiryWorkSheetDataSet_CurrentAndArchiveEnquiry"
        ReportDataSource1.Value = Me.CurrentAndArchiveEnquiryBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "AppWhShtB.MarketingReport.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(744, 1012)
        Me.ReportViewer1.TabIndex = 0
        '
        'CurrentAndArchiveEnquiryBindingSource
        '
        Me.CurrentAndArchiveEnquiryBindingSource.DataMember = "CurrentAndArchiveEnquiry"
        Me.CurrentAndArchiveEnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CurrentAndArchiveEnquiryTableAdapter
        '
        Me.CurrentAndArchiveEnquiryTableAdapter.ClearBeforeFill = True
        '
        'ReportMarketingForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(744, 1012)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "ReportMarketingForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Marketing Report"
        CType(Me.CurrentAndArchiveEnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents CurrentAndArchiveEnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents CurrentAndArchiveEnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.CurrentAndArchiveEnquiryTableAdapter
End Class
