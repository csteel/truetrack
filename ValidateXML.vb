﻿Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.Text
Imports System.Configuration

'Update 31/08/2015
'version no: 2.3.0.1(07/05/2014)
Public Class ValidateXML
    Private _LoanCode As String = ""
    Private isShowSelected As Boolean
    Private isDirty As Boolean = True
    Private prevkey As Integer = 0 'lstClientName.SelectedItem.key
    Private currentCustomerType As String = ""
    Private finPowerClientId As String
    Public clientTrueTrackId As Integer
    Public EnquiryId As Integer
    Public EnquiryCode As String
    Public ApplicationCode As String
    Public EnquiryType As Integer
    'Define objects
    Private finPowerClients As New Dictionary(Of Integer, ClientMappingDetails) ' Stores Selected data with Key (Xml client order num)
    Public finPowerOpObj As finPowerOps
    

    '=======================================================
    'Data selected is stored in List finPowerClients and finally saved to XML on COMMIT
    'finPowerOpObj.ClientXmlInfo in finPowerOps class contains current list of Client Names in XML
    '=======================================================
    Public Property Loancode() As String
        Get
            'returns the loanCode to the parent form
            Return _LoanCode
        End Get
        Set(ByVal value As String)
            _LoanCode = value
        End Set
    End Property

    Public Sub SetFormCaption(ByVal ClientSalutation As String)
        Dim thisFormText As String = "Possible Client matches for importing into finPower"
        'Set form title
        Me.Text = ClientSalutation & " - " & thisFormText
        'Me.Text = thisFormText
    End Sub



    Private Sub ValidateXML_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Me.lblFormHeader.Width = Me.Width
        FormLoadSetting()

        If finPowerOpObj Is Nothing Then 'May already be set by  AppForm: CreatefinPowerLoanPostProcess()

            finPowerOpObj = New finPowerOps(EnquiryCode, ApplicationCode)

            'CheckClientNames reads data from XML and checks a match exists in finPower and Builds the hashtable of Names for display 
            finPowerOpObj.CheckClientNames()

        End If
        'Show error message
        If (finPowerOpObj.ClientXmlInfo.Status = False And String.IsNullOrEmpty(finPowerOpObj.ClientXmlInfo.ErrorMessage)) Then
            MessageBox.Show(finPowerOpObj.ClientXmlInfo.ErrorMessage, "Error", MessageBoxButtons.OK)
            finPowerOpObj.ClientXmlInfo.ErrorMessage = ""
        End If
        'Populate the ListNames
        PopulateClientName() 'requires ClientXmlInfo

        '17/02/2017 added filter for FinanceFacility( no loan)
        If Not EnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            Loancode = finPowerOpObj.LoanCode
        End If

    End Sub

    ''' <summary>
    ''' populates XML client Names into Names listbox on form
    ''' </summary>
    ''' <remarks>Dependant on ClientXmlInfo. Purpose of this list is to save the finPower ClientId to the correct TrueTrack Customer</remarks>
    Public Sub PopulateClientName()
        Dim dispName As String = ""
        Dim i As Integer = 0
        Dim kvp As KeyValuePair(Of String, String)
        Dim clientfinPow As ClientMappingDetails

        For i = 1 To finPowerOpObj.ClientXmlInfo.Clients.Count
            If finPowerOpObj.ClientNames.ContainsKey(i) Then
                clientfinPow = CType(finPowerOpObj.ClientXmlInfo.Clients.Item(i), ClientMappingDetails)
                Select Case clientfinPow.CustomerType
                    Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                        'For Individual display Name
                        dispName = (clientfinPow.FirstName & " " & clientfinPow.LastName).Trim

                        'If clientfinPow.Comment.ToUpper = "AKA" Then
                        '    dispName += " AKA"
                        'End If
                    Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP, Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                        'For Trust and Partnership display Company Name
                        dispName = (clientfinPow.CompanyName).Trim
                    Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                        'For Company display Legal Name
                        dispName = (clientfinPow.LegalName).Trim
                End Select
                kvp = New KeyValuePair(Of String, String)(i.ToString, dispName)
                lstClientName.DisplayMember = "Value"
                lstClientName.ValueMember = "key"
                lstClientName.Items.Add(kvp)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Display Address and Contact Details
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetfinPowerAddressAndContactDetails()

        If dgvNames.RowCount > 0 And dgvNames.SelectedRows.Count > 0 Then

            txtfinAddress.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("PLAddress").Value), "", dgvNames.SelectedRows(0).Cells("PLAddress").Value)
            txtfinSuburb.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("PLSuburb").Value), "", dgvNames.SelectedRows(0).Cells("PLSuburb").Value)
            txtfinCity.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("PLCity").Value), "", dgvNames.SelectedRows(0).Cells("PLCity").Value)
            txtfinPostCode.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("PLPostCode").Value), "", dgvNames.SelectedRows(0).Cells("PLPostCode").Value)
            txtFinPostalAddress.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("Address").Value), "", dgvNames.SelectedRows(0).Cells("Address").Value)
            txtFinPostalSuburb.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("Suburb").Value), "", dgvNames.SelectedRows(0).Cells("Suburb").Value)
            txtFinPostalCity.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("City").Value), "", dgvNames.SelectedRows(0).Cells("City").Value)
            txtFinPostalPostcode.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("PostCode").Value), "", dgvNames.SelectedRows(0).Cells("PostCode").Value)
            txtFinManager.Text = IIf(Convert.IsDBNull(dgvNames.SelectedRows(0).Cells("ManagerId").Value), "", dgvNames.SelectedRows(0).Cells("ManagerId").Value)

            Me.dgvContactMethodFin.DataSource = finPowerOpObj.GetContactMethods(dgvNames.SelectedRows(0).Cells("Client ID").Value)
            'If dgvContactMethodFin.RowCount > 0 And dgvContactMethodFin.ColumnCount > 1 Then
            '    dgvContactMethodFin.Columns(2).Width = (dgvContactMethodFin.Width / 2) - 12
            '    dgvContactMethodFin.Columns(3).Width = (dgvContactMethodFin.Width / 2) - 12
            '    dgvContactMethodFin.Columns(0).Width = 25
            '    dgvContactMethodFin.Columns(1).Width = 0
            '    dgvContactMethodFin.Columns(1).Visible = False
            'End If

            'reset grid column settings
            SetDatagridColumns()

            

        End If

    End Sub
    ''' <summary>
    ''' Clear Address and Contact Details in gpbxExistClientDetails
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearFinPowerAddressAndContactDetails()
        txtfinAddress.Text = String.Empty
        txtfinSuburb.Text = String.Empty
        txtfinCity.Text = String.Empty
        txtfinPostCode.Text = String.Empty
        txtFinPostalAddress.Text = String.Empty
        txtFinPostalSuburb.Text = String.Empty
        txtFinPostalCity.Text = String.Empty
        txtFinPostalPostcode.Text = String.Empty
        txtFinManager.Text = String.Empty
        chkFinAddress.Checked = False
        chkFinPostalAddress.Checked = False
        chkFinManager.Checked = False

        Me.dgvContactMethodFin.DataSource = Nothing


    End Sub

    ''' <summary>
    ''' restricts user from selecting multiple checkboxes, sets finPower address and contact details
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dgvNames_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNames.CellContentClick
        'restricts user from selecting multiple checkboxes
        Dim chkcell As DataGridViewCheckBoxCell
        Dim i As Integer
        btnAddtoGrid.ForeColor = Color.Black
        btnAddtoGrid.BackColor = Color.YellowGreen

        'gpbxfinPowDetails.Enabled = False
        'chkcell = dgvNames.Rows(dgvNames.CurrentRow.Index).Cells(0)

        'If chkcell.EditingCellFormattedValue = True Then
        '    gpbxfinPowDetails.Enabled = True
        'Else
        '    gpbxfinPowDetails.Enabled = False
        'End If
        Dim found As Boolean = False
        If e.ColumnIndex = 0 Then
            For i = 0 To dgvNames.Rows.Count - 1
                chkcell = dgvNames.Rows(i).Cells(0)
                If chkcell.EditedFormattedValue = True Then
                    If Not i = e.RowIndex Then
                        dgvNames.Rows(i).Cells(0).Value = False
                    Else
                        found = True
                        'Show the details of the selected item
                        GetfinPowerAddressAndContactDetails()
                        'Set button text
                        btnAddtoGrid.ForeColor = Color.White
                        btnAddtoGrid.BackColor = Color.Red

                    End If

                End If
            Next
            If found = False Then
                'Clear gpbxfinPowDetails fields
                ClearFinPowerAddressAndContactDetails()
            End If
        End If



    End Sub

    Private Sub dgvNames_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvNames.SelectionChanged

        ''Show the details of the selected item
        'GetfinPowerAddressAndContactDetails()

        Dim chkcell As DataGridViewCheckBoxCell
        chkcell = dgvNames.Rows(dgvNames.CurrentRow.Index).Cells(0)

    End Sub


    ''' <summary>
    ''' returns Dictionary of selected contact methods from the OAC list
    ''' </summary>
    ''' <returns>List(Of ContactMethods)</returns>
    ''' <remarks>Return the dictionary of selected OAC Contact methods in XML</remarks>
    Private Function BuildSelectedContactMethodsOAC() As List(Of ContactMethods)
        Dim i As Integer
        Dim ContactMethodsOACXml As New List(Of ContactMethods)

        'iterate thru contacts in the list
        For i = 0 To dgvContactsXML.RowCount - 1
            'check the contact method is checked
            If dgvContactsXML.Rows(i).Cells(0).Value = True Then
                ContactMethodsOACXml.Add(New ContactMethods(dgvContactsXML.Rows(i).Cells("ContactMethodName").Value, dgvContactsXML.Rows(i).Cells("ContactMethodValue").Value))
            End If
        Next
        'return the dictionary of selected OAC Contact methods in XML
        Return ContactMethodsOACXml

    End Function

    ''' <summary>
    ''' returns Dictionary of selected contact methods from the finPower list
    ''' </summary>
    ''' <returns>List(Of ContactMethods)</returns>
    ''' <remarks></remarks>
    Private Function BuildSelectedContactMethodsFinPOWER() As List(Of ContactMethods)
        Dim i As Integer
        Dim ContactMethodsFinPOWER As New List(Of ContactMethods)

        For i = 0 To dgvContactMethodFin.RowCount - 1
            'check the contact method is checked
            If dgvContactMethodFin.Rows(i).Cells(0).Value = True Then
                ContactMethodsFinPOWER.Add(New ContactMethods(dgvContactMethodFin.Rows(i).Cells("ContactMethodName").Value, dgvContactMethodFin.Rows(i).Cells("ContactMethodValue").Value))
            End If
        Next

        Return ContactMethodsFinPOWER

    End Function



    ''' <summary>
    ''' Add changes of selected Client into clientMapDetails (ClientMappingDetails Object) and adds into the finPowerClients HashTable 
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function AddClientChanges(ByVal key As Integer) As Boolean

        Dim row As DataGridViewRow
        Dim chkcell As DataGridViewCheckBoxCell
        Dim clientMapDetails As New ClientMappingDetails
        Dim clientXML As New ClientMappingDetails
        Dim strAlertMessage As String = ""
        Dim typeApplicantXml As String = String.Empty
        Dim XRefIdXml As String = String.Empty
        Dim ClientTypeIdXml As String = String.Empty
        strAlertMessage = "Cannot have more than one match selected for the client. Do you want to overwrite your current selection?"

        'Alert user if the data already exists in the dataGrid
        If finPowerClients.ContainsKey(key) Then
            If MessageBox.Show(strAlertMessage, "Alert", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = System.Windows.Forms.DialogResult.Cancel Then
                Return False
            End If
        End If

        If lstClientName.SelectedIndex > -1 Then 'Selected client from OAC (Xml)
            'Dim value As Integer = (lstClientName.SelectedItem).key
            If finPowerOpObj.ClientXmlInfo.Clients.ContainsKey((lstClientName.SelectedItem).key) Then
                clientXML = finPowerOpObj.ClientXmlInfo.Clients.Item((lstClientName.SelectedItem).key)
                If clientXML IsNot Nothing Then
                    typeApplicantXml = clientXML.Type
                    XRefIdXml = clientXML.XREfId
                    ClientTypeIdXml = clientXML.ClientTypeId
                End If
            End If
        End If

        clientMapDetails.key = key
        clientMapDetails.ApplicantName = lstClientName.Text
        'Store the current state into HashTable by adding changes into clientfinPower Object 
        For Each row In dgvNames.Rows

            chkcell = row.Cells(0)
            If chkcell.EditedFormattedValue = True Then
                clientMapDetails.NameChecked = True
                'TODO:XML modify Case statement to use new Enum mainCustomerType
                Select Case currentCustomerType 'currentCustomerType = clientXml.CustomerType
                    Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                        clientMapDetails.ClientId = row.Cells("Client ID").Value
                        finPowerClientId = clientMapDetails.ClientId
                        clientMapDetails.CustomerType = row.Cells("Type").Value
                        clientMapDetails.FirstName = row.Cells("First Name").Value
                        clientMapDetails.MiddleNames = If(IsDBNull(row.Cells("Middle Names").Value), "", row.Cells("Middle Names").Value)
                        clientMapDetails.LastName = row.Cells("Last Name").Value
                        clientMapDetails.DateOfBirth = row.Cells("DOB").Value
                    Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                        clientMapDetails.ClientId = row.Cells("Client ID").Value
                        finPowerClientId = clientMapDetails.ClientId
                        clientMapDetails.CustomerType = row.Cells("Type").Value
                        clientMapDetails.LegalName = row.Cells("Legal Name").Value
                    Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                        clientMapDetails.ClientId = row.Cells("Client ID").Value
                        finPowerClientId = clientMapDetails.ClientId
                        clientMapDetails.CustomerType = row.Cells("Type").Value
                        clientMapDetails.LegalName = row.Cells("Company Name").Value
                    Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                        clientMapDetails.ClientId = row.Cells("Client ID").Value
                        finPowerClientId = clientMapDetails.ClientId
                        clientMapDetails.CustomerType = row.Cells("Type").Value
                        clientMapDetails.LegalName = row.Cells("Trust Name").Value
                End Select

                If chkFinManager.Checked = True Then
                    'finPOWER ManagerId
                    clientMapDetails.ManagerIdChecked = True
                    clientMapDetails.ManagerId = txtFinManager.Text
                Else
                    'Xml ManagerId
                    clientMapDetails.ManagerIdChecked = False
                    clientMapDetails.ManagerId = txtXmlManager.Text
                End If
                If chkFinAddress.Checked = True Then
                    'finPOWER address
                    clientMapDetails.AddressPhysicalChecked = True
                    clientMapDetails.AddressPhysical = txtfinAddress.Text
                    clientMapDetails.CityPhysical = txtfinSuburb.Text
                    clientMapDetails.StatePhysical = txtfinCity.Text
                    clientMapDetails.PostCodePhysical = txtfinPostCode.Text
                Else
                    'Xml Address
                    clientMapDetails.AddressPhysicalChecked = False
                    clientMapDetails.AddressPhysical = txtXmlAddress.Text
                    clientMapDetails.CityPhysical = txtXmlSuburb.Text
                    clientMapDetails.StatePhysical = txtXmlCity.Text
                    clientMapDetails.PostCodePhysical = txtXmlPostCode.Text
                End If

                If chkFinPostalAddress.Checked = True Then
                    clientMapDetails.AddressPostalChecked = True
                    clientMapDetails.AddressPostal = txtFinPostalAddress.Text
                    clientMapDetails.CityPostal = txtFinPostalSuburb.Text
                    clientMapDetails.StatePostal = txtFinPostalCity.Text
                    clientMapDetails.PostCodePostal = txtFinPostalPostcode.Text
                Else
                    clientMapDetails.AddressPostalChecked = False
                    clientMapDetails.AddressPostal = txtXmlPostalAddress.Text
                    clientMapDetails.CityPostal = txtXmlPostalSuburb.Text
                    clientMapDetails.StatePostal = txtXmlPostalCity.Text
                    clientMapDetails.PostCodePostal = txtXmlPostalPostcode.Text
                End If

                'For i = 0 To dgvContactMethod.RowCount - 1
                '    If dgvContactMethod.Rows(i).Cells(0).Value = True Then
                '        Select Case dgvContactMethod.Rows(i).Cells(1).Value
                '            Case "Work"
                '                clientfinPower.PhoneWorkChecked = True
                '                clientfinPower.PhoneWork = dgvContactMethod.Rows(i).Cells(2).Value
                '            Case "Phone"
                '                clientfinPower.PhoneHomeChecked = True
                '                clientfinPower.PhoneHome = dgvContactMethod.Rows(i).Cells(2).Value
                '            Case "Mobile"
                '                clientfinPower.PhoneMobileChecked = True
                '                clientfinPower.PhoneMobile = dgvContactMethod.Rows(i).Cells(2).Value
                '            Case "Email"
                '                clientfinPower.EmailChecked = True
                '                clientfinPower.Email = dgvContactMethod.Rows(i).Cells(2).Value

                '        End Select
                '    Else
                '        Select Case dgvContactMethod.Rows(i).Cells(1).Value
                '            Case "Work"
                '                clientfinPower.PhoneWorkChecked = False
                '                'clientfinPower.PhoneWork = dgvContactMethod.Rows(i).Cells(2).Value
                '            Case "Phone"
                '                clientfinPower.PhoneHomeChecked = False
                '                'clientfinPower.PhoneHome = dgvContactMethod.Rows(i).Cells(2).Value
                '            Case "Mobile"
                '                clientfinPower.PhoneMobileChecked = False
                '                'clientfinPower.PhoneMobile = dgvContactMethod.Rows(i).Cells(2).Value
                '            Case "Email"
                '                clientfinPower.EmailChecked = False
                '                'clientfinPower.Email = dgvContactMethod.Rows(i).Cells(2).Value

                '        End Select
                '    End If
                'Next
                clientMapDetails.Type = typeApplicantXml
                clientMapDetails.XREfId = XRefIdXml
                clientMapDetails.ClientTypeId = ClientTypeIdXml
                clientMapDetails.ContactMethodsOACXml = BuildSelectedContactMethodsOAC()
                clientMapDetails.ContactMethodsFinPower = BuildSelectedContactMethodsFinPOWER()

                If finPowerClients.ContainsKey(key) Then
                    finPowerClients.Remove(key)
                End If

                finPowerClients.Add(key, clientMapDetails)
            End If
        Next

        Return True

    End Function

    Private Sub RefreshUpdatedGrid()
        'dgvUpdated.AutoGenerateColumns = False
        Me.dgvUpdated.DataSource = BuildUpdatedDataTableFromSource()
        SetDatagridColumns()
    End Sub

    Private Sub SetDatagridColumns()
        Dim i As Integer
        Dim column As DataGridViewColumn
        Dim style As DataGridViewCellStyle = New DataGridViewCellStyle()

        If dgvContactsXML.RowCount > 0 And dgvContactsXML.ColumnCount > 1 Then
            dgvContactsXML.Columns(1).Width = (dgvContactsXML.Width / 2) - 15
            dgvContactsXML.Columns(2).Width = (dgvContactsXML.Width / 2) - 15
            dgvContactsXML.Columns(0).Width = 25
            dgvContactsXML.Columns(0).Name = "Check"
            dgvContactsXML.Columns(1).Name = "ContactMethodName"
            dgvContactsXML.Columns(2).Name = "ContactMethodValue"
        End If

        dgvNames.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvNames.MultiSelect = False

        style.Alignment = DataGridViewContentAlignment.MiddleCenter
        style.ForeColor = Color.Black
        style.BackColor = System.Drawing.SystemColors.Window
        style.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        dgvNames.ColumnHeadersVisible = True
        dgvNames.AutoGenerateColumns = False

        For Each column In dgvNames.Columns
            column.HeaderCell.Style = style
        Next

        If dgvNames.Columns.Count > 4 Then
            dgvNames.ColumnHeadersVisible = True
            dgvNames.Columns(1).Frozen = True
            dgvNames.Columns(3).Width = 300
            Select Case currentCustomerType
                Case Constants.CONST_OAC_CUSTOMERTYPE_INDIVIDUAL
                    dgvNames.Columns(1).HeaderText = "Client ID"
                    dgvNames.Columns(1).Name = "Client ID"
                    dgvNames.Columns(2).HeaderText = "Type"
                    dgvNames.Columns(2).Name = "Type"
                    dgvNames.Columns(3).HeaderText = "Legal Name"
                    dgvNames.Columns(3).Name = "Legal Name"
                    dgvNames.Columns(3).Visible = False
                    dgvNames.Columns(4).HeaderText = "First Name"
                    dgvNames.Columns(4).Name = "First Name"
                    dgvNames.Columns(4).Visible = True
                    dgvNames.Columns(5).HeaderText = "Middle Names"
                    dgvNames.Columns(5).Name = "Middle Names"
                    dgvNames.Columns(5).Visible = True
                    dgvNames.Columns(6).HeaderText = "Last Name"
                    dgvNames.Columns(6).Name = "Last Name"
                    dgvNames.Columns(6).Visible = True
                    dgvNames.Columns(7).HeaderText = "DOB"
                    dgvNames.Columns(7).Name = "DOB"
                    dgvNames.Columns(7).Visible = True
                    dgvNames.Columns(8).HeaderText = "Address"
                    dgvNames.Columns(8).Name = "PLAddress"
                    dgvNames.Columns(9).HeaderText = "Suburb"
                    dgvNames.Columns(9).Name = "PLSuburb"
                    dgvNames.Columns(10).HeaderText = "City"
                    dgvNames.Columns(10).Name = "PLCity"
                    dgvNames.Columns(11).HeaderText = "PostCode"
                    dgvNames.Columns(11).Name = "PLPostCode"
                    dgvNames.Columns(12).HeaderText = "Postal Address"
                    dgvNames.Columns(12).Name = "Address"
                    dgvNames.Columns(13).HeaderText = "Postal Suburb"
                    dgvNames.Columns(13).Name = "Suburb"
                    dgvNames.Columns(14).HeaderText = "Postal City"
                    dgvNames.Columns(14).Name = "City"
                    dgvNames.Columns(15).HeaderText = "Postal PostCode"
                    dgvNames.Columns(15).Name = "PostCode"
                    dgvNames.Columns(16).HeaderText = "Manager"
                    dgvNames.Columns(16).Name = "ManagerId"
                Case Constants.CONST_OAC_CUSTOMERTYPE_COMPANY
                    dgvNames.Columns(1).HeaderText = "Client ID"
                    dgvNames.Columns(1).Name = "Client ID"
                    dgvNames.Columns(2).HeaderText = "Type"
                    dgvNames.Columns(2).Name = "Type"
                    dgvNames.Columns(3).HeaderText = "Legal Name"
                    dgvNames.Columns(3).Name = "Legal Name"
                    dgvNames.Columns(3).Visible = True
                    dgvNames.Columns(4).HeaderText = "First Name"
                    dgvNames.Columns(4).Name = "First Name"
                    dgvNames.Columns(4).Visible = False
                    dgvNames.Columns(5).HeaderText = "Middle Names"
                    dgvNames.Columns(5).Name = "Middle Names"
                    dgvNames.Columns(5).Visible = False
                    dgvNames.Columns(6).HeaderText = "Last Name"
                    dgvNames.Columns(6).Name = "Last Name"
                    dgvNames.Columns(6).Visible = False
                    dgvNames.Columns(7).HeaderText = "DOB"
                    dgvNames.Columns(7).Name = "DOB"
                    dgvNames.Columns(7).Visible = False
                    dgvNames.Columns(8).HeaderText = "Address"
                    dgvNames.Columns(8).Name = "PLAddress"
                    dgvNames.Columns(9).HeaderText = "Suburb"
                    dgvNames.Columns(9).Name = "PLSuburb"
                    dgvNames.Columns(10).HeaderText = "City"
                    dgvNames.Columns(10).Name = "PLCity"
                    dgvNames.Columns(11).HeaderText = "PostCode"
                    dgvNames.Columns(11).Name = "PLPostCode"
                    dgvNames.Columns(12).HeaderText = "Postal Address"
                    dgvNames.Columns(12).Name = "Address"
                    dgvNames.Columns(13).HeaderText = "Postal Suburb"
                    dgvNames.Columns(13).Name = "Suburb"
                    dgvNames.Columns(14).HeaderText = "Postal City"
                    dgvNames.Columns(14).Name = "City"
                    dgvNames.Columns(15).HeaderText = "Postal PostCode"
                    dgvNames.Columns(15).Name = "PostCode"
                    dgvNames.Columns(16).HeaderText = "Manager"
                    dgvNames.Columns(16).Name = "ManagerId"
                Case Constants.CONST_OAC_CUSTOMERTYPE_PARTNERSHIP
                    dgvNames.Columns(1).HeaderText = "Client ID"
                    dgvNames.Columns(1).Name = "Client ID"
                    dgvNames.Columns(2).HeaderText = "Type"
                    dgvNames.Columns(2).Name = "Type"
                    dgvNames.Columns(3).HeaderText = "Company Name"
                    dgvNames.Columns(3).Name = "Company Name"
                    dgvNames.Columns(3).Visible = True
                    dgvNames.Columns(4).HeaderText = "First Name"
                    dgvNames.Columns(4).Name = "First Name"
                    dgvNames.Columns(4).Visible = False
                    dgvNames.Columns(5).HeaderText = "Middle Names"
                    dgvNames.Columns(5).Name = "Middle Names"
                    dgvNames.Columns(5).Visible = False
                    dgvNames.Columns(6).HeaderText = "Last Name"
                    dgvNames.Columns(6).Name = "Last Name"
                    dgvNames.Columns(6).Visible = False
                    dgvNames.Columns(7).HeaderText = "DOB"
                    dgvNames.Columns(7).Name = "DOB"
                    dgvNames.Columns(7).Visible = False
                    dgvNames.Columns(8).HeaderText = "Address"
                    dgvNames.Columns(8).Name = "PLAddress"
                    dgvNames.Columns(9).HeaderText = "Suburb"
                    dgvNames.Columns(9).Name = "PLSuburb"
                    dgvNames.Columns(10).HeaderText = "City"
                    dgvNames.Columns(10).Name = "PLCity"
                    dgvNames.Columns(11).HeaderText = "PostCode"
                    dgvNames.Columns(11).Name = "PLPostCode"
                    dgvNames.Columns(12).HeaderText = "Postal Address"
                    dgvNames.Columns(12).Name = "Address"
                    dgvNames.Columns(13).HeaderText = "Postal Suburb"
                    dgvNames.Columns(13).Name = "Suburb"
                    dgvNames.Columns(14).HeaderText = "Postal City"
                    dgvNames.Columns(14).Name = "City"
                    dgvNames.Columns(15).HeaderText = "Postal PostCode"
                    dgvNames.Columns(15).Name = "PostCode"
                    dgvNames.Columns(16).HeaderText = "Manager"
                    dgvNames.Columns(16).Name = "ManagerId"
                Case Constants.CONST_OAC_CUSTOMERTYPE_TRUST
                    dgvNames.Columns(1).HeaderText = "Client ID"
                    dgvNames.Columns(1).Name = "Client ID"
                    dgvNames.Columns(2).HeaderText = "Type"
                    dgvNames.Columns(2).Name = "Type"
                    dgvNames.Columns(3).HeaderText = "Trust Name"
                    dgvNames.Columns(3).Name = "Trust Name"
                    dgvNames.Columns(3).Visible = True
                    dgvNames.Columns(4).HeaderText = "First Name"
                    dgvNames.Columns(4).Name = "First Name"
                    dgvNames.Columns(4).Visible = False
                    dgvNames.Columns(5).HeaderText = "Middle Names"
                    dgvNames.Columns(5).Name = "Middle Names"
                    dgvNames.Columns(5).Visible = False
                    dgvNames.Columns(6).HeaderText = "Last Name"
                    dgvNames.Columns(6).Name = "Last Name"
                    dgvNames.Columns(6).Visible = False
                    dgvNames.Columns(7).HeaderText = "DOB"
                    dgvNames.Columns(7).Name = "DOB"
                    dgvNames.Columns(7).Visible = False
                    dgvNames.Columns(8).HeaderText = "Address"
                    dgvNames.Columns(8).Name = "PLAddress"
                    dgvNames.Columns(9).HeaderText = "Suburb"
                    dgvNames.Columns(9).Name = "PLSuburb"
                    dgvNames.Columns(10).HeaderText = "City"
                    dgvNames.Columns(10).Name = "PLCity"
                    dgvNames.Columns(11).HeaderText = "PostCode"
                    dgvNames.Columns(11).Name = "PLPostCode"
                    dgvNames.Columns(12).HeaderText = "Postal Address"
                    dgvNames.Columns(12).Name = "Address"
                    dgvNames.Columns(13).HeaderText = "Postal Suburb"
                    dgvNames.Columns(13).Name = "Suburb"
                    dgvNames.Columns(14).HeaderText = "Postal City"
                    dgvNames.Columns(14).Name = "City"
                    dgvNames.Columns(15).HeaderText = "Postal PostCode"
                    dgvNames.Columns(15).Name = "PostCode"
                    dgvNames.Columns(16).HeaderText = "Manager"
                    dgvNames.Columns(16).Name = "ManagerId"
            End Select

        Else
            dgvNames.ColumnHeadersVisible = False
        End If
        For i = 1 To dgvNames.Columns.Count - 1
            dgvNames.Columns(i).ReadOnly = True
        Next
        If dgvContactMethodFin.RowCount > 0 And dgvContactMethodFin.ColumnCount > 1 Then
            dgvContactMethodFin.Columns(2).Width = (dgvContactMethodFin.Width / 2) - 15
            dgvContactMethodFin.Columns(3).Width = (dgvContactMethodFin.Width / 2) - 15
            dgvContactMethodFin.Columns(0).Width = 25
            dgvContactMethodFin.Columns(1).Width = 0
            dgvContactMethodFin.Columns(1).Visible = False
            dgvContactMethodFin.Columns(0).Name = "Check"
            dgvContactMethodFin.Columns(1).Name = "ContactMethodId"
            dgvContactMethodFin.Columns(2).Name = "ContactMethodName"
            dgvContactMethodFin.Columns(3).Name = "ContactMethodValue"
        End If
        For Each column In dgvUpdated.Columns
            column.HeaderCell.Style = style
        Next
        If dgvUpdated.RowCount > 0 Then
            dgvUpdated.Columns("key").Width = 0
            dgvUpdated.Columns("key").Visible = False
            dgvUpdated.ColumnHeadersVisible = True
        End If

    End Sub

    Private Function BuildContactXMLDataTable(ByVal clientfinObj As ClientMappingDetails) As DataTable
        Dim table As New DataTable

        ' Create columns in the DataTable.
        table.Columns.Add("Method", GetType(String))
        table.Columns.Add("Value", GetType(String))

        'Add Values in Table
        For Each clientContactMethod As ContactMethods In clientfinObj.ContactMethodsOACXml
            If (clientContactMethod IsNot Nothing) Then
                table.Rows.Add(clientContactMethod.MethodName, clientContactMethod.MethodValue)
            End If
        Next

        Return table

    End Function


    Private Function BuildUpdatedDataTableFromSource() As DataTable
        Dim clientfinPow As ClientMappingDetails
        Dim i As Integer
        Dim table As New DataTable

        ' Create four typed columns in the DataTable.
        table.Columns.Add("key", GetType(String))
        table.Columns.Add("ClientID", GetType(String))
        table.Columns.Add("Name", GetType(String))



        'table.Columns.Add("LegalName", GetType(String))
        'table.Columns.Add("CompanyName", GetType(String))
        'table.Columns.Add("FirstName", GetType(String), "")
        'table.Columns.Add("MiddleNames", GetType(String), "")
        'table.Columns.Add("LastName", GetType(String), "")
        'table.Columns.Add("DateofBirth", GetType(String), "")
        'table.Columns.Add("Phone", GetType(String), "") 'PhoneHome
        'table.Columns.Add("Mobile", GetType(String), "")
        'table.Columns.Add("Work", GetType(String), "")
        'table.Columns.Add("Email", GetType(String), "")
        'table.Columns.Add("Address", GetType(String), "")
        'table.Columns.Add("Suburb", GetType(String), "")
        'table.Columns.Add("City", GetType(String), "")
        'table.Columns.Add("Postcode", GetType(String), "")
        'table.Columns.Add("PLAddress", GetType(String), "")
        'table.Columns.Add("PLSuburb", GetType(String), "")
        'table.Columns.Add("PLCity", GetType(String), "")
        'table.Columns.Add("PLPostcode", GetType(String), "")
        'kvp = lstClientName.Text

        For Each i In finPowerClients.Keys
            clientfinPow = finPowerClients(i)
            table.Rows.Add(i, clientfinPow.ClientId, clientfinPow.ApplicantName)
            'table.Rows.Add(i, clientfinPow.ClientId, clientfinPow.ApplicantName, clientfinPow.LegalName, clientfinPow.CompanyName, _
            '               clientfinPow.FirstName, clientfinPow.MiddleNames, clientfinPow.LastName, clientfinPow.DateOfBirth, _
            '               clientfinPow.Address, clientfinPow.City, clientfinPow.State, clientfinPow.PostCode, _
            '               clientfinPow.PLAddress, clientfinPow.PLCity, clientfinPow.PLState, clientfinPow.PLPostCode)
        Next
        'clientfinPow.PhoneHome, clientfinPow.PhoneMobile, clientfinPow.PhoneWork, clientfinPow.Email, _
        Return table

    End Function

    Private Sub SetUpdatedGridColumn()
        Dim Width As Integer

        If dgvUpdated.Rows.Count > 0 Then
            Width = dgvUpdated.Width
            dgvUpdated.Columns("ClientID").Width = (Width * 1 / 5)
            'dgvUpdated.Columns("Name").Width = (Width * 4 / 5) - 30
            dgvUpdated.Columns("Name").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        End If

    End Sub


    Private Sub dgvUpdated_CellClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvUpdated.CellClick
        Dim cell As DataGridViewImageCell = Nothing
        Dim currkey As Integer
        Dim clientfinPower As New ClientMappingDetails

        If e.ColumnIndex = 0 Or e.ColumnIndex = 1 Then
            'For only image columns (first two columns in the grid)
            'Get Image Cell
            cell = CType(dgvUpdated.Rows(e.RowIndex).Cells(e.ColumnIndex), DataGridViewImageCell)
        End If

        If Not (cell Is Nothing) Then
            'remove selected changes
            If e.ColumnIndex = 0 Then
                'Check if Image cell is clicked to remove the record
                currkey = dgvUpdated.Rows(e.RowIndex).Cells("key").Value
                If finPowerClients.ContainsKey(currkey) Then
                    finPowerClients.Remove(currkey)
                    RefreshUpdatedGrid()
                End If
            End If
            If e.ColumnIndex = 1 Then
                currkey = dgvUpdated.Rows(e.RowIndex).Cells("key").Value
                If finPowerClients.ContainsKey(currkey) Then
                    'View modified client details in the updated list in  popup window
                    Dim frmClientDetails As frmSelectedClientDetails
                    frmClientDetails = New frmSelectedClientDetails(finPowerClients.Item(currkey))
                    frmClientDetails.ShowDialog()
                End If
            End If
        Else
            'Refresh the form with the selected Client data
            LoadModifiedData()
        End If

    End Sub


    Private Sub dgvUpdated_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvUpdated.SelectionChanged
        'Refresh the form with the selected Client data
        LoadModifiedData()
    End Sub

    Private Sub dgvUpdated_CellFormatting(ByVal sender As Object, ByVal e As DataGridViewCellFormattingEventArgs) Handles dgvUpdated.CellFormatting
        Dim cell As DataGridViewImageCell = Nothing
        If e.ColumnIndex = 0 Or e.ColumnIndex = 1 Then
            'For only image columns (first two columns in the grid)
            'Get Image Cell
            cell = CType(dgvUpdated.Rows(e.RowIndex).Cells(e.ColumnIndex), DataGridViewImageCell)
        End If
        'Setup tooltip
        If Not (cell Is Nothing) Then
            If e.ColumnIndex = 0 Then
                With Me.dgvUpdated.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    .ToolTipText = "Remove Selected Client"
                End With
            End If
            If e.ColumnIndex = 1 Then
                With Me.dgvUpdated.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    .ToolTipText = "Show Selected Client Details"
                End With
            End If
        End If


    End Sub

    ''' <summary>
    ''' Selects the Row in the UpdatedGrid and updates the form
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SelectRowinUpdatedGrid()

        Dim i As Integer
        If finPowerClients.ContainsKey(prevkey) Then
            For i = 0 To dgvUpdated.Rows.Count - 1
                If dgvUpdated.Rows(i).Cells("key").Value = prevkey Then
                    dgvUpdated.Rows(i).Selected = True
                    'DataGridView1.CurrentCell = DataGridView1.Rows(MyDesiredIndex).Cells(0)
                    dgvUpdated.CurrentCell = dgvUpdated.Rows(i).Cells(0)

                    LoadModifiedData(False)
                End If
            Next
        End If
        SetUpdatedGridColumn()
    End Sub

    ''' <summary>
    ''' Updates the form with the data selected in the Updated Grid
    ''' </summary>
    ''' <param name="selected"></param>
    ''' <remarks></remarks>
    Public Sub LoadModifiedData(Optional ByVal selected As Boolean = True)
        Dim tempClientId As String = ""
        Dim tempName As String = ""
        Dim tempKey As Integer = 0
        Dim i As Integer = 0

        Dim clientMappingDetails As ClientMappingDetails = Nothing

        If Not dgvUpdated.CurrentRow Is Nothing Then

            gpbxfinPowDetails.Enabled = True

            If Not IsDBNull(dgvUpdated.CurrentRow.Cells("ClientID").Value) Then
                tempClientId = dgvUpdated.CurrentRow.Cells("ClientID").Value
            End If
            If Not IsDBNull(dgvUpdated.CurrentRow.Cells("Name").Value) Then
                tempName = dgvUpdated.CurrentRow.Cells("Name").Value
            End If
            If Not IsDBNull(dgvUpdated.CurrentRow.Cells("key").Value) Then
                tempKey = dgvUpdated.CurrentRow.Cells("key").Value
            End If

            If finPowerClients.ContainsKey(tempKey) Then
                clientMappingDetails = CType(finPowerClients.Item(tempKey), ClientMappingDetails)
            End If

            If selected Then
                For i = 0 To lstClientName.Items.Count - 1
                    If lstClientName.Items(i).key = tempKey Then
                        lstClientName.SetSelected(i, True)
                    End If
                Next
            End If

            'Update Client Names Grid
            If Not tempName = "" Then
                For i = 0 To dgvNames.RowCount - 1
                    If dgvNames.Rows(i).Cells("Client ID").Value = tempClientId Then
                        dgvNames.Rows(i).Cells("Check").Value = True
                        dgvNames.Rows(i).Selected = True
                    Else
                        dgvNames.Rows(i).Cells("Check").Value = False
                    End If
                Next
            End If
            'Address and ContactMethods selection
            If Not clientMappingDetails Is Nothing Then
                'load up finPower details for selected dgvName
                GetfinPowerAddressAndContactDetails()

                chkFinManager.Checked = clientMappingDetails.ManagerIdChecked
                chkFinAddress.Checked = clientMappingDetails.AddressPhysicalChecked
                chkFinPostalAddress.Checked = clientMappingDetails.AddressPostalChecked

                SetSelectedContactMethods(clientMappingDetails)

                'For i = 0 To dgvContactMethod.RowCount - 1
                'Select Case (dgvContactMethod.Rows(i).Cells(1).Value).ToString.ToLower
                '    Case "phone"

                '        dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.PhoneHomeChecked

                '    Case "mobile"
                '        dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.PhoneMobileChecked

                '    Case "work"

                '        dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.PhoneWorkChecked
                '    Case "email"

                '        dgvContactMethod.Rows(i).Cells(0).Value = ClientfinPowerObj.EmailChecked

                'End Select
                'next
            End If
        End If

        dgvUpdated.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvUpdated.MultiSelect = False

    End Sub

    ''' <summary>
    ''' Sets the checkbox in the ContactMethod grid checked based on the selection
    ''' </summary>
    ''' <param name="ClientfinPowerObj"></param>
    ''' <remarks></remarks>
    Private Sub SetSelectedContactMethods(ByVal ClientfinPowerObj As ClientMappingDetails)
        'Checkbox in the ContactMethod grid will be checked based on the selection

        Dim ContactMethodsOACXml As New List(Of ContactMethods)
        Dim ContactMethodsFinPOWER As New List(Of ContactMethods)
        Dim Contactmethod As ContactMethods = Nothing
        Dim ie As IEnumerator
        Dim i As Integer = 0

        ContactMethodsOACXml = ClientfinPowerObj.ContactMethodsOACXml
        ContactMethodsFinPOWER = ClientfinPowerObj.ContactMethodsFinPower

        'Reset all Checkboxes to Unselected
        For i = 0 To dgvContactMethodFin.RowCount - 1
            dgvContactMethodFin.Rows(i).Cells(0).Value = False
        Next

        For i = 0 To dgvContactsXML.RowCount - 1
            dgvContactsXML.Rows(i).Cells(0).Value = False
        Next

        'FinPower Contact Method
        If ContactMethodsFinPOWER IsNot Nothing Then
            ie = ContactMethodsFinPOWER.GetEnumerator
            While ie.MoveNext
                Contactmethod = TryCast(ie.Current, ContactMethods)
                If Contactmethod IsNot Nothing Then
                    For i = 0 To dgvContactMethodFin.RowCount - 1
                        'Matches the MethodName and checks the checkbox
                        If dgvContactMethodFin.Rows(i).Cells("ContactMethodName").Value = Contactmethod.MethodName And dgvContactMethodFin.Rows(i).Cells("ContactMethodValue").Value = Contactmethod.MethodValue Then
                            dgvContactMethodFin.Rows(i).Cells(0).Value = True
                        End If
                    Next
                End If
            End While
        End If

        'MembersCentre Contact Method
        If ContactMethodsOACXml IsNot Nothing Then
            ie = ContactMethodsOACXml.GetEnumerator
            While ie.MoveNext
                Contactmethod = TryCast(ie.Current, ContactMethods)
                If Contactmethod IsNot Nothing Then
                    For i = 0 To dgvContactsXML.RowCount - 1
                        'Matches the MethodName and checks the checkbox
                        If dgvContactsXML.Rows(i).Cells("ContactMethodName").Value = Contactmethod.MethodName And _
                                            dgvContactsXML.Rows(i).Cells("ContactMethodValue").Value = Contactmethod.MethodValue Then
                            dgvContactsXML.Rows(i).Cells(0).Value = True
                        End If
                    Next
                End If
            End While
        End If

        ''FinPower Contact Method
        'If ContactMethodsFinPOWER IsNot Nothing Then
        '    For i = 0 To dgvContactMethod.RowCount - 1
        '        dgvContactMethod.Rows(i).Cells(0).Value = False
        '        If ContactMethodsFinPOWER.Contains(i) Then
        '            Contactmethod = ContactMethodsFinPOWER.Item(i)
        '            If Contactmethod IsNot Nothing Then
        '                dgvContactMethod.Rows(i).Cells(0).Value = True

        '            End If
        '        End If
        '    Next
        'End If
        ''MembersCentre Contact Method
        'If ContactMethodsMembersCentre IsNot Nothing Then
        '    For i = 0 To dgvContactsXML.RowCount - 1
        '        dgvContactsXML.Rows(i).Cells(0).Value = False
        '        If ContactMethodsMembersCentre.Contains(i) Then
        '            Contactmethod = ContactMethodsMembersCentre.Item(i)
        '            If Contactmethod IsNot Nothing Then
        '                dgvContactsXML.Rows(i).Cells(0).Value = True
        '            End If
        '        End If
        '    Next
        'End If
    End Sub

    ''' <summary>
    ''' Updates the OAC XML file and saves into the finPOWER Import Folder
    ''' Update Client mapping details in Due Diligence table
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks>Depends on finPowerClients</remarks>
    Private Function UpdateXMLFile() As Boolean
        Dim ClientfinUpd As New ClientMappingDetails
        Dim status As Boolean

        'Assign finPower ClientId to TrueTrack ClientNum
        status = finPowerOpObj.UpdateXMLFileMappingData(finPowerClients, EnquiryId)

        'Update LoanCode in DueDiligence
        '17/02/2017 added filter for FinanceFacility( no loan)
        If Not EnquiryType = MyEnums.EnquiryType.FinanceFacility Then
            finPowerOpObj.UpdateLoanDetails(EnquiryId, finPowerOpObj.LoanCode)
        End If

        Return status

    End Function

    'Show error message
    Private Sub ShowOperationErrorMessage(finPowerOpObj As finPowerOps)

        If (finPowerOpObj IsNot Nothing) Then

            If (finPowerOpObj.ClientXmlInfo.Status = False And String.IsNullOrEmpty(finPowerOpObj.ClientXmlInfo.ErrorMessage)) Then
                MessageBox.Show(finPowerOpObj.ClientXmlInfo.ErrorMessage, "Xml parsing error", MessageBoxButtons.OK)
                finPowerOpObj.ClientXmlInfo.ErrorMessage = ""
            End If
        End If

    End Sub



    ''' <summary>
    ''' Formats phone numbers
    ''' </summary>
    ''' <param name="strPhone"></param>
    ''' <param name="type">Mobile, Work or Home</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FormatPhone(ByVal strPhone As String, ByVal type As String) As String

        Dim tempPhone As String = ""
        If type = "Mobile" Then
            If strPhone.Length > 3 Then
                tempPhone = "(" & strPhone.Substring(0, 3) & ")"
            Else
                tempPhone = "(" & strPhone.Substring(0, strPhone.Length) & ")"
            End If
            If strPhone.Length > 3 Then
                If strPhone.Length < 7 Then
                    tempPhone += " " & strPhone.Substring(3, strPhone.Length - 3)
                Else
                    tempPhone += " " & strPhone.Substring(3, 2)
                End If
            End If
            If strPhone.Length > 6 Then
                If strPhone.Length < 10 Then
                    tempPhone += " " & strPhone.Substring(6, strPhone.Length - 6)
                Else
                    tempPhone += " " & strPhone.Substring(6, 4)
                End If
            End If
        ElseIf type = "Work" Or type = "Home" Then
            If strPhone.Length > 2 Then
                tempPhone = "(" & strPhone.Substring(0, 2) & ")"
            Else
                tempPhone = "(" & strPhone.Substring(0, strPhone.Length) & ")"
            End If
            If strPhone.Length > 2 Then
                If strPhone.Length < 6 Then
                    tempPhone += " " & strPhone.Substring(2, strPhone.Length - 2)
                Else
                    tempPhone += " " & strPhone.Substring(2, 3)
                End If
            End If
            If strPhone.Length > 5 Then
                If strPhone.Length < 10 Then
                    tempPhone += " " & strPhone.Substring(5, strPhone.Length - 5)
                Else
                    tempPhone += " " & strPhone.Substring(5, 4)
                End If
            End If
        End If


        Return tempPhone

    End Function


    Private Sub lstClientName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstClientName.SelectedIndexChanged
        RemoveHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged

        If lstClientName.SelectedIndex > -1 Then
            prevkey = lstClientName.SelectedItem.key

            'lblfinNameForXMName.Text = "for " & lstClientName.SelectedItem.value
            lblDatafinPOWER.Text = "Matches from finPOWER for " & lstClientName.SelectedItem.value
            'gpbxfinPower.Name = "Matches from finPOWER for " & lstClientName.SelectedItem.value 'does not work??

            'Clear current display
            ClearFinPowerAddressAndContactDetails()
            'Get details from finPower
            GetSelectedNameInfo()
            'unset dgvNames selection
            dgvNames.ClearSelection()

            'Set Grid properties
            SetDatagridColumns()

            'Show selected values in Grid
            SelectRowinUpdatedGrid()
            
            'Set button text
            btnAddtoGrid.ForeColor = Color.Black
            btnAddtoGrid.BackColor = Color.YellowGreen

        End If
        AddHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged
    End Sub

    ''' <summary>
    ''' Get data from FinPower for the selected Name in the ListBox
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetSelectedNameInfo()
        Dim i As Integer
        Dim clientXml As ClientMappingDetails
        Dim myDataSet As New Data.DataSet

        chkFinAddress.Checked = False
        chkFinPostalAddress.Checked = False
        chkFinManager.Checked = False
        txtXmlManager.Text = ""
        i = lstClientName.SelectedItem.key

        dgvContactMethodFin.DataSource = Nothing

        If finPowerOpObj.ClientXmlInfo.Clients.ContainsKey(i) Then
            clientXml = CType(finPowerOpObj.ClientXmlInfo.Clients.Item(i), ClientMappingDetails)
            currentCustomerType = clientXml.CustomerType
            txtDOB.Text = String.Format("{0:dd/MM/yyyy}", clientXml.DateOfBirth)
            txtFirstName.Text = clientXml.FirstName
            txtLastName.Text = clientXml.LastName

            txtXmlAddress.Text = clientXml.AddressPhysical
            txtXmlSuburb.Text = clientXml.CityPhysical
            txtXmlCity.Text = clientXml.StatePhysical
            txtXmlPostCode.Text = clientXml.PostCodePhysical

            txtXmlPostalAddress.Text = clientXml.AddressPostal
            txtXmlPostalSuburb.Text = clientXml.CityPostal
            txtXmlPostalCity.Text = clientXml.StatePostal
            txtXmlPostalPostcode.Text = clientXml.PostCodePostal

            txtXmlManager.Text = clientXml.ManagerId

            'Bind ContactMethods XML
            dgvContactsXML.DataSource = BuildContactXMLDataTable(clientXml)

            'Get data from finPOWER
            myDataSet = finPowerOpObj.GetfinPOWERClientInfo(clientXml)
            Me.dgvNames.DataSource = Nothing
            Me.dgvNames.Refresh()

            Me.dgvNames.DataSource = myDataSet.Tables(0)
            TryCast(dgvNames.BindingContext(myDataSet.Tables(0)), CurrencyManager).Refresh()

            'Set Grid Properties
            SetDatagridColumns()

            'Select the row in the Modified data UpdatedGrid
            If Not isShowSelected Then 'isShowSelected is no set anywhere at moment
                For i = 0 To dgvUpdated.RowCount - 1
                    If dgvUpdated.Rows(i).Cells("key").Value = prevkey And dgvUpdated.Rows(i).Selected = False Then
                        dgvUpdated.Rows(i).Selected = True
                    Else
                        dgvUpdated.Rows(i).Selected = False
                    End If
                Next
            End If
        Else
            Me.dgvNames.DataSource = Nothing

        End If


    End Sub

    ''' <summary>
    ''' Commit returns DialogResult.OK if records as selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommit.Click

        If dgvUpdated.RowCount = 0 Then

            If MsgBox("No finPOWER client records selected. Do you want to continue? ", MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.No Then
                DialogResult = System.Windows.Forms.DialogResult.None
                Exit Sub
            Else
                'Saves the changes to XML and updates LoanNum into DueDiligence
                If UpdateXMLFile() Then
                    DialogResult = System.Windows.Forms.DialogResult.OK
                    Me.Close()
                End If
            End If
        Else
            'Saves the changes to XML and updates LoanNum into DueDeligence
            If UpdateXMLFile() Then
                DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If


    End Sub

    ''' <summary>
    ''' Set visibility of grid header
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FormLoadSetting()
        'set visibility of grid header
        Me.dgvUpdated.ColumnHeadersVisible = False
        Me.dgvNames.ColumnHeadersVisible = False

    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Me.Close()

    End Sub

    ''' <summary>
    ''' Adds the selections made into the temporary Updated Grid and Saves the ClientEntity Object in HashTable
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Click to save your selections</remarks>
    Private Sub btnAddtoGrid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoGrid.Click
        RemoveHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged

        Dim icnt As Integer
        Dim jKount As Integer
        Dim tClientId As String = ""
        Dim chkcell As DataGridViewCheckBoxCell
        Dim row As DataGridViewRow

        'validate selections
        For Each row In dgvNames.Rows
            chkcell = row.Cells("Check")
            If chkcell.EditedFormattedValue = True Then
                icnt = 1
            End If
        Next
        If icnt = 0 Then
            MessageBox.Show("Please select a client to update", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        ElseIf icnt > 1 Then
            MessageBox.Show("Cannot select more than one client to update", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        'Add changes into ClientEntity Object and adds into the HashTable  
        If AddClientChanges(prevkey) Then
            RefreshUpdatedGrid()

            For icnt = 0 To dgvUpdated.RowCount - 1
                If dgvUpdated.Rows(icnt).Cells("key").Value = prevkey Then
                    dgvUpdated.Rows(icnt).Selected = True
                Else
                    dgvUpdated.Rows(icnt).Selected = False
                End If
            Next
        Else 'User Opts not to add in the UpdatedGrid if the modified Client is already exists in the UpdatedGrid
            For icnt = 0 To dgvUpdated.RowCount - 1
                If dgvUpdated.Rows(icnt).Selected = True Then
                    tClientId = dgvUpdated.Rows(icnt).Cells("ClientID").Value

                    For jKount = 0 To dgvNames.Rows.Count - 1
                        chkcell = dgvNames.Rows(jKount).Cells(0)
                        If dgvNames.Rows(jKount).Cells("Client ID").Value = tClientId Then
                            dgvNames.Rows(jKount).Cells("Check").Value = True
                            dgvNames.Rows(jKount).Selected = True
                        Else
                            dgvNames.Rows(jKount).Cells("Check").Value = False
                        End If
                    Next
                    Exit For
                End If
            Next
        End If

        SetUpdatedGridColumn()

        AddHandler dgvUpdated.SelectionChanged, AddressOf dgvUpdated_SelectionChanged
    End Sub


End Class



''' <summary>
''' Client details to store mapping FinPower Client data and OAC customer in XML
''' </summary>
''' <remarks>Class used in ValidateXML</remarks>
Public Class ClientMappingDetails : Implements IEquatable(Of ClientMappingDetails)
    Public key As Integer
    Public ClientId As String 'C12345, RKINGM
    Public ApplicantName As String 'Name in the XML file listed in Listbox
    Public Type As String 'main, co-main, guarantor
    Public CustomerType As String 'individual, company, trust, partnership
    Public FirstName As String
    Public MiddleNames As String
    Public LastName As String
    Public DateOfBirth As String
    
    Public LegalName As String
    Public CompanyName As String

    'Public Email As String
    Public AddressPhysical As String
    Public CityPhysical As String
    Public StatePhysical As String
    Public PostCodePhysical As String
    Public AddressPostal As String
    Public CityPostal As String
    Public StatePostal As String
    Public PostCodePostal As String

    Public Comment As String
    Public Sequence As Integer
    Public NameChecked As Boolean
    Public AddressPhysicalChecked As Boolean
    Public AddressPostalChecked As Boolean

    Public ManagerIdChecked As Boolean
    Public ManagerId As String
   
    Public ContactMethodsFinPower As List(Of ContactMethods)
    Public ContactMethodsOACXml As List(Of ContactMethods)

    Public XREfId As String 'added 09/09/2016, to link TrueTrack Customer to OAC Customer
    Public ClientTypeId As String 'added 10/02/2017 for when importing Clients Only (Finance Facility)

    Public Overloads Function Equals(client As ClientMappingDetails) As Boolean _
                    Implements IEquatable(Of ClientMappingDetails).Equals
        If client Is Nothing Then Return False

        If Me.key = client.key Then
            Return True
        Else
            Return False
        End If
    End Function

End Class

''' <summary>
''' Class for storing Contact Methods
''' </summary>
''' <remarks></remarks>
Public Class ContactMethods : Implements IEquatable(Of ContactMethods)

    'constructors
    Public Sub New(ByVal _methodName As String, ByVal _methodValue As String)
        MethodName = _methodName
        MethodValue = _methodValue
    End Sub

    'Public Sub New(ByVal _methodId As Integer, ByVal _methodName As String, ByVal _methodValue As String)
    '    MethodId = _methodId
    '    MethodName = _methodName
    '    MethodValue = _methodValue
    'End Sub

    'properties
    Public MethodName As String
    Public MethodValue As String
    'Public MethodId As Integer

    Public Overloads Function Equals(contactMethod As ContactMethods) As Boolean _
                Implements IEquatable(Of ContactMethods).Equals
        If contactMethod Is Nothing Then Return False

        If Me.MethodName = contactMethod.MethodName And Me.MethodValue = contactMethod.MethodValue Then
            Return True
        Else
            Return False
        End If
    End Function

    

End Class

