﻿Public Class FormAddDTSB

    Dim thisId As Integer
    Dim thisOpType As Integer
    Dim thisCustomerId As Integer
    Dim thisDtsbType As Integer

    ''' <summary>
    ''' Loads form with parameters from Form1
    ''' </summary>
    ''' <param name="recordId"></param>
    ''' <param name="dtsbOpType">0 = update, 1 = insert</param>
    ''' <param name="dtsbCustomerId"></param>
    ''' <param name="dtsbType">0 = Director, 1 = Trustee, 2 = Shareholder, 3 = Beneficary</param>
    ''' <remarks></remarks>
    Friend Sub PassVariable(ByVal recordId As Integer, ByVal dtsbOpType As Integer, Optional ByVal dtsbCustomerId As Integer = 0, Optional ByVal dtsbType As Integer = 0)
        thisId = recordId
        thisOpType = dtsbOpType
        thisCustomerId = dtsbCustomerId
        thisDtsbType = dtsbType
        'load data 
        Me.Enum_AMLRiskTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.Enum_AMLRisk)

        'load datasources for combo boxes
        'With cmbxAMLRisk
        '    .DataSource = MyEnums.ToList(GetType(MyEnums.AMLRisk))
        '    .ValueMember = "Key"
        '    .DisplayMember = "Value"
        'End With
        'cmbxAMLRisk.DisplayMember = "Value"
        'cmbxAMLRisk.ValueMember = "Key"
        'cmbxAMLRisk.DataSource = MyEnums.ToList(GetType(MyEnums.AMLRisk))

        'Show hide IsaPEP
        Select Case dtsbType
            Case MyEnums.DtsbType.ShareHolder
                lblIsaPEP.Visible = True
                ckbxIsaPEP.Visible = True
                ckbxIsaPEP.Enabled = True
                lblAMLRisk.Visible = True
                cmbxAMLRisk.Visible = True
                cmbxAMLRisk.Enabled = True
            Case MyEnums.DtsbType.Beneficiary
                lblIsaPEP.Visible = True
                ckbxIsaPEP.Visible = True
                ckbxIsaPEP.Enabled = True
                lblAMLRisk.Visible = False
                cmbxAMLRisk.Visible = False
                cmbxAMLRisk.Enabled = False
            Case MyEnums.DtsbType.Director
                lblIsaPEP.Visible = False
                ckbxIsaPEP.Visible = False
                ckbxIsaPEP.Enabled = False
                lblAMLRisk.Visible = True
                cmbxAMLRisk.Visible = True
                cmbxAMLRisk.Enabled = True
            Case Else
                lblIsaPEP.Visible = False
                ckbxIsaPEP.Visible = False
                ckbxIsaPEP.Enabled = False
                lblAMLRisk.Visible = False
                cmbxAMLRisk.Visible = False
                cmbxAMLRisk.Enabled = False
        End Select


        If dtsbOpType = 0 Then 'Update
            btnSave.Visible = True
            btnInsert.Visible = False
            Me.Text = "Update " & MyEnums.GetDescription(DirectCast(thisDtsbType, MyEnums.DtsbType))
            Try
                Me.DTSBTableAdapter.FillById(Me.EnquiryWorkSheetDataSet.DTSB, New System.Nullable(Of Integer)(recordId))
                ''set combobox selected value
                'cmbxAMLRisk.SelectedIndex = If(IsDBNull(EnquiryWorkSheetDataSet.DTSB.Rows(DTSBBindingSource.Position()).Item("AMLRisk")), -1, EnquiryWorkSheetDataSet.DTSB.Rows(DTSBBindingSource.Position()).Item("AMLRisk"))

            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        ElseIf dtsbOpType = 1 Then 'Insert
            btnSave.Visible = False
            btnInsert.Visible = True
            Me.Text = "Add a new " & MyEnums.GetDescription(DirectCast(thisDtsbType, MyEnums.DtsbType))
        End If



    End Sub
    Private Sub FormAddDTSB_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            'Dim row As Integer = DTSBBindingSource.Position
            'Dim drv As DataRowView = DTSBBindingSource.Current
            'drv("AMLRisk") = cmbxAMLRisk.SelectedIndex
            Me.Validate()
            DTSBBindingSource.EndEdit()
            DTSBTableAdapter.Update(Me.EnquiryWorkSheetDataSet.DTSB)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.DialogResult = System.Windows.Forms.DialogResult.OK

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click
        Try
            Me.Validate()
            DTSBBindingSource.EndEdit()
            DTSBTableAdapter.Insert(thisDtsbType, thisCustomerId, FirstNameTextBox.Text, LastNameTextBox.Text, DateOfBirthDateTimePicker.Value, IdVerifiedCheckBox.Checked, AddressVerifiedCheckBox.Checked, ckbxIsaPEP.Checked, Convert.ToInt16(cmbxAMLRisk.SelectedValue), thisId)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub


End Class