﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewSettingsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ViewSettingsForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gpbxUserSettings = New System.Windows.Forms.GroupBox()
        Me.lblAppFormLocValue = New System.Windows.Forms.Label()
        Me.lblAppFormLoc = New System.Windows.Forms.Label()
        Me.lblAppFormSizeValue = New System.Windows.Forms.Label()
        Me.lblAppFormSize = New System.Windows.Forms.Label()
        Me.lblPrelimLocValue = New System.Windows.Forms.Label()
        Me.lblPrelimLoc = New System.Windows.Forms.Label()
        Me.lblPrelimSizeValue = New System.Windows.Forms.Label()
        Me.lblPrelimSize = New System.Windows.Forms.Label()
        Me.lblForm1LocValue = New System.Windows.Forms.Label()
        Me.lblForm1Loc = New System.Windows.Forms.Label()
        Me.lblForm1SizeValue = New System.Windows.Forms.Label()
        Me.lblForm1Size = New System.Windows.Forms.Label()
        Me.gpbxAppSettings = New System.Windows.Forms.GroupBox()
        Me.lblWebAppPageValue = New System.Windows.Forms.Label()
        Me.lblWebAppPage = New System.Windows.Forms.Label()
        Me.lblEWSConnStringValue = New System.Windows.Forms.Label()
        Me.lblEWSConnString = New System.Windows.Forms.Label()
        Me.gpbxFormSettings = New System.Windows.Forms.GroupBox()
        Me.lblGetAppformSizeValue = New System.Windows.Forms.Label()
        Me.lblGetAppformSize = New System.Windows.Forms.Label()
        Me.lblGetForm1MaxSizeValue = New System.Windows.Forms.Label()
        Me.lblGetForm1MaxSize = New System.Windows.Forms.Label()
        Me.lblGetForm1ClientSizeValue = New System.Windows.Forms.Label()
        Me.lblGetForm1ClientSize = New System.Windows.Forms.Label()
        Me.lblGetForm1SizeValue = New System.Windows.Forms.Label()
        Me.lblGetForm1Size = New System.Windows.Forms.Label()
        Me.lblClientSizeValue = New System.Windows.Forms.Label()
        Me.lblClientSize = New System.Windows.Forms.Label()
        Me.lblSizeValue = New System.Windows.Forms.Label()
        Me.lblSize = New System.Windows.Forms.Label()
        Me.gpbxSystemSettings = New System.Windows.Forms.GroupBox()
        Me.lblWorkingAreaValue = New System.Windows.Forms.Label()
        Me.lblWorkingArea = New System.Windows.Forms.Label()
        Me.gpbxUserSettings.SuspendLayout()
        Me.gpbxAppSettings.SuspendLayout()
        Me.gpbxFormSettings.SuspendLayout()
        Me.gpbxSystemSettings.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(268, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "View the User and Application settings for this instance."
        '
        'gpbxUserSettings
        '
        Me.gpbxUserSettings.Controls.Add(Me.lblAppFormLocValue)
        Me.gpbxUserSettings.Controls.Add(Me.lblAppFormLoc)
        Me.gpbxUserSettings.Controls.Add(Me.lblAppFormSizeValue)
        Me.gpbxUserSettings.Controls.Add(Me.lblAppFormSize)
        Me.gpbxUserSettings.Controls.Add(Me.lblPrelimLocValue)
        Me.gpbxUserSettings.Controls.Add(Me.lblPrelimLoc)
        Me.gpbxUserSettings.Controls.Add(Me.lblPrelimSizeValue)
        Me.gpbxUserSettings.Controls.Add(Me.lblPrelimSize)
        Me.gpbxUserSettings.Controls.Add(Me.lblForm1LocValue)
        Me.gpbxUserSettings.Controls.Add(Me.lblForm1Loc)
        Me.gpbxUserSettings.Controls.Add(Me.lblForm1SizeValue)
        Me.gpbxUserSettings.Controls.Add(Me.lblForm1Size)
        Me.gpbxUserSettings.Location = New System.Drawing.Point(13, 50)
        Me.gpbxUserSettings.Name = "gpbxUserSettings"
        Me.gpbxUserSettings.Size = New System.Drawing.Size(679, 123)
        Me.gpbxUserSettings.TabIndex = 2
        Me.gpbxUserSettings.TabStop = False
        Me.gpbxUserSettings.Text = "User Settings"
        '
        'lblAppFormLocValue
        '
        Me.lblAppFormLocValue.AutoSize = True
        Me.lblAppFormLocValue.Location = New System.Drawing.Point(213, 98)
        Me.lblAppFormLocValue.Name = "lblAppFormLocValue"
        Me.lblAppFormLocValue.Size = New System.Drawing.Size(39, 13)
        Me.lblAppFormLocValue.TabIndex = 11
        Me.lblAppFormLocValue.Text = "Label3"
        '
        'lblAppFormLoc
        '
        Me.lblAppFormLoc.AutoSize = True
        Me.lblAppFormLoc.Location = New System.Drawing.Point(7, 98)
        Me.lblAppFormLoc.Name = "lblAppFormLoc"
        Me.lblAppFormLoc.Size = New System.Drawing.Size(90, 13)
        Me.lblAppFormLoc.TabIndex = 10
        Me.lblAppFormLoc.Text = "AppFormLocation"
        '
        'lblAppFormSizeValue
        '
        Me.lblAppFormSizeValue.AutoSize = True
        Me.lblAppFormSizeValue.Location = New System.Drawing.Point(213, 85)
        Me.lblAppFormSizeValue.Name = "lblAppFormSizeValue"
        Me.lblAppFormSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblAppFormSizeValue.TabIndex = 9
        Me.lblAppFormSizeValue.Text = "Label2"
        '
        'lblAppFormSize
        '
        Me.lblAppFormSize.AutoSize = True
        Me.lblAppFormSize.Location = New System.Drawing.Point(7, 85)
        Me.lblAppFormSize.Name = "lblAppFormSize"
        Me.lblAppFormSize.Size = New System.Drawing.Size(69, 13)
        Me.lblAppFormSize.TabIndex = 8
        Me.lblAppFormSize.Text = "AppFormSize"
        '
        'lblPrelimLocValue
        '
        Me.lblPrelimLocValue.AutoSize = True
        Me.lblPrelimLocValue.Location = New System.Drawing.Point(213, 66)
        Me.lblPrelimLocValue.Name = "lblPrelimLocValue"
        Me.lblPrelimLocValue.Size = New System.Drawing.Size(39, 13)
        Me.lblPrelimLocValue.TabIndex = 7
        Me.lblPrelimLocValue.Text = "Label2"
        '
        'lblPrelimLoc
        '
        Me.lblPrelimLoc.AutoSize = True
        Me.lblPrelimLoc.Location = New System.Drawing.Point(6, 66)
        Me.lblPrelimLoc.Name = "lblPrelimLoc"
        Me.lblPrelimLoc.Size = New System.Drawing.Size(99, 13)
        Me.lblPrelimLoc.TabIndex = 6
        Me.lblPrelimLoc.Text = "PrelimFormLocation"
        '
        'lblPrelimSizeValue
        '
        Me.lblPrelimSizeValue.AutoSize = True
        Me.lblPrelimSizeValue.Location = New System.Drawing.Point(213, 53)
        Me.lblPrelimSizeValue.Name = "lblPrelimSizeValue"
        Me.lblPrelimSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblPrelimSizeValue.TabIndex = 5
        Me.lblPrelimSizeValue.Text = "Label2"
        '
        'lblPrelimSize
        '
        Me.lblPrelimSize.AutoSize = True
        Me.lblPrelimSize.Location = New System.Drawing.Point(6, 53)
        Me.lblPrelimSize.Name = "lblPrelimSize"
        Me.lblPrelimSize.Size = New System.Drawing.Size(78, 13)
        Me.lblPrelimSize.TabIndex = 4
        Me.lblPrelimSize.Text = "PrelimFormSize"
        '
        'lblForm1LocValue
        '
        Me.lblForm1LocValue.AutoSize = True
        Me.lblForm1LocValue.Location = New System.Drawing.Point(213, 33)
        Me.lblForm1LocValue.Name = "lblForm1LocValue"
        Me.lblForm1LocValue.Size = New System.Drawing.Size(39, 13)
        Me.lblForm1LocValue.TabIndex = 3
        Me.lblForm1LocValue.Text = "Label2"
        '
        'lblForm1Loc
        '
        Me.lblForm1Loc.AutoSize = True
        Me.lblForm1Loc.Location = New System.Drawing.Point(7, 33)
        Me.lblForm1Loc.Name = "lblForm1Loc"
        Me.lblForm1Loc.Size = New System.Drawing.Size(77, 13)
        Me.lblForm1Loc.TabIndex = 2
        Me.lblForm1Loc.Text = "Form1Location"
        '
        'lblForm1SizeValue
        '
        Me.lblForm1SizeValue.AutoSize = True
        Me.lblForm1SizeValue.Location = New System.Drawing.Point(213, 20)
        Me.lblForm1SizeValue.Name = "lblForm1SizeValue"
        Me.lblForm1SizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblForm1SizeValue.TabIndex = 1
        Me.lblForm1SizeValue.Text = "Label2"
        '
        'lblForm1Size
        '
        Me.lblForm1Size.AutoSize = True
        Me.lblForm1Size.Location = New System.Drawing.Point(7, 20)
        Me.lblForm1Size.Name = "lblForm1Size"
        Me.lblForm1Size.Size = New System.Drawing.Size(56, 13)
        Me.lblForm1Size.TabIndex = 0
        Me.lblForm1Size.Text = "Form1Size"
        '
        'gpbxAppSettings
        '
        Me.gpbxAppSettings.Controls.Add(Me.lblWebAppPageValue)
        Me.gpbxAppSettings.Controls.Add(Me.lblWebAppPage)
        Me.gpbxAppSettings.Controls.Add(Me.lblEWSConnStringValue)
        Me.gpbxAppSettings.Controls.Add(Me.lblEWSConnString)
        Me.gpbxAppSettings.Location = New System.Drawing.Point(13, 179)
        Me.gpbxAppSettings.Name = "gpbxAppSettings"
        Me.gpbxAppSettings.Size = New System.Drawing.Size(679, 77)
        Me.gpbxAppSettings.TabIndex = 3
        Me.gpbxAppSettings.TabStop = False
        Me.gpbxAppSettings.Text = "Application settings"
        '
        'lblWebAppPageValue
        '
        Me.lblWebAppPageValue.AutoSize = True
        Me.lblWebAppPageValue.Location = New System.Drawing.Point(213, 48)
        Me.lblWebAppPageValue.Name = "lblWebAppPageValue"
        Me.lblWebAppPageValue.Size = New System.Drawing.Size(39, 13)
        Me.lblWebAppPageValue.TabIndex = 3
        Me.lblWebAppPageValue.Text = "Label5"
        '
        'lblWebAppPage
        '
        Me.lblWebAppPage.AutoSize = True
        Me.lblWebAppPage.Location = New System.Drawing.Point(7, 48)
        Me.lblWebAppPage.Name = "lblWebAppPage"
        Me.lblWebAppPage.Size = New System.Drawing.Size(107, 13)
        Me.lblWebAppPage.TabIndex = 2
        Me.lblWebAppPage.Text = "WebApplicationPage"
        '
        'lblEWSConnStringValue
        '
        Me.lblEWSConnStringValue.Location = New System.Drawing.Point(213, 20)
        Me.lblEWSConnStringValue.Name = "lblEWSConnStringValue"
        Me.lblEWSConnStringValue.Size = New System.Drawing.Size(460, 28)
        Me.lblEWSConnStringValue.TabIndex = 1
        Me.lblEWSConnStringValue.Text = "Label3"
        '
        'lblEWSConnString
        '
        Me.lblEWSConnString.AutoSize = True
        Me.lblEWSConnString.Location = New System.Drawing.Point(7, 20)
        Me.lblEWSConnString.Name = "lblEWSConnString"
        Me.lblEWSConnString.Size = New System.Drawing.Size(177, 13)
        Me.lblEWSConnString.TabIndex = 0
        Me.lblEWSConnString.Text = "EnquiryWorkSheetConnectionString"
        '
        'gpbxFormSettings
        '
        Me.gpbxFormSettings.Controls.Add(Me.lblGetAppformSizeValue)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetAppformSize)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetForm1MaxSizeValue)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetForm1MaxSize)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetForm1ClientSizeValue)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetForm1ClientSize)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetForm1SizeValue)
        Me.gpbxFormSettings.Controls.Add(Me.lblGetForm1Size)
        Me.gpbxFormSettings.Controls.Add(Me.lblClientSizeValue)
        Me.gpbxFormSettings.Controls.Add(Me.lblClientSize)
        Me.gpbxFormSettings.Controls.Add(Me.lblSizeValue)
        Me.gpbxFormSettings.Controls.Add(Me.lblSize)
        Me.gpbxFormSettings.Location = New System.Drawing.Point(13, 262)
        Me.gpbxFormSettings.Name = "gpbxFormSettings"
        Me.gpbxFormSettings.Size = New System.Drawing.Size(679, 144)
        Me.gpbxFormSettings.TabIndex = 4
        Me.gpbxFormSettings.TabStop = False
        Me.gpbxFormSettings.Text = "Form Settings"
        '
        'lblGetAppformSizeValue
        '
        Me.lblGetAppformSizeValue.AutoSize = True
        Me.lblGetAppformSizeValue.Location = New System.Drawing.Point(213, 99)
        Me.lblGetAppformSizeValue.Name = "lblGetAppformSizeValue"
        Me.lblGetAppformSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblGetAppformSizeValue.TabIndex = 11
        Me.lblGetAppformSizeValue.Text = "Label2"
        Me.lblGetAppformSizeValue.Visible = False
        '
        'lblGetAppformSize
        '
        Me.lblGetAppformSize.AutoSize = True
        Me.lblGetAppformSize.Location = New System.Drawing.Point(6, 99)
        Me.lblGetAppformSize.Name = "lblGetAppformSize"
        Me.lblGetAppformSize.Size = New System.Drawing.Size(70, 13)
        Me.lblGetAppformSize.TabIndex = 10
        Me.lblGetAppformSize.Text = "AppForm size"
        '
        'lblGetForm1MaxSizeValue
        '
        Me.lblGetForm1MaxSizeValue.AutoSize = True
        Me.lblGetForm1MaxSizeValue.Location = New System.Drawing.Point(213, 86)
        Me.lblGetForm1MaxSizeValue.Name = "lblGetForm1MaxSizeValue"
        Me.lblGetForm1MaxSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblGetForm1MaxSizeValue.TabIndex = 9
        Me.lblGetForm1MaxSizeValue.Text = "Label3"
        '
        'lblGetForm1MaxSize
        '
        Me.lblGetForm1MaxSize.AutoSize = True
        Me.lblGetForm1MaxSize.Location = New System.Drawing.Point(7, 86)
        Me.lblGetForm1MaxSize.Name = "lblGetForm1MaxSize"
        Me.lblGetForm1MaxSize.Size = New System.Drawing.Size(104, 13)
        Me.lblGetForm1MaxSize.TabIndex = 8
        Me.lblGetForm1MaxSize.Text = "Form1 Maximum size"
        '
        'lblGetForm1ClientSizeValue
        '
        Me.lblGetForm1ClientSizeValue.AutoSize = True
        Me.lblGetForm1ClientSizeValue.Location = New System.Drawing.Point(213, 73)
        Me.lblGetForm1ClientSizeValue.Name = "lblGetForm1ClientSizeValue"
        Me.lblGetForm1ClientSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblGetForm1ClientSizeValue.TabIndex = 7
        Me.lblGetForm1ClientSizeValue.Text = "Label3"
        '
        'lblGetForm1ClientSize
        '
        Me.lblGetForm1ClientSize.AutoSize = True
        Me.lblGetForm1ClientSize.Location = New System.Drawing.Point(7, 73)
        Me.lblGetForm1ClientSize.Name = "lblGetForm1ClientSize"
        Me.lblGetForm1ClientSize.Size = New System.Drawing.Size(86, 13)
        Me.lblGetForm1ClientSize.TabIndex = 6
        Me.lblGetForm1ClientSize.Text = "Form1 Client size"
        '
        'lblGetForm1SizeValue
        '
        Me.lblGetForm1SizeValue.AutoSize = True
        Me.lblGetForm1SizeValue.Location = New System.Drawing.Point(213, 60)
        Me.lblGetForm1SizeValue.Name = "lblGetForm1SizeValue"
        Me.lblGetForm1SizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblGetForm1SizeValue.TabIndex = 5
        Me.lblGetForm1SizeValue.Text = "Label2"
        '
        'lblGetForm1Size
        '
        Me.lblGetForm1Size.AutoSize = True
        Me.lblGetForm1Size.Location = New System.Drawing.Point(7, 60)
        Me.lblGetForm1Size.Name = "lblGetForm1Size"
        Me.lblGetForm1Size.Size = New System.Drawing.Size(57, 13)
        Me.lblGetForm1Size.TabIndex = 4
        Me.lblGetForm1Size.Text = "Form1 size"
        '
        'lblClientSizeValue
        '
        Me.lblClientSizeValue.AutoSize = True
        Me.lblClientSizeValue.Location = New System.Drawing.Point(213, 39)
        Me.lblClientSizeValue.Name = "lblClientSizeValue"
        Me.lblClientSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblClientSizeValue.TabIndex = 3
        Me.lblClientSizeValue.Text = "Label2"
        '
        'lblClientSize
        '
        Me.lblClientSize.AutoSize = True
        Me.lblClientSize.Location = New System.Drawing.Point(6, 39)
        Me.lblClientSize.Name = "lblClientSize"
        Me.lblClientSize.Size = New System.Drawing.Size(103, 13)
        Me.lblClientSize.TabIndex = 2
        Me.lblClientSize.Text = "This Form Client size"
        '
        'lblSizeValue
        '
        Me.lblSizeValue.AutoSize = True
        Me.lblSizeValue.Location = New System.Drawing.Point(213, 26)
        Me.lblSizeValue.Name = "lblSizeValue"
        Me.lblSizeValue.Size = New System.Drawing.Size(39, 13)
        Me.lblSizeValue.TabIndex = 1
        Me.lblSizeValue.Text = "Label2"
        '
        'lblSize
        '
        Me.lblSize.AutoSize = True
        Me.lblSize.Location = New System.Drawing.Point(6, 26)
        Me.lblSize.Name = "lblSize"
        Me.lblSize.Size = New System.Drawing.Size(76, 13)
        Me.lblSize.TabIndex = 0
        Me.lblSize.Text = "This Form Size"
        '
        'gpbxSystemSettings
        '
        Me.gpbxSystemSettings.Controls.Add(Me.lblWorkingAreaValue)
        Me.gpbxSystemSettings.Controls.Add(Me.lblWorkingArea)
        Me.gpbxSystemSettings.Location = New System.Drawing.Point(15, 412)
        Me.gpbxSystemSettings.Name = "gpbxSystemSettings"
        Me.gpbxSystemSettings.Size = New System.Drawing.Size(677, 81)
        Me.gpbxSystemSettings.TabIndex = 5
        Me.gpbxSystemSettings.TabStop = False
        Me.gpbxSystemSettings.Text = "System Settings"
        '
        'lblWorkingAreaValue
        '
        Me.lblWorkingAreaValue.AutoSize = True
        Me.lblWorkingAreaValue.Location = New System.Drawing.Point(211, 26)
        Me.lblWorkingAreaValue.Name = "lblWorkingAreaValue"
        Me.lblWorkingAreaValue.Size = New System.Drawing.Size(39, 13)
        Me.lblWorkingAreaValue.TabIndex = 1
        Me.lblWorkingAreaValue.Text = "Label3"
        '
        'lblWorkingArea
        '
        Me.lblWorkingArea.AutoSize = True
        Me.lblWorkingArea.Location = New System.Drawing.Point(6, 26)
        Me.lblWorkingArea.Name = "lblWorkingArea"
        Me.lblWorkingArea.Size = New System.Drawing.Size(72, 13)
        Me.lblWorkingArea.TabIndex = 0
        Me.lblWorkingArea.Text = "Working Area"
        '
        'ViewSettingsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 564)
        Me.Controls.Add(Me.gpbxSystemSettings)
        Me.Controls.Add(Me.gpbxFormSettings)
        Me.Controls.Add(Me.gpbxAppSettings)
        Me.Controls.Add(Me.gpbxUserSettings)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ViewSettingsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "View Settings Form"
        Me.gpbxUserSettings.ResumeLayout(False)
        Me.gpbxUserSettings.PerformLayout()
        Me.gpbxAppSettings.ResumeLayout(False)
        Me.gpbxAppSettings.PerformLayout()
        Me.gpbxFormSettings.ResumeLayout(False)
        Me.gpbxFormSettings.PerformLayout()
        Me.gpbxSystemSettings.ResumeLayout(False)
        Me.gpbxSystemSettings.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gpbxUserSettings As System.Windows.Forms.GroupBox
    Friend WithEvents lblForm1SizeValue As System.Windows.Forms.Label
    Friend WithEvents lblForm1Size As System.Windows.Forms.Label
    Friend WithEvents lblForm1LocValue As System.Windows.Forms.Label
    Friend WithEvents lblForm1Loc As System.Windows.Forms.Label
    Friend WithEvents lblPrelimSizeValue As System.Windows.Forms.Label
    Friend WithEvents lblPrelimSize As System.Windows.Forms.Label
    Friend WithEvents lblPrelimLocValue As System.Windows.Forms.Label
    Friend WithEvents lblPrelimLoc As System.Windows.Forms.Label
    Friend WithEvents lblAppFormSizeValue As System.Windows.Forms.Label
    Friend WithEvents lblAppFormSize As System.Windows.Forms.Label
    Friend WithEvents lblAppFormLocValue As System.Windows.Forms.Label
    Friend WithEvents lblAppFormLoc As System.Windows.Forms.Label
    Friend WithEvents gpbxAppSettings As System.Windows.Forms.GroupBox
    Friend WithEvents lblWebAppPageValue As System.Windows.Forms.Label
    Friend WithEvents lblWebAppPage As System.Windows.Forms.Label
    Friend WithEvents lblEWSConnStringValue As System.Windows.Forms.Label
    Friend WithEvents lblEWSConnString As System.Windows.Forms.Label
    Friend WithEvents gpbxFormSettings As System.Windows.Forms.GroupBox
    Friend WithEvents lblSizeValue As System.Windows.Forms.Label
    Friend WithEvents lblSize As System.Windows.Forms.Label
    Friend WithEvents lblClientSizeValue As System.Windows.Forms.Label
    Friend WithEvents lblClientSize As System.Windows.Forms.Label
    Friend WithEvents lblGetForm1SizeValue As System.Windows.Forms.Label
    Friend WithEvents lblGetForm1Size As System.Windows.Forms.Label
    Friend WithEvents lblGetForm1ClientSizeValue As System.Windows.Forms.Label
    Friend WithEvents lblGetForm1ClientSize As System.Windows.Forms.Label
    Friend WithEvents lblGetForm1MaxSizeValue As System.Windows.Forms.Label
    Friend WithEvents lblGetForm1MaxSize As System.Windows.Forms.Label
    Friend WithEvents gpbxSystemSettings As System.Windows.Forms.GroupBox
    Friend WithEvents lblWorkingAreaValue As System.Windows.Forms.Label
    Friend WithEvents lblWorkingArea As System.Windows.Forms.Label
    Friend WithEvents lblGetAppformSize As System.Windows.Forms.Label
    Friend WithEvents lblGetAppformSizeValue As System.Windows.Forms.Label
End Class
