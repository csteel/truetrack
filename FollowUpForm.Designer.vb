﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FollowUpForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FollowUpForm))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.gpbxManager = New System.Windows.Forms.GroupBox()
        Me.lblEnquiryManagerName = New System.Windows.Forms.Label()
        Me.gpbxStatus = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.EnquiryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PageSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommentsOnlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EndEnquiryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckFinPowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DealersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalFormToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WWWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCandDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCommentToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckEnquiryNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblContactAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblContactName = New System.Windows.Forms.Label()
        Me.lblDealerName = New System.Windows.Forms.Label()
        Me.lblContactPhone = New System.Windows.Forms.Label()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblTypeLoanEnquiry = New System.Windows.Forms.Label()
        Me.lblLoanAmount = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblDateAndTimeValue = New System.Windows.Forms.Label()
        Me.lblDateAndTime = New System.Windows.Forms.Label()
        Me.lblEnquiryNumberValue = New System.Windows.Forms.Label()
        Me.lblEnquiryCode = New System.Windows.Forms.Label()
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnViewDocs = New System.Windows.Forms.Button()
        Me.btnDocs = New System.Windows.Forms.Button()
        Me.btnWorksheetWizard = New System.Windows.Forms.Button()
        Me.btnSaveAndExit = New System.Windows.Forms.Button()
        Me.btnWebApp = New System.Windows.Forms.Button()
        Me.lblComments = New System.Windows.Forms.Label()
        Me.PageSetupDialog1 = New System.Windows.Forms.PageSetupDialog()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ActiveDealersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.ActiveDealersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveDealersTableAdapter()
        Me.QrgListTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.QRGListTableAdapter()
        Me.PrelimResultsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrelimReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrelimReasonsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter()
        Me.PrelimResultsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter()
        Me.UsersTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter()
        Me.QRGListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryCommentBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryCommentTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter()
        Me.dgvEnquiryComments = New System.Windows.Forms.DataGridView()
        Me.btnAddComment = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EnquiryIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateCreatedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2.SuspendLayout()
        Me.gpbxManager.SuspendLayout()
        Me.gpbxStatus.SuspendLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStripMessage.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActiveDealersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QRGListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryCommentBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEnquiryComments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.gpbxManager)
        Me.Panel2.Controls.Add(Me.gpbxStatus)
        Me.Panel2.Controls.Add(Me.lblContactAddress)
        Me.Panel2.Controls.Add(Me.lblAddress)
        Me.Panel2.Controls.Add(Me.lblContactName)
        Me.Panel2.Controls.Add(Me.lblDealerName)
        Me.Panel2.Controls.Add(Me.lblContactPhone)
        Me.Panel2.Controls.Add(Me.lblContact)
        Me.Panel2.Controls.Add(Me.lblTypeLoanEnquiry)
        Me.Panel2.Controls.Add(Me.lblLoanAmount)
        Me.Panel2.Controls.Add(Me.lblDealer)
        Me.Panel2.Controls.Add(Me.lblDateAndTimeValue)
        Me.Panel2.Controls.Add(Me.lblDateAndTime)
        Me.Panel2.Controls.Add(Me.lblEnquiryNumberValue)
        Me.Panel2.Controls.Add(Me.lblEnquiryCode)
        Me.Panel2.Controls.Add(Me.MenuStrip1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(944, 115)
        Me.Panel2.TabIndex = 2
        '
        'gpbxManager
        '
        Me.gpbxManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxManager.Controls.Add(Me.lblEnquiryManagerName)
        Me.gpbxManager.Location = New System.Drawing.Point(740, 0)
        Me.gpbxManager.Name = "gpbxManager"
        Me.gpbxManager.Size = New System.Drawing.Size(102, 34)
        Me.gpbxManager.TabIndex = 246
        Me.gpbxManager.TabStop = False
        Me.gpbxManager.Text = "Manager"
        '
        'lblEnquiryManagerName
        '
        Me.lblEnquiryManagerName.AutoSize = True
        Me.lblEnquiryManagerName.Location = New System.Drawing.Point(6, 15)
        Me.lblEnquiryManagerName.Name = "lblEnquiryManagerName"
        Me.lblEnquiryManagerName.Size = New System.Drawing.Size(39, 13)
        Me.lblEnquiryManagerName.TabIndex = 0
        Me.lblEnquiryManagerName.Text = "Label2"
        '
        'gpbxStatus
        '
        Me.gpbxStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbxStatus.BackColor = System.Drawing.SystemColors.Control
        Me.gpbxStatus.Controls.Add(Me.lblStatus)
        Me.gpbxStatus.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.gpbxStatus.Location = New System.Drawing.Point(842, 0)
        Me.gpbxStatus.Name = "gpbxStatus"
        Me.gpbxStatus.Size = New System.Drawing.Size(102, 34)
        Me.gpbxStatus.TabIndex = 48
        Me.gpbxStatus.TabStop = False
        Me.gpbxStatus.Text = "Status"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "CurrentStatus", True))
        Me.lblStatus.Location = New System.Drawing.Point(4, 15)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(39, 13)
        Me.lblStatus.TabIndex = 0
        Me.lblStatus.Text = "Label2"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EnquiryBindingSource
        '
        Me.EnquiryBindingSource.DataMember = "Enquiry"
        Me.EnquiryBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.CheckNamesToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.UpdateAllToolStripMenuItem, Me.WWWToolStripMenuItem, Me.EditCandDToolStripMenuItem, Me.AddCommentToolStripMenuItem1, Me.EmailToolStripMenuItem, Me.CheckEnquiryNamesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 245
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PageSetupToolStripMenuItem, Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem, Me.EndEnquiryToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        Me.FileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PageSetupToolStripMenuItem
        '
        Me.PageSetupToolStripMenuItem.Enabled = False
        Me.PageSetupToolStripMenuItem.Name = "PageSetupToolStripMenuItem"
        Me.PageSetupToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.PageSetupToolStripMenuItem.Text = "Page Setup"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentsOnlyToolStripMenuItem1, Me.AllToolStripMenuItem})
        Me.PrintToolStripMenuItem1.Enabled = False
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.PrintToolStripMenuItem1.Text = "Print"
        '
        'CommentsOnlyToolStripMenuItem1
        '
        Me.CommentsOnlyToolStripMenuItem1.Name = "CommentsOnlyToolStripMenuItem1"
        Me.CommentsOnlyToolStripMenuItem1.Size = New System.Drawing.Size(161, 22)
        Me.CommentsOnlyToolStripMenuItem1.Text = "Comments Only"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(134, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EndEnquiryToolStripMenuItem
        '
        Me.EndEnquiryToolStripMenuItem.Name = "EndEnquiryToolStripMenuItem"
        Me.EndEnquiryToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EndEnquiryToolStripMenuItem.Text = "End Enquiry"
        Me.EndEnquiryToolStripMenuItem.Visible = False
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientDetailsToolStripMenuItem, Me.AddCommentToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'ClientDetailsToolStripMenuItem
        '
        Me.ClientDetailsToolStripMenuItem.Name = "ClientDetailsToolStripMenuItem"
        Me.ClientDetailsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ClientDetailsToolStripMenuItem.Text = "Client/Dealer details"
        Me.ClientDetailsToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem
        '
        Me.AddCommentToolStripMenuItem.Name = "AddCommentToolStripMenuItem"
        Me.AddCommentToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AddCommentToolStripMenuItem.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem.Visible = False
        '
        'CheckNamesToolStripMenuItem
        '
        Me.CheckNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckFinPowerToolStripMenuItem, Me.CheckEnquiriesToolStripMenuItem, Me.DealersToolStripMenuItem})
        Me.CheckNamesToolStripMenuItem.Name = "CheckNamesToolStripMenuItem"
        Me.CheckNamesToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.CheckNamesToolStripMenuItem.Text = "&Search"
        '
        'CheckFinPowerToolStripMenuItem
        '
        Me.CheckFinPowerToolStripMenuItem.Name = "CheckFinPowerToolStripMenuItem"
        Me.CheckFinPowerToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckFinPowerToolStripMenuItem.Text = "&FinPower Names"
        '
        'CheckEnquiriesToolStripMenuItem
        '
        Me.CheckEnquiriesToolStripMenuItem.Name = "CheckEnquiriesToolStripMenuItem"
        Me.CheckEnquiriesToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CheckEnquiriesToolStripMenuItem.Text = "&Enquiries Names"
        '
        'DealersToolStripMenuItem
        '
        Me.DealersToolStripMenuItem.Name = "DealersToolStripMenuItem"
        Me.DealersToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.DealersToolStripMenuItem.Text = "Dealers"
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApprovalFormToolStripMenuItem1})
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.DocumentsToolStripMenuItem.Text = "&Documents"
        '
        'ApprovalFormToolStripMenuItem1
        '
        Me.ApprovalFormToolStripMenuItem1.Name = "ApprovalFormToolStripMenuItem1"
        Me.ApprovalFormToolStripMenuItem1.Size = New System.Drawing.Size(153, 22)
        Me.ApprovalFormToolStripMenuItem1.Text = "Approval Form"
        Me.ApprovalFormToolStripMenuItem1.Visible = False
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RefreshToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.refresh16
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        Me.RefreshToolStripMenuItem.ToolTipText = "Refresh Form"
        '
        'UpdateAllToolStripMenuItem
        '
        Me.UpdateAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UpdateAllToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.saveAll16
        Me.UpdateAllToolStripMenuItem.Name = "UpdateAllToolStripMenuItem"
        Me.UpdateAllToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.UpdateAllToolStripMenuItem.Text = "Save All"
        Me.UpdateAllToolStripMenuItem.ToolTipText = "Save All"
        '
        'WWWToolStripMenuItem
        '
        Me.WWWToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.WWWToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.www16
        Me.WWWToolStripMenuItem.Name = "WWWToolStripMenuItem"
        Me.WWWToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WWWToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.WWWToolStripMenuItem.Text = "WebSite"
        Me.WWWToolStripMenuItem.ToolTipText = "YFL Web Site - Ctrl+Alt+W"
        '
        'EditCandDToolStripMenuItem
        '
        Me.EditCandDToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EditCandDToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.editdetails16
        Me.EditCandDToolStripMenuItem.Name = "EditCandDToolStripMenuItem"
        Me.EditCandDToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.EditCandDToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EditCandDToolStripMenuItem.Text = "Edit Client/Dealer details"
        Me.EditCandDToolStripMenuItem.ToolTipText = "Edit Client/Dealer details - Ctrl+Alt+D"
        Me.EditCandDToolStripMenuItem.Visible = False
        '
        'AddCommentToolStripMenuItem1
        '
        Me.AddCommentToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.AddCommentToolStripMenuItem1.Image = Global.AppWhShtB.My.Resources.Resources.comment16
        Me.AddCommentToolStripMenuItem1.Name = "AddCommentToolStripMenuItem1"
        Me.AddCommentToolStripMenuItem1.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.AddCommentToolStripMenuItem1.Size = New System.Drawing.Size(28, 20)
        Me.AddCommentToolStripMenuItem1.Text = "Add Comment"
        Me.AddCommentToolStripMenuItem1.ToolTipText = "Add Comment - Ctrl+Alt+C"
        Me.AddCommentToolStripMenuItem1.Visible = False
        '
        'EmailToolStripMenuItem
        '
        Me.EmailToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmailToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.email16
        Me.EmailToolStripMenuItem.Name = "EmailToolStripMenuItem"
        Me.EmailToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EmailToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.EmailToolStripMenuItem.Text = "Send Email"
        Me.EmailToolStripMenuItem.ToolTipText = "Send an email - Ctrl+Alt+E"
        Me.EmailToolStripMenuItem.Visible = False
        '
        'CheckEnquiryNamesToolStripMenuItem
        '
        Me.CheckEnquiryNamesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CheckEnquiryNamesToolStripMenuItem.Image = Global.AppWhShtB.My.Resources.Resources.CheckNames
        Me.CheckEnquiryNamesToolStripMenuItem.Name = "CheckEnquiryNamesToolStripMenuItem"
        Me.CheckEnquiryNamesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.CheckEnquiryNamesToolStripMenuItem.Size = New System.Drawing.Size(28, 20)
        Me.CheckEnquiryNamesToolStripMenuItem.Text = "Check Enquiry Names"
        Me.CheckEnquiryNamesToolStripMenuItem.ToolTipText = "Search TrueTrack Names - Ctrl+Alt+M"
        '
        'lblContactAddress
        '
        Me.lblContactAddress.AutoSize = True
        Me.lblContactAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactAddress.Location = New System.Drawing.Point(236, 62)
        Me.lblContactAddress.Name = "lblContactAddress"
        Me.lblContactAddress.Size = New System.Drawing.Size(84, 13)
        Me.lblContactAddress.TabIndex = 242
        Me.lblContactAddress.Text = "ClientAddress"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(182, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(48, 13)
        Me.lblAddress.TabIndex = 241
        Me.lblAddress.Text = "Address:"
        '
        'lblContactName
        '
        Me.lblContactName.AutoSize = True
        Me.lblContactName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactName.Location = New System.Drawing.Point(15, 62)
        Me.lblContactName.Name = "lblContactName"
        Me.lblContactName.Size = New System.Drawing.Size(75, 13)
        Me.lblContactName.TabIndex = 240
        Me.lblContactName.Text = "Client Name"
        '
        'lblDealerName
        '
        Me.lblDealerName.AutoSize = True
        Me.lblDealerName.Location = New System.Drawing.Point(59, 87)
        Me.lblDealerName.Name = "lblDealerName"
        Me.lblDealerName.Size = New System.Drawing.Size(0, 13)
        Me.lblDealerName.TabIndex = 238
        '
        'lblContactPhone
        '
        Me.lblContactPhone.AutoSize = True
        Me.lblContactPhone.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "ContactPhone", True))
        Me.lblContactPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactPhone.Location = New System.Drawing.Point(594, 62)
        Me.lblContactPhone.Name = "lblContactPhone"
        Me.lblContactPhone.Size = New System.Drawing.Size(50, 13)
        Me.lblContactPhone.TabIndex = 237
        Me.lblContactPhone.Text = "Number"
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(504, 62)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(87, 13)
        Me.lblContact.TabIndex = 236
        Me.lblContact.Text = "Contact Number:"
        '
        'lblTypeLoanEnquiry
        '
        Me.lblTypeLoanEnquiry.AutoSize = True
        Me.lblTypeLoanEnquiry.Location = New System.Drawing.Point(347, 39)
        Me.lblTypeLoanEnquiry.Name = "lblTypeLoanEnquiry"
        Me.lblTypeLoanEnquiry.Size = New System.Drawing.Size(90, 13)
        Me.lblTypeLoanEnquiry.TabIndex = 235
        Me.lblTypeLoanEnquiry.Text = "TypeLoanEnquiry"
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.AutoSize = True
        Me.lblLoanAmount.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "LoanValue", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "C2"))
        Me.lblLoanAmount.Location = New System.Drawing.Point(483, 39)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 13)
        Me.lblLoanAmount.TabIndex = 234
        Me.lblLoanAmount.Text = "LoanAmount"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(15, 87)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(41, 13)
        Me.lblDealer.TabIndex = 233
        Me.lblDealer.Text = "Dealer:"
        '
        'lblDateAndTimeValue
        '
        Me.lblDateAndTimeValue.AutoSize = True
        Me.lblDateAndTimeValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "DateTime", True))
        Me.lblDateAndTimeValue.Location = New System.Drawing.Point(206, 39)
        Me.lblDateAndTimeValue.Name = "lblDateAndTimeValue"
        Me.lblDateAndTimeValue.Size = New System.Drawing.Size(19, 13)
        Me.lblDateAndTimeValue.TabIndex = 232
        Me.lblDateAndTimeValue.Text = "00"
        '
        'lblDateAndTime
        '
        Me.lblDateAndTime.AutoSize = True
        Me.lblDateAndTime.Location = New System.Drawing.Point(172, 39)
        Me.lblDateAndTime.Name = "lblDateAndTime"
        Me.lblDateAndTime.Size = New System.Drawing.Size(33, 13)
        Me.lblDateAndTime.TabIndex = 231
        Me.lblDateAndTime.Text = "Date:"
        '
        'lblEnquiryNumberValue
        '
        Me.lblEnquiryNumberValue.AutoSize = True
        Me.lblEnquiryNumberValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EnquiryBindingSource, "EnquiryCode", True))
        Me.lblEnquiryNumberValue.Location = New System.Drawing.Point(94, 39)
        Me.lblEnquiryNumberValue.Name = "lblEnquiryNumberValue"
        Me.lblEnquiryNumberValue.Size = New System.Drawing.Size(31, 13)
        Me.lblEnquiryNumberValue.TabIndex = 230
        Me.lblEnquiryNumberValue.Text = "0000"
        '
        'lblEnquiryCode
        '
        Me.lblEnquiryCode.AutoSize = True
        Me.lblEnquiryCode.Location = New System.Drawing.Point(15, 39)
        Me.lblEnquiryCode.Name = "lblEnquiryCode"
        Me.lblEnquiryCode.Size = New System.Drawing.Size(73, 13)
        Me.lblEnquiryCode.TabIndex = 229
        Me.lblEnquiryCode.Text = "Enquiry Code:"
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 990)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStripMessage.Size = New System.Drawing.Size(944, 22)
        Me.StatusStripMessage.TabIndex = 27
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(42, 17)
        Me.ToolStripStatusLabel1.Text = "Ready."
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnViewDocs)
        Me.Panel1.Controls.Add(Me.btnDocs)
        Me.Panel1.Controls.Add(Me.btnWorksheetWizard)
        Me.Panel1.Controls.Add(Me.btnSaveAndExit)
        Me.Panel1.Controls.Add(Me.btnWebApp)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 941)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 49)
        Me.Panel1.TabIndex = 28
        '
        'btnViewDocs
        '
        Me.btnViewDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_invoices_open16
        Me.btnViewDocs.Location = New System.Drawing.Point(36, 13)
        Me.btnViewDocs.Name = "btnViewDocs"
        Me.btnViewDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnViewDocs.TabIndex = 29
        Me.ToolTip1.SetToolTip(Me.btnViewDocs, "Click to view documents in folder")
        Me.btnViewDocs.UseVisualStyleBackColor = True
        '
        'btnDocs
        '
        Me.btnDocs.AllowDrop = True
        Me.btnDocs.Image = Global.AppWhShtB.My.Resources.Resources.folder_with_file_icon_bw
        Me.btnDocs.Location = New System.Drawing.Point(5, 13)
        Me.btnDocs.Name = "btnDocs"
        Me.btnDocs.Size = New System.Drawing.Size(26, 23)
        Me.btnDocs.TabIndex = 28
        Me.ToolTip1.SetToolTip(Me.btnDocs, "If image is grey click here to create Enquiry Folder")
        Me.btnDocs.UseVisualStyleBackColor = True
        '
        'btnWorksheetWizard
        '
        Me.btnWorksheetWizard.Location = New System.Drawing.Point(400, 12)
        Me.btnWorksheetWizard.Name = "btnWorksheetWizard"
        Me.btnWorksheetWizard.Size = New System.Drawing.Size(196, 23)
        Me.btnWorksheetWizard.TabIndex = 7
        Me.btnWorksheetWizard.Text = "Due Diligence Worksheet Wizard"
        Me.btnWorksheetWizard.UseVisualStyleBackColor = True
        '
        'btnSaveAndExit
        '
        Me.btnSaveAndExit.Location = New System.Drawing.Point(122, 12)
        Me.btnSaveAndExit.Name = "btnSaveAndExit"
        Me.btnSaveAndExit.Size = New System.Drawing.Size(93, 23)
        Me.btnSaveAndExit.TabIndex = 6
        Me.btnSaveAndExit.Text = "Save and Exit"
        Me.btnSaveAndExit.UseVisualStyleBackColor = True
        '
        'btnWebApp
        '
        Me.btnWebApp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWebApp.Location = New System.Drawing.Point(806, 12)
        Me.btnWebApp.Name = "btnWebApp"
        Me.btnWebApp.Size = New System.Drawing.Size(110, 23)
        Me.btnWebApp.TabIndex = 8
        Me.btnWebApp.Text = "Web Application"
        Me.btnWebApp.UseVisualStyleBackColor = True
        '
        'lblComments
        '
        Me.lblComments.AutoSize = True
        Me.lblComments.Location = New System.Drawing.Point(28, 132)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(222, 13)
        Me.lblComments.TabIndex = 56
        Me.lblComments.Text = "Details and Comments on follow-up of Enquiry"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSetBindingSource
        '
        'EnquiryWorkSheetDataSetBindingSource
        '
        Me.EnquiryWorkSheetDataSetBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        Me.EnquiryWorkSheetDataSetBindingSource.Position = 0
        '
        'ActiveDealersBindingSource
        '
        Me.ActiveDealersBindingSource.DataMember = "ActiveDealers"
        Me.ActiveDealersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryTableAdapter
        '
        Me.EnquiryTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Me.ActiveDealersTableAdapter
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Me.EnquiryCommentTableAdapter
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Me.QrgListTableAdapter1
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'ActiveDealersTableAdapter
        '
        Me.ActiveDealersTableAdapter.ClearBeforeFill = True
        '
        'QrgListTableAdapter1
        '
        Me.QrgListTableAdapter1.ClearBeforeFill = True
        '
        'PrelimResultsBindingSource
        '
        Me.PrelimResultsBindingSource.DataMember = "PrelimResults"
        Me.PrelimResultsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'PrelimReasonsBindingSource
        '
        Me.PrelimReasonsBindingSource.DataMember = "PrelimReasons"
        Me.PrelimReasonsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'PrelimReasonsTableAdapter
        '
        Me.PrelimReasonsTableAdapter.ClearBeforeFill = True
        '
        'PrelimResultsTableAdapter
        '
        Me.PrelimResultsTableAdapter.ClearBeforeFill = True
        '
        'UsersTableAdapter1
        '
        Me.UsersTableAdapter1.ClearBeforeFill = True
        '
        'QRGListBindingSource
        '
        Me.QRGListBindingSource.DataMember = "QRGList"
        Me.QRGListBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryCommentBS
        '
        Me.EnquiryCommentBS.DataMember = "EnquiryComment"
        Me.EnquiryCommentBS.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryCommentTableAdapter
        '
        Me.EnquiryCommentTableAdapter.ClearBeforeFill = True
        '
        'dgvEnquiryComments
        '
        Me.dgvEnquiryComments.AllowUserToAddRows = False
        Me.dgvEnquiryComments.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEnquiryComments.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEnquiryComments.AutoGenerateColumns = False
        Me.dgvEnquiryComments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Me.dgvEnquiryComments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEnquiryComments.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.EnquiryIdDataGridViewTextBoxColumn, Me.DateCreatedDataGridViewTextBoxColumn, Me.UserIdDataGridViewTextBoxColumn, Me.TextDataGridViewTextBoxColumn})
        Me.dgvEnquiryComments.DataSource = Me.EnquiryCommentBS
        Me.dgvEnquiryComments.Location = New System.Drawing.Point(12, 148)
        Me.dgvEnquiryComments.MultiSelect = False
        Me.dgvEnquiryComments.Name = "dgvEnquiryComments"
        Me.dgvEnquiryComments.ReadOnly = True
        Me.dgvEnquiryComments.RowHeadersWidth = 10
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEnquiryComments.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvEnquiryComments.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgvEnquiryComments.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEnquiryComments.RowTemplate.Height = 10
        Me.dgvEnquiryComments.RowTemplate.ReadOnly = True
        Me.dgvEnquiryComments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnquiryComments.Size = New System.Drawing.Size(920, 782)
        Me.dgvEnquiryComments.TabIndex = 273
        '
        'btnAddComment
        '
        Me.btnAddComment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddComment.Location = New System.Drawing.Point(832, 119)
        Me.btnAddComment.Name = "btnAddComment"
        Me.btnAddComment.Size = New System.Drawing.Size(100, 23)
        Me.btnAddComment.TabIndex = 274
        Me.btnAddComment.Text = "Add Comment"
        Me.btnAddComment.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Id"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Id"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.HeaderText = "EnquiryId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "DateCreated"
        DataGridViewCellStyle5.Format = "g"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn3.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "UserId"
        Me.DataGridViewTextBoxColumn4.HeaderText = "User"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 75
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Text"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn5.HeaderText = "Comment"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'EnquiryIdDataGridViewTextBoxColumn
        '
        Me.EnquiryIdDataGridViewTextBoxColumn.DataPropertyName = "EnquiryId"
        Me.EnquiryIdDataGridViewTextBoxColumn.HeaderText = "EnquiryId"
        Me.EnquiryIdDataGridViewTextBoxColumn.Name = "EnquiryIdDataGridViewTextBoxColumn"
        Me.EnquiryIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.EnquiryIdDataGridViewTextBoxColumn.Visible = False
        '
        'DateCreatedDataGridViewTextBoxColumn
        '
        Me.DateCreatedDataGridViewTextBoxColumn.DataPropertyName = "DateCreated"
        DataGridViewCellStyle2.Format = "g"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DateCreatedDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.DateCreatedDataGridViewTextBoxColumn.HeaderText = "Date"
        Me.DateCreatedDataGridViewTextBoxColumn.Name = "DateCreatedDataGridViewTextBoxColumn"
        Me.DateCreatedDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateCreatedDataGridViewTextBoxColumn.Width = 120
        '
        'UserIdDataGridViewTextBoxColumn
        '
        Me.UserIdDataGridViewTextBoxColumn.DataPropertyName = "UserId"
        Me.UserIdDataGridViewTextBoxColumn.HeaderText = "User"
        Me.UserIdDataGridViewTextBoxColumn.Name = "UserIdDataGridViewTextBoxColumn"
        Me.UserIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.UserIdDataGridViewTextBoxColumn.Width = 75
        '
        'TextDataGridViewTextBoxColumn
        '
        Me.TextDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TextDataGridViewTextBoxColumn.DataPropertyName = "Text"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TextDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.TextDataGridViewTextBoxColumn.HeaderText = "Comment"
        Me.TextDataGridViewTextBoxColumn.Name = "TextDataGridViewTextBoxColumn"
        Me.TextDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FollowUpForm
        '
        Me.AcceptButton = Me.btnSaveAndExit
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = Global.AppWhShtB.My.MySettings.Default.PrelimFormSize
        Me.Controls.Add(Me.btnAddComment)
        Me.Controls.Add(Me.dgvEnquiryComments)
        Me.Controls.Add(Me.lblComments)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.Panel2)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Location", Global.AppWhShtB.My.MySettings.Default, "PrelimFormLocation", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DataBindings.Add(New System.Windows.Forms.Binding("ClientSize", Global.AppWhShtB.My.MySettings.Default, "PrelimFormSize", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = Global.AppWhShtB.My.MySettings.Default.PrelimFormLocation
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(720, 612)
        Me.Name = "FollowUpForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Enquiry Follow-Up Form"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.gpbxManager.ResumeLayout(False)
        Me.gpbxManager.PerformLayout()
        Me.gpbxStatus.ResumeLayout(False)
        Me.gpbxStatus.PerformLayout()
        CType(Me.EnquiryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActiveDealersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimResultsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrelimReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QRGListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryCommentBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEnquiryComments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnWorksheetWizard As System.Windows.Forms.Button
    Friend WithEvents btnSaveAndExit As System.Windows.Forms.Button
    Friend WithEvents btnWebApp As System.Windows.Forms.Button
    Friend WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents EnquiryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ActiveDealersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ActiveDealersTableAdapter
    Friend WithEvents ActiveDealersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ClientTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.ClientTableAdapter
    Friend WithEvents PrelimReasonsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimReasonsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimReasonsTableAdapter
    Friend WithEvents EnquiryWorkSheetDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimResultsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrelimResultsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.PrelimResultsTableAdapter
    Friend WithEvents gpbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblContactAddress As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblContactName As System.Windows.Forms.Label
    Friend WithEvents lblDealerName As System.Windows.Forms.Label
    Friend WithEvents lblContactPhone As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblTypeLoanEnquiry As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTimeValue As System.Windows.Forms.Label
    Friend WithEvents lblDateAndTime As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblEnquiryCode As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommentsOnlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientDetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckFinPowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PageSetupDialog1 As System.Windows.Forms.PageSetupDialog
    Friend WithEvents AddCommentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCandDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddCommentToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents gpbxManager As System.Windows.Forms.GroupBox
    Friend WithEvents lblEnquiryManagerName As System.Windows.Forms.Label
    Friend WithEvents UsersTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalFormToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WWWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckEnquiryNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndEnquiryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QrgListTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.QRGListTableAdapter
    Friend WithEvents QRGListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnViewDocs As System.Windows.Forms.Button
    Friend WithEvents btnDocs As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents DealersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnquiryCommentBS As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryCommentTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryCommentTableAdapter
    Friend WithEvents dgvEnquiryComments As System.Windows.Forms.DataGridView
    Friend WithEvents IdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnquiryIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateCreatedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAddComment As System.Windows.Forms.Button
End Class
