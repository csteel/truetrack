﻿Public Class frmTestCallMC

    Function CallMC() As Boolean

        Try

            Dim user As String = "spillai" 'MembersCentreUtil.UserId
            Dim pass As String = "5Pi11@i1" 'MembersCentreUtil.PassKey(user)

            OpenMembersCentre(user, pass)

            Return 1

        Catch ex As Exception
            MessageBox.Show("Error in Members Centre Link" & vbCrLf & ex.Message, "MembersCentre Link Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return 0

        End Try

    End Function


    Public Sub OpenMembersCentre(ByVal user As String, ByVal pass As String)
        'Dim MyBrowser As New WebBrowser
        Dim navUrl As String = ""
        Dim authHdr As String = "Authorization:" & System.Text.Encoding.UTF8.GetString(System.Text.Encoding.ASCII.GetBytes(user & ":" & pass)) '& vbCr & vbLf
        If My.Settings.IsTest = True Then
            navUrl = My.Settings.WebApplicationPage_TEST
        Else
            navUrl = My.Settings.WebApplicationPage_PROD
        End If

        Me.WebBrowser1.Navigate(navUrl, Nothing, Nothing, authHdr)
        'Me.WebBrowser1.Navigate("http://localhost:50143/MembersCentre/sales/default.aspx")
        'Me.WebBrowser1 = MyBrowser
    End Sub


    Private Sub frmTestCallMC_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CallMC()

    End Sub

End Class