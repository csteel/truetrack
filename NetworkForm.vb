﻿Public Class NetworkForm

    Private Sub btnPing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPing.Click
        Cursor.Current = Cursors.WaitCursor
        'update Status Strip
        StatusStripPingLabel.ForeColor = Color.Blue
        StatusStripPingLabel.Text = "Status: Server being pinged."
        Dim pingResult As Integer = 0
        Dim strPing As String
        strPing = txtbxPing.Text.ToString
        Try
            If My.Computer.Network.Ping(strPing) Then
                'update Status Strip
                StatusStripPingLabel.ForeColor = Color.Blue
                StatusStripPingLabel.Text = "Status: Server pinged successfully."
                pingResult = 1

            Else
                'update Status Strip
                StatusStripPingLabel.ForeColor = Color.Red
                StatusStripPingLabel.Text = "Status: Ping request timed out."
                pingResult = 2

            End If
        Catch ex As Net.NetworkInformation.PingException
            MsgBox("PingException: URL was not valid." & vbCrLf & ex.Message)
        Catch ex As InvalidOperationException
            MsgBox("InvalidOperationException: No network connection is available." & vbCrLf & ex.Message)
       
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        If pingResult = 0 Then
            'update Status Strip
            StatusStripPingLabel.ForeColor = Color.Black
            StatusStripPingLabel.Text = "Error in Ping.  Status:  Ready"
        End If
        Cursor.Current = Cursors.Default


    End Sub
End Class