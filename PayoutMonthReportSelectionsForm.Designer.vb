﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PayoutMonthReportSelectionsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PayoutMonthReportSelectionsForm))
        Me.StatusStripMessage = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.gpbxFilter = New System.Windows.Forms.GroupBox
        Me.ckbxUsers = New System.Windows.Forms.CheckBox
        Me.lstbxUsers = New System.Windows.Forms.ListBox
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet
        Me.gpbxSorting = New System.Windows.Forms.GroupBox
        Me.cmbxOrderBy = New System.Windows.Forms.ComboBox
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.cmbxGroupBy = New System.Windows.Forms.ComboBox
        Me.lblGroupBy = New System.Windows.Forms.Label
        Me.gpbxdateRange = New System.Windows.Forms.GroupBox
        Me.cmbxEndYear = New System.Windows.Forms.ComboBox
        Me.lblEndYear = New System.Windows.Forms.Label
        Me.cmbxEndMonth = New System.Windows.Forms.ComboBox
        Me.cmbxStartYear = New System.Windows.Forms.ComboBox
        Me.lblStartYear = New System.Windows.Forms.Label
        Me.cmbxStartMonth = New System.Windows.Forms.ComboBox
        Me.lblEndMonth = New System.Windows.Forms.Label
        Me.lblStartMonth = New System.Windows.Forms.Label
        Me.UsersTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
        Me.StatusStripMessage.SuspendLayout()
        Me.gpbxFilter.SuspendLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbxSorting.SuspendLayout()
        Me.gpbxdateRange.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStripMessage
        '
        Me.StatusStripMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStripMessage.Location = New System.Drawing.Point(0, 392)
        Me.StatusStripMessage.Name = "StatusStripMessage"
        Me.StatusStripMessage.Size = New System.Drawing.Size(434, 22)
        Me.StatusStripMessage.TabIndex = 25
        Me.StatusStripMessage.Text = "StatusStripMessage"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(83, 17)
        Me.ToolStripStatusLabel1.Text = "Status:  Ready."
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(347, 353)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 24
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(12, 353)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 23
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'gpbxFilter
        '
        Me.gpbxFilter.Controls.Add(Me.ckbxUsers)
        Me.gpbxFilter.Controls.Add(Me.lstbxUsers)
        Me.gpbxFilter.Location = New System.Drawing.Point(12, 182)
        Me.gpbxFilter.Name = "gpbxFilter"
        Me.gpbxFilter.Size = New System.Drawing.Size(410, 165)
        Me.gpbxFilter.TabIndex = 22
        Me.gpbxFilter.TabStop = False
        Me.gpbxFilter.Text = "Filter Information"
        '
        'ckbxUsers
        '
        Me.ckbxUsers.AutoSize = True
        Me.ckbxUsers.Location = New System.Drawing.Point(9, 19)
        Me.ckbxUsers.Name = "ckbxUsers"
        Me.ckbxUsers.Size = New System.Drawing.Size(73, 17)
        Me.ckbxUsers.TabIndex = 3
        Me.ckbxUsers.Text = "Managers"
        Me.ckbxUsers.UseVisualStyleBackColor = True
        '
        'lstbxUsers
        '
        Me.lstbxUsers.DataSource = Me.UsersBindingSource
        Me.lstbxUsers.DisplayMember = "UserName"
        Me.lstbxUsers.Enabled = False
        Me.lstbxUsers.FormattingEnabled = True
        Me.lstbxUsers.Location = New System.Drawing.Point(96, 19)
        Me.lstbxUsers.MultiColumn = True
        Me.lstbxUsers.Name = "lstbxUsers"
        Me.lstbxUsers.Size = New System.Drawing.Size(303, 134)
        Me.lstbxUsers.TabIndex = 2
        Me.lstbxUsers.ValueMember = "UserName"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'gpbxSorting
        '
        Me.gpbxSorting.Controls.Add(Me.cmbxOrderBy)
        Me.gpbxSorting.Controls.Add(Me.lblOrderBy)
        Me.gpbxSorting.Controls.Add(Me.cmbxGroupBy)
        Me.gpbxSorting.Controls.Add(Me.lblGroupBy)
        Me.gpbxSorting.Location = New System.Drawing.Point(12, 91)
        Me.gpbxSorting.Name = "gpbxSorting"
        Me.gpbxSorting.Size = New System.Drawing.Size(410, 85)
        Me.gpbxSorting.TabIndex = 21
        Me.gpbxSorting.TabStop = False
        Me.gpbxSorting.Text = "Sorting"
        '
        'cmbxOrderBy
        '
        Me.cmbxOrderBy.FormattingEnabled = True
        Me.cmbxOrderBy.Items.AddRange(New Object() {"None", "Manager", "Type of Loan", "Dealer", "Enquiry Code"})
        Me.cmbxOrderBy.Location = New System.Drawing.Point(96, 49)
        Me.cmbxOrderBy.Name = "cmbxOrderBy"
        Me.cmbxOrderBy.Size = New System.Drawing.Size(303, 21)
        Me.cmbxOrderBy.TabIndex = 3
        '
        'lblOrderBy
        '
        Me.lblOrderBy.AutoSize = True
        Me.lblOrderBy.Location = New System.Drawing.Point(6, 52)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(48, 13)
        Me.lblOrderBy.TabIndex = 2
        Me.lblOrderBy.Text = "Order By"
        '
        'cmbxGroupBy
        '
        Me.cmbxGroupBy.FormattingEnabled = True
        Me.cmbxGroupBy.Items.AddRange(New Object() {"None", "Manager", "Type of Loan", "Dealer", "Payout Month"})
        Me.cmbxGroupBy.Location = New System.Drawing.Point(96, 22)
        Me.cmbxGroupBy.Name = "cmbxGroupBy"
        Me.cmbxGroupBy.Size = New System.Drawing.Size(303, 21)
        Me.cmbxGroupBy.TabIndex = 1
        '
        'lblGroupBy
        '
        Me.lblGroupBy.AutoSize = True
        Me.lblGroupBy.Location = New System.Drawing.Point(6, 25)
        Me.lblGroupBy.Name = "lblGroupBy"
        Me.lblGroupBy.Size = New System.Drawing.Size(51, 13)
        Me.lblGroupBy.TabIndex = 0
        Me.lblGroupBy.Text = "Group By"
        '
        'gpbxdateRange
        '
        Me.gpbxdateRange.Controls.Add(Me.cmbxEndYear)
        Me.gpbxdateRange.Controls.Add(Me.lblEndYear)
        Me.gpbxdateRange.Controls.Add(Me.cmbxEndMonth)
        Me.gpbxdateRange.Controls.Add(Me.cmbxStartYear)
        Me.gpbxdateRange.Controls.Add(Me.lblStartYear)
        Me.gpbxdateRange.Controls.Add(Me.cmbxStartMonth)
        Me.gpbxdateRange.Controls.Add(Me.lblEndMonth)
        Me.gpbxdateRange.Controls.Add(Me.lblStartMonth)
        Me.gpbxdateRange.Location = New System.Drawing.Point(12, 3)
        Me.gpbxdateRange.Name = "gpbxdateRange"
        Me.gpbxdateRange.Size = New System.Drawing.Size(410, 82)
        Me.gpbxdateRange.TabIndex = 20
        Me.gpbxdateRange.TabStop = False
        Me.gpbxdateRange.Text = "Month Range"
        '
        'cmbxEndYear
        '
        Me.cmbxEndYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxEndYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxEndYear.FormattingEnabled = True
        Me.cmbxEndYear.Items.AddRange(New Object() {"2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "207", "2028", "2030"})
        Me.cmbxEndYear.Location = New System.Drawing.Point(299, 50)
        Me.cmbxEndYear.Name = "cmbxEndYear"
        Me.cmbxEndYear.Size = New System.Drawing.Size(100, 21)
        Me.cmbxEndYear.TabIndex = 12
        '
        'lblEndYear
        '
        Me.lblEndYear.AutoSize = True
        Me.lblEndYear.Location = New System.Drawing.Point(229, 53)
        Me.lblEndYear.Name = "lblEndYear"
        Me.lblEndYear.Size = New System.Drawing.Size(51, 13)
        Me.lblEndYear.TabIndex = 11
        Me.lblEndYear.Text = "End Year"
        '
        'cmbxEndMonth
        '
        Me.cmbxEndMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxEndMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxEndMonth.FormattingEnabled = True
        Me.cmbxEndMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmbxEndMonth.Location = New System.Drawing.Point(299, 22)
        Me.cmbxEndMonth.Name = "cmbxEndMonth"
        Me.cmbxEndMonth.Size = New System.Drawing.Size(100, 21)
        Me.cmbxEndMonth.TabIndex = 10
        '
        'cmbxStartYear
        '
        Me.cmbxStartYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxStartYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxStartYear.FormattingEnabled = True
        Me.cmbxStartYear.Items.AddRange(New Object() {"2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "207", "2028", "2030"})
        Me.cmbxStartYear.Location = New System.Drawing.Point(96, 50)
        Me.cmbxStartYear.Name = "cmbxStartYear"
        Me.cmbxStartYear.Size = New System.Drawing.Size(100, 21)
        Me.cmbxStartYear.TabIndex = 9
        '
        'lblStartYear
        '
        Me.lblStartYear.AutoSize = True
        Me.lblStartYear.Location = New System.Drawing.Point(6, 53)
        Me.lblStartYear.Name = "lblStartYear"
        Me.lblStartYear.Size = New System.Drawing.Size(54, 13)
        Me.lblStartYear.TabIndex = 8
        Me.lblStartYear.Text = "Start Year"
        '
        'cmbxStartMonth
        '
        Me.cmbxStartMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbxStartMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbxStartMonth.FormattingEnabled = True
        Me.cmbxStartMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmbxStartMonth.Location = New System.Drawing.Point(96, 22)
        Me.cmbxStartMonth.Name = "cmbxStartMonth"
        Me.cmbxStartMonth.Size = New System.Drawing.Size(100, 21)
        Me.cmbxStartMonth.TabIndex = 7
        '
        'lblEndMonth
        '
        Me.lblEndMonth.AutoSize = True
        Me.lblEndMonth.Location = New System.Drawing.Point(229, 25)
        Me.lblEndMonth.Name = "lblEndMonth"
        Me.lblEndMonth.Size = New System.Drawing.Size(59, 13)
        Me.lblEndMonth.TabIndex = 2
        Me.lblEndMonth.Text = "End Month"
        '
        'lblStartMonth
        '
        Me.lblStartMonth.AutoSize = True
        Me.lblStartMonth.Location = New System.Drawing.Point(6, 25)
        Me.lblStartMonth.Name = "lblStartMonth"
        Me.lblStartMonth.Size = New System.Drawing.Size(62, 13)
        Me.lblStartMonth.TabIndex = 0
        Me.lblStartMonth.Text = "Start Month"
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'PayoutMonthReportSelectionsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 414)
        Me.ControlBox = False
        Me.Controls.Add(Me.StatusStripMessage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.gpbxFilter)
        Me.Controls.Add(Me.gpbxSorting)
        Me.Controls.Add(Me.gpbxdateRange)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "PayoutMonthReportSelectionsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payout Month Report Selections"
        Me.StatusStripMessage.ResumeLayout(False)
        Me.StatusStripMessage.PerformLayout()
        Me.gpbxFilter.ResumeLayout(False)
        Me.gpbxFilter.PerformLayout()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbxSorting.ResumeLayout(False)
        Me.gpbxSorting.PerformLayout()
        Me.gpbxdateRange.ResumeLayout(False)
        Me.gpbxdateRange.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStripMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents gpbxFilter As System.Windows.Forms.GroupBox
    Friend WithEvents ckbxUsers As System.Windows.Forms.CheckBox
    Friend WithEvents lstbxUsers As System.Windows.Forms.ListBox
    Friend WithEvents gpbxSorting As System.Windows.Forms.GroupBox
    Friend WithEvents cmbxOrderBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents cmbxGroupBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroupBy As System.Windows.Forms.Label
    Friend WithEvents gpbxdateRange As System.Windows.Forms.GroupBox
    Friend WithEvents lblEndMonth As System.Windows.Forms.Label
    Friend WithEvents lblStartMonth As System.Windows.Forms.Label
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents cmbxStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblStartYear As System.Windows.Forms.Label
    Friend WithEvents cmbxEndMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxEndYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblEndYear As System.Windows.Forms.Label
End Class
