﻿
Imports System.IO
''' <summary>
''' Displays formatted text from a RTF document
''' </summary>
Public Class RtfMsgBox
    Dim filePath As String = String.Empty

    ''' <summary>
    ''' Displays message selected from parameters, RTF only
    ''' </summary>
    ''' <param name="displayText">File path from Startup path or text to be displayed</param>
    ''' <param name="title">Message box title</param>
    ''' <param name="mode">Use a FilePath or Display supplied Text</param>
    ''' <remarks>The form width is set by the method (500)</remarks>
    Friend Sub PassVariables(ByVal displayText As String, ByVal title As String, ByVal mode As MyEnums.MsgBoxMode)
        Dim formWidth As Integer = 500
        Try
            Select Case mode
                Case MyEnums.MsgBoxMode.FilePath
                    Me.Width = formWidth
                    Me.Text = title
                    filePath = Path.Combine(Application.StartupPath, displayText)
                    If filePath.Length > 0 Then
                        ' Load the contents of the file into the RichTextBox.
                        rtbMsg.LoadFile(filePath)
                    End If
                Case MyEnums.MsgBoxMode.DisplayText
                    Me.Width = formWidth
                    Me.Text = title
                    rtbMsg.Rtf = displayText
                Case Else
                    MessageBox.Show("No document information", "RTF Message Box", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    Me.DialogResult = System.Windows.Forms.DialogResult.Abort
            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message, "PassVariables", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.DialogResult = System.Windows.Forms.DialogResult.Abort
        End Try
    End Sub

    ''' <summary>
    ''' Displays message selected from parameters, RTF only
    ''' </summary>
    ''' <param name="displayText">File path from Startup path or text to be displayed</param>
    ''' <param name="title">Message box title</param>
    ''' <param name="mode">Use a FilePath or Display supplied Text</param>
    ''' <param name="formWidth">Width of the form</param>
    ''' <remarks>Use for RTF when you want to specify the form width</remarks>
    Friend Sub PassVariables(ByVal displayText As String, ByVal title As String, ByVal mode As MyEnums.MsgBoxMode, ByVal formWidth As Integer)
        Try
            Select Case mode
                Case MyEnums.MsgBoxMode.FilePath
                    Me.Width = formWidth
                    Me.Text = title
                    filePath = Path.Combine(Application.StartupPath, displayText)
                    If filePath.Length > 0 Then
                        ' Load the contents of the file into the RichTextBox.
                        rtbMsg.LoadFile(filePath)
                    End If
                Case MyEnums.MsgBoxMode.DisplayText
                    Me.Width = formWidth
                    Me.Text = title
                    rtbMsg.Rtf = displayText
                Case Else
                    MessageBox.Show("No document information", "RTF Message Box", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    Me.DialogResult = System.Windows.Forms.DialogResult.Abort
            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message, "PassVariables2", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.DialogResult = System.Windows.Forms.DialogResult.Abort
        End Try
    End Sub

    ''' <summary>
    ''' Displays message selected from parameters, can display plain or RTF text
    ''' </summary>
    ''' <param name="displayText">File path from Startup path or text to be displayed</param>
    ''' <param name="title">Message box title</param>
    ''' <param name="mode">Use a FilePath or Display supplied Text</param>
    ''' <param name="formWidth">Width of the form (500 is a good start)</param>
    ''' <param name="format">PlainText or RtfText</param>
    ''' <remarks>Overloaded signature</remarks>
    Friend Sub PassVariables(ByVal displayText As String, ByVal title As String, ByVal mode As MyEnums.MsgBoxMode, ByVal formWidth As Integer, ByVal format As MyEnums.MsgBoxFormat)
        Try
            Select Case mode
                Case MyEnums.MsgBoxMode.FilePath
                    Me.Width = formWidth
                    Me.Text = title
                    filePath = Path.Combine(Application.StartupPath, displayText)
                    If filePath.Length > 0 Then
                        ' Load the contents of the file into the RichTextBox.
                        rtbMsg.LoadFile(filePath)
                    End If
                Case MyEnums.MsgBoxMode.DisplayText
                    Me.Width = formWidth
                    Me.Text = title
                    If format = MyEnums.MsgBoxFormat.PlainText Then
                        rtbMsg.Text = displayText
                    ElseIf MyEnums.MsgBoxFormat.RtfText Then
                        rtbMsg.Rtf = displayText
                    End If
                Case Else
                    MessageBox.Show("No document information", "RTF Message Box", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    Me.DialogResult = System.Windows.Forms.DialogResult.Abort
            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message, "PassVariables2", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.DialogResult = System.Windows.Forms.DialogResult.Abort
        End Try
    End Sub



    Private Sub RtfMsgBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    ''' <summary>
    ''' This event is raised when the bounding rectangle necessary to accept new text changes.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>If the text within the control spans multiple lines, the requested rectangle will always be the width of the control. You can handle this event in your control to implement auto-resizing for multiline RichTextBox controls. The ContentsResizedEventArgs identifies the requested size of the RichTextBox.</remarks>
    Private Sub RtbMsgContentsResized(sender As Object, e As ContentsResizedEventArgs) Handles rtbMsg.ContentsResized
        'Dim messageBoxVB As New System.Text.StringBuilder()
        'messageBoxVB.AppendFormat("{0} = {1}", "NewRectangle", e.NewRectangle)
        'messageBoxVB.AppendLine()
        'MessageBox.Show(messageBoxVB.ToString)
        Me.Height = e.NewRectangle.Height + 94

    End Sub

    Private Sub RtfMsgBox_Layout(sender As Object, e As LayoutEventArgs) Handles Me.Layout
        rtbMsg.Width = Me.Width - 34
        rtbMsg.Height = Me.Height - 93
        btnOK.Location = New Point((Me.Width - btnOK.Width) * 0.5, rtbMsg.Location.Y + rtbMsg.Height + 6)
    End Sub

    Private Sub BtnOKClick(sender As Object, e As EventArgs) Handles btnOK.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub
End Class