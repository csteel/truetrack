﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmailForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EmailForm))
        Me.lblEmailFrom = New System.Windows.Forms.Label()
        Me.txtbxEmailFrom = New System.Windows.Forms.TextBox()
        Me.txtbxEmailSubject = New System.Windows.Forms.TextBox()
        Me.lblSubject = New System.Windows.Forms.Label()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblSignature = New System.Windows.Forms.Label()
        Me.lblSignatureLabel = New System.Windows.Forms.Label()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txtbxAttachmentsList = New System.Windows.Forms.TextBox()
        Me.lblAttachments = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pbEmail = New System.Windows.Forms.PictureBox()
        Me.cmbxEmailTo = New System.Windows.Forms.ComboBox()
        Me.EnquiryTableAdapter1 = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter()
        Me.EnquiryBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.EnquiryWorkSheetDataSet = New AppWhShtB.EnquiryWorkSheetDataSet()
        Me.DealerContactDetailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DealerContactDetailsTableAdapter = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter()
        Me.TableAdapterManager = New AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager()
        Me.lblEmailTo = New System.Windows.Forms.Label()
        Me.lblEmailCC = New System.Windows.Forms.Label()
        Me.cmbxCC = New System.Windows.Forms.ComboBox()
        Me.txtbxEmailBody = New AppWhShtB.SpellBox()
        CType(Me.pbEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DealerContactDetailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblEmailFrom
        '
        Me.lblEmailFrom.AutoSize = True
        Me.lblEmailFrom.Location = New System.Drawing.Point(16, 103)
        Me.lblEmailFrom.Name = "lblEmailFrom"
        Me.lblEmailFrom.Size = New System.Drawing.Size(58, 13)
        Me.lblEmailFrom.TabIndex = 2
        Me.lblEmailFrom.Text = "Email from:"
        '
        'txtbxEmailFrom
        '
        Me.txtbxEmailFrom.Location = New System.Drawing.Point(80, 100)
        Me.txtbxEmailFrom.Name = "txtbxEmailFrom"
        Me.txtbxEmailFrom.ReadOnly = True
        Me.txtbxEmailFrom.Size = New System.Drawing.Size(439, 20)
        Me.txtbxEmailFrom.TabIndex = 3
        '
        'txtbxEmailSubject
        '
        Me.txtbxEmailSubject.Location = New System.Drawing.Point(80, 126)
        Me.txtbxEmailSubject.Name = "txtbxEmailSubject"
        Me.txtbxEmailSubject.Size = New System.Drawing.Size(439, 20)
        Me.txtbxEmailSubject.TabIndex = 4
        '
        'lblSubject
        '
        Me.lblSubject.AutoSize = True
        Me.lblSubject.Location = New System.Drawing.Point(28, 129)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(46, 13)
        Me.lblSubject.TabIndex = 5
        Me.lblSubject.Text = "Subject:"
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(21, 155)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(53, 13)
        Me.lblMessage.TabIndex = 7
        Me.lblMessage.Text = "Message:"
        '
        'btnSend
        '
        Me.btnSend.BackColor = System.Drawing.Color.PaleGreen
        Me.btnSend.Location = New System.Drawing.Point(444, 624)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 8
        Me.btnSend.Text = "Send"
        Me.btnSend.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.MistyRose
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(80, 624)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'lblSignature
        '
        Me.lblSignature.AutoSize = True
        Me.lblSignature.Location = New System.Drawing.Point(94, 525)
        Me.lblSignature.Name = "lblSignature"
        Me.lblSignature.Size = New System.Drawing.Size(52, 13)
        Me.lblSignature.TabIndex = 10
        Me.lblSignature.Text = "Signature"
        '
        'lblSignatureLabel
        '
        Me.lblSignatureLabel.AutoSize = True
        Me.lblSignatureLabel.Location = New System.Drawing.Point(19, 525)
        Me.lblSignatureLabel.Name = "lblSignatureLabel"
        Me.lblSignatureLabel.Size = New System.Drawing.Size(55, 13)
        Me.lblSignatureLabel.TabIndex = 11
        Me.lblSignatureLabel.Text = "Signature:"
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.Location = New System.Drawing.Point(80, 13)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(303, 13)
        Me.lblInfo.TabIndex = 12
        Me.lblInfo.Text = "Email will be sent in Plain Text, does not support formatted text."
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button1.Image = Global.AppWhShtB.My.Resources.Resources.paperclip16a
        Me.Button1.Location = New System.Drawing.Point(494, 488)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(23, 23)
        Me.Button1.TabIndex = 13
        Me.ToolTip1.SetToolTip(Me.Button1, "Click to add an attachment")
        Me.Button1.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txtbxAttachmentsList
        '
        Me.txtbxAttachmentsList.Location = New System.Drawing.Point(80, 491)
        Me.txtbxAttachmentsList.Name = "txtbxAttachmentsList"
        Me.txtbxAttachmentsList.ReadOnly = True
        Me.txtbxAttachmentsList.Size = New System.Drawing.Size(408, 20)
        Me.txtbxAttachmentsList.TabIndex = 14
        '
        'lblAttachments
        '
        Me.lblAttachments.AutoSize = True
        Me.lblAttachments.Location = New System.Drawing.Point(8, 493)
        Me.lblAttachments.Name = "lblAttachments"
        Me.lblAttachments.Size = New System.Drawing.Size(66, 13)
        Me.lblAttachments.TabIndex = 15
        Me.lblAttachments.Text = "Attachments"
        '
        'pbEmail
        '
        Me.pbEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbEmail.Image = Global.AppWhShtB.My.Resources.Resources.questionBsm
        Me.pbEmail.Location = New System.Drawing.Point(507, 8)
        Me.pbEmail.Name = "pbEmail"
        Me.pbEmail.Size = New System.Drawing.Size(18, 18)
        Me.pbEmail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbEmail.TabIndex = 29
        Me.pbEmail.TabStop = False
        Me.ToolTip1.SetToolTip(Me.pbEmail, "How to Separate Multiple Email Recipients")
        '
        'cmbxEmailTo
        '
        Me.cmbxEmailTo.FormattingEnabled = True
        Me.cmbxEmailTo.Location = New System.Drawing.Point(80, 46)
        Me.cmbxEmailTo.Name = "cmbxEmailTo"
        Me.cmbxEmailTo.Size = New System.Drawing.Size(439, 21)
        Me.cmbxEmailTo.TabIndex = 17
        '
        'EnquiryTableAdapter1
        '
        Me.EnquiryTableAdapter1.ClearBeforeFill = True
        '
        'EnquiryBindingSource1
        '
        Me.EnquiryBindingSource1.DataSource = Me.EnquiryWorkSheetDataSet
        Me.EnquiryBindingSource1.Position = 0
        '
        'EnquiryWorkSheetDataSet
        '
        Me.EnquiryWorkSheetDataSet.DataSetName = "EnquiryWorkSheetDataSet"
        Me.EnquiryWorkSheetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DealerContactDetailsBindingSource
        '
        Me.DealerContactDetailsBindingSource.DataMember = "DealerContactDetails"
        Me.DealerContactDetailsBindingSource.DataSource = Me.EnquiryWorkSheetDataSet
        '
        'DealerContactDetailsTableAdapter
        '
        Me.DealerContactDetailsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ActiveDealersTableAdapter = Nothing
        Me.TableAdapterManager.AkldSuburbsTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationCodeGeneratorTableAdapter = Nothing
        Me.TableAdapterManager.ApplicationValuesTableAdapter = Nothing
        Me.TableAdapterManager.AppSettingsTableAdapter = Nothing
        Me.TableAdapterManager.ArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BranchesTableAdapter = Nothing
        Me.TableAdapterManager.BudgetTableAdapter = Nothing
        Me.TableAdapterManager.ContractTypesTableAdapter = Nothing
        Me.TableAdapterManager.CurrentStatusTableAdapter = Nothing
        Me.TableAdapterManager.CustomerArchiveEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerEnquiryTableAdapter = Nothing
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.DTSB01TableAdapter = Nothing
        Me.TableAdapterManager.DTSBTableAdapter = Nothing
        Me.TableAdapterManager.DueDiligenceTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryCommentTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryMethodListTableAdapter = Nothing
        Me.TableAdapterManager.EnquiryTableAdapter = Me.EnquiryTableAdapter1
        Me.TableAdapterManager.Enum_CustomerEnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_CustomerTypeTableAdapter = Nothing
        Me.TableAdapterManager.Enum_EnquiryTypeTableAdapter = Nothing
        Me.TableAdapterManager.finPowerNamesTableAdapter = Nothing
        Me.TableAdapterManager.LatencyPointsTableAdapter = Nothing
        Me.TableAdapterManager.List_AMLRiskTableAdapter = Nothing
        Me.TableAdapterManager.LoanPurposeTableAdapter = Nothing
        Me.TableAdapterManager.LoanTypesTableAdapter = Nothing
        Me.TableAdapterManager.NZTownsTableAdapter = Nothing
        Me.TableAdapterManager.PayoutTableAdapter = Nothing
        Me.TableAdapterManager.PrelimReasonsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimResultsTableAdapter = Nothing
        Me.TableAdapterManager.PrelimSourceTableAdapter = Nothing
        Me.TableAdapterManager.QRGListTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTableAdapter = Nothing
        Me.TableAdapterManager.SecurityTypesTableAdapter = Nothing
        Me.TableAdapterManager.SuburbListTableAdapter = Nothing
        Me.TableAdapterManager.TradeReferencesTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfClientTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfLoanTableAdapter = Nothing
        Me.TableAdapterManager.TypeOfTenancyTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRolesTableAdapter = Nothing
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'lblEmailTo
        '
        Me.lblEmailTo.AutoSize = True
        Me.lblEmailTo.Location = New System.Drawing.Point(27, 49)
        Me.lblEmailTo.Name = "lblEmailTo"
        Me.lblEmailTo.Size = New System.Drawing.Size(47, 13)
        Me.lblEmailTo.TabIndex = 18
        Me.lblEmailTo.Text = "Email to:"
        '
        'lblEmailCC
        '
        Me.lblEmailCC.AutoSize = True
        Me.lblEmailCC.Location = New System.Drawing.Point(50, 76)
        Me.lblEmailCC.Name = "lblEmailCC"
        Me.lblEmailCC.Size = New System.Drawing.Size(24, 13)
        Me.lblEmailCC.TabIndex = 20
        Me.lblEmailCC.Text = "CC:"
        '
        'cmbxCC
        '
        Me.cmbxCC.FormattingEnabled = True
        Me.cmbxCC.Location = New System.Drawing.Point(80, 73)
        Me.cmbxCC.Name = "cmbxCC"
        Me.cmbxCC.Size = New System.Drawing.Size(439, 21)
        Me.cmbxCC.TabIndex = 19
        '
        'txtbxEmailBody
        '
        Me.txtbxEmailBody.IsReadOnly = False
        Me.txtbxEmailBody.Location = New System.Drawing.Point(80, 155)
        Me.txtbxEmailBody.MaxLength = 0
        Me.txtbxEmailBody.MultiLine = True
        Me.txtbxEmailBody.Name = "txtbxEmailBody"
        Me.txtbxEmailBody.SelectedText = ""
        Me.txtbxEmailBody.SelectionLength = 0
        Me.txtbxEmailBody.SelectionStart = 0
        Me.txtbxEmailBody.Size = New System.Drawing.Size(439, 330)
        Me.txtbxEmailBody.TabIndex = 6
        Me.txtbxEmailBody.WordWrap = True
        Me.txtbxEmailBody.Child = New System.Windows.Controls.TextBox()
        '
        'EmailForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(537, 660)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbEmail)
        Me.Controls.Add(Me.lblEmailCC)
        Me.Controls.Add(Me.cmbxCC)
        Me.Controls.Add(Me.txtbxEmailBody)
        Me.Controls.Add(Me.lblEmailTo)
        Me.Controls.Add(Me.cmbxEmailTo)
        Me.Controls.Add(Me.lblAttachments)
        Me.Controls.Add(Me.txtbxAttachmentsList)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.lblSignatureLabel)
        Me.Controls.Add(Me.lblSignature)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.lblSubject)
        Me.Controls.Add(Me.txtbxEmailSubject)
        Me.Controls.Add(Me.txtbxEmailFrom)
        Me.Controls.Add(Me.lblEmailFrom)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(960, 1050)
        Me.MinimumSize = New System.Drawing.Size(553, 612)
        Me.Name = "EmailForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Email Form"
        CType(Me.pbEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnquiryWorkSheetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DealerContactDetailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblEmailFrom As System.Windows.Forms.Label
    Friend WithEvents txtbxEmailFrom As System.Windows.Forms.TextBox
    Friend WithEvents txtbxEmailSubject As System.Windows.Forms.TextBox
    Friend WithEvents lblSubject As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents EnquiryTableAdapter1 As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.EnquiryTableAdapter
    Friend WithEvents EnquiryBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents EnquiryWorkSheetDataSet As AppWhShtB.EnquiryWorkSheetDataSet
    Friend WithEvents lblSignature As System.Windows.Forms.Label
    Friend WithEvents lblSignatureLabel As System.Windows.Forms.Label
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtbxAttachmentsList As System.Windows.Forms.TextBox
    Friend WithEvents lblAttachments As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents DealerContactDetailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DealerContactDetailsTableAdapter As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.DealerContactDetailsTableAdapter
    Friend WithEvents TableAdapterManager As AppWhShtB.EnquiryWorkSheetDataSetTableAdapters.TableAdapterManager
    Friend WithEvents cmbxEmailTo As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmailTo As System.Windows.Forms.Label
    Friend WithEvents txtbxEmailBody As AppWhShtB.SpellBox
    Friend WithEvents lblEmailCC As System.Windows.Forms.Label
    Friend WithEvents cmbxCC As System.Windows.Forms.ComboBox
    Friend WithEvents pbEmail As System.Windows.Forms.PictureBox
End Class
