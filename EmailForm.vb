﻿Imports System.Net.Mail
Imports System.IO

Public Class EmailForm
    Dim ThisEnquiryId As Integer
    Dim fileAttch As New ArrayList 'List of attachments
    Dim AttachListString As String = "" 'String listing attachment file names
    Dim AttachmentError As Boolean = False
    Dim ThisDealerId As String
    Dim DealerEmail As String
    Dim EnquiryContactEmail As String
    Dim signature As String


    'Loads form with parameters from Calling Form
    Friend Sub PassVariable(ByVal LoadEnquiryVar As Integer, ByVal LoadDealerIdVar As String)
        ThisEnquiryId = LoadEnquiryVar
        ThisDealerId = LoadDealerIdVar
        'set email from address
        txtbxEmailFrom.Text = LoggedinEmailAddress
        'signature
        signature = Util.GetSignature
        lblSignature.Text = signature

        'get Dealer email address
        Me.DealerContactDetailsTableAdapter.FillByDealerEmail(EnquiryWorkSheetDataSet.DealerContactDetails, ThisDealerId, "Email")
        For Each dr As DataRow In EnquiryWorkSheetDataSet.Tables("DealerContactDetails").Rows
            If dr.Item("ContactMethod") = "Email" Then
                cmbxEmailTo.Items.Add(dr.Item("ContactValue"))
                cmbxCC.Items.Add(dr.Item("ContactValue"))
            End If
        Next
        'add Contact Email Address to combobox
        Me.EnquiryTableAdapter1.FillByEnquiryId(EnquiryWorkSheetDataSet.Enquiry, ThisEnquiryId)
        If Not IsDBNull(EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource1.Position()).Item("ContactEmail")) Then
            EnquiryContactEmail = EnquiryWorkSheetDataSet.Enquiry.Rows(EnquiryBindingSource1.Position()).Item("ContactEmail")
            If Not EnquiryContactEmail = String.Empty Then
                cmbxEmailTo.Items.Add(EnquiryContactEmail)
                cmbxCC.Items.Add(EnquiryContactEmail)
            End If
        End If

        'DealerEmail = EnquiryWorkSheetDataSet.DealerContactDetails.Rows(DealerContactDetailsBindingSource.Position()).Item("ContactValue")
        'txtbxEmailTo.Text = DealerEmail
        'cmbxEmailTo.Items.Add(DealerEmail)

    End Sub

    Private Sub EmailForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Cursor.Current = Cursors.WaitCursor
        Dim checkFieldsMessage As String = ""
        'Dim emailAddresses As String()
        Dim emailToAddresses As ArrayList
        Dim emailCcAddresses As ArrayList
        Dim emailPart As String
        'check email to field
        If Not cmbxEmailTo.Text.Length > 0 Then
            checkFieldsMessage = checkFieldsMessage + "Please enter address for 'email to'!" + vbCrLf
        ElseIf cmbxEmailTo.Text.Contains(",") Then
            emailToAddresses = New ArrayList(cmbxEmailTo.Text.Split(New Char() {","c}))
            For Each emailPart In emailToAddresses
                If Not ValidateEmail(emailPart.Trim) Then
                    checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a vaild email address!" + vbCrLf
                End If
            Next
        ElseIf cmbxEmailTo.Text.Contains(";") Then
            emailToAddresses = New ArrayList(cmbxEmailTo.Text.Split(New Char() {";"c}))
            For Each emailPart In emailToAddresses
                If Not ValidateEmail(emailPart.Trim) Then
                    checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a vaild email address!" + vbCrLf
                End If
            Next
        Else
            emailToAddresses = New ArrayList
            emailToAddresses.Add(cmbxEmailTo.Text.Trim)
            If Not ValidateEmail(cmbxEmailTo.Text.Trim) Then
                checkFieldsMessage = checkFieldsMessage + "Please enter a valid 'to' email address!" + vbCrLf
            End If
        End If
        'check email CC
        If cmbxCC.Text.Length > 0 Then
            If cmbxCC.Text.Contains(",") Then
                'emailAddresses = cmbxEmailTo.Text.Split(New Char() {","c})
                emailCcAddresses = New ArrayList(cmbxCC.Text.Split(New Char() {","c}))
                For Each emailPart In emailCcAddresses
                    If Not ValidateEmail(emailPart.Trim) Then
                        checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a valid email address!" + vbCrLf
                    End If
                Next
            ElseIf cmbxCC.Text.Contains(";") Then
                'emailAddresses = cmbxEmailTo.Text.Split(New Char() {";"c})
                emailCcAddresses = New ArrayList(cmbxCC.Text.Split(New Char() {";"c}))
                For Each emailPart In emailCcAddresses
                    If Not ValidateEmail(emailPart.Trim) Then
                        checkFieldsMessage = checkFieldsMessage + emailPart.Trim & " is not a vaild email address!" + vbCrLf
                    End If
                Next
            Else
                emailCcAddresses = New ArrayList
                emailCcAddresses.Add(cmbxCC.Text.Trim)
                If Not ValidateEmail(cmbxCC.Text.Trim) Then
                    checkFieldsMessage = checkFieldsMessage + "Please enter a valid 'CC' email address!" + vbCrLf
                End If
            End If
        End If

        'check email subject
        If txtbxEmailSubject.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a subject!" + vbCrLf
        End If
        'check email message
        If txtbxEmailBody.Text = "" Then
            checkFieldsMessage = checkFieldsMessage + "Please enter a message!" + vbCrLf
        End If
        'check for error messages
        If Not checkFieldsMessage = "" Then
            MsgBox(checkFieldsMessage)
            Me.DialogResult = System.Windows.Forms.DialogResult.None
        Else
            'send email
            Try
                Dim ThisMail As New MailMessage
                ThisMail.From = New MailAddress(txtbxEmailFrom.Text)
                'ThisMail.To
                If emailToAddresses.Count > 0 Then
                    For Each emailPart In emailToAddresses
                        ThisMail.To.Add(emailPart.Trim)
                    Next
                End If
                'ThisMail.CC
                If cmbxCC.Text.Length > 0 Then
                    If emailCcAddresses.Count > 0 Then
                        For Each emailPart In emailCcAddresses
                            ThisMail.CC.Add(emailPart.Trim)
                        Next
                    End If
                End If
                'set content
                ThisMail.Subject = txtbxEmailSubject.Text
                ThisMail.Body = txtbxEmailBody.Text & vbCrLf & vbCrLf & signature
                ThisMail.IsBodyHtml = False
                'add attachments
                Dim Attachment As System.Net.Mail.Attachment
                For Each ListFileName As String In fileAttch
                    Dim finfo As New FileInfo(ListFileName)
                    If finfo.Exists Then
                        Attachment = New Net.Mail.Attachment(ListFileName)
                        ThisMail.Attachments.Add(Attachment)
                    Else
                        MessageBox.Show("Can not find file " & finfo.Name, "Missing file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        AttachmentError = True
                    End If
                Next

                'send mail
                Dim client As New SmtpClient(My.Settings.SmtpClient)
                client.Port = My.Settings.SmtpClientPort

                '*********************** adjust credentials
                client.UseDefaultCredentials = True
                'client.Credentials = New Net.NetworkCredential("csteel", "Art1s@n1")
                '*********************** end of adjust credentials
                If AttachmentError = True Then
                    MessageBox.Show("Attachment error detected, sending email aborted", "Attachment Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    client.Send(ThisMail)
                    Cursor.Current = Cursors.Default

                    'user reassurance message
                    MessageBox.Show(String.Format("Message Subject: ' {0} ' successfully sent" & vbCrLf & "from: {1}" & vbCrLf & "to: {2}" & vbCrLf & "CC: {3}", ThisMail.Subject, ThisMail.From, ThisMail.To.ToString, ThisMail.CC.ToString), "EMail", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                    Cursor.Current = Cursors.WaitCursor
                    'add comments to LoanComments data
                    If ThisEnquiryId > 0 Then
                        Dim CommentStr As String
                        Dim TheseAttachments As String
                        If txtbxAttachmentsList.Text.Length > 0 Then
                            TheseAttachments = "Attachments sent: " & txtbxAttachmentsList.Text & vbCrLf
                        Else
                            TheseAttachments = ""
                        End If
                        CommentStr = "Email sent to: " & ThisMail.To.ToString & vbCrLf & "CC: " & ThisMail.CC.ToString & vbCrLf & "Subject: " & ThisMail.Subject & vbCrLf & txtbxEmailBody.Text & vbCrLf & TheseAttachments
                        Dim commentId As Integer
                        commentId = Util.SetComment(ThisEnquiryId, LoggedinName, CommentStr)

                    End If
                    '  Housekeeping
                    ThisMail.Dispose()
                    Cursor.Current = Cursors.Default
                    'Close Form
                    Me.DialogResult = System.Windows.Forms.DialogResult.OK

                End If


            Catch ex As FormatException
                Dim inner As Exception = ex.InnerException
                If Not (inner Is Nothing) Then
                    MsgBox("Format Exception: " + vbCrLf + inner.Message)
                    inner = inner.InnerException
                Else
                    MsgBox("Format Exception: " + vbCrLf + ex.Message)
                End If
            Catch ex As SmtpException
                Dim inner As Exception = ex.InnerException
                If Not (inner Is Nothing) Then
                    MsgBox("SMTP Exception: " + vbCrLf + inner.Message)
                    inner = inner.InnerException
                Else
                    MsgBox("SMTP Exception: " + vbCrLf + ex.Message)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        End If


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Add an attachment
        Dim ThisFileName As String = ""
        ' Displays an OpenFileDialog so the user can select a file.
        Dim openFileDialog1 As New OpenFileDialog()
        openFileDialog1.Filter = "Text|*.txt|All|*.*"
        openFileDialog1.Title = "Select a Document File"
        'Show the Dialog.
        'If the user clicked OK in the dialog and a document file was selected, get the file path
        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            ThisFileName = openFileDialog1.FileName
            'MsgBox(ThisFileName)
            fileAttch.Add(ThisFileName)
            AttachListString = ""
            For Each ListFileName As String In fileAttch
                Dim finfo As New FileInfo(ListFileName)
                If finfo.Exists Then
                    AttachListString = AttachListString & finfo.Name & "; "
                Else
                    MessageBox.Show("Can not find file " & finfo.Name, "Missing file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Next
        End If
        txtbxAttachmentsList.Text = AttachListString
    End Sub

#Region "Information buttons"
    Private Sub pbEmail_Click(sender As Object, e As EventArgs) Handles pbEmail.Click
        Dim rtfMsgBoxFrm As New RtfMsgBox
        rtfMsgBoxFrm.PassVariables("Documents\EmailInformation.rtf", "Email information", MyEnums.MsgBoxMode.FilePath)
        If (rtfMsgBoxFrm.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then

        Else

        End If
        rtfMsgBoxFrm.Dispose()
    End Sub

    Private Sub pbEmail_MouseHover(sender As Object, e As EventArgs) Handles pbEmail.MouseHover
        pbEmail.Image = My.Resources.questionGsm
    End Sub

    Private Sub pbEmail_MouseLeave(sender As Object, e As EventArgs) Handles pbEmail.MouseLeave
        pbEmail.Image = My.Resources.questionBsm
    End Sub

#End Region

    
End Class