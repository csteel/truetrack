﻿Imports Microsoft.Reporting.WinForms
Public Class ReportMarketingForm


    Dim ThisStartdate As Date
    Dim ThisEndDate As Date
    Dim ThisGroupBy As String
    Dim ThisOrderBy As String
    Dim ThisArchiveStatus As String
    Dim ThisLoanPurpose As String
    Dim ThisSource As String
    Dim ThisLoanType As String
    'user settings
    Dim NewLocX As Integer
    Dim NewLocY As Integer

    Friend Sub ReportSelections_PassVariable(ByVal Startdate As Date, ByVal EndDate As Date, ByVal GroupBy As String, ByVal OrderBy As String, ByVal LoanPurpose As String, ByVal Source As String, ByVal LoanType As String, ByVal ArchiveStatus As String)

        ThisStartdate = Startdate
        ThisEndDate = EndDate
        ThisGroupBy = GroupBy
        ThisOrderBy = OrderBy
        ThisLoanPurpose = LoanPurpose
        ThisSource = Source
        ThisLoanType = LoanType

        'MsgBox("Group By: " & ThisGroupBy)
        'MsgBox("User: " & ThisUser)
        'MsgBox("SortBy: " & ThisOrderBy)
        'get user settings

        Me.ClientSize = New Size(My.Settings.ReportEnquiryFormSize)
        Me.Location = New Point(My.Settings.ReportEnquiryFormLocation)
        Select Case ArchiveStatus
            Case Is = "Include Archives"
                'ReportViewer2.Visible = False
                ReportViewer1.Visible = True

                Try
                    'load tableAdapter
                    Me.CurrentAndArchiveEnquiryTableAdapter.FillByDates(Me.EnquiryWorkSheetDataSet.CurrentAndArchiveEnquiry, ThisStartdate, ThisEndDate)
                    'set report parameters
                    'Title
                    Dim p As New ReportParameter("parReportTitle", "Enquiry List")
                    'Group By
                    Dim q As New ReportParameter("parReportGroupBy", ThisGroupBy)
                    'OrderBy
                    Dim r As New ReportParameter("parReportOrderBy", ThisOrderBy)
                    'set LoggedinName
                    Dim t As New ReportParameter("parReportLoggedinName", LoggedinName)
                    'set parReportPurpose
                    Dim u As New ReportParameter("parReportPurpose", ThisLoanPurpose)
                    'set parReportStartDate
                    Dim v As New ReportParameter("parReportStartDate", ThisStartdate)
                    'set parReportEnddate
                    Dim w As New ReportParameter("parReportEndDate", ThisEndDate)
                    'set parReportSource
                    Dim x As New ReportParameter("parReportSource", ThisSource)
                    'set parReportType
                    Dim y As New ReportParameter("parReportType", ThisLoanType)
                    ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p, q, r, t, u, v, w, x, y})

                    Me.ReportViewer1.RefreshReport()

                Catch e As System.Exception
                    Dim inner As Exception = e.InnerException
                    If Not (inner Is Nothing) Then
                        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + inner.Message)
                        inner = inner.InnerException
                    Else
                        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + e.Message)
                    End If
                End Try
            Case Is = "Current"
                'ReportViewer2.Visible = True
                'ReportViewer1.Visible = False
                'Try
                '    'load tableAdapter
                '    Me.CurrentEnquiryReportTableAdapter.FillByDates(Me.EnquiryWorkSheetDataSet.CurrentEnquiryReport, ThisStartdate, ThisEndDate)
                '    'set report parameters
                '    'Title
                '    Dim p As New ReportParameter("parReportTitle", "Enquiry List")
                '    'Group By
                '    Dim q As New ReportParameter("parReportGroupBy", ThisGroupBy)
                '    'OrderBy
                '    Dim r As New ReportParameter("parReportOrderBy", ThisOrderBy)
                '    'set LoggedinName
                '    Dim t As New ReportParameter("parReportLoggedinName", LoggedinName)
                '    'set parReportUser
                '    Dim u As New ReportParameter("parReportUser", ThisLoanPurpose)
                '    'set parReportStartDate
                '    Dim v As New ReportParameter("parReportStartDate", ThisStartdate)
                '    'set parReportEnddate
                '    Dim w As New ReportParameter("parReportEndDate", ThisEndDate)
                '    ReportViewer2.LocalReport.SetParameters(New ReportParameter() {p, q, r, t, u, v, w})

                '    Me.ReportViewer2.RefreshReport()

                'Catch e As System.Exception
                '    Dim inner As Exception = e.InnerException
                '    If Not (inner Is Nothing) Then
                '        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + inner.Message)
                '        inner = inner.InnerException
                '    Else
                '        MsgBox("Error launching ReportEnquiryForm: " + vbCrLf + e.Message)
                '    End If
                'End Try

            Case Else

        End Select

    End Sub

    Private Sub _ReportTest_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ''TODO: This line of code loads data into the 'EnquiryWorkSheetDataSet.CurrentAndArchiveEnquiry' table. You can move, or remove it, as needed.
        'Me.CurrentAndArchiveEnquiryTableAdapter.Fill(Me.EnquiryWorkSheetDataSet.CurrentAndArchiveEnquiry)

        'Me.ReportViewer1.RefreshReport()
    End Sub
End Class