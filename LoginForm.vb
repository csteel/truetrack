﻿Imports System.Data.SqlClient
Public Class LoginForm

    ' Insert code to perform custom authentication using the provided username and password 
    ' (See http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            If My.Computer.Network.Ping(My.Settings.SQLServer) Then
                Try
                    'MsgBox("SQLServer pinged successfully.")
                    Dim isAvailable As Boolean
                    isAvailable = My.Computer.Network.IsAvailable
                    If isAvailable = True Then
                        Dim connectionString As String = My.Settings.EnquiryWorkSheetConnectionString
                        Dim connection As New SqlConnection(connectionString)
                        Dim selectStatement As String = "SELECT Users.UserId,Users.UserName,Users.Password,Users.role,Users.BranchId,Users.active,Users.emailaddress,Users.FullName,UserRoles.PermissionLevel FROM Users Users INNER JOIN UserRoles ON Users.role = UserRoles.Role WHERE UserName ='" & UsernameTextBox.Text & "' AND Password ='" & PasswordTextBox.Text & "'"
                        'TODO: For testing
                        'selectStatement = "SELECT Users.UserId,Users.UserName,Users.Password,Users.role,Users.BranchId,Users.active,Users.emailaddress,Users.FullName,UserRoles.PermissionLevel FROM Users Users INNER JOIN UserRoles ON Users.role = UserRoles.Role WHERE UserName ='" & "csteel" & "' AND Password ='" & "Art1s@n1" & "'"
                        'end of testing
                        Dim selectCommand As New SqlCommand(selectStatement, connection)
                        Dim LoginDataAdapter As New SqlDataAdapter(selectCommand)
                        Dim LoginDataSet As New DataSet
                        Dim LoginDataTable As New DataTable


                        Try
                            'dumps results into datatable LoginDataTable
                            LoginDataAdapter.Fill(LoginDataTable)
                            'if no matching rows .....
                            If LoginDataTable.Rows.Count = 0 Then
                                MessageBox.Show("Login failed, please try again.")
                                'lblLoginError.ForeColor = Color.Red
                                'lblLoginError.Text = "Login failed, please try again."
                                'lblLoginError.Visible = True
                                ' clear the dataTable and the Connect information
                                LoginDataAdapter = Nothing
                                LoginDataTable.Clear()
                                'if there is a matching row
                            ElseIf LoginDataTable.Rows.Count = 1 Then
                                'get active value
                                Dim LoginDataRow As DataRow = LoginDataTable.Rows(0)
                                If LoginDataRow.Item(5) = True Then
                                    'assign username to Public variable LoggedinName
                                    LoggedinName = LoginDataRow.Item(1)
                                    'assign role to Public variable UserRole
                                    UserRole = LoginDataRow.Item(3)
                                    'assign UserId to Public variable LoggedinId
                                    LoggedinId = LoginDataRow.Item(0)
                                    'assign BranchId to Public variable LoggedinBranchId
                                    LoggedinBranchId = LoginDataRow.Item(4)
                                    'assign emailaddress
                                    LoggedinEmailAddress = LoginDataRow.Item(6)
                                    'assign FullName
                                    LoggedinFullName = LoginDataRow.Item(7)
                                    'assign Permission level to variable
                                    LoggedinPermissionLevel = LoginDataRow.Item(8)

                                    ' clear the dataTable and the Connect information
                                    LoginDataAdapter = Nothing
                                    LoginDataTable.Clear()
                                    Me.DialogResult = System.Windows.Forms.DialogResult.OK
                                Else
                                    MessageBox.Show("Login failed, account is disabled." & vbCrLf & "Contact your Administrator.")
                                    ' clear the dataTable and the Connect information
                                    LoginDataAdapter = Nothing
                                    LoginDataTable.Clear()
                                End If
                            End If

                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                        'close the connection
                        If connection.State <> ConnectionState.Closed Then
                            connection.Close()
                        End If
                    Else
                        MessageBox.Show("Error: Computer is not connected to a network", "Network Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)
                    End If
                Catch ex As Exception
                    MsgBox("Availability error: " & ex.Message)
                End Try
            Else
                MsgBox("MS SQL Server not available:" & vbCrLf & "Ping request timed out.")
            End If
        Catch ex As InvalidOperationException
            MsgBox("Invalid Operation" & vbCrLf & ex.Message & vbCrLf & "Check your Network Connections")
            'MsgBox(ex.InnerException.ToString)
        Catch ex As Exception
            MsgBox("Login caused an error:" & vbCrLf & ex.Message)
        End Try
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim logName As String = loggedinUser()
        Dim myInteger As Integer
        myInteger = logName.IndexOf("\")
        If myInteger <> 0 Then
            Dim UserNam As String = logName.Substring(myInteger + 1)

            'set default loggin id
            UsernameTextBox.Text = UserNam
        End If
        'Set form title
        If My.Settings.IsTest = True Then
            Me.Text = "Test Application | " & Me.Text
            Me.BackColor = Color.MistyRose
            pnlTest.Visible = True
        End If
        'Set Version
        Me.Text = Me.Text & " Version " & My.Settings.Version
        
    End Sub

    
End Class
